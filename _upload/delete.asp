<%@ Language=VBScript %>
<%
'*************************************************************************
'
'*************************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%
'Database
dim cn, rs, mySQL

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'Work Fields
dim errMsg
dim delFile
dim fs, Upload
dim upDel
dim Directory

dim pageView : pageView = trim(Request.QueryString("pageView"))

dim idIssue, idIssueOriginal
idIssue = trim(Request.QueryString("recId"))
idIssueOriginal = trim(Request.QueryString("recId"))
'for consistency only, dummy number...
if lCase(pageView) <> "crystal" then
	idIssue = left(idIssue,len(idIssue)-3) & "-01"
end if


'used to redirect user back to tabs they last were on
dim topTab, sideTab
topTab = trim(Request.QueryString("top"))
sideTab = trim(Request.QueryString("side"))


'clear system messages
call clearSessionMessages()	

'*********************************************************************
'Start file delition
'*********************************************************************

'Make sure right flag is called
upDel = trim(Request.QueryString("upDel"))
if upDel <> "Yes" then
	errMsg = "File Deletion is not allowed."
	call endOfPage()
end if

'Get delete file (if any)
delFile = trim(Request.QueryString("del"))

'Set directory path
if lCase(pageView) = "crystal" then
	Directory = dataDir & "\profiles\" & CustomerID & "\crystal"
else
	if lCase(trim(Request.QueryString("dir"))) = "year" then
		Directory = dataDir & "\profiles\" & CustomerID & "\incoming\" & "20" & left(idIssue,2) & "\" & mid(idIssue,3,2) & "\" & idIssue & "\"
	else
		Directory = dataDir & "\profiles\" & CustomerID & "\incoming\" & idIssue	
	end if	
end if


on error resume next


'response.redirect "../error/default.asp?errMsg=" & server.URLEncode(Directory & "\" & delFile)
		

set Upload = Server.CreateObject("Persits.Upload")
if len(delFile) > 0 then
	Upload.DeleteFile Directory & "\" & delFile 
end if

'Get FileSystem Object
if err.number <> 0 then
	errMsg = err.Description & " : " & delFile
	call endOfPage()
end if

on error goto 0


'All went well return to report
if lCase(pageView) = "crystal" then	
	session(session("siteID") & "okMsg") = "File (" & delFile & ") was deleted."
	response.redirect "../reports/crystal/execute.asp?type=edit&recID=" & idIssueOriginal & "&cid=" & CustomerID & "&name=&top=" & topTab 
else	
	session(session("siteID") & "okMsg") = "File (" & delFile & ") was deleted."
	response.redirect "../scripts/" & session(session("siteID") & "issuesEdit") & "?action=edit&recID=" & idIssueOriginal & "&top=" & topTab & "&side=" & sideTab
end if


'*********************************************************************
'End of page processing
'*********************************************************************
sub endOfPage()
	'Clean up
	set Upload = nothing
	'Check for error message
	if len(errMsg) <> 0 then
		on error goto 0	
		session(session("siteID") & "errMsg") = errMsg				
		response.redirect "../scripts/" & session(session("siteID") & "issuesEdit") & "?action=edit&recID=" & idIssue & "&top=" & topTab & "&side=" & sideTab
	end if
	response.end
end sub
%>

<%
'close database connection
call closeDB()
%>



