<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%
'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

%>

<html>
<head>
<title>Upload Documents</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

	<%
'	NEED TO TEST WHY THIS IS TUNRED OFF!!!!!!
'	'Validate User Security, redirect to login if "false"
'	if loadUser() = false then
'		response.redirect ("../default.asp")
'	end if

'	if viewFileUpload <> "Y" then
'		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")	
'	end if
		
	dim pageView
	pageView = trim(Request.QueryString("pageView"))
	
	dim idIssue
	idIssue = trim(Request.QueryString("recId"))
	
	'for consistency only, dummy number...
	if lCase(pageView) = "crystal" then
		idIssue = "1001-AAA-10001-01"	
	'real report...
	else
		idIssue = left(idIssue,len(idIssue)-3) & "-01"
	end if
			
	'*****************************************************************
	'perform upload function...	
	dim UploadProgress, PID, Upload, Res, myDir, File, myCrystalFile
	
	Set UploadProgress = Server.CreateObject("Persits.UploadProgress")
	PID = UploadProgress.CreateProgressID()
	
	Set Upload = Server.CreateObject("Persits.Upload")
	
	' This is needed to enable the progress indicator
	Upload.ProgressID = Request.QueryString("PID")
	Upload.IgnoreNoPost = True ' to use upload script in the same file as the form...ignores first form load error

	if lCase(pageView) = "crystal" then
		Upload.OverwriteFiles = True
	else
		Upload.OverwriteFiles = False	
	end if

	if lCase(pageView) = "crystal" then
		myDir = dataDir & "\profiles\" & sCustomerID & "\crystal"
	else
		'''myDir = dataDir & "\profiles\" & sCustomerID & "\incoming\" & idIssue
		'CUSTID/YYYY/MM/CRSID directory format
		myDir = dataDir & "\profiles\" & sCustomerID & "\incoming\" & "20" & left(idIssue,2) & "\" & mid(idIssue,3,2) & "\" & idIssue
	end if

	'create directory if needed
	Upload.CreateDirectory myDir, True 
	'initiate upload	
	Upload.Save myDir
	
	'inform user of success
	If Upload.Files.Count > 0 Then
		Res = "Success! " & Upload.Files.Count & " files have been uploaded."
		
		if lCase(pageView) = "crystal" then
			For Each File in Upload.Files
				myCrystalFile = myCrystalFile & File.OriginalFileName
			Next	
			%>
			<script language="javascript">
				topWin.parent.setCrystalReport('<% =myCrystalFile %>');
            </script>
			<%		
		else

			for each File in Upload.Files
				%>				
				<script language="javascript">
					topWin.parent.addDocUploaded('<% =File.OriginalFileName %>','<% =File.LastWriteTime %>','<% =File.OriginalSize %>','<% =replace(File.Ext,".","") %>');
				</script>
				<%					
			next			
		
		end if
			
	Else
		Res = ""
	End If
	
	%>
	<script src="progress_ajax.js"></script> 

</head>

<body style="padding:20px;">

	<form name="uploadForm" METHOD="POST" ENCTYPE="multipart/form-data" action="default.asp?pid=<% = PID %>&pageView=<% =pageView %>&recID=<% =idIssue %>" OnSubmit="ShowProgress('<% = PID %>')" style="padding:0px; margin:0px;"> 

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/32/arrow_up.png" title="Issues" width="32" height="32" align="absmiddle"> 
       	<span class="popupTitle">
        	<% 
			if pageView = "" or len(pageView) <=0 then 
				response.write("Documents")
			elseif lCase(pageView) = "crystal" then 
				response.write("Crystal Reports")
			end if
			%>            
        </span>
   	</div>                   

	<table class="formTable" style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
    		<td colspan="2">
       			<fieldset style="border:1px solid #CCCCCC; background-color:#FFFFFF; ">
          			<legend>Information</legend>
                 		<div align="left" style="padding:5px;">
                       		<div style="float:left; height:50px;"><img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px;"></div>
							<div>Upload file(s) using the form below.<br>&nbsp;&nbsp;- Existing files <em>will be</em> overwritten.<br>&nbsp;&nbsp;- 10MB per file size limit.</div>
						</div>            
              	</fieldset>
        	</td>
      	</tr>
		<tr>
			<td style="text-align:right; font-weight:bold; color:#35487B; padding-right:6px; width:50px; white-space:normal;" nowrap>File 1:</td>
			<td align=left><input name="FILE1" class="inputLong" size="30" type="file"></td>
       	</tr>    
        <% if lCase(pageView) <> "crystal" then %>
		<tr>            
			<td style="text-align:right; font-weight:bold; color:#35487B; padding-right:6px; width:50px; white-space:normal;" nowrap>File 2:</td>
			<td align=left><input name="FILE2" class="inputLong" size="30" type="file"></td>
     	</tr>    
		<tr>            
			<td style="text-align:right; font-weight:bold; color:#35487B; padding-right:6px; width:50px; white-space:normal;" nowrap>File 3:</td>
			<td align=left><input name="FILE3" class="inputLong" size="30" type="file"></td>
      	</tr>    
        <% end if %>
      	<tr>
       		<td colspan="2">    
         		<!-- START Buttons -->
           		<div style="padding-top:5px; margin-bottom:0px; float:right;"> 	       		
					<input type="submit" value="Upload">          
                    <input type="submit" onClick="OnStop()" value="Stop!">
                    <% if lCase(pageView) = "crystal" then %>
	                    <input type="submit" onClick="this.blur(); SimpleModal.close();" value="Close">
                    <% else %>
	                    <input type="submit" onClick="this.blur(); SimpleModal.close();" value="Close">                    
                    <% end if %>                    
           		</div>
              	<!-- STOP Buttons -->                    
         	</td>
    	</tr>            
	</table>

	</form>

    <div style="width:100%; border-top:1px dotted #cccccc; padding-top:3px;"><strong>Progress:</strong></div>    
    <div id="txtProgress"></div>        
    <% = Res %>            

</body>
</html>

<%
'close database connection
call closeDB()
%>

