
<%
Set UploadProgress = Server.CreateObject("Persits.UploadProgress")
PID = UploadProgress.CreateProgressID()

Set Upload = Server.CreateObject("Persits.Upload")

' This is needed to enable the progress indicator
Upload.ProgressID = Request.QueryString("PID")
Upload.IgnoreNoPost = True ' to use upload script in the same file as the form.

Upload.Save "c:\upload"

If Upload.Files.Count > 0 Then
	Res = "Success! " & Upload.Files.Count & " files have been uploaded."
Else
	Res = ""
End If

%>

<script src="progress_ajax.js"></script> 


