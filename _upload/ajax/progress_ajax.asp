<HTML>
<HEAD>

<%
Set UploadProgress = Server.CreateObject("Persits.UploadProgress")
PID = UploadProgress.CreateProgressID()

Set Upload = Server.CreateObject("Persits.Upload")

' This is needed to enable the progress indicator
Upload.ProgressID = Request.QueryString("PID")
Upload.IgnoreNoPost = True ' to use upload script in the same file as the form.

Upload.Save "c:\upload"

If Upload.Files.Count > 0 Then
	Res = "Success! " & Upload.Files.Count & " files have been uploaded."
Else
	Res = ""
End If

%>

<script src="progress_ajax.js"></script> 

</HEAD>
<BODY>
<BASEFONT FACE="Arial" SIZE="2">

	<h3>Ajax-based Progress Bar (requires AspUpload 3.1)</h3>
	
	<P>

	<FORM METHOD="POST" ENCTYPE="multipart/form-data"
			ACTION="progress_ajax.asp?pid=<% = PID %>"
			OnSubmit="ShowProgress('<% = PID %>')"> 

	<INPUT NAME="TEST" SIZE="40"><BR>
	<INPUT TYPE="FILE" NAME="FILE1" SIZE="40"><BR>
	<INPUT TYPE="FILE" NAME="FILE2" SIZE="40"><BR>
	<INPUT TYPE="FILE" NAME="FILE3" SIZE="40"><P>

	<INPUT TYPE="SUBMIT" VALUE="Upload!">
	<INPUT TYPE="BUTTON" VALUE="Stop" OnClick="OnStop()">

	</FORM>

	<div id="txtProgress"></div>

	<% = Res %>


<P>

<A HREF="progress_ajax.zip">Download source files for this live demo</A>

</basefont>
</BODY>
</HTML>
