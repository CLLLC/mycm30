<%@ Language=VBScript %>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Form Submit Sample</title>
           
	<script language="JavaScript" src="../../_jquery/jquery-1.5.1.min.js"></script>
	<script language="JavaScript" src="../../_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../../_jquery/jquery-ui-1.8.11.custom.css" />
    
    <script src="../../_jquery/prompt/jquery.alerts.js"></script>
    <link rel="stylesheet" type="text/css" href="../../_jquery/prompt/jquery.alerts.css"/>

	<script type="text/javascript" src="../../_jquery/jform/jquery.form.js"></script>  
    
    <script type="text/javascript"> 
		$(document).ready(function() { 
			var options = { 
				success: showResponse  // post-submit callback 
			}; 		 
			// bind form using 'ajaxForm' 
			$('#frm').ajaxForm(options); 
		}); 
		 
		// post-submit callback 
		function showResponse(responseText, statusText, xhr, $form)  { 		 
			 alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + '\n\nThe output div should have already been updated with the responseText.');
		} 
		
		//for sending submit from <a> tag
		function autoPostForm() { 
			$("#frm").submit();
        } 				
    </script>    

</head>

<BODY>

	<!-- THIS ACTUALLY WORKED WITH AJAX AND DID NOT REFRESH THE FORM -->
    <!-- ONLY PROBLEM WAS NEEDED TWO FORMS 1 FOR THE ISSUE AND 1 FOR THE UPLOAD -->
    <!-- PAUSED THIS FOR NOW AND LEFT THINGS AS THEY ARE... -->


	<FORM ID="frm" METHOD="POST" ENCTYPE="multipart/form-data" ACTION="progress_jquery_upload.asp?pid=<% = PID %>" OnSubmit="ShowProgress('<% = PID %>')"> 

	<INPUT NAME="TEST" SIZE="40"><BR>
	<INPUT TYPE="FILE" NAME="FILE1" SIZE="40"><BR>
	<INPUT TYPE="FILE" NAME="FILE2" SIZE="40"><BR>
	<INPUT TYPE="FILE" NAME="FILE3" SIZE="40"><P>

	<INPUT TYPE="SUBMIT" VALUE="Upload!">
	<INPUT TYPE="BUTTON" VALUE="Stop" OnClick="OnStop()">

	</FORM>

	<% =Res %>
    

<P>

</BODY>
</HTML>
