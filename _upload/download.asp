<%
' AspUpload Code samples: text_download.asp
' Invoked by text.asp
' Copyright (c) 2001 Persits Software, Inc
' This file must not contain any HTML tags
%>

<!-- MUST BE HERE for directory path -->
<!--#include file="../_config/config.asp"-->

<%
'User & CustomerID
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'get Issue ID where file is located
dim idIssue
idIssue = trim(Request.QueryString("recID"))
if len(idIssue) <= 0 then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue #.")
end if

dim pageView, myDir
pageView = trim(Request.QueryString("pageView"))

dim CustomerID
CustomerID = trim(Request.QueryString("cid"))

'crystal file
if lCase(pageView) = "crystal" then
	myDir = dataDir & "\profiles\" & CustomerID & "\crystal\"
'real report...
else
	if lCase(trim(Request.QueryString("dir"))) = "year" then
		myDir = dataDir & "\profiles\" & CustomerID & "\incoming\" & "20" & left(idIssue,2) & "\" & mid(idIssue,3,2) & "\" & left(idIssue,len(idIssue)-3) & "-01" & "\"
	else
		myDir = dataDir & "\profiles\" & CustomerID & "\incoming\" & left(idIssue,len(idIssue)-3) & "-01\"	
	end if
end if

'get file to download
dim fileName
fileName = trim(Request.QueryString("Name"))
if len(fileName) <= 0 then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("File not found.")
end if
	
	
'initial ASP Upload component
Set Upload = Server.CreateObject("Persits.Upload")

' Build path to file
Path = myDir & "\" & fileName

' Trigger downloading of file...
'   Parmeters:
'   1. Path to file to download
'   2. Yes, build content-xxx headers
'   3. Use this value for Content-Type header
'   4. Include the word "attachment;" to Content-Disposition to force download

'Upload.SendBinary Path, True, "application/octet-binary", True
Upload.SendBinary Path, True, "application/octet-binary", True, chr(34) & fileName & chr(34) 
%> 

