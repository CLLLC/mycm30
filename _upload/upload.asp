<%@ Language=VBScript %>

<!--#include file="../_includes/_INCconfig_.asp"-->

<%
	dim pageView
	pageView = trim(Request.QueryString("pageView"))
	
	dim idIssue
	idIssue = trim(Request.QueryString("recId"))

	dim CustomerID
	CustomerID = trim(Request.QueryString("cid"))
	
	'not used for Crystal Reports
	if lCase(pageView) <> "crystal" and lCase(pageView) <> "import" and lCase(pageView) <> "mcr" then
		idIssue = left(idIssue,len(idIssue)-3) & "-01"
	end if
			
	'*****************************************************************
	'perform upload function...	
	dim UploadProgress, Upload, myDir, File, myCrystalFile
		
	'open ASPUpload object
	Set Upload = Server.CreateObject("Persits.Upload")
	
	'replace any existing RPT file
	if lCase(pageView) = "crystal" then
		Upload.OverwriteFiles = True
	elseif lCase(pageView) = "mcr" then
		Upload.OverwriteFiles = True		
	'overwite files name(2) if already found
	else
		Upload.OverwriteFiles = False		
	end if
	
	'set max file size 10mb
	Upload.SetMaxSize 10485760, True 
	
	'build dir structure
	if lCase(pageView) = "crystal" then
		myDir = dataDir & "\profiles\" & lCase(customerID) & "\crystal"
	elseif lCase(pageView) = "mcr" then
		myDir = mcrDir & "\" & lCase(customerID)
	elseif lCase(pageView) = "import" then
		myDir = dataDir & "\profiles\" & lCase(customerID) & "\import"
	else
		'CUSTID/YYYY/MM/CRSID directory format
		myDir = dataDir & "\profiles\" & lCase(customerID) & "\incoming\" & "20" & left(idIssue,2) & "\" & mid(idIssue,3,2) & "\" & idIssue
	end if

	'create directory if needed
	Upload.CreateDirectory myDir, True 
	
	'initiate upload
	Upload.Save myDir

	'inform user of success
	if Upload.Files.count > 0 Then
		for each File in Upload.Files
			'DNN - mapped M: drive from data server in to INT Web Server
			'put a copy of SQL server for BULK import
			'if lCase(pageView) = "import" then
			'	File.Copy "\\100.100.10.209\d$\Data\myCM\BulkFiles\" & File.FileName
			'end if		
			response.write("{""status"":""1"",""name"":""" & File.FileName & """,""size"":""" & File.OriginalSize & """,""date"":""" & File.LastWriteTime & """,""extension"":""" & replace(File.Ext,".","") & """}")
		next		
	'error in upload			
	else
		response.write("{""status"":""0"",""error"":""Invalid Upload (No file to upload)""}")
	end if

	'end page build
	response.end()	

%>
