<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="_includes/_INCconfig_.asp"-->
<!--#include file="_includes/_INCappDBConn_.asp"-->
<!--#include file="_includes/_INCappFunctions_.asp"-->
<!--#include file="_includes/_INCsecurity_.asp"-->

<%
'Database
dim mySQL, cn, rs
dim msg

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'site configuration
if loadConfig() = false then
	response.redirect "error/default.asp?errMsg=" & server.URLEncode("Could not load Configuration settings.")
end if

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

if cLng(session(session("siteID") & "adminLoggedOn")) < 3 or ProgramAdmin="Y" then 

	if lCase(trim(request.QueryString("action"))) = "ghost" then
	
		dim ghostAdminUser, ghostAdminPass, userRequestingGhost

		userRequestingGhost = session(session("siteID") & "adminLoggedOn")
		
		'****************************************
		'prepare for new user session
		'clear all past session variables and 
		'create RANDOM session for this user
		'session.abandon -- when this is called script failes
		randomize
		siteID = siteID & "_" & Int((rnd*99999))+1		
		session("siteID") = siteID
		'****************************************
			
		'Open Database Connection
		call openDB()
	
		'DBAdmins and CCI Admins
		if cLng(userRequestingGhost) < 3 then
			mySQL = "SELECT * FROM Logins WHERE Logid=" & request.QueryString("recID") & " "		
		
		'Program Admins for Customers
		elseif ProgramAdmin="Y" then		
			'mySQL = "SELECT * FROM Logins WHERE Logid=" & request.QueryString("recID") & " AND CustomerID ='" & sCustomerID & "' "
			mySQL = "SELECT * FROM Logins " _
				  & "	WHERE Logid=" & request.QueryString("recID") & " AND " _
				  & "		EXISTS ( " _
				  & "			SELECT vwLogins_IssueType.CustomerID, vwLogins_IssueType.LogID " _
				  & "				FROM vwLogins_IssueType " _
				  & "				WHERE (vwLogins_IssueType.CustomerID=Logins.CustomerID) AND (vwLogins_IssueType.LogID=" & sLogid & ") AND (vwLogins_IssueType.Active='Y') ) "
					  
		'Everyone else points back to themselves
		else
			mySQL = "SELECT * FROM Logins WHERE Logid=" & sLogid & " AND CustomerID ='" & sCustomerID & "' "
					
		end if
		
		'open user record
		set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,50)	
					
		'User found in database
		if not rs.eof then
			
			ghostAdminUser = rs("UserName")
			ghostAdminPass = rs("Password")			
			
			'PASSWORD matches database	
			if cLng(rs("LoginAttempts"))<5 and rs("active") = "Y" Then	
										
				'set user session variables
				session(session("siteID") & "adminLoggedOn") = rs("SecurityLevel")
				if cLng(session(session("siteID") & "adminLoggedOn")) <= 0 then
					call closeRS(rs)						
					msg = "Invalid permission settings. Please contact CCI at 1-800-617-0415."
					'session.abandon			
					response.redirect ("logon.asp")	
				end if
				
				session(session("siteID") & "firstname")	 = rs("FirstName")
				session(session("siteID") & "lastname")		 = rs("LastName")
				session(session("siteID") & "customerid")	 = rs("customerid")
				session(session("siteID") & "homepage")	 	 = rs("homepage")				
				session(session("siteID") & "logid")		 = rs("logid")

				'*********************************************
				'account needs to be RESET
				if rs("PasswordReset") <= date() then				
					if cLng(session(session("siteID") & "adminLoggedOn")) > 3 and ProgramAdmin="N" then	
						response.redirect "logon_reset.asp?action=reset&recid=" & rs("logid") & "&redirect="
					end if
				end if
				'*********************************************

				'***************************************************
				'Used to diirect user to proper issues_edit.asp page
				'	issues_edit.asp =  users level >= 10
				'	issues_edit_00.asp = users level < 10
				if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
					session(session("siteID") & "issuesEdit") = "issues_edit_00.asp"
				else
					session(session("siteID") & "issuesEdit") = "issues_edit.asp"
				end if		
				'***************************************************
				
				'before loggin in set session userGhost=Yes so Autosave and other 
				'functions are deactivated when navigating and using myCM
				session(session("siteID") & "userGhost") = "Yes"
				
				'Send to issues page
				if session(session("siteID") & "homepage") = "profiles.asp" then					
					response.redirect siteURL & "profiles/profiles.asp"
				elseif session(session("siteID") & "homepage") = "issues.asp" then					
					response.redirect siteURL & "scripts/issues.asp"
				elseif len(session(session("siteID") & "homepage")) > 0 then									
					response.redirect siteURL & session(session("siteID") & "homepage")
				end if
			
			'Login Attempts >= 5 OR not active
			else

				'*********************************************
				' Update number of attempts login
				mySQL = "UPDATE Logins SET " _
					  & "LoginAttempts=[LoginAttempts]+1 " _
					  & "WHERE Username = '" & ghostAdminUser & "' AND Active = 'Y'"
				set rs = openRSexecute(mySQL)
	
				'Open Recordset of Users
				mySQL = "SELECT Active, LoginAttempts FROM Logins WHERE Username = '" & ghostAdminUser & "' "
				set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,50)	
				
				'account is INACTIVE
				if rs("Active") = "N" then					
					response.redirect "logon_locked.asp?t=inactive"
				
				'account is LOCKED
				elseif cLng(rs("LoginAttempts")) >= 5 then					
					response.redirect "logon_locked.asp?t=locked"
				
				'wrong credintials
				else					
					msg = "Invalid login credentials. Please try again."				
					session.abandon
					response.redirect ("logon.asp")
					
				end if			

			end if
						
		'User NOT found in database
		else						
			msg = "Invalid login credentials. Please try again."
			session.abandon
			response.redirect ("logon.asp")
			
		end if
		
	'BAD request kill all and send home
	else
		'kill all session variables and cookies
		session.abandon 	
		%>        
		<!-- CCI Common Javascript functions -->
		<script type="text/javascript" src="scripts/javascript/common.js"></script>   
		<script language="javascript">	
			destroyAllCookies();
		</script>
		<%					
		response.redirect ("logon.asp")
	
	end if

'*********************************************	
'BAD request kill all and send home
else
	'kill all session variables and cookies
	session.abandon 	
	%>        
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="scripts/javascript/common.js"></script>   
    <script language="javascript">	
       	destroyAllCookies();
	</script>
    <%
	response.redirect ("logon.asp")
	
end if
%>


<%
'close database connection
call closeDB()
%>


