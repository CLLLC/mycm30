<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->

<%
'Database
dim mySQL, cn, rs, rs2

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'Products
dim idReport
dim arrTemp
dim tempI
dim i

'User Fields
dim optName
dim customerID
dim optDesc
dim optDataset
dim optDatasetID
dim optUserRole
dim optUser
dim rptLogid
dim optType
dim optQueryValue
dim optSQLValue
dim optFields
dim optFilter
'dim optGroup
'dim optGroupCalc
dim optSortOrder
dim optSortField
dim optIssueList
'dim optPublic
dim optURLField
dim optURLFieldMissing
dim optCategory
dim optNotes

'Chart Values
dim optChart
dim optChartXAxis
'dim optChartXRotate
dim optChartYAxis
dim optChartSize
dim optChartLegend
'dim optChartBackground
'dim optChartCanvas
'dim optChartElement


'ADD FILTER
dim expI
dim arrField
dim arrExp
dim arrValue
dim tmpValue

'Work Fields
dim action

'Get action
action = trim(lCase(Request.Form("action")))
if len(action) = 0 then
	action = trim(lCase(Request.QueryString("action")))
end if
if  action <> "edit" _
and action <> "del"  _
and action <> "add"  _
and action <> "bulkdel" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if

'Get idReport
if action = "edit" _
or action = "del"  then

	idReport = trim(Request.Form("idReport"))
	if len(idReport) = 0 then
		idReport = trim(Request.QueryString("recID"))
	end if
	if idReport = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Report ID")
	end if

end if


'Page to return user too.
dim pageReturn
pageReturn = trim(Request.QueryString("return"))
if len(pageReturn) = 0 then
	pageReturn = trim(Request.Form("pageReturn"))
end if


'used to redirect user back to tabs they last were on
dim topTab, sideTab
topTab = trim(Request.Form("topTab"))
if len(topTab) = 0 then
	topTab = trim(Request.QueryString("top"))
end if

'Get Metric Details
if action = "edit" or action = "add" then

	'Get Report Type --> Required (This MUST be left in 1st place.)
	optType = trim(Request.Form("optType"))
	if len(optType) = 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Metric Type.")
	end if

	'Get Report Name --> Required
	optName	= trim(Request.Form("optName"))
	if len(optName) = 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Metric Name.")
	end if

	'Get Report Dataset --> Required
'	optDataset	= trim(Request.Form("optDataset"))
'	if len(optDataset) = 0 then
'		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Dataset.")
'	end if

	'Get Report Dataset --> Required
	optDatasetID = trim(Request.Form("optDatasetID"))
	if len(optDatasetID) = 0 and idReport <> 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Dataset.")
	end if



	'Get Report owner --> Required
	customerID = trim(Request.Form("customerID"))
	if len(customerID) = 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if

	'Get Report owner --> Required
	rptLogid = trim(Request.Form("rptLogid"))
	if len(rptLogid) = 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Report Owner.")
	end if

	'Get Option Name --> Required
	optIssueList = trim(Request.Form("optIssueList"))
	if len(optIssueList) = 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue List assignment.")
	end if

	'Get Option Name --> Required
	optURLField = trim(Request.Form("optURLField"))

	'Get Category --> Optional
	optCategory = trim(Request.Form("optCategory"))

	'Get Notes --> Optional
	optNotes = trim(Request.Form("optNotes"))


	'Get Display Fields --> Required
	if lCase(optType) = "register" or lCase(optType) = "calculated" then

		'Get Sort Order --> Optional
		optSortOrder = trim(Request.Form("optSortOrder"))

		'Get Sort Order --> Optional
		optSortField = trim(Request.Form("optSortField"))
		if len(trim(optSortField)) > 0 then
			optSortField = "[" & optSortField & "]"
		end if

'		if lCase(optType) = "register" then	
			'Get Assigned Fields --> Required
			optFields = trim(Request.Form("list_checkBox"))
			if (len(optFields) = 0 or optFields = "" or isNull(optFields)) and idReport<>0 then
				'response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Field Selection. At least one field must be selected.")
				if lCase(optType) = "register" then	
					optFields = "[Issue #]"
				elseif lCase(optType) = "calculated" then	
					optFields = "[Issue_#]"				
				end if
			end if
'		end if
						
		'Get Chart Specific Options
		if lCase(optType) = "calculated" then	
	
			'Get Chart Type --> Required
			optChart = trim(Request.Form("optChart"))
			if len(optChart) = 0 then
				optChart = "Column"
			end if
	
			'Get Chart X Axis Title --> Required
			optChartXAxis = trim(Request.Form("optChartXAxis"))
			if len(optChartXAxis) = 0 then
				response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid X-Axis Selection.")
			end if
	
			'Get X-Axis Orientation --> Required
	'		optChartXRotate = trim(Request.Form("optChartXRotate"))
	'		if len(optChartXRotate) = 0 then
	'			response.redirect "sysMsg.asp?errMsg=" & server.URLEncode("Invalid X-Axis Orientation.")
	'		end if
	
			'Get Chart X Axis Title --> Optional
			optChartYAxis = trim(Request.Form("optChartYAxis"))
	
			'Get Chart Size --> Required
			optChartSize = trim(Request.Form("optChartSize"))
			if len(optChartSize) = 0 then
				optChartSize = "medium"
			end if
	
			'Get Chart Background Color --> Optional
	'		optChartBackground = trim(Request.Form("optChartBackground"))
			
			'Get Chart Canvas Color --> Optional
	'		optChartCanvas = trim(Request.Form("optChartCanvas"))		
	
			'Get Chart Legend --> Optional
			optChartLegend = trim(Request.Form("optChartLegend"))		
			
		end if		
	
	'Get SQL Statement --> Required
	elseif lCase(optType) = "sql" then	
		optSQLValue = trim(Request.Form("optSQLValue"))
		if len(optSQLValue) = 0 then
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Query Statement.")			
		end if		
	end if

	
end if


'*********************************
'get array of all fields selected
'*********************************
'NOT SURE ABOUT LOOKING FOR "CALCULATED"...MIGHT WANT TO LOOK FOR SOMETHING ELSE......
if lCase(optType) <> "sql" then
	arrField = split(optFields,",")
	optFields = ""
	'Loop through all selected fields one by one
	for expI = 0 to UBound(arrField)
		'add to column list if NOT equal to defined ListURLField
		if lCase(trim(arrField(expI))) <> lCase("[" & optURLField & "]") then
			optFields = optFields & ", " & trim(arrField(expI))
			'used for reports without a defined ListURLField in the Dataset table
			if len(optURLFieldMissing) = 0 and (lCase(trim(arrField(expI))) <> lCase("[total]")) then
				optURLFieldMissing = trim(arrField(expI))
			end if
		end if			
	next	
	'clean up extra comma ","
	if len(optFields) > 0 then
		optFields = right(optFields,len(optFields)-2)
	end if	

	'add these back in for issue list...REQUIRED FIELDS...the checkboxes are disabled on reportsCongigure.asp
	if idReport=0 then
		optFields = "[Date],[Severity]," & optFields
		if right(optFields,1) = "," then
			optFields = mid(optFields,1,len(optFields)-1)
		end if
	end if	
	
end if


'*********************************
'set first column if dataset 
'provides one, used to navigate
'back to issues.asp
'*********************************
if len(optURLField) > 0 then
	if len(optFields) > 0 then
		optFields = "[" & optURLField & "], " & optFields	
	else
		optFields = "[" & optURLField & "]"
	end if

'Dataset does not have a defined ListURLField, 
'so add first column selected
elseif len(optURLField) <= 0 and lCase(optType) <> "sql" then
	optURLField = optURLFieldMissing
	
end if


'******************************
'build filter XML with filter
if action = "edit" or action = "add" then	
	
	optFilter = "<?xml version='1.0' encoding='ISO-8859-1'?>"
	optFilter = optFilter & "<filters>"

	'------------------------------------------------
	'find all form elements in filter box
	'------------------------------------------------	
	'discover filtered fields
	arrField = split(Request.Form("idQueryField"),",")
	'discover operators on each filtered field
	arrExp = split(Request.Form("idQueryExp"),",")
	'discovery query values entered by user
	for i = 1 To request.form("idQueryValue").Count 
		arrValue = arrValue & request.form("idQueryValue")(i) & "|"
  	next 
	if right(arrValue,1) = "|" then arrValue = left(arrValue,len(arrValue)-1)
	arrValue = split(arrValue,"|")
	
	'------------------------------------------------
	'Loop through filter list and add one by one to XML
	'------------------------------------------------	
	for expI = LBound(arrField) to UBound(arrField)	
		'Clean up value and replace where needed
		if len(trim(Request.Form("idQueryValue"))) > 0 then
			tmpValue = trim(arrValue(expI))
			tmpValue = replace(tmpValue,"*","%")
			tmpValue = replace(tmpValue,"""","'")
		else
			tmpValue = ""
		end if
		'put string together
		optFilter = optFilter & "<filter>"
		optFilter = optFilter & "<field>" & trim(arrField(expI)) & "</field>"
		optFilter = optFilter & "<operator>" & Server.HTMLEncode(trim(arrExp(expI))) & "</operator>"		
		optFilter = optFilter & "<value>" & tmpValue & "</value>"
		optFilter = optFilter & "</filter>"		
	next	
	'close XML
	optFilter = optFilter & "</filters>"
	'------------------------------------------------
	
end if

'******************************

'ADD report
if action = "add" then
	'right now...nothing: reports are added in /dataset/default.asp	
end if


'EDIT report
if action = "edit" and idReport <> 0 then
		
	'Update Record
	mySQL = "UPDATE Reports SET " _
		  & "CustomerID='"		& customerID						& "'," _		  
		  & "DatasetID="		& optDatasetID						& ", " _
		  & "Logid="   			& rptLogid							& ", " _
		  & "Name='"   			& replace(optName,"'","''")			& "'," _
		  & "Category='"   		& replace(optCategory,"'","''")		& "'," _		  
		  & "IssueList='"		& optIssueList						& "'," _
  		  & "QueryFields='"		& optFields							& "'," _
  		  & "QueryFilter='"		& replace(optFilter,"'","''")		& "'," _		  
		  & "SQL='"				& replace(optSQLValue,"'","''")		& "'," _	
		  & "QuerySortOrder='"	& optSortOrder						& "'," _		  		  		  		  		  		  		  		  		  		  		  		  		  
  		  & "QuerySortField='"	& optSortField						& "'," _
		  & "Notes='"			& replace(optNotes,"'","''")		& "'," _		  
		  & "ListURLField='"	& optURLField						& "'," _
		  & "ChartType='"		& optChart							& "'," _		
		  & "ChartXAxis='"		& optChartXAxis						& "'," _		  		  		  
		  & "ChartYAxis='"		& optChartYAxis						& "'," _
		  & "ChartLegend='"		& optChartLegend					& "'," _
		  & "ModifiedBy="		& sLogid							& ", " _		  
		  & "ModifiedDate='"    & Now()								& "' "		  		  
'		  & "ChartSize='"		& optChartSize						& "' " _	

	'finalize update query
	mySQL = mySQL & "WHERE ReportID = " & idReport

	set rs = openRSexecute(mySQL)

	session(session("siteID") & "okMsg") = "Report (" & optName & ") was saved successfully."
	Response.Redirect "../reports/reports_edit.asp?action=edit&recID=" & idReport & "&top=" & topTab & "&return=" & pageReturn
	
elseif action = "edit" and idReport = 0 then

	'Update Record
	mySQL = "UPDATE Logins SET " _
		  & "IssuesColumns='"	& optFields						& "'," _
  		  & "WhereClause='"		& replace(optFilter,"'","''")	& "'," _
  		  & "sortField='"		& optSortField					& "'," _
		  & "sortOrder='"		& optSortOrder					& "'," _
		  & "ModifiedBy="		& sLogid						& ", " _
		  & "ModifiedDate='"    & Now()							& "' "		  
		  		  
  	'finalize update query
	mySQL = mySQL & "WHERE LogID = " & sLogid
	
	set rs = openRSexecute(mySQL)

	session(session("siteID") & "okMsg") = "Report (Issues) was saved successfully."
	Response.Redirect "../reports/reports_edit.asp?action=edit&recID=0" & "&top=" & topTab & "&return=" & pageReturn

end if


'DELETE or BULK DELETE
if action = "del" then

	'Get RPQID of INSERTed Record
	mySQL = "SELECT Name, LogID " _
		  & "	FROM   	Reports " _
		  & "	WHERE 	ReportID=" & idReport '& " AND LogID=" & sLogid & " AND CustomerID='" & customerID & "'"		  
    set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,0)
	optName = rs("name")
	rptLogid = rs("LogID")
	call closeRS(rs)

	'Set CursorLocation of the Connection Object to Client
	cn.CursorLocation = adUseClient
	
	'BEGIN Transaction
	cn.BeginTrans
		
	'Delete records from Categories_Products
	mySQL = "DELETE FROM Reports " _
		  & "	WHERE ReportID=" & idReport '& " AND LogID=" & sLogid & " AND CustomerID='" & sCustomerID & "'"

	'non-CCI staff and non-ProgramAdmins can ONLY view their own reports
	if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and ProgramAdmin<>"Y" then		  
		mySQL = insertWhereClause(mySQL,"Reports.CustomerID = '" & sCustomerID & "' and Reports.LOGID=" & sLogid & "")
							
	elseif cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and ProgramAdmin="Y" then		  
		mySQL = insertWhereClause(mySQL,"Reports.CustomerID IN (" & profileIDs & ") ")

	end if	  
	'execute deletion
	set rs = openRSexecute(mySQL)
				
	'END Transaction
	cn.CommitTrans
		
	'close database connection
	call closeDB()

	session(session("siteID") & "okMsg") = "Report (" & optName & ") was deleted successfully."

	if pageReturn = "user" then
		response.redirect "../profiles/users_edit.asp?action=edit&recID=" & rptLogid & "&top=" & topTab & "&return=" & pageReturn		
	else
		response.redirect "../reports/reports.asp"	
	end if

end if


'Just in case we ever get this far...
call closeDB()
response.redirect "../reports/reports.asp"

%>
