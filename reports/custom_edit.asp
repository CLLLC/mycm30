<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = false
const showDHTMLMenu = true
const helpID = 5
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "custom:edit"
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

dim showCRSID '...need for NEW search box
dim startDate
dim endDate

'Issue Fields on DETAILS tab
'dim optID
dim optName
dim optSort
dim optQueryValue
dim optSQLValue
dim optUserRole
dim optUser
dim rptLogid
dim optUsers
'dim optOwner
dim rptOwner
dim optDataset
dim optDatasetDesc
dim optTable
dim optType
dim optDesc
dim optFields
dim optFilter
dim optIssueList
dim optURLField
dim optGroup
dim optGroupCalc
dim optSortOrder
dim optSortField
dim optCategory
dim intStep
dim strTemp
dim optNotes
dim optDatasetCustID
dim optParamDate

dim rptProfileCount

'Work Fields
dim action, idProduct, pgNotice
dim idIssue

'Work Fields
dim optArr, optArrIndex

'Used with Dynamic List Contact selections
dim intGroups, arrGroups, groupsI, intUsers
dim tmpUserID, tmpUserName, lstUserName
dim arrUsers, usersI
dim idReport


'Check User Security
if trim(session(session("siteID") & "viewStats")) = "N" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to view this page.")	
end if

'Page to return user too.
dim pageReturn
pageReturn = trim(Request.QueryString("return"))
if len(pageReturn) = 0 then
	pageReturn = trim(Request.Form("pageReturn"))
end if

'Get action
action = trim(Request.QueryString("action"))
if len(action) = 0 then
	action = trim(Request.Form("action"))
end if
action = lCase(action)
if  action <> "edit" _
and action <> "add" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if


'Get idIssue
if action = "edit" then
	idReport = trim(Request.QueryString("recId"))
	if len(idReport) = 0 then
		idReport = trim(Request.Form("recId"))
	end if
	if idReport = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Report ID.")
	end if
end if


'Get idIssue
if action = "add" then
	optDataset = trim(Request.QueryString("dataset"))
	if len(optDataset) = 0 then
		optDataset = trim(Request.Form("dataset"))
	end if
	if optDataset = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Report Dataset.")
	end if
end if


'used to set side top and side tabs after user saves record
dim topTab
topTab = trim(Request.QueryString("top"))
if len(topTab) <= 0 then
	topTab = "configure"
end if


'Get Crystal file
dim crystalFile, crystalFileError
crystalFile = Request.QueryString("name")


'Get Parameters
dim strParameters
strParameters = lCase(Request.QueryString("p"))

	
'Get Issue Record... idReport 0 == Issue List on home page
if action = "edit" then

	'get customer record
	mySQL="SELECT Reports.ReportID, Reports.CustomerID, Dataset.CustomerID AS [Dataset_CustomerID], Dataset.Name AS Dataset, Dataset.[Table], Dataset.Type, Dataset.ListURLField, Dataset.Description, Reports.LogID, Reports.Name, Reports.IssueList, " _
		& "       Reports.QueryFields, Reports.QueryFilter, Reports.QuerySumGroup, Reports.QuerySumCalculation, Reports.Category, Reports.ParameterDate, " _
		& "       Reports.QuerySortOrder, Reports.QuerySortField, Reports.SQL, Reports.ChartType, Reports.ChartXAxis, Reports.ChartYAxis, Reports.ChartSize, Reports.SortOrder, Reports.Notes, " _
		& "       Logins.FirstName, Logins.LastName, Logins.Email " _
 	    & "	FROM  Reports INNER JOIN " _
		& "			Dataset ON Reports.DatasetID = Dataset.DatasetID LEFT OUTER JOIN " _
		& "			Logins ON Reports.CustomerID = Logins.CustomerID AND Reports.LogID = Logins.LOGID " _
	    & " WHERE ReportID = " & idReport & " "
		
	'non-CCI staff and non-ProgramAdmins can ONLY view their own reports
	if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and ProgramAdmin<>"Y" then		  
		mySQL = insertWhereClause(mySQL,"Reports.CustomerID = '" & sCustomerID & "' and Reports.LOGID=" & sLogid & "")
							
	elseif cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and ProgramAdmin="Y" then		  
		mySQL = insertWhereClause(mySQL,"Reports.CustomerID IN (" & profileIDs & ") ")

	end if	  

	'set rs = openRSexecute(mySQL)
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	
	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Report ID.")
	else
		'optID			 = rs("ReportID")
		customerID		 = rs("customerID")		
		rptLogid		 = rs("Logid")
		optName			 = rs("Name")
		optCategory		 = rs("Category")
		optDatasetCustID = rs("Dataset_CustomerID")				
		optDataset		 = rs("Dataset")
		optDatasetDesc	 = rs("Description")
		optType			 = rs("Type")
		optNotes		 = rs("Notes")
		optParamDate	 = rs("ParameterDate") 
		
		'set optOwner...used for display only
		if len(rs("LastName")) > 0 then rptOwner = rs("LastName")
		if len(rs("FirstName")) > 0 and len(rptOwner) > 0 then rptOwner = rptOwner & ", " & rs("FirstName") else rptOwner = rs("FirstName")
		if len(rs("Email")) > 0 then rptOwner = rptOwner & " [" & rs("Email") & "]"
		if len(rs("ParameterDate")) < 0 or rs("ParameterDate")="" or isNull(rs("ParameterDate")) then optParamDate = "1/1/2000"
		
	end if
	call closeRS(rs)
	
end if


'Set JSon params and data
if action = "edit" then

	'CCI staff -- limit jSon data pulls to customerID this report belongs too
	if cLng(sLogid) <> cLng(rptLogid) then		  
	
		mySQL = "SELECT CustomerID " _
			  & "	FROM vwLogins_IssueType " _
			  & "	WHERE LOGID = " & rptLogid & " " _
			  & "	GROUP BY CustomerID "			
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)			  		
		rptProfileCount = rs.recordcount
		
'		do while not rs.eof
'			jsonCID = jsonCID & "'" & rs("CustomerID") & "',"
'			rs.movenext
'		loop
'		jsonCID = left(jsonCID,len(jsonCID)-1)
				
	else
		rptProfileCount = profileCount
		
	end if	  

end if


'Set initial dates
dim fromStart, toStart
dim crystalCookie
crystalCookie = request.cookies("myCM3.0-Crystal")
'make sure valid dates, the <5 makes sure the cookie doesn't start with "*|*"
if len(crystalCookie) < 1 or crystalCookie = "" or inStr(crystalCookie,"*|*") < 5 then
	fromStart = "01/01/" & year(date())
	toStart = "12/31/" & year(date())
else
	crystalCookie = split(crystalCookie,"*|*")
	fromStart = trim(crystalCookie(0))
	toStart = trim(crystalCookie(1))
end if
%>

<% 
dim pageTitle
if action = "add" then
	pageTitle = "Report: (new report)" 
else
	pageTitle = optName & " (edit)"
end if
%>


<!--#include file="../_includes/_INCheaderLite_.asp"-->


<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
  
	    <!-- START Left sided area -->  
		<td width="190" align="right" valign="top">    

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="../profiles/users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">
    
                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->              
                    <% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>      
	                    <script>
   		                	stExpandSubTree('myCMtree',3,0);
       		            </script>
            		<% else %>
	                    <script>
   		                	stExpandSubTree('myCMtree',2,0);
       		            </script>
                    <% end if %>
                    <!-- STOP side menu include -->
                                        
                </td>
              </tr>
              <!-- STOP Left sided menu -->
        
            </table>

		</td>
    	<!-- STOP Left sided area -->


        <!-- START Main Middle Section -->
        <td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">    

			<%
			'used to STOP updating the cookie crumb if page is being recalled from a SAVE
			dim updateCrumb
            if len(session(session("siteID") & "errMsg")) > 0 or len(session(session("siteID") & "okMsg")) > 0 then
				updateCrumb = "false"
            end if
			%>
            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs('<% =updateCrumb %>'));
       	        </script>
            </div>
    
            <div style="text-align:left; margin-bottom:10px;">				
                <div style="float:left; padding-right:2px; margin-right:2px;"><img src="../_images/icons/32/file_extension_rpt.png" title="Issues" width="32" height="32" align="absmiddle" style="vertical-align:middle;"></div>
                <div>
                	<span class="pageTitle">Report: <% =optName %></span>
	               	<br/>:: <a href="../profiles/users_edit.asp?action=edit&recid=<% =rptLogid %>&pageview=users&cid=<% =customerID %>"><% response.write( rptOwner ) %></a>                                    
                </div>
            </div>                   
            
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
            	            <legend>Information</legend>
	                        <div align="left">
    	                        <img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; vertical-align:middle;">Use the interface below to select filters to apply to this report.
								<% response.write( getInlineHelp("getPageURL", 0) ) %>
        	                </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             
                                    
                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">
                    	<% if addReport="Y" then %>
	                     	<div class="infoButtons">
    	                    	<a href="../datasets/default.asp"><img src="../_images/icons/24/rpt_add.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
        	                    <a href="../datasets/default.asp">Add Report</a>
            	            </div>                   
                        <% end if %>                                                
                    </td>
                    <!-- STOP Information buttons -->
                        
                </tr>
            </table>        
            <!-- STOP information box -->


    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage_2","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			
            if len(session(session("siteID") & "errMsg")) > 0 or len(crystalFile) <= 0 or crystalFile = "" or isNull(crystalFile) then
				'used to disable all javascript and calendars
				crystalFileError = "Y"
				'somethings wrong w file, clear it out!
				crystalFile = ""
			end if			
			
			'clear system messages
			call clearSessionMessages()
            %>
    		<!-- STOP message notification box -->


            <!-- START Top Page section with tabs & buttons -->
            <table id="table_tabs" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                
                    <!-- START Top Page Tabs-->
                    <td height="40" width="70%" valign="top">
                        <ul id="ul_tabs" class="tabs">
                            <li id="configure" style="width:125px;" class="on" onClick="clickTopTab('configure',''); changeObjectDisplay('saveButtonTop', 'none'); changeObjectDisplay('saveButtonBottom', 'none'); resetSystemMsg('systemMessage');"><div>Configuration</div></li>
                            <li id="settings" style="width:125px;" class="off" onClick="clickTopTab('settings',''); changeObjectDisplay('saveButtonTop', ''); changeObjectDisplay('saveButtonBottom', ''); resetSystemMsg('systemMessage');"><div>Settings</div></li>
                            <!-- the last id must be set to "end" -->
                            <li id="end" class="empty" style="width:150px;"><div>&nbsp;</div></li>
                        </ul>
                    </td>                
                    <!-- STOP Top Page Tabs-->              
                    
                    <!-- START Submit Button at top of page--> 
                    <td nowrap style="padding-bottom:10px;" align="left">
                        <%
						'ALL users can edit Crystal reports
                        if action="edit" then    
							%>
							<a id="saveButtonTop" class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); resetSystemMsg('systemMessage'); document.frmSave.submit(); return false;"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
							<%
                        elseif action="add" then
							if addReport="Y" then
                        		%>                            
								<a class="myCMbutton" href="#" accesskey="A" onClick="this.blur(); document.frmSave.submit(); return false;"><span class="add" style="padding-right:15px;"><u>A</u>dd</span></a>
                      			<%
							else
								%>
		                        <a class="myCMbutton-off" href="#" onClick="this.blur(); return false;"><span class="add" style="padding-right:7px;"><u>A</u>dd</span></a>
								<%
							end if
                        end if
                        %>
                        
                        <!-- CLOSE button -->
                        <%
                        if pageReturn = "user" then
                        	%>
							<a class="myCMbutton" href="#" accesskey="R" onClick="this.blur(); navigateAway('../profiles/users_edit.asp?action=edit&recid=<% =rptLogid %>&pageview=users&cid=<% =customerID %>&top=reports&side='); return false;"><span class="return" style="padding-right:5px;"><u>R</u>eturn</span></a>
							<%
                        else
                        	%>                        
							<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); location.href='reports.asp'; return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>
                        	<%
                        end if
                        %>

                    </td>
                    <!-- STOP Submit Button -->
                             
                </tr>
            </table>
            <!-- STOP Top Page Tabs -->
            
            
            <!-- START Side Page Tabs [details] "id=tab:details" defines the section for the Top Tabs -->
            <form name="frmCrystal" id="frmCrystal" method="post" action="crystal/execute.asp" target="_blank" style="padding:0px; margin:0px;">
               	<!-- hidden fields --> 
    	    	<input name="type" id="type" type="hidden" value="" />
   		    	<input name="fileName" id="fileName" type="hidden" value="<% =crystalFile %>" />                
                <input name="reportID" id="reportID" type="hidden" value="<% =idReport %>" />                
                
            	<!--#include file="../_includes/_INCcustomConfigure_.asp"-->            
            </form>
            <!-- STOP Side Page Tabs [details] -->    
        
        
            <!-- START Side Page Tabs [notes] "id=tab:notes" defines the section for the Top Tabs -->
            <form name="frmSave" id="frmSave" method="post" action="custom_exec.asp" style="padding:0px; margin:0px;">
               	<!-- hidden fields --> 
                <input type="hidden" name="idReport" id="idReport" value="<% =idReport %>">
                <input type="hidden" name="customerID" value="<% =customerID %>" >             
                <input type="hidden" name="return" value="<% =pageReturn %>">                                           
                <input type="hidden" name="topTab" id="topTab" value="<% =topTab %>">                
                <input type="hidden" name="optDataset" id="optDataset" value="<% =optDataset %>">
                <input type="hidden" name="optType" id="optType" value="<% =optType %>">	
                <input type="hidden" name="action" value="<% =action %>">
    	        <!--#include file="../_includes/_INCcustomSettings_.asp"-->
            </form>            
            <!-- STOP Side Page Tabs [notes] -->


			<div style="padding:10px;">&nbsp;</div>    
    
            <!-- START Final table -->        
            <table class="formTable" width="100%" cellpadding="0" cellspacing="0">      
                                                              
                <tr>
                    <td width="70%" valign="top" align="left" nowrap>&nbsp;</td>
                    
                    <!-- START Submit Button at bottom of page -->                 
                    <td valign="top" align="left" nowrap>                
                        <!-- SAVE/ADD button -->                                       
                        <%
						'ALL users can edit Crystal reports
                        if action="edit" then    
							%>
							<a id="saveButtonBottom" class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); resetSystemMsg('systemMessage'); document.frmSave.submit(); return false;"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
							<%
                        elseif action="add" then
							if addReport="Y" then
                        		%>                            
								<a class="myCMbutton" href="#" accesskey="A" onClick="this.blur(); document.frmSave.submit(); return false;"><span class="add" style="padding-right:15px;"><u>A</u>dd</span></a>
                      			<%
							else
								%>
		                        <a class="myCMbutton-off" href="#" onClick="this.blur(); return false;"><span class="add" style="padding-right:7px;"><u>A</u>dd</span></a>
								<%
							end if
                        end if
                        %>

                        <!-- CLOSE button -->
                        <%
                        if pageReturn = "user" then
                        	%>
							<a class="myCMbutton" href="#" accesskey="R" onClick="this.blur(); navigateAway('../profiles/users_edit.asp?action=edit&recid=<% =rptLogid %>&pageview=users&cid=<% =customerID %>&top=reports&side='); return false;"><span class="return" style="padding-right:5px;"><u>R</u>eturn</span></a>
							<%
                        else
                        	%>                        
							<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); location.href='reports.asp'; return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>
                        	<%
                        end if
                        %>

                    </td>
                    <!-- STOP Submit Button --> 
    
                </tr>      
                                      
            </table>
            <!-- STOP Final table -->                
            
    
	    </td>         
	    <!-- STOP Main Middle Section -->

 
    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
	</tr>
</table>
<!-- STOP Page -->

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<script>
	//initial setting for save buttons
	changeObjectDisplay('saveButtonTop', 'none'); 
	changeObjectDisplay('saveButtonBottom', 'none');
</script>

<script>
	//build calendars
	var dateCal;
    window.onload = function() {
    	//from date calendar
        dateCal = new dhtmlxCalendarObject(['fromDate','toDate','optParamDate'], true, {isWinHeader:true, headerButtons:"XT", isWinDrag:false, isYearEditable:true, isMonthEditable:true });
        dateCal.setSkin("simplegrey");
        dateCal.setDateFormat("%m/%d/%Y");
        dateCal.setYearsRange(2000, 2020);
        dateCal.setHeaderText("Date Range");
		
		//load form where user last was...pulled from query URL or form submit
		clickTopTab('<% =topTab %>','');		
	}		
</script>

<script language="javascript">
	//execute Crytal Reports
	function runCrystal(type) {
		var ele = document.getElementById('type');
		ele.value = type
		document.forms["frmCrystal"].submit(); 
	}	
	
	function setCurrentLocation(sNameID,sName,sAddress,sCity,sState,sPostal,sCountry,sHierarchy) {		
		//fill in location fields from select location_popup.asp
		document.getElementById('location_name').value = decodeURI(sName);		
	}

	//hide/show caller related fields based on Anonynimity
	function setCrystalReport(fileName) {						
		var el = document.getElementById('optFileName');	
		el.value = fileName;
		el.text = fileName;
	}	
</script>

<script language="javascript">
	function setOwner(text,id) {
		//used by child window to add selected Report Owners
		var addE = document.getElementById('rptOwner');
		addE.value = text

		//check and see if value being added is in Removed list
		var delE = document.getElementById('rptLogid');
		delE.value = id 
	}
</script>

<script>
	function configDropDownOption(obj,list) {
		//used to rebuild options list with new values
		var el = document.getElementById(obj);
		var selIndex = el.selectedIndex;
		var selectedValue;
		//get current option value selected
		if (selIndex != -1) {
			for(i=el.length-1; i>=0; i--)
			{
			  if(el.options[i].selected)
			  {
				if(el.options[i].value){
					selectedValue = el.options[i].value; //current selection
				}
			  }
			}
		}
			
		//removed current selects in dropdown
		el.length = 0;
				
		//add new values to dropdown
		var i;		
		var temp = list.split("|"); //split list provided by popup_config_options.asp
		//add new options provided by user
		addOptionsItems(obj,"","-- Select --");
		for(i=0; i<temp.length; i++) {
			if (temp[i].length>1) {
				addOptionsItems(obj,temp[i],temp[i]);
			}
		}
		
		//check for existance of originally selected value		
		if(selectedValue){
			var bolMatch = false;
			i = 0;		
			for (i=0; i<el.options.length; i++) {
				if (el.options[i].text == selectedValue) {
					el.selectedIndex = i;
					bolMatch = true; //origianl still exists in list
				}
			}		
			//user removed original value, put back with "Missing:" at end of list
			if (bolMatch == false){
				addOptionsItems(obj,selectedValue,"Missing:"+selectedValue);
				el.selectedIndex = el.options.length-1;
			}
		}
		
	}
</script>

<script>
	function navigateAway(url) {
		window.location = url;
	}		
</script>	

