<%@ Language=VBScript %>
<%
'This page was created by ReCrystallize Pro Web Wizard for Crystal Reports, version 11.7.1
'http://www.recrystallize.com
'

basePath = Request.ServerVariables("PATH_TRANSLATED")
While (Right(basePath, 1) <> "\" And Len(basePath) <> 0)
    iLen = Len(basePath) - 1
    basePath = Left(basePath, iLen)
Wend

baseVirtualPath = Request.ServerVariables("PATH_INFO")
While (Right(baseVirtualPath, 1) <> "/" And Len(baseVirtualPath) <> 0)
    iLen = Len(baseVirtualPath) - 1
    baseVirtualPath = Left(baseVirtualPath, iLen)
Wend

If Not IsObject(session("oApp")) Then
    Set session("oApp") = Server.CreateObject("CrystalRuntime.Application.11")
    If Not IsObject(session("oApp")) Then
        response.write "Error:  Could not instantiate the Crystal Reports automation server.  Please check to see that Crystal Reports or ReCrystallize has been properly installed on the web server PC.  See the ReCrystallize Pro Getting Started guide for more information."
        response.end
    End If
End If   
 

If IsObject(session("oRpt")) Then
    set session("oRpt") = nothing
End If

Set session("oRpt") = session("oApp").OpenReport(basePath & "Issue Register - User.rpt", 1)
If Err.Number <> 0 Then
  Response.Write "Error Occurred creating Report Object: " & Err.Description
  Set Session("oRpt") = Nothing
  Set Session("oApp") = Nothing
  Session.Abandon
  Response.End
End If

session("oRpt").DiscardSavedData

session("oRpt").MorePrintEngineErrorMessages = False
session("oRpt").EnableParameterPrompting = False

 
set crtable = session("oRpt").Database.Tables.Item(1)
crtable.SetLogonInfo cstr("myCM30"), cstr("myCM"), cstr("sa"), cstr("0p3nup4m3W0n")
if not crtable.TestConnectivity then
    response.write "ReCrystallize Warning:  Unable to connect to data source using the following information.<BR><BR>"
    response.write "Server / ODBC data source: myCM30<BR>"
    response.write "Database / Table: myCM<BR>"
    response.write "User Name: " & "sa<BR>"
    response.write "Please verify the database user password in this ASP file.<BR><BR>"
end if
 
set crtable = session("oRpt").Database.Tables.Item(2)
crtable.SetLogonInfo cstr("myCM30"), cstr("myCM"), cstr("sa"), cstr("0p3nup4m3W0n")
if not crtable.TestConnectivity then
    response.write "ReCrystallize Warning:  Unable to connect to data source using the following information.<BR><BR>"
    response.write "Server / ODBC data source: myCM30<BR>"
    response.write "Database / Table: myCM<BR>"
    response.write "User Name: " & "sa<BR>"
    response.write "Please verify the database user password in this ASP file.<BR><BR>"
end if

set session("ParamCollection") = Session("oRpt").Parameterfields 

set Param1 =  session("ParamCollection").Item(1)

tempd = CInt( Request("P1D") )
if tempd = 0 then
   tempd = 1
end if

tempm = CInt( Request("P1M") )
if tempm = 0 then
   tempm = 1
end if

tempy = CInt( Request("P1Y") )

ParamValue1 = DateSerial( CInt( tempy ), CInt( tempm ), CInt( tempd ) ) 
Call Param1.SetCurrentValue (CDate(ParamValue1), 10)


set Param2 =  session("ParamCollection").Item(2)

tempd = CInt( Request("P2D") )
if tempd = 0 then
   tempd = 1
end if

tempm = CInt( Request("P2M") )
if tempm = 0 then
   tempm = 1
end if

tempy = CInt( Request("P2Y") )

ParamValue2 = DateSerial( CInt( tempy ), CInt( tempm ), CInt( tempd ) ) 
Call Param2.SetCurrentValue (CDate(ParamValue2), 10)


set Param3 =  session("ParamCollection").Item(3)
ParamValue3 = Request("P3")
Call Param3.SetCurrentValue (CStr(ParamValue3), 12)


set Param4 =  session("ParamCollection").Item(4)
ParamValue4 = Request("P4")
Call Param4.SetCurrentValue (CDbl(ParamValue4), 7)

  If IsObject (session("oPageEngine")) Then                              
     set session("oPageEngine") = nothing
  End If

set session("oPageEngine") = session("oRpt").PageEngine

%>
<HTML>
<HEAD>
<TITLE>
ReCrystallize Test
</TITLE>
</HEAD>
<BODY BGCOLOR=C6C6C6 LANGUAGE=VBScript ONLOAD="Page_Initialize" ONUNLOAD="CallDestroy()" leftmargin=0 topmargin=0 rightmargin=0 bottommargin=0>
<OBJECT ID="CRViewer"
    CLASSID="CLSID:6F0892F7-0D44-41C3-BF07-7599873FAA04"
    WIDTH=100% HEIGHT=100%
    CODEBASE="/crystalreportviewers115/ActiveXControls/ActiveXViewer.cab#Version=11,5,3,417" VIEWASTEXT>
<PARAM NAME="EnableRefreshButton" VALUE=1>
<PARAM NAME="EnableGroupTree" VALUE=1>
<PARAM NAME="DisplayGroupTree" VALUE=1>
<PARAM NAME="EnableExportButton" VALUE=1>
<PARAM NAME="EnablePrintButton" VALUE=1>
<PARAM NAME="EnableDrillDown" VALUE=1>
<PARAM NAME="EnableAnimationControl" VALUE=1>
<PARAM NAME="EnableZoomControl" VALUE=1>
<PARAM NAME="EnableSearchControl" VALUE=1>
<PARAM NAME="DisplayToolbar" VALUE=1>
<PARAM NAME="EnableProgressControl" VALUE=1>
<PARAM NAME="EnableStopButton" VALUE=1>
<PARAM NAME="EnableCloseButton" VALUE=1>
<PARAM NAME="EnableNavigationControls" VALUE=1>
<PARAM NAME="PromptOnRefresh" VALUE=0>
<PARAM NAME="EnablePopupMenu" VALUE=1>
<PARAM NAME="DisplayBackgroundEdge" VALUE=1>
<PARAM NAME="DisplayBorder" VALUE=1>
<PARAM NAME="DisplayTabs" VALUE=1>
</OBJECT>

<SCRIPT LANGUAGE="JavaScript" type="text/javascript" src="activate.js"></SCRIPT>

<SCRIPT LANGUAGE="VBScript">
<!--
zoomTimer = window.setInterval("setZoom",250)

Sub setZoom()
 if not CRViewer.IsBusy then
     window.clearInterval(zoomTimer)
     CRViewer.Zoom(100)
 end if
End Sub
Sub Page_Initialize
    On Error Resume Next
    Dim webBroker
    Set webBroker = CreateObject("CrystalReports115.WebReportBroker.1")
    if ScriptEngineMajorVersion < 2 then
        window.alert "Internet Explorer 3.02 users running Windows NT 4 should get the latest version of VBScript or install IE 4.01 SP1. IE 3.02 users running Windows 95 need DCOM95 and the latest version of VBScript, or install IE 4.01 SP1. These files are available at Microsoft's web site."
        CRViewer.ReportName = Location.Protocol + "//" + Location.Host + <% response.write( chr(34) & baseVirtualPath & chr(34)) %> + "rptserver.asp"
    else
        Dim webSource
        Set webSource = CreateObject("CrystalReports115.WebReportSource.1")
        webSource.ReportSource = webBroker
        webSource.URL = Location.Protocol + "//" + Location.Host + <% response.write( chr(34) & baseVirtualPath & chr(34)) %> + "rptserver.asp"
        webSource.PromptOnRefresh = False
        CRViewer.ReportSource = webSource
    end if
    CRViewer.ViewReport
End Sub
-->
</SCRIPT>
<script language="javascript">
function CallDestroy()
{
    window.open("Cleanup.asp","Cleanup","top=2000");
}
</script>
<OBJECT ID="ReportSource"
    CLASSID="CLSID:84D35B77-75B4-4ff0-A2DE-6ED1B3EBE036"
    HEIGHT=1% WIDTH=1%
    CODEBASE="/crystalreportviewers115/ActiveXControls/ActiveXViewer.cab#Version=11,5,3,417">
</OBJECT>
<OBJECT ID="ViewHelp"
    CLASSID="CLSID:9FD60A56-0184-4886-88A7-9943FB0DF9EA"
    HEIGHT=1% WIDTH=1%
    CODEBASE="/crystalreportviewers115/ActiveXControls/ActiveXViewer.cab#Version=11,5,3,417">
</OBJECT>
<div>
</div>

</BODY>
</HTML>
