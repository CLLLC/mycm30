<html>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<meta name="GENERATOR" content="ReCrystallize Web Wizard for Crystal Reports 11.7.1">
<title>ReCrystallize Test</title>
</head>

<body bgcolor="#FFFFFF">

<p><font size="5"><strong>ReCrystallize Test</strong></font></p>

<form action="Issue%20Register%20-%20User-dynamic.asp" method="POST" name="parmform">
    <table border="1">
        <tr>
            <td>Enter FromDate:
            </td>
            <td>
<%@ Language=VBScript %>
<select NAME="P1M" SIZE="1">
<%

curmonth = Month( Now )

quote = chr(34)
selstr = ""

for x = 1 to 12
  
  selstr = selstr & "<OPTION VALUE=" & quote 
  
  if x < 10 then
      selstr = selstr & "0" & CStr(x) & quote
  else
      selstr = selstr & CStr(x) & quote
  end if
  
  if x = curmonth then
      selstr = selstr & " SELECTED"
  end if
  
  selstr = selstr & ">"
  
  select case x
  case 1
      selstr = selstr & "January"
  case 2
      selstr = selstr & "February"
  case 3
      selstr = selstr & "March"
  case 4
      selstr = selstr & "April"
  case 5
      selstr = selstr & "May"
  case 6
      selstr = selstr & "June"
  case 7 
      selstr = selstr & "July"
  case 8
      selstr = selstr & "August"
  case 9
      selstr = selstr & "September"
  case 10
      selstr = selstr & "October"
  case 11
      selstr = selstr & "November"
  case 12
      selstr = selstr & "December"
  end select
  
next

response.write Cstr(selstr)
%>
</select>
<select NAME="P1D" SIZE="1">
<%
curday = Day( Now )

quote = chr(34)
selstr = ""

for x = 1 to 31
 
 selstr = selstr & "<OPTION VALUE=" & quote 
 
     selstr = selstr &  CStr(x) & quote
 
 if x = curday then
     selstr = selstr & " SELECTED"
 end if
 
 selstr = selstr & ">" & CStr( x )
 
next

response.write Cstr(selstr)
%>
</select>
<select NAME="P1Y" SIZE="1">
<%
'change the value below to adjust the number of year options before and after the current year
range = 10

curyear = Year( Now )

quote = chr(34)
selstr = ""

for x = (curyear - range) to (curyear + range)
 
 selstr = selstr & "<OPTION VALUE=" & quote 
 
     selstr = selstr &  CStr(x) & quote
 
 if x = curyear then
     selstr = selstr & " SELECTED"
 end if
 
 selstr = selstr & ">" & CStr( x )
 
next

response.write Cstr(selstr)
%>
</select>
            </td>
        </tr>
        <tr>
            <td>Enter ToDate:
            </td>
            <td>
<select NAME="P2M" SIZE="1">
<%

curmonth = Month( Now )

quote = chr(34)
selstr = ""

for x = 1 to 12
  
  selstr = selstr & "<OPTION VALUE=" & quote 
  
  if x < 10 then
      selstr = selstr & "0" & CStr(x) & quote
  else
      selstr = selstr & CStr(x) & quote
  end if
  
  if x = curmonth then
      selstr = selstr & " SELECTED"
  end if
  
  selstr = selstr & ">"
  
  select case x
  case 1
      selstr = selstr & "January"
  case 2
      selstr = selstr & "February"
  case 3
      selstr = selstr & "March"
  case 4
      selstr = selstr & "April"
  case 5
      selstr = selstr & "May"
  case 6
      selstr = selstr & "June"
  case 7 
      selstr = selstr & "July"
  case 8
      selstr = selstr & "August"
  case 9
      selstr = selstr & "September"
  case 10
      selstr = selstr & "October"
  case 11
      selstr = selstr & "November"
  case 12
      selstr = selstr & "December"
  end select
  
next

response.write Cstr(selstr)
%>
</select>
<select NAME="P2D" SIZE="1">
<%
curday = Day( Now )

quote = chr(34)
selstr = ""

for x = 1 to 31
 
 selstr = selstr & "<OPTION VALUE=" & quote 
 
     selstr = selstr &  CStr(x) & quote
 
 if x = curday then
     selstr = selstr & " SELECTED"
 end if
 
 selstr = selstr & ">" & CStr( x )
 
next

response.write Cstr(selstr)
%>
</select>
<select NAME="P2Y" SIZE="1">
<%
'change the value below to adjust the number of year options before and after the current year
range = 10

curyear = Year( Now )

quote = chr(34)
selstr = ""

for x = (curyear - range) to (curyear + range)
 
 selstr = selstr & "<OPTION VALUE=" & quote 
 
     selstr = selstr &  CStr(x) & quote
 
 if x = curyear then
     selstr = selstr & " SELECTED"
 end if
 
 selstr = selstr & ">" & CStr( x )
 
next

response.write Cstr(selstr)
%>
</select>
            </td>
        </tr>
        <tr>
            <td>Enter CustomerID:
            </td>
            <td>
            <input type="text" size="10" maxlength="10" name="P3">
            </td>
        </tr>
        <tr>
            <td>Enter LogID:
            </td>
            <td>
            <input type="text" size="10" maxlength="10" name="P4">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <p align="center">
                    <input type="submit" name="B1" value="View Report">
                </p>
            </td>
        </tr>
    </table>
</form>
</body>
</html>

