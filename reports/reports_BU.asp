<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
Server.ScriptTimeout =25000

const showSearchBox = true
const showDHTMLMenu = true
const helpID = 2
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = request.queryString("pageView")
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'Products
dim action

'Work Fields
dim I
dim item
dim pageSize
dim totalPages
dim showArr
dim sortField
dim sortOrder
dim curPage

dim showCRSID

'Get type of view being requested
dim tableProperty

dim optType
optType = Request.QueryString("type")


'Set Row Colors
dim foreColor
dim rowColor, col1, col2
col1 = "#F0F0F0"
col2 = "#FFFFFF"

%>

<% 
dim pageTitle
if lCase(optType) = "register" then
	pageTitle = "Reports (registers)" 
elseif lCase(optType) = "calculated" then	
	pageTitle = "Reports (calculated)" 
elseif lCase(optType) = "sql" then		
	pageTitle = "Reports (sql)"  	
elseif lCase(optType) = "crystal" then	
	pageTitle = "Reports (crystal)" 	
else
	pageTitle = "Reports"
end if
%>
<!--#include file="../_includes/_INCheaderLite_.asp"-->


<!-- START Page Loader and inform user content being loaded -->
<div id="loaderContainer" style="margin-top:15px; margin-left:15px; border:1px solid #B3B3B3; background-color:#FFFFFF; padding:2px; width:225px;">
	<img src="../_images/myloader.gif" alt="loading..." align="absmiddle" style="margin-left:5px; margin-right:10px; vertical-align:middle;" />Loading content, please wait...
</div>
<%
'push to window right now!
response.flush()
%>
<!-- STOP Page Loader -->


<script>
	//execute Crytal Reports
	function runCrystal(id,type,name) {
		var rid = document.getElementById('reportID');
		var ele = document.getElementById('type');
		var file = document.getElementById('fileName');
		rid.value = id;
		ele.value = type;
		file.value = name;
		document.forms["frm"].submit(); 
	}
</script>


<form name="frm" id="frm" method="post" action="crystal/execute.asp" style="padding:0px; margin:0px;" target="_blank">

<input name="type" id="type" type="hidden" value="" />
<input name="fileName" id="fileName" type="hidden" value="" />             
<input name="reportID" id="reportID" type="hidden" value="" />
<input name="fromDate" id="fromDate" type="hidden" value="<% ="1/1/" & year(date()) %>" />             
<input name="toDate" id="toDate" type="hidden" value="<% ="12/31/" & year(date()) %>" />                


<!-- START Page -->
<table id="masterTable" style="background:#FFF; display:none;" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
        
        <!-- START Left sided area -->
		<td width="190" align="right" valign="top">

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="../profiles/users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided tree menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">                

                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->              
                    <% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>      
	                    <script>
   		                	stExpandSubTree('myCMtree',3,0);
       		            </script>
            		<% else %>
	                    <script>
   		                	stExpandSubTree('myCMtree',2,0);
       		            </script>
                    <% end if %>
                    <!-- STOP side menu include -->
                    
                </td>
              </tr>
              <!-- END Left sided tree menu -->
        
            </table>
        
        </td>
        <!-- STOP Left sided area -->
            
	    <!-- START Main Middle Section -->
		<td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">

			<%
			'used to STOP updating the cookie crumb if page is being recalled from a SAVE
			dim updateCrumb
            if len(session(session("siteID") & "errMsg")) > 0 or len(session(session("siteID") & "okMsg")) > 0 then
				updateCrumb = "false"
            end if
			%>
            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs('<% =updateCrumb %>'));
       	        </script>
            </div>

            <div style="text-align:left; margin-bottom:10px;">            
            	<% 
				if lCase(optType) = "register" then
					response.write("<img src=""../_images/icons/32/table.png"" title="""" width=""32"" height=""32"" align=""absmiddle"" style=""vertical-align:middle;"">")
					response.write("&nbsp;")
					response.write("<span class=""pageTitle"">Reports: Registers</span>")
				elseif lCase(optType) = "calculated" then
	                response.write("<img src=""../_images/icons/32/table_sum.png"" title="""" width=""32"" height=""32"" align=""absmiddle"" style=""vertical-align:middle;"">")
					response.write("&nbsp;")
					response.write("<span class=""pageTitle"">Reports: Calculated</span>")
				elseif lCase(optType) = "sql" then	
	                response.write("<img src=""../_images/icons/32/table_link.png"" title="""" width=""32"" height=""32"" align=""absmiddle"" style=""vertical-align:middle;"">")				
					response.write("&nbsp;")
					response.write("<span class=""pageTitle"">Reports: SQL Queries</span>")
				elseif lCase(optType) = "crystal" then	
	                response.write("<img src=""../_images/icons/32/file_extension_rpt.png"" title="""" width=""32"" height=""32"" align=""absmiddle"" style=""vertical-align:middle;"">")				
					response.write("&nbsp;")
					response.write("<span class=""pageTitle"">Reports: Crystal Reports</span>")
				else
	                response.write("<img src=""../_images/icons/32/chart.png"" title="""" width=""32"" height=""32"" align=""absmiddle"" style=""vertical-align:middle;"">")
					response.write("&nbsp;")
					response.write("<span class=""pageTitle"">Reports</span>")
                end if
                %>                
            </div>                   
                                          
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
                        <legend>Information</legend>
                        <div align="left">
                        	<% 
							dim infoString							
							if lCase(optType) = "register" then 
								infoString = "All register style reports you have configured or set as owner are listed below."
							elseif lCase(optType) = "calculated" then 
								infoString = "All calculated style reports with charts you have configured or set as owner are listed below."							
							elseif lCase(optType) = "sql" then 
								infoString = "All reports using <a href=""http://msdn.microsoft.com/en-us/library/bb264565(SQL.90).aspx"" target=""_blank"">MS SQL</a> are listed below."
							elseif lCase(optType) = "crystal" then 
								infoString = "Reports that have been developed using <a href=""http://www.crystalreports.com/"" target=""_blank"">Business Objects Crystal Reports&reg;</a> are listed below."							
							else
								infoString = "Reports you have configured or set as owner are listed below."
							end if
							%>
                            <img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; vertical-align:middle;"><% =infoString %>
							<% response.write( getInlineHelp("getPageURL", 0) ) %>
                        </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             

                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">                    
                       	<% if addReport="Y" then %>
                            <div class="infoButtons">                        
                                <a href="../datasets/default.asp"><img src="../_images/icons/24/chart_add.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                <a href="../datasets/default.asp">Add Report [F9]</a>
                            </div>                                                                   
                        <% end if %>                                                    
                    </td>
                    <!-- STOP Information buttons -->
                                                                                
                </tr>
            </table>        
            <!-- STOP information box -->
    

    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			'clear system messages
			call clearSessionMessages()
            %>            
    		<!-- STOP message notification box -->


            <!-- START Reports table -->
            <%
			'open reports table...
			if len(optType) > 0 then
				mySQL="SELECT Reports.Name, Reports.Category, Dataset.Name AS Dataset, Dataset.Type, Reports.IssueList, Convert(varchar, LastRunDate, 101) AS [Last Ran], Reports.FileName, ReportID " _
					& "   FROM Reports INNER JOIN Dataset ON (Reports.DatasetID = Dataset.DatasetID) " _
					& "   WHERE (LOGID=" & sLogid & ") AND (Reports.CustomerID = '" & sCustomerID & "') AND (Type ='" & optType & "')" _
					& "   ORDER BY Reports.Name ASC "
			else
				mySQL="SELECT Reports.Name, Reports.Category, Dataset.Name AS Dataset, Dataset.Type, Reports.IssueList, Convert(varchar, LastRunDate, 101) AS [Last Ran], Reports.FileName, ReportID " _
					& "   FROM Reports INNER JOIN Dataset ON (Reports.DatasetID = Dataset.DatasetID) " _
					& "   WHERE (LOGID=" & sLogid & ") AND (Reports.CustomerID = '" & sCustomerID & "') " _
					& "   ORDER BY Reports.Name ASC "			
			end if

			mySQL = mySQL

			'do NOT show calculated or register reports
			if cLng(session(session("siteID") & "adminLoggedOn")) >= 50 then
				mySQL = insertWhereClause(mySQL,"Dataset.Type='Crystal'")
			end if

	        set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
			%>

            <!-- START Reports table -->
			<table id="reportTable" width="100%" style="background-color:#969696;" cellpadding=0 cellspacing=0>
                        
                <thead>
                    <tr>
                        <th align="left" nowrap="nowrap">&nbsp;</th>
                        <th align="left" nowrap="nowrap">Name</th>
                        <th align="left" nowrap="nowrap">Category</th>                                
                        <th align="left" nowrap="nowrap">Dataset</th>
                        <th align="left" nowrap="nowrap">Issue List</th>                                
                        <th align="left" nowrap="nowrap">Last Ran</th>                                                                
                    </tr>
                </thead>
                <tbody>

				<%
                do until rs.eof
					
					response.write("<tr>")

					'ICONS delete/print
					response.Write("<td width=""58"" nowrap=""nowrap"" align=""left"">")								
					if lCase(rs("type")) = "crystal" then
						response.Write("<a href=""#"" onClick=""runCrystal(" & rs("reportid") & ",'PDF','" & Server.HtmlEncode(rs("filename")) & "'); return false;"" ><img src=""../_images/icons/16/file_extension_pdf.png"" title=""View PDF"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; vertical-align:middle; margin-right:5px;"" border=""0"" /></a>")
						response.Write("<a href=""#"" onClick=""runCrystal(" & rs("reportid") & ",'ExcelData','" & Server.HtmlEncode(rs("filename")) & "'); return false;"" ><img src=""../_images/icons/16/file_extension_xls.png"" title=""Download CSV"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle; margin-right:5px;"" border=""0"" /></a>")			
						
						'only allow the deleting of a report if ADMIN
						if cLng(session(session("siteID") & "adminLoggedOn")) <= 10 then						
							response.Write("<a href=""#"" onClick=""confirmPrompt('Confirm Delete','This action can not be undone.<br><br>Are you sure you want to delete this report?','redirect', 'reports_exec.asp?action=del&recID=" & rs("reportid") & "'); return false;"" ><img src=""../_images/icons/16/cancel.png"" title=""Delete Report"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle;"" border=""0"" /></a>")
						else
							response.Write("<img src=""../_images/icons/16/cancel_disabled.png"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle;"" border=""0"" />")									
						end if									
					
					elseif lCase(rs("type")) = "sql" then
						response.Write("<a href=""#"" onClick=""javascript:popup('reports_print.asp?pageview=" & rs("reportid") & "',600,600); return false;"" ><img src=""../_images/icons/16/database_lightning.png"" title=""Execute"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; vertical-align:middle; margin-right:5px;"" border=""0"" /></a>")						
						response.Write("<a href=""reports_export.asp?pageview=" & rs("reportid") & "&action=csv"" ><img src=""../_images/icons/16/file_extension_xls.png"" title=""Download CSV"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle; margin-right:5px;"" border=""0"" /></a>")																
						response.Write("<a href=""#"" onClick=""confirmPrompt('Confirm Delete','This action can not be undone.<br><br>Are you sure you want to delete this report?','redirect', 'reports_exec.asp?action=del&recID=" & rs("reportid") & "'); return false;"" ><img src=""../_images/icons/16/cancel.png"" title=""Delete Report"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle;"" border=""0"" /></a>")					
					else
						response.Write("<a href=""#"" onClick=""javascript:popup('reports_print.asp?pageview=" & rs("reportid") & "',600,600); return false;"" ><img src=""../_images/icons/16/document_inspector.png"" title=""Print Preview"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; vertical-align:middle; margin-right:5px;"" border=""0"" /></a>")						
						response.Write("<a href=""reports_export.asp?pageview=" & rs("reportid") & "&action=csv"" ><img src=""../_images/icons/16/file_extension_xls.png"" title=""Download CSV"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle; margin-right:5px;"" border=""0"" /></a>")																
						
						'delete reports
						if cLng(session(session("siteID") & "adminLoggedOn")) < 50 then
							response.Write("<a href=""#"" onClick=""confirmPrompt('Confirm Delete','This action can not be undone.<br><br>Are you sure you want to delete this report?','redirect', 'reports_exec.asp?action=del&recID=" & rs("reportid") & "'); return false;"" ><img src=""../_images/icons/16/cancel.png"" title=""Delete Report"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle;"" border=""0"" /></a>")					
						'those >= 50 can NOT delete reports
						else
							response.Write("<img src=""../_images/icons/16/cancel_disabled.png"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle;"" border=""0"" />")									
						end if									
						
					end if
					response.Write("</td>")

					'NAME and edit icon link
					if lCase(rs("type")) = "crystal" then
						response.write("<td align=""left"" nowrap=""nowrap"" style=""cursor:pointer;"" onClick=""window.location='crystal/execute.asp?type=edit&recID=" & rs("reportid") & "&name=" & Server.URLEncode(rs("filename")) & "'"">")
						response.write("<img style=""margin-right:5px; vertical-align:middle;"" src=""../_images/icons/16/page_blue_edit.png"" title=""Open Item"" border=""0"" align=""absmiddle"">")
						response.write("<a style=""color:#333333;"">" & rs("name") & "</a>")
						response.write("</td>")
					else
						if cLng(session(session("siteID") & "adminLoggedOn")) < 50 then					
							response.Write("<td align=""left"" style=""cursor:pointer;"" onClick=""window.location='reports_edit.asp?action=edit&recID=" & rs("reportid") & "'"">")
							response.Write("<img style=""margin-right:5px; vertical-align:middle;"" src=""../_images/icons/16/page_blue_edit.png"" title=""Open Item"" border=""0"" align=""absmiddle"">")
							response.write("<a style=""color:#333333;"">" & rs("name") & "</a>")
							response.Write("</td>")
						else
							response.Write("<td align=""left"" style=""cursor:pointer;"" onClick=""javascript:popup('reports_print.asp?pageview=" & rs("reportid") & "',600,600);"">")
							response.Write("<img style=""margin-right:5px; vertical-align:middle;"" src=""../_images/icons/16/page_blue_edit.png"" title=""Open Item"" border=""0"" align=""absmiddle"">")
							response.write("<a style=""color:#333333;"">" & rs("name") & "</a>")
							response.Write("</td>")							
						end if							
					end if
											
					response.Write("<td align=""left"">" & rs("category") & "</td>")
					response.Write("<td align=""left"">" & rs("dataset") & "</td>")
					response.Write("<td align=""left"">" & rs("issuelist") & "</td>")
					response.Write("<td align=""left"">" & rs("last ran") & "</td>")						

					'CLOSE current row						
					response.write("</tr>")
					
					rs.movenext
				loop
				%>					

				</tbody>
			</table>
       	    <!-- STOP Reports table -->

			<%
			tableProperty = "sort: true, sort_config:{sort_types:['String','String','String','String','String','date']}, filters_row_index: 1, " _
						  & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
						  & "status_bar: true, col_0: """", col_1: ""input"", col_2: ""select"", col_3: ""select"", col_4: ""select"", col_5: ""none"", " _
						  & "col_width:[null,""40%"",null,null,null,null], paging: true, paging_length: 10, " _
						  & "enable_non_empty_option: true, or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, " _
						  & "highlight_keywords: true, " _
						  & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
						  & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"			
			%>
            <!-- STOP Reports table -->

            <!-- Fire table build -->
			<script language="javascript" type="text/javascript">
				//<![CDATA[
				var tableProp = {<% =tableProperty %>};
				//initiate table setup
				var tf1 = setFilterGrid("reportTable",tableProp);
				//]]>
			</script>

            <!-- Shadow table -->
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td height="5px" style="background:#D0D0D0;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
              </tr>
            </table>
        
        </td>
    	<!-- STOP Main Middle Section -->

    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
  	</tr>
</table>
<!-- STOP Page -->

</form>

<script>
	//REFERENCE: <div> "loaderContainer" at top of page
	document.getElementById('loaderContainer').style.display = 'none';	//hide page loader
	document.getElementById('masterTable').style.display = '';			//display table holding page content
</script>

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<script>
	function navigateAway(url) {
		window.location = url;
	}		
</script>	
