<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

dim pageSize

'Work Fields
dim I
dim item
dim count
dim countString

dim action
action = Request.QueryString("action")

dim customerID
customerID = Request.QueryString("cid")

dim pageView
pageView = Request.QueryString("pageview")

dim queryView
queryView = Request.QueryString("view")


'make sure right action was passed
if action <> "csv" and action <> "xls" and action <> "xml" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Could not load report.")	

'all good
else

	'-------------------------------------------------------
	'report download, not profile
	'-------------------------------------------------------	
	if lCase(pageView) <> "profile" and lCase(pageView) <> "issues" then

		dim rptLogid, jsonCID
		dim listID, listSQL, listName, listURL, listDataset, listType, listTable, listFilterFields, listFilterXML, listProgramAdmin
		dim listSortOrder, listSortField
		dim chartXAxis, chartYAxis

		'Update Run Date
		mySQL = "UPDATE Reports SET LastRunDate='" & Now() & "' " _
			  & "WHERE ReportID = " & pageView
		set rs = openRSexecute(mySQL)

		'find requested list					
		mySQL="SELECT Reports.ReportID, Reports.LogID, Reports.Name, Reports.QueryFields, Reports.QueryFilter, Reports.QuerySortOrder, Reports.QuerySortField, Reports.SQL, Dataset.Name AS Dataset, " _
			  & "       Dataset.Type, Dataset.[Table], Reports.ListURLField, Reports.chartXAxis, Reports.chartYAxis, Logins.ProgramAdmin " _  
			  & "	FROM Reports INNER JOIN " _
			  & "         	Dataset ON Reports.DatasetID = Dataset.DatasetID INNER JOIN " _
			  & "          	Logins ON Reports.LogID = Logins.LOGID " _
			  & "WHERE ReportID = " & pageView & " "
						  
		'non-CCI staff and non-ProgramAdmins can ONLY view their own reports
		if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and ProgramAdmin<>"Y" then		  
			mySQL = insertWhereClause(mySQL,"Reports.CustomerID = '" & sCustomerID & "' and Reports.LOGID=" & sLogid & "")
		
		'view their's and other's reports in same company
		elseif cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and ProgramAdmin="Y" then		  
			mySQL = insertWhereClause(mySQL,"Reports.CustomerID IN (" & profileIDs & ") ")
	
		end if	  
			  
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	
		if not rs.eof then
			listID = rs("reportid")
			rptLogid = rs("logid")								
			listDataset = rs("dataset")
			listType = rs("type")
			listTable = rs("table")
			listFilterFields = rs("queryfields")
			listSortOrder = rs("querysortorder")
			listSortField = rs("querysortfield")
			listFilterXML = rs("queryfilter")
			listSQL = rs("sql")
			chartXAxis = rs("chartXAxis")
			chartYAxis = rs("chartYAxis")
			listProgramAdmin = rs("ProgramAdmin")
		
			listName = rs("name")
			listURL = rs("listURLfield")
			if lCase(listType) <> "sql" then
				listURL = replace(listURL,"[","")										
				listURL = replace(listURL,"]","")					
			end if
				
			'construct SQL statement
			if lCase(listType) <> "sql" then
				listSQL = BuildQuery(listDataset, listFilterFields, listSortField, listSortOrder, listFilterXML, listTable, listType, chartXAxis, chartYAxis)
	
				'-------------------------------------------------------------
				'for customers...ensures user can only view specific customers 
				'and issue types pulled from vwLogins_IssueType							
				'-------------------------------------------------------------
				'DBAdmin and CCI Admins
				if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
					'do nothing...			
				'CCI RS/RA
				elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
					listSQL = insertWhereClause(listSQL,"IssueTypeID<>3")				
				'everyone else...
				else
					'double single quotes needed for crosstab stored procedure
					if lCase(listType) = "calculated" and len(chartYAxis) > 0 then
						listSQL = insertWhereClause(listSQL,"CustomerID IN (" & replace(profileIDs,"'","''") & ") and LogIDView=" & sLogid & "")
					'does not use stored procedure so double single quotes NOT needed
					else
						listSQL = insertWhereClause(listSQL,"CustomerID IN (" & profileIDs & ") and LogIDView=" & sLogid & "")
					end if
				end if
				'-------------------------------------------------------------
								
				'should be pointing to a User specific SQL view, not for CCI staff
				if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then			
					if programAdmin = "N" then
						listSQL = insertWhereClause(listSQL,"LOGID=" & sLogid)
					end if
				end if

				'-------------------------------------------------------------
				'user running someone elses report
				'-------------------------------------------------------------
				if cLng(sLogid) <> cLng(rptLogid) then
					'limit to customers user is able to view
					mySQL = "SELECT CustomerID " _
						  & "	FROM vwLogins_IssueType " _
						  & "	WHERE LOGID = " & rptLogid & " " _
						  & "	GROUP BY CustomerID "			
					set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)			  
					do while not rs.eof
						jsonCID = jsonCID & "'" & rs("CustomerID") & "',"
						rs.movenext
					loop
					jsonCID = left(jsonCID,len(jsonCID)-1)
					'double single quotes needed for crosstab stored procedure
					if lCase(listType) = "calculated" and len(chartYAxis) > 0 then
						listSQL = insertWhereClause(listSQL,"CustomerID IN (" & replace(jsonCID,"'","''") & ") and LogIDView=" & rptLogid & "")
					'does not use stored procedure so double single quotes NOT needed
					else
						listSQL = insertWhereClause(listSQL,"CustomerID IN (" & jsonCID & ") and LogIDView=" & rptLogid & "")
					end if									
					'limit to specific user if NOT admin
					if listProgramAdmin = "Y" then
						'do nothing...
					else
						listSQL = insertWhereClause(listSQL,"LOGID=" & rptLogid)				
					end if					
				end if

				
			'prepare and correct replacement tags
			elseif lCase(listType) = "sql" then
				listSQL = replace(listSQL,"#LOGID#",sLogid)
				listSQL = replace(listSQL,"#CUSID#",sCustomerID)
	
			end if
		
		end if			
		call closeRS(rs)

	'-------------------------------------------------------
	'called from MAIN ISSUES page
	'-------------------------------------------------------
	elseif lCase(pageView) = "issues" then
		
		dim mySQLCount
		dim rowCount
		dim tempWHERE
		dim tempDelimiter
		dim tempValue
		dim showSEARCH, showField
		dim arrSearch, searchColumns, searchX
		
		showSEARCH = Request.QueryString("search")
	
		'used later to perform search...must be set before issuesColumns is modified
		searchColumns = issuesColumns
	
		'make sure something is here
		if len(issuesColumns) <= 0 then
			issuesColumns = "[Issue #], [Date], [Severity] "
		end if
	
		'build custom SQL statement with user defined XML filter, used by the user to limit reports returned
		mySQL = BuildQuery("Issues", issuesColumns, "", "", userWhereClause, userIssuesView, "Register","","")		
	
		'-----------------------------------------------------------
		'security setting for customers and cci staff
		'-----------------------------------------------------------	
		'ensures user can only view specific customers and issue types 
		'pulled from vwLogins_IssueType
		if inStr(lCase(mySQL),"where") then
			'DBAdmin and CCI Admins
			if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
				'do nothing...			
			'CCI RS/RA
			elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
				mySQL = mySQL & " AND IssueTypeID<>3 "
			'everyone else...
			else
				mySQL = mySQL & " AND " & userIssuesView & ".LogIDView=" & sLogid & " "
			end if
		else
			'DBAdmin and CCI Admins	
			if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
				mySQL = mySQL & " WHERE 1=1 "
			'CCI RS/RA			
			elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
				mySQL = mySQL & " WHERE IssueTypeID<>3 "
			'everyone else...
			else
				mySQL = mySQL & " WHERE " & userIssuesView & ".LogIDView=" & sLogid & " "
			end if
		end if
		
		'---------------------------------------------------
		'this statement MUST be after search box query build 
		'or the search box query will be added twice (2)!....
		'might need to add something about GroupAdmins here.
		' --> THIS MIGHT BE WHERE YOU CAN ADD GROUP ADMINS
		' --> AND THEY DON'T HAVE TO BE ASSIGED TO ISSUE
		'---------------------------------------------------		
		'apply for customers and NOT cci staff with security less than 10
		if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then
			if programAdmin = "N" then
				mySQL = mySQL & " AND ( (EXISTS (SELECT CRS_Logins.CRSID " _
							  & "        FROM CRS_Logins " _
							  & "	     WHERE " & userIssuesView & ".[Issue #]=CRS_Logins.CRSID AND " & userIssuesView & ".CustomerID=CRS_Logins.CustomerID AND CRS_Logins.LOGID=" & sLogid & ")) " _
							  & "	     OR (" & userIssuesView & ".LOGID = " & sLogid & ") " _
							  & "	  ) "
			end if
		end if
		
		'final sql to export
		listSQL = mySQL
			
		'get total record count for page and record counts ONLY pull out FROM statement from mySQL
		mySQLCount = "SELECT Count([Issue #]) AS ReturnCount " & mid(listSQL,inStr(1,listSQL,"FROM"),len(listSQL))
		set rs = openRSopen(mySQLCount,0,adOpenStatic,adLockReadOnly,adCmdText,0)		
		rowCount = rs("ReturnCount")
		if rowCount > 2000 then
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Over 2,000 rows where returned. Please limit records and try again.")	
			response.end()
		end if
			
	'-------------------------------------------------------	
	'profile download -- LOCATION
	'-------------------------------------------------------	
	elseif lCase(pageView) = "profile" then

		'validate security access
		if loadUser(customerID) = false then
			response.redirect ("../default.asp")
		end if
	
		listType = "register"
		listSQL = "SELECT * FROM " & queryView & " Where CustomerID = '" & customerID & "' "
		
	end if
				
end if


'*****************************************************************
'EXPORT CSV File
'*****************************************************************
if (action = "csv" or action = "xls") and listSQL <> "" then
		
dim x, y
dim fldValue
	
	'crosstab sql
	if lCase(listType) = "calculated" and len(chartYAxis) > 0 then
		set rs = openRSexecute(" EXEC sp_Crosstab '" & listSQL & "', Null, Null, '" & chartXAxis & "', 'CRSID', 'COUNT' ")
	'normal sql/export
	else					
		set rs = openRSexecute(listSQL)	
	end if

	' Save Output as a CSV file 
	response.ContentType = "text/csv"
	response.AddHeader "Content-Disposition", "filename=export.csv;"

	'Field Headers...
	for x = 0 to rs.fields.count-1 
		if lCase(rs.fields(x).name)="customerid" _
		or lCase(rs.fields(x).name)="locationid" _
		or lCase(rs.fields(x).name)="categoryid" _			
		or lCase(rs.fields(x).name)="logid" _			
		or lCase(rs.fields(x).name)="viewedby" _			
		or lCase(rs.fields(x).name)="hierarchy_search_1" _									
		or lCase(rs.fields(x).name)="hierarchy_search_2" _
		or lCase(rs.fields(x).name)="hierarchy_search_3" _																		
		or lCase(rs.fields(x).name)="hierarchy_search_4" _									
		or lCase(rs.fields(x).name)="hierarchy_search_5" then
			'don't export these
		else
			response.write """" & rs.fields(x).name & ""","
		end if
	next 
	response.write vbCrLf
		
	'Field detail/data
	while not rs.EOF 
		for y = 0 to rs.Fields.Count-1 
			if lCase(rs.fields(y).name)="customerid" _
			or lCase(rs.fields(y).name)="locationid" _
			or lCase(rs.fields(y).name)="categoryid" _			
			or lCase(rs.fields(y).name)="logid" _			
			or lCase(rs.fields(y).name)="viewedby" _			
			or lCase(rs.fields(y).name)="hierarchy_search_1" _									
			or lCase(rs.fields(y).name)="hierarchy_search_2" _
			or lCase(rs.fields(y).name)="hierarchy_search_3" _																		
			or lCase(rs.fields(y).name)="hierarchy_search_4" _									
			or lCase(rs.fields(y).name)="hierarchy_search_5" then
				'don't export these
			else
				fldValue = rs.fields(y).value
				'check for Text/Memo field
				if rs.fields(y).type = adLongVarWChar then
					'strip out html characters
					if action = "xls" then					
						fldValue = stripHTMLlimited(fldValue)					
					else
						fldValue = stripHTML(fldValue)					
					end if
					'strip out double quotes
					fldValue = replace(fldValue,"""","'")				
				end if												
				response.write """" & fldValue & ""","
			end if
		next 
	 	response.write vbCrLf 
	 	rs.movenext 
	wend 

	call closeRS(rs)
	
end if

	
'*****************************************************************
'EXPORT XML File
'*****************************************************************
if action = "xml" and listSQL <> "" then

Dim xmlDoc 

	'Very Important : Set the ContentType property of the Response object to text/xml.
	response.contentType = "text/xml"

	'Open recordset
	set rs = openRSopen(listSQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	'Persist the Recorset in XML format to the ASP Response object. 
	'The constant value for adPersistXML is 1.

	rs.Save Response, 1

	call closeRS(rs)

end if

'close database connection
call closeDB()

%>

