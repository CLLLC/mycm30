<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_includes/_INCchartJS_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%	
'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

dim pageSize

'Work Fields
dim I
dim item
dim count
dim countString

dim jsonCID

dim pageView
pageView = Request.QueryString("pageView")
if pageView = "" or len(pageView) = 0 then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Could not load report.")	
end if

'where was the call made from
dim viewSource
viewSource = Request.QueryString("source")

'if PageView is called get list settings, name and sql statement
dim listID, listSQL, listName, listURL, listDataset, listType, listTable, listFilterFields, listFilterXML, listProgramAdmin
dim listSortOrder, listSortField
dim chartXAxis, chartYAxis

dim rptLogid

'was a page view provided?
if pageView <> 0 then

	'Update Run Date
	mySQL = "UPDATE Reports SET LastRunDate='" & Now() & "' " _
		  & "WHERE ReportID = " & pageView
	set rs = openRSexecute(mySQL)

	'find requested list					
	mySQL="SELECT Reports.ReportID, Reports.LogID, Reports.Name, Reports.QueryFields, Reports.QueryFilter, Reports.QuerySortOrder, Reports.QuerySortField, Reports.SQL, Dataset.Name AS Dataset, " _
		  & "       Dataset.Type, Dataset.[Table], Reports.ListURLField, Reports.chartXAxis, Reports.chartYAxis, Logins.ProgramAdmin " _  
		  & "	FROM Reports INNER JOIN " _
          & "         	Dataset ON Reports.DatasetID = Dataset.DatasetID INNER JOIN " _
          & "          	Logins ON Reports.LogID = Logins.LOGID " _
		  & "WHERE ReportID = " & pageView & " "
				  
	'non-CCI staff and non-ProgramAdmins can ONLY view their own reports
	if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and ProgramAdmin <> "Y" then		  
		mySQL = insertWhereClause(mySQL,"Reports.CustomerID = '" & sCustomerID & "' and Reports.LOGID=" & sLogid & "")
	
	'view their's and other's reports in same company
	elseif cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and ProgramAdmin = "Y" then		  
		mySQL = insertWhereClause(mySQL,"Reports.CustomerID IN (" & profileIDs & ") ")

	end if	  
		
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
				
	if not rs.eof then
		listID 				= rs("reportid")					
		rptLogid 			= rs("logid")					
		listDataset 		= rs("dataset")
		listType 			= rs("type")					
		listTable 			= rs("table")
		listFilterFields 	= rs("queryfields")
		listSortOrder 		= rs("querysortorder")
		listSortField 		= rs("querysortfield")				
		listFilterXML 		= rs("queryfilter")
		listSQL 			= rs("sql")
		chartXAxis 			= rs("chartXAxis")
		chartYAxis 			= rs("chartYAxis")
		listName 			= rs("name")		
		listURL 			= rs("listURLfield")
		listProgramAdmin	= rs("ProgramAdmin")
		
		if lCase(listType) <> "sql" then
			listURL = replace(listURL,"[","")										
			listURL = replace(listURL,"]","")					
		end if

		'construct SQL statement
		if lCase(listType) <> "sql" then
			listSQL = BuildQuery(listDataset, listFilterFields, listSortField, listSortOrder, listFilterXML, listTable, listType, chartXAxis, chartYAxis)			
			
			'-------------------------------------------------------------
			'for customers...ensures user can only view specific customers 
			'and issue types pulled from vwLogins_IssueType							
			'-------------------------------------------------------------
			'DBAdmin and CCI Admins
			if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then		
				'do nothing...								
			'CCI RS/RA
			elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
				listSQL = insertWhereClause(listSQL,"IssueTypeID<>3")								
			'everyone else...
			else
				'double single quotes needed for crosstab stored procedure
				if lCase(listType) = "calculated" and len(chartYAxis) > 0 then
					listSQL = insertWhereClause(listSQL,"CustomerID IN (" & replace(profileIDs,"'","''") & ") and LogIDView=" & sLogid & "")
				'does not use stored procedure so double single quotes NOT needed
				else
					listSQL = insertWhereClause(listSQL,"CustomerID IN (" & profileIDs & ") and LogIDView=" & sLogid & "")
				end if
				
			end if
			'-------------------------------------------------------------
							
			'should be pointing to a User specific SQL view, not for CCI staff
			if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then
				if programAdmin = "N" then
					listSQL = insertWhereClause(listSQL,"LOGID=" & sLogid)
				end if				
			end if


			'-------------------------------------------------------------
			'user running someone elses report
			'-------------------------------------------------------------
			if cLng(sLogid) <> cLng(rptLogid) then
				'limit to customers user is able to view
				mySQL = "SELECT CustomerID " _
					  & "	FROM vwLogins_IssueType " _
					  & "	WHERE LOGID = " & rptLogid & " " _
					  & "	GROUP BY CustomerID "			
				set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)			  
				do while not rs.eof
					jsonCID = jsonCID & "'" & rs("CustomerID") & "',"
					rs.movenext
				loop
				jsonCID = left(jsonCID,len(jsonCID)-1)
				'double single quotes needed for crosstab stored procedure
				if lCase(listType) = "calculated" and len(chartYAxis) > 0 then
					listSQL = insertWhereClause(listSQL,"CustomerID IN (" & replace(jsonCID,"'","''") & ") and LogIDView=" & rptLogid & "")
				'does not use stored procedure so double single quotes NOT needed
				else
					listSQL = insertWhereClause(listSQL,"CustomerID IN (" & jsonCID & ") and LogIDView=" & rptLogid & "")
				end if								
				'limit to specific user if NOT admin
				if listProgramAdmin = "Y" then
					'do nothing...
				else
					listSQL = insertWhereClause(listSQL,"LOGID=" & rptLogid)				
				end if				
			end if

												
		'prepare and correct replacement tags
		elseif lCase(listType) = "sql" then
			listSQL = replace(listSQL,"#LOGID#",sLogid)
			listSQL = replace(listSQL,"#CUSID#",sCustomerID)
						
		end if
	
	'report not found
	else	
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Could not load report.")																	
	end if			
		
	call closeRS(rs)

'-------------------------------------------------------
'called from MAIN ISSUES page
'-------------------------------------------------------									
elseif pageView = 0  then								

	dim mySQLCount
	dim rowCount
	dim tempWHERE
	dim tempDelimiter
	dim tempValue
	dim arrSearch, searchColumns, searchX

	listType = "Issues"
	
	'used later to perform search...must be set before issuesColumns is modified
	searchColumns = issuesColumns
	
	'make sure something is here
	if len(issuesColumns) <= 0 then
		issuesColumns = "[Issue #], [Date], [Severity] "
	end if
	
	'build custom SQL statement with user defined XML filter, used by the user to limit reports returned
	mySQL = BuildQuery("Issues", issuesColumns, "", "", userWhereClause, userIssuesView, "Register","","")		
	
	'-----------------------------------------------------------
	'security setting for customers and cci staff
	'-----------------------------------------------------------	
	'ensures user can only view specific customers and issue types 
	'pulled from vwLogins_IssueType
	if inStr(lCase(mySQL),"where") then
		'DBAdmin and CCI Admins
		if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
			'do nothing...			
		'CCI RS/RA
		elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
			mySQL = mySQL & " AND IssueTypeID<>3 "
		'everyone else...
		else
			mySQL = mySQL & " AND " & userIssuesView & ".LogIDView=" & sLogid & " "
		end if
	else
		'DBAdmin and CCI Admins	
		if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
			mySQL = mySQL & " WHERE 1=1 "
		'CCI RS/RA			
		elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
			mySQL = mySQL & " WHERE IssueTypeID<>3 "
		'everyone else...
		else
			mySQL = mySQL & " WHERE " & userIssuesView & ".LogIDView=" & sLogid & " "
		end if
	end if
		
	'---------------------------------------------------
	'this statement MUST be after search box query build 
	'or the search box query will be added twice (2)!....
	'might need to add something about GroupAdmins here.
	' --> THIS MIGHT BE WHERE YOU CAN ADD GROUP ADMINS
	' --> AND THEY DON'T HAVE TO BE ASSIGED TO ISSUE
	'---------------------------------------------------		
	'apply for customers and NOT cci staff with security less than 10
	if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then
		if programAdmin = "N" then
			mySQL = mySQL & " AND ( (EXISTS (SELECT CRS_Logins.CRSID " _
						  & "        FROM CRS_Logins " _
						  & "	     WHERE " & userIssuesView & ".[Issue #]=CRS_Logins.CRSID AND " & userIssuesView & ".CustomerID=CRS_Logins.CustomerID AND CRS_Logins.LOGID=" & sLogid & ")) " _
						  & "	     OR (" & userIssuesView & ".LOGID = " & sLogid & ") " _
						  & "	  ) "
		end if
	end if
		
	'final sql to export
	listSQL = mySQL
			
	'get total record count for page and record counts ONLY pull out FROM statement from mySQL
	mySQLCount = "SELECT Count([Issue #]) AS ReturnCount " & mid(listSQL,inStr(1,listSQL,"FROM"),len(listSQL))
	set rs = openRSopen(mySQLCount,0,adOpenStatic,adLockReadOnly,adCmdText,0)		
	rowCount = rs("ReturnCount")
	if rowCount > 2000 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Over 2,000 rows where returned. Please limit records and try again.")																		
	end if
				
end if

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Report: <% =listName %></title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- HTML Table Filter Generator http://tablefilter.free.fr/ -->
    <link rel="stylesheet" type="text/css" href="../_tablefilter/filtergrid.css"/>
	<script type="text/javascript" src="../_tablefilter/tablefilter_all.js"></script>
	<script type="text/javascript" src="../_tablefilter/sortabletable.js"></script>
    <script type="text/javascript" src="../_tablefilter/tfAdapter.sortabletable.js"></script>        

	<!--JQuery required components -->
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>

</head>

<body style="padding:20px;">

    <!-- START Page Loader and inform user content being loaded -->
    <div id="loaderContainer" style="margin-top:15px; margin-left:15px; border:1px solid #B3B3B3; background-color:#FFFFFF; padding:2px; width:225px;">
        <img src="../_images/myloader.gif" alt="loading..." align="absmiddle" style="margin-left:5px; margin-right:10px; vertical-align:middle;" />Loading content, please wait...
    </div>
    <%
    'push to window right now!
    response.flush()
    %>
    <!-- STOP Page Loader -->

	<!-- START Master Div - used to hide while loading -->
	<div id="masterDiv" >
    
        <div style="float:left; text-align:left; margin-bottom:10px;">
            <span class="popupTitle">Report: <% =listName %></span>
            <%
            if session(session("siteID") & "adminLoggedOn")<2 then
                response.write("&nbsp;&nbsp;[<a href=""#"" onclick=""toggleVisibility('table_SQL'); return false;"">Toggle SQL</a>]")
            end if
            %>                
        </div>                   
    
        <!-- START Buttons -->
        <div style="float:right; text-align:left; margin-bottom:10px;">
            <a class="myCMbutton" href="#" accesskey="P" onClick="this.blur(); window.print(); return false;"><span class="print" style="padding-right:10px;"><u>P</u>rint</span></a>
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); javascript:window.close(); return false;"><span class="cancel"><u>C</u>lose</span></a>
        </div>
        <!-- STOP Buttons -->
    
    
        <table id="table_SQL" width="100%" border="0" cellspacing="0" cellpadding="0" style="display:none;">
          <tr>
            <td style="padding-right:7px;">
                <textarea name="optSQLValue" style="width:100%; height:50px; margin-bottom:10px;" ><% =server.HTMLEncode(listSQL & "") %></textarea>            
            </td>
          </tr>
        </table>
        
        
        <!-- START Print table -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="clear:both;">    
            <tr>
                <td>                
                    <%													
	               	if lCase(listType) = "calculated" then 
						if viewSource = "reports" then
							call buildChart(listID,"print","png",listSQL,"")
						else
							dim chartCookie
							chartCookie = request.cookies("myCM3-Charts")
							if chartCookie = "checked" then
								call buildChart(listID,"print","png",listSQL,"")
							end if						
						end if
                    end if
					
                    'even though some are NOT sql statement, use this flag to prevent links back to main issue list
                    listURL = "Print" 	'<-- so links back to issues.asp are turned off
    
                    dim tableProperty
                    'set _tablefilter/default.asp settings.
                    tableProperty = "sort: true, filters_row_index: 1, grid: false, " _
                                  & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: false, " _
                                  & "status_bar: false, paging: false, loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
                                  & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"
    
                    'do NOT let user run a DELETE statement
                    if lCase(listType) = "sql" and inStr(lCase(listSQL),"delete") > 0 then
                        Response.Write ("<div class=""required"" style=""padding-top:40px; padding-left:40px;""><strong>This SQL statement type cannot be used. Only a SELECT statement can be used.</strong></div>")			
                    else
                                    
                        'make call to build grid found in ../_tablefilter/default.asp
                        '''call buildGrid(listSQL,"",listURL,"../_images/icons/16/page_blue_go.png",tableProperty)
                        
                        'crosstab grid
                        if lCase(listType) = "calculated" then 			
                            'YAxis provided...build crosstab
                            if len(chartYAxis) > 0 then
                                call buildCrosstab("issueTableID",listSQL,"",listURL,"",tableProperty,chartXAxis) 
                            'NO YAxis...build regular table
                            else
                                call buildGrid("issueTableID",listSQL,"",listURL,"",tableProperty) 
                            end if
                            
                        'register grid
                        elseif lCase(listType) = "register" then 														
                            call buildGrid("issueTableID",listSQL,"",listURL,"",tableProperty) 

                        'register grid
                        elseif lCase(listType) = "issues" then 														
                            call buildGrid("issueTableID",listSQL,"",listURL,"",tableProperty) 
                        
                        'sql command
                        else		
							'gridSQL			
							if inStr(lCase(listSQL),"update") > 0 or inStr(lCase(listSQL),"insert") > 0 then
	                            set rs = openRSopen(listSQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	                            response.write ("<div class=""required"" style=""padding-top:40px; padding-left:40px;""><strong>SQL COMMAND EXECUTED on " & Date() & " at " & Time() & "</strong></div>")								
							else
								call buildGrid("issueTableID",listSQL,"",listURL,"",tableProperty) 
							end if
                        
                        end if
                        
                    end if				
                    %>
                </td>
            </tr>        
                
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
                        <tr>
                            <td width="50%" align="left">Printed: <% =Date() %></td>
                            <td width="50%" align="right">Copyright &copy; <% =Year(Date()) %>, Compliance Concepts, Inc.</td>
                        </tr>
                    </table>
                </td>
            </tr>
            
        </table>
        <!-- STOP Print table -->
    
	</div>
	<!-- STOP Master Div -->
              
<%
call closeRS(rs)
%>
    
</body>
</html>

<script>
	//REFERENCE: <div> "loaderContainer" at top of page
	document.getElementById('loaderContainer').style.display = 'none';	//hide page loader
	document.getElementById('masterDiv').style.display = '';			//display table holding page content
</script>
