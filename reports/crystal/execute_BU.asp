<%@ Language=VBScript %>

<!-- MUST BE HERE for directory path -->
<!--#include file="../../_config/config.asp"-->

<%
'This page was created by ReCrystallize Pro Web Wizard for Crystal Reports, version 11.7.1
'http://www.recrystallize.com
'

''' USE FOR TESTING
''' response.write "Error: " & Directory
'''	response.end

dim viewer
viewer = Request.Querystring("type")
if len(viewer) <= 0 then
	viewer = Request.Form("type")
end if

dim followUp
includeFollowUp = Request.Querystring("includeIssue")
if len(includeFollowUp) <= 0 then
	includeFollowUp = Request.Form("includeIssue")
end if

dim includeSection
includeSection = Request.Querystring("list_checkBox")
if len(includeSection) <= 0 then
	includeSection = Request.Form("list_checkBox")
end if

dim customerID
customerID = Request.Querystring("cid")
if len(customerID) <= 0 then
	customerID = Request.Form("customerID")
end if

dim reportID
reportID = Request.Querystring("recid")
if len(reportID) <= 0 then
	reportID = Request.Form("reportID")
end if

'used to redirect user back to tabs they last were on
dim topTab
topTab = trim(Request.QueryString("top"))

dim crystalFile
crystalFile = Request.Querystring("name")
if len(crystalFile) <= 0 then
	crystalFile = Request.Form("fileName")
end if

'DO ERROR CHECKING...

dim fromDate, toDate, rptProfiles, rptStatus, idIssue, rptIssueType, rptLocation_Level_1, rptLocation_Level_2, rptLocation_Level_3

idIssue = Request.Querystring("recID")
if len(idIssue) <= 0 then
	idIssue = Request.Form("idIssue")
end if

rptTitle = Request.Querystring("reporttitle")
if len(rptTitle) <= 0 then
	rptTitle = Request.Form("reporttitle")
end if

fromDate = Request.Querystring("fromDate")
if len(fromDate) <= 0 then
	fromDate = Request.Form("fromDate")
end if

toDate = Request.Querystring("toDate")
if len(toDate) <= 0 then
	toDate = Request.Form("toDate")
end if

rptProfiles = Request.Querystring("profiles")
if len(rptProfiles) <= 0 then
	rptProfiles = Request.Form("profiles")
end if

rptStatus = Request.Querystring("status")
if len(rptStatus) <= 0 then
	rptStatus = Request.Form("status")
end if

rptIssueType = Request.Querystring("issuetype")
if len(rptIssueType) <= 0 then
	rptIssueType = Request.Form("issuetype")
end if

rptLocation_Name = Request.Querystring("location_name")
if len(rptLocation_Name) <= 0 then
	rptLocation_Name = Request.Form("location_name")
end if

rptLocation_Level_1 = Request.Querystring("location_level_1")
if len(rptLocation_Level_1) <= 0 then
	rptLocation_Level_1 = Request.Form("location_level_1")
end if

rptLocation_Level_2 = Request.Querystring("location_level_2")
if len(rptLocation_Level_2) <= 0 then
	rptLocation_Level_2 = Request.Form("location_level_2")
end if

rptLocation_Level_3 = Request.Querystring("location_level_3")
if len(rptLocation_Level_3) <= 0 then
	rptLocation_Level_3 = Request.Form("location_level_3")
end if

rptCategory = Request.Querystring("category")
if len(rptCategory) <= 0 then
	rptCategory = Request.Form("category")
end if

rptIndustry = Request.Querystring("industry")
if len(rptIndustry) <= 0 then
	rptIndustry = Request.Form("industry")
end if

'*************************************
'Set directory and find all files		
dim Directory
'single issue requested, use system reports
if inStr(1,lCase(viewer),"issue") then
	Directory = dataDir & "\crystal\"
'use customer's custom reports
else
	if viewer = "edit" and len(customerID) > 0 then
		Directory = dataDir & "\profiles\" & customerID & "\crystal\"		
	else	
		Directory = dataDir & "\profiles\" & session(session("siteID") & "customerid") & "\crystal\"	
	end if
end if
'*************************************


basePath = Request.ServerVariables("PATH_TRANSLATED")
While (Right(basePath, 1) <> "\" And Len(basePath) <> 0)
    iLen = Len(basePath) - 1
    basePath = Left(basePath, iLen)
Wend

baseVirtualPath = Request.ServerVariables("PATH_INFO")
While (Right(baseVirtualPath, 1) <> "/" And Len(baseVirtualPath) <> 0)
    iLen = Len(baseVirtualPath) - 1
    baseVirtualPath = Left(baseVirtualPath, iLen)
Wend

If Not IsObject(session("oApp")) Then
    Set session("oApp") = Server.CreateObject("CrystalRuntime.Application.11")
    If Not IsObject(session("oApp")) Then
        response.write "Error:  Could not instantiate the Crystal Reports automation server.  Please check to see that Crystal Reports or ReCrystallize has been properly installed on the web server PC.  See the ReCrystallize Pro Getting Started guide for more information."
        response.end
    End If
End If   

If IsObject(session("oRpt")) Then
    set session("oRpt") = nothing
End If

On Error Resume Next
Set session("oRpt") = session("oApp").OpenReport(Directory & crystalFile, 1)
If Err.Number <> 0 Then

  '''Response.Write "Error Occurred creating Report Object: " & Err.Description
  '''Set Session("oRpt") = Nothing
  '''Set Session("oApp") = Nothing
  
  Response.Clear
  
  Session("oRpt") = Null
  Session("oApp") = Null
  
  session(session("siteID") & "errMsg") = "Crystal Report file missing or invalid."  
  Response.Redirect "../custom_edit.asp?action=edit&recid=" & reportID & "&name=" & Server.URLEncode( crystalFile ) & "&p=" & strParameters & "&top=" & topTab

  'Session.Abandon
  Response.End
    
End If
On Error Goto 0
	
	
session("oRpt").DiscardSavedData
session("oRpt").MorePrintEngineErrorMessages = False
session("oRpt").EnableParameterPrompting = False


'connect to DB if user is trying to run report
if lCase(viewer) <> "edit" then
	set crtable = session("oRpt").Database.Tables.Item(1)	
	crtable.SetLogonInfo cstr("myCM30"), cstr(""), cstr("sa"), cstr("0p3nup4m3W0n")	
	if not crtable.TestConnectivity then
	  	Response.Clear	  
		Session("oRpt") = Null
		Session("oApp") = Null	
		Response.Write "Warning:  Unable to connect to data source (myCM30)."
		Response.End
	end if
end if

set session("ParamCollection") = Session("oRpt").Parameterfields 

dim strParameters, arrI

'use to ensure a customer id param is on every report
dim bolCustomerID :  bolCustomerID = false

'populate parameters, viewer equals PDF, Excel...or "edit" if pass through
if lCase(viewer) <> "edit" and inStr(1,lCase(viewer),"issue") <= 0 then

	'cycle through all report PARAMS to see which exist and fill were needed
   	for arrI = 1 to session("ParamCollection").Count
	
		'customer id
		if session("ParamCollection").Item(arrI).Name = "{?CustomerID}" then
			bolCustomerID = true '<-- GOOD!!! it's okay to run report
			if len(rptProfiles) > 1 then 
				' "*" is necessary so EMO doesn't pull DEMO
				set ParamX = session("ParamCollection").Item(arrI)
				rptProfiles = replace(rptProfiles,", ","*")
				rptProfiles = "*" & rptProfiles & "*"
				NewParamValue = rptProfiles
				Call ParamX.SetCurrentValue(CStr(NewParamValue),12)
			else
				set ParamX = session("ParamCollection").Item(arrI)
				NewParamValue = session(session("siteID") & "customerid")
				Call ParamX.SetCurrentValue(CStr(NewParamValue),12)			
			end if

		'report title
		elseif session("ParamCollection").Item(arrI).Name = "{?ReportTitle}" then 
			set ParamX = session("ParamCollection").Item(arrI)
			NewParamValue = rptTitle
			Call ParamX.SetCurrentValue(CStr(NewParamValue),12)
	
		'from date param
		elseif session("ParamCollection").Item(arrI).Name = "{?FromDate}" then 
			set ParamX = session("ParamCollection").Item(arrI)
			NewParamValue = fromDate
			Call ParamX.SetCurrentValue(cDate(NewParamValue))
			
		'to date
		elseif session("ParamCollection").Item(arrI).Name = "{?ToDate}" then 
			set ParamX = session("ParamCollection").Item(arrI)
			NewParamValue = toDate
			Call ParamX.SetCurrentValue(cDate(NewParamValue))

		'status...uses the IN filter setting
		elseif session("ParamCollection").Item(arrI).Name = "{?Status}" then 
			set ParamX = session("ParamCollection").Item(arrI)
			NewParamValue = "[" & rptStatus & "]"
			Call ParamX.SetCurrentValue(CStr(NewParamValue),12)

		'issuetype...uses the IN filter setting
		elseif session("ParamCollection").Item(arrI).Name = "{?IssueType}" then 
			set ParamX = session("ParamCollection").Item(arrI)
			NewParamValue = "[" & rptIssueType & "]"
			Call ParamX.SetCurrentValue(CStr(NewParamValue),12)

		'location name...uses LIKE filter setting
		elseif session("ParamCollection").Item(arrI).Name = "{?Location_Name}" then 
			set ParamX = session("ParamCollection").Item(arrI)
			NewParamValue = rptLocation_Name
			Call ParamX.SetCurrentValue(CStr(NewParamValue),12)

		'issuetype...uses the IN filter setting
		elseif session("ParamCollection").Item(arrI).Name = "{?Location_Level_1}" then 
			set ParamX = session("ParamCollection").Item(arrI)
			'--> because Crystal would not let me pass unique value into "in [...]"
			'--> i had to add a unique identifier to each value "*"
			rptLocation_Level_1 = replace(rptLocation_Level_1,", ","*")
			rptLocation_Level_1 = "*" & rptLocation_Level_1 & "*"
			NewParamValue = rptLocation_Level_1
			Call ParamX.SetCurrentValue(CStr(NewParamValue),12)

		'issuetype...uses the IN filter setting
		elseif session("ParamCollection").Item(arrI).Name = "{?Location_Level_2}" then 
			set ParamX = session("ParamCollection").Item(arrI)
			rptLocation_Level_2 = replace(rptLocation_Level_2,", ","*")
			rptLocation_Level_2 = "*" & rptLocation_Level_2 & "*"
			NewParamValue = rptLocation_Level_2
			Call ParamX.SetCurrentValue(CStr(NewParamValue),12)

		'issuetype...uses the IN filter setting
		elseif session("ParamCollection").Item(arrI).Name = "{?Location_Level_3}" then 
			set ParamX = session("ParamCollection").Item(arrI)
			rptLocation_Level_3 = replace(rptLocation_Level_3,", ","*")
			rptLocation_Level_3 = "*" & rptLocation_Level_3 & "*"
			NewParamValue = rptLocation_Level_3
			Call ParamX.SetCurrentValue(CStr(NewParamValue),12)

		'location name...uses LIKE filter setting
		elseif session("ParamCollection").Item(arrI).Name = "{?Category}" then 
			set ParamX = session("ParamCollection").Item(arrI)
			NewParamValue = rptCategory
			Call ParamX.SetCurrentValue(CStr(NewParamValue),12)

		'location name...uses LIKE filter setting
		elseif session("ParamCollection").Item(arrI).Name = "{?Industry}" then 
			set ParamX = session("ParamCollection").Item(arrI)
			NewParamValue = trim(rptIndustry)
			Call ParamX.SetCurrentValue(CStr(NewParamValue),12)
			
		'user id
		elseif session("ParamCollection").Item(arrI).Name = "{?LogID}" then 
			set ParamX = session("ParamCollection").Item(arrI)
			NewParamValue = session(session("siteID") & "logid")
			Call ParamX.SetCurrentValue(CLng(NewParamValue))

		end if		
		
    next	

	'BAD!!! no customer id param means NO report...
	if bolCustomerID = false then
		response.write "Error: No Customer ID parameter defined."
		response.end	
	end if

	
'single issue requested
elseif inStr(1,lCase(viewer),"issue") then

	'pull out PDF from "PDF:issue"
	viewer = mid(viewer,1,inStr(1,lCase(viewer),":")-1)

	'cycle through all report PARAMS to see which exist and fill were needed
   	for arrI = 1 to session("ParamCollection").Count
	
		'customer id
		if session("ParamCollection").Item(arrI).Name = "{?CustomerID}" then 
			set ParamX = session("ParamCollection").Item(arrI)
			NewParamValue = session(session("siteID") & "customerid")
			Call ParamX.SetCurrentValue(CStr(NewParamValue),12)
	
		'from date param
		elseif session("ParamCollection").Item(arrI).Name = "{?CRSID}" then 						
			set ParamX = session("ParamCollection").Item(arrI)
			if includeFollowUp = "yes" then
				NewParamValue = left(idIssue,len(idIssue)-3) & "*"
			else
				NewParamValue = idIssue
			end if
			Call ParamX.SetCurrentValue(CStr(NewParamValue),12)			
			
		elseif session("ParamCollection").Item(arrI).Name = "{?IncludeSection}" then 
			set ParamX = session("ParamCollection").Item(arrI)
			NewParamValue = includeSection
			Call ParamX.SetCurrentValue(CStr(NewParamValue),12)

		end if		
		
    next	

'pass through to custom_edit.asp
elseif lCase(viewer) = "edit" then

	'dim strParameters, arrI
	'collect parameters
   	for arrI = 1 to session("ParamCollection").Count
    	strParameters = strParameters & session("ParamCollection").Item(arrI).Name
    next	
	'clean up...remove all { and }
	strParameters = replace(strParameters,"{","")
	strParameters = replace(strParameters,"?","")	
	strParameters = replace(strParameters,"}",",")

end if


If IsObject (session("oPageEngine")) Then                              
	set session("oPageEngine") = nothing
End If

set session("oPageEngine") = session("oRpt").PageEngine

%>
<% 'viewer = "PDF" %>
<%
if viewer = "edit" then
	Response.Clear
	Response.Redirect "../custom_edit.asp?action=edit&recid=" & reportID & "&name=" & Server.URLEncode( crystalFile ) & "&p=" & strParameters & "&top=" & topTab
end if
if viewer = "Word" then
    exportfmt = "crxf_wordw:0"
    exportext = ".doc"
end if
if viewer = "Excel" then
    exportfmt = "crxf_xls:0"
    exportext = ".xls"
end if
if viewer = "ExcelData" then
    exportfmt = "crxf_xls:10"
    exportext = ".xls"
end if
if viewer = "RTF" then
    exportfmt = "crxf_rtf:0"
    exportext = ".rtf"
end if
if viewer = "EditableRTF" then
    exportfmt = "crxf_rtf:39"
    exportext = ".rtf"
end if
if viewer = "PDF" then
    exportfmt = "crxf_pdf:0"
    exportext = ".pdf"
end if
%>
<%
Response.Clear
Response.Redirect "rptserver.asp?cmd=export&export_fmt=" & exportfmt & "&exportfilename=myReport" & exportext
%>

