<HTML>
<HEAD>
<TITLE>Session Cleanup</Title>
</HEAD>
<BODY Onload="CallClose();">
<%
Function DestroyObjects(ByRef ObjectToDestroy)
  If isobject(ObjectToDestroy) then
    set ObjectToDestroy = nothing
    'Not supported with IIS4, so ignore an error here.
    On Error Resume Next
    Session.Contents.Remove(ObjectToDestroy)
    On Error Goto 0
    DestroyObjects = true
  Else
    DestroyObjects = false
  End if
End Function

DestroyObjects(session("oPageEngine"))
DestroyObjects(session("oRpt")) 
DestroyObjects(session("oApp"))
%>

<SCRIPT LANGUAGE="Javascript">

function CallClose()
{
 if ( location.toString().search(/cleanup.asp/i) != -1 ) { window.close() };
}
</SCRIPT>
</BODY>
</HTML>
<%
Session.Abandon
Response.End
%>
