<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->

<%
'Database
dim mySQL, cn, rs, rs2

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'Products
dim idReport
dim arrTemp
dim tempI
dim i

'User Fields
dim customerID
dim optName
dim optDesc
dim optDataset
dim optUserRole
dim rptLogid
dim optUser
dim optType
dim optQueryValue
dim optCategory
dim optFileName
dim optNotes
dim optParamDate

dim optModifiedDate

'ADD FILTER
dim expI
dim arrField
dim arrExp
dim arrValue
dim tmpValue

'FILE DELETION
dim delFile
dim fs
dim upDel
dim Directory

'Work Fields
dim action

'Get action
action = trim(lCase(Request.Form("action")))
if len(action) = 0 then
	action = trim(lCase(Request.QueryString("action")))
end if
if  action <> "edit" _
and action <> "del"  _
and action <> "add"  _
and action <> "bulkdel" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if

'Get idReport
if action = "edit" _
or action = "del"  then

	idReport = trim(Request.Form("idReport"))
	if len(idReport) = 0 then
		idReport = trim(Request.QueryString("recID"))
	end if
	if idReport = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Report ID")
	end if

end if

'used to redirect user back to tabs they last were on
dim topTab
topTab = trim(Request.Form("topTab"))
if len(topTab) = 0 then
	topTab = trim(Request.QueryString("top"))
end if
		
dim pageReturn
pageReturn = Request.Querystring("return")
if len(pageReturn) <= 0 then
	pageReturn = Request.Form("return")
end if
		
'Get Metric Details
if action = "edit" or action = "add" then

	'flag right now as when this issue was modified
	optModifiedDate = Now()

	'Get Customer ID --> Required
	customerID	= trim(Request.Form("customerID"))
	if len(customerID) = 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if

	'Get Report Type --> Required (This MUST be left in 1st place.)
	optType = trim(Request.Form("optType"))
	if len(optType) = 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Metric Type.")
	end if

	'Get Report Name --> Required
	optName	= trim(Request.Form("optName"))
	if len(optName) = 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Metric Name.")
	end if

	'Get Report Dataset --> Required
	optDataset	= trim(Request.Form("optDataset"))
	if len(optDataset) = 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Dataset.")
	end if

	'Get Report owner --> Required
	rptLogid = trim(Request.Form("rptLogid"))
	if len(rptLogid) = 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Report Owner.")
	end if

	'Get Report owner --> Required
	optFileName = trim(Request.Form("optFileName"))

	'Get Category --> Optional
	optCategory = trim(Request.Form("optCategory"))

	'Get Notes --> Optional
	optNotes = trim(Request.Form("optNotes"))

	'Get Report Dataset --> Required
	optParamDate = trim(Request.Form("optParamDate"))
	if len(optParamDate) = 0 then
		optParamDate = "1/1/2000"
	end if
	
end if


'EDIT report
if action = "edit" and idReport <> 0 then
		
	'Update Record
	mySQL = "UPDATE Reports SET " _
		  & "CustomerID='"		& customerID						& "'," _
		  & "Logid="   			& rptLogid							& ", " _
		  & "Name='"   			& replace(optName,"'","''")			& "'," _
		  & "Category='"   		& replace(optCategory,"'","''")		& "'," _		  
		  & "FileName='"		& optFileName						& "'," _   		  		  		  		  		  		  	  		  		  		  		  		  		  
		  & "ParameterDate='"	& optParamDate						& "'," _ 
		  & "Notes='"   		& replace(optNotes,"'","''")		& "'," _		  
		  & "ModifiedBy="		& sLogid							& ", " _
		  & "ModifiedDate='"    & optModifiedDate					& "' "		  
	'finalize update query
	mySQL = mySQL & "WHERE ReportID = " & idReport

'	response.redirect "../error/default.asp?errMsg=" & server.URLEncode(mySQL)	

						  
	set rs = openRSexecute(mySQL)
	
	session(session("siteID") & "okMsg") = "Report (" & optName & ") was saved successfully."
	response.redirect "../reports/crystal/execute.asp?type=edit&recID=" & idReport & "&cid=" & customerID & "&name=" & optFileName & "&top=" & topTab & "&return=" & pageReturn

end if


'DELETE
if action = "del" then

	'Make sure right flag is called
	upDel = trim(Request.QueryString("upDel"))
	if upDel <> "Yes" then
		call endOfPage("File Deletion is not allowed.")
	end if

	'***********************************************	
	'remove report from directory...
	'***********************************************	
	
'TURNED OFF DELETING REPORT FROM DIRECTORY FOR NOW...STILL BUILING ALL REPORTS
'THIS WORKS SO JUST TURN IT BACK ON WHEN NEEDED	
'	on error resume next

	'Get delete file (if any)
'	delFile = trim(Request.QueryString("del"))

'	Directory = dataDir & "\profiles\" & sCustomerID & "\crystal"
	'''Directory = "C:\inetpub\wwwroot\myCM\_data\crystal"

	'Get FileSystem Object
'	set fs = CreateObject("Scripting.FileSystemObject")
'	if err.number <> 0 then
'		call endOfPage(err.Description)
'	end if
	
	'If delete was requested, delete the file
'	if len(delFile) > 0 then
'		fs.DeleteFile(Directory & "\" & delFile)
'		if err.Number <> 0 then
'			call endOfPage(err.Description & " : " & Directory & "\" & delFile)
'		end if
'	end if


	'***********************************************	
	'remove report from database...
	'***********************************************	
	'Set CursorLocation of the Connection Object to Client
	cn.CursorLocation = adUseClient
	
	'BEGIN Transaction
	cn.BeginTrans
		
	'Delete records from Categories_Products
	mySQL = "DELETE FROM Reports " _
		  & "WHERE ReportID=" & idReport & " AND LogID=" & sLogid & " AND CustomerID='" & customerID & "'"
	set rs = openRSexecute(mySQL)
				
	'END Transaction
	cn.CommitTrans
		
	'close database connection
	call closeDB()

	session(session("siteID") & "okMsg") = "Report was deleted successfully."
	Response.Redirect "../reports/reports.asp"

end if


'Just in case we ever get this far...
call closeDB()
response.redirect "../reports/reports.asp"


'*********************************************************************
'End of page processing
'*********************************************************************
sub endOfPage(msg)
	'Clean up
	set fs = nothing
	'Check for error message
	if len(msg) <> 0 then
		session(session("siteID") & "errMsg") = msg
		Response.Redirect "../reports/reports.asp"
	end if
	Response.End
end sub

%>
