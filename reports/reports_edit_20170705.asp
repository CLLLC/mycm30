<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = false
const showDHTMLMenu = true
const helpID = 4
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "reports:edit"
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

dim showCRSID '...need for NEW search box
dim startDate
dim endDate

'Issue Fields on DETAILS tab
dim optID
dim optName
dim optSort
dim optQueryValue
dim optSQLValue
dim optUserRole
'dim optUser
dim rptOwner
dim rptLogid
dim optUsers
dim optOwner
dim optDataset
dim optDatasetID
dim optDatasetDesc

dim optTable
dim jsonTable
dim jsonCID

dim optType
dim optDesc
dim optFields
dim optFilter
dim optIssueList
dim optURLField
dim optGroup
dim optGroupCalc
dim optSortOrder
dim optSortField
dim optCategory
dim optNotes
dim optDatasetCustID

dim listColumn, listFriendly

'Chart options
dim optChart
dim optChartXAxis
'dim optChartXRotate
dim optChartYAxis
dim optChartSize
dim optChartLegend
'dim optChartBackground
'dim optChartCanvas
'dim optChartElement

dim intStep
dim strTemp

'Work Fields
dim action, idProduct, pgNotice
dim idReport

'Work Fields
dim optArr, optArrIndex

'Used with Dynamic List Contact selections
dim intGroups, arrGroups, groupsI, intUsers
dim tmpUserID, tmpUserName, lstUserName
dim arrUsers, usersI
dim userLogID

dim field, arrFields, arrColumn, arrFriendly

'Page to return user too.
dim pageReturn
pageReturn = trim(Request.QueryString("return"))
if len(pageReturn) = 0 then
	pageReturn = trim(Request.Form("pageReturn"))
end if

'Get action
action = trim(Request.QueryString("action"))
if len(action) = 0 then
	action = trim(Request.Form("action"))
end if
action = lCase(action)
if  action <> "edit" _
and action <> "del"  _
and action <> "view" _
and action <> "add"  _
and action <> "addfu"  then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if

'Get idIssue
if action = "edit" then
	idReport = trim(Request.QueryString("recId"))
	if len(idReport) = 0 then
		idReport = trim(Request.Form("recId"))
	end if
	if idReport = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Report ID.")
	end if
	if editReport<>"Y" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You do not have the required permissions to edit this report.")		
	end if
end if

'Get idIssue
if action = "add" then
	optDataset = trim(Request.QueryString("dataset"))
	if len(optDataset) = 0 then
		optDataset = trim(Request.Form("dataset"))
	end if
	if optDataset = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Report Dataset.")
	end if
	if addReport<>"Y" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You do not have the required permissions to add this report.")		
	end if	
end if

'used to set side top and side tabs after user saves record
dim topTab
topTab = trim(Request.QueryString("top"))
if len(topTab) <= 0 then
	topTab = "configure"
end if

'Get Issue Record... idReport 0 == Issue List on home page
if action = "edit" and idReport <> 0 then

	'get report record
	mySQL = "SELECT Reports.ReportID, Reports.CustomerID, Dataset.CustomerID AS [Dataset_CustomerID], Dataset.DatasetID, Dataset.Name AS Dataset, Dataset.[Table], Dataset.Type, Dataset.ListURLField, " _
		  & "		Dataset.Description, Reports.LogID, Reports.Name, Reports.IssueList, Reports.QueryFields, Reports.QueryFilter, " _
		  & "		Reports.QuerySumGroup, Reports.QuerySumCalculation, Reports.Category, Reports.Notes, Reports.QuerySortOrder, " _
		  & "		Reports.QuerySortField, Reports.SQL, Reports.ChartType, Reports.ChartXAxis, Reports.ChartYAxis, Reports.ChartLegend, " _
		  & "		Reports.ChartSize, Reports.SortOrder, Logins.FirstName, Logins.LastName, Logins.Email " _
		  & "FROM   Reports INNER JOIN " _
		  & "		Dataset ON Reports.DatasetID = Dataset.DatasetID LEFT OUTER JOIN " _
		  & "		Logins ON Reports.CustomerID = Logins.CustomerID AND Reports.LogID = Logins.LOGID " _
		  & "WHERE ReportID = " & idReport & " "

'ORIGINAL
'mySQL = mySQL & " WHERE ReportID = " & idReport & " AND Reports.CustomerID = '" & sCustomerID & "' AND (Reports.LOGID=" & sLogid & ")"
				  
	'non-CCI staff and non-ProgramAdmins can ONLY view their own reports
	if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and ProgramAdmin<>"Y" then		  
		mySQL = insertWhereClause(mySQL,"Reports.CustomerID = '" & sCustomerID & "' and Reports.LOGID=" & sLogid & "")
							
	elseif cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and ProgramAdmin="Y" then		  
		mySQL = insertWhereClause(mySQL,"Reports.CustomerID IN (" & profileIDs & ") ")

	end if	  
		  
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	
	if rs.eof then
		response.redirect "../reports/reports.asp"
	else
		optID			 = rs("ReportID")
		customerID		 = rs("customerID")
		rptLogid		 = rs("Logid")
		optName			 = rs("Name")
		optCategory		 = rs("Category") & ""		
		optDatasetCustID = rs("Dataset_CustomerID")						
		optDataset		 = rs("Dataset") & ""	
		optDatasetID	 = rs("DatasetID") & ""	
		optDatasetDesc	 = rs("Description") & ""
		optTable		 = rs("Table") & ""				
		optType			 = rs("Type") & ""
		optIssueList	 = rs("IssueList") & ""
		optURLField		 = rs("ListURLField") & ""
		optFields		 = rs("QueryFields") & ""
		optFilter		 = rs("QueryFilter") & ""
		optGroup		 = rs("QuerySumGroup") & ""
		optGroupCalc	 = rs("QuerySumCalculation") & ""
		optSortOrder	 = rs("QuerySortOrder") & ""
		optSortField	 = rs("QuerySortField") & ""
		optSQLValue		 = rs("SQL") & ""				
		optChart		 = rs("ChartType") & ""
		optChartXAxis	 = rs("ChartXAxis") & ""
		optChartYAxis	 = rs("ChartYAxis") & ""
		optChartLegend	 = rs("ChartLegend") & ""
		optChartSize	 = rs("ChartSize") & ""
		optNotes		 = rs("Notes")
		
		'set rptOwner...used for display only
		if len(rs("LastName")) > 0 then rptOwner = rs("LastName")
		if len(rs("FirstName")) > 0 and len(rptOwner) > 0 then rptOwner = rptOwner & ", " & rs("FirstName") else rptOwner = rs("FirstName")		
		if len(rs("Email")) > 0 then rptOwner = rptOwner & " [" & rs("Email") & "]"
		
	end if
	call closeRS(rs)

	'make sure something at least the List URL Field is selected
	if len(optFields) <= 0 then
		if lCase(optType) = "calculated" then
			optFields 	= "[Issue_#]"	
		else
			optFields = "[" & optURLField & "]"		
		end if		
	end if

	'set compulsory fields
	if lCase(optType) = "calculated" then
		optURLField	= "Issue_#"
	end if

'Get info for main issue list
'FINISH THIS FOR MODIFYING ISSUES.ASP 
elseif idReport = 0 then

	'get customer record
	mySQL="SELECT customerID, IssuesColumns, WhereClause, SortField, SortOrder " _
		& "       FROM Logins " _
	    & "		  WHERE LogID = " & sLogid 						
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	
	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("User account not found.")
	else
		optID			= 0
'		optUser			= sLogid
		customerID		= rs("customerID")
		rptLogid		= sLogid		
		optName			= "Issue List" 
		optDataset		= "Issues" 
		optTable		= userIssuesView '--> points to CRS/Issues table
		optType			= "Register"		
		optURLField		= "Issue #"		
		optIssueList	= "Y"
		optFields		= rs("IssuesColumns") & ""
		optFilter		= rs("WhereClause") & ""		
		optSortField	= rs("SortField") & ""
		optSortOrder	= rs("SortOrder") & ""
	end if
	call closeRS(rs)
	
	'make sure something is assigned to Issue Fields
	if len(optFields) <= 0 then
		optFields = "[Issue #], [Date], [Severity], [Status], [Location Name]"
	end if

	'set rptOwner...used for display only
	if len(session(session("siteID") & "lastname")) > 0 then rptOwner = session(session("siteID") & "lastname")
	if len(session(session("siteID") & "firstname")) > 0 and len(rptOwner) > 0 then rptOwner = rptOwner & ", " & session(session("siteID") & "firstname") else rptOwner = session(session("siteID") & "firstname")
	if len(Email) > 0 then rptOwner = rptOwner & " [" & Email & "]"
	
end if


'Get fields for list box...these are the fields the use selects
if (len(optDataset) > 0 and optDataset > "") and (lCase(optType) <> "sql") then

	'get customer record
	mySQL="SELECT * " _
		& "   FROM " & optTable & " " _
	    & "	  WHERE 1=2"								
	'set rs = openRSexecute(mySQL)
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	'loop through all columns
	for each field in rs.fields
		if lCase(field.name) = "logid" or lCase(field.name) = "viewedby" or lCase(field.name) = "customerid" or lCase(field.name) = "issuetypeid" or lCase(field.name) = "crsid" or lCase(field.name) = "logidview" or inStr(lCase(field.name),"hierarchy_search") then
			'DO NOT ADD THIS FIELD...
		else
			listColumn = listColumn & "[" & field.name & "],"
			listFriendly = listFriendly & field.name & ","
		end if
	next
	
	'create array of available fields
	if right(listColumn,1) = "," then listColumn = left(listColumn,len(listColumn)-1)	
	arrColumn = split(listColumn,",")

	'create array of available fields WITHOUT "[ ]" brackets for display only	
	if right(listFriendly,1) = "," then listColumn = left(listFriendly,len(listFriendly)-1)	
	arrFriendly = split(listFriendly,",")

	'create array of fields already selected by user previously
	arrFields = split(optFields,",")

end if


'set Owner for newly added reports
if action = "add" then				
	rptLogid = sLogid
	'set rptOwner...used for display only
	if len(session(session("siteID") & "lastname")) > 0 then rptOwner = session(session("siteID") & "lastname")
	if len(session(session("siteID") & "firstname")) > 0 and len(rptOwner) > 0 then rptOwner = rptOwner & ", " & session(session("siteID") & "firstname") else rptOwner = session(session("siteID") & "firstname")
	if len(Email) > 0 then rptOwner = rptOwner & " [" & Email & "]"
end if
if rptLogID = "" or isNull(rptLogID) then
	rptLogID = 0
end if


'Get Coordinator Record
if action = "add" then
	optChart = "Column"
end if


'Set JSon params and data
if action = "edit" or action = "add" then

	'SQL View/Table assigned to report
	jsonTable = optTable
	
	'CCI staff -- limit jSon data pulls to customerID this report belongs too
	
'	if cLng(session(session("siteID") & "adminLoggedOn")) < 10 and pageReturn = "user" then		  
	if cLng(sLogid) <> cLng(rptLogid) then	
	
'		jsonCID = customerID

'THIS WORKS, BUT WITHIN JSON.ASP BECUSE THE USER <10 THE QUERY IS SET AS
'CUSTOMERID='' AND DOESN'T WORK WITH 'AAA','DEMO' etc..
'MAYBE CAN FIX LATER		
		mySQL = "SELECT CustomerID " _
			  & "	FROM vwLogins_IssueType " _
			  & "	WHERE LOGID = " & rptLogid & " " _
			  & "	GROUP BY CustomerID "			
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)			  

		'user is NOT associated with any type of issues yet		
		if rs.eof then
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("ERROR: User is NOT associated with any Issue Types.")
		else
			do while not rs.eof
				jsonCID = jsonCID & "'" & rs("CustomerID") & "',"
				rs.movenext
			loop
			jsonCID = left(jsonCID,len(jsonCID)-1)
		end if
		
	else
		jsonCID = profileIDs
		
	end if	  

end if

%>

<% 
dim pageTitle
if action = "add" then
	pageTitle = "Report: (new report)" 
else
	pageTitle = optName & " (edit)"
end if
%>

<!--#include file="../_includes/_INCheader_.asp"-->

<!-- START Main Middle Section -->
<form name="frm" id="frm" method="post" action="reports_exec.asp" style="padding:0px; margin:0px;">

<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
  
	    <!-- START Left sided area -->  
		<td width="190" align="right" valign="top">    

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="../profiles/users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">
    
                    <!-- START side menu include -->
                    <!--#include file="../scripts/tree/tree.asp"-->
                    <% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>      
	                    <script>
   		                	stExpandSubTree('myCMtree',3,0);
       		            </script>
            		<% else %>
	                    <script>
   		                	stExpandSubTree('myCMtree',2,0);
       		            </script>
                    <% end if %>
                    <!-- STOP side menu include -->
                    
                </td>
              </tr>
              <!-- STOP Left sided menu -->
        
            </table>

		</td>
    	<!-- STOP Left sided area -->


        
        <td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">    

			<%
			'used to STOP updating the cookie crumb if page is being recalled from a SAVE
			dim updateCrumb
            if len(session(session("siteID") & "errMsg")) > 0 or len(session(session("siteID") & "okMsg")) > 0 then
				updateCrumb = "false"
            end if
			%>
            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs('<% =updateCrumb %>'));
       	        </script>
            </div>
    
            <div style="text-align:left; margin-bottom:10px;">                        
				<div style="float:left; padding-right:2px; margin-right:2px;">            
					<% 
                    if action = "add" then
                        response.write("<img src=""../_images/icons/32/chart_bar_add.png"" title="""" width=""32"" height=""32"" align=""absmiddle"" style=""margin-right:2px; vertical-align:middle; vertical-align:middle;"">")
                    else
                        if idReport > 0 then				
                            response.write("<img src=""../_images/icons/32/chart_bar_edit.png"" title="""" width=""32"" height=""32"" align=""absmiddle"" style=""margin-right:2px; vertical-align:middle; vertical-align:middle;"">")
                        elseif idReport = 0 then				
                            response.write("<img src=""../_images/icons/32/table_edit.png"" title="""" width=""32"" height=""32"" align=""absmiddle"" style=""margin-right:2px; vertical-align:middle; vertical-align:middle;"">")
                        end if
                    end if
                    %>                
                </div>
                <div>                
					<% 
                    if action = "add" then
                        response.write("<span class=""pageTitle"">Report: " & optName & "</span>")
                    else
                        if idReport > 0 then				
                            response.write("<span class=""pageTitle"">Report: " & optName & "</span>")
                        elseif idReport = 0 then				
                            response.write("<span class=""pageTitle"">" & optName & "</span>")
                        end if
                    end if
                    %>                
                	<br/>:: <a href="../profiles/users_edit.asp?action=edit&recid=<% =rptLogid %>"><% response.write( rptOwner ) %></a>
                </div>                
            </div>                   
                               
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
            	            <legend>Information</legend>
	                        <div align="left">
    	                        <img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; vertical-align:middle;">Use the interface below to select the columns and filters to apply to this report.
								<% response.write( getInlineHelp("getPageURL", 0) ) %>
        	                </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             
                                    
                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">
                    	<% if idReport <> 0 then %>
                           	<div class="infoButtons">
                            	<% if lCase(optType) <> "sql" then %>                            
                                    <a href="#" onClick="javascript:popup('reports_print.asp?pageview=<% =optID %>&source=reports',600,600); return false;"><img src="../_images/icons/24/printer.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                    <a href="#" onClick="javascript:popup('reports_print.asp?pageview=<% =optID %>&source=reports',600,600); return false;">Print</a>        	                        
       	                    	<% else %>
                                    <a href="#" onClick="javascript:popup('reports_print.asp?pageview=<% =optID %>&source=reports',600,600); return false;"><img src="../_images/icons/24/database_lightning.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                    <a href="#" onClick="javascript:popup('reports_print.asp?pageview=<% =optID %>&source=reports',600,600); return false;">Execute</a>        	                        
	                            <% end if %>                            
                            </div>
                            <% if lCase(optType) <> "sql" then %>     
                                <div class="infoButtons">
                                    <a href="reports_export.asp?pageview=<% =optID %>&action=csv"><img src="../_images/icons/24/file_extension_xls.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                    <a href="reports_export.asp?pageview=<% =optID %>&action=csv">Excel (.csv)</a>
                                </div>                   
                            <% end if %>
                        <% else %>
                           	<div class="infoButtons">
                                <a href="#" onClick="javascript:popup('reports_print.asp?pageview=0&source=reports',600,600); return false;"><img src="../_images/icons/24/printer.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                <a href="#" onClick="javascript:popup('reports_print.asp?pageview=0&source=reports',600,600); return false;">Print</a>        	                        
                            </div>                        
                            <div class="infoButtons">
                                <a href="reports_export.asp?pageview=issues&action=csv"><img src="../_images/icons/24/file_extension_xls.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                <a href="reports_export.asp?pageview=issues&action=csv">Excel (.csv)</a>
                            </div>                                                                   
						<% end if %>
                    </td>
                    <!-- STOP Information buttons -->
                        
                </tr>
            </table>        
            <!-- STOP information box -->

    
    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			'clear system messages
			call clearSessionMessages()
            %>            
    		<!-- STOP message notification box -->

                        
            <!-- START Top Page section with tabs & buttons -->
            <table id="table_tabs" width="100%" cellpadding="0" cellspacing="0">            
                <tr>
                
                    <!-- START Top Page Tabs-->
                    <td height="40" width="70%" valign="top">
                        <ul id="ul_tabs" class="tabs">
                            <li id="configure" style="width:125px;" class="on" onClick="clickTopTab('configure',''); resetSystemMsg('systemMessage');"><div>Configuration</div></li>
                            <li id="settings" style="width:125px;" class="off" onClick="clickTopTab('settings',''); resetSystemMsg('systemMessage');"><div>Settings</div></li>							
                            <li id="chart" style="width:125px; <% if lCase(optType)="register" or lCase(optType)="sql" then response.write("display:none;") %>" class="on" onClick="clickTopTab('chart',''); resetSystemMsg('systemMessage');"><div>Chart</div></li>
                            <!-- the last id must be set to "end" -->
                            <li id="end" class="empty" style="width:150px;"><div>&nbsp;</div></li>
                        </ul> 

                        <input type=hidden name="topTab" id="topTab" value="<% =topTab %>">                                                                
                        
                    </td>                
                    <!-- STOP Top Page Tabs-->              
                    
                    <!-- START Submit Button at top of page--> 
                    <td nowrap style="padding-bottom:10px;" align="left">
                    
                    	<!-- hidden fields --> 
                        <input type=hidden name="optType" id="optType" value="<% =optType %>">
                        <input type=hidden name="optURLField" value="<% =optURLField %>">
                        <input type=hidden name="pageReturn" value="<% =pageReturn %>">                        
                        <input type=hidden name="customerID" value="<% =customerID %>" >
                        
                        <!-- used to pass SQL View params to json.asp -- NOT SAVED with report --> 
                        <input type=hidden name="jsonTable" id="jsonTable" value="<% =jsonTable %>">                                                                        
                        <input type=hidden name="jsonCID" id="jsonCID" value="<% =jsonCID %>" >
                                                
                        <!-- SAVE/ADD button -->                        
                        <%
                        if action="edit" then    
                        	if editReport="Y" then
							%>
                                <input type="hidden" name="idReport" value="<% =optID %>">
                                <input type="hidden" name="action" value="edit">
                                <!-- SAVE button -->
                                <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); resetSystemMsg('systemMessage'); if(filterCheck()==false){document.frm.submit()}; return false;"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
							<%
							else
							%>
                            	<a class="myCMbutton-off" href="#" onClick="this.blur(); return false;"><span class="save" style="padding-right:7px;">Save Issue</span></a>
                            <%
							end if
                        elseif action="add" then
							if addReport="Y" then
                        	%>                            
                                <input type=hidden name=idReport value="<% =optID %>">
                                <input type=hidden name=action value="<% =action %>">
                                <!-- ADD button -->                            
                                <a class="myCMbutton" href="#" accesskey="A" onClick="this.blur(); if(filterCheck()==false){document.frm.submit()}; return false;"><span class="add" style="padding-right:15px;"><u>A</u>dd</span></a>
                      		<%
							else
							%>
		                        <a class="myCMbutton-off" href="#" onClick="this.blur(); return false;"><span class="add" style="padding-right:7px;">Add Issue</span></a>
							<%
							end if
                        end if
                        %>
                        
                        <!-- CLOSE button -->
                        <%
                        if pageReturn = "issuelist" then
                        %>
							<a class="myCMbutton" href="#" accesskey="R" onClick="this.blur(); navigateAway('../scripts/issues.asp'); return false;"><span class="return" style="padding-right:5px;"><u>R</u>eturn</span></a>
                        <%
                        elseif pageReturn = "sublist" then
                        %>
							<a class="myCMbutton" href="#" accesskey="R" onClick="this.blur(); navigateAway('../scripts/issues.asp?pageView=<% =optID %>'); return false;"><span class="return" style="padding-right:5px;"><u>R</u>eturn</span></a>
                        <%
                        elseif pageReturn = "user" then
                        %>
							<a class="myCMbutton" href="#" accesskey="R" onClick="this.blur(); navigateAway('../profiles/users_edit.asp?action=edit&recid=<% =rptLogid %>&pageview=users&cid=<% =customerID %>&top=reports&side='); return false;"><span class="return" style="padding-right:5px;"><u>R</u>eturn</span></a>
						<%
                        else
                        %>                        
							<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); navigateAway('reports.asp'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>
                        <%
                        end if
                        %>
                    </td>
                    <!-- STOP Submit Button -->
                             
                </tr>
            </table>
            <!-- STOP Top Page Tabs -->

            <!-- START Side Page Tabs [details] "id=tab:details" defines the section for the Top Tabs -->
            <!--#include file="../_includes/_INCreportConfigure_.asp"-->            
            <!-- STOP Side Page Tabs [details] -->    
        
            <!-- START Side Page Tabs [notes] "id=tab:notes" defines the section for the Top Tabs -->
            <!--#include file="../_includes/_INCreportSettings_.asp"-->            
            <!-- STOP Side Page Tabs [notes] -->

            <!-- START Side Page Tabs [notes] "id=tab:notes" defines the section for the Top Tabs -->
            <!--#include file="../_includes/_INCreportChart_.asp"-->            
            <!-- STOP Side Page Tabs [notes] -->

			<div style="padding:10px;">&nbsp;</div>    
    
            <!-- START Final table -->        
            <table class="formTable" width="100%" cellpadding="0" cellspacing="0">      
                                                              
                <tr>
                    <td width="70%" valign="top" align="left" nowrap>&nbsp;</td>
                    
                    <!-- START Submit Button at bottom of page -->                 
                    <td valign="top" align="left" nowrap>           
                    
                        <!-- SAVE/ADD button -->                                                 
                        <%
                        if action="edit" then    
                        	if editReport="Y" then
							%>
								<a class="myCMbutton" href="#" onClick="this.blur(); if(filterCheck()==false){document.frm.submit()}; return false;"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
							<%
							else
							%>
                            	<a class="myCMbutton-off" href="#" onClick="this.blur(); return false;"><span class="save" style="padding-right:7px;">Save Issue</span></a>
                            <%
							end if
                        elseif action="add" then
							if addReport="Y" then
                        	%>                            
								<a class="myCMbutton" href="#" onClick="this.blur(); if(filterCheck()==false){document.frm.submit()}; return false;"><span class="add" style="padding-right:15px;"><u>A</u>dd</span></a>
                      		<%
							else
							%>
		                        <a class="myCMbutton-off" href="#" onClick="this.blur(); return false;"><span class="add" style="padding-right:7px;">Add Issue</span></a>
							<%
							end if
                        end if
                        %>
                        
                        <!-- CLOSE button -->
                        <%
                        if pageReturn = "issuelist" then
                        %>
							<a class="myCMbutton" href="#" accesskey="R" onClick="this.blur(); location.href='../scripts/issues.asp'; return false;"><span class="return" style="padding-right:5px;"><u>R</u>eturn</span></a>                                                
                        <%
                        elseif pageReturn = "sublist" then
                        %>
							<a class="myCMbutton" href="#" accesskey="R" onClick="this.blur(); location.href='../scripts/issues.asp?pageView=<% =optID %>'; return false;"><span class="return" style="padding-right:5px;"><u>R</u>eturn</span></a>                        
                        <%
                        elseif pageReturn = "user" then
                        %>
							<a class="myCMbutton" href="#" accesskey="R" onClick="this.blur(); navigateAway('../profiles/users_edit.asp?action=edit&recid=<% =rptLogid %>&pageview=users&cid=<% =customerID %>&top=reports&side='); return false;"><span class="return" style="padding-right:5px;"><u>R</u>eturn</span></a>
						<%
                        else
                        %>                        
							<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); location.href='reports.asp'; return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>
                        <%
                        end if
                        %>
                    </td>
                    <!-- STOP Submit Button --> 
    
                </tr>      
                                      
            </table>
            <!-- STOP Final table -->               
    
	    </td> 
        

 
    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
	</tr>
</table>
<!-- STOP Page -->

</form>
<!-- STOP Main Middle Section -->

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<script type="text/javascript"> 
	//load form where user last was...pulled from query URL or form submit
	window.onload = function () {
		clickTopTab('<% =topTab %>','');
	}		
</script>


<script language="javascript">
	function setOwner(text,id) {
		
		//used by child window to add selected Report Owners
		var addE = document.getElementById('rptOwner');
		addE.value = text

		//check and see if value being added is in Removed list
		var delE = document.getElementById('rptLogid');
		delE.value = id 

	}
</script>


<script>
	function configDropDownOption(obj,list) {
		//used to rebuild options list with new values
		var el = document.getElementById(obj);
		var selIndex = el.selectedIndex;
		var selectedValue;
		//get current option value selected
		if (selIndex != -1) {
			for(i=el.length-1; i>=0; i--)
			{
			  if(el.options[i].selected)
			  {
				if(el.options[i].value){
					selectedValue = el.options[i].value; //current selection
				}
			  }
			}
		}
			
		//removed current selects in dropdown
		el.length = 0;
				
		//add new values to dropdown
		var i;		
		var temp = list.split("|"); //split list provided by popup_config_options.asp
		//add new options provided by user
		addOptionsItems(obj,"","-- Select --");
		for(i=0; i<temp.length; i++) {
			if (temp[i].length>1) {
				addOptionsItems(obj,temp[i],temp[i]);
			}
		}
		
		//check for existance of originally selected value		
		if(selectedValue){
			var bolMatch = false;
			i = 0;		
			for (i=0; i<el.options.length; i++) {
				if (el.options[i].text == selectedValue) {
					el.selectedIndex = i;
					bolMatch = true; //origianl still exists in list
				}
			}		
			//user removed original value, put back with "Missing:" at end of list
			if (bolMatch == false){
				addOptionsItems(obj,selectedValue,"Missing:"+selectedValue);
				el.selectedIndex = el.options.length-1;
			}
		}
		
	}
</script>

<script language="javascript">
	//NEED TO RESEARCH why this throws change message
	//even after the save button is pressed
	function navigateAway(url) {
//		var title = "Cancel Changes";
//		var msg = "Changes have been made to this issue and <strong>must<br>be saved</strong> before continuing.<br><br>Continue and cancel changes?";
		//if(form_is_modified(document.frm)==true) {

//		if(isFormChanged(document.frm)==true) {		
//			confirmPrompt(title,msg,'redirect',url);
//		}
//		else {
			window.location = url;
//		}
	}		
</script>


<script language="javascript">
	//DEAD FUNCTION...MUST BE HERE FOR 
	//popup_config_options.asp and popup_user.asp to work
	//this is called within Issues_Edit.asp and I didn't
	//want to code the change when called from here.
	function StartTheTimer() {
	}		
</script>



<script language="javascript">
	function filterCheck() {	
		var elSel 				//select element
		var errFound = false;	//retunred value
		var el = document.frm.elements.length;	//all elements in form
		//cycle though all form elements
    	for (i=0; i<el; i++) {
			//form element is a query field
			if (document.frm.elements[i].id.indexOf("idQueryValue_") > -1) {							
				//pull out ### on ID
				var elNbr = document.frm.elements[i].id.substring(document.frm.elements[i].id.indexOf("_")+1, document.frm.elements[i].id.length);
				//using number in elNbr find coresponding dropdown with query expression
				var elSel = document.getElementById('idQueryExp_' + elNbr);
				//check existance of select element
				if (document.getElementById('idQueryExp_' + elNbr)) {
					//assign select element
					elSel = document.getElementById('idQueryExp_' + elNbr);
					//keep going if expression is NOT null or is null
					if (elSel.options[elSel.selectedIndex].value != 'is null' && elSel.options[elSel.selectedIndex].value != 'is not null') {
						//check for NULL or BLANK value
						if (!document.frm.elements[i].value) {
							//set to yellow
							document.frm.elements[i].style.background = '#FFFF9F';
							//inform user of error
							jAlert('<strong>Filter Error!</strong><br/><br/>Please ensure all query filters have a value.', 'myCM Alert');
							errFound = true;
						}
						else {
							if (document.frm.elements[i].style.background == '#FFFF9F') {
								//set to white
								document.frm.elements[i].style.background = '#FFF';
							}
						}					
					}				
				}				
			}
		}	
		return errFound;
	}
</script>


