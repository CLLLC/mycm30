<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%	
'CHECK SECURITY HERE
if session(session("siteID") & "adminLoggedOn")<>"1" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You do not have permission to view this page.")
end if

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Email Log</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    

	<!-- HTML Table Filter Generator http://tablefilter.free.fr/ -->
    <link rel="stylesheet" type="text/css" href="../_tablefilter/filtergrid.css"/>
	<script type="text/javascript" src="../_tablefilter/tablefilter_all.js"></script>
	<script type="text/javascript" src="../_tablefilter/sortabletable.js"></script>
    <script type="text/javascript" src="../_tablefilter/tfAdapter.sortabletable.js"></script>        

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/32/email.32.png" alt="Users" width="32" height="32" align="absmiddle"> 
       	<span class="popupTitle">E-mail Log</span>
   	</div>                   

  	<!-- START information box -->
   	<table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
       	 <tr>	
			<!-- START Information message -->             
           	<td style="width:60%;">
            	<fieldset class="infoBox">
            	<legend>Information</legend>
              		<div align="left" style="padding:5px;">
                   		<img src="../_images/icons/16/information.png" alt="Information" width="16" height="16" align="absmiddle" style="margin-right:3px;">Below is a log of all e-mails sent from myCM.
             		</div>
                </fieldset>                                    
         	</td>
         	<!-- STOP Information message -->                                                                                             
		</tr>
	</table>        
	<!-- STOP information box -->
                
	<%                                
	'check how many records are returned
	mySQL="SELECT mailDate AS [Mail Date], fromEmail AS [From], toEmail AS [To], Subject FROM MailLog ORDER BY mailDate desc "
	
	dim tableProperty	
	'set _tablefilter/default.asp settings.
	tableProperty = "sort: true, sort_config:{sort_types:['String','String','String']}, filters_row_index: 1, " _
            	& "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
         		& "paging: true, paging_length: 5, " _
				& "highlight_keywords: true, " _
           		& "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", " _
          		& "or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
        		& "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"
			
	'make call to build grid found in ../_tablefilter/default.asp
	call buildGrid("emailTableID",mySQL,"","mailLog","",tableProperty) 	
	%>    
                
   	<!-- START Buttons -->
    <div style="border-top:1px dotted #cccccc; margin-top:10px; padding-top:5px;"> 	       		
    	<a class="myCMbutton" href="#" onClick="this.blur(); javascript:window.close();" style="float:right;"><span class="cancel">Close</span></a>
    </div>
    <!-- STOP Buttons -->

</body>
</html>

<%
'close database connection
call closeDB()
%>
