<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = false
const showDHTMLMenu = true
const helpID = 6
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "activeusers"
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate user security
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'Products
dim action
dim tableProperty
dim I
dim item
dim pageSize
dim totalPages
dim showArr
dim sortField
dim sortOrder
dim curPage

dim showCRSID

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) <> 1 then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if

dim arrUsers, userX

%>

<% dim pageTitle : pageTitle = "Active Users" %>
<!--#include file="../_includes/_INCheaderLite_.asp"-->

<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
        
        <!-- START Left sided area -->
		<td width="190" align="right" valign="top">

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="../profiles/users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided tree menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">                

                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->              
					<script>
   	                	stCollapseSubTree('myCMtree',1,0);
						stCollapseSubTree('myCMtree',2,0);
						stCollapseSubTree('myCMtree',3,0);
       	            </script>
                    <!-- STOP side menu include -->
                    
                </td>
              </tr>
              <!-- END Left sided tree menu -->
        
            </table>
        
        </td>
        <!-- STOP Left sided area -->
            
	    <!-- START Main Middle Section -->
		<td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">       

            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs());
       	        </script>
            </div>
            
            <div style="text-align:left; margin-bottom:10px;">            
				<img src="../_images/icons/32/group.png" title="" width="32" height="32" align="absmiddle">&nbsp;<span class="pageTitle">Active Users</span>
            </div>                   
                                          
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
                        <legend>Information</legend>
                        <div align="left">
	                        <img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px;">Below is a list of current users online.
                        </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             

                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">
                    	<% if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then %>
                            <div class="infoButtons">
								<!-- NO BUTTONS RIGHT NOW -->
                            </div>                   
						<% end if %>                                                
                    </td>
                    <!-- STOP Information buttons -->
                                                                                
                </tr>
            </table>        
            <!-- STOP information box -->
    

    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			'clear system messages
			call clearSessionMessages()
            %>            
    		<!-- STOP message notification box -->


            <!-- START Reports table -->
            <%                                
            'split Hierarchy values
            arrUsers = left(Application("ActiveUserList"),len(Application("ActiveUserList"))-1)
            arrUsers = split(arrUsers,"|")			
            %>
            
            <table id="activityTable" width="100%" style="background-color:#969696;" cellpadding=0 cellspacing=0>
                <thead>
                    <tr>
                        <th align="left" nowrap="nowrap" style="text-align:center;">Session ID</th>
                        <th align="left" nowrap="nowrap" style="text-align:center;">Last Active</th>                                
                    </tr>
                </thead>
                <tbody>            
                <%
                for userX = 0 to UBound(arrUsers)		
                    response.Write("<tr>")
                    response.Write("<td style=""text-align:left;"">" & mid(trim(arrUsers(userX)),1,instr(trim(arrUsers(userX)),":")-1)  & "</td>")
                    response.Write("<td style=""text-align:right;"">" & mid(trim(arrUsers(userX)),instr(trim(arrUsers(userX)),":")+1,len(trim(arrUsers(userX)))) & "</td>")					
                    response.Write("</tr>")
                next		
                %>
                </tbody>
            </table>
            
            <%
            tableProperty = "sort: true, sort_config:{sort_types:['String','date']}, filters_row_index: 1, " _
                & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
                & "status_bar: true, col_0: ""input"", col_1: ""input"", " _
                & "col_width:[""50%"",null], paging: true, paging_length: 10, " _
                & "enable_non_empty_option: true, or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, " _
                & "highlight_keywords: true, " _
                & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
                & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"			
            %>
            <!-- STOP Reports table -->
            
            <!-- Fire table build -->
            <script language="javascript" type="text/javascript">
                //<![CDATA[
                var tableProp = {<% =tableProperty %>};
                //initiate table setup
                var tf1 = setFilterGrid("activityTable",tableProp);
                //]]>
            </script>

            <!-- Shadow table -->
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td height="5px" style="background:#D0D0D0;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
              </tr>
            </table>
        
        </td>
    	<!-- STOP Main Middle Section -->

    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
  	</tr>
</table>
<!-- STOP Page -->

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

