﻿/*

Minimum required HTML for this class:

<div id="[container_id]">
	<div id="[list_id]">
	</div>
</div>

Optional HTML:

<div id="[container_id]">
	Some text displaying the count of: <span id="counter"></span> item(s) in the list.
	<div id="[list_id]">
	<div id="zeroCaption">Text displayed when there are 0 items in the list. This will be hidden when items exist.</div>
	</div>
</div>

*/

CheckList.instances = new Array;

/*

@e = the HTML DOM element of the actual list ([list_id] in the HTML notes above)
@isCheckList = true builds class as a CHECK list. false or null builds as a simple list (no checkboxes)
*/

function CheckList(e)
{
	
	this.container = e;
	this.list = this.container.getElementsByTagName("div")[0]; //get the first div - the list itself
	
	// add it to the collection
	
	CheckList.instances[CheckList.instances.length] = this;
	
	this.createShortCuts();
	
}

CheckList.findInstanceByName = function(s)
{
	for (var i = 0; i < CheckList.instances.length; i++)
		if (CheckList.instances[i].container.id == s)
			return CheckList.instances[i];
}

CheckList.prototype.enable = function()
{
	for (nodes = this.list.getElementsByTagName("input"), i = nodes.length-1; i >= 0; --i)
		nodes[i].disabled = false;
}

CheckList.prototype.disable = function()
{
	for (nodes = this.list.getElementsByTagName("input"), i = nodes.length-1; i >= 0; --i)
		nodes[i].disabled = true;
}
CheckList.prototype.selectAll = function()
{
	for (nodes = this.list.getElementsByTagName("input"), i = nodes.length-1; i >= 0; --i)
		if (!nodes[i].disabled)
			nodes[i].checked = true;
}

CheckList.prototype.selectNone = function()
{
	for (nodes = this.list.getElementsByTagName("input"), i = nodes.length-1; i >= 0; --i)
		if (!nodes[i].disabled)
			nodes[i].checked = false;
}

CheckList.prototype.createShortCuts = function()
{
	var sc = document.createElement("div");
		sc.id = this.container.ic + "_shortcuts";
		
	sc.appendChild(document.createTextNode("Select: "));

	var a = document.createElement("a");
		a.setAttribute("href", "javascript:CheckList.findInstanceByName('" + this.container.id + "').selectAll();");
		a.appendChild(document.createTextNode("All"));
		
	sc.appendChild(a);
	sc.appendChild(document.createTextNode(" | "));
	
	var a = document.createElement("a");
		a.setAttribute("href", "javascript:CheckList.findInstanceByName('" + this.container.id + "').selectNone();");
		a.appendChild(document.createTextNode("None"));
	
	sc.appendChild(a);
	
	this.container.appendChild(sc);
}

CheckList.prototype.addItem = function(value, caption)
{
	var d = document.createElement("div");
		d.id = this.list.id + "_item_" + value.toString();
		d.className = "checkItem";
		
	var c = document.createElement("input");
		c.id = this.list.id + "_input_" + value;
		c.setAttribute("type", "checkbox");
		c.setAttribute("name", this.list.id);
		c.setAttribute("value", value);
		
	var l = document.createElement("label");
		l.setAttribute("for", c.id);
		l.appendChild(document.createTextNode(caption));
	
	d.appendChild(c);
	d.appendChild(l);
	
	this.list.appendChild(d);
}

CheckList.prototype.removeItem = function(value)
{
	for (nodes = this.list.getElementsByTagName("input"), i = nodes.length-1; i >= 0; --i)
		if (nodes[i].value == value)
			nodes[i].parentNode.removeChild(nodes[i]);
}

CheckList.prototype.selectItem = function(value)
{
	for (nodes = this.list.getElementsByTagName("input"), i = nodes.length-1; i >= 0; --i)
		if (nodes[i].value == value)
			nodes[i].checked = true;
}
