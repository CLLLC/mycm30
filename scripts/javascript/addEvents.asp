﻿<script type="text/javascript">
//  addIssues.js is used to start new issues depending on the user's access
//  to customer profiles and the number of profiles their group is 
//  associated with.
//
//	Required items: profileCount = comes from _INCsecurity.asp
//					sCustomerID = current user's associated customerID
//					cLng(session(session("siteID") & "adminLoggedOn")) = security cookie at login

//translate keycode and fire function
function keyListen(e) {
	var keycode = e.keyCode;
	var profileCount = '<% =profileCount %>';
	var pageView = '<% =pageView %>';
	var homePageKey = '<% =session(session("siteID") & "homepagekey") %>';

	//HOME key pressed --  moves through buttons
	if(keycode == '36') {
		if (homePageKey=="Y") {
			if (document.frm) {
				if(isFormChanged(document.frm)==true) {		
					confirmPrompt('Cancel Changes','Changes have been made on this page and <strong>must<br>be saved</strong> before continuing.<br><br>Continue and cancel changes?','redirect','../default.asp');
				}
				else {
					window.location = '../default.asp';
				}
			} else {
				window.location = '../default.asp';
			}
		}		
	}

	//RIGHT ARROW key pressed --  moves through buttons
	if(keycode == '39' && pageView == 'issue:add') {
		if (document.getElementById('customerID')) nextButton('forward');
	}

	//LEFT ARROW key pressed --  moves through buttons
	if(keycode == '37' && pageView == 'issue:add') {
		if (document.getElementById('customerID')) nextButton('backward');
	}

	//ESCAPE key pressed --  WORKS JUST NEED TO TEST A LITTLE MORE...
	if(keycode == '27') {				
		//SimpleModal.close();
		//killkeydown(e);
	}
	
	//F2 key pressed -- NEW ISSUE
	if(keycode == '113') {
		<% if cLng(session(session("siteID") & "adminLoggedOn")) <= 2 or addNewIssue="Y" then %>
			//User only has access to one (1) profile
			if (profileCount <= '1') {
				addIssue('<% =sCustomerID %>','<% =cLng(session(session("siteID") & "adminLoggedOn")) %>')					
			}			
			//User only has access to more than one (1) profile
			else if (profileCount > '1') {
				SimpleModal.open('../_dialogs/popup_profile.asp', 430, 700, 'no');		
			}
		<% end if %>		
	}

	//F3 key pressed -- PROFILE SNAPSHOT
	if(keycode == '114' && (pageView == 'issue:add' || pageView == 'issue:00')) {				
		SimpleModal.open('../_dialogs/popup_profile_snapshot.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 475, 500, 'no'); <% =extraClick %>
		killkeydown(e);
	}

	//F4 key pressed -- DIRECTIVES
	if(keycode == '115' && (pageView == 'issue:add' || pageView == 'issue:00')) {				
		SimpleModal.open('../_dialogs/popup_directive_table.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 500, 625, 'no'); <% =extraClick %>
		killkeydown(e);
	}

	//F5 key pressed -- NOTHING
	if(keycode == '116' && (pageView == 'issue:add' || pageView == 'issue:00')) {
		<% if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>
			//kill so operator doesn't accedently lose issue
			killkeydown(e);
		<% end if %>
	}

	//F6 key pressed -- USERS
	if(keycode == '117' && (pageView == 'issue:add' || pageView == 'issue:00')) {				
		SimpleModal.open('../_dialogs/popup_user_table.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 500, 700, 'no'); <% =extraClick %>
		killkeydown(e);
	}

	//F7 key pressed -- LOCATIONS
	if(keycode == '118' && (pageView == 'issue:add' || pageView == 'issue:00')) {				
		SimpleModal.open('../_dialogs/popup_location_table.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 525, 700, 'no'); <% =extraClick %>
		killkeydown(e);
	}

	//F8 key pressed -- NEW REPORT
	if(keycode == '119') {
		<% if cLng(session(session("siteID") & "adminLoggedOn")) <= 2 or addNewFollowUp="Y" then %>
			addFollowUp('<% =crsid %>');
		<% end if %>		
	}

	//F9 key pressed -- NEW REPORT
	if(keycode == '120') {
		window.location = '../datasets/default.asp';
	}
		
}

//used to stop default F fuction in browsers
function killkeydown(e) {	
	//kill keypress in IE
	if(window.event) {
		event.keyCode = 0;		
		event.returnValue = false;
	}
	//kill keypress all other browsers
	else {
		e.preventDefault();
		e.returnValue = false;
	}
}
	
//get event -- this watches key event
function callkeydownhandler(evnt) {
   ev = (evnt) ? evnt : event;
   keyListen(ev);
}

//initiate listener
if (window.document.addEventListener) {
   window.document.addEventListener("keydown", callkeydownhandler, false);
} else {
   window.document.attachEvent("onkeydown", callkeydownhandler);
}

//start new issue --  dependent on profile selected and security rights
function addIssue(customerID,security) {
	if (security < 10) {
		window.location = '../scripts/issues_add.asp?cid='+customerID+'&action=add';
	}
	else {
		window.location = '../scripts/<% =session(session("siteID") & "issuesEdit") %>?cid='+customerID+'&action=add';
	}
}

//start follow-up
function addFollowUp(crsid) {
	jPrompt('Enter Issue #:', crsid, 'Add Follow-Up', function(r) {
		if( r ) { 
			//check for properly formated CRSID with a 3rd "-" hypon 1101-aaa-10001[-]01
			if( r.substr(r.length-3,1) != "-" ) {
				r += "-01";
			}
			//navigate to new follow-up
			window.location = '../scripts/<% =session(session("siteID") & "issuesEdit") %>?action=addfu&recId='+r;
		}
	});
}

</script>

