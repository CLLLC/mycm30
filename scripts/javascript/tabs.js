﻿function clickTopTab(topTab,sideTab) {
	// this function cycle through tabs on top of page
	// changing the style and changing the visibility
	// of all DIV's that have a name containing the phrase "tab:"
	var obj = document.getElementById(topTab);
	var p = obj.parentNode;

	// cycle through all children and change style
	for (var i = 0; i < p.childNodes.length - 1; i++) {
		// tab selected, change style to "on"
		if (p.childNodes[i].id == topTab) {
			p.childNodes[i].className = "on";
		}
		// tab not selected, change style to "off"
		else {
			if (p.childNodes[i].id != "end") 
				p.childNodes[i].className = "off";
		}
	}

	// hide ALL DIV elements whose name contains "tab:"
	var doc = document.getElementsByTagName('div'); 
	for (var i=0; i < doc.length; i++) { 		
		if (doc[i].id.indexOf('tab:') > -1){
			doc[i].style.display = "none";
		}
	}	

	// turn on this DIV, must use .display = '' and NOT 
	// .display = 'block' to retain 100% width in FF
	document.getElementById('tab:' + topTab).style.display = '';
	
	// used to redirect user back after save...make sure it exists first
	if (document.getElementById('topTab')) {
		document.getElementById('topTab').value = topTab;				
		
		// make sure there is a side tab, then set
		if (document.getElementById('sideTab')) {
			document.getElementById('sideTab').value = sideTab;							
			
			//if (topTab=="details") {
			//	document.getElementById('sideTab').value = "general";	
			//}
			//else if (topTab=="notes") {
			//	document.getElementById('sideTab').value = "information";	
			//}
		}
		
		
	}	

//	alert(document.getElementById('topTab').value);
//	alert(document.getElementById('sideTab').value);
	
	
}

function clickSideTab(topTab, sideTab) {
	// this function hides all tables within the docutment that 
	// have a name containing the phrase "table:"
	// it also changes the currently selected side tab

	//make sure a sideTab was provided
	if (sideTab) {
		
		// turn off ALL side tabs
		var obj = document.getElementById(sideTab);
		var p = obj.parentNode;
		var nodes = p.getElementsByTagName("div");	
		for (var i = 0; i < nodes.length; i++)
		{
			//turn off all div classes but the last one "empty"
			if (nodes[i].className != "empty") 
				nodes[i].className = "off";	
			//reactivate the "resolution" side tab to yellow
			if (nodes[i].id == "resolution") 
				nodes[i].className = "highlight";			
		}		
		// turn on this side tab
		obj.className = "on"; 	
	
		// hide ALL tables whose name contains "table:"
		var doc = document.getElementsByTagName('table'); 
		for (var i=0; i < doc.length; i++)
		{ 		
			if (doc[i].id.indexOf('table:' + topTab + ':') > -1){
				doc[i].style.display = "none";
			}
		}		
		
		// turn on this table, must use .display = '' and NOT 
		// .display = 'block' to retain 100% width in FF
		document.getElementById('table:' + topTab + ':' + sideTab).style.display = '';	
	
		// used to redirect user back after save...make sure it exists first
		if (document.getElementById('topTab')) {
			document.getElementById('topTab').value = topTab;	
			// make sure there is a side tab, then set
			if (document.getElementById('sideTab')) {
				document.getElementById('sideTab').value = sideTab;	
			}
		}

	}
	
}

function menuSideRollover(obj,status) {
	// for changing background color on 
	// tabMenu on side of every page

	if (obj.className != "sideMenuOn")
	{
		if (status == 'on')
		{
			obj.style.background = '#EFF2F7';
		}
		else
		{
			obj.style.background = '#FFFFFF';
		}
	}
}

function setSideMenuCookie(value) {
	//used to set current side menu cookie
	//app passes what side menu was just clicked
	var c_name = "myCM3-SideMenu"
	var exdate = new Date();
	var expiredays = 1
	exdate.setDate(exdate.getDate()+expiredays);
	document.cookie=c_name+ "=" +escape(value)+
	((expiredays==null) ? "" : ";expires="+exdate.toUTCString());
}

function getSideMenuCookie() {
	//used to retrieve current side menu cookie
	var c_name = "myCM3-SideMenu";
	if (document.cookie.length>0) {
	  c_start=document.cookie.indexOf(c_name + "=");
	  if (c_start!=-1)
		{
		c_start=c_start + c_name.length+1;
		c_end=document.cookie.indexOf(";",c_start);
		if (c_end==-1) c_end=document.cookie.length;
		return unescape(document.cookie.substring(c_start,c_end));
		}
	  }
	return "";
}

function setSideMenu() {
	// this function sets all classNames for all DIV's 
	// that have a ID containing the phrase "sidemenu:"
	var c = getSideMenuCookie();
	var p = document.getElementById("sidemenu:");
	var nodes = p.getElementsByTagName("div");
	// cycle through all DIVs and set class
	for (var i=0; i < nodes.length; i++)
	{ 		
		if (nodes[i].id.indexOf('sidemenu:') > -1){
			if (nodes[i].id == c) {
				nodes[i].className = "sideMenuOn";
			}
			else {
				nodes[i].className = "sideMenuOff";				
			}
		}
	}		
}

