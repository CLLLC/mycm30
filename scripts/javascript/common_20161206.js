// ********************************************************************
// Product  : MyComplianceManagement, Pro, Lite, Metrics
// Version  : 3.0
// Modified : September 2010
// Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
//            You are NOT authorized to copy or distribute any part  
//            of this code! If you have any questions regarding this
//	     website, please contact support@ccius.com
// ********************************************************************
function setCookie(name, value, expires, path, domain, secure) {
  var curCookie = name + "=" + escape(value) +
      ((expires) ? "; expires=" + expires.toGMTString() : "") +
      ((path) ? "; path=" + path : "") +
      ((domain) ? "; domain=" + domain : "") +
      ((secure) ? "; secure" : "");
  	document.cookie = curCookie; 
}

function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    } else {
        begin += 2;
    }
    var end = document.cookie.indexOf(";", begin);
    if (end == -1) {
        end = dc.length;
    }
    return unescape(dc.substring(begin + prefix.length, end));
}

function setChartCookie() {	
	var obj = document.getElementById("checkChart"); //checkbox element holding current value
	var value

	if (obj.checked == true) {
		obj.checked = false;
		value = "";			//value stored in cookie and checkbox
	}
	else {
		obj.checked = true;
		value = "checked";	//value stored in cookie and checkbox
	}	
		
	//used to set current side menu cookie
	//app passes what side menu was just clicked
	var c_name = "myCM3-Charts"
	var exdate = new Date();
	var expiredays = 1
	exdate.setDate(exdate.getDate()+expiredays);
	document.cookie=c_name+ "=" +escape(value)+
	((expiredays==null) ? "" : ";path=/;expires="+exdate.toUTCString());
}

function setCrystalCookie() {	
	var objFrom = document.getElementById("fromDate"); //checkbox element holding current value
	var objTo = document.getElementById("toDate"); //checkbox element holding current value
	var value
	//assign dates to value for storing
	value = objFrom.value + "*|*" + objTo.value;
	//used to set current side menu cookie
	//app passes what side menu was just clicked
	var c_name = "myCM3.0-Crystal"
	var exdate = new Date();
	var expiredays = 1
	exdate.setDate(exdate.getDate()+expiredays);
	document.cookie=c_name+ "=" +escape(value)+
	((expiredays==null) ? "" : ";expires="+exdate.toUTCString());
}

function destroyAllCookies() {	
	var cookies = document.cookie.split(";");   
	var exdate = new Date();
	exdate.setDate(exdate.getDate() - 1);
	//cycle through all known cookies
	for (var i=0; i<cookies.length; i++){
		var pair = cookies[i].split("=");			
		//leave the Remember Me cookies alone
		if (unescape(pair[0]) != "myCM3-UsernameCookie" && unescape(pair[0]) != "myCM3-RememberMeCookie") {	
			setCookie(unescape(pair[0]), "", exdate, "/");
		}		
	}   	
}

function CheckAll(obj) {
  // Used to check or uncheck on checkboxes found on grid lists
  var chk = obj.checkAll.checked;
  var len = obj.elements.length;
  // cycle through all elements, if checkbox then check it
  for(var i=0; len >i; i++) {
	var elm = obj.elements[i];
	if (elm.type == "checkbox")
	{
		elm.checked = chk;
	}
  }
}

function fieldCheckList(frm,obj,option) {
  // Used to check or uncheck on checkboxes found on list boxes
  // in Issue Lists and Report forms
	var elm = document[frm].elements[obj]; 
 	for (var i=0; i <elm.length; i++) { 
	  	elm[i].checked = option
 	} 
}

function deleteSelected(obj,itemType) {		 
	// Used to confirm user wants to delete selected grid items, then submit form
	var bln;
	var frm = document.getElementById(obj);
	// cycle through all form elements, if checkbox is it checked?
	for(i=0; i<frm.elements.length; i++)
	{
		if(frm.elements[i].type=="checkbox")
		{
			if (frm.elements[i].checked)
			{
				bln = true;
			}
		}
	}
	// if no checkboxes are checked, tell user
	if (!bln)
	{
		jAlert("No " + itemType + " were selected.", "myCM Alert");		
		return;
	}
	// if checked checkbox found, confirm with user to delete/submit form
	jConfirm("Delete the selected " + itemType + "?<br/>This action cannot be undone.", "myCM Alert", function(r) {
		if (r==true) {
			frm.submit();
		}
		else {
			return false;
		}			
	})
			
			
}

function popup(url,winWidth,winHeight) {
	//used to pop open dialog windows (i.e. popup_category.asp)
  	var h=Math.min(winHeight,screen.height-50);
  	window.open(url,'popup','width='+winWidth+',height='+h+',toolbar=0,scrollbars=1,left=10,top=10,statusbar=1,menubar=0,resizable=1');
  	//window.open(url,'popup');
  	return false;
}

function selectAllOptions(obj,setOpt) {
	//used on Option/Select lists to either de/select ALL options
	var el = document.getElementById(obj);
	//if list is disabled, don't do anything		
	if (el.disabled){
		return;
	}
	//cycle through and set selection option
	if (el.options){
		for(var i = 0; i < el.options.length; i++) {
			el.options[i].selected = setOpt;
		}
	}
}
	
function removeOption(obj) {
	//used to remove all selected items in an Option/Select list
	var el = document.getElementById(obj);
  	var selIndex = el.selectedIndex;
	if (selIndex != -1) {
		for(i=el.length-1; i>=0; i--)
		{
		  if(el.options[i].selected)
		  {
			el.options[i] = null;
		  }
		}
	}
	else {
		alert("No options selected.")
	}
}

function selectAll(obj){
	//used to select all item in a list
	var el = document.getElementById(obj);
	
	if (el.disabled){
		return;
	}
	
	if (el.options){
		for(var i = 0; i < el.options.length; i++) {
			el.options[i].selected = true;
		}
	}
}

function selectNone(obj){
	//used to de-select all item in a list
	var el = document.getElementById(obj);	
	if (el.options){
		for(var i = 0; i < el.options.length; i++) {
			el.options[i].selected = false;
		}
	}
}


function focusField(name) {
  for(i=0; i < document.forms.length; ++i) {
    var obj = document.forms[i].elements[name];
    if (obj && obj.focus) {obj.focus();}
  }
}

function selectField(name) {
  for(i=0; i < document.forms.length; ++i) {
    var obj = document.forms[i].elements[name];
    if (obj && obj.focus){obj.focus();}
    if (obj && obj.select){obj.select();}
  }
}

function limitField(name, maxlimit) {
  // HTML Example: <textarea name=rptSummary onKeyPress="formFldTrunc(this,249)">text here...</textarea>
  for(i=0; i < document.forms.length; ++i) {
    var obj = document.forms[i].elements[name];
    if (obj) {
      if (obj.value.length > maxlimit) {
        obj.value = obj.value.substring(0, maxlimit);
      }
    }
  }
}

function limitFieldById(id, maxlimit) {
  if (document.getElementById) {
    var obj = document.getElementById(id);
    if (obj && (obj.value.length > maxlimit)) {
      obj.value = obj.value.substring(0, maxlimit);
    }
  }
}

function getStyleObject(objectId) {
  // cross-browser function to get an object's style object given its id
  if(document.getElementById && document.getElementById(objectId)) {
    // W3C DOM
    return document.getElementById(objectId).style;
  } else if (document.all && document.all(objectId)) {
    // MSIE 4 DOM
    return document.all(objectId).style;
  } else if (document.layers && document.layers[objectId]) {
    // NN 4 DOM.. note: this won't find nested layers
    return document.layers[objectId];
  } else {
    return false;
  }
}

function getObject(objectId) {
  // cross-browser function to get an object given its id
  // If you need to change the style of an object or its display/visibility
  // use one of the other methods in this template
  // This function won't work for NN 4 DOM ==> Only use internally
  if(document.getElementById && document.getElementById(objectId)) {
    // W3C DOM
    return document.getElementById(objectId);
  } else if (document.all && document.all(objectId)) {
    // MSIE 4 DOM
    return document.all(objectId);
  } else {
    return false;
  }
}

function changeObjectDisplay(objectId, newDisplay) {
  // get a reference to the cross-browser style object and make sure the object exists
  var styleObject = getStyleObject(objectId);
  if(styleObject) {
    styleObject.display = newDisplay;
    return true;
  } else {
    // we couldn't find the object, so we can't change its visibility
    return false;
  }
}

function setOnOff(id1, id2) {
  changeObjectDisplay(id1, 'block');
  changeObjectDisplay(id2, 'none');
}

// cross platform equiv. to document.getElementById
function gGetElementById(s) {
  var o = (document.getElementById ? document.getElementById(s) : document.all[s]);
  return o == null ? false : o;
}

// simple hide/display
function toggleVisibility(id) {
  var o = gGetElementById(id);
  if (o != null) {
    if (o.style.display == 'none')
      o.style.display = '';
    else
      o.style.display = 'none';
  }
}
	
function isGoodBrowser() {
  var ua=navigator.userAgent;
  var isGood=0;
  if ((ua.indexOf("MSIE 5")!=-1)
      || (ua.indexOf("MSIE 6")!=-1)
      || (ua.indexOf("Mozilla/5")!=-1) )
  {
    if (ua.indexOf("Opera")==-1){
      isGood = 1;
    }
  }
  return isGood;
}

function isBoolean(a) {
    return typeof a == 'boolean';
}


function isString(a) {
    return typeof a == 'string';
}

function confirmAction(objLabel,objAction) {	
  // Used to change the form element ACTION to objAction 
  // Example: Survey.asp uses actions bulkPause, bulkResume, and bulkDelete
  // HTML Example: <input type=submit name=Pause id=Pause value="Pause" onClick="return confirmAction('Are you sure...','bulkPause')">
  document.all.action.value = objAction
  var agree=confirm(objLabel);
  if (agree)			
	return true ;
  else
	return false ;
}

function confirmSubmit(confirmMsg) {
  var agree=confirm(confirmMsg);
  if (agree)
  	return true ;
  else
    return false ;
  }



function confirmPrompt(title,msg,action,url,form) {

	jConfirm(msg, title, function(r) {
		if (r==true) {
			//submit form
			if (action=="submit") {
				//stuff...
			}			
			//redirect page
			else if (action=="redirect") {				
				window.location = url;
			}
		}
		else {
			return false;
		}
	});

}
  

function fillTextArea(objArea,objValue){
	if(document.all[objValue].selectedIndex != 0) {
		document.all[objArea].value = document.all[objValue].value
	}
}		

function configImage(img,action){
	// Used to switch images on labels/options that can be edited
	// by users (User Defined Fields, Status, Awareness)
	if (action == "over") {
		document.getElementById(img).src="../_images/icons/16/bullet_wrench.png";
	}
	else {
		document.getElementById(img).src="../_images/x_cleardot_16.gif";
	}
}


function replaceCarriageReturn(stringWithCarriage,replaceWith)
	//Miscelaneous script to remove line breaks and carriage returns.
	{
	stringWithCarriage = escape(stringWithCarriage); //encode all characters in text area to find carriage return character
	
	for(i=0; i < stringWithCarriage.length; i++) {
		//loop through string, replacing carriage return encoding with HTML break tag
		if(stringWithCarriage.indexOf("%0D%0A") > -1) {
			//Windows encodes returns as \r\n hex
			stringWithCarriage=stringWithCarriage.replace("%0D%0A",replaceWith)
		}
		else if(stringWithCarriage.indexOf("%0A") > -1) {
			//Unix encodes returns as \n hex
			stringWithCarriage=stringWithCarriage.replace("%0A",replaceWith)
		}
		else if(stringWithCarriage.indexOf("%0D") > -1) {
			//Macintosh encodes returns as \r hex
			stringWithCarriage=stringWithCarriage.replace("%0D",replaceWith)
		}
	}
	stringWithCarriage=unescape(stringWithCarriage) //decode all characters in text area back

	return stringWithCarriage;
}

function addOptionsItems(obj,value,text){
	// Create an Option object and add new values to it
	var opt = document.createElement("option");
    // Add an Option object to Drop Down/List Box
    document.getElementById(obj).options.add(opt);
    opt.text = text;
    opt.value = value;
}


function CurrentDate(){
  var currentDate = new Date();
  var day = currentDate.getDate();
  var month = currentDate.getMonth() + 1; //Jan = 0 and Dec = 11
  var year = currentDate.getFullYear();
  //send back current date
  return month + "/" + day + "/" + year;
}

function CurrentTime(){
  var currentTime = new Date();
  var hours = currentTime.getHours();
  var minutes = currentTime.getMinutes();
  var seconds = currentTime.getSeconds();
  var suffix = "AM";
  
  if (hours >= 12) {
  suffix = "PM";
  hours = hours - 12;
  }
  if (hours == 0) {
  	hours = 12;
  }
  if (minutes < 10)
  minutes = "0" + minutes
  
  //send back current time AM/PM
  return hours + ":" + minutes + ":" + seconds + " " + suffix;
}

/**
* Convert number of bytes into human readable format
*
* @param integer bytes     Number of bytes to convert
* @param integer precision Number of digits after the decimal separator
* @return string
*/
function bytesToSize(bytes, precision)
{   
    var kilobyte = 1024;
    var megabyte = kilobyte * 1024;
    var gigabyte = megabyte * 1024;
    var terabyte = gigabyte * 1024;
    
    if ((bytes >= 0) && (bytes < kilobyte)) {
        return bytes + ' B';
 
    } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
        return (bytes / kilobyte).toFixed(precision) + ' Kb';
 
    } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
        return (bytes / megabyte).toFixed(precision) + ' Mb';
 
    } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
        return (bytes / gigabyte).toFixed(precision) + ' Gb';
 
    } else if (bytes >= terabyte) {
        return (bytes / terabyte).toFixed(precision) + ' Tb';
 
    } else {
        return bytes + ' B';
    }
}

