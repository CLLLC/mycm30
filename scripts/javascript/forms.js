﻿function resetSystemMsg(obj) {
	//used to reset/hide update/error message box
	var msg = document.getElementById(obj);
	if (msg != null) {
		msg.style.display = "none";
	}
	//remove the second 2nd one if visible
	var msg = document.getElementById(obj + '_2');
	if (msg != null) {
		msg.style.display = "none";
	}
}

function formChanged(form) {
	//used to check and see if form elements changed at all
	//this is used to alert user changes have been made

	//cycle through all form elements
	for (var i = 0; i < form.elements.length; i++) {
		//element "select"...this one was not fully tested
		if (form.elements[i].type == "select") {
			if(form.elements[i].selected != form.elements[i].defaultSelected) {
				//change made, inform user
				return(true); 
		  	}
	  	}	  
		//element "select-one"  
	  	else if (form.elements[i].type == "select-one") {				
			var list = form.elements[form.elements[i].name]
			//cycle through list to see if change was made
			for(var x=0; x <list.options.length; ++x) {
				  if(list.options[x].selected != list.options[x].defaultSelected) {
					//change made, inform user
					return(true); 
				  }
			}
	  	} 
	  	//if element type is "select-multiple"...this was not fully tested
		//may need to add loop through, but for now hidden fields are tracking changes
		else if (form.elements[i].type == "select-multiple") { 
		  if(form.elements[i].selected != form.elements[i].defaultSelected) {
				//change made, inform user
				return(true); 
		  }
	  	} 
	  	//element "radio"
	  	else if (form.elements[i].type == "radio") { 			
			var radio = form.elements[form.elements[i].name]
			//cycle through radio options looking for change
			for (var x=0; x <radio.length; x++) { 
				if (radio[x].checked != radio[x].defaultChecked) { 
					//change made, inform user
					return(true); 
				}
			} 
	  	} 
	  	//element checkbox
	  	else if (form.elements[i].type == "checkbox") { 
			var radio = form.elements[form.elements[i].name]
			for (var x=0; x <radio.length; x++) { 
				if (radio[x].checked != radio[x].defaultChecked) { 
					//change made, inform user
					return(true);
				}
			} 
	  	} 
	  	//all other elements
		else {
			if(form.elements[i].value != form.elements[i].defaultValue) {
				//change made, inform user
				return(true); 
		  	}
	  	}
	
	} 
  	//no change made to form!
  	return(false); 
} 

//**************************************************************
// Used to flag TinyMCE instance as dirty. Must have hidden
// field dirtyTinyMCE somewhere on form
//**************************************************************
function setTinyMCEdirty() {
	if (document.getElementById('dirtyTinyMCE')) {
		var editor = document.getElementById('dirtyTinyMCE');
		editor.value = "dirty";
	}
}


//**************************************************************
// Form validation functions checking for >0 length and format
//**************************************************************
//TURNED OFF FOR NOW....NOTHING WRONG JUST TRYING TO SPEED UP SITE
function validateFormOnSubmit(theForm) {
	// used for form validation --> if a field ID contains "required:"
	// then it must be populated before the form will be submitted
	
	var reason = "";
	// cycle through all fields in form
	for(i=0; i<theForm.elements.length; i++)
	{		
		
		// validate empty fields
		if (theForm.elements[i].id.indexOf('required:') > -1){
			reason += validateField(theForm.elements[i]);
		}
		
		// validate date format
		if (theForm.elements[i].id.indexOf('date:') > -1){
			reason += validateFormat(theForm.elements[i],"date");
		}
		
		// validate time format
		if (theForm.elements[i].id.indexOf('time:') > -1){
			reason += validateFormat(theForm.elements[i],"time");
		}
		
	}
	// if error, inform user and do NOT submit form
	if (reason != "") {
		// remove duplicate entries
		if (reason.match("-Some required fields have not been filled in.")) {
			reason = reason.replace(/-Some required fields have not been filled in.\n/gi, "");
			reason = reason + "-Some required fields have not been filled in.";			
		}
		// inform user of errors						
		alert("Errors found!\n\n" + reason);
    	return false;
  	}
	// all good, submit form
	return true;
}

function validateField(fld) {
	//used to validate field based on type
    var error;
	var p;
	var errMsg = "-Some required fields have not been filled in.\n";
	
	// check length of text field	
	if (fld.type == "text") {
		if (fld.value.length == 0) {			
			error = false;
		} else { error = true; }
	}
	
	// check radio "checked" status	
	if (fld.type == "radio") {
		error = checkRadio(fld.name);
	}

	// find parent of field, then <td>, then <tr>
	p = fld.parentNode;
	p = p.parentNode;
	p = p.parentNode;

	// set background color and notices
	if (error == false) {			
		p.style.background = '#FFFFB3'; 
		error = errMsg;						
	} else {
		error = "";
		p.style.background = ''; 			
	}

	return error;
	
}


function validateFormat(fld, format) {
	// used to validate field based on format
    var error;
	var p;
	var dateMsg = "-Invalid Date format found (mm/dd/yyyy). \n";
	var timeMsg = "-Invalid Time format found.\n";
	
	// check date format	
	if (format == "date") {
		error = checkDate(fld);
	}

	// check date format	
	if (format == "time") {
		error = checkTime(fld);
	}

	// find parent of field, then <td>, then <tr>
	p = fld.parentNode;
	p = p.parentNode;
	p = p.parentNode;

	// set background color and notices
	if (error == false) {
		p.style.background = '#FFFFB3'; 
		
		if (format == "date") {
			error = dateMsg;
		}
		else if (format == "time") {
			error = timeMsg;
		}						
		
	} else {
		// don't reset if it's already yellow
		if (p.style.background != '#FFFFB3') {
			error = "";
			p.style.background = ''; 			
		}
	}

	return error;
	
}


function checkRadio(field) { 
	// used to check if radio button was selected
	var radios = document["frm"].elements[field]; 
 	for (var i=0; i <radios.length; i++) { 
	  	if (radios[i].checked) { 
   			return true; 
  		} 
 	} 
 	return false; 
}


function checkDate(field) {
  	// Date validation functions
    var allowBlank = true;
    var minYear = 1902;
    var maxYear = (new Date()).getFullYear();

    var errorMsg = "";
	
    // regular expression to match required date format
    re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
    
    if(field.value != '') {
      if(regs = field.value.match(re)) {
        if(regs[2] < 1 || regs[2] > 31) {
          errorMsg = "Invalid value for day: " + regs[2];
        } else if(regs[1] < 1 || regs[1] > 12) {
          errorMsg = "Invalid value for month: " + regs[1];
        } else if(regs[3] < minYear || regs[3] > maxYear) {
          errorMsg = "Invalid value for year: " + regs[3] + " - must be between " + minYear + " and " + maxYear;
        }
      } else {
        errorMsg = "Invalid date format: " + field.value + " (mm/dd/yyyy)";
      }
    } else if(!allowBlank) {
      errorMsg = "Empty date not allowed!";
    }
    
    if(errorMsg != "") {
      return false;
    }
	
    return true;
}
  

function checkTime(field) {
  	// Date validation functions "12:12pm"  
    var errorMsg = "";

    // regular expression to match required time format
    re = /^(\d{1,2}):(\d{2})(:00)?([ap]m)?$/;
    
    if(field.value != '') {
      if(regs = field.value.match(re)) {
        if(regs[4]) {
          // 12-hour time format with am/pm
          if(regs[1] < 1 || regs[1] > 12) {
            errorMsg = "Invalid value for hours: " + regs[1];
          }
        } else {
          // 24-hour time format
          if(regs[1] > 23) {
            errorMsg = "Invalid value for hours: " + regs[1];
          }
        }
        if(!errorMsg && regs[2] > 59) {
          errorMsg = "Invalid value for minutes: " + regs[2];
        }
      } else {
        errorMsg = "Invalid time format: " + field.value;
      }
    }

    if(errorMsg != "") {
      return false;
    }
    
    return true;
}

// ************************************************************************
// ************************************************************************
// * Returns true if any field in the form has changed. 
// ************************************************************************
// ************************************************************************
function isFormChanged(form) { 
	//fields to ignore during save..not sure what "slcPages_" is...
	var ignorFields = "topTab,sideTab,slcPages_subjectTable,slcPages_resolutionTable";
    var isChanged = false ; 	
    for (var i = 0; i< form.length; i++) {
		//ignore tab hidden elements		
		if(ignorFields.indexOf(form.elements[i].id)<=-1) {
			//setup case challenge
			switch(form.elements[i].type) { 
				case "text" : 
					isChanged =  isTextChanged(form.elements[i]) ; 
					break; 				
				case "hidden" :
					isChanged =  isTextChanged(form.elements[i]) ; 
					break; 
				case "select-one" :                 
					isChanged =  isSelectChangedOne(form.elements[i]) ;             
					break; 
				case "select-multiple" :                 
					isChanged =  isSelectChangedMul(form.elements[i]) ;             
					break; 
				case "radio" :     
					isChanged =  isRadioChanged(form.elements[i]) ;  
					break; 
				case "checkbox" :     
					isChanged =  isCheckChanged(form.elements[i]) ;  
					break; 
				case "textarea" :     
					isChanged =  isTextAreaChanged(form.elements[i]) ;  
					break; 
			}
			//change found STOP code
			if (isChanged) break;
		}		
	} 	
	
	//FOR TESTING...
	//alert(form.elements[i].id);
	
	//send back findings	
    return isChanged;
} 

// ************************************************************************ 
// * Returns true if the select-one value has changed, else returns false.  
// ************************************************************************ 
function isSelectChangedOne(selectbox) {
	var len = selectbox.options.length ; 
    var defaultIndex = -1 ; 
    for (var i = 0 ; i < len ; i++) {
		if (selectbox.options[i].defaultSelected) {
			defaultIndex = i ; 
            break; 
        }       
	} 
    if (selectbox.size <= 1) {
		if (defaultIndex == -1 && selectbox.selectedIndex == 0) return false; 
        	return (defaultIndex == selectbox.selectedIndex ) ? false : true ; 
	}   
	else {
		return (defaultIndex == selectbox.selectedIndex) ? false : true ; 
	}                       
}  
// **************************************************************************** 
// * Returns true if the select-multiple value has changed, else returns false.  
// **************************************************************************** 
function isSelectChangedMul(selectbox) {
	var len = selectbox.options.length ; 
    for (var i = 0 ; i < len ; i++) {
		if (selectbox.options[i].selected != selectbox.options[i].defaultSelected ) {
			return true ; 
        }       
	}  
  return false; 
}
// ********************************************************************** 
// * returns true if the text value has changed, else returns false.  
// ********************************************************************** 
function isTextChanged(textBox) {
	return (textBox.value == textBox.defaultValue) ? false : true ; 
} 
// ****************************************************************************** 
// * Returns true if the radio checked condition has changed, else returns false.  
// ****************************************************************************** 
function isRadioChanged(radioButton) {
	return (radioButton.checked == radioButton.defaultChecked) ? false : true ; 
}
// ***************************************************************************** 
// * returns true if checkbox checked condition has changed, else returns false.  
// ***************************************************************************** 
function isCheckChanged(checkBox) {
	return (checkBox.checked == checkBox.defaultChecked) ? false : true ; 
}  
// ********************************************************************** 
// * returns true if the select value has changed else returns false  
// ********************************************************************** 
function isTextAreaChanged(textAreaField) {
	return (textAreaField.defaultValue == textAreaField.value) ? false : true ; 
}

// ************************************************************************
// ************************************************************************
// * Updates all form default values to current value, save was performed 
// ************************************************************************
// ************************************************************************
function FormSaved(form,clearElements) { 
	var j, hasDefault, opt;
	var ignorFields = "topTab,sideTab";
    var isChanged = false ; 	
    for (var i = 0; i< form.length; i++) {		
		//ignore tab hidden elements		
		if(ignorFields.indexOf(form.elements[i].id)<=-1) {			
			//setup case challenge
			switch(form.elements[i].type) { 
				case "text" :
				case "hidden" :
				case "textarea" :				
					//updateTextDefault(form.elements[i]) ;
					(form.elements[i].value == form.elements[i].defaultValue) ? false : form.elements[i].defaultValue = form.elements[i].value ;					
					break;
				case "select-one" :
				case "select-multiple" :
					//updateSelectDefault(form.elements[i]) ;
					j = 0, hasDefault = false;
					while (opt = form.elements[i].options[j++])
						if (opt.defaultSelected) hasDefault = true;
						j = hasDefault ? 0 : 1;
					while (opt = form.elements[i].options[j++]) 
						if (opt.selected != opt.defaultSelected) opt.defaultSelected = opt.selected;
					break;
				case "radio" :
				case "checkbox" :
					//updateCheckDefault(form.elements[i]) ;
					(form.elements[i].checked == form.elements[i].defaultChecked) ? false : form.elements[i].defaultChecked = form.elements[i].checked ; 
					break;
			}
		}		
	}	
	
	//reset form elements value and defaultvalue
	var clearE = clearElements.split(",");
	for(i=0; i<clearE.length; i++) {
		if (clearE[i]>'') {
			if (document.getElementById(clearE[i])) {
				document.getElementById(clearE[i]).value = '';
				document.getElementById(clearE[i]).defaultValue = '';
			}
		}
	}	

} 
 
// ************************************************************************
// ************************************************************************
// * Strips ALL html tags and strange characters from sent object
// * removes: everything BUT [a-Z 0-9 . ' " carriage returns] etc...
// ************************************************************************
// ************************************************************************
function clean_text(obj) {
	var input = document.getElementById(obj).value;		
	//document.getElementById(obj).value = input.replace( new RegExp("[^a-zA-Z0-9.'-/\r()\"_!@#$%*=+ ]","gm"),"" )		
	document.getElementById(obj).value = input.replace( new RegExp("[^a-zA-Z0-9.'-r()\"_!@#$%*=+ ]","gm"),"" )		
}






