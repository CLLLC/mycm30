<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->

<%
'Database
dim mySQL, cn, rs, rs2

'Needed for CLIVE updates for SHG/CFL -- TEMPORARY for CLIVE UPDATES
dim cliveUserAdded, cliveUserAddedReadOnly

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'validate security access
if loadUser(null) = false then
	response.write("Error: Your session has expired! To resume please <strong><a href=""#"" title=""Login to myCM"" onclick=""SimpleModal.open('../logon_dialog.asp', 243, 395, 'no'); StopTheTimer(); resetSystemMsg('systemMessage');"">Login to myCM</a></strong>.")
	response.end
end if
'*********************************************************
																	
'Issues
dim idIssue
dim idRelated

'fields from DETAILS tab
dim	CRSID
dim customerID
dim rptLogid, rptLogidOld
dim issueType
dim rptModifiedDate
dim	rptDate
dim	rptTime
dim	rptMonth
dim rptQuarter
dim	rptYear
dim	severityLevel, severityLevelOrig
dim	firstTime
dim	commTool
dim	callBack
dim	anonCaller
dim interpreterused
dim language
dim	resGiven
dim	locName, locNameID
dim	locAddr
dim	locCity
dim	locState
dim	locZip
dim locCountry
dim locHierarchy	'used to parse location levels
dim locLevel_1
dim locLevel_2
dim locLevel_3
dim locLevel_4
dim locLevel_5
dim locLevel_6
dim locLevel_7
dim locLevel_8
dim callerType
dim	callerFirst
dim	callerLast
dim callerTitle
dim	callerAddr
dim	callerCity
dim	callerState
dim	callerZip
dim	callerHome
dim	callerWork
dim callerCell
dim callerFax
dim callerOther
dim callerPager
dim callerEmail
dim callerBestTime
dim callerPassword
dim	rptSummary
dim	rptDetails
dim	rptAddendum
dim ownerID

'fields from NOTES tab
dim rptStatus
'dim rptStatusOld
dim rptSource
'dim rptRespGroup
dim rptCaseNotes
dim rptDateClosed
dim rptDateDeadline
'dim rptCaseType
'dim rptInvestigationStatus
'dim rptInvestigationStatusOld
'dim rptInvestigation
'dim rptPrivNotes
dim rptResolution
dim rptResSatisfaction
'dim rptResolutionOld
dim rptResAppDate
dim rptResAppBy
dim rptResApp
'dim rptResAppNew
'dim rptResAppOld
'dim rptMCRAvailable
'dim rptDaysOpen
'dim rptDaysOpenDate
'dim rptDaysOpenDateOld
'dim rptDaysLeftDateInt
'dim rptDaysLeft
dim rptUserField1
dim rptUserField2
dim rptUserField3
dim rptUserField4
dim rptUserField5
dim rptUserField6
dim rptUserField7
dim rptUserField8
dim rptUserField9
dim rptUserField10
dim rptUserField11
dim rptUserField12
dim rptUserField13
dim rptUserField14
dim rptUserField15
dim rptUserField16
dim rptUserField17
dim rptUserField18
dim rptUserField19
dim rptUserField20

dim rptProgress

dim idInvestigation
dim arrInvestigation

dim pCaseStatusCloseOn
dim pInvestigationStatusCloseOn

'OptionsGroups
'dim idOptionGroup
'dim optionGroupDesc
'dim statusMail
'dim statusMailInv
'dim adminEmail
'dim adminEmailInv
'dim adminEmailBody
'dim logidEmail

'dim managerMail
'dim managerName
dim managerEmail
dim managerEmailSubject
dim managerEmailBody
dim tmpEmailBody

dim useInvDeadlineDays


dim mcrAvailable 'TEMP --> should be field "Confidential", SQL would not save at time of demo
dim mcrComments 'TEMP --> should be field "Confidential_Details", SQL would not save at time of demo
dim Caller_Confidential
dim Details_Redacted


'Categories
'dim idCategory
'dim categoryDesc
'dim idParentCategory

'dim idDepartment
'dim idDeptProd

'Categories_Issues
'dim idCatProd

'optionsGroupsXref
'dim idOptGrpProd

'DiscProd
'dim idDiscProd
'dim discAmt
'dim discFromQty
'dim discToQty
'dim discPerc

'IssueGroups
'dim idProdGroup
'dim prodGroupP
'dim prodGroupC

'OptionsProdEx
'dim idOptionsProdEx
'dim idOption

dim emailPrefix : emailPrefix = "<table width=""90%"" border=""0"" align=""center"" cellpadding=""5"" cellspacing=""0""><tr><td height=""20""><span style=""font-family: 'Verdana','sans-serif'; COLOR: black; FONT-SIZE: 7.5pt;"">Please DO NOT reply to this email. Send all correspondence to original sender: <strong>" & session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname") & "</strong>&nbsp;[<a href=mailto:" & email & ">" & email & "</a>]</span></td></tr></table>"
dim emailSuffix : emailSuffix = "<table width=""90%"" border=""0"" align=""center"" cellpadding=""5"" cellspacing=""0""><tr><td><span style=""font-family: verdana,geneva; color: black; font-size: 10px;"">** Please do not reply to this message. If you need assistance visit <a href=""http://support.complianceconcepts.com/""><span style=""color: #0000ff;"">support.complianceconcepts.com</span></a></span></td></tr></table>"

'get action
dim action
action = trim(lCase(Request.Form("action")))
if len(action) = 0 then
	action = trim(lCase(Request.QueryString("action")))
end if
if  action <> "edit" _
and action <> "editaddinv" _
and action <> "del" _
and action <> "dellink" _
and action <> "add" _
and action <> "addfu"  _
and action <> "addinv" _
and action <> "delinv" _
and action <> "bulkdel" _
and action <> "bulkdelall" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if

'get idIssue
if action = "edit"  _
or action = "editaddinv" _
or action = "addfu" _
or action = "del" _
or action = "dellink" _
or action = "addinv" _
or action = "delinv" then

	'ID of issue to work with
	idIssue = trim(Request.Form("idIssue"))
	if len(idIssue) = 0 then
		idIssue = trim(Request.QueryString("recID"))
	end if
	if idIssue = "" then
		'for AJax display
		if action = "edit" then
			response.write("Error: Invalid Issue ID.")
			response.end 
		'all others, redirect
		else
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")		
		end if
	end if

end if


'get Linked issue
if action = "dellink" then

	'ID of issue to work with
	idRelated = trim(Request.Form("recrelated"))
	if len(idRelated) = 0 then
		idRelated = trim(Request.QueryString("recrelated"))
	end if

end if


'Get invID
if action = "delinv" then

	idInvestigation = trim(Request.Form("invID"))
	if len(idInvestigation) = 0 then
		idInvestigation = trim(Request.QueryString("invID"))
	end if
	if idInvestigation = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Investigation ID.")
	end if

end if


'used to redirect user back to tabs they last were on
dim topTab, sideTab
topTab = trim(Request.Form("topTab"))
if len(topTab) = 0 then
	topTab = trim(Request.QueryString("top"))
end if
sideTab = trim(Request.Form("sideTab"))
if len(sideTab) = 0 then
	sideTab = trim(Request.QueryString("side"))
end if


'Get Issue Details
if action = "edit" or action = "editaddinv" or action = "add" or action = "addfu" then

	'get/set customerID...must be from FORM submission only for security -- mandatory
	customerID = trim(Request.Form("customerID"))
	if customerID = "" then
		'for AJax display
		if action = "edit" then
			response.write("Error: Invalid Customer ID.")
			response.end 
		'all others, redirect
		else
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
		end if		
	end if

	'get IssueType (1,2,3 or 4)...must be from FORM submission only for security -- mandatory
	issueType = trim(Request.Form("issueType"))
	if issueType = "" then
		'for AJax display
		if action = "edit" then
			response.write("Error: Invalid Issue Type ID.")
			response.end 
		'all others, redirect
		else
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue Type ID.")
		end if				
	end if

	'get Case Status close on...
	pCaseStatusCloseOn = trim(Request.Form("pCaseStatusCloseOn"))

	'get Investigation Status close on...
	pInvestigationStatusCloseOn = trim(Request.Form("pInvestigationStatusCloseOn"))
	
	'get Owner of this issue
	rptLogid = trim(Request.Form("rptLogid"))

	'flag right now as when this issue was modified
	'''rptModifiedDate = Now()
	
	'Set Date parameters
	rptDate = trim(Request.Form("rptDate"))
	if IsDate(rptDate) = true then
		rptDate = FormatDateTime(rptDate,vbShortDate)
		rptMonth = DatePart("m",rptDate)   '--> this was clever, just dont need right now.... Right("0" & DatePart("m",rptDate),2)
		rptQuarter = DatePart("q",rptDate)
		rptYear = DatePart("yyyy",rptDate)
	end if

	'Set Issue Time
	rptTime = trim(Request.Form("rptTime"))
	if IsDate(rptTime) = true then
		rptTime = FormatDateTime(rptTime,3) 
	end if

	'Get Severity
	severityLevel = trim(Request.Form("severityLevel"))

	rptProgress = trim(Request.Form("rptProgress"))
	if len(rptProgress) <= 0 or rptProgress = "" or isNull(rptProgress) then	
		'MUST BE SET TO 3 in order for the client to see this issue
		rptProgress = 3
	end if


	'Get firstTime
	firstTime = trim(Request.Form("firstTime"))
	if firstTime <> "-1" and firstTime <> "0" then
		firstTime = "0"
	end if

	'Get commTool -- optional
	commTool	= trim(Request.Form("commTool"))

	'Get callBack -- optional
	callBack = trim(Request.Form("callBack"))
	if (len(callBack) > 0 and IsDate(callBack) = False) then
		'for AJax display
		if action = "edit" then
			response.write("Error: Invalid Call Back Date.")
			response.end 
		'all others, redirect
		else
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Call Back Date.")
		end if
	end if
	


	'Get confidentiality -- mandatory
	Caller_Confidential = trim(Request.Form("Caller_Confidential"))
	if Caller_Confidential <> "Y" then
		Caller_Confidential = "N"
	end if

	'Get Details -- optional
	Details_Redacted = trim(Request.Form("Details_Redacted"))
	Details_Redacted = replace(Details_Redacted,"<br />","<br>")



	'Get anonCaller -- mandatory
	anonCaller = trim(Request.Form("anonCaller"))
	if anonCaller <> "-1" and anonCaller <> "0" then
		'for AJax display
		if action = "edit" then
			response.write("Error: Invalid 'Anonymous Caller' Indicator.")
			response.end 
		'all others, redirect
		else
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid 'Anonymous Caller' Indicator.")
		end if
	end if

	'Get Interpreter Used -- mandatory
	interpreterused = trim(Request.Form("interpreterused"))
	if interpreterused <> "-1" and interpreterused <> "0" then
		interpreterused = "0"
	end if

	'Get Language used -- optional
	language = trim(Request.Form("language"))

	'Get resGiven
''	resGiven = trim(Request.Form("resGiven"))
''	if resGiven <> "-1" and resGiven <> "0" then
''		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid 'Resolution Given' Indicator.")
''	end if

	'Get Location Fields -- all optional
	locNameID	= trim(Request.Form("locNameID"))
	locName		= trim(Request.Form("locName"))
	locAddr		= trim(Request.Form("locAddr"))
	locCity		= trim(Request.Form("locCity"))
	locState	= trim(Request.Form("locState"))
	locZip		= trim(Request.Form("locZip"))
	locCountry	= trim(Request.Form("locCountry"))
	
	'Get Caller Fields -- all optional
	callerType 		= trim(Request.Form("callerType"))
	callerFirst		= trim(Request.Form("callerFirst"))
	callerLast		= trim(Request.Form("callerLast"))
	callerTitle		= trim(Request.Form("callerTitle"))
	callerAddr		= trim(Request.Form("callerAddr"))
	callerCity		= trim(Request.Form("callerCity"))
	callerState		= trim(Request.Form("callerState"))
	callerZip		= trim(Request.Form("callerZip"))
	callerHome		= trim(Request.Form("callerHome"))
	callerWork		= trim(Request.Form("callerWork"))
	callerCell 		= trim(Request.Form("callerCell"))
	callerPager 	= trim(Request.Form("callerPager"))
	callerFax 		= trim(Request.Form("callerFax"))
	callerOther 	= trim(Request.Form("callerOther"))
	callerEmail		= trim(Request.Form("callerEmail"))
	callerPassword	= trim(Request.Form("callerPassword"))		
	
	'Get Summary -- optional
	rptSummary	= trim(Request.Form("rptSummary"))
	rptSummary = replace(rptSummary,"<br />","<br>")	
		
	'Get Details -- optional
	rptDetails = trim(Request.Form("rptDetails"))
	rptDetails = replace(rptDetails,"<br />","<br>")
	
	'Get Addendum -- optional
	rptAddendum = trim(Request.Form("rptAddendum"))
	rptAddendum = replace(rptAddendum,"<br />","<br>")	

	'Get Case Notes -- optional
	rptCaseNotes = trim(Request.Form("rptCaseNotes"))

	'Get Report Status -- mandatory (set if not)
	rptStatus = trim(Request.Form("rptStatus"))
	if len(rptStatus) = 0 then rptStatus = "Open"

	'Get Report Source -- optional
	rptSource = trim(Request.Form("rptSource"))

	'Get Close Date -- optional
	rptDateClosed = trim(Request.Form("rptDateClosed"))

	'Get Close Date -- optional
	rptDateDeadline = trim(Request.Form("rptDateDeadline"))
	
	

	'Get User Defined Fields -- all optional
	rptUserField1 	= trim(Request.Form("rptUserField1"))
	rptUserField2 	= trim(Request.Form("rptUserField2"))
	rptUserField3 	= trim(Request.Form("rptUserField3"))
	rptUserField4 	= trim(Request.Form("rptUserField4"))
	rptUserField5 	= trim(Request.Form("rptUserField5"))
	rptUserField6 	= trim(Request.Form("rptUserField6"))
	rptUserField7 	= trim(Request.Form("rptUserField7"))
	rptUserField8 	= trim(Request.Form("rptUserField8"))
	rptUserField9 	= trim(Request.Form("rptUserField9"))
	rptUserField10 	= trim(Request.Form("rptUserField10"))
	rptUserField11 	= trim(Request.Form("rptUserField11"))
	rptUserField12 	= trim(Request.Form("rptUserField12"))
	rptUserField13 	= trim(Request.Form("rptUserField13"))
	rptUserField14 	= trim(Request.Form("rptUserField14"))
	rptUserField15 	= trim(Request.Form("rptUserField15"))
	rptUserField16 	= trim(Request.Form("rptUserField16"))
	rptUserField17 	= trim(Request.Form("rptUserField17"))
	rptUserField18 	= trim(Request.Form("rptUserField18"))
	rptUserField19 	= trim(Request.Form("rptUserField19"))
	rptUserField20 	= trim(Request.Form("rptUserField20"))

	'Get Location Levels -- optional
	dim arrLevel, levelI
	locHierarchy= trim(Request.Form("locHierarchy"))
	if len(locHierarchy) > 0 then
		arrLevel = split(locHierarchy,"*|*")
		'move through all categories selected
		for levelI = LBound(arrLevel) to UBound(arrLevel)
			if len(arrLevel(levelI)) > 0 then
				if levelI = 0 then locLevel_1 = arrLevel(levelI)
				if levelI = 1 then locLevel_2 = arrLevel(levelI)				
				if levelI = 2 then locLevel_3 = arrLevel(levelI)
				if levelI = 3 then locLevel_4 = arrLevel(levelI)
				if levelI = 4 then locLevel_5 = arrLevel(levelI)
				if levelI = 5 then locLevel_6 = arrLevel(levelI)																
				if levelI = 6 then locLevel_7 = arrLevel(levelI)
				if levelI = 7 then locLevel_8 = arrLevel(levelI)								
			end if
		next
	end if


	'Get resolution text -- optional
	rptResSatisfaction = trim(Request.Form("rptResSatisfaction"))

	'Get resolution text -- optional
	rptResolution = trim(Request.Form("rptResolution"))

	'Get Resolution Approved UserID -- optional
	rptResAppBy = trim(Request.Form("rptResAppBy"))		
	if len(rptResAppBy) = 0  then
		rptResAppBy = 0
	end if

	'Get Resolution Approved -- Must equal -1 or 0
	rptResApp = trim(Request.Form("rptResApp"))
	if rptResApp = "0" or len(rptResApp) = 0 or rptResApp = "" then
		rptResApp = 0
	elseif rptResApp = "-1" and len(rptResolution) = 0 then
		'for AJax display
		if action = "edit" then
			response.write("Error: Invalid 'Resolution' Text.")
			response.end 
		'all others, redirect
		else
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid 'Resolution' Text.")		
		end if		
	elseif rptResApp = "-1" and len(rptResolution) > 0 and editCaseResolutionApp = "Y" and rptResAppBy <= 0 then 		
		rptResApp = sLogid
		rptResAppDate = Date()
	elseif rptResApp = "-1" and len(rptResolution) > 0 and rptResAppBy > 0 then 						
		rptResApp = rptResAppBy
	end if
				
				
end if


'Generate CRSID
if action = "add" then

	dim rResetFrequency, rNextReset, lngTrackingNumber
	dim sMonth, sYear, rReset

	'get necessarsy variables to generated CRSID
	'the YEAR helps prevent dup issues if a 1999 year is in database
	mySQL = "SELECT MAX(CRSID) AS LastCRS " _
		  & "FROM   CRS " _
		  & "WHERE  CustomerID = '" & customerID & "' AND IssueTypeID=" & issueType & " AND CRSID Like '%-" & customerID & "-%-01' AND Year>='" & Year(Date())-10 & "'"
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if not rs.eof and len(rs("LastCRS"))>0 then
	
		'determine year
		sYear = mid(rs("LastCRS"),1,2)
		if sYear <> mid(year(Date),3,2)	then
			rReset = true
		end if
		
		'determine month
		sMonth = mid(rs("LastCRS"),3,2)	
		if sMonth <> right("0" & month(Date),2) then
			rReset = true
		end if
		
		'determine rolling number
		if rReset = true then
			lngTrackingNumber = "0001"
		else
			lngTrackingNumber = cLng(mid(rs("LastCRS"), 8 + len(customerID),4)) 'should pull 1101-AAA-1[0001]-01)
			lngTrackingNumber = lngTrackingNumber + 1	
			'add leading zeros
			if Len(lngTrackingNumber) = 1 Then
				lngTrackingNumber = "000" & lngTrackingNumber
			elseif Len(lngTrackingNumber) = 2 Then	
				lngTrackingNumber = "00" & lngTrackingNumber
			elseif Len(lngTrackingNumber) = 3 Then	
				lngTrackingNumber = "0" & lngTrackingNumber
			elseif Len(lngTrackingNumber) = 4 Then	
				lngTrackingNumber = lngTrackingNumber
			end if
		end if
	
		'NEW issue # -- put all the pieces together to create CRSID
		idIssue = mid(year(Date),3,2) & right("0" & month(Date),2) & "-" & uCase(customerID) & "-" & issueType & lngTrackingNumber & "-01"	
	
	'no issues found, add 1st one
	else

		'NEW issue # -- put all the pieces together to create CRSID
		idIssue = mid(year(Date),3,2) & right("0" & month(Date),2) & "-" & uCase(customerID) & "-" & issueType & "0001-01"	
	
	end if
		
end if


'Delete/Add Categories
if action = "edit" or action = "editaddinv" or action = "add" or action = "addfu" then

	'***************************************
	'Add any Categories selected to be added
	'MUST be after CRSID # is generated in 
	'case this is a new issue	
	dim listCategoryAdded
	dim arrAddedCat
	dim addedC

	'discover added categories
	listCategoryAdded = trim(Request.Form("listCategoryAdded"))
	if len(listCategoryAdded) > 0 then
		arrAddedCat = split(listCategoryAdded,",")		
		'move through all categories selected
		for addedC = LBound(arrAddedCat) to UBound(arrAddedCat)
			if len(arrAddedCat(addedC)) > 0 then
				'make sure does NOT already exist so it's not added twice
				mySQL = "SELECT Count(CRSID) AS CatCount FROM CRS_Category " _
					  & "WHERE CRSID = '" & idIssue & "' and CategoryID = " & arrAddedCat(addedC) & " "
				if action = "edit" then
					set rs = openRSexecuteAjax(mySQL)
				else
					set rs = openRSexecute(mySQL)
				end if
				'does NOT exist, go ahead and add
				if rs("CatCount") <= 0 then					
					'add record to table
					mySQL = "INSERT INTO CRS_Category (" _
						  & "CRSID,CategoryID,ModifiedBy,ModifiedDate" _
						  & ") VALUES (" _
						  & "'" & idIssue & "'," & arrAddedCat(addedC) & "," & sLogid & ",'" & Now() & "' " _
						  & ")"
					if action = "edit" then
						set rs = openRSexecuteAjax(mySQL)
					else
						set rs = openRSexecute(mySQL)
					end if
				end if
			end if
		next
	end if

	'***************************************
	'set default severity
	if action = "add" or action = "addfu" then	
		mySQL = "SELECT min(Category.Severity) AS [Severity] " _
	  		  & "	FROM Category INNER JOIN CRS_Category ON Category.CategoryID = CRS_Category.CategoryID "_
			  & "	WHERE CRSID = '" & idIssue & "' "
		'open record
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)		
		severityLevelOrig = severityLevel ' hold for later
		'categories found
		if not rs.eof then
			if len(rs("Severity")) > 0 then
				if cLng(rs("Severity")) > 0 then
					if cLng(rs("Severity")) <> cLng(severityLevel) then
						severityLevel = rs("Severity") ' set NEW severity based on category
					end if
				end if
			end if
		end if
	end if

	'***************************************
	'Remove any Categories selected	
	dim listCategoryRemoved
	dim arrayRemovedCat
	dim removedC

	'Add new users to Case History
	listCategoryRemoved = trim(Request.Form("listCategoryRemoved"))
	if len(listCategoryRemoved) > 0 then
		arrayRemovedCat = split(listCategoryRemoved,",")		
		'move through all users.
		for removedC = LBound(arrayRemovedCat) to UBound(arrayRemovedCat)
			if len(arrayRemovedCat(removedC)) > 0 then

				'UPDATE 1st to get accruate user
				mySQL = "UPDATE CRS_Category SET " _
					  & "	ModifiedBy="	 & sLogid												& ", " _
					  & "	ModifiedDate='"  & Now()												& "' " _
					  & "WHERE CRSID = '" & idIssue & "' AND CategoryID = " & arrayRemovedCat(removedC)
				'update table
				if action = "edit" then
					set rs = openRSexecuteAjax(mySQL)
				else
					set rs = openRSexecute(mySQL)
				end if

				'Delete Record from Categories_Issues
				mySQL = "DELETE FROM CRS_Category " _
					  & "WHERE CRSID = '" & idIssue & "' AND CategoryID = " & arrayRemovedCat(removedC)
				if action = "edit" then
					set rs = openRSexecuteAjax(mySQL)
				else
					set rs = openRSexecute(mySQL)
				end if
				
			end if
		next	
	end if

	'***************************************
	'Update primary Category
	dim primaryCategory, primaryOriginal
	primaryCategory = trim(Request.Form("primaryCategory"))
	primaryOriginal = trim(Request.Form("primaryOriginal"))
	'primary was provided
	if len(primaryCategory) > 0 then
		'there was already one
		if len(primaryOriginal) > 0 then
			'the original and new DONT match...so update
			if primaryCategory <> primaryOriginal then
				'update record in table
				mySQL = "UPDATE CRS_Category SET " _
					  & "	[Primary]='Y', " _					  
					  & "	ModifiedBy="		& sLogid	& ", " _
					  & "	ModifiedDate='"		& Now()		& "' " _
					  & "WHERE  CRSID = '" & idIssue & "' " _ 					  
					  & "AND    CategoryID = " & primaryCategory & " "		  		  		  		    
				if action = "edit" then
					set rs = openRSexecuteAjax(mySQL)
				else
					set rs = openRSexecute(mySQL)
				end if
				'update record in table
				mySQL = "UPDATE CRS_Category SET " _
					  & "	[Primary]='', " _
					  & "	ModifiedBy="		& sLogid	& ", " _
					  & "	ModifiedDate='"		& Now()		& "' " _					  
					  & "WHERE  CRSID = '" & idIssue & "' " _ 
					  & "AND    CategoryID = " & primaryOriginal & " "		  		  		  		    
				if action = "edit" then
					set rs = openRSexecuteAjax(mySQL)
				else
					set rs = openRSexecute(mySQL)
				end if
			end if
		'there was NO ORIGINAL...update new
		else
			'update record in table
			mySQL = "UPDATE CRS_Category SET " _
				  & "	[Primary]='Y', " _
				  & "	ModifiedBy="		& sLogid	& ", " _
				  & "	ModifiedDate='"		& Now()		& "' " _				  
				  & "WHERE  CRSID = '" & idIssue & "' " _ 
				  & "AND    CategoryID = " & primaryCategory & " "		  		  		  		    
			if action = "edit" then
				set rs = openRSexecuteAjax(mySQL)
			else
				set rs = openRSexecute(mySQL)
			end if
		end if
			
	'no primary provided
	elseif len(primaryCategory) <= 0 then
		if len(primaryOriginal) > 0 then
			'update record in table
			mySQL = "UPDATE CRS_Category SET " _
				  & "	[Primary]='', " _
				  & "	ModifiedBy="		& sLogid	& ", " _
				  & "	ModifiedDate='"		& Now()		& "' " _				  
				  & "WHERE  CRSID = '" & idIssue & "' " _ 
				  & "AND    CategoryID = " & primaryOriginal & " "		  		  		  		    
			if action = "edit" then
				set rs = openRSexecuteAjax(mySQL)
			else
				set rs = openRSexecute(mySQL)
			end if
		end if

	end if

	'***************************************
	'Add any investigations based on Categories selected	
	if action = "editaddinv" then	
	
		dim catName
		dim catRS
		dim listCategoryInvAdd, arrCategory, catI


		'get deadline day default settings
		mySQL = "SELECT InvDeadlineDays " _
			  & "FROM   Customer_IssueType " _
			  & "WHERE  CustomerID = '" & customerID & "' AND IssueTypeID=" & issueType
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		if not rs.eof then
			useInvDeadlineDays = rs("InvDeadlineDays")
		end if

	
		listCategoryInvAdd = trim(lCase(Request.Form("listCategoryInvAdd")))
		'make sure categories were selected to add based on
		if len(listCategoryInvAdd) > 0 then
			arrCategory = split(listCategoryInvAdd,",")			
			'move through all categories selected
			for catI = LBound(arrCategory) to UBound(arrCategory)		
				if len(arrCategory(catI)) > 0 then
					'get category name to use as investigation name
					mySQL = "SELECT Category.Name, Category.Sub " _
						  & "FROM CRS_Category AS a INNER JOIN Category ON a.CategoryID = Category.CategoryID " _
						  & "WHERE Category.CustomerID='" & customerID & "' AND a.CRSID='" & idIssue & "' AND Category.CategoryID=" & arrCategory(catI) & " AND " _
						  & "	NOT (Exists (SELECT c.CategoryID " _
						  & "			  FROM Investigation c " _
						  & "			 WHERE a.CRSID = c.CRSID AND Category.CategoryID = c.CategoryID)) "
					set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
					if not rs.eof then
						if len(rs("sub")) > 0 then
							catName = rs("name") & " [" & rs("sub") & "]"	
						else
							catName = rs("name")	
						end if
					else
						'do nothing			
					end if
				
					'add investigation
					if len(catName) > 0 then
					
						'add new category to investigations table...
						mySQL = "INSERT INTO Investigation (" _
							  & "CustomerID,CRSID,CategoryID,Name,Status,DateAdded,ModifiedBy,ModifiedDate"
							  if len(useInvDeadlineDays) > 0 then mySQL = mySQL & ",DateDeadline"
						mySQL = mySQL & ") VALUES (" _
							  & "'" & uCase(customerID) & "'," & "'" & idIssue & "'," & arrCategory(catI) & ",'" & replace(catName,"'","''") & "','Active','" & Date() & "'," & sLogid & ",'" & Now() & "'"
							  if len(useInvDeadlineDays) > 0 then mySQL = mySQL & ",'" & dateAdd("d",cLng(useInvDeadlineDays),Date()) & "'"
						mySQL = mySQL & ")"
						
						'execute update
						set rs = openRSexecute(mySQL)
						
					end if
				end if								
			'move to next category			
			next
	
		end if
		
	end if

end if


'Delete/Add Users
if action = "edit" or action = "editaddinv" or action = "add" or action = "addfu" then

	'***************************************
	'Get global FROM EMAIL address for emails
	'	-this is necessary as some email servers see the FROM address as user@client.com
	'	 and the sending domain as "ccius.com" and consider it SPOOFING and block the email.	
	'***************************************	
	mySQL = "SELECT appSendFromEmail FROM Customer " _
		  & "	WHERE Customer.CustomerID='" & customerID & "' "
	if action = "edit" then
		set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	else
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	end if
	if len(rs("appSendFromEmail")) > 0 then emailFrom = rs("appSendFromEmail") else emailFrom = email

	'***************************************
	'Add any Users/Logins selected to be added
	'MUST be after CRSID # is generated in 
	'case this is a new issue	
	dim listUserAdded
	dim arrAddedUser
	dim addedU
	dim emailUserAdded	
	dim defaultView
	
	'discover added users
	listUserAdded = trim(Request.Form("listUserAdded"))
	if len(listUserAdded) > 0 then

		'-----------------------------------		
		'add newly assigned users to issue
		'-----------------------------------		
		arrAddedUser = split(listUserAdded,",")
		'move through all users selected
		for addedU = LBound(arrAddedUser) to UBound(arrAddedUser)
			if len(arrAddedUser(addedU)) > 0 then
				'make sure does NOT already exist so it's not added twice
				mySQL = "SELECT Count(CRSID) AS UserCount FROM CRS_Logins " _
					  & "WHERE CRSID = '" & idIssue & "' and LogID = " & arrAddedUser(addedU) & " "
				if action = "edit" then
					set rs = openRSexecuteAjax(mySQL)
				else
					set rs = openRSexecute(mySQL)
				end if

				'does NOT exist, go ahead and add
				if rs("UserCount") <= 0 then					

					'get default view (full or read-only)
					mySQL = "SELECT Max(Groups.ReadOnly) AS DefaultView " _
						  & "FROM Groups INNER JOIN Logins_Groups ON Groups.GroupID = Logins_Groups.GroupID " _
						  & "WHERE Logins_Groups.LOGID=" & arrAddedUser(addedU)
					if action = "edit" then
						set rs = openRSexecuteAjax(mySQL)
					else
						set rs = openRSexecute(mySQL)
					end if
					defaultView = trim(rs("DefaultView"))
				
					'add record to table
					mySQL = "INSERT INTO CRS_Logins (" _
						  & "CustomerID,CRSID,LogID,ReadOnly,ModifiedBy,ModifiedDate" _
						  & ") VALUES (" _
						  & "'" & uCase(customerID) & "','" & idIssue & "'," & arrAddedUser(addedU) & ",'" & defaultView & "'," & sLogid & ",'" & Now() & "' " _
						  & ")"						  
					if action = "edit" then
						set rs = openRSexecuteAjax(mySQL)
					else
						set rs = openRSexecute(mySQL)
					end if
					
					'array for sending emails to new users
					emailUserAdded = emailUserAdded & arrAddedUser(addedU) & ","
										
				end if			
			end if
		next
		
		'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx		
		'just for Sun Health/SHG 999 -- TEMPORARY for CLIVE UPDATES
		'if uCase(customerID) = "SHG" or uCase(customerID) = "CFL" then

		'	mySQL = "SELECT CRS_CoordinatorID FROM CRS_Logins INNER JOIN Logins ON CRS_Logins.LOGID = Logins.LOGID WHERE CRS_Logins.CRSID='" & idIssue & "' AND ReadOnly IS NULL "
		'	if action = "edit" then
		'		set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		'	else
		'		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		'	end if			
		'	'make sure a user was found
		'	if not rs.eof then				
		'		do until rs.eof
		'			cliveUserAdded = cliveUserAdded & "*|*" & rs("CRS_CoordinatorID") & "*|*"
		'			rs.movenext
		'		loop
		'	end if
		'	'clean up string and double delimiters
		'	cliveUserAdded = Replace(cliveUserAdded,"*|**|*","*|*")				
		'	
		'	closeRS(rs)

		'	mySQL = "SELECT CRS_CoordinatorID FROM CRS_Logins INNER JOIN Logins ON CRS_Logins.LOGID = Logins.LOGID WHERE CRS_Logins.CRSID='" & idIssue & "' AND ReadOnly='Y' "
		'	if action = "edit" then
		'		set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		'	else
		'		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		'	end if			
		'	'make sure a user was found
		'	if not rs.eof then				
		'		do until rs.eof
		'			cliveUserAddedReadOnly = cliveUserAddedReadOnly & "*|*" & rs("CRS_CoordinatorID") & "*|*"
		'			rs.movenext
		'		loop
		'	end if
		'	'clean up string and double delimiters
		'	cliveUserAddedReadOnly = Replace(cliveUserAddedReadOnly,"*|**|*","*|*")				

		'	closeRS(rs)
			
		'	'assign new values to UDFs 19 & 20
		'	rptUserField19 = cliveUserAdded
		'	rptUserField20 = cliveUserAddedReadOnly
			
		'end if
		'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx		
		
		
		'-----------------------------------		
		'find users who want email notice
		'-----------------------------------	
		addedU = 0
		managerEmail = ""		
		arrAddedUser = split(emailUserAdded,",") 'those users added above
		for addedU = LBound(arrAddedUser) to UBound(arrAddedUser)		
			if len(arrAddedUser(addedU)) > 0 then
				mySQL = "SELECT LastName, Email FROM Logins Where LogID=" & arrAddedUser(addedU) & " AND EmailAssignment='Y' AND EmailIssueTypes Like '%" & issueType & "%' "				
				if action = "edit" then
					set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
				else
					set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
				end if
				'make sure a user was found
				if not rs.eof then
					if len(rs("EMail")) > 0 then
						managerEmail = managerEmail & ";" & rs("Email")
					end if
				end if
			end if			
		next

		'-----------------------------------		
		'send email to newly assigned users
		'-----------------------------------				
		if len(managerEmail) > 0 then
			'clean up
			if left(managerEmail,1) = ";" then managerEmail = right(managerEmail,len(managerEmail)-1)
				
			'subject
			managerEmailSubject = idIssue & ": Issue Received"
							
			'get email template
			mySQL = "SELECT Template FROM Email_Template WHERE Event='CaseManagerEmail' AND CustomerID='" & customerID & "'"
			if action = "edit" then
				set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
			else
				set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
			end if

			if rs.eof then
				if action = "edit" then
					response.write("Error: There is a error with the e-mail template necessary to send updates.")
					response.end
				else
					response.redirect "../error/default.asp?errMsg=" & server.URLEncode("There is a error with the e-mail template necessary to send updates.")
				end if
			end if
			
			managerEmailBody = rs("Template")			
			closeRS(rs)
			
			'set replacement tags
			managerEmailBody = Replace(managerEmailBody,"|CRS.Name|",idIssue)
			managerEmailBody = Replace(managerEmailBody,"|Logins.Email|","myCM User")
			managerEmailBody = Replace(managerEmailBody,"|URL.Link|","<a href='https://www.mycompliancemanagement.com/default.asp?redirect=" & idIssue & "'>Link to issue</a>")			
			managerEmailBody = Replace(managerEmailBody,"|CRS.Summary|",rptSummary)
			managerEmailBody = Replace(managerEmailBody,"|CRS.Date|",rptDate & " " & rptTime)
			managerEmailBody = Replace(managerEmailBody,"|CRS.Severity|",severityLevel)
			managerEmailBody = Replace(managerEmailBody,"|CRS.LocationName|",locName)

			'different FROM address than user logged so add prefix
			if lCase(email) <> lCase(emailFrom) then
				managerEmailBody = Replace(managerEmailBody,"<body>","<body>" & emailPrefix)		
			end if
			'add disclaimer suffix
			managerEmailBody = Replace(managerEmailBody,"</body>",emailSuffix & "</body>")

			' used to clean up old values				
			managerEmailBody = Replace(managerEmailBody,"|Email.Prefix|","") 
				
			'log and send email...
			mySQL = "INSERT INTO MailLog (" _
				  & "CustomerID, CRSID, mailDate, fromEmail, toEmail, subject, body, conttype" _
				  & ") VALUES (" _
				  & "'" & uCase(customerID) & "'," _
				  & "'" & idIssue & "'," _
				  & "'" & Now() & "'," _
				  & "'" & emailFrom & "'," _
				  & "'" & managerEmail & "'," _
				  & "'" & replace(managerEmailSubject,"'","''") & "'," _
				  & "'" & replace(managerEmailBody,"'","''") & "'," _
				  & "'" & "HTML" & "'" _
				  & ")"
			if action = "edit" then
				set rs = openRSexecuteAjax(mySQL)
			else
				set rs = openRSexecute(mySQL)
			end if
				
		end if
						
	end if


	'***************************************
	'send email to newly assigned owner
	dim sendOwnerEmail : sendOwnerEmail = False
	
	'make sure there is an owner
	if cLng(rptLogid)>0 then

		if action = "edit" or action = "editaddinv" then
			'find the old/original owner
			mySQL = "SELECT Logid FROM CRS " _
				  & "WHERE CRSID = '" & idIssue & "' "
			set rs = openRSexecuteAjax(mySQL)
			rptLogidOld = rs("logid")
			
			'comparison check old to new
			if len(rptLogidOld) > 0 then	
				if cLng(rptLogid) <> cLng(rptLogidOld) then
					sendOwnerEmail = True
				end if
			'no old, but there is a new
			else
				sendOwnerEmail = True
			end if
			
		elseif (action = "add" or action = "addfu") and cLng(sLogid) <> cLng(rptLogid) then
			sendOwnerEmail = True		
		end if
						
		'go ahead and send away
		if sendOwnerEmail = True then
		
			mySQL = "SELECT LastName, Email FROM Logins Where LogID=" & rptLogid & " AND EmailAssignment='Y' AND EmailIssueTypes Like '%" & issueType & "%' "
			if action = "edit" then
				set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
			else
				set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
			end if
			'make sure a user was found
			if not rs.eof then
				if len(rs("EMail")) > 0 then
					managerEmail = rs("Email")
				end if
			end if
		
			'make sure there is someone to send too
			if len(managerEmail) > 0 then
			
				'subject
				managerEmailSubject = idIssue & ": Ownership Assigned"
										
				'get email template
				mySQL = "SELECT Template FROM Email_Template WHERE Event='CaseOwnerEmail' AND CustomerID='" & customerID & "'"					
				if action = "edit" then
					set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
				else
					set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
				end if

				if rs.eof then
					if action = "edit" then
						response.write("Error: There is a error with the e-mail template necessary to send updates.")
						response.end
					else
						response.redirect "../error/default.asp?errMsg=" & server.URLEncode("There is a error with the e-mail template necessary to send updates.")
					end if
				end if
				
				managerEmailBody = rs("Template")
				closeRS(rs)
				
				'set replacement tags
				managerEmailBody = Replace(managerEmailBody,"|CRS.Name|",idIssue)
				managerEmailBody = Replace(managerEmailBody,"|Logins.Email|",managerEmail)
				managerEmailBody = Replace(managerEmailBody,"|URL.Link|","<a href='https://www.mycompliancemanagement.com/default.asp?redirect=" & idIssue & "'>Link to issue</a>")
				managerEmailBody = Replace(managerEmailBody,"|CRS.Summary|",rptSummary)
				managerEmailBody = Replace(managerEmailBody,"|CRS.Date|",rptDate & " " & rptTime)
				managerEmailBody = Replace(managerEmailBody,"|CRS.Severity|",severityLevel)
				managerEmailBody = Replace(managerEmailBody,"|CRS.LocationName|",locName)
			
				'different FROM address than user logged so add prefix
				if lCase(email) <> lCase(emailFrom) then
					managerEmailBody = Replace(managerEmailBody,"<body>","<body>" & emailPrefix)		
				end if
				'add disclaimer suffix
				managerEmailBody = Replace(managerEmailBody,"</body>",emailSuffix & "</body>")
	
				' used to clean up old values				
				managerEmailBody = Replace(managerEmailBody,"|Email.Prefix|","") 
				
				'log and send email...
				mySQL = "INSERT INTO MailLog (" _
					  & "CustomerID, CRSID, mailDate, fromEmail, toEmail, subject, body, conttype" _
					  & ") VALUES (" _
					  & "'" & uCase(customerID) & "'," _
					  & "'" & idIssue & "'," _
					  & "'" & Now() & "'," _
					  & "'" & emailFrom & "'," _
					  & "'" & managerEmail & "'," _
					  & "'" & replace(managerEmailSubject,"'","''") & "'," _
					  & "'" & replace(managerEmailBody,"'","''") & "'," _
					  & "'" & "HTML" & "'" _
					  & ")"
				if action = "edit" then
					set rs = openRSexecuteAjax(mySQL)
				else
					set rs = openRSexecute(mySQL)
				end if

			end if
							
		end if
		
	end if


	'***************************************
	'Set any Users Permissions selected	
	dim listUserPermission
	dim arrayPermissionUser
	dim changedU, currentUser

	'Add new users to Case History
	listUserPermission = trim(Request.Form("listUserPermission"))
	if len(listUserPermission) > 0 then
		arrayPermissionUser = split(listUserPermission,",")		
		'move through all users.
		for changedU = LBound(arrayPermissionUser) to UBound(arrayPermissionUser)
			if len(arrayPermissionUser(changedU)) > 0 then
				'remove any commas "," from current string
				currentUser = replace(arrayPermissionUser(changedU),",","")			
				'update record in table
				mySQL = "UPDATE CRS_Logins SET " _
					  & "ReadOnly = '"   & right(currentUser,1) & "'," _
					  & "ModifiedBy="		& sLogid			& ", " _
					  & "ModifiedDate='"    & Now()				& "' " _					  
					  & "WHERE  CRSID = '" & idIssue & "' " _ 
					  & "AND    LogID = " & left(currentUser,len(currentUser)-2)		  		  		  		    
				if action = "edit" then
					set rs = openRSexecuteAjax(mySQL)
				else
					set rs = openRSexecute(mySQL)
				end if
			end if
		next	
	end if

	'***************************************
	'Remove any Users selected	
	dim listUserRemoved
	dim arrayRemovedUser
	dim removedU

	'Add new users to Case History
	listUserRemoved = trim(Request.Form("listUserRemoved"))
	if len(listUserRemoved) > 0 then
		arrayRemovedUser = split(listUserRemoved,",")		
		'move through all users.
		for removedU = LBound(arrayRemovedUser) to UBound(arrayRemovedUser)
			if len(arrayRemovedUser(removedU)) > 0 then
				
				'UPDATE 1st to get accruate user
				mySQL = "UPDATE CRS_Logins SET " _
					  & "	ModifiedBy="	 & sLogid												& ", " _
					  & "	ModifiedDate='"  & Now()												& "' " _
					  & "WHERE CRSID = '" & idIssue & "' AND LogID = " & arrayRemovedUser(removedU)
				'update table
				if action = "edit" then
					set rs = openRSexecuteAjax(mySQL)
				else
					set rs = openRSexecute(mySQL)
				end if
				
				'delete record from table
				mySQL = "DELETE FROM CRS_Logins " _
					  & "WHERE CRSID = '" & idIssue & "' AND LogID = " & arrayRemovedUser(removedU)
				if action = "edit" then
					set rs = openRSexecuteAjax(mySQL)
				else
					set rs = openRSexecute(mySQL)
				end if
				
				
			end if
		next	
	end if

end if


'send STATUS changed email
if action = "edit" then

	managerEmail = ""
	managerEmailSubject = ""
	managerEmailBody = ""
		
	'get old/current status value
	mySQL = "SELECT Status FROM CRS Where CRSID='" & idIssue & "' "
	set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	
	'check if status has changed
	if (rs("Status") <> rptStatus) then

		'-----------------------------------------
		'set Close Date for issue
		if (rptStatus = pCaseStatusCloseOn) then
			rptDateClosed = Date()
		end if
		'-----------------------------------------

		mySQL = "SELECT Logins.Email " _
			  & "FROM Logins INNER JOIN CRS_Logins ON Logins.LOGID = CRS_Logins.LOGID " _
			  & "WHERE CRS_Logins.CRSID='" & idIssue & "' AND Logins.EmailStatusChange='Y' AND Email<>'nothing@nothing.com' "
		set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		do until rs.eof
			managerEmail = managerEmail & ";" & rs("Email")
			rs.movenext
		loop

		'send email
		if len(managerEmail) > 0 then
		
			'clean up
			if left(managerEmail,1) = ";" then managerEmail = right(managerEmail,len(managerEmail)-1)
			
			'subject
			managerEmailSubject = idIssue & ": Issue Status - " & rptStatus

			'get email template
			mySQL = "SELECT Template FROM Email_Template WHERE Event='CaseStatusEmail' AND CustomerID='" & customerID & "'"
			set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

			if rs.eof then
				response.write("Error: There is a error with the e-mail template necessary to send updates.")
				response.end
			end if

			managerEmailBody = rs("Template")
			closeRS(rs)
 
			'set replacement tags			
			managerEmailBody = Replace(managerEmailBody,"|CRS.Name|",idIssue)
			managerEmailBody = Replace(managerEmailBody,"|CRS.Status|",rptStatus)
			managerEmailBody = Replace(managerEmailBody,"|Logins.Email|","myCM User")
			managerEmailBody = Replace(managerEmailBody,"|URL.Link|","<a href='https://www.mycompliancemanagement.com/default.asp?redirect=" & idIssue & "'>Link to issue</a>")			
			managerEmailBody = Replace(managerEmailBody,"|CRS.Summary|",rptSummary)
			managerEmailBody = Replace(managerEmailBody,"|CRS.Date|",rptDate & " " & rptTime)
			managerEmailBody = Replace(managerEmailBody,"|CRS.Severity|",severityLevel)
			managerEmailBody = Replace(managerEmailBody,"|CRS.LocationName|",locName)
			
			'different FROM address than user logged so add prefix
			if lCase(email) <> lCase(emailFrom) then
				managerEmailBody = Replace(managerEmailBody,"<body>","<body>" & emailPrefix)		
			end if
			'add disclaimer suffix
			managerEmailBody = Replace(managerEmailBody,"</body>",emailSuffix & "</body>")

			' used to clean up old values				
			managerEmailBody = Replace(managerEmailBody,"|Email.Prefix|","") 

			'log and send email...
			mySQL = "INSERT INTO MailLog (" _
				  & "CustomerID, CRSID, mailDate, fromEmail, toEmail, subject, body, conttype" _
				  & ") VALUES (" _
				  & "'" & uCase(customerID) & "'," _
				  & "'" & idIssue & "'," _
				  & "'" & Now() & "'," _
				  & "'" & emailFrom & "'," _
				  & "'" & managerEmail & "'," _
				  & "'" & replace(managerEmailSubject,"'","''") & "'," _
				  & "'" & replace(managerEmailBody,"'","''") & "'," _
				  & "'" & "HTML" & "'" _
				  & ")"
			set rs = openRSexecuteAjax(mySQL)
			
		end if
	
	end if		

end if


'Update/Save INVESTIGATIONS xxxxx
if action = "edit" then

	dim invX
	dim rptInvManager
	dim rptInvCategory
	dim rptInvName
	dim rptInvStatus
	dim rptInvDateAdded	
	dim rptInvDeadline
	dim rptInvCloseDate
	dim rptAction
	dim rptSubstantiated
	dim rptRiskLevel
	dim rptInvDiary
	dim rptInvOutcome
	dim arrInvStatusChanged
	dim arrInvManagerChanged
	dim rptInvField1
	dim rptInvField2
			
	'discover string of investigations to work with... "5,7,8,9,"
	arrInvestigation = trim(Request.Form("arrInvestigation"))
	if len(arrInvestigation) > 0 then
		arrInvestigation = split(arrInvestigation,",")		
		'move through all categories selected
		for invX = 0 to UBound(arrInvestigation)
			if len(arrInvestigation(invX)) > 0 then

				'discover investigations where STATUS has changed ''' with status set to Complete
				mySQL = "SELECT Status, LOGID FROM Investigation " _
					  & "WHERE CustomerID='" & customerID & "' AND InvestigationID=" & arrInvestigation(invX) & " "
				set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
				
				'get due date
				rptInvDateAdded = trim(Request.Form("rptInvDateAdded" & invX))

				'get due date
				rptInvDeadline = trim(Request.Form("rptInvDeadline" & invX))

				'get close date
				rptInvCloseDate = trim(Request.Form("rptInvCloseDate" & invX))
				
				'check for status changes
				if len(trim(Request.Form("rptInvStatus" & invX))) > 0 and (trim(Request.Form("rptInvStatus" & invX)) <> rs("Status")) then

					'set Close Date for investigation
					if (trim(Request.Form("rptInvStatus" & invX)) = pInvestigationStatusCloseOn) then
						rptInvCloseDate = Date()
					end if

					'array used to send status change emails
					arrInvStatusChanged = arrInvStatusChanged & "," & arrInvestigation(invX)
																		
				end if
				
				'check for investigator changes, or where no one has yet been assigned and now is
				if len((trim(Request.Form("rptInvManager" & invX)))) > 0 then
					if (cLng(trim(Request.Form("rptInvManager" & invX))) <> rs("LOGID")) or isNull(rs("LOGID")) or rs("LOGID")<0 then
						arrInvManagerChanged = arrInvManagerChanged & "," & arrInvestigation(invX)
					end if
				end if
				
				'Update Record
				mySQL = "UPDATE Investigation SET " _	
					  & "Name='"			& replace(trim(Request.Form("rptInvName" & invX)),"'","''")		& "'," _
					  & "Status='"			& trim(Request.Form("rptInvStatus" & invX)) 					& "'," _
					  & "ActionTaken='"		& trim(Request.Form("rptAction" & invX)) 						& "'," _
					  & "Substantiated='"	& trim(Request.Form("rptSubstantiated" & invX)) 				& "'," _
					  & "RiskLevel='"		& trim(Request.Form("rptRiskLevel" & invX)) 					& "'," _				  
					  & "InvField1='"		& replace(trim(Request.Form("rptInvField1" & invX)),"'","''") 	& "'," _					  
					  & "InvField2='"		& replace(trim(Request.Form("rptInvField2" & invX)),"'","''") 	& "'," _					  
					  & "Notes='"			& replace(trim(Request.Form("rptInvDiary" & invX)),"'","''") 	& "'," _
					  & "Outcome='"			& replace(trim(Request.Form("rptInvOutcome" & invX)),"'","''")	& "'," _
					  & "ModifiedBy="		& sLogid														& ", " _
					  & "ModifiedDate='"    & Now()															& "' "		  					  
				'check for existance of case manager
				if len(trim(Request.Form("rptInvManager" & invX))) > 0 then mySQL = mySQL & ",LOGID=" & trim(Request.Form("rptInvManager" & invX)) & " " else mySQL = mySQL & ",LOGID=null "
				'check for existance of category
			  	if len(trim(Request.Form("rptInvCategory" & invX))) > 0 then mySQL = mySQL & ",CategoryID=" & trim(Request.Form("rptInvCategory" & invX)) & " " else mySQL = mySQL & ",CategoryID=null "
				'check for a date added
				if len(rptInvDateAdded) > 0 then mySQL = mySQL & ",DateAdded='" & trim(rptInvDateAdded) & "' " else mySQL = mySQL & ",DateAdded=null "
				'check for a datedue
				if len(rptInvDeadline) > 0 then mySQL = mySQL & ",DateDeadline='" & trim(rptInvDeadline) & "' " else mySQL = mySQL & ",DateDeadline=null "
				'check for a dateclosed
				if len(rptInvCloseDate) > 0 then mySQL = mySQL & ",DateClosed='" & trim(rptInvCloseDate) & "' " else mySQL = mySQL & ",DateClosed=null "
				'finalize update query
				mySQL = mySQL & "WHERE CustomerID='" & customerID & "' AND InvestigationID=" & arrInvestigation(invX) & " "				
				'update issue
				set rs = openRSexecuteAjax(mySQL)													
				
			end if
		next
	end if


	'--------------------------------------
	'send emails where INV MANAGER changed
	'--------------------------------------
	managerEmail = ""
	managerEmailSubject = ""
	managerEmailBody = ""
	
	'get email template
	mySQL = "SELECT Template FROM Email_Template WHERE Event='InvestigatorEmail' AND CustomerID='" & customerID & "'"
	set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	
	if rs.eof then
		response.write("Error: There is a error with the e-mail template necessary to send updates.")
		response.end
	end if
	
	managerEmailBody = rs("Template")	
	closeRS(rs)

	arrInvManagerChanged = split(arrInvManagerChanged,",")
	
	'move through all Investigation with Investigator change
	for invX = 0 to UBound(arrInvManagerChanged)
		if len(arrInvManagerChanged(invX)) > 0 then
		
			managerEmail = ""
			managerEmailSubject = ""
			tmpEmailBody = managerEmailBody

			mySQL = "SELECT Logins.Email " _
				  & "FROM Logins INNER JOIN Investigation ON Logins.LOGID = Investigation.LOGID " _
				  & "WHERE Logins.EmailInvAssignment='Y' AND Investigation.InvestigationID=" & arrInvManagerChanged(invX) & " AND Email<>'nothing@nothing.com' "
			set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
			do until rs.eof
				managerEmail = rs("Email")
				rs.movenext
			loop

			'send email
			if len(managerEmail) > 0 then					
			
				'get investigation information
				mySQL = "SELECT Investigation.CRSID, Investigation.Name, Category.Name AS [Category], Category.Sub, CRS.Summary " _
					  & "	FROM CRS INNER JOIN (Investigation LEFT JOIN Category ON Investigation.CategoryID = Category.CategoryID) ON CRS.CRSID = Investigation.CRSID " _
					  & "	WHERE Investigation.CustomerID='" & customerID & "' AND Investigation.InvestigationID=" & arrInvManagerChanged(invX) & " "					  
				set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)				

				'subject
				managerEmailSubject = rs("CRSID") & ": Investigation Assignment"
				
				'set replacement tags
				tmpEmailBody = Replace(tmpEmailBody,"|Logins.Email|",managerEmail)
				tmpEmailBody = Replace(tmpEmailBody,"|CRS.Name|",rs("CRSID"))
				tmpEmailBody = Replace(tmpEmailBody,"|Investigation.Name|",rs("Name") & "&nbsp;")
				tmpEmailBody = Replace(tmpEmailBody,"|Investigation.Category|",rs("Category") & "&nbsp;" & rs("Sub"))
				tmpEmailBody = Replace(tmpEmailBody,"|CRS.Summary|",rs("Summary") & "&nbsp;")
				tmpEmailBody = Replace(tmpEmailBody,"|URL.Link|","<a href='https://www.mycompliancemanagement.com/default.asp?redirect=" & rs("CRSID") & "'>Link to issue</a>")				
				tmpEmailBody = Replace(tmpEmailBody,"|CRS.Date|",rptDate & " " & rptTime)
				tmpEmailBody = Replace(tmpEmailBody,"|CRS.Severity|",severityLevel)
				tmpEmailBody = Replace(tmpEmailBody,"|CRS.LocationName|",locName)
							
				closeRS(rs)				

				'different FROM address than user logged so add prefix
				if lCase(email) <> lCase(emailFrom) then
					tmpEmailBody = Replace(tmpEmailBody,"<body>","<body>" & emailPrefix)		
				end if
				'add disclaimer suffix
				tmpEmailBody = Replace(tmpEmailBody,"</body>",emailSuffix & "</body>")
	
				' used to clean up old values				
				tmpEmailBody = Replace(tmpEmailBody,"|Email.Prefix|","") 

				'log and send email...
				mySQL = "INSERT INTO MailLog (" _
					  & "CustomerID, CRSID, mailDate, fromEmail, toEmail, subject, body, conttype" _
					  & ") VALUES (" _
					  & "'" & uCase(customerID) & "'," _
					  & "'" & idIssue & "'," _
					  & "'" & Now() & "'," _
					  & "'" & emailFrom & "'," _
					  & "'" & managerEmail & "'," _
					  & "'" & replace(managerEmailSubject,"'","''") & "'," _
					  & "'" & replace(tmpEmailBody,"'","''") & "'," _
					  & "'" & "HTML" & "'" _
					  & ")"
				set rs = openRSexecuteAjax(mySQL)				
			end if	
	
		end if
	next


	'--------------------------------------
	'send emails where INV STATUS changed
	'--------------------------------------
	managerEmail = ""
	managerEmailSubject = ""
	managerEmailBody = ""
	
	'get email template
	mySQL = "SELECT Template FROM Email_Template WHERE Event='InvestigationStatusEmail' AND CustomerID='" & customerID & "'"	
	set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	if rs.eof then
		response.write("Error: There is a error with the e-mail template necessary to send updates.")
		response.end
	end if

	managerEmailBody = rs("Template")
	closeRS(rs)

	arrInvStatusChanged = split(arrInvStatusChanged,",")
	
	'move through all Investigation with STATUS change
	for invX = 0 to UBound(arrInvStatusChanged)
		if len(arrInvStatusChanged(invX)) > 0 then
		
			managerEmail = ""
			managerEmailSubject = ""
			tmpEmailBody = managerEmailBody
			
			'discover all users assigned to issue
			mySQL = "SELECT Logins.Email " _
				  & "FROM Logins INNER JOIN CRS_Logins ON Logins.LOGID = CRS_Logins.LOGID " _
				  & "WHERE CRS_Logins.CRSID='" & idIssue & "' AND Logins.EmailInvStatusChange='Y' AND Email<>'nothing@nothing.com' "
			set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
			do until rs.eof
				managerEmail = managerEmail & ";" & rs("Email")
				rs.movenext
			loop

			'discover investigator assigned to issue...may not be assigned to actual issue, only investigation (i.e. Follow-Up)
			mySQL = "SELECT Logins.Email " _
				  & "FROM Logins INNER JOIN Investigation ON Logins.LOGID = Investigation.LOGID " _
				  & "WHERE Logins.EmailInvAssignment='Y' AND Investigation.InvestigationID=" & arrInvStatusChanged(invX) & " "
			set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
			do until rs.eof
				'make sure email not already in list
				if instr(managerEmail,rs("Email")) <= 0 then
					managerEmail = managerEmail & ";" & rs("Email")
				end if
				rs.movenext
			loop

			'send email
			if len(managerEmail) > 0 then
			
				'clean up
				if left(managerEmail,1) = ";" then managerEmail = right(managerEmail,len(managerEmail)-1)
		
				'get investigation information
				mySQL = "SELECT Investigation.CRSID, Investigation.Name AS [Name], Investigation.Status, Logins.FirstName, Logins.LastName, Category.Name AS [Category], Category.Sub "_
					  & "	FROM (Investigation LEFT JOIN Logins ON Investigation.LOGID = Logins.LOGID) LEFT " _
					  & "   JOIN Category ON Investigation.CategoryID = Category.CategoryID " _
					  & "	WHERE Investigation.CustomerID='" & customerID & "' AND Investigation.InvestigationID=" & arrInvStatusChanged(invX) & " "
				set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

				'subject
				managerEmailSubject = rs("CRSID") & ": Investigation Status - " & rs("Status")
				
				'set replacement tags
				tmpEmailBody = Replace(tmpEmailBody,"|CRS.Name|",rs("CRSID"))
				tmpEmailBody = Replace(tmpEmailBody,"|Investigation.Status|",rs("Status"))
				tmpEmailBody = Replace(tmpEmailBody,"|Investigation.Name|",rs("Name") & "&nbsp;")
				tmpEmailBody = Replace(tmpEmailBody,"|Logins.Email|","myCM User")
				tmpEmailBody = Replace(tmpEmailBody,"|Investigation.Manager|",rs("FirstName") & "&nbsp;" & rs("LastName") )										
				tmpEmailBody = Replace(tmpEmailBody,"|Investigation.Category|",rs("Category") & "&nbsp;" & rs("Sub"))
				tmpEmailBody = Replace(tmpEmailBody,"|URL.Link|","<a href='https://www.mycompliancemanagement.com/default.asp?redirect=" & rs("CRSID") & "'>Link to issue</a>")								
				tmpEmailBody = Replace(tmpEmailBody,"|CRS.Date|",rptDate & " " & rptTime)
				tmpEmailBody = Replace(tmpEmailBody,"|CRS.Severity|",severityLevel)
				tmpEmailBody = Replace(tmpEmailBody,"|CRS.LocationName|",locName)
								
				closeRS(rs)				

				'different FROM address than user logged so add prefix
				if lCase(email) <> lCase(emailFrom) then
					tmpEmailBody = Replace(tmpEmailBody,"<body>","<body>" & emailPrefix)		
				end if
				'add disclaimer suffix
				tmpEmailBody = Replace(tmpEmailBody,"</body>",emailSuffix & "</body>")
	
				' used to clean up old values				
				tmpEmailBody = Replace(tmpEmailBody,"|Email.Prefix|","") 
						
				'log and send email...
				mySQL = "INSERT INTO MailLog (" _
					  & "CustomerID, CRSID, mailDate, fromEmail, toEmail, subject, body, conttype" _
					  & ") VALUES (" _
					  & "'" & uCase(customerID) & "'," _
					  & "'" & idIssue & "'," _
					  & "'" & Now() & "'," _
					  & "'" & emailFrom & "'," _
					  & "'" & managerEmail & "'," _
					  & "'" & replace(managerEmailSubject,"'","''") & "'," _
					  & "'" & replace(tmpEmailBody,"'","''") & "'," _
					  & "'" & "HTML" & "'" _
					  & ")"
				set rs = openRSexecuteAjax(mySQL)				
			end if	
	
		end if
	next
		
end if


'Update/Save ISSUE QUESTIONS
dim qesX
dim arrQuestion

if action = "edit" then

	'discover string of questions to work with... "5,7,8,9,"
	arrQuestion = trim(Request.Form("arrQuestion"))

	if len(arrQuestion) > 0 then
		arrQuestion = split(arrQuestion,",")
		'move through all questions loaded with issue
		for qesX = 0 to UBound(arrQuestion)
			if len(arrQuestion(qesX)) > 0 then
				'Update Question
				mySQL = "UPDATE CRS_Question SET " _
					  & "	Answer='"		 & replace(trim(Request.Form("rptQuesAnswer" & qesX)),"'","''") & "'," _
					  & "	ModifiedBy="	 & sLogid														& ", " _
					  & "	ModifiedDate='"  & Now()													 	& "' " _
					  & "WHERE CustomerID='" & customerID & "' AND CRSQuestionID=" & arrQuestion(qesX)  	& " "
				'update question
				set rs = openRSexecuteAjax(mySQL)
			end if
		next
	end if

'add ISSUE QUESIONS
elseif action = "add" or action = "addfu" then

	'discover string of questions to work with... "5,7,8,9,"
	arrQuestion = trim(Request.Form("arrQuestion"))

	if len(arrQuestion) > 0 then
		arrQuestion = split(arrQuestion,",")
		'move through all questions loaded with issue
		for qesX = 0 to UBound(arrQuestion)
			if len(arrQuestion(qesX)) > 0 then
				'add Question
				mySQL = "INSERT INTO CRS_Question (" _
					  & "CustomerID,CRSID,QuestionID,Question,Answer,ModifiedBy,ModifiedDate,DateCreated" _
					  & ") VALUES (" _
					  & "'" & uCase(customerID) & "'," & "'" & idIssue & "'," & arrQuestion(qesX) & ",'" & replace(trim(Request.Form("rptQuesName" & qesX)),"'","''") & "','" & replace(trim(Request.Form("rptQuesAnswer" & qesX)),"'","''") & "'," & sLogid & ",'" & Now() & "','" & Now() & "'" _
					  & ")"
				set rs = openRSexecute(mySQL)
			end if
		next		
	end if

end if


'Update/Save SUBJECT & RESOLUTION ENTRIES
dim subX
dim arrSubject, arrResolution

if action = "edit" then

	'discover string of questions to work with... "5,7,8,9,"
	arrSubject = trim(Request.Form("arrSubject"))
	'update existing activities
	if len(arrSubject) > 0 then
		arrSubject = split(arrSubject,",")
		'move through all questions loaded with issue
		for subX = 0 to UBound(arrSubject)
			if len(arrSubject(subX)) > 0 then
				'Update Question
				mySQL = "UPDATE Subject SET " _
					  & "	Name='"		 		& replace(trim(Request.Form("rptSubjectNote_" & arrSubject(subX))),"'","''") 	& "'," _
					  & "	SanctionCheck='" 	& Request.Form("rptSubjectSanction_" & arrSubject(subX))						& "'," _
					  & "	ModifiedBy="	 	& sLogid																		& ", " _
					  & "	ModifiedDate='"  	& Now()														 					& "' " _
					  & "WHERE CRSID='" 		& idIssue & "' AND SubjectID=" & arrSubject(subX)  								& "  "
				'update question
				set rs = openRSexecuteAjax(mySQL)
			end if
		next
	end if

	'discover string of questions to work with... "5,7,8,9,"
	arrSubject = trim(Request.Form("arrSubjectAdded"))
	'add new activities
	if len(arrSubject) > 0 then
		arrSubject = split(arrSubject,",")
		'move through all questions loaded with issue
		for subX = 0 to UBound(arrSubject)
			if len(arrSubject(subX)) > 0 then
				'Add Activity
				mySQL = "INSERT INTO Subject (" _
					  & "CustomerID,CRSID,Name,SanctionCheck,ModifiedBy,ModifiedDate" _
					  & ") VALUES (" _
					  & "'" & uCase(customerID) & "'," & "'" & idIssue & "','" & replace(trim(Request.Form("rptSubjectNote_" & arrSubject(subX))),"'","''") & "','" & Request.Form("rptSubjectSanction_" & arrSubject(subX)) & "'," & sLogid & ",'" & Now() & "'" _
					  & ")"
				set rs = openRSexecuteAjax(mySQL)
			end if
		next
	end if

	'discover string of questions to work with... "5,7,8,9,"
	arrSubject = trim(Request.Form("arrSubjectRemoved"))
	'delete existing activities
	if len(arrSubject) > 0 then
		arrSubject = split(arrSubject,",")
		'move through all questions loaded with issue
		for subX = 0 to UBound(arrSubject)
			if len(arrSubject(subX)) > 0 then
				'Update Question
				mySQL = "DELETE FROM Subject " _
					  & "WHERE CRSID = '" & idIssue & "' AND SubjectID = " & arrSubject(subX)
				set rs = openRSexecuteAjax(mySQL)
			end if
		next
	end if

	'discover string of questions to work with... "5,7,8,9,"
	arrResolution = trim(Request.Form("arrResolutionRemoved"))
	subX = 0
	'delete existing activities
	if len(arrResolution) > 0 then
		arrResolution = split(arrResolution,",")
		'move through all questions loaded with issue
		for subX = 0 to UBound(arrResolution)
			if len(arrResolution(subX)) > 0 then

				'UPDATE 1st to get accruate user
				mySQL = "UPDATE Resolution SET " _
					  & "	ModifiedBy="	 & sLogid					& ", " _
					  & "	ModifiedDate='"  & Now()					& "' " _
					  & "WHERE ResolutionID = " & arrResolution(subX)
				set rs = openRSexecuteAjax(mySQL)

				'Update Question
				mySQL = "DELETE FROM Resolution " _
					  & "WHERE ResolutionID = " & arrResolution(subX)
				set rs = openRSexecuteAjax(mySQL)
				
			end if			
		next
	end if

end if


'add RELATED ISSUES
if action = "addfu" and 1=2 then

	'add follow-up as related issue
	mySQL = "INSERT INTO CRS_Related (" _
		  & "CRSID,RelatedCRSID,ModifiedBy,ModifiedDate" _
		  & ") VALUES (" _
		  & "'" & left(idIssue,len(idIssue)-3) & "-01" & "','" & idIssue & "'," & sLogid & ",'" & Now() & "'" _
		  & ")"
	set rs = openRSexecute(mySQL)

end if


'ADD new issue or follow-up
if action = "add" or action = "addfu" then

	dim fieldsSQL, valueSQL

	'Add Issue with Callback
	fieldsSQL = "INSERT INTO CRS (" _
		  & "crsid,logid,logids,severity,[date],[time],month,quarter,year," _
		  & "customerid,issuetypeid,location_nameid,location_name,location_address," _
		  & "location_city,location_state,location_postalcode,location_country," _
		  & "communicationtool,anonymous,interpreterused,language," _
		  & "firsttimeuser,caller_type,caller_firstname,caller_lastname,caller_title," _
		  & "caller_address,caller_city,caller_state,caller_postalcode," _
		  & "caller_home,caller_work,caller_cell,caller_fax,caller_pager,caller_other, " _
		  & "caller_email,caller_password,summary,details,addendum,casenotes," _
		  & "status,source," _
		  & "location_level_1,location_level_2,location_level_3,location_level_4,location_level_5," _
		  & "location_level_6,location_level_7,location_level_8," _		
		  & "Resolution, ResolutionApprovedBy, ResolutionSatisfaction, " _  
		  & "userfield1,userfield2,userfield3,userfield4,userfield5," _
		  & "userfield6,userfield7,userfield8,userfield9,userfield10, " _
		  & "userfield11,userfield12,userfield13,userfield14,userfield15," _
		  & "userfield16,userfield17,userfield18,userfield19,userfield20, " _
		  & "caller_confidential,details_redacted, " _
		  & "progress,modifiedby,modifieddate,datecreated"

	'check for empty dates	      
	if len(callBack) > 0 then fieldsSQL = fieldsSQL & ",callback"
	if len(rptDateClosed) > 0 then fieldsSQL = fieldsSQL & ",dateclosed"
	if len(rptDateDeadline) > 0 then fieldsSQL = fieldsSQL & ",datedeadline"	
	if len(rptResAppDate) > 0 then fieldsSQL = fieldsSQL & ",resolutionapproveddate"		
	fieldsSQL = fieldsSQL & ") "
		  
	valueSQL = " VALUES (" _
		  & "'"    	& idIssue							& "'," _
		  &     	rptLogid							& " ," _
		  & "'"    	& "*|*" & sLogid & "*|*" 			& "'," _
		  & "'"    	& severityLevel						& "'," _	  
		  & "'"    	& rptDate							& "'," _
		  & "'"    	& rptTime							& "'," _
		  & "'"    	& rptMonth							& "'," _
		  & "'"    	& rptQuarter						& "'," _		  
		  & "'"    	& rptYear							& "'," _		  		  
		  & "'"    	& uCase(customerID)					& "'," _
		  &     	issueType							& " ," _
		  & "'"    	& replace(locNameID,"'","''")		& "'," _
		  & "'"    	& replace(locName,"'","''")			& "'," _
		  & "'"    	& replace(locAddr,"'","''")			& "'," _
		  & "'"     & replace(locCity,"'","''")			& "'," _
		  & "'"     & locState							& "'," _
		  & "'" 	& locZip							& "'," _
		  & "'" 	& replace(locCountry,"'","''")		& "'," _
		  & "'" 	& commTool							& "'," _		  
		  &  		anonCaller							& " ," _
		  &  		interpreterused						& " ," _		  
		  & "'" 	& language							& "'," _		  		  		  
		  & 		firstTime							& " ," _
		  & "'" 	& callerType						& "'," _		  
		  & "'" 	& replace(callerFirst,"'","''")		& "'," _
		  & "'" 	& replace(callerLast,"'","''")		& "'," _		  		  
		  & "'" 	& replace(callerTitle,"'","''")		& "'," _
		  & "'" 	& replace(callerAddr,"'","''")		& "'," _
		  & "'" 	& replace(callerCity,"'","''")		& "'," _		  
		  & "'" 	& callerState						& "'," _
		  & "'" 	& callerZip							& "'," _
		  & "'" 	& callerHome						& "'," _
		  & "'" 	& callerWork						& "'," _
		  & "'" 	& callerCell						& "'," _
		  & "'" 	& callerFax							& "'," _
		  & "'" 	& callerPager						& "'," _
		  & "'" 	& callerOther						& "'," _		  
		  & "'" 	& callerEmail						& "'," _	
		  & "'" 	& callerPassword					& "'," _
		  & "'" 	& replace(rptSummary,"'","''")		& "'," _
		  & "'" 	& replace(rptDetails,"'","''")		& "'," _
		  & "'" 	& replace(rptAddendum,"'","''")		& "'," _
		  & "'" 	& replace(rptCaseNotes,"'","''")	& "'," _		  		  
		  & "'"    	& rptStatus							& "'," _	
		  & "'"    	& rptSource							& "'," _		  
		  & "'"    	& replace(locLevel_1,"'","''")		& "'," _
		  & "'"    	& replace(locLevel_2,"'","''")		& "'," _
		  & "'"    	& replace(locLevel_3,"'","''")		& "'," _
		  & "'"    	& replace(locLevel_4,"'","''")		& "'," _
		  & "'"    	& replace(locLevel_5,"'","''")		& "'," _
		  & "'"    	& replace(locLevel_6,"'","''")		& "'," _
		  & "'"    	& replace(locLevel_7,"'","''")		& "'," _
		  & "'"    	& replace(locLevel_8,"'","''")		& "'," _
		  & "'" 	& replace(rptresolution,"'","''")	& "'," _		  		  	  		  
		  &     	rptResApp							& ", " _			  
		  & "'"    	& rptResSatisfaction				& "'," _			  		  
		  & "'"		& validSQL(rptUserField1,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField2,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField3,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField4,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField5,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField6,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField7,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField8,"A")		& "'," _		  		 
		  & "'"		& validSQL(rptUserField9,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField10,"A")		& "'," _
		  & "'"		& validSQL(rptUserField11,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField12,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField13,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField14,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField15,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField16,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField17,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField18,"A")		& "'," _		  		 
		  & "'"		& validSQL(rptUserField19,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField20,"A")		& "'," _

		  & "'" 	& Caller_Confidential				& "'," _		  
		  & "'" 	& replace(Details_Redacted,"'","''")		& "'," _
		  
		  & " "		& rptProgress						& ", " _			  	  		  
		  & " "		& sLogid							& ", " _		  
		  & "'"    	& Now()								& "'," _		  
		  & "'"    	& Now()								& "' " _		  

	'check for empty dates
	if len(callBack) > 0 then valueSQL = valueSQL & ",'" & callBack & "'"
	if len(rptDateClosed) > 0 then valueSQL = valueSQL & ",'" & rptDateClosed & "'"				  
	if len(rptDateDeadline) > 0 then valueSQL = valueSQL & ",'" & rptDateDeadline & "'"				  	
	if len(rptResAppDate) > 0 then valueSQL = valueSQL & ",'" & rptResAppDate & "'"
	
	'finish sql	
	valueSQL = valueSQL & ") "
	
	'execute update
	set rs = openRSexecute(fieldsSQL & valueSQL)

	'close database connection
	call closeDB()

	'severity auto set			
	if cLng(severityLevelOrig) <> cLng(severityLevel) then
		session(session("siteID") & "okMsg") = "Issue was Added.<br><span class=""required"">Severity was automatically set and may have been modified.</span>"
	'severity was left as-is
	else
		session(session("siteID") & "okMsg") = "Issue was Added."
	end if						
	response.redirect "issues_edit.asp?action=edit&recID=" & idIssue & "&top=" & topTab & "&side=" & sideTab	
	
end if


'DELETE or BULK DELETE
if action = "del" or action = "bulkdel" or action = "bulkdelall" then

	'Declare additional variables
	dim delI		'Array index
	dim delArray	'List of idIssue's that will be deleted

	'If just one delete is being performed, we populate just the 
	'first position in the delete array, else we populate the array
	'with a list of all the records that were selected for deletion.
	if action = "del" then
		delArray = split(idIssue)
	else
		delArray = split(Request.Form("idIssue"),",")
	end if

	'Set CursorLocation of the Connection Object to Client
	cn.CursorLocation = adUseClient
	
	'Loop through list of records and delete one by one
	for delI = LBound(delArray) to UBound(delArray)
			
		'UPDATE 1st to get accruate user
		mySQL = "UPDATE CRS SET " _
			  & "	ModifiedBy="	 & sLogid					& ", " _
			  & "	ModifiedDate='"  & Now()					& "' " _
		      & "WHERE CRSID = '" &  trim(delArray(delI)) & "'"
		set rs = openRSexecute(mySQL)

		'Delete records from optionsGroupsXref
		mySQL = "DELETE FROM CRS " _
		      & "WHERE CRSID = '" &  trim(delArray(delI)) & "'"
		set rs = openRSexecute(mySQL)

		'delete all related records
		if action = "bulkdelall"  then
			'activity table
			mySQL = "DELETE FROM Activity WHERE CRSID = '" &  trim(delArray(delI)) & "'"
			set rs = openRSexecute(mySQL)	
			'categories
			mySQL = "DELETE FROM CRS_Category WHERE CRSID = '" &  trim(delArray(delI)) & "'"
			set rs = openRSexecute(mySQL)
			'assigned logids
			mySQL = "DELETE FROM CRS_Logins WHERE CRSID = '" &  trim(delArray(delI)) & "'"
			set rs = openRSexecute(mySQL)
			'questions
			mySQL = "DELETE FROM CRS_Question WHERE CRSID = '" &  trim(delArray(delI)) & "'"
			set rs = openRSexecute(mySQL)
			'investigations
			mySQL = "DELETE FROM Investigation WHERE CRSID = '" &  trim(delArray(delI)) & "'"
			set rs = openRSexecute(mySQL)
			'resolutions
			mySQL = "DELETE FROM Resolution WHERE CRSID = '" &  trim(delArray(delI)) & "'"
			set rs = openRSexecute(mySQL)
		end if
						
	next

	'close database connection
	call closeDB()
	
	session(session("siteID") & "okMsg") = "Issue(s) were Deleted."
	response.redirect "issues.asp"
	
end if


'DELETE Related Issue
if action = "dellink" then

	'UPDATE 1st to get accruate user
	mySQL = "UPDATE CRS_Related SET " _
		  & "	ModifiedBy="	 & sLogid										& ", " _
		  & "	ModifiedDate='"  & Now()										& "' " _
	      & "WHERE CRSID = '" &  idIssue & "' AND RelatedCRSID = '" & idRelated & "' "
	set rs = openRSexecute(mySQL)

	'Delete records from optionsGroupsXref
	mySQL = "DELETE FROM CRS_Related " _
	      & "WHERE CRSID = '" &  idIssue & "' AND RelatedCRSID = '" & idRelated & "' "
	set rs = openRSexecute(mySQL)

	'close database connection
	call closeDB()
	
	session(session("siteID") & "okMsg") = "Link Deleted."
	response.redirect "issues.asp?pageView=related&recid=" & idIssue
	
end if


'EDIT
if action = "edit" or action = "editaddinv" then
			
	'Update Record
	mySQL = "UPDATE CRS SET " _	
		  & "logid=" 					& rptLogid							& ", " _		
		  & "[date]='"     				& rptDate							& "'," _
		  & "[time]='"     				& rptTime							& "'," _
		  & "month='"     				& rptMonth							& "'," _
		  & "quarter='"     			& rptQuarter						& "'," _
		  & "year='"     				& rptYear							& "'," _
		  & "severity='"     			& severityLevel						& "'," _
		  & "location_nameid='"    		& replace(locNameID,"'","''")		& "'," _
		  & "location_name='"     		& replace(locName,"'","''")			& "'," _
		  & "location_address='"    	& replace(locAddr,"'","''")			& "'," _
		  & "location_city='"     		& replace(locCity,"'","''")			& "'," _
		  & "location_state='"     		& locState							& "'," _
		  & "location_postalcode='" 	& locZip							& "'," _
		  & "location_country='" 		& replace(locCountry,"'","''")		& "'," _
		  & "communicationtool='" 		& commTool							& "'," _
		  & "caller_confidential='"		& caller_confidential				& "'," _
		  & "details_redacted='"		& replace(details_redacted,"'","''")		& "'," _
		  & "anonymous=" 				& anonCaller						& ", " _
		  & "interpreterused=" 			& interpreterused					& ", " _		  
		  & "language='"		 		& language							& "'," _		  		  		  
		  & "firsttimeuser=" 			& firstTime							& ", " _
		  & "caller_type='"		 		& callerType						& "'," _		  
		  & "caller_firstname='" 		& replace(callerFirst,"'","''")		& "'," _
		  & "caller_lastname='" 		& replace(callerLast,"'","''")		& "'," _		  		  
		  & "caller_title='" 			& replace(callerTitle,"'","''")		& "'," _
		  & "caller_address='" 			& replace(callerAddr,"'","''")		& "'," _
		  & "caller_city='" 			& replace(callerCity,"'","''")		& "'," _
		  & "caller_state='" 			& callerState						& "'," _
		  & "caller_postalcode='" 		& callerZip							& "'," _
		  & "caller_home='" 			& callerHome						& "'," _
		  & "caller_work='" 			& callerWork						& "'," _
		  & "caller_cell='" 			& callerCell						& "'," _
		  & "caller_fax='" 				& callerFax							& "'," _
		  & "caller_pager='" 			& callerPager						& "'," _
		  & "caller_other='" 			& callerOther						& "'," _
		  & "caller_email='" 			& callerEmail						& "'," _		  
		  & "caller_password='"			& callerPassword					& "'," _		  		  
		  & "summary='"     			& replace(rptSummary,"'","''")		& "'," _
		  & "details='"     			& replace(rptDetails,"'","''")		& "'," _
		  & "addendum='"     			& replace(rptAddendum,"'","''")		& "'," _
		  & "casenotes='"     			& replace(rptCaseNotes,"'","''")	& "'," _
		  & "status='"		 			& rptStatus							& "'," _
		  & "source='"		 			& rptSource							& "'," _
		  & "location_level_1='"		& replace(locLevel_1,"'","''")		& "'," _
		  & "location_level_2='"		& replace(locLevel_2,"'","''")		& "'," _
		  & "location_level_3='"		& replace(locLevel_3,"'","''")		& "'," _
		  & "location_level_4='"		& replace(locLevel_4,"'","''")		& "'," _
		  & "location_level_5='"		& replace(locLevel_5,"'","''")		& "'," _
		  & "location_level_6='"		& replace(locLevel_6,"'","''")		& "'," _
		  & "location_level_7='"		& replace(locLevel_7,"'","''")		& "'," _
		  & "location_level_8='"		& replace(locLevel_8,"'","''")		& "'," _
		  & "Resolution='" 				& replace(rptresolution,"'","''")	& "'," _		  		  	  
		  & "ResolutionApprovedBy=" 	& rptResApp							& ", " _			  
		  & "ResolutionSatisfaction='" 	& rptResSatisfaction				& "', " _			  
		  & "UserField1='"				& validSQL(rptUserField1,"A")		& "'," _		  
		  & "UserField2='"				& validSQL(rptUserField2,"A")		& "'," _		  
		  & "UserField3='"				& validSQL(rptUserField3,"A")		& "'," _		  
		  & "UserField4='"				& validSQL(rptUserField4,"A")		& "'," _		  
		  & "UserField5='"				& validSQL(rptUserField5,"A")		& "'," _		  
		  & "UserField6='"				& validSQL(rptUserField6,"A")		& "'," _		  
		  & "UserField7='"				& validSQL(rptUserField7,"A")		& "'," _		  
		  & "UserField8='"				& validSQL(rptUserField8,"A")		& "'," _	  
		  & "UserField9='"				& validSQL(rptUserField9,"A")		& "'," _		  
		  & "UserField10='"				& validSQL(rptUserField10,"A")		& "'," _
		  & "UserField11='"				& validSQL(rptUserField11,"A")		& "'," _		  
		  & "UserField12='"				& validSQL(rptUserField12,"A")		& "'," _		  
		  & "UserField13='"				& validSQL(rptUserField13,"A")		& "'," _		  
		  & "UserField14='"				& validSQL(rptUserField14,"A")		& "'," _		  
		  & "UserField15='"				& validSQL(rptUserField15,"A")		& "'," _		  
		  & "UserField16='"				& validSQL(rptUserField16,"A")		& "'," _		  
		  & "UserField17='"				& validSQL(rptUserField17,"A")		& "'," _		  
		  & "UserField18='"				& validSQL(rptUserField18,"A")		& "'," _	  
		  & "UserField19='"				& validSQL(rptUserField19,"A")		& "'," _		  
		  & "UserField20='"				& validSQL(rptUserField20,"A")		& "'," _		 
		  & "Progress="					& rptProgress						& ", " _
		  & "ModifiedBy="				& sLogid							& ", " _		  
		  & "modifieddate='"     		& Now()								& "' "		  

	'check for special existance of dates  
	if len(callback) > 0 then mySQL = mySQL & ",callback='" & callBack & "' "
	if len(rptDateClosed) > 0 then mySQL = mySQL & ",dateclosed='" & rptDateClosed & "' " else mySQL = mySQL & ",dateclosed=null "
	if len(rptDateDeadline) > 0 then mySQL = mySQL & ",datedeadline='" & rptDateDeadline & "' "	else mySQL = mySQL & ",datedeadline=null "
	if len(rptResAppDate) > 0 then mySQL = mySQL & ",resolutionapproveddate='" & rptResAppDate & "' "
		
	'finalize update query
	mySQL = mySQL & "WHERE CRSID = '" & idIssue & "' "
	
	'for AJax display
	if action = "edit" then
		'execute update
		set rs = openRSexecuteAjax(mySQL)
		response.write("Issue saved successfully.")
	elseif action = "editaddinv" then
		'execute update
		set rs = openRSexecute(mySQL)	
		session(session("siteID") & "okMsg") = "Issue has been saved successfully and all investigations available added."		
		response.redirect "issues_edit.asp?action=edit&recID=" & idIssue & "&top=" & topTab & "&side=" & sideTab
	end if		

	'close database connection
	call closeDB()			
	
end if


'add single investigation NOT tied to category
if action = "addinv" then

	'can user view this issue?
	if userViewIssue(idIssue,sLogid) = False then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
	end if

	'check user's READ-ONLY status
	if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then
		mySQL = "SELECT ReadOnly FROM CRS_Logins WHERE CRSID='" & idIssue & "' AND LOGID = " & sLogid & " "		  
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		if not rs.eof then		
			if rs("ReadOnly") = "Y" then
				response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
			end if		
		end if
	end if

	'get/set customerID
	mySQL = "SELECT CustomerID, LogID, IssueTypeID FROM CRS WHERE CRSID='" & idIssue & "' "		  
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	customerID = rs("customerid")
	ownerID = rs("LogID")
	issueType = rs("IssueTypeID")
	
	'get deadline day default settings
	mySQL = "SELECT InvDeadlineDays " _
		  & "FROM   Customer_IssueType " _
		  & "WHERE  CustomerID = '" & customerID & "' AND IssueTypeID=" & issueType
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if not rs.eof then
		useInvDeadlineDays = rs("InvDeadlineDays")
	end if

	'add new category to investigations table...
	mySQL = "INSERT INTO Investigation (" _
		  & "CustomerID,CRSID,Name,Status,DateAdded,ModifiedBy,ModifiedDate"
		  if len(useInvDeadlineDays) > 0 then mySQL = mySQL & ",DateDeadline"
		  if programAdmin <> "Y" then mySQL = mySQL & ",LogID"			  
	mySQL = mySQL & ") VALUES (" _
		  & "'" & uCase(customerID) & "'," & "'" & idIssue & "','Undefined','Active','" & Date() & "'," & sLogid & ",'" & Now() & "'"
		  if len(useInvDeadlineDays) > 0 then mySQL = mySQL & ",'" & dateAdd("d",cLng(useInvDeadlineDays),Date()) & "'"
		  if programAdmin <> "Y" then mySQL = mySQL & "," & ownerID
  	mySQL = mySQL & ")"
	set rs = openRSexecute(mySQL)	

	'close database connection
	call closeDB()
	
	session(session("siteID") & "okMsg") = "Investigation(s) has been added successfully."
	response.redirect "issues_edit.asp?action=edit&recID=" & idIssue & "&top=" & topTab & "&side=" & sideTab
	
end if

'delete single investigation
if action = "delinv" then

	'UPDATE 1st to get accruate user
	mySQL = "UPDATE Investigation SET " _
		  & "	ModifiedBy="	 & sLogid				  & ", " _
		  & "	ModifiedDate='"  & Now()				  & "' " _
	      & "WHERE InvestigationID = " &  idInvestigation & " "
	set rs = openRSexecute(mySQL)

	'Delete records from optionsGroupsXref
	mySQL = "DELETE FROM Investigation " _
	      & "WHERE InvestigationID = " &  idInvestigation & " "
	set rs = openRSexecute(mySQL)

	'close database connection
	call closeDB()

	session(session("siteID") & "okMsg") = "Investigation was deleted successfully."
	response.redirect "issues_edit.asp?action=edit&recID=" & idIssue & "&top=" & topTab & "&side=" & sideTab

end if

'just in case we ever get this far...and NOT...called by action=edit
if action <> "edit" then
	call closeDB()
	Response.Redirect "issues.asp"
end if

%>
