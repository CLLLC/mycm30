
<script type="text/javascript">
<!--
stBM(300,"myCMtree",[1,"","","../scripts/tree/blank.gif",0,"left","hand","hand",1,0,-1,180,-1,"none",0,"#000000","transparent","","repeat",0,"../scripts/tree/defButton_f.gif","../scripts/tree/defButton_uf.gif",9,9,0,"../scripts/tree/line_def0.gif","../scripts/tree/line_def1.gif","../scripts/tree/line_def2.gif","../scripts/tree/line_def3.gif",0,0,0,2,"center",1,1,0,"","","","",""]);
stBS("p0",[0,1,"",-2,"",-2,50,20,3]);
/*
<%
dim menuID, subID, menuSQL, menuRS, tempName

'show CUSTOMER tree menu if CCI staff
if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
	'user to set proper order of submenus
	subID = 0 ' --> PUT BACK TO "1" WHEN ADDING CUSTOMER SUB-MENEU...ALSO CHANGE 
	%>
	*/
//	 --> TURNED OFF CUSTOMER SUB-MENUS...UNCOMMENT TO GET SUB-MENUS BACK ON
//	stIT("p0i0",["Customers","../profiles/profiles.asp","_self","","","../_images/icons/16/building.png","../_images/icons/16/building.png",16,16,"8pt 'Verdana','Arial'","#333333","none","transparent","","no-repeat","8pt 'Verdana','Arial'","#333333","none","#EFF2F7","","no-repeat","bold 8pt 'Verdana','Arial'","#333333","none","#CED3EB","","no-repeat","bold 8pt 'Verdana','Arial'","#333333","none","#CED3EB","","no-repeat",1,0,"left","middle",180,0,"","","","",0,0,0]);
//	stBS("p1",[,0,,,,,,0],"p0");
//	stIT("p1i0",["Users","../maintenance/users_edit.asp",,,,"../_images/icons/16/users.png","../_images/icons/16/users.png",16,16],"p0i0");
//	stIT("p1i1",["Add Customer","../maintenance/users_edit.asp",,,,"../_images/icons/16/add.png","../_images/icons/16/add.png",16,16],"p0i0");
//	stES();	
//	stIT("p0i1",["Issue List","../scripts/issues.asp",,,,"../_images/icons/16/table.png","../_images/icons/16/table.png"],"p0i0");
//	stBS("p2",[],"p1");

//	 --> COMMENT THESE 3 LINES OUT WHEN SUB-MENUS IS TURNED BACK ON
	stIT("p0i0",["Customers","../profiles/profiles.asp","_self","","","../_images/icons/16/building.png","../_images/icons/16/building.png",16,16,"8pt 'Verdana','Arial'","#333333","none","transparent","","no-repeat","8pt 'Verdana','Arial'","#333333","none","#EFF2F7","","no-repeat","bold 8pt 'Verdana','Arial'","#333333","none","#CED3EB","","no-repeat","bold 8pt 'Verdana','Arial'","#333333","none","#CED3EB","","no-repeat",1,0,"left","middle",180,0,"","","","",0,0,0]);
	stIT("p0i1",["Issue List","../scripts/issues.asp",,,,"../_images/icons/16/table.png","../_images/icons/16/table.png"],"p0i0");
	stBS("p1",[,0,,,,,,0],"p0");
	/*
<%
'non-CCI staff logged in
else
	'user to set proper order of submenus
	subID = 0
	%>
	*/
	stIT("p0i0",["Issue List","../scripts/issues.asp","_self","","","../_images/icons/16/table.png","../_images/icons/16/table.png",16,16,"8pt 'Verdana','Arial'","#333333","none","transparent","","no-repeat","8pt 'Verdana','Arial'","#333333","none","#EFF2F7","","no-repeat","bold 8pt 'Verdana','Arial'","#333333","none","#CED3EB","","no-repeat","bold 8pt 'Verdana','Arial'","#333333","none","#CED3EB","","no-repeat",1,0,"left","middle",180,0,"","","","",0,0,0]);
	stBS("p1",[,0,,,,,,0],"p0");
	/*
	<%
end if
%>
*/
/*
<%
'sub-menu for issues_edit.asp page...
if (pageView = "related" or pageView = "issue" or pageView = "issue:00" or pageView = "issue:tools") and action <> "add" and action <> "addfu" then
	menuID = 0	
	if len(idIssue)>17 then tempName = left(idIssue,15) & "..." else tempName = idIssue
	%>
	*/
	stIT("p<% =subID+1 %>i<% =menuID %>",["<% =tempName %>","../scripts/<% =session(session("siteID") & "issuesEdit") %>?action=edit&recid=<% =idIssue %>",,,,"../_images/icons/16/tree_level.gif","../_images/icons/16/tree_level.gif",16,16],"p0i0");
	stIT("p<% =subID+1 %>i<% =menuID+1 %>",["Related Issues","../scripts/issues.asp?pageView=related&recid=<% =idIssue %>",,,,"../_images/icons/16/arrow_switch.png","../_images/icons/16/arrow_switch.png",16,16],"p0i0");

	/*
	<%	
	'customers NOT allowed to add FUP to issues 1 or 2
	if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then
		if issueType<>"3" then
			%>
			*/
			stIT("p<% =subID+1 %>i<% =menuID+2 %>",["Follow-Up [F8]","javascript:jAlert('Permission denied.', 'myCM Alert');",,,,"../_images/icons/16/add_disabled.png","../_images/icons/16/add.png",16,16],"p0i0");
			/*
			<%	
		else
			%>
			*/
			stIT("p<% =subID+1 %>i<% =menuID+2 %>",["Follow-Up [F8]","../scripts/<% =session(session("siteID") & "issuesEdit") %>?action=addfu&recId=<% =left(idIssue,len(idIssue)-3) & "-01" %>",,,,"../_images/icons/16/add.png","../_images/icons/16/add.png",16,16],"p0i0");
			/*
			<%				
		end if
	'CCI staff
	else
		'for CCI Staff only allow FUP on IssueType = 1
		if issueType<>"1" then
			%>
			*/
			stIT("p<% =subID+1 %>i<% =menuID+2 %>",["Follow-Up [F8]","javascript:jAlert('Follow-Ups not allowed.', 'myCM Alert');",,,,"../_images/icons/16/add_disabled.png","../_images/icons/16/add.png",16,16],"p0i0");
			/*
			<%	
		else
			%>
			*/
			stIT("p<% =subID+1 %>i<% =menuID+2 %>",["Follow-Up [F8]","../scripts/<% =session(session("siteID") & "issuesEdit") %>?action=addfu&recId=<% =left(idIssue,len(idIssue)-3) & "-01" %>",,,,"../_images/icons/16/add.png","../_images/icons/16/add.png",16,16],"p0i0");
			/*
			<%	
		end if
	end if
	
	if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
		%>
		*/
		stIT("p<% =subID+1 %>i<% =menuID+3 %>",["Toolbox","../scripts/issues_tools.asp?recid=<% =idIssue %>&action=<% =action %>",,,,"../_images/icons/16/setting_tools.png","../_images/icons/16/setting_tools.png",16,16],"p0i0");
		/*
		<%	
	end if
	
elseif (pageView = "issue" or pageView = "issue:00" or pageView = "issue:add") and action = "add" then
	%>
	*/
	stIT("p<% =subID+1 %>i0",["<none assigned>","",,,,"../_images/icons/16/tree_level.gif","../_images/icons/16/tree_level.gif"],"p0i0");
	stIT("p<% =subID+1 %>i1",["Add Issue [F2]","../scripts/<% =session(session("siteID") & "issuesEdit") %>?cid=<% =customerID %>&action=add",,,,"../_images/icons/16/add.png","../_images/icons/16/add.png"],"p0i0");
	/*
	<%

elseif (pageView = "related" or pageView = "issue" or pageView = "issue:00") and action = "addfu" then
	menuID = 0
	%>
	*/
	stIT("p<% =subID+1 %>i<% =menuID %>",["<% =idIssue %>","",,,,"../_images/icons/16/tree_level.gif","../_images/icons/16/tree_level.gif",16,16],"p0i0");
	stIT("p<% =subID+1 %>i<% =menuID+1 %>",["Related Issues","../scripts/issues.asp?pageView=related&recid=<% =idIssue %>",,,,"../_images/icons/16/arrow_switch.png","../_images/icons/16/arrow_switch.png",16,16],"p0i0");
	stIT("p<% =subID+1 %>i<% =menuID+2 %>",["Follow-Up [F8]","../scripts/<% =session(session("siteID") & "issuesEdit") %>?action=addfu&recId=<% =left(idIssue,len(idIssue)-3) & "-01" %>",,,,"../_images/icons/16/add.png","../_images/icons/16/add.png",16,16],"p0i0");
	/*
	<%

else

	dim treeImage
	menuSQL = "SELECT Reports.ReportID, Reports.Name, Dataset.Type " _
		    & "FROM Reports INNER JOIN Dataset ON Reports.DatasetID = Dataset.DatasetID " _
            & "WHERE  (Reports.IssueList='Y') AND (Reports.LOGID=" & sLogid & ") AND (Reports.CustomerID='" & sCustomerID & "') " _
            & "ORDER BY Reports.SortOrder"
    set menuRS = openRSexecute(menuSQL)
    if not menuRS.eof then
		menuID = 0		
        do until menuRS.eof
			'shorten name if necessasry
			if len(menuRS("name"))>20 then tempName = left(menuRS("name"),16) & "..." else tempName = menuRS("name")
			'set tree image
			if lCase(menuRS("type")) = "register" then 
				treeImage = "../_images/icons/16/table.png"
			elseif lCase(menuRS("type")) = "calculated" then 
				treeImage = "../_images/icons/16/table_sum.png"
			elseif lCase(menuRS("type")) = "sql" then 
				treeImage = "../_images/icons/16/table_link.png"
			else 
				treeImage = "../_images/icons/16/tree_level.gif"						
			end if
			%>
			*/
			stIT("p<% =subID+1 %>i<% =menuID %>",["<% =tempName %>","../scripts/issues.asp?pageView=<%=menuRS("ReportID") %>",,,,"<% =treeImage %>","../_images/icons/16/tree_level.gif",16,16],"p0i0");
			/*
			<%
			menuID = menuID + 1
        	menuRS.movenext
		loop
	end if
    call closeRS(menuRS)
	
	'linked to one (1) account, no pop up required
	if profileCount <= 1 then		
		%>
		*/
		stIT("p<% =subID+1 %>i<% =menuID %>",["Add Issue [F2]","../scripts/<% =session(session("siteID") & "issuesEdit") %>?cid=<% =sCustomerID %>&action=add",,,,"../_images/icons/16/add.png","../_images/icons/16/add.png"],"p0i0");
		/*
		<%
			
	'show customer_popup...linked to more than one (1) account			
	elseif profileCount > 1 then
		%>
		*/
		stIT("p<% =subID+1 %>i<% =menuID %>",["Add Issue [F2]","javascript:SimpleModal.open('../_dialogs/popup_profile.asp', 430, 700, 'no');",,,,"../_images/icons/16/add.png","../_images/icons/16/add.png"],"p0i0");			
		/*
		<%
			
	end if

	'show follow-up on main shortcut list
	if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
		%>
		*/
		stIT("p<% =subID+1 %>i<% =menuID %>",["Follow-Up [F8]","javascript:addFollowUp();",,,,"../_images/icons/16/add.png","../_images/icons/16/add.png"],"p0i0");			
		/*
		<%
	end if
	
end if
%>
*/
stES();
/*
<%
'if CCI staff logged in set report menu with proper settings
if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
%>
*/
stIT("p0i2",["Reports","../reports/reports.asp",,,,"../_images/icons/16/chart.png","../_images/icons/16/chart.png"],"p0i0");
stBS("p3",[],"p2");
/*
<%
'non-CCI staff logged in
else
%>
*/
stIT("p0i1",["Reports","../reports/reports.asp",,,,"../_images/icons/16/chart.png","../_images/icons/16/chart.png"],"p0i0");
stBS("p2",[],"p1");
/*
<%
end if
%>
*/
/*
<%
'sub-menu for reports_edit.asp page...
if pageView = "reports:edit" then
	menuID = 1
	if len(optName)>20 then tempName = left(optName,16) & "..." else tempName = optName
	%>
	*/
	stIT("p<% =subID+2 %>i0",["<% =tempName %>","../reports/reports_edit.asp?action=edit&recID=<% =idReport %>",,,,"../_images/icons/16/tree_level.gif","../_images/icons/16/tree_level.gif",16,16],"p0i0");
	/*
	<%	
'sub-menu for execute.asp page...
elseif pageView = "custom:edit" then
	menuID = 1
	if len(optName)>20 then tempName = left(optName,16) & "..." else tempName = optName	
	%>
	*/
	stIT("p<% =subID+2 %>i0",["<% =tempName %>","../reports/crystal/execute.asp?type=edit&recID=<% =idReport %>&name=<% =Server.URLEncode( crystalFile ) %>",,,,"../_images/icons/16/tree_level.gif","../_images/icons/16/tree_level.gif",16,16],"p0i0");
	/*
	<%
else
	menuID = 0
end if
%>
*/

stIT("p<% =subID+2 %>i<% =menuID %>",["Registers","../reports/reports.asp?type=register",,,,"../_images/icons/16/table.png","../_images/icons/16/table.png",16,16],"p0i0");
stIT("p<% =subID+2 %>i<% =menuID+1 %>",["Calculated","../reports/reports.asp?type=calculated",,,,"../_images/icons/16/table_sum.png","../_images/icons/16/table_sum.png",16,16],"p0i0");
/*
<% 
	dim addMenu
	addMenu = 0
	menuSQL = "SELECT Dataset.Type " _
	    & "FROM Dataset " _
        & "WHERE  CustomerID='" & sCustomerID & "' AND Type='SQL' and SecurityLevel=" & cLng(session(session("siteID") & "adminLoggedOn")) & " "
	set menuRS = openRSexecute(menuSQL)
    if not menuRS.eof then
		addMenu = 1
		%>
		*/
		stIT("p<% =subID+2 %>i<% =menuID+2 %>",["SQL","../reports/reports.asp?type=sql",,,,"../_images/icons/16/table_link.png","../_images/icons/16/table_link.png",16,16],"p0i0");
		/*
		<% 	
	end if 	
%>
*/
stIT("p<% =subID+2 %>i<% =menuID+2+addMenu %>",["Crystal","../reports/reports.asp?type=crystal",,,,"../_images/icons/16/file_extension_rpt.png","../_images/icons/16/file_extension_rpt.png",16,16],"p0i0");

/*
<% 
if addReport="Y" then
%>
*/
	stIT("p<% =subID+2 %>i<% =menuID+3+addMenu %>",["Add Report [F9]","../datasets/default.asp",,,,"../_images/icons/16/add.png","../_images/icons/16/add.png",16,16],"p0i0");
/*
<% 	
end if 	
%>
*/

stES();

stES();
stEM();
//-->
</script>

