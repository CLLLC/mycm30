<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = false
const showDHTMLMenu = true
dim helpID : helpID = 300001 'for customers NOT cci staff
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "issue:tools"
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'make sure user can update this page
if uCase(viewToolBox) = "Y" then
	'good to go
else
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if


dim showCRSID '...need for NEW search box
dim startDate
dim endDate

'Issue Fields on DETAILS tab
dim issueType

dim readonly

'Get action
dim action : action = trim(Request.QueryString("action"))
if len(action) = 0 then
	action = trim(Request.Form("action"))
end if
action = lCase(action)
if  action <> "edit" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if


'get idIssue (1101-AAA-10001-01)
dim idIssue
idIssue = trim(Request.QueryString("recId"))
if len(idIssue) = 0 then
	idIssue = trim(Request.Form("recId"))
end if
if idIssue = "" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")
end if

'check user view status	for customer and issuetype
'based on vwLogins_IssueType and security sets
if action = "edit" then
	if userViewIssue(idIssue,sLogid) = False then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to view this issue.")
	end if
end if


'setup issue to edit
if action = "edit" then

	'check user's READ-ONLY status, NOT for CCI users only customer logins
	if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then
		mySQL = "SELECT ReadOnly FROM CRS_Logins WHERE CRSID='" & idIssue & "' AND LOGID = " & sLogid & " "		  
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		if not rs.eof then		
			readonly = rs("ReadOnly")
		end if
	end if

	'DBAdmins and CCI Admins
	if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
		'get issue record/details
		mySQL = "SELECT CRS.*, Logins.FirstName, Logins.LastName, Logins.Email, Customer_IssueType.Resolutions " _
			  & "	FROM (CRS LEFT JOIN Logins ON CRS.LOGID = Logins.LOGID) INNER JOIN Customer_IssueType ON (CRS.CustomerID = Customer_IssueType.CustomerID) AND (CRS.IssueTypeID = Customer_IssueType.IssueTypeID) " _
			  & "	WHERE CRS.CRSID='" & idIssue & "' "

	'CCI RA/RS
	elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
		'get issue record/details
		mySQL = "SELECT CRS.*, Logins.FirstName, Logins.LastName, Logins.Email, Customer_IssueType.Resolutions " _
			  & "	FROM (CRS LEFT JOIN Logins ON CRS.LOGID = Logins.LOGID) INNER JOIN Customer_IssueType ON (CRS.CustomerID = Customer_IssueType.CustomerID) AND (CRS.IssueTypeID = Customer_IssueType.IssueTypeID) " _
			  & "	WHERE CRS.CRSID='" & idIssue & "' AND CRS.IssueTypeID<>3 "

	'everyone else...				  
	else
		'get issue record/details
		mySQL = "SELECT CRS.*, Logins.FirstName, Logins.LastName, Logins.Email, Customer_IssueType.Resolutions, vwLogins_IssueType.LOGID AS [LogIDView] " _	
			  & "	FROM ((CRS LEFT JOIN Logins ON CRS.LOGID = Logins.LOGID) INNER JOIN Customer_IssueType ON (CRS.IssueTypeID = Customer_IssueType.IssueTypeID) AND (CRS.CustomerID = Customer_IssueType.CustomerID)) INNER JOIN vwLogins_IssueType ON (Customer_IssueType.IssueTypeID = vwLogins_IssueType.IssueTypeID) AND (Customer_IssueType.CustomerID = vwLogins_IssueType.CustomerID) " _
			  & " 	WHERE CRS.CRSID='" & idIssue & "' AND CRS.Progress=3 AND vwLogins_IssueType.LOGID = " & sLogid & " "
			  
	end if

	'open record
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")		
	else	
		CRSID 		= rs("crsid")
		customerID	= uCase(rs("customerid"))
		issueType	= rs("issuetypeid")
	end if
	
	call closeRS(rs)
	
end if


dim pageTitle
pageTitle = "Tools: " & idIssue 
%>
<!--#include file="../_includes/_INCheader_.asp"-->

<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
  
	    <!-- START Left sided area -->  
		<td width="190" align="right" valign="top" >    

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">
    
                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->              
                    <% if session(session("siteID") & "adminLoggedOn") < "10" then %>      
	                    <script>
   		                	stExpandSubTree('myCMtree',2,0);
       		            </script>
            		<% else %>
	                    <script>
   		                	stExpandSubTree('myCMtree',1,0);
       		            </script>
                    <% end if %>
                    <!-- STOP side menu include -->
                                    
                </td>
              </tr>
              <!-- STOP Left sided menu -->
        
            </table>

		</td>
    	<!-- STOP Left sided area -->


        <!-- START Main Middle Section -->
        <td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">    

			<%
			'used to STOP updating the cookie crumb if page is being recalled from a SAVE
			dim updateCrumb
			if inStr(1, session(session("siteID") & "okMsg"), "was Added") > 0 then
				'do nothing...will update cookie
            elseif len(session(session("siteID") & "errMsg")) > 0 or len(session(session("siteID") & "okMsg")) > 0 then
				updateCrumb = "false"
            end if
			%>
            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs('<% =updateCrumb %>'));
       	        </script>
            </div>
    
            <div style="text-align:left; margin-bottom:10px;">				
                <div style="float:left; padding-right:2px; margin-right:2px;"><img src="../_images/icons/32/setting_tools.png" title="" width="32" height="32" align="absmiddle" style="vertical-align:middle;"></div>
                <div>
                	<span class="pageTitle">Tools: <% =idIssue %></span>
                    <%
					if (cLng(session(session("siteID") & "adminLoggedOn")) < 10) or (programAdmin = "Y") then
						response.write("<br/>:: <a href=""../profiles/profiles_menu.asp?cid=" & customerID & """>" & getCustomerName(customerID) & "</a>")
					else
						response.write("<br/>:: " & getCustomerName(customerID) & "")
					end if					
					%>
                </div>
            </div>                   
                               
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
            	            <legend>Information</legend>
	                        <div align="left">
                                <img src="../_images/icons/16/exclamation.png" alt="Information" width="16" height="16" align="absmiddle" style="margin-right:3px;"><span class="required">WARNING</span>: Take care when using the tool set below.
								<% response.write( getInlineHelp("getPageURL", 0) ) %>
        	                </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             
                                    
                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">               	
                    	<!-- TEMPORARILY TUNRED OFF TILL I CAN FIGURE WHAT TO PUT HERE... -->
                        &nbsp;
                    </td>
                    <!-- STOP Information buttons -->
                        
                </tr>
            </table>        
            <!-- STOP information box -->

    
    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			'clear system messages
			call clearSessionMessages()
            %>
    		<!-- STOP message notification box -->


            <!-- START Tools div -->
            <div id="tab:tools">        
            
                <!-- START Group Detail table -->   
                <table cellpadding="0" cellspacing="0" style="width:100%;">
    
                    <tr>
                        
                        <td valign="top">
                                            
                            <!-- START Details Form Fields -->	                    
                            <table width="100%" class="formTable" cellpadding="0" cellspacing="0">                           
                    
                                <tr>                            
                                    <td align=left style="padding-left:10px; width:50%; margin-right:10px;">
                                    	<img src="../_images/icons/16/clock_history_frame.png" alt="History" width="16" height="16" align="absmiddle" style="vertical-align:middle;">
										<a href="#" style="margin-right:4px;" onClick="SimpleModal.open('../_dialogs/popup_hist_table.asp?recid=<% =idIssue %>', 500, 700, 'no'); return false;"><strong>View History</strong></a><br />View historical information that was changed/modified/deleted. History includes details, case managers, e-mails and investigations.                                    
                                    </td>
                                    <form name="frmDelete" id="frmDelete" method="post" action="issues_exec.asp">        
                                    <td align=left style="padding-left:10px; width:50%;">                                    	
                                        <% if (inStr(1,deleteIssues,reportType(idIssue))) then %>                                       
                                            <img src="../_images/icons/16/cancel.png" alt="Delete" width="16" height="16" align="absmiddle" style="vertical-align:middle;">
                                            <a href="#" onClick="deleteIssue('frmDelete'); return false;"><strong>Delete Issue</strong></a><br />Permently remove issue and all related details [Categories, Case Managers, Investigations, Resolutions].
                                        <% else %>
	                                        <img src="../_images/icons/16/cancel_disabled.png" alt="Delete" width="16" height="16" align="absmiddle" style="vertical-align:middle;">
                                            <strong>Delete Issue</strong><br />Permently remove issue and all related details [Categories, Case Managers, Investigations, Resolutions].
                                        <% end if %>                                                                                
                                        <input type=hidden name="action" id="action" value="bulkDel">
                                        <input type=hidden name="idIssue" id="idIssue" value="<% =idIssue %>">
                                    </td>
                                    </form>
                                </tr>

                                <tr>                            
                                    <form name="frmMove" id="frmMove" method="post" action="issues.asp">
                                    <td align=left style="padding-left:10px; width:50%; margin-right:10px;">
                                    	<img src="../_images/icons/16/transform_move.png" alt="Move" width="16" height="16" align="absmiddle" style="vertical-align:middle;">
                                    	<a href="#" onClick="SimpleModal.open('../_dialogs/popup_profile.asp?pageView=issue:tools', 430, 700, 'no'); return false;"><strong>Move Issue</strong></a>
                                       	<br />Move this issue and all related records to another profile. Using this tool will move both the original and all follow-ups to the selected destination profile.
                                   	    <input type=hidden name="profilesName" id="profilesName" value="">
                                   	    <input type=hidden name="profiles" id="profiles" value="">
                                    </td>
                                    </form>                                    
                                    <% if cLng(session(session("siteID") & "adminLoggedOn")) <= 2 then %>
                                        <td align=left style="padding-left:10px; width:50%;">
                                            <img src="../_images/icons/16/page_white_lightning.png" alt="Issue Type" width="16" height="16" align="absmiddle" style="vertical-align:middle;">
                                            <a href="#" style="margin-right:4px;" onClick="SimpleModal.open('../_dialogs/popup_type_issue.asp?action=change&cid=<% =customerID %>&recid=<% =idIssue %>&issuetype=<% =issueType %>', 300, 400, 'no'); return false;"><strong>Change Issue Type</strong></a><br />Change the issue type originally assigned to this issue.
                                        </td>
                                    <% else %>
	                                    <td align=left style="padding-left:10px; width:50%;">&nbsp;</td>
									<% end if %>                                    
                                </tr>
                                
								<% if cLng(session(session("siteID") & "adminLoggedOn")) <= 2 then %>
                                <tr>                            
                                    <td align=left style="padding-left:10px; width:50%; margin-right:10px;">
                                    	<img src="../_images/icons/16/building.png" alt="Customer View" width="16" height="16" align="absmiddle" style="vertical-align:middle;">
										<a href="../scripts/issues_edit.asp?action=edit&recid=<% =idIssue %>" style="margin-right:4px;"><strong>Customer View</strong></a><br />View this issue in the same form and format as a customer with investigations, documents etc.
                                    </td>
                                    <td align=left style="padding-left:10px; width:50%;">&nbsp;</td>
                                </tr>
								<% end if %>
                                
                                <tr>                            
                                    <td align=left>&nbsp;</td>
                                    <td align=left>&nbsp;</td>
                                </tr>
                                    
                            </table>
                            <!-- END Details Form Fields -->
                            
                        </td>
                        
                    </tr>
                
                </table>
                <!-- END User Account table -->
        
            </div>
            <!-- STOP Account div -->        
    
    
	    </td>         
	    <!-- STOP Main Middle Section -->

 
    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
	</tr>
</table>
<!-- STOP Page -->

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<script language="javascript">
	function navigateAway(url) {		
		window.location = url;
	}		
</script>

<script type="text/javascript"> 
	function deleteIssue(obj){		
		// Used to confirm user wants to delete selected grid items, then submit form
		var frm = document.getElementById(obj);
		//confirm with user
		jConfirm("Delete this issue?<br/>This action cannot be undone.<br/><br/>If <strong>OK</strong>, the following will also be removed:<br/>- Categories<br/>- Case Managers<br/>- Investigations<br/>- Resolutions", "myCM Alert", function(r) {
			if (r==true) {							
					document.getElementById("action").value = "bulkDelAll";
					frm.submit();
			}
			else {
				return false;
			}			
		})		
	}	
</script>

<script type="text/javascript"> 
	function moveIssue(){
		//check for different profile
		if (document.getElementById("profiles").value == '<% =customerID %>'){
			jAlert("Please select a different destination profile.<br />You cannot move this issue to the same profile.");		
			return false;
		}		
		//make sure user is ok with move
		jConfirm("You have elected to move this issue and all its related issues: <br/><br/><strong>" + document.getElementById("profilesName").value + "</strong><br/><br/>Continue with move?", "myCM Alert", function(r) {
			if (r==true) {												
				SimpleModal.open('../_dialogs/popup_move_issue.asp?action=move&cid='+document.getElementById("profiles").value+'&recid=<% =mid(idIssue,1,len(idIssue)-3) & "-01" %>&issuetype=<% =issueType %>', 275, 350, 'no');
			}
			else {
				return false;
			}			
		})		
	}	
</script>

