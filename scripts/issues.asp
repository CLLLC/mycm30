<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     	 website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
Server.ScriptTimeout =25000

const showSearchBox = true
const showDHTMLMenu = true
dim helpID ' had to use "dim" instead of const so could redifine based on pageView
'shortcut list
if len(request.queryString("pageView")) > 0 then
	helpID = 2
'main issues list
else
	helpID = 1
end if
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_includes/_INCchartJS_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = request.queryString("pageView")
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************
																	

'Work Fields
dim I
dim item
dim pageSize
dim totalPages
dim showArr
dim sortField
dim sortOrder
dim curPage
dim issueType

'Query fields
dim showSEARCH
dim showSEARCHfields
dim showField
dim showValue
dim showType

dim optArr, optArrIndex
dim bolRecordsFound

dim listWhereClause, listWhereField, listWhereType
dim listFilterXML
dim listFilterFields
dim listSortOrder
dim listSortField

dim action
action = Request.QueryString("action")
						
'get number of customer of profiles for this user
'if more than 1, then show popup_profile.asp when
'adding new issue
dim popupCustomer
'CCI Staff
if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
	mySQL = "SELECT CustomerID " _
		  & "	FROM Customer " _
		  & "	WHERE Canceled<>-1 "
'everyone else...
else
	mySQL = "SELECT CustomerID " _
		  & "	FROM vwLogins_IssueType " _
		  & "	WHERE LOGID = " & sLogid & " " _
		  & "	GROUP BY CustomerID "
end if
set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,0)
if rs.recordcount <= 1 then		
	popupCustomer = "N"
elseif rs.recordcount > 1 then
	popupCustomer = "Y"
end if
call closeRS(rs)

'*********************************************************
									
'Set Row Colors
dim foreColor, fontWeight
dim rowColor, col1, col2,rowSet
col1 = "#F0F0F0"
col2 = "#FFFFFF"

'Set Number of Items per Page
pageSize = 25

'*********************************************************

'Get type of view being requested
dim tableProperty

dim idIssue
idIssue = Request.QueryString("recid")

'used by addEvents to begin new FUP
CRSID = idIssue

'Get Page to show
curPage = Request.Form("curPage")
if len(curPage) = 0 then
	curPage = Request.QueryString("curPage")
end if

'Get showField from Dynamic DropDowns
showType = Request.Form("showType")
if len(showType) = 0 then
	showType = Request.QueryString("showType")
end if

showSEARCHfields = Request.Form("showSEARCHfields")
if len(showSEARCHfields) = 0 then
	showSEARCHfields = Request.QueryString("showSEARCHfields")
end if
if len(showSEARCHfields) = 0 then
	if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
		showSEARCHfields = "crsid" 'internal CCI/RA
	else
		showSEARCHfields = "all" 'customers default
	end if		
end if

showSEARCH = Request.Form("showSEARCH")
if len(showSEARCH) = 0 then
	showSEARCH = Request.QueryString("showSEARCH")
else
	'form search, reset to page 1
	curPage = 1
end if

'put search request in session
if len(showSEARCH) = 0 then
	session(session("siteID") & "searchBox") = ""
else
	session(session("siteID") & "searchBox") = showSEARCH
	session(session("siteID") & "searchBoxfields") = showSEARCHfields
end if

'Get showField from Dynamic DropDowns
showField = Request.Form("showField")
if len(showField) = 0 then
	showField = Request.QueryString("showField")
else
	'form search, reset to page 1
	curPage = 1
end if

'Get showValue from Dynamic DropDowns
showValue = Request.Form("showValue")
if len(showValue) = 0 then
	showValue = Request.QueryString("showValue")
else
	'form search, reset to page 1
	curPage = 1
end if

'Get sortOrder
sortOrder = Request.Form("sortOrder")
if len(sortOrder) = 0 then
	sortOrder = Request.QueryString("sortOrder")
end if

'Check what we will be sorting the results on
sortField = Request.Form("sortField")
if len(sortField) = 0 then
	sortField = Request.QueryString("sortField")
end if

'*********************************************************

'After attempting to retrieve the search criteria through the various 
'mechanisms above (Form/QueryString/Cookie), check that some of the 
'critical values are valid. If not, set to default values.
if len(curPage) = 0 or not isNumeric(curPage) or isNull(curPage) then
	curPage = 1
else
	'do not allow more than can be held in a long variable
	if len(curPage) > 9 then
		response.redirect ("../default.asp")
	else
		curPage = CLng(curPage)
	end if
end if

'Check what we will be sorting the results on
if len(sortField) = 0 then
	sortField = userSortField
end if
if len(sortField) = 0 or IsNull(sortField) or sortField = "" then
	sortField = "date"
end if

'Get sortOrder
if len(sortOrder) = 0 then
	sortOrder = userSortOrder
end if
if len(sortOrder) = 0 or IsNull(sortOrder) or SortOrder = "" then
	sortOrder = "desc"
end if

'*********************************************************

'if PageView is called get list settings, name and sql statement
dim listID, listSQL, listName, listURL, listDataset, listType, listTable, resultsLimited
dim chartXAxis, chartYAxis
			
'was a page view provided (based on a SQL View)?
if len(pageView) > 0 and lCase(pageView) <> "related" then					

	'Update Run Date
	mySQL = "UPDATE Reports SET LastRunDate='" & Now() & "' " _
		  & "WHERE ReportID = " & pageView
	set rs = openRSexecute(mySQL)
			
	'find requested list/report
	menuSQL="SELECT Reports.ReportID, Reports.Name, Reports.QueryFields, Reports.QueryFilter, Reports.QuerySortOrder, Reports.QuerySortField, Reports.SQL, Dataset.Name AS Dataset, " _
		  & "       Dataset.Type, Dataset.[Table], Reports.ListURLField, Reports.chartXAxis, Reports.chartYAxis " _
		  & "FROM   Reports INNER JOIN " _
		  & "		Dataset ON Reports.DatasetID = Dataset.DatasetID " _
		  & "WHERE (Reports.IssueList='Y') AND (Reports.ReportID = " & pageView & ") AND (Reports.LogID=" & sLogid & ") AND (Reports.CustomerID = '" & sCustomerID & "') "					
	'open recordset
	set menuRS = openRSopen(menuSQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
				
	'collect list/report details				
	if not menuRS.eof then
		listID = menuRS("reportid")					
		listDataset = menuRS("dataset")
		listFilterFields = menuRS("queryfields")
				
		if lCase(showType)="register" and len(showField)>0 and len(showValue)>0 then
			listType = showType
			if inStr(lCase(listFilterFields),"issue_#")<=0 then
				listFilterFields = "[Issue_#], " & listFilterFields
			end if
		else		
			listType = menuRS("type")
		end if	
						
		listTable = menuRS("table")
		listSortOrder = menuRS("querysortorder")
		listSortField = menuRS("querysortfield")
		listFilterXML = menuRS("queryfilter")
		listSQL = menuRS("sql")
		chartXAxis = menuRS("chartXAxis")
		chartYAxis = menuRS("chartYAxis")

		listName = menuRS("name")
		listURL = menuRS("listURLfield")
		if lCase(listType) <> "sql" then
			listURL = replace(listURL & "","[","")										
			listURL = replace(listURL & "","]","")					
		end if
					
		'construct SQL statement
		if lCase(listType) <> "sql" then
			listSQL = BuildQuery(listDataset, listFilterFields, listSortField, listSortOrder, listFilterXML, listTable, listType, chartXAxis, chartYAxis)
			'check for existance of filter, so we can tell user
			if ValidXMLQuery(listTable,listFilterXML) = "Yes" then resultsLimited="Yes"

			'-------------------------------------------------------------
			'for customers...ensures user can only view specific customers 
			'and issue types pulled from vwLogins_IssueType							
			'-------------------------------------------------------------
			'DBAdmin and CCI Admins
			if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
				'do nothing...			
			'CCI RS/RA
			elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
				listSQL = insertWhereClause(listSQL,"IssueTypeID<>3")				
			'everyone else...
			else
				'double single quotes needed for crosstab stored procedure
				if lCase(listType) = "calculated" and len(chartYAxis) > 0 then
					listSQL = insertWhereClause(listSQL,"CustomerID IN (" & replace(profileIDs,"'","''") & ") and LogIDView=" & sLogid & "")
				'does not use stored procedure so double single quotes NOT needed					
				else				
					listSQL = insertWhereClause(listSQL,"CustomerID IN (" & profileIDs & ") and LogIDView=" & sLogid & "")
				end if						
			end if
			'-------------------------------------------------------------

			'-------------------------------------------------------------
			'user is drilling down into specific issues from a calculated
			'report type
			'-------------------------------------------------------------						
			if lCase(showType)="register" and len(showField)>0 and len(showValue)>0 then
				listSQL = insertWhereClause(listSQL,showField & "='" & showValue & "'")
			end if

			'-------------------------------------------------------------
			'should be pointing to a User SPECIFIC SQL VIEW, 
			'not for CCI staff. makes sure user only sees assigned issues
			'-------------------------------------------------------------
			if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then			
				if programAdmin = "N" then
					listSQL = insertWhereClause(listSQL,"LOGID=" & sLogid)
				end if
			end if
								
		'prepare and correct replacement tags
		elseif lCase(listType) = "sql" then
			listSQL = replace(listSQL,"#LOGID#",sLogid)
			listSQL = replace(listSQL,"#CUSID#",sCustomerID)
						
		end if
						
	'report not found, send home
	else	
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("ERROR: Report not found.")	
	end if			
	call closeRS(menuRS)
				
elseif len(pageView) <= 0 and lCase(pageView) <> "related" then								
	listID = 1
	'check for existance of filter, so we can tell user
	if ValidXMLQuery(userIssuesView,userWhereClause) = "Yes" then resultsLimited="Yes"

elseif lCase(pageView) = "related" then
	
	'xxx
	dim tmpCRSID
		
	tmpCRSID  = left(idIssue,len(idIssue)-3)
	if right(tmpCRSID,1) = "-" then '1304-GHSG-10001-100
		tmpCRSID = left(tmpCRSID,len(tmpCRSID)-1)
	end if

	mySQL = "SELECT CRS.CustomerID, CRS.CRSID, CRS.IssueTypeID " _
		 &  "	FROM CRS " _
		 &  "	WHERE CRS.CRSID = '" & tmpCRSID & "-01' "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	'get issue type used to determine FUP availability
	CustomerID = rs("customerid")
	issueType = rs("IssueTypeID")
	'close recordset
	call closeRS(rs)

end if


'*********************************************************

dim pageTitle
if len(pageView) > 0 and lCase(pageView) <> "related" then
	pageTitle = listName & " (list)"
elseif lCase(pageView) = "related" then
	pageTitle = left(idIssue,len(idIssue)-3) & " (related issues)" 
else
	pageTitle = "Issues" 
end if

%>
<!--#include file="../_includes/_INCheaderLite_.asp"-->


<!-- START Page Loader and inform user content being loaded -->
<div id="loaderContainer" style="margin-top:15px; margin-left:15px; border:1px solid #B3B3B3; background-color:#FFFFFF; padding:2px; width:225px;">
	<img src="../_images/myloader.gif" alt="loading..." align="absmiddle" style="margin-left:5px; margin-right:10px; vertical-align:middle;" />Loading content, please wait...
</div>
<%
'push to window right now!
response.flush()
%>
<!-- STOP Page Loader -->


<!-- START Page -->
<table id="masterTable" style="background:#FFF; " width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
        
        <!-- START Left sided area -->
		<td width="190" align="right" valign="top">

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
					<a href="../profiles/users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>                        
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided tree menu -->
              <tr>
                <td align="right" valign="top" style="padding-left:7px; padding-top:10px;">

                    <!-- START side menu include -->                                  
					<!--#include file="../scripts/tree/tree.asp"-->                                  
                    <% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>      
	                    <script>
							stExpandSubTree('myCMtree',2,0)
   		                	// --> SET TO stExpandSubTree('myCMtree',1,0); WHEN SUB-MENUS ARE BACK OFF
       		            </script>
            		<% else %>
	                    <script>
   		                	stExpandSubTree('myCMtree',1,0);
       		            </script>
                    <% end if %>
                    <!-- STOP side menu include -->

                </td>
              </tr>
              <!-- END Left sided tree menu -->
        
            </table>
        
        </td>
        <!-- STOP Left sided area -->
            
	    <!-- START Main Middle Section -->
		<td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">      

			<%
			'used to STOP updating the cookie crumb if page is being recalled from a SAVE
			dim updateCrumb
            if len(session(session("siteID") & "errMsg")) > 0 or len(session(session("siteID") & "okMsg")) > 0 then
				updateCrumb = "false"
            end if
			%>
            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs('<% =updateCrumb %>'));
       	        </script>
            </div>
                    
            <div style="text-align:left; margin-bottom:10px;">
                <% if lCase(pageView) = "related" then %>                
	                <img src="../_images/icons/32/table_related.png" title="Issues" width="32" height="32" align="absmiddle" style="vertical-align:middle;"> <span class="pageTitle">Issues<% if len(listName)>0 then response.write(": " & listName) %></span>
				<% elseif lCase(listType) = "register" then %>
  	                <img src="../_images/icons/32/table.png" title="Issues" width="32" height="32" align="absmiddle" style="vertical-align:middle;"> <span class="pageTitle">Issues<% if len(listName)>0 then response.write(": " & listName) %></span>							
				<% elseif lCase(listType) = "calculated" then %>
  	                <img src="../_images/icons/32/table_sum.png" title="Issues" width="32" height="32" align="absmiddle" style="vertical-align:middle;"> <span class="pageTitle">Issues<% if len(listName)>0 then response.write(": " & listName) %></span>							
				<% elseif lCase(listType) = "sql" then %>				
  	                <img src="../_images/icons/32/table_link.png" title="Issues" width="32" height="32" align="absmiddle" style="vertical-align:middle;"> <span class="pageTitle">Issues<% if len(listName)>0 then response.write(": " & listName) %></span>				
				<% else %>
  	                <img src="../_images/icons/32/table_page.png" title="Issues" width="32" height="32" align="absmiddle" style="vertical-align:middle;"> <span class="pageTitle">Issues<% if len(listName)>0 then response.write(": " & listName) %></span>
				<% end if %>
            </div>                   
                              
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
                        <legend>Information</legend>
						  <div align="left">          
                              
                        	<!-- Check if we need to show Chart Option for calculated queries -->          
							<% if lCase(listType) = "calculated" then 
								dim chartCookie
								chartCookie = request.cookies("myCM3-Charts")
								%>
                               	<div align="left" style="padding-left:0px; padding-top:3px; margin:0px;">
    								<a href="#" onClick="this.blur(); SimpleModal.open('../_dialogs/popup_report_chart.asp?recid=<% =pageView %>&action=edit', 300, 400, 'no'); return false;"><img src="../_images/icons/16/chart_pie.png" title="View Chart" width="16" height="16" align="absmiddle" style="margin-right:3px; vertical-align:middle;" border="0"></a><a href="#" onClick="this.blur(); SimpleModal.open('../_dialogs/popup_report_chart.asp?recid=<% =pageView %>&action=edit', 300, 400, 'no'); return false;">Charting</a> is <a href="#" onClick="this.blur(); setChartCookie(); window.location.reload(); return false;"><strong><% if chartCookie = "checked" then response.write("ON") else response.write("OFF") %></strong></a> and available for viewing on this issue list.
                                    <input name="checkChart" id="checkChart" type="checkbox" style="vertical-align:middle; display:none;" <% response.write(chartCookie) %> />
                                </div>                            
							<% else %>
                                <img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; vertical-align:middle;">Below is a list of issues. Click on 'Issue #' to view details.
                        	<% end if %>
                            
							<% response.write( getInlineHelp("getPageURL", 0) ) %>                            
                            
                            <!-- Check for XML Filter and inform user if necessary -->
                            <% if resultsLimited="Yes" then %>
                               	<div align="left" style="padding-left:0px; padding-top:3px; margin:0px;">
                                	<% if len(pageView) > 0 then %>
                                		<a href="#" onClick="this.blur(); SimpleModal.open('../_dialogs/popup_report_filter.asp?recid=<% =pageView %>&action=edit', 350, 700, 'yes'); return false;"><img src="../_images/icons/16/exclamation.png" width="16" height="16" align="absmiddle" style="margin-right:3px; vertical-align:middle;" border="0"></a><a href="#" onClick="this.blur(); SimpleModal.open('../_dialogs/popup_report_filter.asp?recid=<% =pageView %>&action=edit', 350, 700, 'yes'); return false;">Filters</a> have be applied which may limit results. Edit filter to change results.
	                                <% else %>
		                            	<a href="#" onClick="this.blur(); SimpleModal.open('../_dialogs/popup_report_filter.asp?recid=0&action=edit', 350, 700, 'yes'); return false;"><img src="../_images/icons/16/exclamation.png" width="16" height="16" align="absmiddle" style="margin-right:3px; vertical-align:middle;" border="0"></a><a href="#" onClick="this.blur(); SimpleModal.open('../_dialogs/popup_report_filter.asp?recid=0&action=edit', 350, 700, 'yes'); return false;">Filters</a> have be applied which may limit results. Edit filter to change results.
        	                        <% end if %>
                                </div>
                                
							<% end if %>             

							<% 
							'GOOD FOR DEBUGGING...turn on when need to view current sql statement on issue list or chart
							if cLng(session(session("siteID") & "adminLoggedOn")) < 2 and 1=2 then
								%>                                
                               	<div align="left" style="padding-left:0px; padding-top:3px; margin:0px;">                                
                           			<a href="#" onClick="this.blur(); SimpleModal.open('../error/popup_default.asp?errMsg=<% =server.URLEncode(listSQL) %>', 350, 700, 'yes'); return false;"><img src="../_images/icons/16/table_link.png" title="Edit Filter" width="16" height="16" align="absmiddle" style="margin-right:3px; vertical-align:middle;" border="0"></a><a href="#" onClick="this.blur(); SimpleModal.open('../error/popup_default.asp?errMsg=<% =server.URLEncode(listSQL) %>', 350, 700, 'yes'); return false;">SQL Statement</a>.
                      			</div>
								<%
							end if 
							%>                                    
                                                        
                          </div>                          
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             
                                    
                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">
                    
                    	<%
						'something other than the main issue list was requested
						if len(pageView) > 0 then
							'anything BUT follow-ups
                        	if pageView <> "related" then 
								%>                            
                            	<div class="infoButtons">
	            	                <a href="#" onClick="javascript:popup('../reports/reports_print.asp?pageview=<% =listID %>',600,600); return false;"><img src="../_images/icons/24/printer.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
    	    	                    <a href="#" onClick="javascript:popup('../reports/reports_print.asp?pageview=<% =listID %>',600,600); return false;">Print</a>        	                        
           	                    </div>
                                
                                <% 
								'this user CAN edit report so they will download XLS from the edit screen and NOT here
								if editReport="Y" then 
									%>
	                                <div class="infoButtons">
   		                            	<a href="../reports/reports_edit.asp?action=edit&recID=<% =listID %>&return=sublist"><img src="../_images/icons/24/chart_bar_edit.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
       		                            <a href="../reports/reports_edit.asp?action=edit&recID=<% =listID %>&return=sublist">Edit</a>
            	                    </div>                                                   
                                	<% 
								'this user CANNOT edit report so they need to be able to download results here
								else 
									%>                                
                                    <div class="infoButtons">
                                        <a href="../reports/reports_export.asp?pageview=<% =listID %>&action=csv"><img src="../_images/icons/24/file_extension_xls.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                        <a href="../reports/reports_export.asp?pageview=<% =listID %>&action=csv">Excel (.csv)</a>
                                    </div>                   
                                	<% 
								end if 
								%>

                                <div class="infoButtons">
                                    <a href="#" onClick="this.blur(); SimpleModal.open('../_dialogs/popup_report_filter.asp?recid=<% =pageView %>&action=edit', 350, 700, 'yes'); return false;"><img src="../_images/icons/24/filter.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                    <a href="#" onClick="this.blur(); SimpleModal.open('../_dialogs/popup_report_filter.asp?recid=<% =pageView %>&action=edit', 350, 700, 'yes'); return false;">Filter</a>
                                </div>
                                
                        		<% 
							'show follow-ups table
							elseif pageView = "related" then 
								'customers NOT allowed to add FUP to issues 1 or 2
								if issueType<>"3" and cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then	
									%>
                                    <div class="infoButtons">
                                        <img src="../_images/icons/24/page_white_add_disabled.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/>
                                        Add Follow-Up
                                    </div>                            
    	                			<% 
								else
									%>
                                    <div class="infoButtons">
                                        <a href="<% =session(session("siteID") & "issuesEdit") %>?action=add"><img src="../_images/icons/24/page_white_add.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                        <a href="<% =session(session("siteID") & "issuesEdit") %>?action=addfu&recId=<% =idIssue %>">Add Follow-Up</a>
                                    </div>                            
    	                			<%
								end if 
							end if
							
						'main issue list called
						else	
											
							'modify/edit main list and columns :: users security greater than 50 NOT ALLOWED
							if cLng(session(session("siteID") & "adminLoggedOn")) < 50 then
								%>
                                
								<% 
                                'for those who can and cannot add new/import issues
                                if cLng(session(session("siteID") & "adminLoggedOn")) <= 2 or addNewIssue="Y" then %>                                
                                    <div class="infoButtons">
                                        <% if popupCustomer = "Y" then %>
                                            <a href="#" onClick="SimpleModal.open('../_dialogs/popup_profile.asp', 430, 700, 'no'); return false;"><img src="../_images/icons/24/page_white_add.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                            <a href="#" onClick="SimpleModal.open('../_dialogs/popup_profile.asp', 430, 700, 'no'); return false;">Add Issue [F2]</a>
                                        <% else %>
                                            <a href="<% =session(session("siteID") & "issuesEdit") %>?cid=<% =sCustomerID %>&action=add"><img src="../_images/icons/24/page_white_add.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                            <a href="<% =session(session("siteID") & "issuesEdit") %>?cid=<% =sCustomerID %>&action=add">Add Issue [F2]</a>
                                        <% end if %>
                                    </div>                                    
                                    <% 
                                    'CAN TURN ON FOR CUSTOMER...after used by CCI for awhilte
                                    if cLng(session(session("siteID") & "adminLoggedOn")) <= 2 or sLogid="307292" then %>
                                        <div class="infoButtons">
                                            <a href="#" onClick="javascript:parent.location='../profiles/issue_import.asp?cid=<% =sCustomerID %>'; return false;"><img src="../_images/icons/24/table_import.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                            <a href="#" onClick="javascript:parent.location='../profiles/issue_import.asp?cid=<% =sCustomerID %>'; return false;">Import</a>
                                        </div>
                                    <% end if %>
                               	<% end if %>
                                
                                <div class="infoButtons">
                                    <a href="../reports/reports_edit.asp?action=edit&recID=0&return=issuelist"><img src="../_images/icons/24/table_edit.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                    <a href="../reports/reports_edit.asp?action=edit&recID=0&return=issuelist">Edit</a>
                                </div>
                                
                               	<% 
							'this user CANNOT edit issue list so they need to be able to download AND print results here
							else 
								%>             
                                <div class="infoButtons">
                                    <a href="#" onClick="javascript:popup('../reports/reports_print.asp?pageview=0',600,600); return false;"><img src="../_images/icons/24/printer.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                    <a href="#" onClick="javascript:popup('../reports/reports_print.asp?pageview=0',600,600); return false;">Print</a>        	                        
                                </div>                                                                           
                                <div class="infoButtons">
                                    <a href="../reports/reports_export.asp?pageview=issues&action=csv"><img src="../_images/icons/24/file_extension_xls.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                    <a href="../reports/reports_export.asp?pageview=issues&action=csv">Excel (.csv)</a>
                                </div>                                                                   
	                    		<%
							end if
							%>
                            
                            <div class="infoButtons">
								<a href="#" onClick="this.blur(); SimpleModal.open('../_dialogs/popup_report_filter.asp?recid=0&action=edit', 350, 700, 'yes'); return false;"><img src="../_images/icons/24/filter.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                <a href="#" onClick="this.blur(); SimpleModal.open('../_dialogs/popup_report_filter.asp?recid=0&action=edit', 350, 700, 'yes'); return false;">Filter</a>
                            </div>
							
							<%							
						end if
						%>
                                                
                    </td>
                    <!-- STOP Information buttons -->
                        
                </tr>
            </table>        
            <!-- END information box -->
    
            <%
			if len(session(session("siteID") & "okMsg")) > 0 then			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))
            end if
			call clearSessionMessages()
            %>        
            
            <!-- START Building Data Grids -->
            <%
			'was a page view provided?
			if len(pageView) > 0 then
					
				'show related issues to currently viewed issue
				if pageView = "related" then

					'DBAdmin and CCI Admins
					'if cLng(session(session("siteID") & "adminLoggedOn")) < 3 or ((lcase(sCustomerID)="demo" or lcase(sCustomerID)="cha" or lcase(sCustomerID)="bsw" or lcase(sCustomerID)="bswh") and cLng(session(session("siteID") & "adminLoggedOn")) >= 3 ) then
					if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and (lcase(sCustomerID)="demo" or lcase(sCustomerID)="cha" or lcase(sCustomerID)="bsw" or lcase(sCustomerID)="bswh") then
					
						'mySQL = "SELECT CRS.CRSID AS [Issue #], CRS.Date, CRS.CallBack, CRS.Severity, CRS.Anonymous " _
						'	 &  "	FROM CRS " _
						'	 &  "	WHERE CRS.CRSID Like '" & left(idIssue,len(idIssue)-3) & "%' " _
						'	 &  "   ORDER BY CRS.CRSID ASC "
						'mySQL = mySQL						
						
						'DONE...
						mySQL = "SELECT CRS.CRSID AS [Issue #], CRS.Date, CRS.CallBack, CRS.Status, 'Series' AS [Relationship] " _
							  & "	FROM CRS " _
							  & "	WHERE CRS.CRSID Like '" & left(idIssue,len(idIssue)-3) & "%' " _							  
							  & " UNION " _
							  & "SELECT CRS.CRSID AS [Issue #], CRS.Date, CRS.Callback, CRS.Status, 'Primary' AS [Relationship] " _
							  & "	FROM CRS INNER JOIN CRS_Related ON CRS.CRSID = CRS_Related.CRSID " _
							  & "	WHERE CRS_Related.RelatedCRSID = '" & idIssue & "' " _							  
							  & " UNION " _
							  & "SELECT CRS.CRSID AS [Issue #], CRS.Date, CRS.Callback, CRS.Status, 'Sub' AS [Relationship] " _
							  & "	FROM CRS INNER JOIN CRS_Related ON CRS.CRSID = CRS_Related.RelatedCRSID " _
							  & "	WHERE CRS_Related.CRSID = '" & idIssue & "' " _
							  & "   ORDER BY [Issue #] ASC "
						mySQL = mySQL	
						
					'CCI RS/RA
					elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
						'DONE...
						mySQL = "SELECT CRS.CRSID AS [Issue #], CRS.Date, CRS.CallBack, CRS.Severity, CRS.Anonymous, 'Series' AS [Relationship] " _
							 &  "	FROM CRS " _
							 &  "	WHERE CRS.CRSID Like '" & left(idIssue,len(idIssue)-3) & "%' AND CRS.IssueTypeID<>3 " _
							 &  "   ORDER BY CRS.CRSID ASC "
						mySQL = mySQL
						
					'everyone else...
					else
						'NOT DONE... need to add related issues for sub users					
						mySQL = "SELECT CRS.CRSID AS [Issue #], CRS.Date, CRS.CallBack, CRS.Status, 'Series' AS [Relationship] " _
							 &  "	FROM CRS INNER JOIN vwLogins_IssueType ON (CRS.CustomerID = vwLogins_IssueType.CustomerID) AND (CRS.IssueTypeID = vwLogins_IssueType.IssueTypeID) " _
							 &  "	WHERE CRS.CRSID Like '" & left(idIssue,len(idIssue)-3) & "%' AND CRS.Progress=3 AND vwLogins_IssueType.LOGID = " & sLogid & " " _
							 &  "   ORDER BY CRS.CRSID ASC "
							 
						mySQL = mySQL
						
					end if

			        set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)										
					
					'99999
					%>
					<!-- START Related Issue table -->
					<table id="relatedTable" width="100%" style="background-color:#969696;" cellpadding=0 cellspacing=0>
								
						<thead>
                        	<% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>                        
                                <tr>
                                    <th align="left" nowrap="nowrap">&nbsp;</th>
                                    <th align="left" nowrap="nowrap">Issue #</th>
                                    <th align="left" nowrap="nowrap">Date</th>
                                    <th align="left" nowrap="nowrap">CallBack</th>                                
                                    <th align="left" nowrap="nowrap">Severity</th>                                                      
                                    <th align="left" nowrap="nowrap">Relationship</th>
                                </tr>
                            <% else %>
                                <tr>
                                    <th align="left" nowrap="nowrap">&nbsp;</th>
                                    <th align="left" nowrap="nowrap">Issue #</th>
                                    <th align="left" nowrap="nowrap">Date</th>
                                    <th align="left" nowrap="nowrap">CallBack</th>                                
                                    <th align="left" nowrap="nowrap">Status</th>                                                      
                                    <th align="left" nowrap="nowrap">Relationship</th>
                                </tr>                            
                            <% end if %>                            
						</thead>

						<tbody>
		
						<%
						do until rs.eof
								
							'START current row
							response.write("<tr>")									
							
							'ICONS
							response.Write("<td width=""39"" nowrap=""nowrap"" align=""center"">")
								if lcase(rs("Relationship")) = "series" or lcase(rs("Relationship")) = "primary" then
									response.Write("<img src=""../_images/icons/16/cancel_disabled.png"" title=""Delete Link"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:7px; vertical-align:middle;"" />")								
								else
									response.Write("<a href=""#"" onClick=""confirmPrompt('Confirm Delete','This action can not be undone.<br><br>Are you sure you want to delete this link?','redirect','issues_exec.asp?recid=" & idIssue & "&recrelated=" & rs("issue #") & "&action=dellink&cid=" & customerid & "'); return false;"" ><img src=""../_images/icons/16/cancel.png"" title=""Delete Link"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:7px; vertical-align:middle;"" /></a>")
								end if
								
								response.Write("<a href=""#"" onClick=""window.location='" & session(session("siteID") & "issuesEdit") & "?action=edit&recid=" & rs("issue #") & "';"" ><img src=""../_images/icons/16/page_blue_edit.png"" title=""Open Location"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; vertical-align:middle;""></a>")
							response.Write("</td>")											
								
							'Issue link
							response.Write("<td align=""left"" style=""cursor:pointer;"" onClick=""window.location='" & session(session("siteID") & "issuesEdit") & "?action=edit&recid=" & rs("issue #") & "'; return false;"" >")
								response.write("<a style=""color:#333333;"">" & rs("Issue #") & "</a>")
							response.Write("</td>")
							
							'Additional fields
							response.Write("<td align=""left"">" & rs("Date") & "</td>")
							response.Write("<td align=""left"">" & rs("CallBack") & "</td>")								
							
							response.Write("<td align=""left"">")
								if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
									response.Write(rs("Severity"))								
								else
									call colorStatusBox(rs("Status"))
									response.Write(rs("Status"))
								end if
							response.Write("</td>")
							
							response.Write("<td align=""left"">" & rs("Relationship") & "</td>")													
							
							'CLOSE current row						
							response.write("</tr>")
							
							rs.movenext
						loop
						%>					
		
						</tbody>
					</table>
		
					<%
					'set _tablefilter/default.asp settings.
					tableProperty = "sort: true, sort_config:{sort_types:['String','String','date','mdydate','String','String']}, filters_row_index: 1, " _
								  & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
								  & "status_bar: true, col_0: """", col_1: ""none"", col_2: ""none"", col_3: ""none"", col_4: ""select"", col_5: ""select"", " _
								  & "col_width:[null,""35%"",null,null,null,null], paging: true, paging_length: 25, " _
								  & "highlight_keywords: true, " _
								  & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
								  & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"
								  
								  
					%>
					<!-- STOP Location table -->
		
					<!-- Fire table build -->
					<script language="javascript" type="text/javascript">
						//<![CDATA[
						var tableProp = {<% =tableProperty %>};
						//initiate table setup
						var tf1 = setFilterGrid("relatedTable",tableProp);
						//]]>
					</script>
		
					<!-- Shadow table -->
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td height="5px" style="background:#D0D0D0;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
					  </tr>
					</table>
					
					<%					
					'buttons Add Follow-Up and Sync Status
					response.write("<div style=""padding-top:5px;"">")

					'customer NOT allowed to add FUP to issues 1 or 2
					if issueType<>"3" and cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then	
						'no FUP buttong
					else
						response.write("  <a class=""myCMbutton"" href=""" & session(session("siteID") & "issuesEdit") & "?action=addfu&recID=" & left(idIssue,len(idIssue)-3) & "-01" & """ onclick=""this.blur();""><span class=""add"" style=""padding-right:7px;"">Add Follow-Up</span></a>")
					end if									
					
					if cLng(session(session("siteID") & "adminLoggedOn")) <= 30 and (lcase(sCustomerID)="demo" or lcase(sCustomerID)="cha" or lcase(sCustomerID)="bsw" or lcase(sCustomerID)="bswh") then
						response.write("  <a class=""myCMbutton"" href=""#"" onclick=""this.blur(); SimpleModal.open('../_dialogs/popup_issue.asp?cid=" & CustomerID & "&recID=" & idIssue & "&pageview=" & pageView & "', 525, 800, 'no'); return false;""><span class=""link"" style=""padding-right:7px;"">Link Issue</span></a>")
					end if

					response.write("  <a class=""myCMbutton"" href=""#"" onclick=""this.blur(); SimpleModal.open('../_dialogs/popup_syncfup.asp?recid=" & idIssue & "', 450, 600, 'yes'); return false;""><span class=""sync"" style=""padding-right:7px;"">Sync Issues</span></a>")

					response.write("</div>")
					
				'not "related" view so look up list query in database 					
				else

                	if lCase(listType) = "calculated" and chartCookie = "checked" then 
						''' - ORIGINAL: call buildChart(listID,"issues","swf",listSQL,listWhereClause)
						'NOT SURE --> listWhereClause <-- IS NEEDED												
						call buildChart(listID,"issues","swf",listSQL,listWhereClause)
					end if
				
					if lCase(listType) = "sql" then
						listURL = "SQL" 				'<-- so links back to issues.asp are turned off
						tableProperty = "grid: false, "	'<-- used to turn OFF the filter bar for SQL statement
					else
						tableProperty = ""
					end if
					
					'set _tablefilter/default.asp settings.
					tableProperty = "sort: true, filters_row_index: 1, " & tableProperty _
								  & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
								  & "status_bar: true, paging: true, paging_length: 25, " _
								  & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", enable_empty_option: true, " _
								  & "enable_non_empty_option: true, or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, " _
								  & "highlight_keywords: true, loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
								  & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"
		
					'crosstab grid
                	if lCase(listType) = "calculated" then
					
						'use sp_crosstab
						if len(chartYAxis) > 0 then
							''' PUT BACK AFTER YOU FIGURE OUT WHY THE LINK IS NOT WORKING --> call buildCrosstab("issueTableID",listSQL,"",listURL,"../_images/icons/16/page_blue_go.png",tableProperty,chartXAxis)
							call buildCrosstab("issueTableID",listSQL,"",listURL,"../_images/icons/16/page_blue_go.png",tableProperty,chartXAxis)
							
						'use regular GROUP BY sql
						else						
							''' PUT BACK AFTER YOU FIGURE OUT WHY THE LINK IS NOT WORKING --> call buildGrid("issueTableID",listSQL,"",listURL,"../_images/icons/16/page_blue_go.png",tableProperty)
							'''999
							call buildGrid("issueTableID",listSQL,"",listURL,"../_images/icons/16/page_blue_go.png",tableProperty)
							
							''' ORIGINAL call buildGrid("issueTableID",listSQL,"",listURL,"",tableProperty)
							
						end if

					'register grid
					elseif lCase(listType) = "register" then
						call buildGrid("issueTableID",listSQL,"",listURL,"../_images/icons/16/page_blue_go.png",tableProperty)

					'sql command
					else
						set rs = openRSopen(gridSQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
						response.write ("<div class=""required"" style=""padding-top:40px; padding-left:40px;""><strong>SQL COMMAND EXECUTED on " & Date() & " at " & Time() & "</strong></div>")
											
					end if
					
				end if									
			
			'no pageView provided, show Issue List...
			else
			%>
            
                <!-- START Default Data Grid -->
                <!--#include file="../_includes/_INCissues_.asp"-->            
                <!-- STOP Default Data Grid -->    
			
			<%			
			end if					
			%>
            <!-- STOP Building Data Grids -->
                  
			<br>
        
        </td>
    	<!-- STOP Main Middle Section -->

    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
  	</tr>
</table>
<!-- STOP Page -->

<script>
	//REFERENCE: <div> "loaderContainer" at top of page
	document.getElementById('loaderContainer').style.display = 'none';	//hide page loader
	document.getElementById('masterTable').style.display = '';			//display table holding page content
</script>

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<script language="javascript">	
	function pageReload() {  	
	 	window.location.reload();
    }
	function navigateAway(url) {
		window.location = url;
	}		
</script>	

<script language="javascript">	
function deleteIssues(obj,itemType) {		 
	// Used to confirm user wants to delete selected grid items, then submit form
	var bln;
	var frm = document.getElementById(obj);
	// cycle through all form elements, if checkbox is it checked?
	for(i=0; i<frm.elements.length; i++)
	{
		if(frm.elements[i].type=="checkbox")
		{
			if (frm.elements[i].checked)
			{
				bln = true;
			}
		}
	}
	// if no checkboxes are checked, tell user
	if (!bln)
	{
		jAlert("No " + itemType + " were selected.", "myCM Alert");		
		return;
	}
	// if checked checkbox found, confirm with user to delete/submit form
	jConfirm("Delete the selected " + itemType + "?<br/>This action cannot be undone.", "myCM Alert", function(r) {
		if (r==true) {
						
			// if checked checkbox found, confirm with user to delete/submit form
			jConfirm("Delete all associated history and records linked to issue?<br/><br/>If <strong>OK</strong>, the following will also be removed:<br/>- Categories<br/>- Case Managers<br/>- Investigations<br/>- Resolutions", "myCM Alert", function(r) {
				//delete issue from all tables
				if (r==true) {
					document.getElementById("action").value = "bulkDelAll";
					frm.submit();										
				}
				//only remove from CRS table
				else {
					//frm.submit();
					return false;					
				}			
			})
						
		}
		else {
			return false;
		}			
	})

}
</script>	


<script>
	function addLinkedIssue(crsid,date,callback,status,type){

		var editPage = '<% =session(session("siteID") & "issuesEdit") %>';	

		//add new linked issue
		var tempHTML;

		// create randon numbers used to create unique element IDs for idQueryValue form fields
		var valueID_1 = Math.floor(Math.random()*1001);
				  
		// create table body
		var table = document.getElementById("relatedTableID");
		var tbody = table.getElementsByTagName("tbody")[0];
				
		// ----- ADD ROW -----
		// create row element and assign unique ID
		var rowCount = table.rows.length;
		var rowID = "issue:" + crsid + "_tr";				
		var row = document.createElement("tr");
		row.id = rowID;

		// ----- ADD CELL #1 -----	
		var td1 = document.createElement("td")					
		td1.id = "issue:" + crsid + "_crsid";
		td1.style.textAlign = "left";			
		tempHTML = "<div style=\"width:100%;\">";
		tempHTML = "<a href=\"#\" onClick=\"delLink(" + crsid + "); resetSystemMsg(\'systemMessage\'); return false;\"><img src=\"../_images/icons/16/cancel.png\" title=\"Delete Link\" border=\"0\" width=\"16\" height=\"16\" align=\"absmiddle\" style=\"padding-top:1px; padding-bottom:1px; padding-right:7px; vertical-align:middle;\"></a>";
		tempHTML += "<a href=\"#\" onClick=\"window.location='" + editPage + "?action=edit&recid=" + crsid + "'; return false;\"><img src=\"../_images/icons/16/page_blue_edit.png\" title=\"Open Item\" border=\"0\" width=\"16\" height=\"16\" align=\"absmiddle\" style=\"padding-top:1px; padding-bottom:1px; vertical-align:middle;\"></a>";
		tempHTML += "<a hre=\"#\" onClick=\"window.location='" + editPage + "?action=edit&recid=" + crsid + "'; return false;\" style=\"color:#333333; cursor:pointer; padding-left:5px;\">" + crsid + "</a>";
		tempHTML += "</div>";
		td1.innerHTML = tempHTML;				


		// ----- ADD CELL #2 -----	
		var td2 = document.createElement("td")			
		td2.id = "issue:" + crsid + "_date";				
		td2.style.textAlign = "left";
		td2.innerHTML = date;

		// ----- ADD CELL #3 -----
		var td3 = document.createElement("td")			
		td3.id = "issue:" + crsid + "_callback";				
		td3.style.textAlign = "left";
		td3.innerHTML = callback;
				
		// ----- ADD CELL #4 -----	
		var td4 = document.createElement("td")			
		td4.id = "issue:" + crsid + "_status";				
		td4.style.textAlign = "left";				
		td4.innerHTML = status;			

		// ----- ADD CELL #5 -----	
		var td5 = document.createElement("td")			
		td5.id = "issue:" + crsid + "_type";				
		td5.style.textAlign = "left";				
		//tempHTML = status;
		td5.innerHTML = type;							

		// ----- FINILIZE AND APPEND NEW CELLS AND ROW -----
		row.appendChild(td1);
		row.appendChild(td2);
		row.appendChild(td3);
		row.appendChild(td4);
		row.appendChild(td5);
						
		// append row to table
		tbody.appendChild(row);
							
		// hide "no documents" message if exists
//		if (document.getElementById('NoResolutionTable')) {
//			document.getElementById('NoResolutionTable').style.display = "none";				
//		}
		//make sure table is visible
//		if (document.getElementById('resolutionTable').style.display == "none") {
//			document.getElementById('resolutionTable').style.display = "";
//		}				
		//refresh grid
		tf1.RefreshGrid();		
		
		//resetSystemMsg('systemMessage'); 
		//autoPostForm('no');
		
	}
</script>




