<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->

<%
'Database
dim mySQL, cn, rs, rs2

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'validate security access
if loadUser(null) = false then
	response.write("Error: Your session has expired! To resume please <strong><a href=""#"" title=""Login to myCM"" onclick=""SimpleModal.open('../logon_dialog.asp', 243, 395, 'no'); StopTheTimer(); resetSystemMsg('systemMessage');"">Login to myCM</a></strong>.")
	response.end
end if
'*********************************************************

'Issues
dim idIssue

'fields from DETAILS tab
dim	CRSID
dim customerID
dim	rptRSID
dim	rptQAID
dim issueType
dim rptModifiedDate
dim	rptDate
dim	rptTime
dim	rptMonth
dim rptQuarter
dim	rptYear
dim	severityLevel
dim	firstTime
dim	commTool
dim	callBack
dim	anonCaller
dim interpreterUsed
dim interpreterNumber
dim language
dim	resGiven
dim	locName, locNameID
dim	locAddr
dim	locCity
dim	locState
dim	locZip
dim locCountry
dim locHierarchy	'used to parse location levels
dim locLevel_1
dim locLevel_2
dim locLevel_3
dim locLevel_4
dim locLevel_5
dim locLevel_6
dim locLevel_7
dim locLevel_8
dim callerType
dim	callerFirst
dim	callerLast
dim callerTitle
dim	callerAddr
dim	callerCity
dim	callerState
dim	callerZip
dim	callerHome
dim	callerWork
dim callerCell
dim callerFax
dim callerOther
dim callerPager
dim callerEmail
dim callerBestTime
dim callerPassword
dim	rptSummary
dim	rptDetails
dim	rptAddendum

'fields from NOTES tab
dim rptStatus
'dim rptStatusOld
dim rptSource
'dim rptRespGroup
dim rptCaseNotes
dim rptDateClosed
'dim rptCaseType
'dim rptInvestigationStatus
'dim rptInvestigationStatusOld
'dim rptInvestigation
'dim rptPrivNotes
dim rptResolution
dim rptResSatisfaction
'dim rptResolutionOld
dim rptResAppDate
dim rptResAppBy
dim rptResApp
'dim rptResAppNew
'dim rptResAppOld
'dim rptMCRAvailable
'dim rptDaysOpen
'dim rptDaysOpenDate
'dim rptDaysOpenDateOld
'dim rptDaysLeftDateInt
'dim rptDaysLeft
dim rptUserField1
dim rptUserField2
dim rptUserField3
dim rptUserField4
dim rptUserField5
dim rptUserField6
dim rptUserField7
dim rptUserField8
dim rptUserField9
dim rptUserField10
dim rptUserField11
dim rptUserField12
dim rptUserField13
dim rptUserField14
dim rptUserField15
dim rptUserField16
dim rptUserField17
dim rptUserField18
dim rptUserField19
dim rptUserField20

dim rptProgress, rptProgressOriginal

dim idInvestigation
dim arrInvestigation

dim pCaseStatusCloseOn
dim pInvestigationStatusCloseOn

'OptionsGroups
'dim idOptionGroup
'dim optionGroupDesc
'dim statusMail
'dim statusMailInv
'dim adminEmail
'dim adminEmailInv
'dim adminEmailBody
'dim logidEmail

'dim managerMail
'dim managerName

dim managerEmail
dim managerEmailSubject
dim managerEmailBody
dim tmpEmailBody

'Categories
'dim idCategory
'dim categoryDesc
'dim idParentCategory

'dim idDepartment
'dim idDeptProd

'Categories_Issues
'dim idCatProd

'optionsGroupsXref
'dim idOptGrpProd

'DiscProd
'dim idDiscProd
'dim discAmt
'dim discFromQty
'dim discToQty
'dim discPerc

'IssueGroups
'dim idProdGroup
'dim prodGroupP
'dim prodGroupC

'OptionsProdEx
'dim idOptionsProdEx
'dim idOption

dim emailPrefix : emailPrefix = "<table width=""90%"" border=""0"" align=""center"" cellpadding=""5"" cellspacing=""0""><tr><td height=""20""><span style=""font-family: 'Verdana','sans-serif'; COLOR: black; FONT-SIZE: 7.5pt;"">Please DO NOT reply to this email. Send all correspondence to original sender: <strong>" & session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname") & "</strong>&nbsp;[<a href=mailto:" & email & ">" & email & "</a>]</span></td></tr></table>"
dim emailSuffix : emailSuffix = "<table width=""90%"" border=""0"" align=""center"" cellpadding=""5"" cellspacing=""0""><tr><td><span style=""font-family: verdana,geneva; color: black; font-size: 10px;"">** Please do not reply to this message. If you need assistance visit <a href=""http://support.complianceconcepts.com/""><span style=""color: #0000ff;"">support.complianceconcepts.com</span></a></span></td></tr></table>"
																
'get action
dim action
action = trim(lCase(Request.Form("action")))
if len(action) = 0 then
	action = trim(lCase(Request.QueryString("action")))
end if
if  action <> "edit" _
and action <> "del" _
and action <> "add" _
and action <> "addfu"  _
and action <> "bulkdel" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if

'get idIssue
if action = "edit"  _
or action = "addfu" _
or action = "del" then

	'ID of issue to work with
	idIssue = trim(Request.Form("idIssue"))
	if len(idIssue) = 0 then
		idIssue = trim(Request.QueryString("recID"))
	end if
	if idIssue = "" then
		'for AJax display
		if action = "edit" then
			response.write("Error: Invalid Issue ID.")
			response.end
		'all others, redirect
		else
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")		
		end if
	end if

end if

'used to redirect user back to tabs they last were on
dim topTab, sideTab
topTab = trim(Request.Form("topTab"))
if len(topTab) = 0 then
	topTab = trim(Request.QueryString("top"))
end if
sideTab = trim(Request.Form("sideTab"))
if len(sideTab) = 0 then
	sideTab = trim(Request.QueryString("side"))
end if


'Get Issue Details
if action = "edit" or action = "add" or action = "addfu" then

	'get/set customerID...must be from FORM submission only for security -- mandatory
	customerID = trim(Request.Form("customerID"))
	if customerID = "" then
		'for AJax display
		if action = "edit" then
			response.write("Error: Invalid Customer ID.")
			response.end
		'all others, redirect
		else
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
		end if		
	end if

	'get IssueType (1,2,3 or 4)...must be from FORM submission only for security -- mandatory
	issueType = trim(Request.Form("issueType"))
	if issueType = "" then
		'for AJax display
		if action = "edit" then
			response.write("Error: Invalid Issue Type ID.")
			response.end
		'all others, redirect
		else
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue Type ID.")
		end if				
	end if

	rptRSID = trim(Request.Form("rptRSID"))
	if len(rptRSID) <= 0 or rptRSID = "" or isNull(rptRSID) then
		rptRSID = 0
	end if

	rptQAID = trim(Request.Form("rptQAID"))
	if len(rptQAID) <= 0 or rptQAID = "" or isNull(rptQAID) then
		rptQAID = 0
	end if

	'Set Date parameters
	rptDate = trim(Request.Form("rptDate"))
	if IsDate(rptDate) = true then
		rptDate = FormatDateTime(rptDate,vbShortDate)
		rptMonth = DatePart("m",rptDate)   '--> this was clever, just dont need right now.... Right("0" & DatePart("m",rptDate),2)
		rptQuarter = DatePart("q",rptDate)
		rptYear = DatePart("yyyy",rptDate)
	end if

	'Set Issue Time
	rptTime = trim(Request.Form("rptTime"))
	if IsDate(rptTime) = true then
		rptTime = FormatDateTime(rptTime,3) 
	end if

	'Get Severity
	severityLevel = trim(Request.Form("severityLevel"))

	rptProgressOriginal = trim(Request.Form("rptProgressOriginal"))	
	rptProgress = trim(Request.Form("rptProgress"))	
	if action = "add" or action = "addfu" then
		rptProgress = 1
		rptProgressOriginal = 1
		'auto close RFI's
		if cLng(issueType) = 2 then
			rptStatus = "Closed"
		else
			rptStatus = "Open"
		end if
		'only saves on add or addfu
		rptSource = "ComplianceLine"		
	end if

	'Get firstTime
	firstTime = trim(Request.Form("firstTime"))
	if firstTime <> "-1" and firstTime <> "0" then
		firstTime = "0"
	end if

	'Get commTool -- optional
	commTool	= trim(Request.Form("commTool"))

	'Get callBack -- optional
	callBack = trim(Request.Form("callBack"))
	if (len(callBack) > 0 and IsDate(callBack) = False) then
		'for AJax display
		if action = "edit" then
			response.write("Error: Invalid Call Back Date.")
			response.end
		'all others, redirect
		else
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Call Back Date.")
		end if
	end if
	
	'Get anonCaller -- mandatory
	anonCaller = trim(Request.Form("anonCaller"))
	if anonCaller <> "-1" and anonCaller <> "0" then
		'for AJax display
		if action = "edit" then
			response.write("Error: Invalid 'Anonymous Caller' Indicator.")
			response.end
		'all others, redirect
		else
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid 'Anonymous Caller' Indicator.")
		end if
	end if

	'Get Interpreter Used -- mandatory
	interpreterused = trim(Request.Form("interpreterused"))
	if interpreterused <> "-1" and interpreterused <> "0" then
		interpreterused = "0"
	end if

	interpreterNumber = trim(Request.Form("interpreterNumber"))

	'Get Language used -- optional
	language = trim(Request.Form("language"))

	'Get Location Fields -- all optional
	locNameID	= trim(Request.Form("locNameID"))
	locName		= trim(Request.Form("locName"))
	locAddr		= trim(Request.Form("locAddr"))
	locCity		= trim(Request.Form("locCity"))
	locState	= trim(Request.Form("locState"))
	locZip		= trim(Request.Form("locZip"))
	locCountry	= trim(Request.Form("locCountry"))
	
	'Get Caller Fields -- all optional
	callerType 		= trim(Request.Form("callerType"))
	callerFirst		= trim(Request.Form("callerFirst"))
	callerLast		= trim(Request.Form("callerLast"))
	callerTitle		= trim(Request.Form("callerTitle"))
	callerAddr		= trim(Request.Form("callerAddr"))
	callerCity		= trim(Request.Form("callerCity"))
	callerState		= trim(Request.Form("callerState"))
	callerZip		= trim(Request.Form("callerZip"))
	callerHome		= trim(Request.Form("callerHome"))
	callerWork		= trim(Request.Form("callerWork"))
	callerCell 		= trim(Request.Form("callerCell"))
	callerPager 	= trim(Request.Form("callerPager"))
	callerFax 		= trim(Request.Form("callerFax"))
	callerOther 	= trim(Request.Form("callerOther"))
	callerEmail		= trim(Request.Form("callerEmail"))
	
	callerPassword	= trim(Request.Form("callerPassword"))		
	if action = "add" then	
		Randomize
		callerPassword = Right("000" & Int(rnd*10000)+1, 4)  'Int((rnd*1000))+1		
	end if	
	
	rptSummary	= trim(Request.Form("rptSummary"))
	rptSummary = replace(rptSummary,"<br />","<br>")	
		
	'Get Details -- optional
	rptDetails = trim(Request.Form("rptDetails"))
	rptDetails = replace(rptDetails,"<br />","<br>")
	
	'Get Addendum -- optional
	rptAddendum = trim(Request.Form("rptAddendum"))
	rptAddendum = replace(rptAddendum,"<br />","<br>")	

	'Get User Defined Fields -- all optional
	rptUserField1 	= trim(Request.Form("rptUserField1"))
	rptUserField2 	= trim(Request.Form("rptUserField2"))
	rptUserField3 	= trim(Request.Form("rptUserField3"))
	rptUserField4 	= trim(Request.Form("rptUserField4"))
	rptUserField5 	= trim(Request.Form("rptUserField5"))
	rptUserField6 	= trim(Request.Form("rptUserField6"))
	rptUserField7 	= trim(Request.Form("rptUserField7"))
	rptUserField8 	= trim(Request.Form("rptUserField8"))
	rptUserField9 	= trim(Request.Form("rptUserField9"))
	rptUserField10 	= trim(Request.Form("rptUserField10"))
	rptUserField11 	= trim(Request.Form("rptUserField11"))
	rptUserField12 	= trim(Request.Form("rptUserField12"))
	rptUserField13 	= trim(Request.Form("rptUserField13"))
	rptUserField14 	= trim(Request.Form("rptUserField14"))
	rptUserField15 	= trim(Request.Form("rptUserField15"))
	rptUserField16 	= trim(Request.Form("rptUserField16"))
	rptUserField17 	= trim(Request.Form("rptUserField17"))
	rptUserField18 	= trim(Request.Form("rptUserField18"))
	rptUserField19 	= trim(Request.Form("rptUserField19"))
	rptUserField20 	= trim(Request.Form("rptUserField20"))

	'Get Location Levels -- optional
	dim arrLevel, levelI
	locHierarchy= trim(Request.Form("locHierarchy"))
	if len(locHierarchy) > 0 then
		arrLevel = split(locHierarchy,"*|*")
		'move through all categories selected
		for levelI = LBound(arrLevel) to UBound(arrLevel)
			if len(arrLevel(levelI)) > 0 then
				if levelI = 0 then locLevel_1 = arrLevel(levelI)
				if levelI = 1 then locLevel_2 = arrLevel(levelI)				
				if levelI = 2 then locLevel_3 = arrLevel(levelI)
				if levelI = 3 then locLevel_4 = arrLevel(levelI)
				if levelI = 4 then locLevel_5 = arrLevel(levelI)
				if levelI = 5 then locLevel_6 = arrLevel(levelI)																
				if levelI = 6 then locLevel_7 = arrLevel(levelI)
				if levelI = 7 then locLevel_8 = arrLevel(levelI)								
			end if
		next
	end if

	'Get resolution text -- optional
	rptResSatisfaction = trim(Request.Form("rptResSatisfaction"))

	'Get resolution text -- optional
	rptResolution = trim(Request.Form("rptResolution"))

	'Get Resolution Approved UserID -- optional
	rptResAppBy = trim(Request.Form("rptResAppBy"))		
	if len(rptResAppBy) = 0  then
		rptResAppBy = 0
	end if

	'Get Resolution Approved -- Must equal -1 or 0
	rptResApp = trim(Request.Form("rptResApp"))
	if rptResApp = "0" or len(rptResApp) = 0 or rptResApp = "" then
		rptResApp = 0
	elseif rptResApp = "-1" and len(rptResolution) = 0 then
		'for AJax display
		if action = "edit" then
			response.write("Error: Invalid 'Resolution' Text.")
			response.end
		'all others, redirect
		else
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid 'Resolution' Text.")		
		end if		
	elseif rptResApp = "-1" and len(rptResolution) > 0 and editCaseResolutionApp = "Y" and rptResAppBy <= 0 then 		
		rptResApp = sLogid
		rptResAppDate = Date()
	elseif rptResApp = "-1" and len(rptResolution) > 0 and rptResAppBy > 0 then 						
		rptResApp = rptResAppBy
	end if
				
				
end if


'Generate CRSID
if action = "add" then

	dim rResetFrequency, rNextReset, lngTrackingNumber
	dim sMonth, sYear, rReset

	'get necessarsy variables to generated CRSID
	'the YEAR helps prevent dup issues if a 1999 year is in database
	mySQL = "SELECT MAX(CRSID) AS LastCRS " _
		  & "FROM   CRS " _
		  & "WHERE  CustomerID = '" & customerID & "' AND IssueTypeID=" & issueType & " AND CRSID Like '%-" & customerID & "-%-01' AND Year>='" & Year(Date())-3 & "'"		  
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if not rs.eof and len(rs("LastCRS"))>0 then
	
		'determine year
		sYear = mid(rs("LastCRS"),1,2)
		if sYear <> mid(year(Date),3,2)	then
			rReset = true
			'sYear = mid(year(Date),3,2)
		end if
		
		'determine month
		sMonth = mid(rs("LastCRS"),3,2)	
		if sMonth <> right("0" & month(Date),2) then
			rReset = true
			'sMonth = right("0" & month(Date),2)
		end if
		
		'determine rolling number
		if rReset = true then
			lngTrackingNumber = "0001"
		else
			lngTrackingNumber = cLng(mid(rs("LastCRS"), 8 + len(customerID),4)) 'should pull 1101-AAA-1[0001]-01)
			lngTrackingNumber = lngTrackingNumber + 1	
			'add leading zeros
			if Len(lngTrackingNumber) = 1 Then
				lngTrackingNumber = "000" & lngTrackingNumber
			elseif Len(lngTrackingNumber) = 2 Then	
				lngTrackingNumber = "00" & lngTrackingNumber
			elseif Len(lngTrackingNumber) = 3 Then	
				lngTrackingNumber = "0" & lngTrackingNumber
			elseif Len(lngTrackingNumber) = 4 Then	
				lngTrackingNumber = lngTrackingNumber
			end if
		end if
	
		'NEW issue # -- put all the pieces together to create CRSID
		idIssue = mid(year(Date),3,2) & right("0" & month(Date),2) & "-" & uCase(customerID) & "-" & issueType & lngTrackingNumber & "-01"	
	
	'no issues found, add 1st one
	else

		'NEW issue # -- put all the pieces together to create CRSID
		idIssue = mid(year(Date),3,2) & right("0" & month(Date),2) & "-" & uCase(customerID) & "-" & issueType & "0001-01"	
	
	end if
		
end if

'Delete/Add Categories
if action = "edit" or action = "add" or action = "addfu" then

	'***************************************
	'Add any Categories selected to be added
	'MUST be after CRSID # is generated in 
	'case this is a new issue	
	dim listCategoryAdded
	dim arrAddedCat
	dim addedC

	'discover added categories
	listCategoryAdded = trim(Request.Form("listCategoryAdded"))
	if len(listCategoryAdded) > 0 then
		arrAddedCat = split(listCategoryAdded,",")		
		'move through all categories selected
		for addedC = LBound(arrAddedCat) to UBound(arrAddedCat)
			if len(arrAddedCat(addedC)) > 0 then
				'make sure does NOT already exist so it's not added twice
				mySQL = "SELECT Count(CRSID) AS CatCount FROM CRS_Category " _
					  & "WHERE CRSID = '" & idIssue & "' and CategoryID = " & arrAddedCat(addedC) & " "
				if action = "edit" then
					set rs = openRSexecuteAjax(mySQL)
				else
					set rs = openRSexecute(mySQL)
				end if
				'does NOT exist, go ahead and add
				if rs("CatCount") <= 0 then					
					'add record to table
					mySQL = "INSERT INTO CRS_Category (" _
						  & "CRSID,CategoryID,ModifiedBy,ModifiedDate" _
						  & ") VALUES (" _
						  & "'" & idIssue & "'," & arrAddedCat(addedC) & "," & sLogid & ",'" & Now() & "' " _
						  & ")"
					if action = "edit" then
						set rs = openRSexecuteAjax(mySQL)
					else
						set rs = openRSexecute(mySQL)
					end if
				end if
			end if
		next
	end if

	'get callback time frame
	if action = "add" or action = "addfu" then	
		mySQL = "SELECT Category.CategoryID, Category.Sev1, Category.Sev2, Category.Sev3, " _
			  & " 	Category.Sev1Weekends, Category.Sev2Weekends, Category.Sev3Weekends " _
			  & "	FROM Category INNER JOIN CRS_Category ON Category.CategoryID = CRS_Category.CategoryID " _
			  & "	WHERE CRSID = '" & idIssue & "' " _
			  & "	ORDER BY Category.Sev" & severityLevel & " ASC "
		'open record
		if action = "edit" then
			set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		else
			set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		end if
		
		if not rs.eof then
			'no call back if set to zero (0)
			if cLng(rs("Sev" & severityLevel)) > 0 then
				'generate callback WITHOUT weekends
				if rs("Sev" & severityLevel & "Weekends") = "True" then			
					callBack = DateAddW(date(),rs("Sev" & severityLevel))
					
				'regular callback with weekends
				else			
					callBack = DateAdd("d",rs("Sev" & severityLevel),Date())
				end if							
			end if
		end if
	end if
	

	'***************************************
	'Remove any Categories selected	
	dim listCategoryRemoved
	dim arrayRemovedCat
	dim removedC

	'Add new users to Case History
	listCategoryRemoved = trim(Request.Form("listCategoryRemoved"))
	if len(listCategoryRemoved) > 0 then
		arrayRemovedCat = split(listCategoryRemoved,",")		
		'move through all users.
		for removedC = LBound(arrayRemovedCat) to UBound(arrayRemovedCat)
			if len(arrayRemovedCat(removedC)) > 0 then
				'Delete Record from Categories_Issues
				mySQL = "DELETE FROM CRS_Category " _
					  & "WHERE CRSID = '" & idIssue & "' AND CategoryID = " & arrayRemovedCat(removedC)
				if action = "edit" then
					set rs = openRSexecuteAjax(mySQL)
				else
					set rs = openRSexecute(mySQL)
				end if
			end if
		next	
	end if

	'***************************************
	'Update primary Category
	dim primaryCategory, primaryOriginal
	primaryCategory = trim(Request.Form("primaryCategory"))
	primaryOriginal = trim(Request.Form("primaryOriginal"))
	'primary was provided
	if len(primaryCategory) > 0 then
		'there was already one
		if len(primaryOriginal) > 0 then
			'the original and new DONT match...so update
			if primaryCategory <> primaryOriginal then
			
				'update record in table
				mySQL = "UPDATE CRS_Category SET " _
					  & "	[Primary]='Y', " _					  
					  & "	ModifiedBy="		& sLogid	& ", " _
					  & "	ModifiedDate='"		& Now()		& "' " _
					  & "WHERE  CRSID = '" & idIssue & "' " _ 					  
					  & "AND    CategoryID = " & primaryCategory & " "		  		  		  		    
				if action = "edit" then
					set rs = openRSexecuteAjax(mySQL)
				else
					set rs = openRSexecute(mySQL)
				end if
				
				'update record in table
				mySQL = "UPDATE CRS_Category SET " _
					  & "	[Primary]='', " _
					  & "	ModifiedBy="		& sLogid	& ", " _
					  & "	ModifiedDate='"		& Now()		& "' " _					  
					  & "WHERE  CRSID = '" & idIssue & "' " _ 
					  & "AND    CategoryID = " & primaryOriginal & " "		  		  		  		    
				if action = "edit" then
					set rs = openRSexecuteAjax(mySQL)
				else
					set rs = openRSexecute(mySQL)
				end if

			end if
		'there was NO ORIGINAL...update new
		else
			'update record in table
			mySQL = "UPDATE CRS_Category SET " _
				  & "	[Primary]='Y', " _
				  & "	ModifiedBy="		& sLogid	& ", " _
				  & "	ModifiedDate='"		& Now()		& "' " _				  
				  & "WHERE  CRSID = '" & idIssue & "' " _ 
				  & "AND    CategoryID = " & primaryCategory & " "		  		  		  		    
			if action = "edit" then
				set rs = openRSexecuteAjax(mySQL)
			else
				set rs = openRSexecute(mySQL)
			end if

		end if
			
	'no primary provided
	elseif len(primaryCategory) <= 0 then
		if len(primaryOriginal) > 0 then
			'update record in table
			mySQL = "UPDATE CRS_Category SET " _
				  & "	[Primary]='', " _
				  & "	ModifiedBy="		& sLogid	& ", " _
				  & "	ModifiedDate='"		& Now()		& "' " _				  
				  & "WHERE  CRSID = '" & idIssue & "' " _ 
				  & "AND    CategoryID = " & primaryOriginal & " "		  		  		  		    
			if action = "edit" then
				set rs = openRSexecuteAjax(mySQL)
			else
				set rs = openRSexecute(mySQL)
			end if

		end if

	end if

end if


'Delete/Add Users
if action = "edit" or action = "add" or action = "addfu" then

	'***************************************
	'Get global FROM EMAIL address for emails
	'	-this is necessary as some email servers see the FROM address as user@client.com
	'	 and the sending domain as "ccius.com" and consider it SPOOFING and block the email.	
	'***************************************	
	mySQL = "SELECT appSendFromEmail FROM Customer " _
		  & "	WHERE Customer.CustomerID='" & customerID & "' "
	if action = "edit" then
		set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	else
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	end if
	if len(rs("appSendFromEmail")) > 0 then emailFrom = rs("appSendFromEmail") else emailFrom = email

	'***************************************
	'Add any Users/Logins selected to be added
	'MUST be after CRSID # is generated in 
	'case this is a new issue	
	dim listUserAdded
	dim arrAddedUser
	dim addedU
	dim emailUserAdded

	'discover added users
	listUserAdded = trim(Request.Form("listUserAdded"))
	if len(listUserAdded) > 0 then

		'-----------------------------------		
		'add newly assigned users to issue
		'-----------------------------------		
		arrAddedUser = split(listUserAdded,",")
		'move through all users selected
		for addedU = LBound(arrAddedUser) to UBound(arrAddedUser)
			if len(arrAddedUser(addedU)) > 0 then
			
				'make sure does NOT already exist so it's not added twice
				mySQL = "SELECT Count(CRSID) AS UserCount FROM CRS_Logins " _
					  & "WHERE CRSID = '" & idIssue & "' and LogID = " & arrAddedUser(addedU) & " "
				if action = "edit" then
					set rs = openRSexecuteAjax(mySQL)
				else
					set rs = openRSexecute(mySQL)
				end if

				'does NOT exist, go ahead and add
				if rs("UserCount") <= 0 then									
					'add record to table
					mySQL = "INSERT INTO CRS_Logins (" _
						  & "CustomerID,CRSID,LogID,ModifiedBy,ModifiedDate" _
						  & ") VALUES (" _
						  & "'" & uCase(customerID) & "','" & idIssue & "'," & arrAddedUser(addedU) & "," & sLogid & ",'" & Now() & "' " _
						  & ")"
					if action = "edit" then
						set rs = openRSexecuteAjax(mySQL)
					else
						set rs = openRSexecute(mySQL)
					end if

					'array for sending emails to new users
					emailUserAdded = emailUserAdded & arrAddedUser(addedU) & ","			
							
				end if			
			end if
		next								

		'-----------------------------------------------------------------
		'find users who want email notice, after issue already processed
		'and only if they were just added above...no change in Progress
		'-----------------------------------------------------------------
		if cLng(rptProgress) = 3 and cLng(rptProgressOriginal) = 3 then		
			addedU = 0
			managerEmail = ""		
			arrAddedUser = split(emailUserAdded,",") 'those users added above
			for addedU = LBound(arrAddedUser) to UBound(arrAddedUser)		
				if len(arrAddedUser(addedU)) > 0 then
					mySQL = "SELECT LastName, Email FROM Logins Where LogID=" & arrAddedUser(addedU) & " AND EmailAssignment='Y' AND EmailIssueTypes Like '%" & issueType & "%' "
					if action = "edit" then
						set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
					else
						set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
					end if
					'make sure a user was found
					if not rs.eof then
						if len(rs("EMail")) > 0 then
							managerEmail = managerEmail & ";" & rs("Email")
						end if
					end if
				end if			
			next			
		end if
				
	end if


	'***************************************
	'Set any Users Permissions selected	
	dim listUserPermission
	dim arrayPermissionUser
	dim changedU, currentUser

	'Add new users to Case History
	listUserPermission = trim(Request.Form("listUserPermission"))
	if len(listUserPermission) > 0 then
		arrayPermissionUser = split(listUserPermission,",")		
		'move through all users.
		for changedU = LBound(arrayPermissionUser) to UBound(arrayPermissionUser)
			if len(arrayPermissionUser(changedU)) > 0 then
				'remove any commas "," from current string
				currentUser = replace(arrayPermissionUser(changedU),",","")			
				'update record in table
				mySQL = "UPDATE CRS_Logins SET " _
					  & "ReadOnly = '"   & right(currentUser,1) & "'," _
					  & "ModifiedBy="		& sLogid	& ", " _
					  & "ModifiedDate='"    & Now()		& "' " _					  
					  & "WHERE  CRSID = '" & idIssue & "' " _ 
					  & "AND    LogID = " & left(currentUser,len(currentUser)-2)		  		  		  		    
				if action = "edit" then
					set rs = openRSexecuteAjax(mySQL)
				else
					set rs = openRSexecute(mySQL)
				end if

			end if
		next	
	end if

	'***************************************
	'Remove any Users selected	
	dim listUserRemoved
	dim arrayRemovedUser
	dim removedU

	'Add new users to Case History
	listUserRemoved = trim(Request.Form("listUserRemoved"))
	if len(listUserRemoved) > 0 then
		arrayRemovedUser = split(listUserRemoved,",")		
		'move through all users.
		for removedU = LBound(arrayRemovedUser) to UBound(arrayRemovedUser)
			if len(arrayRemovedUser(removedU)) > 0 then
				'delete record from table
				mySQL = "DELETE FROM CRS_Logins " _
					  & "WHERE CRSID = '" & idIssue & "' AND LogID = " & arrayRemovedUser(removedU)
				if action = "edit" then
					set rs = openRSexecuteAjax(mySQL)
				else
					set rs = openRSexecute(mySQL)
				end if

			end if
		next	
	end if


	'************************************************
	'Send e-mails to all users who want one now that
	'the progress is set to 3 for the first time
	if cLng(rptProgress) = 3 and cLng(rptProgressOriginal) <> cLng(rptProgress) then
		managerEmail = ""		
   		mySQL = "SELECT a.LogID, b.FirstName, b.LastName, a.ReadOnly, b.Email " _
      		  & "	FROM   CRS_Logins a LEFT JOIN Logins b " _
          	  & "		ON     a.LogID = b.LogID " _
			  & "   WHERE  a.CRSID = '" & idIssue & "' AND EmailAssignment='Y' AND EmailIssueTypes Like '%" & issueType & "%' " _
	       	  & "	ORDER BY b.LastName, b.FirstName"
		if action = "edit" then
			set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		else
			set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		end if
		'make sure a user(s) was found
		if not rs.eof then
			do while not rs.eof
				if len(rs("EMail")) > 0 then
					managerEmail = managerEmail & ";" & rs("Email")
				end if
				rs.movenext
			loop		
		end if
	end if
	
	
	'************************************************
	'send ALL email that need to go
	'************************************************
	if len(managerEmail) > 0 then
	
		'clean up
		if left(managerEmail,1) = ";" then managerEmail = right(managerEmail,len(managerEmail)-1)
				
		'subject
		managerEmailSubject = idIssue & ": Issue Received"
							
		'get email template
		mySQL = "SELECT Template FROM Email_Template WHERE Event='CaseManagerEmail' AND CustomerID='" & customerID & "'"		
		if action = "edit" then
			set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		else
			set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		end if

		if rs.eof then
			if action = "edit" then
				response.write("Error: There is a error with the e-mail template necessary to send updates.")
				response.end
			else
				response.redirect "../error/default.asp?errMsg=" & server.URLEncode("There is a error with the e-mail template necessary to send updates.")
			end if
		end if
		
		managerEmailBody = rs("Template")
		
		closeRS(rs)

		'set replacement tags				
		managerEmailBody = Replace(managerEmailBody,"|CRS.Name|",idIssue)
		managerEmailBody = Replace(managerEmailBody,"|Logins.Email|","myCM User")
		managerEmailBody = Replace(managerEmailBody,"|URL.Link|","<a href='https://www.mycompliancemanagement.com/default.asp?redirect=" & idIssue & "'>Link to issue</a>")
		managerEmailBody = Replace(managerEmailBody,"|CRS.Summary|",rptSummary)
		managerEmailBody = Replace(managerEmailBody,"|CRS.Date|",rptDate & " " & rptTime)
		managerEmailBody = Replace(managerEmailBody,"|CRS.Severity|",severityLevel)
		managerEmailBody = Replace(managerEmailBody,"|CRS.LocationName|",locName)

		'different FROM address than user logged so add prefix
		if lCase(email) <> lCase(emailFrom) then
			managerEmailBody = Replace(managerEmailBody,"<body>","<body>" & emailPrefix)		
		end if
		'add disclaimer suffix
		managerEmailBody = Replace(managerEmailBody,"</body>",emailSuffix & "</body>")
		
		' used to clean up old values				
		managerEmailBody = Replace(managerEmailBody,"|Email.Prefix|","") 				

		'log and send email...
		mySQL = "INSERT INTO MailLog (" _
			  & "CustomerID, CRSID, mailDate, fromEmail, toEmail, subject, body, conttype" _
			  & ") VALUES (" _
			  & "'" & uCase(customerID) & "'," _
			  & "'" & idIssue & "'," _
			  & "'" & Now() & "'," _
			  & "'" & emailFrom & "'," _
			  & "'" & managerEmail & "'," _
			  & "'" & replace(managerEmailSubject,"'","''") & "'," _
			  & "'" & replace(managerEmailBody,"'","''") & "'," _
			  & "'" & "HTML" & "'" _
			  & ")"
		if action = "edit" then
			set rs = openRSexecuteAjax(mySQL)
		else
			set rs = openRSexecute(mySQL)
		end if

	end if


end if


'Update/Save ISSUE QUESTIONS
dim qesX
dim arrQuestion

if action = "edit" then

	'discover string of questions to work with... "5,7,8,9,"
	arrQuestion = trim(Request.Form("arrQuestion"))

	if len(arrQuestion) > 0 then
		arrQuestion = split(arrQuestion,",")
		'move through all questions loaded with issue
		for qesX = 0 to UBound(arrQuestion)
			if len(arrQuestion(qesX)) > 0 then
				'Update Question
				mySQL = "UPDATE CRS_Question SET " _
					  & "	Answer='"		 & replace(trim(Request.Form("rptQuesAnswer" & qesX)),"'","''") & "'," _
					  & "	ModifiedBy="	 & sLogid														& ", " _
					  & "	ModifiedDate='"  & Now()													 	& "' " _
					  & "WHERE CustomerID='" & customerID & "' AND CRSQuestionID=" & arrQuestion(qesX)  	& " "
				'update question
				set rs = openRSexecuteAjax(mySQL)
			end if
		next
	end if

'add ISSUE QUESIONS
elseif action = "add" or action = "addfu" then

	'discover string of questions to work with... "5,7,8,9,"
	arrQuestion = trim(Request.Form("arrQuestion:" & issueType))

	if len(arrQuestion) > 0 then
		arrQuestion = split(arrQuestion,",")
		'move through all questions loaded with issue
		for qesX = 0 to UBound(arrQuestion)
			if len(arrQuestion(qesX)) > 0 then
				'add Question
				mySQL = "INSERT INTO CRS_Question (" _
					  & "CustomerID,CRSID,QuestionID,Question,Answer,ModifiedBy,ModifiedDate" _
					  & ") VALUES (" _
					  & "'" & uCase(customerID) & "'," & "'" & idIssue & "'," & arrQuestion(qesX) & ",'" & replace(trim(Request.Form("rptQuesName:" & issueType & ":" & qesX)),"'","''") & "','" & replace(trim(Request.Form("rptQuesAnswer:" & issueType & ":" & qesX)),"'","''") & "'," & sLogid & ",'" & Now() & "'" _
					  & ")"
				set rs = openRSexecute(mySQL)
			end if
		next		
	end if

end if


'Update/Save ACTIVITY ENTRIES
dim actX
dim arrActivity

if action = "edit" then

	'discover string of questions to work with... "5,7,8,9,"
	arrActivity = trim(Request.Form("arrActivity"))
	'update existing activities
	if len(arrActivity) > 0 then
		arrActivity = split(arrActivity,",")
		'move through all questions loaded with issue
		for actX = 0 to UBound(arrActivity)
			if len(arrActivity(actX)) > 0 then
				'Update Question
				mySQL = "UPDATE Activity SET " _
					  & "	Activity='"		 	& replace(trim(Request.Form("rptActivityNote_" & arrActivity(actX))),"'","''") 	& "'," _
					  & "	ModifiedBy="	 	& sLogid																		& ", " _
					  & "	ModifiedDate='"  	& Now()														 					& "' " _
					  & "WHERE CRSID='" 		& idIssue & "' AND ActivityID=" & arrActivity(actX)  							& "  "
				'update question
				set rs = openRSexecuteAjax(mySQL)
			end if
		next
	end if

	'discover string of questions to work with... "5,7,8,9,"
	arrActivity = trim(Request.Form("arrActivityAdded"))
	'add new activities
	if len(arrActivity) > 0 then
		arrActivity = split(arrActivity,",")
		'move through all questions loaded with issue
		for actX = 0 to UBound(arrActivity)
			if len(arrActivity(actX)) > 0 then
				'Add Activity
				mySQL = "INSERT INTO Activity (" _
					  & "CustomerID,CRSID,LOGID,Date,Activity,ModifiedBy,ModifiedDate" _
					  & ") VALUES (" _
					  & "'" & uCase(customerID) & "'," & "'" & idIssue & "'," & sLogid & ",'" & Now() & "','" & replace(trim(Request.Form("rptActivityNote_" & arrActivity(actX))),"'","''") & "'," & sLogid & ",'" & Now() & "'" _
					  & ")"
				set rs = openRSexecuteAjax(mySQL)
			end if
		next
	end if

	'discover string of questions to work with... "5,7,8,9,"
	arrActivity = trim(Request.Form("arrActivityRemoved"))
	'delete existing activities
	if len(arrActivity) > 0 then
		arrActivity = split(arrActivity,",")
		'move through all questions loaded with issue
		for actX = 0 to UBound(arrActivity)
			if len(arrActivity(actX)) > 0 then
				'Update Question
				mySQL = "DELETE FROM Activity " _
					  & "WHERE CRSID = '" & idIssue & "' AND ActivityID = " & arrActivity(actX)
				set rs = openRSexecuteAjax(mySQL)
			end if
		next
	end if

'add new entries to activity table	
elseif action = "add" or action = "addfu" then

	'insert RS Start activity
	mySQL = "INSERT INTO Activity (" _
		  & "CustomerID,CRSID,LOGID,Date,Activity,ModifiedBy,ModifiedDate" _
		  & ") VALUES (" _
		  & "'" & uCase(customerID) & "'," & "'" & idIssue & "'," & sLogid & ",'" & Now() & "','RS Start [1]'," & sLogid & ",'" & Now() & "'" _
		  & ")"
	set rs = openRSexecute(mySQL)

end if


'Update/Save SUBJECT ENTRIES
dim subX
dim arrSubject, arrResolution

if action = "edit" then

	'discover string of questions to work with... "5,7,8,9,"
	arrSubject = trim(Request.Form("arrSubject"))
	'update existing activities
	if len(arrSubject) > 0 then
		arrSubject = split(arrSubject,",")
		'move through all questions loaded with issue
		for subX = 0 to UBound(arrSubject)
			if len(arrSubject(subX)) > 0 then
				'Update Question
				mySQL = "UPDATE Subject SET " _
					  & "	Name='"		 		& replace(trim(Request.Form("rptSubjectNote_" & arrSubject(subX))),"'","''") 	& "'," _
					  & "	SanctionCheck='" 	& Request.Form("rptSubjectSanction_" & arrSubject(subX))						& "'," _
					  & "	ModifiedBy="	 	& sLogid																		& ", " _
					  & "	ModifiedDate='"  	& Now()														 					& "' " _
					  & "WHERE CRSID='" 		& idIssue & "' AND SubjectID=" & arrSubject(subX)  								& "  "
				'update question
				set rs = openRSexecuteAjax(mySQL)
			end if
		next
	end if

	'discover string of questions to work with... "5,7,8,9,"
	arrSubject = trim(Request.Form("arrSubjectAdded"))
	'add new activities
	if len(arrSubject) > 0 then
		arrSubject = split(arrSubject,",")
		'move through all questions loaded with issue
		for subX = 0 to UBound(arrSubject)
			if len(arrSubject(subX)) > 0 then
				'Add Activity
				mySQL = "INSERT INTO Subject (" _
					  & "CustomerID,CRSID,Name,SanctionCheck,ModifiedBy,ModifiedDate" _
					  & ") VALUES (" _
					  & "'" & uCase(customerID) & "'," & "'" & idIssue & "','" & replace(trim(Request.Form("rptSubjectNote_" & arrSubject(subX))),"'","''") & "','" & Request.Form("rptSubjectSanction_" & arrSubject(subX)) & "'," & sLogid & ",'" & Now() & "'" _
					  & ")"
				set rs = openRSexecuteAjax(mySQL)
			end if
		next
	end if

	'discover string of questions to work with... "5,7,8,9,"
	arrSubject = trim(Request.Form("arrSubjectRemoved"))
	'delete existing activities
	if len(arrSubject) > 0 then
		arrSubject = split(arrSubject,",")
		'move through all questions loaded with issue
		for subX = 0 to UBound(arrSubject)
			if len(arrSubject(subX)) > 0 then
				'Update Question
				mySQL = "DELETE FROM Subject " _
					  & "WHERE CRSID = '" & idIssue & "' AND SubjectID = " & arrSubject(subX)
				set rs = openRSexecuteAjax(mySQL)
			end if
		next
	end if

	'discover string of questions to work with... "5,7,8,9,"
	arrResolution = trim(Request.Form("arrResolutionRemoved"))
	subX = 0
	'delete existing activities
	if len(arrResolution) > 0 then
		arrResolution = split(arrResolution,",")
		'move through all questions loaded with issue
		for subX = 0 to UBound(arrResolution)
			if len(arrResolution(subX)) > 0 then
				'Update Question
				mySQL = "DELETE FROM Resolution " _
					  & "WHERE CRSID = '" & idIssue & "' AND ResolutionID = " & arrResolution(subX)
				set rs = openRSexecuteAjax(mySQL)
			end if
		next
	end if

end if


'add RELATED ISSUES
if action = "addfu" and 1=2 then

	'add follow-up as related issue
	mySQL = "INSERT INTO CRS_Related (" _
		  & "CRSID,RelatedCRSID,ModifiedBy,ModifiedDate" _
		  & ") VALUES (" _
		  & "'" & left(idIssue,len(idIssue)-3) & "-01" & "','" & idIssue & "'," & sLogid & ",'" & Now() & "'" _
		  & ")"
	set rs = openRSexecute(mySQL)

end if


'ADD new issue or follow-up
if action = "add" or action = "addfu" then	
	
	dim fieldsSQL, valueSQL

	'Add Issue with Callback
	fieldsSQL = "INSERT INTO CRS (" _
		  & "crsid,severity,[date],[time],month,quarter,year," _
		  & "customerid,issuetypeid,location_nameid,location_name,location_address," _
		  & "location_city,location_state,location_postalcode,location_country," _
		  & "communicationtool,anonymous,interpreterused,interpreternumber,language," _
		  & "firsttimeuser,caller_type,caller_firstname,caller_lastname,caller_title," _
		  & "caller_address,caller_city,caller_state,caller_postalcode," _
		  & "caller_home,caller_work,caller_cell,caller_fax,caller_pager,caller_other, " _
		  & "caller_email,caller_password,summary,details,addendum," _
		  & "source,status," _
		  & "location_level_1,location_level_2,location_level_3,location_level_4,location_level_5," _
		  & "location_level_6,location_level_7,location_level_8," _		
		  & "Resolution, ResolutionApprovedBy, ResolutionSatisfaction, " _  
		  & "userfield1,userfield2,userfield3,userfield4,userfield5," _
		  & "userfield6,userfield7,userfield8,userfield9,userfield10, " _
		  & "userfield11,userfield12,userfield13,userfield14,userfield15," _
		  & "userfield16,userfield17,userfield18,userfield19,userfield20, " _		  
		  & "rsid, progress, " _
		  & "modifiedby,modifieddate,datecreated"

	'check for empty dates	      
	if len(callBack) > 0 then fieldsSQL = fieldsSQL & ",callback"
	if len(rptDateClosed) > 0 then fieldsSQL = fieldsSQL & ",dateclosed"
	if len(rptResAppDate) > 0 then fieldsSQL = fieldsSQL & ",resolutionapproveddate"		
	fieldsSQL = fieldsSQL & ") "
		  
	valueSQL = " VALUES (" _
		  & "'"    	& idIssue							& "'," _
		  & "'"    	& severityLevel						& "'," _	  
		  & "'"    	& rptDate							& "'," _
		  & "'"    	& rptTime							& "'," _
		  & "'"    	& rptMonth							& "'," _
		  & "'"    	& rptQuarter						& "'," _		  
		  & "'"    	& rptYear							& "'," _		  		  
		  & "'"    	& uCase(customerID)					& "'," _
		  &     	issueType							& " ," _
		  & "'"    	& replace(locNameID,"'","''")		& "'," _
		  & "'"    	& replace(locName,"'","''")			& "'," _
		  & "'"    	& replace(locAddr,"'","''")			& "'," _
		  & "'"     & replace(locCity,"'","''")			& "'," _
		  & "'"     & locState							& "'," _
		  & "'" 	& locZip							& "'," _
		  & "'" 	& replace(locCountry,"'","''")		& "'," _
		  & "'" 	& commTool							& "'," _		  
		  &  		anonCaller							& " ," _
		  &  		interpreterused						& " ," _	
		  & "'"    	& interpreterNumber					& "'," _
		  & "'" 	& language							& "'," _		  		  		  
		  & 		firstTime							& " ," _
		  & "'" 	& callerType						& "'," _		  
		  & "'" 	& replace(callerFirst,"'","''")		& "'," _
		  & "'" 	& replace(callerLast,"'","''")		& "'," _		  		  
		  & "'" 	& replace(callerTitle,"'","''")		& "'," _
		  & "'" 	& replace(callerAddr,"'","''")		& "'," _
		  & "'" 	& replace(callerCity,"'","''")		& "'," _		  
		  & "'" 	& callerState						& "'," _
		  & "'" 	& callerZip							& "'," _
		  & "'" 	& callerHome						& "'," _
		  & "'" 	& callerWork						& "'," _
		  & "'" 	& callerCell						& "'," _
		  & "'" 	& callerFax							& "'," _
		  & "'" 	& callerPager						& "'," _
		  & "'" 	& callerOther						& "'," _		  
		  & "'" 	& callerEmail						& "'," _	
		  & "'" 	& callerPassword					& "'," _
		  & "'" 	& replace(rptSummary,"'","''")		& "'," _
		  & "'" 	& replace(rptDetails,"'","''")		& "'," _
		  & "'" 	& replace(rptAddendum,"'","''")		& "'," _
		  & "'"    	& rptSource							& "'," _		  
		  & "'"    	& rptStatus							& "'," _		  		  
		  & "'"    	& replace(locLevel_1,"'","''")		& "'," _
		  & "'"    	& replace(locLevel_2,"'","''")		& "'," _
		  & "'"    	& replace(locLevel_3,"'","''")		& "'," _
		  & "'"    	& replace(locLevel_4,"'","''")		& "'," _
		  & "'"    	& replace(locLevel_5,"'","''")		& "'," _
		  & "'"    	& replace(locLevel_6,"'","''")		& "'," _
		  & "'"    	& replace(locLevel_7,"'","''")		& "'," _
		  & "'"    	& replace(locLevel_8,"'","''")		& "'," _
		  & "'" 	& replace(rptresolution,"'","''")	& "'," _		  		  	  		  
		  &     	rptResApp							& ", " _			  
		  & "'"    	& rptResSatisfaction				& "'," _			  		  		  
		  & "'"		& validSQL(rptUserField1,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField2,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField3,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField4,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField5,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField6,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField7,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField8,"A")		& "'," _		  		 
		  & "'"		& validSQL(rptUserField9,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField10,"A")		& "'," _
		  & "'"		& validSQL(rptUserField11,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField12,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField13,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField14,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField15,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField16,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField17,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField18,"A")		& "'," _		  		 
		  & "'"		& validSQL(rptUserField19,"A")		& "'," _		  
		  & "'"		& validSQL(rptUserField20,"A")		& "'," _
		  & " "		& sLogid							& ", " _	
		  & " "		& rptProgress						& ", " _			  	  		  
		  & " "		& sLogid							& ", " _		  
		  & "'"    	& Now()								& "'," _		  
		  & "'"    	& Now()								& "' " _		  

	'check for empty dates
	if len(callBack) > 0 then valueSQL = valueSQL & ",'" & callBack & "'"
	if len(rptDateClosed) > 0 then valueSQL = valueSQL & ",'" & rptDateClosed & "'"
	if len(rptResAppDate) > 0 then valueSQL = valueSQL & ",'" & rptResAppDate & "'"
	
	'finish sql	
	valueSQL = valueSQL & ") "
	
	'execute update
	set rs = openRSexecute(fieldsSQL & valueSQL)
			
	'close database connection
	call closeDB()
	
	session(session("siteID") & "okMsg") = "Issue was Added."

	'show dialog with CRSIS/Issue # and Call Back? (did = dialog ID)
	response.redirect "issues_edit_00.asp?action=edit&recID=" & idIssue & "&top=" & topTab & "&side=" & sideTab & "&did=yes"
	
end if


'DELETE or BULK DELETE
if action = "del" or action = "bulkdel" then

	'Declare additional variables
	dim delI		'Array index
	dim delArray	'List of idIssue's that will be deleted

	'If just one delete is being performed, we populate just the 
	'first position in the delete array, else we populate the array
	'with a list of all the records that were selected for deletion.
	if action = "del" then
		delArray = split(idIssue)
	else
		delArray = split(Request.Form("idIssue"),",")
	end if

	'Set CursorLocation of the Connection Object to Client
	cn.CursorLocation = adUseClient
	
	'Loop through list of records and delete one by one
	for delI = LBound(delArray) to UBound(delArray)
	
		'BEGIN Transaction
		cn.BeginTrans
		
		'Delete records from optionsGroupsXref
		mySQL = "DELETE FROM CRS " _
		      & "WHERE CRSID = '" &  trim(delArray(delI)) & "'"
		set rs = openRSexecute(mySQL)
			
		'END Transaction
		cn.CommitTrans
	
	next

	'close database connection
	call closeDB()
	
	session(session("siteID") & "okMsg") = "Issue(s) were Deleted."
	response.redirect "issues.asp"
	
end if

'EDIT
if action = "edit" then
			
	'Update Record
	mySQL = "UPDATE CRS SET " _	
		  & "[date]='"     				& rptDate							& "'," _
		  & "[time]='"     				& rptTime							& "'," _
		  & "month='"     				& rptMonth							& "'," _
		  & "quarter='"     			& rptQuarter						& "'," _
		  & "year='"     				& rptYear							& "'," _
		  & "severity='"     			& severityLevel						& "'," _
		  & "location_nameid='"    		& replace(locNameID,"'","''")		& "'," _
		  & "location_name='"     		& replace(locName,"'","''")			& "'," _
		  & "location_address='"    	& replace(locAddr,"'","''")			& "'," _
		  & "location_city='"     		& replace(locCity,"'","''")			& "'," _
		  & "location_state='"     		& locState							& "'," _
		  & "location_postalcode='" 	& locZip							& "'," _
		  & "location_country='" 		& replace(locCountry,"'","''")		& "'," _
		  & "communicationtool='" 		& commTool							& "'," _
		  & "anonymous=" 				& anonCaller						& ", " _
		  & "interpreterused=" 			& interpreterused					& ", " _		
		  & "interpreternumber='" 		& interpreterNumber					& "'," _				  
		  & "language='"		 		& language							& "'," _		  		  		  
		  & "firsttimeuser=" 			& firstTime							& ", " _
		  & "caller_type='"		 		& callerType						& "'," _		  
		  & "caller_firstname='" 		& replace(callerFirst,"'","''")		& "'," _
		  & "caller_lastname='" 		& replace(callerLast,"'","''")		& "'," _		  		  
		  & "caller_title='" 			& replace(callerTitle,"'","''")		& "'," _
		  & "caller_address='" 			& replace(callerAddr,"'","''")		& "'," _
		  & "caller_city='" 			& replace(callerCity,"'","''")		& "'," _
		  & "caller_state='" 			& callerState						& "'," _
		  & "caller_postalcode='" 		& callerZip							& "'," _
		  & "caller_home='" 			& callerHome						& "'," _
		  & "caller_work='" 			& callerWork						& "'," _
		  & "caller_cell='" 			& callerCell						& "'," _
		  & "caller_fax='" 				& callerFax							& "'," _
		  & "caller_pager='" 			& callerPager						& "'," _
		  & "caller_other='" 			& callerOther						& "'," _
		  & "caller_email='" 			& callerEmail						& "'," _		  
		  & "caller_password='"			& callerPassword					& "'," _		  		  
		  & "summary='"     			& replace(rptSummary,"'","''")		& "'," _
		  & "details='"     			& replace(rptDetails,"'","''")		& "'," _
		  & "addendum='"     			& replace(rptAddendum,"'","''")		& "'," _
		  & "location_level_1='"		& replace(locLevel_1,"'","''")		& "'," _
		  & "location_level_2='"		& replace(locLevel_2,"'","''")		& "'," _
		  & "location_level_3='"		& replace(locLevel_3,"'","''")		& "'," _
		  & "location_level_4='"		& replace(locLevel_4,"'","''")		& "'," _
		  & "location_level_5='"		& replace(locLevel_5,"'","''")		& "'," _
		  & "location_level_6='"		& replace(locLevel_6,"'","''")		& "'," _
		  & "location_level_7='"		& replace(locLevel_7,"'","''")		& "'," _
		  & "location_level_8='"		& replace(locLevel_8,"'","''")		& "'," _
		  & "Resolution='" 				& replace(rptresolution,"'","''")	& "'," _		  		  	  
		  & "ResolutionApprovedBy=" 	& rptResApp							& ", " _			  
		  & "ResolutionSatisfaction='" 	& rptResSatisfaction				& "', " _			  
		  & "RSID="						& rptRSID							& ", " _		  		  
		  & "QAID="						& rptQAID							& ", " _		  		  
		  & "ModifiedBy="				& sLogid							& ", " _		  
		  & "modifieddate='"     		& Now()								& "' "		  

	'check for special existance of dates  
	if len(rptProgress) > 0 then mySQL = mySQL & ",progress='" & rptProgress & "' "		  	
	if len(callback) > 0 then mySQL = mySQL & ",callback='" & callBack & "' " else mySQL = mySQL & ",callback=null "
	if len(rptDateClosed) > 0 then mySQL = mySQL & ",dateclosed='" & rptDateClosed & "' " else mySQL = mySQL & ",dateclosed=null "
	if len(rptResAppDate) > 0 then mySQL = mySQL & ",resolutionapproveddate='" & rptResAppDate & "' "
		
	'finalize update query
	mySQL = mySQL & "WHERE CRSID = '" & idIssue & "' "
	
	'execute update
	set rs = openRSexecuteAjax(mySQL)

	'for AJax display
	response.write("Issue saved successfully.")		

	'close database connection
	call closeDB()

end if

'just in case we ever get this far...and NOT...called by action=edit
if action <> "edit" then
	call closeDB()
	Response.Redirect "issues.asp"
end if
%>
