<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = true
const showDHTMLMenu = true
const helpID = 7 'this #7 initiates the sidePanel slide to fire on page load, see _INCsidePanel.asp
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "issue:add"
'===================================================================

'Database
dim mySQL, cn, rs, rs2

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************
																	
dim showCRSID '...need for NEW search box
dim startDate
dim endDate

'Issue Fields on DETAILS tab
dim arrIssueType
dim issueType
dim anonCaller
dim interpreterUsed
dim interpreterNumber
dim language
dim useResolutions
dim callBack
dim callerAddr
dim callerCell
dim callerCity
dim callerEmail
dim callerFax
dim callerFirst
dim callerHome
dim callerLast
dim callerOther
dim callerPager
dim callerState
dim callerTitle
dim callerType
dim callerWork
dim callerZip
dim commTool
dim firstTime
dim locAddr
dim locCity
dim locCountry
dim locName, locNameID
dim locState
dim locZip
dim locLevel_1
dim locLevel_2
dim locLevel_3
dim locLevel_4
dim locLevel_5
dim locLevel_6
dim locLevel_7
dim locLevel_8
dim locHierarchy

dim appDeliverySystem

dim resGiven
dim severityLevel
dim viewedBy
dim readonly
'dim logidsreadonly

'Issue Fields on NOTES tab
dim rptAction 
dim rptAddendum
'dim rptCaseMgrEmail 
'dim rptCaseMgrEmailReadOnly 
'dim rptCaseMgrName 
dim rptCaseNotes 
'dim rptCaseType 
dim rptDateClosed 
dim rptDate
'dim rptDaysLeft 
'dim rptDaysLeftDateInt 
dim rptDaysOpen 
'dim rptDaysOpenDate 
'dim rptDaysOpenDateOld 
dim rptDetails
'dim rptFinancial 
dim rptInvestigation 
dim rptInvestigationStatus 
dim rptInvestigationStatusOld 
'dim rptLegal 
dim rptlogID 
dim rptOwner
'dim rptMgrAppRes 
'dim rptPrivileged 
'dim rptPrivNotes 
dim rptResApp 
dim rptResAppDate
dim rptResAppBy 
dim rptResAppOld 
dim rptResolution 
dim rptResolutionOld 
'dim rptRespGroup 
dim rptSource 
dim rptStatus 
dim rptStatusOld 
dim rptSummary
'dim rptSummaryOMG
dim rptTime
dim rptUserField1, rptUserField1Default 
dim rptUserField2, rptUserField2Default
dim rptUserField3, rptUserField3Default 
dim rptUserField4, rptUserField4Default 
dim rptUserField5, rptUserField5Default 
dim rptUserField6, rptUserField6Default 
dim rptUserField7, rptUserField7Default 
dim rptUserField8, rptUserField8Default 
dim rptUserField9, rptUserField9Default 
dim rptUserField10, rptUserField10Default 
dim rptUserField11, rptUserField11Default
dim rptUserField12, rptUserField12Default 
dim rptUserField13, rptUserField13Default 
dim rptUserField14, rptUserField14Default 
dim rptUserField15, rptUserField15Default 
dim rptUserField16, rptUserField16Default 
dim rptUserField17, rptUserField17Default 
dim rptUserField18, rptUserField18Default 
dim rptUserField19, rptUserField19Default 
dim rptUserField20, rptUserField20Default 

dim rptInvSubject
dim rptInvCategory
dim rptInvCloseDate
dim rptInvDiary

dim rptQuestionCount

'Work Fields
dim action, idIssue, pgNotice
dim optArr, optArrIndex

'Used with Dynamic List Contact selections
dim intGroups, arrGroups, groupsI, intUsers
dim tmpUserID, tmpUserName, lstUserName
dim arrUsers, usersI

dim dNis 'phone number passed by Vertical


dim rowColor, col1, col2,rowSet
col1 = "#F0F0F0"
col2 = "#FFFFFF"


'Determin TinyMCE to Initialize
dim activeElements
activeElements = "rptSummary,rptDetails,rptAddendum"


'Determin DHTML pop-up calendars to Initialize
dim activeCalendars
activeCalendars = "" '"'date:callBack'"


'Get action
action = trim(Request.QueryString("action"))
if len(action) = 0 then
	action = trim(Request.Form("action"))
end if
action = lCase(action)
if  action <> "add"  _
and action <> "addfu"  then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if


'ONLY CCI Users allowed to view this page
if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if


'used to determine what next step/page to show to user
dim curForm
curForm = trim(Request.QueryString("form"))
if len(curForm) <= 0 then
	curForm = "1"
end if


'get idIssue (1101-AAA-10001-01)
if action = "addfu" then
	idIssue = trim(Request.QueryString("recId"))
	if len(idIssue) = 0 then
		idIssue = trim(Request.Form("recId"))
	end if
	if idIssue = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")
	end if
end if


'check user view status	for customer and issuetype
'based on vwLogins_IssueType and security sets
if action = "addfu" then
	if userViewIssue(idIssue,sLogid) = False then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to view this issue.")
	end if
end if


'setup issue to edit
if action = "addfu" then

	'check user's READ-ONLY status
	if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then
		mySQL = "SELECT ReadOnly FROM CRS_Logins WHERE CRSID='" & idIssue & "' AND LOGID = " & sLogid & " "		  
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		if not rs.eof then		
			if rs("ReadOnly") = "Y" then
				response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
			end if		
		end if
	end if

	'DBAdmins and CCI Admins
	if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
		'get issue record/details
		mySQL = "SELECT CRS.*, Logins.FirstName, Logins.LastName, Logins.Email, Customer_IssueType.Resolutions " _
			  & "	FROM (CRS LEFT JOIN Logins ON CRS.LOGID = Logins.LOGID) INNER JOIN Customer_IssueType ON (CRS.CustomerID = Customer_IssueType.CustomerID) AND (CRS.IssueTypeID = Customer_IssueType.IssueTypeID) " _
			  & "	WHERE CRS.CRSID='" & idIssue & "' "

	'CCI RA/RS
	elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
		'get issue record/details
		mySQL = "SELECT CRS.*, Logins.FirstName, Logins.LastName, Logins.Email, Customer_IssueType.Resolutions " _
			  & "	FROM (CRS LEFT JOIN Logins ON CRS.LOGID = Logins.LOGID) INNER JOIN Customer_IssueType ON (CRS.CustomerID = Customer_IssueType.CustomerID) AND (CRS.IssueTypeID = Customer_IssueType.IssueTypeID) " _
			  & "	WHERE CRS.CRSID='" & idIssue & "' AND CRS.IssueTypeID<>3 "

	'everyone else...				  
	else
		'get issue record/details
		mySQL = "SELECT CRS.*, Logins.FirstName, Logins.LastName, Logins.Email, Customer_IssueType.Resolutions, vwLogins_IssueType.LOGID AS [LogIDView] " _	
			  & "	FROM ((CRS LEFT JOIN Logins ON CRS.LOGID = Logins.LOGID) INNER JOIN Customer_IssueType ON (CRS.IssueTypeID = Customer_IssueType.IssueTypeID) AND (CRS.CustomerID = Customer_IssueType.CustomerID)) INNER JOIN vwLogins_IssueType ON (Customer_IssueType.IssueTypeID = vwLogins_IssueType.IssueTypeID) AND (Customer_IssueType.CustomerID = vwLogins_IssueType.CustomerID) " _
			  & " 	WHERE CRS.CRSID='" & idIssue & "' AND vwLogins_IssueType.LOGID = " & sLogid & " "
			  
	end if
	
	'open record
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")
	else	
		CRSID 					= rs("crsid")
		customerID				= rs("customerid")
		issueType				= rs("issuetypeid")
		rptDate    				= rs("date")
		rptTime    				= rs("time")
		severityLevel   		= rs("severity")
		firstTime	  			= rs("firsttimeuser")
		commTool	  			= rs("communicationtool")
		callBack	  			= rs("callback")
		anonCaller	  			= rs("anonymous")
		interpreterUsed			= rs("interpreterused")
		interpreterNumber		= rs("interpreterNumber")
		language				= rs("language")		
		resGiven	  			= rs("resolutiongiven")
		locNameID  				= rs("location_nameid")
		locName	  				= rs("location_name")
		locAddr	  				= rs("location_address")
		locCity	  				= rs("location_city")
		locState	  			= rs("location_state")
		locZip	  				= rs("location_postalcode")
		locCountry 				= rs("location_country")		
		locLevel_1				= rs("location_level_1")
		locLevel_2				= rs("location_level_2")		
		locLevel_3				= rs("location_level_3")
		locLevel_4				= rs("location_level_4")		
		locLevel_5				= rs("location_level_5")
		locLevel_6				= rs("location_level_6")		
		locLevel_7				= rs("location_level_7")
		locLevel_8				= rs("location_level_8")		
		callerType				= rs("caller_type")
		callerFirst				= rs("caller_firstname")
		callerLast				= rs("caller_lastname")
		callerAddr				= rs("caller_address")
		callerTitle				= rs("caller_title")
		callerCity				= rs("caller_city")
		callerState				= rs("caller_state")
		callerZip				= rs("caller_postalcode")
		callerHome				= rs("caller_home")
		callerWork				= rs("caller_work")
		callerCell				= rs("caller_cell")
		callerPager				= rs("caller_pager")
		callerFax				= rs("caller_fax")
		callerOther				= rs("caller_other")
		callerEmail				= rs("caller_email")		
		viewedBy 				= rs("viewedBy")		
		rptLogID				= rs("logid")
		rptStatus				= rs("status")
		rptCaseNotes			= rs("casenotes")
		rptDateClosed			= rs("dateclosed")
		rptSource				= rs("source")			
		rptUserField1			= rs("userfield1")
		rptUserField2			= rs("userfield2")
		rptUserField3			= rs("userfield3")
		rptUserField4			= rs("userfield4")			
		rptUserField5			= rs("userfield5")
		rptUserField6			= rs("userfield6")
		rptUserField7			= rs("userfield7")
		rptUserField8			= rs("userfield8")
		rptUserField9			= rs("userfield9")
		rptUserField10			= rs("userfield10")			
		rptUserField11			= rs("userfield11")
		rptUserField12			= rs("userfield12")
		rptUserField13			= rs("userfield13")
		rptUserField14			= rs("userfield14")			
		rptUserField15			= rs("userfield15")
		rptUserField16			= rs("userfield16")
		rptUserField17			= rs("userfield17")
		rptUserField18			= rs("userfield18")
		rptUserField19			= rs("userfield19")
		rptUserField20			= rs("userfield20")						
		rptResAppBy				= rs("resolutionapprovedby")
		rptResAppDate			= rs("resolutionapproveddate")
		rptResolution			= rs("resolution")
		rptResolutionOld		= rs("resolution")
		rptSummary				= rs("summary")	
		rptDetails				= rs("details")	
		rptAddendum				= rs("addendum")	
		useResolutions			= rs("resolutions")				'turns on/off resolution side tab
		
		'build loccation hierarchy for display
		locHierarchy = locLevel_1 & "*|*" & locLevel_2 & "*|*" & locLevel_3 & "*|*" & locLevel_4 _
								  & "*|*" & locLevel_5 & "*|*" & locLevel_6 & "*|*" & locLevel_7 & "*|*" & locLevel_8
								  
		'change to proper value to be stored in database
		locHierarchy = reformatHierarchy(locHierarchy,"value")

		'set rptOwner...used for display only
		if len(rs("LastName")) > 0 then rptOwner = rs("LastName")
		if len(rs("FirstName")) > 0 and len(rptOwner) > 0 then rptOwner = rptOwner & ", " & rs("FirstName") else rptOwner = rs("FirstName")
		if len(rs("Email")) > 0 then rptOwner = rptOwner & " [" & rs("Email") & "]"

		'set Date/Time for new Issues
		if action = "addfu" then
			rptDate = Date()
			rptTime = Time()
			rptDetails = ""
			rptAddendum = ""
			rptStatus = "Open"
		end if
			
	end if
	
	call closeRS(rs)

'set/get customer ID
elseif action = "add" then
		
	'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	'NEED TO ADD SOME CODE TO ASK WHAT TYPE OF ISSUE
	'IS BEING STARTED RATHER THAN ASSUMING 1 OR 3
	'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!	
	'set Issue Type for new Issues
	if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
'		issueType = 1	'CCI staff...
	else
'		issueType = 3	'customer
	end if
	issueType = 0
	
	
	
	'for CCI Staff to start new issue from Vertical
	if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
		'get/set DNIS digit from vertical
		dNis = trim(lCase(Request.QueryString("dnis")))
		if len(dNis) = 0 then
			dNis = trim(Request.Form("dnis"))
		end if	
	end if

	'get/set customerID -- either from string or DNIS
	customerID = trim(lCase(Request.QueryString("cid")))
	if len(customerID) = 0 then
		customerID = trim(Request.Form("customerID"))
	end if	
	'no customerID, but there is a DNIS digit
	if customerID = "" and len(dNis) > 0 and cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
		mySQL = "SELECT Customer.CustomerID " _
			  & "	FROM Customer INNER JOIN Telcom ON Customer.CustomerID = Telcom.CustomerID " _
			  & "	WHERE Customer.Canceled<>-1 AND Telcom.DNIS='" & dNis & "' "
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		if rs.eof then
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
		else
			customerID = rs("customerid")
		end if
	elseif customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if


	'discovery issue types user has access to (1,2,3,4)
	if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then	
		mySQL = "SELECT Customer_IssueType.IssueTypeID " _
			  & "	FROM Customer_IssueType " _
			  & "	WHERE Customer_IssueType.CustomerID='" & customerID & "' " _
			  & "	ORDER BY Customer_IssueType.IssueTypeID"	
	else	
		mySQL = "SELECT vwLogins_IssueType.IssueTypeID " _
			  & "	FROM vwLogins_IssueType INNER JOIN IssueType ON vwLogins_IssueType.IssueTypeID = IssueType.IssueTypeID " _
			  & "	WHERE vwLogins_IssueType.LOGID=" & sLogid & " AND vwLogins_IssueType.CustomerID='" & customerID & "' " _
			  & "	ORDER BY IssueType.IssueTypeID"
	end if		  
	'open question recordset
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	do while not rs.eof
		arrIssueType = arrIssueType & rs("IssueTypeID") & ","
		rs.movenext
	loop
	call closeRS(rs)
	
	
	'CCI Staff only
	if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then			
		mySQL = "SELECT Customer.Name, Customer.Address, Customer.City, Customer.State, Customer.PostalCode, Customer.Phone_Work, Customer.Phone_Fax, Customer.Phone_Customer, Customer.Phone_Other, appDeliverySystem " _
			  & "	FROM Customer " _
			  & "	WHERE Customer.CustomerID='" & customerID & "' "

	'RIGHT NOW ONLY CCI STAFF IS ALLOWED TO RUN THIS FORM/PAGE
	'everyone else --> specific to Issue Type being added --> See 1=2 below
	else			  
		mySQL = "SELECT Customer.Name, Customer.Address, Customer.City, Customer.State, Customer.PostalCode, Customer.Phone_Work, Customer.Phone_Fax, Customer.Phone_Customer, Customer.Phone_Other " _
			  & "	FROM Customer " _
			  & "	WHERE Customer.CustomerID='" & customerID & "' AND 1=2 AND " _ 
			  & "			EXISTS ( " _
			  & "			SELECT vwLogins_IssueType.CustomerID, vwLogins_IssueType.LogID " _
			  & "				FROM vwLogins_IssueType " _
			  & "				WHERE (vwLogins_IssueType.CustomerID=Customer.CustomerID) AND (vwLogins_IssueType.IssueTypeID=" & issueType & ") AND (vwLogins_IssueType.LogID=" & sLogid & ") ) "
			  
	end if		  
	
	'open customer record
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
	else
		customerName = rs("Name")		
		customerProfile = rs("Address") & "<br/>" & rs("City") & ", " & rs("State") & " " & rs("PostalCode") & "<br/><br/>" & "Corporate: " & rs("Phone_Work") & "<br/>" & "Customer: " & rs("Phone_Customer")
		appDeliverySystem = rs("appDeliverySystem")		
	end if
	call closeRS(rs)

	'set initial issues variables
	rptDate = Date()
	rptTime = Time()
	rptDetails = ""
	rptAddendum = ""
	rptStatus = "Open"
	anonCaller = ""
	
	
end if


'create follow-up CRSID
if action = "addfu" then
	mySQL="SELECT CRSID " _
	    & "  FROM   CRS " _
	    & "  WHERE  CRSID LIKE '" & Mid(idIssue,1,Len(idIssue)-2) & "%' " _
		& "	 ORDER BY CRSID desc"
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")
	else
		CRSID = (Mid(rs("crsid"),Len(rs("crsid"))-1,2)+1)
		if len(CRSID) = 1 then
			CRSID = "0" & CRSID
		end if
		CRSID = Mid(rs("crsid"),1,len(rs("crsid"))-2) & CRSID
	end if
	call closeRS(rs)
	'NEW issue follow-up #
	idIssue = CRSID
end if


'discover profile default settings
if action = "add" or action = "addfu" then

	mySQL = "SELECT pUserField1Default, pUserField2Default, pUserField3Default, pUserField4Default, pUserField5Default, pUserField6Default, pUserField7Default, pUserField8Default, pUserField9Default, pUserField10Default, " _
		  & "pUserField11Default, pUserField12Default, pUserField13Default, pUserField14Default, pUserField15Default, pUserField16Default, pUserField17Default, pUserField18Default, pUserField19Default, pUserField20Default " _		  		  
		  & "FROM   Customer " _
		  & "WHERE  CustomerID = '" & customerID & "' "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)	

	'default User Field values
	rptUserField1Default = rs("pUserField1Default")
	rptUserField2Default = rs("pUserField2Default")	
	rptUserField3Default = rs("pUserField3Default")
	rptUserField4Default = rs("pUserField4Default")
	rptUserField5Default = rs("pUserField5Default")
	rptUserField6Default = rs("pUserField6Default")
	rptUserField7Default = rs("pUserField7Default")
	rptUserField8Default = rs("pUserField8Default")
	rptUserField9Default = rs("pUserField9Default")	
	rptUserField10Default = rs("pUserField10Default")
	rptUserField11Default = rs("pUserField11Default")
	rptUserField12Default = rs("pUserField12Default")
	rptUserField13Default = rs("pUserField13Default")
	rptUserField14Default = rs("pUserField14Default")
	rptUserField15Default = rs("pUserField15Default")
	rptUserField16Default = rs("pUserField16Default")	
	rptUserField17Default = rs("pUserField17Default")
	rptUserField18Default = rs("pUserField18Default")
	rptUserField19Default = rs("pUserField19Default")
	rptUserField20Default = rs("pUserField20Default")
	
	closeRS(rs)	
	
end if


'set default User Field values
if action = "add" or action = "addfu" then
	if len(rptUserField1Default) > 0 then rptUserField1 = rptUserField1Default
	if len(rptUserField2Default) > 0 then rptUserField2 = rptUserField2Default	
	if len(rptUserField3Default) > 0 then rptUserField3 = rptUserField3Default
	if len(rptUserField4Default) > 0 then rptUserField4 = rptUserField4Default	
	if len(rptUserField5Default) > 0 then rptUserField5 = rptUserField5Default
	if len(rptUserField6Default) > 0 then rptUserField6 = rptUserField6Default	
	if len(rptUserField7Default) > 0 then rptUserField7 = rptUserField7Default
	if len(rptUserField8Default) > 0 then rptUserField8 = rptUserField8Default	
	if len(rptUserField9Default) > 0 then rptUserField9 = rptUserField9Default
	if len(rptUserField10Default) > 0 then rptUserField10 = rptUserField10Default	
	if len(rptUserField11Default) > 0 then rptUserField11 = rptUserField11Default
	if len(rptUserField12Default) > 0 then rptUserField12 = rptUserField12Default	
	if len(rptUserField13Default) > 0 then rptUserField13 = rptUserField13Default
	if len(rptUserField14Default) > 0 then rptUserField14 = rptUserField14Default	
	if len(rptUserField15Default) > 0 then rptUserField15 = rptUserField15Default
	if len(rptUserField16Default) > 0 then rptUserField16 = rptUserField16Default	
	if len(rptUserField17Default) > 0 then rptUserField17 = rptUserField17Default
	if len(rptUserField18Default) > 0 then rptUserField18 = rptUserField18Default	
	if len(rptUserField19Default) > 0 then rptUserField19 = rptUserField19Default
	if len(rptUserField20Default) > 0 then rptUserField120 = rptUserField20Default	
end if


'discover if any questions exist for customer
if( action = "add" or action = "addfu") and len(trim(arrIssueType)) > 0 then
	'clean up trailing commas
	if right(trim(arrIssueType),1) = "," then arrIssueType = left(arrIssueType,len(trim(arrIssueType))-1)
	'find all available questions
	mySQL = "SELECT Count(QuestionID) AS QuestionCount " _
		  & "	FROM Question " _
		  & "	WHERE CustomerID='" & customerID & "' AND IssueTypeID in (" & arrIssueType & ") " & " AND Active='Y' "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	rptQuestionCount = rs("QuestionCount")
	call closeRS(rs)
end if


'set Owner for newly added issue, not used for CCI staff
if action = "add" and cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then				
	rptLogid = sLogid
	'set rptOwner...used for display only
	if len(session(session("siteID") & "lastname")) > 0 then rptOwner = session(session("siteID") & "lastname")
	if len(session(session("siteID") & "firstname")) > 0 and len(rptOwner) > 0 then rptOwner = rptOwner & ", " & session(session("siteID") & "firstname") else rptOwner = session(session("siteID") & "firstname")
	if len(Email) > 0 then rptOwner = rptOwner & " [" & Email & "]"
end if
if rptLogID = "" or isNull(rptLogID) then
	rptLogID = 0
end if


dim dirBegin
if action = "add" or action = "addfu" then
	mySQL = "SELECT Notes " _
		  & "	FROM Directive " _
		  & "	WHERE CustomerID='" & customerID & "' AND Event = 'Interview: Begin' " _
		  & "	ORDER BY SortOrder "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if not rs.eof then
		dirBegin = rs("notes")
	end if									
end if


dim pageTitle
if action = "add" then
	pageTitle = "Add Issue" 
else
	pageTitle = crsid
end if
%>

<!--#include file="../_includes/_INCheader_.asp"-->

<form name="frm" id="frm" method="post" action="issues_exec_00.asp" style="padding:0px; margin:0px;">

<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
  
	    <!-- START Left sided area -->  
		<td width="190" align="right" valign="top">    

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="../profiles/users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">
    
                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->              
                    <% if session(session("siteID") & "adminLoggedOn") < "10" then %>      
	                    <script>
   		                	stExpandSubTree('myCMtree',2,0);
       		            </script>
            		<% else %>
	                    <script>
   		                	stExpandSubTree('myCMtree',1,0);
       		            </script>
                    <% end if %>
                    <!-- STOP side menu include -->
                                    
                </td>
              </tr>
              <!-- STOP Left sided menu -->
        
            </table>

		</td>
    	<!-- STOP Left sided area -->


        <!-- START Main Middle Section -->
        <td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">    
        
			<%
			'used to STOP updating the cookie crumb if page is being recalled from a SAVE
			dim updateCrumb
            if len(session(session("siteID") & "errMsg")) > 0 or len(session(session("siteID") & "okMsg")) > 0 then
				updateCrumb = "false"
            end if
			%>
            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs('<% =updateCrumb %>'));
       	        </script>
            </div>    

            <div style="text-align:left; margin-bottom:10px;">				            
                <div style="float:left; padding-right:2px; margin-right:2px;">
					<% if action = "add" or action = "addfu" then %>            
                        <img src="../_images/icons/32/page_white_add.png" title="Issues" width="32" height="32" align="absmiddle" style="vertical-align:middle;"> 
                    <% else %>
                        <img src="../_images/icons/32/page_white.png" title="Issues" width="32" height="32" align="absmiddle" style="vertical-align:middle;">                 
                    <% end if %>
                </div>
                <div>
                	<span class="pageTitle">Issue: <% if action = "addfu" then response.Write(crsid) else response.Write("<i>&lt;none assigned&gt;</i>") %></span>
                    <%
					if (cLng(session(session("siteID") & "adminLoggedOn")) < 10) or (programAdmin = "Y") then
						response.write("<br/>:: <a href=""../profiles/profiles_menu.asp?cid=" & customerID & """>" & getCustomerName(customerID) & "</a>")
					else
						response.write("<br/>:: " & getCustomerName(customerID) & "")
					end if					
					%>
                </div>                
            </div>                   
                               
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
            	            <legend>Information</legend>
	                        <div align="left">
    	                        <img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; vertical-align:middle;">Required items are marked with an asterisk (<span class="required">*</span>).
								<% if (cLng(session(session("siteID") & "adminLoggedOn")) < 10) then %>
		                            View Customer Profile (<strong>F1</strong>)
    							<% end if %>
                            </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             
                                    
                    <!-- MISC area -->                
                    <td width="40%" valign="top" style="padding-top:7px;">&nbsp;</td>
                    <!-- STOP MISC area -->
                        
                </tr>
            </table>        
            <!-- STOP information box -->

    
    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			'clear system messages
			call clearSessionMessages()
            %>
    		<!-- STOP message notification box -->


            <!-- START Top Page section with tabs & buttons -->
            <table id="table_tabs" width="100%" cellpadding="0" cellspacing="0">            
                <tr>                
                
                    <td id="titleHeader" height="40" width="65%" align="left" class="sectionTitle">
						<%
						 if len(trim(dirBegin)) > 0 then
						 	response.write("Opening instructions")
						else
							response.write("Is an interpreter required?")
						end if
						%>
                    </td>                
                                                               
                    <!-- START Submit Button at top of page--> 
                    <td nowrap style="padding-bottom:10px;" align="left">

						<!-- NEXT button -->
						<a id="button:top" class="myCMbutton" href="#" accesskey="N" title="[Right Arrow] to move forward" onClick="this.blur(); nextButton('forward'); return false;"><span class="next" style="padding-right:10px;"><u>N</u>ext</span></a>

						<!-- ADD button -->
						<a id="button:top:add" class="myCMbutton" style="display:none;" href="#" accesskey="A" onClick="this.blur(); document.frm.submit(); return false;"><span class="add" style="padding-right:10px;"><u>A</u>dd Issue</span></a>

						<!-- BACK button -->                                                
						<a id="button:back:general" class="myCMbutton" href="#" accesskey="B" title="[Left Arrow] to move backward" onClick="this.blur(); nextButton('backward'); return false;"><span class="back" style="padding-right:10px;"><u>B</u>ack</span></a>
                        
                        <!-- CLOSE button -->
						<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); location.href='../default.asp'; return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>

                        <input type="hidden" name="idIssue" value="<%=crsid%>">
                        <input type="hidden" name="customerID" id="customerID" value="<% =customerID %>">                             
                        <input type="hidden" name="issueType" id="issueType" value="<% =issueType %>">         
                        <input type="hidden" name="anonCaller" id="anonCaller" value="<% =anonCaller %>">                                
                        <input type="hidden" name="interpreterUsed" id="interpreterUsed" value="<% =interpreterUsed %>">                                                         
                        <input type="hidden" name="firstTime" id="firstTime" value="<% =firstTime %>">                                                         
                        <input type="hidden" name="rptStatus" id="rptStatus" value="<% =rptStatus %>">      
                        <input type="hidden" name="rptDate" id="rptDate" value="<% =rptDate %>">      
                        <input type="hidden" name="rptTime" id="rptTime" value="<% =rptTime %>">                                                                                 
                        <input type="hidden" name="rptLogid" id="rptLogid" value="<% =rptLogid %>" >
                        <input type="hidden" name="action" id="action" value="<%=action%>">

                    </td>
                    <!-- STOP Submit Button -->
                             
                </tr>
            </table>
            <!-- STOP Top Page Tabs -->



            <!-- START Side Page Tabs [details] "id=tab:details" defines the section for the Top Tabs -->
            <!--#include file="../_includes/_INCissueAdd_.asp"-->
            <!-- STOP Side Page Tabs [details] -->
        

    
            <!-- START Final table -->        
            <table class="formTable" width="100%" cellpadding="0" cellspacing="0">      
                                                              
                <tr>
                    <td width="65%" valign="top" align="left" nowrap>&nbsp;</td>
                    
                    <!-- START Submit Button at bottom of page -->                 
                    <td valign="top" align="left" nowrap>
                    
						<!-- NEXT button -->    
						<a id="button:bottom" class="myCMbutton" href="#" title="[Right Arrow] to move forward" onClick="this.blur(); nextButton('forward'); return false;"><span class="next" style="padding-right:10px;"><u>N</u>ext</span></a>

						<!-- ADD button -->
						<a id="button:bottom:add" class="myCMbutton" style="display:none;" href="#" accesskey="A" onClick="this.blur(); document.frm.submit(); return false;"><span class="add" style="padding-right:10px;"><u>A</u>dd Issue</span></a>                        

						<!-- BACK button -->                                                
						<a id="button:back:general" class="myCMbutton" href="#" title="[Left Arrow] to move backward" onClick="this.blur(); nextButton('backward'); return false;"><span class="back" style="padding-right:10px;"><u>B</u>ack</span></a>
                        
                        <!-- CLOSE button -->
						<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); location.href='../default.asp'; return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>

                    </td>
                    <!-- STOP Submit Button --> 
    
                </tr>      
                                      
            </table>
            <!-- STOP Final table -->               
    
	    </td>         
	    <!-- STOP Main Middle Section -->

 
    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
	</tr>
</table>
<!-- STOP Page -->

</form>

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<script>
	function navigateAway(url) {
		window.location = url;
	}		
</script>	

<script type="text/javascript"> 
	// Initialize TinyMCE with the tab_focus option 
	tinyMCE.init({ 
		mode : "exact",
		elements : "<% =activeElements %>",
		auto_resize : true,
		theme : "advanced",
		content_css : "../_css/tinyMCE.css",
		plugins : "tabfocus,searchreplace,fullscreen,insertdatetime,paste,inlinepopups",
		theme_advanced_buttons1 : "search,separator,pasteword,separator,insertdate,inserttime,separator,fullscreen",
		theme_advanced_buttons2 : "", 
		theme_advanced_buttons3 : "", 
		theme_advanced_toolbar_location : "top", 
		theme_advanced_toolbar_align : "center", 
		onchange_callback : "setTinyMCEdirty",
		plugin_insertdate_dateFormat : "%A, %B %d, %Y",
		plugin_insertdate_timeFormat : "%I:%M %p",
		tab_focus : ':prev,:next'
	}); 
</script>     


<script type="text/javascript"> 
	// Initialize TinyMCE with the tab_focus option 
	var dateCal;
//	dateCal = new dhtmlxCalendarObject([< =activeCalendars >], true, {isWinHeader:true, headerButtons:"XT", isWinDrag:false, isYearEditable:true, isMonthEditable:true });
//	dateCal.setSkin("simplegrey");
//	dateCal.setDateFormat("%m/%d/%Y");
//	dateCal.setYearsRange(2000, 2020);
//	dateCal.setHeaderText("Issue Date");
</script>     


<script language="javascript">
	function setCategoryList(text,id) {
		//used by child window to add selected Categories
		var i, x;
		var el = document.getElementById('listCategory');		
		el.options[el.length] = new Option(text, id, false);

		//add to Categories to hidden element for adding to CRS_Category table
		var addE = document.getElementById('listCategoryAdded');
		addE.value = id + "," + addE.value;

		//check and see if value being added is in Removed list
		var delE = document.getElementById('listCategoryRemoved');
		delE.value = delE.value.replace(new RegExp(id + ",", "gi"),"");

		//add new categories to investigations combo boxes
		for (i=0;i<=100;i++){
			//check existance of element
			if (document.getElementById('rptInvCategory' + i)) {				
				var combo = document.getElementById('rptInvCategory' + i);
				//check through this combo for existance of cat already
				for(x=combo.length-1;x>=0;x--)
				{
				  //dup found, flag for not adding again
				  if (combo.options[x].value==id) {
					return false;
				  }
				}
				//not dups found, add category to this combo
				combo.options[combo.length] = new Option(text, id, false);	
			}
			//end of available elements, force stop
			else {
				return false;
			}
		}		
	}
	
	function removeCategory(obj) {
		//used to remove all selected items in an Option/Select list
		var addE = document.getElementById('listCategoryAdded');
		var delE = document.getElementById('listCategoryRemoved');
		var el = document.getElementById(obj);		
		var selIndex = el.selectedIndex;
		if (selIndex != -1) {						
			//make sure user agrees to change
			jConfirm('Remove categories from issue?', 'Confirm Delete', function(r) {
				//user agrees, remove categories
				if (r==true) {
					//cycle through all categories			
					for(i=el.length-1; i>=0; i--)
					{
					  if(el.options[i].selected)
					  {
						//add to Removed Category hidden field to delete from CRS_Category table				
						delE.value = el.options[i].value + "," + delE.value;
						
						//check and see if value being removed is in Added list
						addE.value = addE.value.replace(new RegExp(el.options[i].value + ",", "gi"),"");
											
						//remove fron list listCategory
						el.options[i] = null;				
					  }
					}
				}
				//user disagrees
				else {
					return false;
				}
			});							
		}
		//no category selected
		else {
			jAlert('No categories were selected.', 'myCM Alert');
		}
	}		
	
	function setPrimary() {
		//used by child window to add selected Categories
		var i, x;
		var el = document.getElementById('listCategory');		
		var pc = document.getElementById('primaryCategory');
		var selIndex = el.selectedIndex;
		if (selIndex != -1) {

			x=0;
			//cycle through all categories to get selected count
			for(i=el.length-1; i>=0; i--) {
			  if(el.options[i].selected) {					  
				x=x+1;
			  }
			}
			//more that one 1 was choosen so STOP
			if(x>1) {
			  jAlert('More than one category was selected.', 'myCM Alert');
			  return false;
			}
			
			//all is good...process selection of primary			
			for(i=el.length-1; i>=0; i--) {
			  //found selected category
			  if(el.options[i].selected) {
				//if was already flagged...turn off
				if (el.options[i].text.indexOf("(primary)") > -1) {
				  pc.value = "";
				  el.options[i].text = el.options[i].text.replace(" (primary)", "");
				}
				//was not flagged...turn on
				else {
				  pc.value = el.options[i].value;
				  el.options[i].text = el.options[i].text + " (primary)";
				}
			  }	
			  //not selected category...turn off just in case
			  else {
			    el.options[i].text = el.options[i].text.replace(" (primary)", "");
			  }
			}				
		//no category selected
		}
		else {
			jAlert('No categories were selected.', 'myCM Alert');			
		}				
	}	


	function categoryLookUp(obj) {
        //used by child window to add selected Categories
        var i, x;
        var el = document.getElementById(obj);		
        var selIndex = el.selectedIndex;
        if (selIndex != -1) {
        
            x=0;
            //cycle through all categories to get selected count
            for(i=el.length-1; i>=0; i--) {
              if(el.options[i].selected) {					  
                x=x+1;
              }
            }
            //more that one 1 was choosen so STOP
            if(x>1) {
              jAlert('More than one category was selected.', 'myCM Alert');
              return false;
            }
                    
            //all is good...process selection of primary			
            for(i=el.length-1; i>=0; i--) {
              //found selected category
              if(el.options[i].selected) {                          
                  //alert(el.options[i].value);
                  SimpleModal.open('../_dialogs/popup_category_record.asp?recid=' + el.options[i].value + '&pageView=<% =pageView %>', 410, 450, 'no');
              }	
            }				
                    
        //no category selected
        }
        else {
            jAlert('No category was selected.', 'myCM Alert');			
        }
    }

</script>

<script language="javascript">
	function setUserList(text,id) {
		var i, x;
		
		//used by child window to add selected users
		var el = document.getElementById('listUser');		
		el.options[el.length] = new Option(text + " (full access)", id, false);

		//add to users to hidden element for adding to CRS_Logins table
		var addE = document.getElementById('listUserAdded');
		addE.value = id + "," + addE.value;

		//check and see if value being added is in Removed list
		var delE = document.getElementById('listUserRemoved');
		delE.value = delE.value.replace(new RegExp(id + ",", "gi"),"");
				
		//add new user/manager to investigations combo boxes
		for (i=0;i<=100;i++){
			//check existance of element
			if (document.getElementById('rptInvManager' + i)) {				
				var combo = document.getElementById('rptInvManager' + i);
				//check through this combo for existance of user/manager already
				for(x=combo.length-1;x>=0;x--)
				{
				  //dup found, flag for not adding again
				  if (combo.options[x].value==id) {
					return false;
				  }
				}
				//not dups found, add user/manager to this combo
				combo.options[combo.length] = new Option(text, id, false);	
			}
			//end of available elements, force stop
			else {
				return false;
			}
		}
		
	}

	function removeUser(obj) {
		//used to remove all selected items in an Option/Select list
		var addE = document.getElementById('listUserAdded');
		var delE = document.getElementById('listUserRemoved');
		var el = document.getElementById(obj);		
		var selIndex = el.selectedIndex;
		if (selIndex != -1) {
			//make sure user agrees to change
			var agree=confirm("Remove user(s) from issue?");
			if (agree) {
				//cycle through all selected users
				for(i=el.length-1; i>=0; i--)
				{
				  if(el.options[i].selected)
				  {
					//add to Removed User hidden field to delete from CRS_Logins table				
					delE.value = el.options[i].value + "," + delE.value;
					
					//check and see if value being removed is in Added list
					addE.value = addE.value.replace(new RegExp(el.options[i].value + ",", "gi"),"");
										
					//remove fron list listUser
					el.options[i] = null;				
				  }
				}
			}
			//user disagrees				
			else {
				return false ;					
			}							
		//no users selected
		}
		else {
			alert("No users selected.")
		}
	}	

	function setUserFull(obj) {
		//used to set all Users selected to Full-access
		var addP = document.getElementById('listUserPermission');
		var el = document.getElementById(obj);		
		var selIndex = el.selectedIndex;
		
		//make sure a user/contact was selected
		if (selIndex != -1) {				
			//make sure user agrees to change
			var agree=confirm("Set user(s) permission to full access?");
			if (agree) {
				//cycle through all selected users
				for(i=el.length-1; i>=0; i--)
				{
				  if(el.options[i].selected)
				  {
					//change present text value
					el.options[i].text = el.options[i].text.replace("(read only)", "(full access)");
					//found in permissions hidden element "listUserPermission", replace for saving
					if (addP.value.indexOf(el.options[i].value + ":") > -1){
						//only make change if they were set to full access
						if (addP.value.indexOf(el.options[i].value + ":Y") > -1){					
						addP.value = addP.value.replace(new RegExp(el.options[i].value + ":Y", "gi"), el.options[i].value + ":N");
						}
					}
					//not found in permissions file, add for saving
					else {
						addP.value += el.options[i].value + ":N,";
					}				
				  }
				}				
			}
			//user disagrees				
			else {
				return false ;					
			}							
		}
		//no users were selected
		else {
			alert("No users selected.");
		}		
	}	


	function setUserReadOnly(obj) {
		//used to set all Users selected to Read-only
		var addP = document.getElementById('listUserPermission');
		var el = document.getElementById(obj);		
		var selIndex = el.selectedIndex;
			
		//make sure a user/contact was selected
		if (selIndex != -1) {
			//make sure user agrees to change		
			var agree=confirm("Set user(s) permission to read only?");
			if (agree) {
				//cycle through all selected users
				for(i=el.length-1; i>=0; i--)
				{
				  if(el.options[i].selected)
				  {
					//change present text value
					el.options[i].text = el.options[i].text.replace("(full access)", "(read only)");
					//found in permissions hidden element "listUserPermission", replace for saving
					if (addP.value.indexOf(el.options[i].value + ":") > -1){
						//only make change if they were set to full access
						if (addP.value.indexOf(el.options[i].value + ":N") > -1){					
						addP.value = addP.value.replace(new RegExp(el.options[i].value + ":N", "gi"), el.options[i].value + ":Y");
						}
					}
					//not found in permissions file, add for saving
					else {
						addP.value += el.options[i].value + ":Y,";
					}				
				  }
				}				
			}
			//user disagrees		
			else { 
				return false ;	
			}
		}
		//no users were selected
		else {
			alert("No users selected.");
		}
	}	

	function setOwner(text,id) {
		//used by child window to add selected Report Owners
		var addE = document.getElementById('rptOwner');
		addE.value = text

		//check and see if value being added is in Removed list
		var delE = document.getElementById('rptLogid');
		delE.value = id 

	}

	function setCurrentLocation(sNameID,sName,sAddress,sCity,sState,sPostal,sCountry,sHierarchy) {		
		//fill in location fields from select location_popup.asp
		document.getElementById('locNameID').value = decodeURI(sNameID);
		document.getElementById('locName').value = decodeURI(sName);
		document.getElementById('locAddr').value = decodeURI(sAddress);		
		document.getElementById('locCity').value = decodeURI(sCity);
		document.getElementById('locState').value = sState;		
		document.getElementById('locZip').value = decodeURI(sPostal);
		document.getElementById('locCountry').value = decodeURI(sCountry);		
		
		//format location hierarchy
		document.getElementById('locHierarchy').value = sHierarchy;
		var newHierarchy = sHierarchy; // WAS: sHierarchy.replace(/\*|\*/g,"&nbsp;&raquo;&nbsp;");	//replace all ":" to show friendly display
		newHierarchy = newHierarchy.replace(/\*/g,"&nbsp;");	//replace all "*" to " "  show friendly display
		newHierarchy = newHierarchy.replace(/\|/g,"&raquo;");	//replace all "|" to ">>" show friendly display				
		document.getElementById('divHierarchy').innerHTML = newHierarchy;
		
		//directive table row
		changeObjectDisplay('tr_locationHelp',''); 		
	}

	function setCurrentCaller(sFirst,sLast,sTitle,sAddress,sCity,sState,sPostal,sHome,sWork) {		
		//fill in location fields from select location_popup.asp
		document.getElementById('callerFirst').value = sFirst;
		document.getElementById('callerLast').value = sLast;		
		document.getElementById('callerTitle').value = sTitle;				
		document.getElementById('callerAddr').value = sAddress;		
		document.getElementById('callerCity').value = sCity;
		document.getElementById('callerState').value = sState;		
		document.getElementById('callerZip').value = sPostal;
		document.getElementById('callerHome').value = sHome;		
		document.getElementById('callerWork').value = sWork;						
	}

</script>

<script>
	function configDropDownOption(obj,list) {
		//used to rebuild options list with new values
		var el = document.getElementById(obj);
		var selIndex = el.selectedIndex;
		var selectedValue;
		//get current option value selected
		if (selIndex != -1) {
			for(i=el.length-1; i>=0; i--)
			{
			  if(el.options[i].selected)
			  {
				if(el.options[i].value){
					selectedValue = el.options[i].value; //current selection
				}
			  }
			}
		}
			
		//removed current selects in dropdown
		el.length = 0;
				
		//add new values to dropdown
		var i;		
		var temp = list.split("|"); //split list provided by popup_config_options.asp
		//add new options provided by user
		addOptionsItems(obj,"","-- Select --");
		for(i=0; i<temp.length; i++) {
			if (temp[i].length>1) {
				addOptionsItems(obj,temp[i],temp[i]);
			}
		}
		
		//check for existance of originally selected value		
		if(selectedValue){
			var bolMatch = false;
			i = 0;		
			for (i=0; i<el.options.length; i++) {
				if (el.options[i].text == selectedValue) {
					el.selectedIndex = i;
					bolMatch = true; //origianl still exists in list
				}
			}		
			//user removed original value, put back with "Missing:" at end of list
			if (bolMatch == false){
				addOptionsItems(obj,selectedValue,"Missing:"+selectedValue);
				el.selectedIndex = el.options.length-1;
			}
		}
		
	}
</script>

<script>
	//global variable to hold current button 
	var startButton = '<% if len(trim(dirBegin)) > 0 then response.write("0") else response.write("1") %>'
	var currentButton = parseInt(startButton);	
	//show starting table
	if (currentButton==0) {
		document.getElementById('table:begin').style.display = '';
	} else {
		document.getElementById('table:origination').style.display = '';
	}

	
	function nextButton(direction) {
		var i;
		var formError = false; //default to NO
		//id of tables and the order of appearance
		var tabOrder = "begin,origination,awareness,location,issuetype,anonymous,questions,synopsis,severity,category,add";
		
		//array of tables
		tabOrder = tabOrder.split(",");
		
		//current button/form to show
		if (direction=="forward") { 
			if (currentButton<(tabOrder.length-1)) {
				currentButton += 1;
			}			
		} else {
			if (currentButton>parseInt(startButton)) {
				currentButton -= 1;
			}
		}
		
		//cycle through tables
		for(i=parseInt(startButton); i<tabOrder.length; i++) {			
			if (i==currentButton) {
				if (direction=="forward") {					
					// ** ERROR CHECK **
					//run any checks required when moving forward
					if ((document.getElementById('issueType').value=="" || document.getElementById('issueType').value=="0") && tabOrder[i-1]=="issuetype") {
						jAlert('You must select an Issue Type.', 'myCM Alert');
						currentButton -= 1; //put button back so they don't move forward
						formError = true; //flag with errors found
					} else if (tabOrder[i-1]=="issuetype") {
						var typeSelected = document.getElementById('issueType').value;
						//hide any alredy visible
						if (document.getElementById('questionTableSet:1')!=null) document.getElementById('questionTableSet:1').style.display = 'none';
						if (document.getElementById('questionTableSet:2')!=null) document.getElementById('questionTableSet:2').style.display = 'none';						
						if (document.getElementById('questionTableSet:3')!=null) document.getElementById('questionTableSet:3').style.display = 'none';
						if (document.getElementById('questionTableSet:4')!=null) document.getElementById('questionTableSet:4').style.display = 'none';						
						//turn on question table						
						if (document.getElementById('questionTableSet:' + typeSelected)!=null) document.getElementById('questionTableSet:' + typeSelected).style.display = '';
					}
					//check anonymous/identified
					if (document.getElementById('anonCaller').value=="" && tabOrder[i-1]=="anonymous") {
						jAlert('You must select a Reporter Identity.', 'myCM Alert');
						currentButton -= 1; //put button back so they don't move forward
						formError = true; //flag with errors found
					}
					//check severityLevel
					if (document.getElementById('severityLevel').value=="" && tabOrder[i-1]=="severity") {
						jAlert('You must select a Severity Level.', 'myCM Alert');
						currentButton -= 1; //put button back so they don't move forward
						formError = true; //flag with errors found
					}
					//check awareness
					if (document.getElementById('commTool').value=="" && tabOrder[i-1]=="awareness") {
						jAlert('You must select an option for Awareness.', 'myCM Alert');
						currentButton -= 1; //put button back so they don't move forward
						formError = true; //flag with errors found
					}
					//check awareness
					if (document.getElementById('firstTime').value=="" && tabOrder[i-1]=="awareness") {
						jAlert('You must select an option for First Time Reporter.', 'myCM Alert');
						currentButton -= 1; //put button back so they don't move forward
						formError = true; //flag with errors found
					}
					//check interpreterUsed
					if (document.getElementById('interpreterUsed').value=="" && tabOrder[i-1]=="origination") {
						jAlert('You must indicated if an Interpreter was used.', 'myCM Alert');
						currentButton -= 1; //put button back so they don't move forward
						formError = true; //flag with errors found
					}
					//check language
					if (document.getElementById('language').value=="" && tabOrder[i-1]=="origination" && document.getElementById('interpreterUsed').value=="-1") {
						jAlert('You must indicated a Language interpreted.', 'myCM Alert');
						currentButton -= 1; //put button back so they don't move forward
						formError = true; //flag with errors found
					}
					//********************************************
					// ** NO ERRORS ... ALL GOOD ... MOVE FORWARD
					//********************************************
					if (formError==false) {
						if (document.getElementById('table:' + tabOrder[i-1])) document.getElementById('table:' + tabOrder[i-1]).style.display = 'none';
					}
					
				} else {
					if (document.getElementById('table:' + tabOrder[i+1])) document.getElementById('table:' + tabOrder[i+1]).style.display = 'none';
				}

				//ONLY move if no errors found in form
				if (formError==false) {
					//show requested table and header
					document.getElementById('table:' + tabOrder[i]).style.display = '';
					document.getElementById('titleHeader').innerHTML = document.getElementById('title:' + tabOrder[i]).value;
					
					//show add button
					if (tabOrder[i]=="add") {
						document.getElementById('button:top').style.display = 'none';
						document.getElementById('button:bottom').style.display = 'none';
						document.getElementById('button:top:add').style.display = '';
						document.getElementById('button:bottom:add').style.display = '';			
					} 
					//show forward button
					else {
						document.getElementById('button:top').style.display = '';
						document.getElementById('button:bottom').style.display = '';					
						document.getElementById('button:top:add').style.display = 'none';
						document.getElementById('button:bottom:add').style.display = 'none';								
					}						
				}
				
			}			
		}				
	}
	
</script>

