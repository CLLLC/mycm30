<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = true
const showDHTMLMenu = true
dim helpID : helpID = 300001 'for customers NOT cci staff
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "issue"
'===================================================================

'Database
dim mySQL, cn, rs, rs2

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************
																	
dim showCRSID '...need for NEW search box
dim startDate
dim endDate

'Issue Fields on DETAILS tab
dim issueType
dim anonCaller
dim interpreterUsed
dim interpreterNumber
dim language
dim useResolutions
dim useDeadlineDays
dim useInvDeadlineDays
dim rptResSatisfaction
dim callBack
dim callerAddr
dim callerCell
dim callerCity
dim callerEmail
dim callerFax
dim callerFirst
dim callerHome
dim callerLast
dim callerOther
dim callerPager
dim callerState
dim callerTitle
dim callerType
dim callerWork
dim callerZip
dim callerPassword
dim commTool
dim firstTime
dim locAddr
dim locCity
dim locCountry
dim locName, locNameID
dim locState
dim locZip
dim locLevel_1
dim locLevel_2
dim locLevel_3
dim locLevel_4
dim locLevel_5
dim locLevel_6
dim locLevel_7
dim locLevel_8
dim locHierarchy

dim pCaseStatusCloseOn
dim pInvestigationStatusCloseOn

dim resGiven
dim severityLevel
dim viewedBy
dim readonly
'dim logidsreadonly

'Issue Fields on NOTES tab
dim rptAction 
dim rptAddendum
'dim rptCaseMgrEmail 
'dim rptCaseMgrEmailReadOnly 
'dim rptCaseMgrName 
dim rptCaseNotes 
'dim rptCaseType 
dim rptDateClosed 
dim rptDateDeadline
dim rptDate
'dim rptDaysLeft 
'dim rptDaysLeftDateInt 
dim rptDaysOpen 
'dim rptDaysOpenDate 
'dim rptDaysOpenDateOld 
dim rptDetails
'dim rptFinancial 
dim rptInvestigation 
dim rptInvestigationStatus 
dim rptInvestigationStatusOld 
'dim rptLegal 
dim rptLogID
dim rptOwner
'dim rptMgrAppRes 
'dim rptPrivileged 
'dim rptPrivNotes 
dim rptResApp 
dim rptResAppDate
dim rptResAppBy 
dim rptResAppOld 
dim rptResolution 
dim rptResolutionOld 
'dim rptRespGroup 
dim rptSource 
dim rptStatus 
dim rptStatusOld 
dim rptSummary
'dim rptSummaryOMG
dim rptTime
dim rptUserField1, rptUserField1Default 
dim rptUserField2, rptUserField2Default
dim rptUserField3, rptUserField3Default 
dim rptUserField4, rptUserField4Default 
dim rptUserField5, rptUserField5Default 
dim rptUserField6, rptUserField6Default 
dim rptUserField7, rptUserField7Default 
dim rptUserField8, rptUserField8Default 
dim rptUserField9, rptUserField9Default 
dim rptUserField10, rptUserField10Default 
dim rptUserField11, rptUserField11Default
dim rptUserField12, rptUserField12Default 
dim rptUserField13, rptUserField13Default 
dim rptUserField14, rptUserField14Default 
dim rptUserField15, rptUserField15Default 
dim rptUserField16, rptUserField16Default 
dim rptUserField17, rptUserField17Default 
dim rptUserField18, rptUserField18Default 
dim rptUserField19, rptUserField19Default 
dim rptUserField20, rptUserField20Default 

dim mcrAvailable 'TEMP --> should be field "Confidential", SQL would not save at time of demo
dim mcrComments 'TEMP --> should be field "Confidential_Details", SQL would not save at time of demo

dim rptProgress

dim rptInvSubject
dim rptInvCategory
dim rptInvCloseDate
dim rptInvDeadline
dim rptInvDiary
dim rptInvDateAdded

dim rptQuestionCount

'Work Fields
dim action, idIssue, pgNotice
dim optArr, optArrIndex

'Used with Dynamic List Contact selections
dim intGroups, arrGroups, groupsI, intUsers
dim tmpUserID, tmpUserName, lstUserName
dim arrUsers, usersI

dim tempUser

dim rowColor, col1, col2,rowSet
col1 = "#F0F0F0"
col2 = "#FFFFFF"


'Determin DHTML pop-up calendars to Initialize...will append Investigation TextAreas also
dim activeCalendars
if uCase(viewCaseNotes) <> "N" then
	activeCalendars = "'required:date:rptDate','date:callBack','date:rptDateClosed','date:rptDateDeadline'"
else
	activeCalendars = "'required:date:rptDate','date:callBack'"
end if


'get action
action = trim(Request.QueryString("action"))
if len(action) = 0 then
	action = trim(Request.Form("action"))
end if
action = lCase(action)
'investigations added set back to just EDIT
if action = "editaddinv" then
	action = "edit"
end if
if  action <> "edit" _
and action <> "add"  _
and action <> "addfu"  then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if


'is user allowed to add new issues
'if action = "add" then
'	if addReport = "N" then
'		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
'	end if
'end if


'used to set side top and side tabs after user saves record
dim topTab, sideTab
topTab = trim(Request.QueryString("top"))
if len(topTab) <= 0 then
	topTab = "details"
end if
sideTab = trim(Request.QueryString("side"))
if len(sideTab) <= 0 then
	sideTab = "general"
end if


'get idIssue (1101-AAA-10001-01)
if action = "edit" or action = "addfu" then
	idIssue = trim(Request.QueryString("recId"))
	if len(idIssue) = 0 then
		idIssue = trim(Request.Form("recId"))
	end if
	if idIssue = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")
	end if
end if


'check user view status	for customer and issuetype
'based on vwLogins_IssueType and security sets
if action = "edit" or action = "addfu" then
	if userViewIssue(idIssue,sLogid) = False then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to view this issue.")
	end if
end if
	

'setup issue to edit
if action = "edit" or action = "addfu" then

	'check user's READ-ONLY status, NOT for CCI users only customer logins
	if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then
		mySQL = "SELECT ReadOnly FROM CRS_Logins WHERE CRSID='" & idIssue & "' AND LOGID = " & sLogid & " "		  
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		if not rs.eof then		
			readonly = rs("ReadOnly")
		end if
	end if


	'DBAdmins and CCI Admins
	if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
		'get issue record/details
		mySQL = "SELECT CRS.*, Logins.FirstName, Logins.LastName, Logins.Email, Customer_IssueType.Resolutions, Customer_IssueType.DeadlineDays, Customer_IssueType.InvDeadlineDays, Customer_IssueType.CloseOn, Customer_IssueType.InvCloseOn " _
			  & "	FROM (CRS LEFT JOIN Logins ON CRS.LOGID = Logins.LOGID) INNER JOIN Customer_IssueType ON (CRS.CustomerID = Customer_IssueType.CustomerID) AND (CRS.IssueTypeID = Customer_IssueType.IssueTypeID) " _
			  & "	WHERE CRS.CRSID='" & idIssue & "' "

	'CCI RA/RS
	elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
		'get issue record/details
		mySQL = "SELECT CRS.*, Logins.FirstName, Logins.LastName, Logins.Email, Customer_IssueType.Resolutions, Customer_IssueType.DeadlineDays, Customer_IssueType.InvDeadlineDays, Customer_IssueType.CloseOn, Customer_IssueType.InvCloseOn " _
			  & "	FROM (CRS LEFT JOIN Logins ON CRS.LOGID = Logins.LOGID) INNER JOIN Customer_IssueType ON (CRS.CustomerID = Customer_IssueType.CustomerID) AND (CRS.IssueTypeID = Customer_IssueType.IssueTypeID) " _
			  & "	WHERE CRS.CRSID='" & idIssue & "' AND CRS.IssueTypeID<>3 "

	'everyone else...				  
	else
		'get issue record/details
		mySQL = "SELECT CRS.*, Logins.FirstName, Logins.LastName, Logins.Email, Customer_IssueType.Resolutions, Customer_IssueType.DeadlineDays, Customer_IssueType.InvDeadlineDays, Customer_IssueType.CloseOn, Customer_IssueType.InvCloseOn, vwLogins_IssueType.LOGID AS [LogIDView] " _	
			  & "	FROM ((CRS LEFT JOIN Logins ON CRS.LOGID = Logins.LOGID) INNER JOIN Customer_IssueType ON (CRS.IssueTypeID = Customer_IssueType.IssueTypeID) AND (CRS.CustomerID = Customer_IssueType.CustomerID)) INNER JOIN vwLogins_IssueType ON (Customer_IssueType.IssueTypeID = vwLogins_IssueType.IssueTypeID) AND (Customer_IssueType.CustomerID = vwLogins_IssueType.CustomerID) " _
			  & " 	WHERE CRS.CRSID='" & idIssue & "' AND CRS.Progress=3 AND vwLogins_IssueType.LOGID = " & sLogid & " "
			  
	end if

	'open record
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")		
	else	
		CRSID 					= rs("crsid")
		customerID				= rs("customerid")
		issueType				= rs("issuetypeid")
		rptDate    				= rs("date")
		rptTime    				= rs("time")
		severityLevel   		= rs("severity")
		firstTime	  			= rs("firsttimeuser")
		commTool	  			= rs("communicationtool")
		callBack	  			= rs("callback")
		anonCaller	  			= rs("anonymous")
		interpreterUsed			= rs("interpreterused")
		interpreterNumber		= rs("interpreternumber")
		language				= rs("language")		
		resGiven	  			= rs("resolutiongiven")
		locNameID				= rs("location_nameid")
		locName	  				= rs("location_name")
		locAddr	  				= rs("location_address")
		locCity	  				= rs("location_city")
		locState	  			= rs("location_state")
		locZip	  				= rs("location_postalcode")
		locCountry 				= rs("location_country")		
		locLevel_1				= rs("location_level_1")
		locLevel_2				= rs("location_level_2")		
		locLevel_3				= rs("location_level_3")
		locLevel_4				= rs("location_level_4")		
		locLevel_5				= rs("location_level_5")
		locLevel_6				= rs("location_level_6")		
		locLevel_7				= rs("location_level_7")
		locLevel_8				= rs("location_level_8")		
		callerType				= rs("caller_type")
		callerFirst				= rs("caller_firstname")
		callerLast				= rs("caller_lastname")
		callerAddr				= rs("caller_address")
		callerTitle				= rs("caller_title")
		callerCity				= rs("caller_city")
		callerState				= rs("caller_state")
		callerZip				= rs("caller_postalcode")
		callerHome				= rs("caller_home")
		callerWork				= rs("caller_work")
		callerCell				= rs("caller_cell")
		callerPager				= rs("caller_pager")
		callerFax				= rs("caller_fax")
		callerOther				= rs("caller_other")
		callerEmail				= rs("caller_email")
		callerPassword			= rs("caller_password")
		viewedBy 				= rs("viewedBy")		
		rptLogID				= rs("logid")
		rptStatus				= rs("status")
		rptCaseNotes			= rs("casenotes")
		rptDateClosed			= rs("dateclosed")
		rptDateDeadline			= rs("datedeadline")		
		rptSource				= rs("source")			
		rptUserField1			= rs("userfield1")
		rptUserField2			= rs("userfield2")
		rptUserField3			= rs("userfield3")
		rptUserField4			= rs("userfield4")			
		rptUserField5			= rs("userfield5")
		rptUserField6			= rs("userfield6")
		rptUserField7			= rs("userfield7")
		rptUserField8			= rs("userfield8")
		rptUserField9			= rs("userfield9")
		rptUserField10			= rs("userfield10")			
		rptUserField11			= rs("userfield11")
		rptUserField12			= rs("userfield12")
		rptUserField13			= rs("userfield13")
		rptUserField14			= rs("userfield14")			
		rptUserField15			= rs("userfield15")
		rptUserField16			= rs("userfield16")
		rptUserField17			= rs("userfield17")
		rptUserField18			= rs("userfield18")
		rptUserField19			= rs("userfield19")
		rptUserField20			= rs("userfield20")						
		rptResAppBy				= rs("resolutionapprovedby")
		rptResAppDate			= rs("resolutionapproveddate")
		rptResolution			= rs("resolution")
		rptResolutionOld		= rs("resolution")
		rptSummary				= rs("summary")	
		rptDetails				= rs("details")	
		rptAddendum				= rs("addendum")	
		useResolutions			= rs("resolutions")				'turns on/off resolution side tab
		useDeadlineDays			= rs("DeadlineDays")			'default days for setting deadline dates
		useInvDeadlineDays		= rs("invdeadlinedays")			'default days for setting deadline dates

		mcrAvailable			= rs("mcrAvailable")		
		mcrComments				= rs("mcrComments")	

		if isNull(mcrAvailable) or mcrAvailable="" or len(mcrAvailable)<=0 then
			mcrAvailable = "N"
		end if

		pCaseStatusCloseOn 			= rs("CloseOn")		
		pInvestigationStatusCloseOn = rs("InvCloseOn")

		rptResSatisfaction		= rs("resolutionsatisfaction")
		rptProgress				= rs("progress")

		
		'build loccation hierarchy for display
		locHierarchy = locLevel_1 & "*|*" & locLevel_2 & "*|*" & locLevel_3 & "*|*" & locLevel_4 _
								  & "*|*" & locLevel_5 & "*|*" & locLevel_6 & "*|*" & locLevel_7 & "*|*" & locLevel_8
								  
		'change to proper value to be stored in database
		locHierarchy = reformatHierarchy(locHierarchy,"value")

		'set rptOwner...used for display only
		if len(rs("LastName")) > 0 then rptOwner = rs("LastName")
		if len(rs("FirstName")) > 0 and len(rptOwner) > 0 then rptOwner = rptOwner & ", " & rs("FirstName") else rptOwner = rs("FirstName")
		if len(rs("Email")) > 0 then rptOwner = rptOwner & " [" & rs("Email") & "]"

		'set Date/Time for new Issues
		if action = "addfu" then
			rptDate = Date()
			rptTime = Time()
			rptDetails = ""
			rptAddendum = ""
			rptStatus = "Open"
		end if
			
	end if
	
	call closeRS(rs)

'set/get customer ID
elseif action = "add" then
	
	'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	'NEED TO ADD SOME CODE TO ASK WHAT TYPE OF ISSUE
	'IS BEING STARTED RATHER THAN ASSUMING 1 OR 3
	'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!	
	'set Issue Type for new Issues
	issueType = 3	'customer

	'get/set customerID
	customerID = trim(lCase(Request.QueryString("cid")))
	if len(customerID) = 0 then
		customerID = trim(Request.Form("customerID"))
	end if
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if

	'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	'MIGHT NEED TO ADD LOOKING AT ABILITY TO ADD
	'TO THIS SPECIFIC ISSUE TYPE NOT JUST ACCESS
	'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!	
	'check users access to add this type of issue
	mySQL = "SELECT IssueTypeID " _
		 &  "	FROM vwLogins_IssueType " _
		 &  "	WHERE CustomerID = '" & customerID & "' AND IssueTypeID = " & issueType & " AND LOGID = " & sLogid & " "
	mySQL = mySQL
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if rs.eof then			
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You do not have the required permissions to add this issue.")
	end if	

	'get deadline day default settings
	mySQL = "SELECT DeadlineDays, InvDeadlineDays, CloseOn, InvCloseOn " _	
		  & "FROM   Customer_IssueType " _
		  & "WHERE  CustomerID = '" & customerID & "' AND IssueTypeID=" & issueType
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if not rs.eof then
		useDeadlineDays 			= rs("DeadlineDays")
		useInvDeadlineDays 			= rs("InvDeadlineDays")		
		pCaseStatusCloseOn 			= rs("CloseOn")		
		pInvestigationStatusCloseOn = rs("InvCloseOn")		
	end if

	call closeRS(rs)

	'set initial issues variables
	rptDate = Date()
	rptTime = Time()
	rptDetails = ""
	rptAddendum = ""
	rptStatus = "Open"
	anonCaller = "True"
	
end if


'customer NOT allowed to add FUP to issues 1 or 2
if issueType<>"3" and cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and action = "addfu" then	
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to add a follow-up to this issue.")
end if


'discover profile settings -- MIGHT CAN USE THIS WHEN SETTING ISSUETYPE
if action = "edit" or action = "add" or action = "addfu" then

	dim rIndicator, appShowUnassignedUserFields, appAssignFUPLogid, appAssignFUPCategory, appDeliverySystem, appOwnerOverride
	
	mySQL = "SELECT Customer.Name, Customer.Address, Customer.City, Customer.State, Customer.PostalCode, Customer.Phone_Work, "_ 
		  & "Customer.Phone_Fax, Customer.Phone_Customer, Customer.Phone_Other, rIndicator, pCaseStatusCloseOn, pInvestigationStatusCloseOn, appShowUnassignedUserFields, " _
		  & "appAssignFUPLogid, appAssignFUPCategory, appDeliverySystem, appOwnerOverride, " _
		  & "pUserField1Default, pUserField2Default, pUserField3Default, pUserField4Default, pUserField5Default, pUserField6Default, pUserField7Default, pUserField8Default, pUserField9Default, pUserField10Default, " _
		  & "pUserField11Default, pUserField12Default, pUserField13Default, pUserField14Default, pUserField15Default, pUserField16Default, pUserField17Default, pUserField18Default, pUserField19Default, pUserField20Default " _		  		  
		  & "FROM   Customer " _
		  & "WHERE  CustomerID = '" & customerID & "' "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)	
	rIndicator = rs("rIndicator")		
	'pCaseStatusCloseOn = rs("pCaseStatusCloseOn")		
	'pInvestigationStatusCloseOn = rs("pInvestigationStatusCloseOn")
	appShowUnassignedUserFields = rs("appShowUnassignedUserFields")
	appAssignFUPLogid = rs("appAssignFUPLogid")
	appAssignFUPCategory = rs("appAssignFUPCategory")	
	appDeliverySystem = rs("appDeliverySystem")	
	appOwnerOverride = rs("appOwnerOverride")	

	'default User Field values
	rptUserField1Default = rs("pUserField1Default")
	rptUserField2Default = rs("pUserField2Default")	
	rptUserField3Default = rs("pUserField3Default")
	rptUserField4Default = rs("pUserField4Default")
	rptUserField5Default = rs("pUserField5Default")
	rptUserField6Default = rs("pUserField6Default")
	rptUserField7Default = rs("pUserField7Default")
	rptUserField8Default = rs("pUserField8Default")
	rptUserField9Default = rs("pUserField9Default")	
	rptUserField10Default = rs("pUserField10Default")
	rptUserField11Default = rs("pUserField11Default")
	rptUserField12Default = rs("pUserField12Default")
	rptUserField13Default = rs("pUserField13Default")
	rptUserField14Default = rs("pUserField14Default")
	rptUserField15Default = rs("pUserField15Default")
	rptUserField16Default = rs("pUserField16Default")	
	rptUserField17Default = rs("pUserField17Default")
	rptUserField18Default = rs("pUserField18Default")
	rptUserField19Default = rs("pUserField19Default")
	rptUserField20Default = rs("pUserField20Default")
		
	'user on Help flyout window for CCI staff
	customerName = rs("Name")		
	customerProfile = rs("Address") & "<br/>" & rs("City") & ", " & rs("State") & " " & rs("PostalCode") & "<br/><br/>" & "Corporate: " & rs("Phone_Work") & "<br/>" & "Customer: " & rs("Phone_Customer")	
	
	closeRS(rs)	
	
end if


'Flag Issue has having been opened/viewed by User
if (InStr(1,viewedBy,"*|*" & sLogid & "*|*") <= 0) or len(viewedBy) = 0 or viewedBy = "" or IsNull(viewedBy) then
	if len(viewedBy) = 0 or viewedBy = "" or IsNull(viewedBy) then
		viewedBy = "*|*" & sLogid & "*|*"		
	else
		viewedBy = viewedBy & sLogid & "*|*"		
	end if		
	'Update Record
	mySQL = "UPDATE CRS SET " _
		  & "		viewedBy='" & viewedBy & "' " _
		  & " WHERE CRSID = '" & idIssue & "' "
	set rs = openRSexecute(mySQL)
end if


'add date/time report was opened
mySQL = "UPDATE CRS_Logins SET " _
	  & "       Viewed='" & Now() & "' " _
	  & " WHERE CRSID = '" & idIssue & "' AND Logid = " & sLogid
set rs = openRSexecute(mySQL)


'create follow-up CRSID
if action = "addfu" then
	mySQL="SELECT CRSID " _
	    & "  FROM   CRS " _
	    & "  WHERE  CRSID LIKE '" & Mid(idIssue,1,Len(idIssue)-2) & "%' " _
		& "	 ORDER BY CRSID desc"
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")
	else
		CRSID = (Mid(rs("crsid"),Len(rs("crsid"))-1,2)+1)
		if len(CRSID) = 1 then
			CRSID = "0" & CRSID
		end if
		CRSID = Mid(rs("crsid"),1,len(rs("crsid"))-2) & CRSID
	end if
	call closeRS(rs)
	'NEW issue follow-up #
	idIssue = CRSID
end if


'discover if any questions exist for issue
if action = "edit" then
	mySQL = "SELECT Count(CRSQuestionID) AS QuestionCount " _
		  & "	FROM CRS_question INNER JOIN Customer ON CRS_question.CustomerID = Customer.CustomerID " _
		  & "	WHERE CRS_Question.CRSID = '" & idIssue & "' AND Customer.appShowIssueQuestions='Y' "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	rptQuestionCount = rs("QuestionCount")
	call closeRS(rs)

'discover if any questions exist for customer		
elseif action = "add" or action = "addfu" then
	mySQL = "SELECT Count(QuestionID) AS QuestionCount " _
		  & "	FROM Question " _
		  & "	WHERE CustomerID='" & customerID & "' AND IssueTypeID = " & issueType
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	rptQuestionCount = rs("QuestionCount")
	call closeRS(rs)		
end if


'set Owner for newly added issue
if action = "add" then
	'is there a override owner set
	if len(appOwnerOverride) > 0 then
		mySQL = "SELECT Logid, FirstName, LastName, Email " _
			  & "	FROM Logins " _
			  & "	WHERE CustomerID='" & customerID & "' AND Logid = " & appOwnerOverride
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)		
		if not rs.eof then
			rptLogid = rs("Logid")			
			if len(rs("lastname")) > 0 then rptOwner = rs("lastname")
			if len(rs("firstname")) > 0 and len(rptOwner) > 0 then rptOwner = rptOwner & ", " & rs("firstname") else rptOwner = rs("firstname")
			if len(rs("Email")) > 0 then rptOwner = rptOwner & " [" & rs("Email") & "]"			
			call closeRS(rs)		
		else
			rptLogid = ""
		end if
	'no override continue as normal
	else
		rptLogid = sLogid
		'set rptOwner...used for display only
		if len(session(session("siteID") & "lastname")) > 0 then rptOwner = session(session("siteID") & "lastname")
		if len(session(session("siteID") & "firstname")) > 0 and len(rptOwner) > 0 then rptOwner = rptOwner & ", " & session(session("siteID") & "firstname") else rptOwner = session(session("siteID") & "firstname")
		if len(Email) > 0 then rptOwner = rptOwner & " [" & Email & "]"
	end if
end if
if rptLogID = "" or isNull(rptLogID) then
	rptLogID = 0
end if


'set default User Field values
if action = "add" or action = "addfu" then
	if len(rptUserField1Default) > 0 then rptUserField1 = rptUserField1Default
	if len(rptUserField2Default) > 0 then rptUserField2 = rptUserField2Default	
	if len(rptUserField3Default) > 0 then rptUserField3 = rptUserField3Default
	if len(rptUserField4Default) > 0 then rptUserField4 = rptUserField4Default	
	if len(rptUserField5Default) > 0 then rptUserField5 = rptUserField5Default
	if len(rptUserField6Default) > 0 then rptUserField6 = rptUserField6Default	
	if len(rptUserField7Default) > 0 then rptUserField7 = rptUserField7Default
	if len(rptUserField8Default) > 0 then rptUserField8 = rptUserField8Default	
	if len(rptUserField9Default) > 0 then rptUserField9 = rptUserField9Default
	if len(rptUserField10Default) > 0 then rptUserField10 = rptUserField10Default	
	if len(rptUserField11Default) > 0 then rptUserField11 = rptUserField11Default
	if len(rptUserField12Default) > 0 then rptUserField12 = rptUserField12Default	
	if len(rptUserField13Default) > 0 then rptUserField13 = rptUserField13Default
	if len(rptUserField14Default) > 0 then rptUserField14 = rptUserField14Default	
	if len(rptUserField15Default) > 0 then rptUserField15 = rptUserField15Default
	if len(rptUserField16Default) > 0 then rptUserField16 = rptUserField16Default	
	if len(rptUserField17Default) > 0 then rptUserField17 = rptUserField17Default
	if len(rptUserField18Default) > 0 then rptUserField18 = rptUserField18Default	
	if len(rptUserField19Default) > 0 then rptUserField19 = rptUserField19Default
	if len(rptUserField20Default) > 0 then rptUserField120 = rptUserField20Default	
end if

'set default deadline date
if action = "add" or action = "addfu" then
	if len(useDeadlineDays) > 0 then
		rptDateDeadline = dateAdd("d",useDeadlineDays,Date())
	end if
end if


'Determin TinyMCE to Initialize...will append Investigation TextAreas also
dim activeDetailsElements, activeDetailsReadOnly, activeNotesElements

' THIS WORKS - turn back on later... hide/show Details side-tab
'if uCase(viewDetailsSummary) <> "N" then
'	activeDetailsElements = "rptSummary_" & idIssue & ",rptDetails_" & idIssue & ",rptAddendum_" & idIssue	
'else
'	activeDetailsElements = ""
'end if	
activeDetailsElements = "rptSummary_" & idIssue & ",rptDetails_" & idIssue & ",rptAddendum_" & idIssue & ",mcrComments_" & idIssue



'Enable/disable Details
if uCase(editDetailsSummary) <> "N" then
	activeDetailsReadOnly = "false"
else
	if rptLogID<>sLogid then
		activeDetailsReadOnly = "true"
	else
		'allow owners of their own issue to modify
		activeDetailsReadOnly = "false"
	end if
end if	



'hide/show CaseNotes tab
if uCase(viewCaseNotes) <> "N" then
	activeNotesElements = "rptCaseNotes_" & idIssue
else
	activeNotesElements = ""
end if	


dim pageTitle
if action = "add" then
	pageTitle = "Add Issue" 
else
	pageTitle = crsid
end if

%>
<!--#include file="../_includes/_INCheader_.asp"-->

<form name="frm" id="frm" method="post" action="issues_exec.asp" style="padding:0px; margin:0px;">

<!-- ----------------------------------------------------------------- -->
<!-- AUTO SAVE fuctions for posting "frm" data to issues_exec.asp      -->
<!-- use autoPostForm('no'); to manually call form submit              -->
<!-- source: http://jquery.malsup.com/form/#getting-started            -->
<!-- ----------------------------------------------------------------- -->
<!--#include file="../_jquery/jform/autosave.asp"-->
<script type="text/javascript" src="../_jquery/jform/jquery.form.js"></script>  
<% 
if readonly = "Y" or session(session("siteID") & "userGhost")="Yes" then
	'do not activate autosave
	autoSave = 0
else
	%>
	<script type="text/javascript">
        //timer executes on the autoSAVE ASP variable for each login setting
        StartTheTimer();
    </script>
   <%
end if
%>
<!-- ----------------------------------------------------------------- -->

<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
  
	    <!-- START Left sided area -->  
		<td width="190" align="right" valign="top">    

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="../profiles/users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">
    
                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->              
                    <% if session(session("siteID") & "adminLoggedOn") < "10" then %>      
	                    <script>
   		                	stExpandSubTree('myCMtree',2,0);
       		            </script>
            		<% else %>
	                    <script>
   		                	stExpandSubTree('myCMtree',1,0);
       		            </script>
                    <% end if %>
                    <!-- STOP side menu include -->
                                    
                </td>
              </tr>
              <!-- STOP Left sided menu -->
        
            </table>

		</td>
    	<!-- STOP Left sided area -->


        <!-- START Main Middle Section -->
        <td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">    
        
			<%
			'used to STOP updating the cookie crumb if page is being recalled from a SAVE
			dim updateCrumb
            if len(session(session("siteID") & "errMsg")) > 0 or len(session(session("siteID") & "okMsg")) > 0 then
				updateCrumb = "false"
            end if
			%>
            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs('<% =updateCrumb %>'));
       	        </script>
            </div>
    
            <div style="text-align:left; margin-bottom:10px;">				            
                <div style="float:left; padding-right:2px; margin-right:2px;">
					<% if action = "add" or action = "addfu" then %>            
                        <img src="../_images/icons/32/page_white_add.png" title="Issues" width="32" height="32" align="absmiddle" style="vertical-align:middle;"> 
                    <% else %>
                        <img src="../_images/icons/32/page_white.png" title="Issues" width="32" height="32" align="absmiddle" style="vertical-align:middle;">                 
                    <% end if %>
                </div>
                <div>
                	<span class="pageTitle">Issue: <% if action = "edit" or action = "addfu" then response.Write(crsid) else response.Write("<i>&lt;none assigned&gt;</i>") %></span>
                    <%
					if (cLng(session(session("siteID") & "adminLoggedOn")) < 10) or (programAdmin = "Y") then
						response.write("<br/>:: <a href=""#"" onClick=""navigateAway('../profiles/profiles_menu.asp?cid=" & customerID & "'); return false;"">" & getCustomerName(customerID) & "</a>")						
					else
						response.write("<br/>:: " & getCustomerName(customerID) & "")
					end if					
					%>
                </div>                
            </div>                   
                               
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
            	            <legend><% response.write( getIssueTypeName(issueType) ) %></legend>                            
                            <div align="left">              
                            	<!-- Information on required fields and email triggers -->                      
                                <div align="left" style="padding-left:0px; padding-top:3px; margin:0px;">
                                   	<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; vertical-align:middle;">Required items marked with (<span class="required">*</span>). E-mail triggers marked with (<span class="emailTrigger">e</span>).
                                </div>    
                                <!-- Information about Autosave -->
                                <div align="left" style="padding-left:0px; padding-top:3px; margin:0px;">
                                	<!-- DISALLOWED USER FROM CHANGING HERE CAUSE AUTOSAVE WAS CAUSING SOME ISSUES 		-->
                                	<!-- CAN TURN BACK ON ONCE YOU FIGURE OUT WHY DUPLICATE ISSUES WERE BEING GENERATED -->   
                                	<% if 1=1 or readonly = "Y" or session(session("siteID") & "userGhost")="Yes" then %>
	                                    <img src="../_images/icons/16/page_save.png" title="Autosave" width="16" height="16" align="absmiddle" style="margin-right:3px; vertical-align:middle;">Autosave is currently
                                    <% else %>
   	                                    <img src="../_images/icons/16/page_save.png" title="Autosave" width="16" height="16" align="absmiddle" style="margin-right:3px; vertical-align:middle;"><a href="#" onClick="this.blur(); SimpleModal.open('../_dialogs/popup_config_autosave.asp?action=edit', 250, 400, 'no'); StopTheTimer(); return false;">Autosave</a> is currently
                                    <% end if %>
                                    <!-- Used by java function configAutosave to change innerHTL -->
                                    <span id="AutosaveONOFF">
                                        <% 
                                        if autoSave = 0 or autoSave = "" or len(autoSave) <=0 or isNull(autoSave) then
                                            response.write("<strong>OFF</strong>.")
                                        else
                                            response.write("<strong>ON</strong>.")
                                        end if
                                        %>
                                    </span>
                                </div>
							</div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             
                                    
                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">         

						<% if action = "add" or action = "addfu" then %>
                            <div class="infoButtons-off">
                                <img src="../_images/icons/24/printer-disabled.png" width="24" height="24" align="absmiddle" style="margin-right:6px; vertical-align:middle;"/>Print
                            </div>                   
                            <div class="infoButtons-off">
                                <img src="../_images/icons/24/email-disabled.png" width="24" height="24" align="absmiddle" style="margin-right:6px; vertical-align:middle;"/>E-mail
                            </div>
                            						
                        <% else %>                          	
                            <div class="infoButtons">
                                <a href="#" onClick="SimpleModal.open('../_dialogs/popup_issue_print.asp?recid=<% =idIssue %>&cid=<% =customerID %>', 340, 400, 'no'); StopTheTimer(); return false;"><img src="../_images/icons/24/printer.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                <a href="#" onClick="SimpleModal.open('../_dialogs/popup_issue_print.asp?recid=<% =idIssue %>&cid=<% =customerID %>', 340, 400, 'no'); StopTheTimer(); return false;">Print</a>
                            </div>                   
                            <div class="infoButtons">
                                <a href="#" onClick="SimpleModal.open('../_dialogs/popup_email.asp?recid=<% =idIssue %>&cid=<% =customerID %>', 510, 500, 'no'); StopTheTimer(); return false;"><img src="../_images/icons/24/email.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                <a href="#" onClick="SimpleModal.open('../_dialogs/popup_email.asp?recid=<% =idIssue %>&cid=<% =customerID %>', 510, 500, 'no'); StopTheTimer(); return false;">E-mail</a>
                            </div>
                            
                        <% end if %>                
                           
                    </td>
                    <!-- STOP Information buttons -->
                        
                </tr>
            </table>        
            <!-- STOP information box -->

    
    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) <= 0 and len(session(session("siteID") & "errMsg")) <= 0 then	
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")						
				response.write("		<div id=""systemMessage"" class=""statusMessageOK"" style=""display:none;"">")
				response.write("			<div class=""innerBorder"">")
				response.write("				<div class=""pad"">")
				response.write("					<div class=""icon"" style=""float:left;""></div>")
				response.write("					<div style=""margin-left:24px; margin-bottom:0px; text-align:left; height:100%;"">")
				response.write("						<span class=""title"">Notice</span><br><span id=""systemMessageTxt"" class=""description"">message...</span>")
				response.write("					</div>")
				response.write("				</div>")
				response.write("			</div>")
				response.write("		</div>  ")
				response.write("   </td>")
				response.write("</tr></table>")				
			end if
			
			'clear system messages
			call clearSessionMessages()
            %>
    		<!-- STOP message notification box -->


            <!-- START Top Page section with tabs & buttons -->
            <table id="table_tabs" width="100%" cellpadding="0" cellspacing="0">            
                <tr>
                
                    <!-- START Top Page Tabs-->
                    <td height="40" width="70%" valign="top">
                        <ul id="ul_tabs" class="tabs">
                        
                        	<!-- VISIBLE for everyone -->
                            <li id="details" style="width:125px;" class="on" onClick="clickTopTab('details','general'); resetSystemMsg('systemMessage');"><div>Details</div></li>

                            <!-- Program Admins and those w rights -->
							<% if programAdmin = "Y" or uCase(viewCaseNotes) = "Y" then 
								%>
		                        <li id="notes" style="width:125px;<% if uCase(viewCaseNotes) = "N" then response.write("display:none;")%>" class="off" onClick="clickTopTab('notes','information'); resetSystemMsg('systemMessage');"><div>Case Notes</div></li>
								<%
                            else
								'do nothing...
							end if
							%>
                                                        
                            <!-- the last id must be set to "end" -->
                            <li id="end" class="empty" style="width:150px;"><div>&nbsp;</div></li>
                            
                        </ul> 
                        
                        <input type=hidden name="topTab" id="topTab" value="<% =topTab %>">                                                                
                        <input type=hidden name="sideTab" id="sideTab" value="<% =sideTab %>">
                        
                    </td>                
                    <!-- STOP Top Page Tabs-->              
                    
                    <!-- START Submit Button at top of page--> 
                    <td nowrap style="padding-bottom:10px;" align="left">

						<div style="float:left;">
                            <!-- SAVE button -->
                            <%
                            if action = "edit" then
                                %>
                                <input type=hidden name="action" id="action" value="edit">                                
                                <input type=hidden name="dirtyTinyMCE" id="dirtyTinyMCE" value="">                                                        
                                <%
                                '<!-- SAVE button disabled, read-only, NOT used for CCI users -->
                                if readonly = "Y" then
                                    %>
                                    <a class="myCMbutton-off" href="#" onClick="this.blur(); return false;"><span class="save" style="padding-right:7px;">Save Issue</span></a>
                                    <%
                                '<!-- SAVE button, good-to-go -->	
                                else
									'uses Ajax autoPostForm('no'), the "no" supresses the popup window jGrowl and shows status at top of page.								
                                    %>                                            
									<a class="myCMbutton" href="#top" accesskey="S" onClick="this.blur(); resetSystemMsg('systemMessage'); autoPostForm('no');"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
                                    <%
                                end if
                                
                            '<!-- ADD button -->                                                            
                            elseif action = "add" or action = "addfu" then
                                %>
                                <input type=hidden name="action" value="<%=action%>">
                                <a class="myCMbutton" href="#" accesskey="A" onClick="this.blur(); document.frm.submit(); return false;"><span class="add" style="padding-right:15px;"><u>A</u>dd</span></a>
                                <%
                            end if
                            %>
                            </div>
                        
                        <div>
                            <!-- CLOSE button -->
                            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); navigateAway('../default.asp'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>
                        </div>
                               
                    	<!-- issue global variables --> 
                    	<input type=hidden name="idIssue" id="idIssue" value="<% =crsid %>">
						<input type=hidden name="customerID" id="customerID" value="<% =customerID %>">                                                 
                        <input type=hidden name="issueType" id="issueType" value="<% =issueType %>">
                        <input type=hidden name="rptProgress" id="rptProgress" value="<% =rptProgress %>">                        
                        <input type=hidden name="pCaseStatusCloseOn" id="pCaseStatusCloseOn" value="<% =pCaseStatusCloseOn %>">
                        <input type=hidden name="pInvestigationStatusCloseOn" id="pInvestigationStatusCloseOn" value="<% =pInvestigationStatusCloseOn %>">                    
                    
                    </td>
                    <!-- STOP Submit Button -->
                             
                </tr>
            </table>
            <!-- STOP Top Page Tabs -->


            <!-- START Side Page Tabs [details] "id=tab:details" defines the section for the Top Tabs -->
            <!--#include file="../_includes/_INCissueDetails_.asp"-->            
            <!-- STOP Side Page Tabs [details] -->    
        
            <!-- START Side Page Tabs [notes] "id=tab:notes" defines the section for the Top Tabs -->
            <!--#include file="../_includes/_INCissueNotes_.asp"-->            
            <!-- STOP Side Page Tabs [notes] -->
            
    
            <!-- START Final table -->        
            <table class="formTable" width="100%" cellpadding="0" cellspacing="0">      
                                                              
                <tr>
                    <td width="70%" valign="top" align="left" nowrap>&nbsp;</td>
                    
                    <!-- START Submit Button at bottom of page -->                 
                    <td valign="top" align="left" nowrap>      

   						<div style="float:left;">               
                            <!-- SAVE button -->     
                            <%
                            if action = "edit" then
                                '<!-- SAVE button disabled, read-only, NOT used for CCI users -->
                                if readonly = "Y" then
                                    %>
                                    <a class="myCMbutton-off" href="#" onClick="this.blur(); return false;"><span class="save" style="padding-right:7px;">Save Issue</span></a>
                                    <%
                                '<!-- SAVE button, good-to-go -->								
                                else
									'uses Ajax autoPostForm('no'), the "no" supresses the popup window jGrowl and shows status at top of page.
                                    %>                                            
									<a class="myCMbutton" href="#top" onClick="this.blur(); resetSystemMsg('systemMessage'); autoPostForm('no');"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
                                    <%
                                end if
                            '<!-- ADD button --> 
                            elseif action = "add" or action = "addfu" then
                                %>
                                <a class="myCMbutton" href="#" onClick="this.blur(); document.frm.submit(); return false;"><span class="add" style="padding-right:15px;"><u>A</u>dd</span></a>
                                <%
                            end if
                            %>
                        </div>
                        
                        <div>
                            <!-- CLOSE button -->
                            <a class="myCMbutton" href="#" onClick="this.blur(); navigateAway('../default.asp'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>
                   		</div>
                        
                    </td>
                    <!-- STOP Submit Button --> 
    
                </tr>      
                                      
            </table>
            <!-- STOP Final table -->               
    
	    </td>         
	    <!-- STOP Main Middle Section -->

 
    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
	</tr>
</table>
<!-- STOP Page -->

</form>

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()

'shows dialog with Issue # and Call Back date
if trim(Request.QueryString("did"))="yes" and issueType="1" then
%>
	<script type="text/javascript">
		jAlert('Your issue has been added successfully. <br><br> Issue #: <strong><% =crsid %></strong> <br><br> PIN/Pass: <strong><% =callerPassword %></strong> <br><br> Call Back: <strong><% =callBack %></strong>', 'Issue Added');
	</script>
<%
end if
%>

<script type="text/javascript"> 
	//load form where user last was...pulled from query URL or form submit
	window.onload = function () {
		clickTopTab('<% =topTab %>','<% =sideTab %>');
		clickSideTab('<% =topTab %>','<% =sideTab %>');
	}		
</script>

<script type="text/javascript"> 
	// DETAILS TAB ONLY
	// Initialize TinyMCE with the tab_focus option 
	tinyMCE.init({ 
		mode : "exact",
		forced_root_block : false,
		force_br_newlines : true,
		force_p_newlines : false,    
		convert_newlines_to_brs : true,		
		elements : "<% =activeDetailsElements %>",
		readonly : <% =activeDetailsReadOnly %>,
		auto_resize : true,
		theme : "advanced",
		content_css : "../_css/tinyMCE.css",
		plugins : "tabfocus,searchreplace,fullscreen,insertdatetime,paste,tinyautosave,inlinepopups",
		theme_advanced_buttons1 : "mybutton,mybutton2,|,bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,forecolor,backcolor,|,search,|,cut,copy,paste,pastetext,pasteword,|,insertdate,inserttime,mybutton3,|,tinyautosave,|,fullscreen",
		theme_advanced_buttons2 : "", 
		theme_advanced_buttons3 : "", 
		theme_advanced_toolbar_location : "top", 
		theme_advanced_toolbar_align : "center", 
		onchange_callback : "setTinyMCEdirty",
		plugin_insertdate_dateFormat : "%A, %B %d, %Y",
		plugin_insertdate_timeFormat : "%I:%M %p",
		tab_focus : ':prev,:next',
		paste_preprocess : function(pl, o) {				
				//o.content = o.content.replace(/<p>(&nbsp;|&#160;|\s|\u00a0)<\/p>/ig,''); //replaces <p>&nbsp</p> with "" -- TURNED OFF UNTIL NEEDED, BUT TOOK FOREVER TO FIND SO LEAVE
				o.content = o.content.replace(/<p[^>]*>/ig,''); //removes all <p> with null
				o.content = o.content.replace(/<\/p>/ig, '<br />'); //replaces all </p> with <br />
				o.content = strip_tags(o.content,'<b><u><i><br><img>'); //strips ALL html code from pasted text
			}, 		
		setup : function(ed) { 
				// Add a custom button 
				ed.addButton('mybutton', { 
					title : 'Add Subject', 
					image : '../_images/icons/20/subject_add.png', 
					onclick : function() { 
						//add subject
						if (tinyMCE.activeEditor.selection.getContent({format : 'text'})) {
							var subName = jQuery.trim(tinyMCE.activeEditor.selection.getContent({format : 'text'})); //attachSubject(jQuery.trim(tinyMCE.activeEditor.selection.getContent({format : 'text'})),true);			
							SimpleModal.open("../_dialogs/popup_config_subject.asp?recid=<% =idIssue %>&cid=<% =customerID %>&action=add&name=" + subName + "", 450, 450, "no"); 
							StopTheTimer(); 
							resetSystemMsg('systemMessage');
						}
						else {
							jAlert('No text was selected. Highlight a name<br\>within this memo box then click \'Add Subject\'.', 'myCM Alert');
						}
					} 
				}); 

				// add CUSTOM TEMPLATE button 
				// used by CCI staff only on issues_edit_00.asp
				ed.addButton('mybutton2', { 
					title : 'Insert Template', 
					image : '../_images/icons/20/document_template.png', 
					onclick : function() { 
						//set focus to active editor
						ed.focus(); 
						//stop autosave timer
						StopTheTimer();
						//open custom paragraphs for insert
						SimpleModal.open('../_dialogs/popup_template.asp?cid=<% =customerID %>', 500, 625, 'no');
					} 
				}); 

				// add CUSTOM TEMPLATE button 
				// used by all to add user, date and time
				ed.addButton('mybutton3', { 
					title : 'User Date Timestamp', 
					image : '../_images/icons/20/date_time_user.png', 
					onclick : function() { 
						//set focus to active editor
						ed.focus(); 
						
						//appends text to END of current content	
						tinyMCE.activeEditor.setContent(tinyMCE.activeEditor.getContent() + decodeURI('<% ="<br>[" & session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname") & ", " & date() & " " & time() & " ET] " %>'));
						
					} 
				}); 

			} 
	}); 
	// CASE NOTES TAB ONLY
	// Initialize TinyMCE with the tab_focus option 
	tinyMCE.init({ 
		mode : "exact",
		elements : "<% =activeNotesElements %>",
		auto_resize : true,
		theme : "advanced",
		content_css : "../_css/tinyMCE.css",
		plugins : "tabfocus,searchreplace,fullscreen,insertdatetime,paste,inlinepopups",
		theme_advanced_buttons1 : "mybutton,mybutton2,|,bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,forecolor,backcolor,|,search,|,cut,copy,paste,pastetext,pasteword,|,insertdate,inserttime,mybutton3,|,fullscreen",
		theme_advanced_buttons2 : "", 
		theme_advanced_buttons3 : "", 
		theme_advanced_toolbar_location : "top", 
		theme_advanced_toolbar_align : "center", 
		onchange_callback : "setTinyMCEdirty",
		plugin_insertdate_dateFormat : "%A, %B %d, %Y",
		plugin_insertdate_timeFormat : "%I:%M %p",
		tab_focus : ':prev,:next',
		paste_preprocess : function(pl, o) {				
				//o.content = o.content.replace(/<p>(&nbsp;|&#160;|\s|\u00a0)<\/p>/ig,''); //replaces <p>&nbsp</p> with "" -- TURNED OFF UNTIL NEEDED, BUT TOOK FOREVER TO FIND SO LEAVE
				o.content = o.content.replace(/<p[^>]*>/ig,''); //removes all <p> with null
				o.content = o.content.replace(/<\/p>/ig, '<br />'); //replaces all </p> with <br />
				o.content = strip_tags(o.content,'<b><u><i><br><img>'); //strips ALL html code from pasted text
			}, 		
		setup : function(ed) { 
				// Add a custom button 
				ed.addButton('mybutton', { 
					title : 'Add Subject', 
					image : '../_images/icons/20/subject_add.png', 
					onclick : function() { 
						//add subject
						if (tinyMCE.activeEditor.selection.getContent({format : 'text'})) {
							var subName = jQuery.trim(tinyMCE.activeEditor.selection.getContent({format : 'text'})); //attachSubject(jQuery.trim(tinyMCE.activeEditor.selection.getContent({format : 'text'})),true);			
							SimpleModal.open("../_dialogs/popup_config_subject.asp?recid=<% =idIssue %>&cid=<% =customerID %>&action=add&name=" + subName + "", 450, 450, "no"); 
							StopTheTimer(); 
							resetSystemMsg('systemMessage');
						}
						else {
							jAlert('No text was selected. Highlight a name<br\>within this memo box then click \'Add Subject\'.', 'myCM Alert');
						}
					} 
				}); 			

				// add CUSTOM TEMPLATE button 
				// used by CCI staff only on issues_edit_00.asp
				ed.addButton('mybutton2', { 
					title : 'Insert Template', 
					image : '../_images/icons/20/document_template.png', 
					onclick : function() { 
						//set focus to active editor
						ed.focus(); 
						//stop autosave timer
						StopTheTimer();
						//open custom paragraphs for insert
						SimpleModal.open('../_dialogs/popup_template.asp?cid=<% =customerID %>', 500, 625, 'no');
					} 
				}); 

				// add CUSTOM TEMPLATE button 
				// used by all to add user, date and time
				ed.addButton('mybutton3', { 
					title : 'User Date Timestamp', 
					image : '../_images/icons/20/date_time_user.png', 
					onclick : function() { 
						//set focus to active editor
						ed.focus(); 
						
						//appends text to END of current content	
						tinyMCE.activeEditor.setContent(tinyMCE.activeEditor.getContent() + decodeURI('<% ="<br>[" & session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname") & ", " & date() & " " & time() & " ET] " %>'));
						
					} 
				}); 
				
		}
		
	}); 	
	
	//append select text
	function insertHTML(text) {
		//appends text to END of current content	
		tinyMCE.activeEditor.setContent(tinyMCE.activeEditor.getContent() + decodeURI(text));	
	}

	//strips ALL html tags from sent text
	function strip_tags (input, allowed) {
		// http://kevin.vanzonneveld.net
		allowed = (((allowed || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
		var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
			commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
		return input.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
			return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
		});
	}

</script>

<script type="text/javascript"> 
	// Initialize TinyMCE with the tab_focus option 
	var dateCal;
	dateCal = new dhtmlxCalendarObject([<% =activeCalendars %>], true, {isWinHeader:true, headerButtons:"XT", isWinDrag:false, isYearEditable:true, isMonthEditable:true });
	dateCal.setSkin("simplegrey");
	dateCal.setDateFormat("%m/%d/%Y");
	dateCal.setYearsRange(2000, 2020);
	dateCal.setHeaderText("Date");
</script>     


<script language="javascript">
	function setCategoryList(text,id) {
		//used by child window to add selected Categories
		var i, x;
		var el = document.getElementById('listCategory');		
		el.options[el.length] = new Option(text + '  (Not Started)', id, false);

		//add to Categories to hidden element for adding to CRS_Category table
		var addE = document.getElementById('listCategoryAdded');
		addE.value = id + "," + addE.value;

		//check and see if value being added is in Removed list
		var delE = document.getElementById('listCategoryRemoved');
		delE.value = delE.value.replace(new RegExp(id + ",", "gi"),"");

		//add new categories to investigations combo boxes
		for (i=0;i<=100;i++){
			//check existance of element
			if (document.getElementById('rptInvCategory' + i)) {				
				var combo = document.getElementById('rptInvCategory' + i);
				//check through this combo for existance of cat already
				for(x=combo.length-1;x>=0;x--)
				{
				  //dup found, flag for not adding again
				  if (combo.options[x].value==id) {
					return false;
				  }
				}
				//not dups found, add category to this combo
				combo.options[combo.length] = new Option(text, id, false);	
			}
			//end of available elements, force stop
			else {
				return false;
			}
		}		
	}
	
	function removeCategory(obj) {
		//used to remove all selected items in an Option/Select list
		var addE = document.getElementById('listCategoryAdded');
		var delE = document.getElementById('listCategoryRemoved');
		var el = document.getElementById(obj);		
		var selIndex = el.selectedIndex;
		var i, x;
		var catInv = "||";		
		var catNotRemoved = "";

		if (selIndex != -1) {						
			//make sure user agrees to change
			jConfirm('Remove categories from issue?', 'Confirm Delete', function(r) {
																				 
				//get all investigation category assignments
				for(x=0; x < document.frm.length; ++x) {
					if(document.frm[x].id.indexOf("rptInvCategory")>=0) {
						if (document.frm[x].options[document.frm[x].selectedIndex].value) {
							catInv += '|' + document.frm[x].options[document.frm[x].selectedIndex].value + '|';
						}
					}			
				}

				//user agrees, remove categories
				if (r==true) {
					//cycle through all categories			
					for(i=el.length-1; i>=0; i--) {
					  if(el.options[i].selected) {
						//check investigations for category already being used
						if(catInv.indexOf('|' + el.options[i].value + '|')>=0) {						
							catNotRemoved += "- " + el.options[i].text + "<br/>";
						}			
						else {  
							//add to Removed Category hidden field to delete from CRS_Category table				
							delE.value = el.options[i].value + "," + delE.value;						
							//check and see if value being removed is in Added list
							addE.value = addE.value.replace(new RegExp(el.options[i].value + ",", "gi"),"");											
							//remove fron list listCategory
							el.options[i] = null;		
						}
					  }
					}
					//some categories were found in use, tell user which ones were not removed
					if (catNotRemoved.length>0) {
						jAlert('<strong>Form Error!</strong><br/><br/>The following categories are assigned<br/>to an investigation and could not be removed:<br/><br/>' + catNotRemoved, 'myCM Alert');
					}					

				}
				//user disagrees
				else {
					return false;
				}
			});							
		}
		//no category selected
		else {
			jAlert('No categories were selected.', 'myCM Alert');
		}
	}		
	
	function setPrimary() {
		//used by child window to add selected Categories
		var i, x;
		var el = document.getElementById('listCategory');		
		var pc = document.getElementById('primaryCategory');
		var selIndex = el.selectedIndex;
		if (selIndex != -1) {

			x=0;
			//cycle through all categories to get selected count
			for(i=el.length-1; i>=0; i--) {
			  if(el.options[i].selected) {					  
				x=x+1;
			  }
			}
			//more that one 1 was choosen so STOP
			if(x>1) {
			  jAlert('More than one category was selected.', 'myCM Alert');
			  return false;
			}
			
			//all is good...process selection of primary			
			for(i=el.length-1; i>=0; i--) {
			  //found selected category
			  if(el.options[i].selected) {
				//if was already flagged...turn off
				if (el.options[i].text.indexOf("(primary)") > -1) {
				  pc.value = "";
				  el.options[i].text = el.options[i].text.replace(" (primary)", "");
				}
				//was not flagged...turn on
				else {
				  pc.value = el.options[i].value;
				  el.options[i].text = el.options[i].text + " (primary)";
				}
			  }	
			  //not selected category...turn off just in case
			  else {
			    el.options[i].text = el.options[i].text.replace(" (primary)", "");
			  }
			}				
		//no category selected
		}
		else {
			jAlert('No categories were selected.', 'myCM Alert');			
		}				
	}	
		
</script>

<script language="javascript">
	function setUserList(text,id) {
		var i, x;
		
		//used by child window to add selected users
		var el = document.getElementById('listUser');		
		
		//check for contact only just for labeling purposes
		if (text.indexOf("(contact only)") > -1) {	
			el.options[el.length] = new Option(text, id, false);
		} else {
			el.options[el.length] = new Option(text + " (full access)", id, false);			
		}

		//add to users to hidden element for adding to CRS_Logins table
		var addE = document.getElementById('listUserAdded');
		addE.value = id + "," + addE.value;

		//check and see if value being added is in Removed list
		var delE = document.getElementById('listUserRemoved');
		delE.value = delE.value.replace(new RegExp(id + ",", "gi"),"");
				
		//add new user/manager to investigations combo boxes
		for (i=0;i<=100;i++){
			//check existance of element
			if (document.getElementById('rptInvManager' + i)) {				
				var combo = document.getElementById('rptInvManager' + i);
				//check through this combo for existance of user/manager already
				for(x=combo.length-1;x>=0;x--)
				{
				  //dup found, flag for not adding again
				  if (combo.options[x].value==id) {
					return false;
				  }
				}
				//not dups found, add user/manager to this combo
				combo.options[combo.length] = new Option(text, id, false);	
			}
			//end of available elements, force stop
			else {
				return false;
			}
		}
		
	}

	function removeUser(obj) {
		//used to remove all selected items in an Option/Select list
		var addE = document.getElementById('listUserAdded');
		var delE = document.getElementById('listUserRemoved');
		var el = document.getElementById(obj);		
		var selIndex = el.selectedIndex;
		var i, x;
		var mgrInv = "||";		
		var mgrNotRemoved = "";
		
		
		//make sure user was selected
		if (selIndex != -1) {						
			//make sure user agrees to change
			jConfirm('Remove selected user(s) from issue?', 'Confirm Delete', function(r) {
																					   
				//get all investigation category assignments
				for(x=0; x < document.frm.length; ++x) {
					if(document.frm[x].id.indexOf("rptInvManager")>=0) {
						if (document.frm[x].options[document.frm[x].selectedIndex].value) {
							mgrInv += '|' + document.frm[x].options[document.frm[x].selectedIndex].value + '|';
						}
					}			
				}				
				//user agrees, remove categories
				if (r==true) {
					//cycle through all selected users
					for(i=el.length-1; i>=0; i--) {
					  if(el.options[i].selected) {						  
						//check investigations for users already being used
						if(mgrInv.indexOf('|' + el.options[i].value + '|')>=0) {						
							mgrNotRemoved += "- " + el.options[i].text + "<br/>";
						}			
						else {  
							//add to Removed User hidden field to delete from CRS_Logins table				
							delE.value = el.options[i].value + "," + delE.value;							
							//check and see if value being removed is in Added list
							addE.value = addE.value.replace(new RegExp(el.options[i].value + ",", "gi"),"");												
							//remove fron list listUser
							el.options[i] = null;				
						}
					  }
					}
					//some categories were found in use, tell user which ones were not removed
					if (mgrNotRemoved.length>0) {
						jAlert('<strong>Form Error!</strong><br/><br/>The following case managers are assigned<br/>to an investigation and could not be removed:<br/><br/>' + mgrNotRemoved, 'myCM Alert');
					}					
				}
				//user disagrees
				else {
					return false;
				}
				
			});
		}
		//no users selected
		else {
			jAlert('No users were selected.', 'myCM Alert');
		}
	}

	function setUserFull(obj) {
		//used to set all Users selected to Full-access
		var addP = document.getElementById('listUserPermission');
		var el = document.getElementById(obj);		
		var selIndex = el.selectedIndex;
		var contactsFound = false;
		
		//make sure a user/contact was selected
		if (selIndex != -1) {						
		  //cycle through all selected users
		  for(i=el.length-1; i>=0; i--) {
			if(el.options[i].selected) {
			  if (el.options[i].text.indexOf("(contact only)") > -1) {				
				  jAlert('<strong>Permission Error!</strong><br/><br/>One of the selected users is a \'Contact Only\'<br/>and cannot be given permission over an issue.', 'myCM Alert');			  
				  contactsFound = true;
			  }
			}
		  }					
		}

		//make sure a user/contact was selected
		if (contactsFound == false) {
			if (selIndex != -1) {						
				//make sure user agrees to change
				jConfirm('Set user(s) permission to full access?', 'Confirm Change', function(r) {
					//user agrees, remove categories
					if (r==true) {
						//cycle through all selected users
						for(i=el.length-1; i>=0; i--)
						{
						  if(el.options[i].selected)
						  {
							//change present text value
							el.options[i].text = el.options[i].text.replace("(read only)", "(full access)");
							//found in permissions hidden element "listUserPermission", replace for saving
							if (addP.value.indexOf(el.options[i].value + ":") > -1){
								//only make change if they were set to full access
								if (addP.value.indexOf(el.options[i].value + ":Y") > -1){					
								addP.value = addP.value.replace(new RegExp(el.options[i].value + ":Y", "gi"), el.options[i].value + ":N");
								}
							}
							//not found in permissions file, add for saving
							else {
								addP.value += el.options[i].value + ":N,";
							}				
						  }
						}					
					}
					//user disagrees
					else {
						return false;
					}
				});							
			}
			//no users were selected
			else {
				jAlert('No users were selected.', 'myCM Alert');
			}		
		}
	}	

	function setUserReadOnly(obj) {
		//used to set all Users selected to Read-only
		var addP = document.getElementById('listUserPermission');
		var el = document.getElementById(obj);		
		var selIndex = el.selectedIndex;
		var contactsFound = false;
		
		//make sure a user/contact was selected
		if (selIndex != -1) {						
		  //cycle through all selected users
		  for(i=el.length-1; i>=0; i--) {
			if(el.options[i].selected) {
			  if (el.options[i].text.indexOf("(contact only)") > -1) {				
				  jAlert('<strong>Permission Error!</strong><br/><br/>One of the selected users is a \'Contact Only\'<br/>and cannot be given permission over an issue.', 'myCM Alert');			  
				  contactsFound = true;
			  }
			}
		  }					
		}

		//make sure a user/contact was selected
		if (contactsFound == false) {
			//make sure a user/contact was selected
			if (selIndex != -1) {						
				//make sure user agrees to change
				jConfirm('Set user(s) permission to read only?', 'Confirm Change', function(r) {
					//user agrees, remove categories
					if (r==true) {
						//cycle through all selected users
						for(i=el.length-1; i>=0; i--)
						{
						  if(el.options[i].selected)
						  {
							//change present text value
							el.options[i].text = el.options[i].text.replace("(full access)", "(read only)");
							//found in permissions hidden element "listUserPermission", replace for saving
							if (addP.value.indexOf(el.options[i].value + ":") > -1){
								//only make change if they were set to full access
								if (addP.value.indexOf(el.options[i].value + ":N") > -1){					
								addP.value = addP.value.replace(new RegExp(el.options[i].value + ":N", "gi"), el.options[i].value + ":Y");
								}
							}
							//not found in permissions file, add for saving
							else {
								addP.value += el.options[i].value + ":Y,";
							}				
						  }
						}				
					}
					//user disagrees
					else {
						return false;
					}
				});							
			}		
			//no users were selected
			else {
				jAlert('No users were selected.', 'myCM Alert');
			}
		}
	}	


	function userLookUp(obj) {
		//used by child window to add selected Categories
		var i, x;
		var el = document.getElementById(obj);		
		var selIndex = el.selectedIndex;
		if (selIndex != -1) {

			x=0;
			//cycle through all categories to get selected count
			for(i=el.length-1; i>=0; i--) {
			  if(el.options[i].selected) {					  
				x=x+1;
			  }
			}
			//more that one 1 was choosen so STOP
			if(x>1) {
			  jAlert('More than one user was selected.', 'myCM Alert');
			  return false;
			}
			
			//all is good...process selection of primary			
			for(i=el.length-1; i>=0; i--) {
			  //found selected category
			  if(el.options[i].selected) {
				  
				  //alert(el.options[i].value);
				  SimpleModal.open('../_dialogs/popup_user_record.asp?recid=' + el.options[i].value + '&pageview=<% =pageView %>', 500, 700, 'yes');
			  }	
			}				
			
		//no category selected
		}
		else {
			jAlert('No user was selected.', 'myCM Alert');			
		}
	}
	
	function setOwner(text,id) {
		//used by child window to add selected Report Owners
		var addE = document.getElementById('rptOwner');
		addE.value = text

		//check and see if value being added is in Removed list
		var delE = document.getElementById('rptLogid');
		delE.value = id 

	}

	function setCurrentLocation(sNameID,sName,sAddress,sCity,sState,sPostal,sCountry,sHierarchy) {		
		//fill in location fields from select location_popup.asp
		document.getElementById('locNameID').value = decodeURI(sNameID);
		document.getElementById('locName').value = decodeURI(sName);
		document.getElementById('locAddr').value = decodeURI(sAddress);		
		document.getElementById('locCity').value = decodeURI(sCity);
		document.getElementById('locState').value = sState;		
		document.getElementById('locZip').value = decodeURI(sPostal);
		document.getElementById('locCountry').value = decodeURI(sCountry);		
		
		//format location hierarchy, replace all ":" to show friendly display
		document.getElementById('locHierarchy').value = sHierarchy;		
		var newHierarchy = sHierarchy;
		newHierarchy = newHierarchy.replace(/\*/g,"&nbsp;");	//replace all "*" to " "  show friendly display
		newHierarchy = newHierarchy.replace(/\|/g,"&raquo;");	//replace all "|" to ">>" show friendly display				
		document.getElementById('divHierarchy').innerHTML = newHierarchy;
	}

	//execute Crytal Reports
	function runCrystal(report,type) {
		var idIssue = document.getElementById('idIssue').value;
		var url = "../reports/crystal/execute.asp?type="+type+"&name="+report+"&recID="+idIssue;
		window.open(url);
	}

	//for adding investigations for selected categories
	function addInvestigations(obj,issue) {
		//used to add investigations for selected categories
		var el = document.getElementById(obj);		
		var selIndex = el.selectedIndex;	
		var addE = document.getElementById('listCategoryInvAdd'); //categories hidden element for adding to Investigations table
		var actionE = document.getElementById('action');
		var url;
		var goodToGo = false;
		
		//make sure a category was selected
		if (selIndex != -1) {				
			//cycle through all selected users
			for(i=el.length-1; i>=0; i--)
			{
			  if(el.options[i].selected)
			  {
				if (addE.value.indexOf(el.options[i].value + ",") > -1) {
				  //do nothing...already added						
				}
				else {
				  //make sure investigation not already started
				  if (el.options[i].text.indexOf("(Not Started)") > -1) {					
				    //add to element used to add investigations
					goodToGo = true;
				    addE.value = el.options[i].value + "," + addE.value;
				  }
				  
				}
			  }
			}
				
			//make sure there is something to do before sending submit
			if(goodToGo==true){
				//WAS USING THIS TO CHECK FOR CHANGES, BUT...EVERYTIME YOU CLICK IN THE LIST IT TRIGGERS A CHANGE
				//TURNED OFF FOR NOW...
				//var title = "Cancel Changes";
				//var msg = "Changes have been made to this issue and <strong>must<br>be saved</strong> before continuing.<br><br>Continue and cancel changes?";				
				//if(isFormChanged(document.frm)==true) {		
					jConfirm("Add investigation(s) and associate<br/>with the selected categories?", "myCM Confirm", function(r) {
						if (r==true) {
						    actionE.value = "editaddinv";
						    document.frm.submit();
						}
						else {
							return false;
						}
					});					
				//}
				//else {
				//   actionE.value = "editaddinv"
				//    document.frm.submit();
				//}
			    //actionE.value = "editaddinv"
			    //document.frm.submit();
			}
			//something's wrong, STOP
			else{
				jAlert('All categories selected have already<br>been associated with an investigation.', 'myCM Alert');
			}
								
		}
		//no category was selected
		else {
			jAlert('No categories were selected.', 'myCM Alert');
		}		
	}	

</script>

<script>
	function configAutosave(secs) {
		//get new user setting for Autosave
		if (secs != null && secs != '0' && secs != '' ) {
			auto = 'yes';
			secs = parseInt(secs) * 60000 //convert minutes to milliseconds
			timerID = setInterval("autoPostForm('yes');", secs);
			//relabel information box at top of page
			document.getElementById('AutosaveONOFF').innerHTML = "<strong>ON</strong>.";
		}
		else {
			auto = 'no';
			StopTheTimer();
			//relabel information box at top of page
			document.getElementById('AutosaveONOFF').innerHTML = "<strong>OFF</strong>.";			
		}			
	}
</script>

<script>
	function configDropDownOption(obj,list) {
		//used to rebuild options list with new values
		var el = document.getElementById(obj);
		var selIndex = el.selectedIndex;
		var selectedValue;
		//get current option value selected
		if (selIndex != -1) {
			for(i=el.length-1; i>=0; i--)
			{
			  if(el.options[i].selected)
			  {
				if(el.options[i].value){
					selectedValue = el.options[i].value; //current selection
				}
			  }
			}
		}
			
		//removed current selects in dropdown
		el.length = 0;
				
		//add new values to dropdown
		var i;		
		var temp = list.split("|"); //split list provided by popup_config_options.asp
		//add new options provided by user
		addOptionsItems(obj,"","-- Select --");
		for(i=0; i<temp.length; i++) {
			if (temp[i].length>1) {
				addOptionsItems(obj,temp[i],temp[i]);
			}
		}
		
		//check for existance of originally selected value		
		if(selectedValue){
			var bolMatch = false;
			i = 0;		
			for (i=0; i<el.options.length; i++) {
				if (el.options[i].text == selectedValue) {
					el.selectedIndex = i;
					bolMatch = true; //origianl still exists in list
				}
			}		
			//user removed original value, put back with "Missing:" at end of list
			if (bolMatch == false){
				addOptionsItems(obj,selectedValue,"Missing:"+selectedValue);
				el.selectedIndex = el.options.length-1;
			}
		}

	}		
</script>

<script language="javascript">
	//for adding a single investigation using 
	//the ADD button
	function navigateAway(url) {
		var title = "Cancel Changes";
		var msg = "Changes have been made to this issue and <strong>must<br>be saved</strong> before continuing.<br><br>Continue and cancel changes?";
		tinyMCE.triggerSave();	//trigger save/convert of all TinyMCE text areas for proper save
		if(isFormChanged(document.frm)==true) {		
			confirmPrompt(title,msg,'redirect',url);
		}
		else {
			window.location = url;
		}
	}		
</script>

<script language="javascript">	
	// dependent on SimpleModal.js
	// note: topWin.parent is defined in SimpleModal.js and replaces "window.opener"
	function setCallback(sCRSID) {
		//make sure user agrees to change
		jConfirm('Assign new Call Back date?<br><br><strong>Note:</strong> If Severity Level has changed or categories have been added <br> you must first SAVE the issue before assigning new call back date.', 'Confirm Change', function(r) {
			//user agrees, remove categories
			if (r==true) {				
				//insert/display new callback
				$.ajax({
					url: "../_jquery/suggestbox/json-data.asp?view=crs&field=callback&term="+sCRSID,
					cache: false,
					async: false,
					dataType: "json",
					success: function(data) {
					  if(data!=null) {
						document.getElementById('date:callBack').value = data;
					  } else {
						document.getElementById('date:callBack').value = "";
					  }				
				}});				
			}
			//user disagrees
			else {
				return false;
			}
		});									
	}
</script>



