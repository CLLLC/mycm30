<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, pageSize
dim fieldsSQL, valueSQL

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim countString
dim optConfigure

dim modifiedDate : modifiedDate = Now()

dim recId
recId = trim(Request.QueryString("recId"))
if recId = "" then
	recId = request.form("recid")
	if recId = "" and action = "edit" then			
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Resolution ID.")
	elseif recId = "" and action = "add" then					
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")
	end if
end if


'Get action type 
dim action : action = Request.Form("action")
if len(action) = 0 then
	action = Request.QueryString("action")
end if
if lCase(action) <> "edit" and lCase(action) <> "add" then	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Action Type.")	
end if

'Get calling Issue #
dim readcrsid : readcrsid = Request.Form("readcrsid")
if len(readcrsid) = 0 then
	readcrsid = Request.QueryString("readcrsid")
end if

'make sure the can view this issue
'if userViewIssue(recId,sLogid) = False then
'	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to view this issue.2")
'end if


'form variables
dim resID
dim resLogid
dim resResolution
dim resApprovedBy, resApprovedByUser
dim resApprovedDate, resApprovedDateTemp
dim resDateRead
dim resDateReadBy, resDateReadByUser, resDateReadBySecurityLevel
dim resGiven
dim resSatisfaction
dim resCRSID
dim resReadBy
dim resReadCRSID

'save resolution
if request.form("save") = "save" then

	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
	<%

	'get new resolution field values	
	customerID = request.form("cid")
	resLogid = request.form("resLogid")
	resResolution = request.form("resResolution")
	resApprovedBy = request.form("resApprovedBy")
	resApprovedDate = request.form("resApprovedDate")
	resSatisfaction = request.form("resSatisfaction")
	resApprovedBy = request.form("resApprovedBy")
	
	resReadBy = request.form("resReadBy")

	'given to reporter
	if len(resSatisfaction)>0 and len(resDateReadBy)<=0 then
		resDateRead = Now()
		resDateReadBy = sLogid
		resGiven = "Yes"
		resReadCRSID = readcrsid
	else
		resDateRead = ""
		resDateReadBy = ""
		resGiven = "No"
		resReadCRSID = ""
	end if
	
	'save existing user field
	if action = "edit" then
	
		'check for change in resolution text
		mySQL = "SELECT Resolution, ApprovedBy FROM Resolution WHERE ResolutionID=" & recId & " "				  
       	set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
		if rs.eof then				
			response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Resolution ID.")	
		else
			'reset approval status if resolution changed by someone who CANNOT approve resolutions
			if (rs("Resolution")<>resResolution) and rs("ApprovedBy")>0 and editCaseResolutionApp<>"Y" then
				resApprovedBy = null
				resApprovedDate = null
			end if
		end if		
	
		'prepare update statement
		mySQL = "UPDATE Resolution Set " _
				& "CustomerID='" 	& uCase(customerID)					& "'," _
				& "Resolution='"	& replace(resResolution,"'","''") 	& "'," _				
				& "Satisfaction='" 	& resSatisfaction 					& "'," _
				& "ReadCRSID='" 	& resReadCRSID	 					& "'," _				
				& "ModifiedBy="  	& sLogid							& ", " _
				& "ModifiedDate='" 	& modifiedDate	 					& "' "
								
		'check for special existance of dates 
		if len(trim(resLogid))>0 then mySQL = mySQL & ",LOGID=" & resLogid & " " else mySQL = mySQL & ",LOGID=null "
		if len(trim(resApprovedDate))>0 then mySQL = mySQL & ",ApprovedDate='" & resApprovedDate & "' " else mySQL = mySQL & ",ApprovedDate=null "
		if len(trim(resApprovedBy))>0 then mySQL = mySQL & ",ApprovedBy=" & resApprovedBy & " " else mySQL = mySQL & ",ApprovedBy=null "
		if len(trim(resDateRead))>0 then mySQL = mySQL & ",DateRead='" & resDateRead & "' " else mySQL = mySQL & ",DateRead=null "
		if len(trim(resDateReadBy))>0 then mySQL = mySQL & ",DateReadBy=" & resDateReadBy & " " else mySQL = mySQL & ",DateReadBy=null "
								
		'finalize update query
		mySQL = mySQL & "WHERE ResolutionID=" & recId & " " 			
		
		set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

		call closeRS(rs)

		'session(session("siteID") & "okMsg") = "Resolution has been saved successfully."	
		%>
		<script type='text/javascript'>
			//add resolution to parent grid
			topWin.parent.saveResolution('<% =recId %>','<% =resApprovedDate %>','<% =resGiven %>','<% =resSatisfaction %>');
			//restart autosave timer
			topWin.parent.StartTheTimer();
			//close window	
			SimpleModal.close();
		</script>			
		<%

	'add new resolution
	elseif lCase(action) = "add" then
		dim resolutionID
		'Add Issue with Callback
		fieldsSQL = "INSERT INTO Resolution (" _
			  & "CustomerID,CRSID,LOGID,Resolution,Satisfaction,ModifiedBy,ModifiedDate, " _
			  & "ApprovedDate,ApprovedBy,DateRead,DateReadBy"
		fieldsSQL = fieldsSQL & ") "
			  
		valueSQL = " VALUES (" _
			  & "'"    	& uCase(customerID)					& "'," _
			  & "'"    	& recId								& "'," _
			  & " "    	& sLogid							& ", " _
			  & "'"    	& replace(resResolution,"'","''")	& "'," _
			  & "'"    	& resSatisfaction					& "'," _
			  & " "    	& sLogid							& ", " _
			  & "'"    	& modifiedDate						& "' "
	
		'check for empty dates
		if len(trim(resApprovedDate)) > 0 then valueSQL = valueSQL & ",'" & resApprovedDate & "' " else valueSQL = valueSQL & ",null"
		if len(trim(resApprovedBy)) > 0 then valueSQL = valueSQL & "," & resApprovedBy & " " else valueSQL = valueSQL & ",null"
		if len(trim(resDateRead)) > 0 then valueSQL = valueSQL & ",'" & resDateRead & "' " else valueSQL = valueSQL & ",null"
		if len(trim(resDateReadBy)) > 0 then valueSQL = valueSQL & "," & resDateReadBy & " " else valueSQL = valueSQL & ",null"
		valueSQL = valueSQL & ") "
	
		'set identity to pull back newly added record new record id is returned to newID
		mySQL = "SET NOCOUNT ON; " & fieldsSQL & valueSQL & " SELECT SCOPE_IDENTITY() AS newID;"
		set rs = openRSexecuteDialog(mySQL)		
		resolutionID = rs("newID")
						
		call closeRS(rs)
	
		%>
		<script type='text/javascript'>
			//add resolution to parent grid
			topWin.parent.addResolution('<% =resolutionID %>','<% =recId %>','<% =resApprovedDate %>','<% =resGiven %>','<% =resSatisfaction %>');
			//restart autosave timer
			topWin.parent.StartTheTimer();
			//close window	
			SimpleModal.close();
		</script>			
		<%
	
	end if		
	
end if

'used to set date approved Server Time, not local user time
resApprovedDateTemp = Now()

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Resolution</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>
      
	<!--JQuery required components -->
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />
    
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

	<!-- jQuery Prompt Replacements http://abeautifulsite.net/blog/2008/12/jquery-alert-dialogs/ -->
    <script src="../_jquery/prompt/jquery.alerts.js"></script>
    <link rel="stylesheet" type="text/css" href="../_jquery/prompt/jquery.alerts.css"/>

    <script src="../scripts/javascript/forms.js"></script>
        
</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="popup_config_resolution.asp" style="padding:0px; margin:0px;">
    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/document_quote.png" title="Resolution" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Resolution</span></div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">'Release to Reporter' must equal <span style="color:#F00">"Yes"</span> for reporter to receive resolution.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Resolution table -->
    <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">
    
        <%
		if lCase(action) = "edit" then
		
			dim userLabel
			
			mySQL = "SELECT Resolution.ResolutionID, Resolution.CustomerID, Resolution.CRSID, Resolution.LOGID, Resolution.Resolution, Resolution.ReadCRSID, " _
				  & "	Resolution.ApprovedBy, Resolution.ApprovedDate, Resolution.DateRead, Resolution.DateReadBy, Resolution.Satisfaction, " _
				  & "	a.FirstName + ' ' + a.LastName AS [ApprovedByUser], " _
				  & "	b.FirstName + ' ' + b.LastName AS [ReadByUser], b.SecurityLevel, " _				  
				  & "	Resolution.ModifiedBy, Resolution.ModifiedDate " _
				  & "		FROM (Resolution LEFT JOIN Logins AS a ON Resolution.ApprovedBy = a.LOGID) LEFT JOIN Logins AS b ON Resolution.DateReadBy = b.LOGID " _
				  & "		WHERE ResolutionID=" & recId & " "				  
	        set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
			if rs.eof then				
				response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Resolution ID.")	
			end if		

			resID = rs("ResolutionID")
			resLogid = rs("Logid")
			resCRSID = rs("CRSID")
			resResolution = rs("Resolution")
			resApprovedBy = rs("ApprovedBy")
			resApprovedByUser = rs("ApprovedByUser")
			resApprovedDate = rs("ApprovedDate")
			resDateRead = rs("DateRead")
			resDateReadByUser = rs("ReadByUser")			
			resDateReadBySecurityLevel = rs("SecurityLevel")			
			resSatisfaction = rs("Satisfaction")		
				
			if len(rs("ReadCRSID")) > 0 then
				readcrsid = rs("ReadCRSID")
				resReadCRSID = rs("ReadCRSID")				
			else
				'leave "readcrsid" only and pass the calling Issue #
			end if

		end if
        %>

		<!-- START Resolution Approval -->
		<tr>
			<td class="clearFormat" style="border:none; padding:0px; margin:0px;">

				<table width="100%">                       
					<tr>
						<td class="formLabel" style="width:150px; border:none; margin-bottom:0px; margin-top:0px; padding-bottom:0px; padding-top:0px;">Release to Reporter:</td>
						<td align="left" style="border:none; padding:0px; margin:0px;">
							<%
							dim approvedSelect, waitingLabel, resApprovedDisable
							if len(resApprovedDate)>0 then
                           		approvedSelect = "True"
                           	else
                         		approvedSelect = "False"
                         		waitingLabel = "<span style=""color:#F00""><em>waiting approval</em></span>"
                         	end if				

							'---------------------------------
							'SET RESOLUTION PERMISSION
							'---------------------------------							
							'CCI RS, turn off
							if (cLng(session(session("siteID") & "adminLoggedOn")) > 3 and cLng(session(session("siteID") & "adminLoggedOn")) < 10) then
								resApprovedDisable = " disabled "								
							'users witout permission
							' --> 6/26/13: Used to also include "and ProgramAdmin<>"Y" then " but now the Group settings override everything!
							elseif uCase(editCaseResolutionApp)="N" then 
								resApprovedDisable = " disabled "
							'resolution already approved and given to reporter
							elseif len(resApprovedDate)>0 and len(resDateRead)>0 then
								resApprovedDisable = " disabled "
							end if												
                            %>
                   			<div style="float:left;">
                           		<select name="resolutionApproved" id="resolutionApproved" size=1 class="inputShort" onChange="setApproval();" <% =resApprovedDisable %>>
                                	<option value="0"  <%=checkMatch(approvedSelect,"False")%>>No</option>
                                 	<option value="-1" <%=checkMatch(approvedSelect,"True")%>>Yes</option>
                               	</select> 
                                <input name="resApprovedBy" id="resApprovedBy" type="hidden" value="<% =resApprovedBy %>">
                                <input name="resApprovedDate" id="resApprovedDate" type="hidden" value="<% =resApprovedDate %>">                                
                    		</div>
                     		<div>
                       			<div class="formLabel" style="float:left; width:100px; margin-bottom:0px; padding-bottom:0px;">Approved by:</div>
                            	<div><% =resApprovedByUser & waitingLabel %><% if len(resApprovedDate) > 0 then response.write(" on " & FormatDateTime(resApprovedDate,vbShortDate) ) %></div>
                       		</div>
                        	<br>
                  		</td>                          
                  	</tr>                                                        
				</table>
                                
          	</td>
        </tr>
		<!-- STOP Resolution Approval -->
        
		
        <!-- START Resolution Text -->
		<tr>
			<td class="clearFormat" style="border:none; padding:0px; margin:0px;">                                
				<table width="100%">
					<tr>
						<td align="left" class="clearFormat" style="padding-right:5px; margin:0px;">
							<textarea name="resResolution" id="resResolution" style="width:100%; height:150px;" ><%=server.HTMLEncode(resResolution & "")%></textarea>
						</td>
					</tr>
				</table>
                                    
			</td>
		</tr>
		<!-- STOP Resolution Text -->
        

		<!-- START Resolution Templates -->
		<tr>
			<td class="clearFormat" style="border:none; padding:0px; margin:0px;">

				<table width="100%">                       
					<tr>     
           				<td align="left" style="border-top:none; padding-top:3px; margin-top:3px;">
               				<div style="float:left;">Templates:
								<%
                                '---------------------------------
                                'SET TEMPLATE PERMISSION
                                '---------------------------------							
                                'CCI RS, turn off
                                dim resTemplateDisable
								if cLng(session(session("siteID") & "adminLoggedOn")) < 4 then
									'do nothing...CCI Admins can always change
								elseif (cLng(session(session("siteID") & "adminLoggedOn")) > 3 and cLng(session(session("siteID") & "adminLoggedOn")) < 10) then
									resTemplateDisable = " disabled "								
								'resolution already approved and given to reporter
								elseif len(resApprovedDate)>0 and len(resDateRead)>0 then
									resTemplateDisable = " disabled "
                                elseif len(trim(resSatisfaction))>0 then
                                    resTemplateDisable = " disabled "
                                end if												
                                %>                                                    
                       			<select name="resTemplate" id="resTemplate" size=1 class="inputLong" <% =resTemplateDisable %> >
                         			<option value="" selected>-- Select --</option>
                            		<%
                       				mySQL="SELECT Name, Template " _
                                	& "FROM   Resolution_Template a " _
                              		& "WHERE  CustomerID='" & CustomerID & "' " _												
                               		& "ORDER By SortOrder "
                                	'set rs = openRSexecuteDialog(mySQL)
                                 	set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                	do while not rs.eof								
                                 		Response.Write "<option value=""" & rs("template") & """>" & rs("name") & "</option>"
                                     	rs.movenext
                              		loop
                               		call closeRS(rs)
                                	%>
                           		</select>
                      		</div>
              				<div>
                 				<a class="myCMbutton" href="#" onClick="this.blur(); fillResolution(); return false;"><span class="attach" style="padding-right:10px;">Load</span></a>
                     		</div>
                 		</td>                          
					</tr>                                                        
				</table>

			</td>
		</tr>
		<!-- STOP Resolution Templates -->


		<!-- START Resolution Satisfaction -->
		<tr>
			<td style="border-bottom:none; padding:0px; margin:0px; background-color:#FFFFC8;">
				<table width="100%">                       
					<tr>
						<td class="formLabel" style="width:150px; border-top:none; padding-bottom:5px; margin-bottom:5px;">Satisfaction:</td>
						<td align=left style="border-top:none; padding-bottom:5px; margin-bottom:5px;">
							<%
							'---------------------------------
							'SET SATISFACTION PERMISSION
							'---------------------------------							
							'CCI RS, turn off
							dim resSatisfactionDisable
							if cLng(session(session("siteID") & "adminLoggedOn")) < 4 then
								'do nothing...CCI Admins can always change
							elseif len(trim(resSatisfaction))>0 then
								resSatisfactionDisable = " disabled "
							elseif approvedSelect="False" then
								resSatisfactionDisable = " disabled "
							elseif uCase(editCaseResolutionApp)="N" and ProgramAdmin<>"Y" and cLng(session(session("siteID") & "adminLoggedOn")) >=10 then 
								resSatisfactionDisable = " disabled "								
							elseif InStr(ucase(resCRSID), "-" & ucase(customerid) & "-1") or InStr(ucase(resCRSID), "-" & ucase(customerid) & "-4") then
								if cLng(session(session("siteID") & "adminLoggedOn")) >=10 then
									resSatisfactionDisable = " disabled "		
								end if					
							end if												
                            %>                        
							<select name="resSatisfaction" id="resSatisfaction" size=1 class="inputLong" <% =resSatisfactionDisable %> >
                           		<option value="" selected>-- Select --</option>
                           		<option value="Satisfied"  <%=checkMatch(resSatisfaction,"Satisfied")%>>Satisfied</option>
                           		<option value="Neutral" <%=checkMatch(resSatisfaction,"Neutral")%>>Neutral</option>
                               	<option value="Dissatisfied" <%=checkMatch(resSatisfaction,"Dissatisfied")%>>Dissatisfied</option>         
                               	<option value="Aggressive" <%=checkMatch(resSatisfaction,"Aggressive")%>>Aggressive</option>         
                           	</select> 
                       	</td>
                   	</tr>                    
					<tr>
						<td class="formLabel" style="width:150px; border-top:none; padding-top:0px; margin-top:0px;">Provided to Reporter:</td>
						<td align=left style="border-top:none; padding-top:0px; margin-top:0px;">
                          	<% 
							if len(resApprovedDate)>0 and len(resSatisfaction)>0 then 
								'CCI staff can always see who gave resolution
								if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then								
									response.write("by " & resDateReadByUser & " on " & FormatDateTime(resDateRead,vbShortDate) & " [" & resReadCRSID & "]")
								'hide operators name from customers
								else
									if resDateReadBySecurityLevel < 10 then
										response.write("by CCI on " & FormatDateTime(resDateRead,vbShortDate) & " [" & resReadCRSID & "]")
									else
										response.write("by " & resDateReadByUser & " on " & FormatDateTime(resDateRead,vbShortDate) & " [" & resReadCRSID & "]")
									end if
								end if
							end if
							%>                            
                       	</td>
                   	</tr>
   				</table>                                
           	</td>
		</tr>
		<!-- STOP Resolution Satisfaction -->
        
    
		<tr>
           	<td align=left nowrap style="padding:0px; margin:0px;">&nbsp;</td>
       	</tr>
        
    </table>
	<!-- STOP Resolution table -->

	<input name="recid" type="hidden" value="<% =recId %>">
	<input name="readcrsid" type="hidden" value="<% =readcrsid %>">
	<input name="resLogid" type="hidden" value="<% =resLogid %>">        
    <input name="action" type="hidden" value="<% =action %>">    
    <input name="cid" type="hidden" value="<% =customerid %>">
    <input name="save" type="hidden" value="save">
        
	<!-- START Buttons -->
	<div style="float:right;"> 	       		
		<% 
		   'always allow CCI Admins 1 and 2's to change and reassign a resolution
		   if cLng(session(session("siteID") & "adminLoggedOn")) < 4 then %>
	        <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); clean_resolution('resResolution'); document.forms['frm'].submit();"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
		<% elseif trim(resApprovedDisable)="disabled" and len(trim(resSatisfaction))>0 then %>    
	        <a class="myCMbutton-off" href="#" accesskey="S" onClick="this.blur();"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
    	<% elseif len(resApprovedDate)>0 and (cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and uCase(editCaseResolutionApp)="N" and ProgramAdmin<>"Y") then %>
	        <a class="myCMbutton-off" href="#" accesskey="S" onClick="this.blur();"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
		<% else %>
	        <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); clean_resolution('resResolution'); document.forms['frm'].submit();"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
        <% end if %>        
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Buttons -->

</form>
        
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script>
	function setApproval() {
		var user = '<% =sLogid %>';
		var resAppDate = '<% =resApprovedDateTemp %>';
		var resApp = document.getElementById('resolutionApproved');
		var resBy = document.getElementById('resApprovedBy');
		//set approval status
		if (resApp.value=="-1" && resBy.value!=user) {
			resBy.value = user;
			//document.getElementById('resApprovedDate').value = CurrentDate() + ' ' + CurrentTime();
			document.getElementById('resApprovedDate').value = resAppDate;
		}
		//reset approval status
		else if (resApp.value=="0" && resBy.value!=null) {

			jConfirm('Setting Release to Reporter to \'No\' will disallow<br/>the resolution from be available to the reporter.<br/><br/>Continue with reseting<br/>Release to Reporter back to <strong>No</strong>?', 'Confirm Update', function(r) {
				//user agrees, remove categories
				if (r==true) {
					document.getElementById('resApprovedDate').value="";
					document.getElementById('resApprovedBy').value="";				
				}
				//user disagrees
				else {
					resApp.selectedIndex = 1;
				}
			});
		}
	}

	//fill-in resolution box
	function fillResolution() {
		var res = document.getElementById('resResolution');
		var opt = document.getElementById('resTemplate')
		if (opt.selectedIndex != 0) {
			if (!res.value) {
				res.value = opt.value;
			}
			else {
				res.value += '\r\r' + opt.value;						
			}
		}
	}		


	function clean_resolution(obj) {
		var input = document.getElementById(obj).value;		
		document.getElementById(obj).value = input.replace( new RegExp("[^a-zA-Z0-9.'-/\r/\n()\"_!@#$%*=+:?, ]","gm"),"" )				
	}
</script>

