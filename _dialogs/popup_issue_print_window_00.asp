<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%
'Database
dim mySQL, cn, rs, rsMain

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim showCRSID '...need for NEW search box
dim startDate
dim endDate

'Issue Fields on DETAILS tab
dim CRSID
dim anonCaller
dim callBack
dim interpreterUsed
dim language
dim callerAddr
dim callerBestTime
dim callerCell
dim callerCity
dim callerEmail
dim callerFax
dim callerFirst
dim callerHome
dim callerLast
dim callerOther
dim callerPager
dim callerState
dim callerTitle
dim callerType
dim callerWork
dim callerZip
dim commTool
dim firstTime
dim locAddr
dim locCity
dim locCountry
dim locName
dim locState
dim locZip
dim locLevel_1
dim locLevel_2
dim locLevel_3
dim locLevel_4
dim locLevel_5
dim locLevel_6
dim locLevel_7
dim locLevel_8
dim locHierarchy

dim mcrAvailable 
dim resGiven
dim severityLevel
dim viewedBy
dim logidsreadonly

'Issue Fields on NOTES tab
dim rptAction 
dim rptAddendum
dim rptCaseMgrEmail 
dim rptCaseMgrEmailReadOnly 
dim rptCaseMgrName 
dim rptCaseNotes 
'dim rptCaseType 
dim rptDateClosed 
dim rptDate
dim rptDaysLeft 
dim rptDaysLeftDateInt 
dim rptDaysOpen 
dim rptDaysOpenDate 
dim rptDaysOpenDateOld 
dim rptDetails
dim rptFinancial 
dim rptInvestigation 
dim rptInvestigationStatus 
dim rptInvestigationStatusOld 
dim rptLegal 
dim rptlogID 
dim rptLogIDs 
dim rptLogIDsReadOnly 
dim rptMCRAvailable 
dim rptMgrAppRes 
dim rptPrivileged 
dim rptPrivNotes 
dim rptResApp 
dim rptResAppBy 
dim rptResAppOld 
dim rptResolution 
dim rptResolutionOld 
'dim rptRespGroup 
dim rptSource 
dim rptStatus 
dim rptStatusOld 
dim rptSummary
dim rptTime
dim rptUserField1 
dim rptUserField2 
dim rptUserField3 
dim rptUserField4 
dim rptUserField5 
dim rptUserField6 
dim rptUserField7 
dim rptUserField8 
dim rptUserField9 
dim rptUserField10 
dim rptUserField11 
dim rptUserField12 
dim rptUserField13 
dim rptUserField14 
dim rptUserField15 
dim rptUserField16 
dim rptUserField17 
dim rptUserField18 
dim rptUserField19 
dim rptUserField20 


dim rptInvSubject
dim rptInvCategory
dim rptInvCloseDate
dim rptInvDiary


'Work Fields
dim action, idIssue, pgNotice
dim optArr, optArrIndex

'Used with Dynamic List Contact selections
dim intGroups, arrGroups, groupsI, intUsers
dim tmpUserID, tmpUserName, lstUserName
dim arrUsers, usersI

dim rowColor, col1, col2,rowSet
col1 = "#F0F0F0"
col2 = "#FFFFFF"

dim hideOptional
hideOptional = "true"


idIssue = trim(Request.QueryString("recId"))
if len(idIssue) = 0 then
	idIssue = trim(Request.Form("idIssue"))
end if
if idIssue = "" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")
end if


'check user view status	for customer and issuetype
'based on vwLogins_IssueType and security sets
if userViewIssue(idIssue,sLogid) = False then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to view this issue.")
end if


'pull what user selected to print
dim selSections
selSections = trim(Request.QueryString("list_checkBox"))
if len(selSections) = 0 then
	selSections = trim(Request.Form("list_checkBox"))
end if


'print only this issue or ALL issues
dim includeIssue
includeIssue = trim(Request.QueryString("includeIssue"))
if len(includeIssue) = 0 then
	includeIssue = trim(Request.Form("includeIssue"))
end if

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Print Issue</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    

	<style type="text/css">
    <!--
    .sectionHeader {
       padding:3px !important; 
	   margin:0px !important; 
	   background-color:#E2E2E2 !important; 
	   font-weight:bold !important; 
	   border-top:1px solid #5B5B5B !important; 
	   border-bottom:1px solid #5B5B5B !important;
       }
	 .printTable td {
		vertical-align:top;
		border-top:1px dotted #cccccc;
		padding-top:8px;
		padding-bottom:10px;
	 }	   
	 .printTable .printLabel {
		text-align:right;
		font-weight:bold;
		color:#35487B;
		padding-right:6px;
		width:85px; white-space:normal;
	 }	   
	-->
    </style>

	<script type="text/javascript">
		function ClipBoard() 
		{
		//var link = document.getElementById('copyLink');		
		//link.style.display = 'none';
		this.document.execCommand("SelectAll", true); 
		this.document.execCommand("Copy", true); 
		this.document.execCommand("UnSelect", true); 
		//link.style.display = '';		
		alert("This issue has been copied to your clipboard.");
		}
	</script>

</head>

<body style="padding:20px;">

<form id="frm" style="padding:0px; margin:0px;">    

	<div id="copytext">

	<%
    'Get Issue Record
    if includeIssue = "no" then
        mySQL="SELECT CRS.* " _
            & "FROM   CRS " _
            & "WHERE  CRSID='" & idIssue & "' "
            
    'Get all Related Issues
    elseif includeIssue = "yes" then
        mySQL="SELECT CRS.* " _
            & "FROM   CRS " _
            & "WHERE  CRSID Like '" & left(idIssue,len(idIssue)-3) & "%' " _
            & "ORDER BY CRSID "
    
    end if
    
    'Open recordset
    set rsMain = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
        
    if rsMain.eof then
        response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")
    end if
    
		'Loop through all issues returned
		do while not rsMain.eof
	
			'some fields are appended with a "" so len() works and the field is NOT NULL
			CRSID 					= rsMain("crsid")
			rptDate    				= rsMain("date")
			rptTime    				= rsMain("time")
			severityLevel   		= rsMain("severity")
			firstTime	  			= rsMain("firsttimeuser")
			commTool	  			= rsMain("communicationtool")
			callBack	  			= rsMain("callback")
			anonCaller	  			= rsMain("anonymous")
			resGiven	  			= rsMain("resolutiongiven")
			interpreterUsed			= rsMain("interpreterused")
			language				= rsMain("language")	
			locName	  				= rsMain("location_name")
			locAddr	  				= rsMain("location_address")
			locCity	  				= rsMain("location_city")
			locState	  			= rsMain("location_state")
			locZip	  				= rsMain("location_postalcode")
			locCountry 				= rsMain("location_country")
			locLevel_1				= rsMain("location_level_1")
			locLevel_2				= rsMain("location_level_2")	
			locLevel_3				= rsMain("location_level_3")
			locLevel_4				= rsMain("location_level_4")	
			locLevel_5				= rsMain("location_level_5")
			locLevel_6				= rsMain("location_level_6")	
			locLevel_7				= rsMain("location_level_7")
			locLevel_8				= rsMain("location_level_8")				
			callerType				= rsMain("caller_type")
			callerFirst				= rsMain("caller_firstname")
			callerLast				= rsMain("caller_lastname")
			callerAddr				= rsMain("caller_address")
			callerTitle				= rsMain("caller_title")
			callerCity				= rsMain("caller_city")
			callerState				= rsMain("caller_state")
			callerZip				= rsMain("caller_postalcode")
			callerHome				= rsMain("caller_home")
			callerWork				= rsMain("caller_work")
			callerCell				= rsMain("caller_cell")
			callerPager				= rsMain("caller_pager")
			callerFax				= rsMain("caller_fax")
			callerOther				= rsMain("caller_other")
			callerEmail				= rsMain("caller_email")	
			callerBestTime			= rsMain("caller_besttime")	
			viewedBy 				= rsMain("viewedBy")		
			rptLogID				= rsMain("logid")
			rptLogIDs				= rsMain("logids")		
			rptStatus				= rsMain("status")
			rptCaseNotes			= rsMain("casenotes")
			rptDateClosed			= rsMain("dateclosed")
			rptSource				= rsMain("source")
			'rptRespGroup			= rsMain("respgroup")
			rptUserField1			= rsMain("userfield1")
			rptUserField2			= rsMain("userfield2")
			rptUserField3			= rsMain("userfield3")
			rptUserField4			= rsMain("userfield4")	
			rptUserField5			= rsMain("userfield5")
			rptUserField6			= rsMain("userfield6")
			rptUserField7			= rsMain("userfield7")
			rptUserField8			= rsMain("userfield8")
			rptUserField9			= rsMain("userfield9")
			rptUserField10			= rsMain("userfield10")
			rptUserField11			= rsMain("userfield11")
			rptUserField12			= rsMain("userfield12")
			rptUserField13			= rsMain("userfield13")
			rptUserField14			= rsMain("userfield14")
			rptUserField15			= rsMain("userfield15")
			rptUserField16			= rsMain("userfield16")
			rptUserField17			= rsMain("userfield17")
			rptUserField18			= rsMain("userfield18")
			rptUserField19			= rsMain("userfield19")
			rptUserField20			= rsMain("userfield20")		
			rptResAppBy				= rsMain("resolutionapprovedby")
			rptResolution			= rsMain("resolution")
			rptResolutionOld		= rptResolution
			rptMCRAvailable			= rsMain("mcrAvailable")		
			rptSummary				= rsMain("summary")		
			rptDetails				= rsMain("details")	
			rptAddendum				= rsMain("addendum")
	
			'build loccation hierarchy for display
			locHierarchy = locLevel_1 & "*|*" & locLevel_2 & "*|*" & locLevel_3 & "*|*" & locLevel_4 _
									  & "*|*" & locLevel_5 & "*|*" & locLevel_6 & "*|*" & locLevel_7 & "*|*" & locLevel_8
									  
			'change to proper value to be stored in database
			locHierarchy = reformatHierarchy(locHierarchy,"string")

			dim rptQuestionCount
			mySQL = "SELECT Count(CRSQuestionID) AS QuestionCount " _
				  & "	FROM CRS_Question INNER JOIN Customer ON CRS_Question.CustomerID = Customer.CustomerID " _
				  & "	WHERE CRS_Question.CRSID = '" & CRSID & "' AND Customer.appShowIssueQuestions='Y' "
			set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
			rptQuestionCount = rs("QuestionCount")
			'close recordset		
			call closeRS(rs)

			%>

                <div style="text-align:left; margin-bottom:10px;">
                    <span class="popupTitle" onClick="ClipBoard();" style="cursor:pointer;">Issue: <% =CRSID %></span>
                </div>                   

					  	<!-- START General Form Fields -->	                    
                        <% if inStr(selSections,"details") then %>
						<table id="table:details:general" width="100%" class="printTable" cellpadding="0" cellspacing="0">

      						<tr>
      						  <td colspan="2" class="sectionHeader">Details: General</td>
   						  	</tr>
                            
      						<tr>
                                <td class="printLabel" style="border-top:none;">Date/Time:</td>
                                <td align=left style="border-top:none;"><% =rptDate %>&nbsp;<% =FormatDateTime(rptTime,3) %>&nbsp;</td>
                            </tr>

							<tr>
                                <td class="printLabel">Severity:</td>
                                <td align=left><% =reformatSeverity(severityLevel) %>&nbsp;</td>
                            </tr>
                                            
                            <tr <% if len(callBack)<=0 or isNull(callBack) then response.write("style=""display:none;""") %>>
                              <td class="printLabel">Call Back:</td>
                              <td align=left><%=callBack%>&nbsp;</td>
                            </tr>                            
                            
                            <% if hideOptional="false" then %>
						    <tr>
                                <td class="printLabel">Awareness:</td>
                                <td align=left><%=commTool%>&nbsp;</td>
                            </tr>
                        	<% end if %>
                            
                            <tr>
                              <td class="printLabel">Issue Location:</td>
                              <td align=left>                              
                                <div class="subLabel" <% if len(locName)<=0 or isNull(locName) then response.write("style=""display:none;""") %>>Name: <%=locName%>&nbsp;</div>      
                                <div class="subLabel" <% if len(locAddr)<=0 or isNull(locAddr) then response.write("style=""display:none;""") %>>Street Address:</div><div><%=locAddr%>&nbsp;</div>                                                               
                                <div class="subLabel" <% if len(locCity)<=0 or isNull(locCity) then response.write("style=""display:none;""") %>>City: <%=locCity%>&nbsp;</div>
    	                        <div class="subLabel" <% if len(locState)<=0 or isNull(locState) then response.write("style=""display:none;""") %>>State/Province: <%=locState%>&nbsp;</div>
       	                        <div class="subLabel" <% if len(locZip)<=0 or isNull(locZip) then response.write("style=""display:none;""") %>>Postal Code: <%=locZip%>&nbsp;</div>
       	                        <div class="subLabel" <% if len(locCountry)<=0 or isNull(locCountry) then response.write("style=""display:none;""") %>>Country: <%=locCountry%>&nbsp;</div>
       	                        <div class="subLabel" <% if len(locHierarchy)<=0 or isNull(locHierarchy) then response.write("style=""display:none;""") %>>Hierarchy: <% if len(locHierarchy)<=0 or isNull(locHierarchy) then response.write("<em>none assigned</em>") else response.write(locHierarchy) %></div>
                              </td>
                            </tr>
                            
                            <tr>
                              <td class="printLabel">Categories:</td>    
                              
                              <!-- START Category Table -->                         
                              <td align=left> 
									<!--  Find Categories ALREADY assigned -->
                                  	<%
									mySQL = "SELECT a.CRSID, a.[Primary], Category.CategoryID, Category.Name, Category.Sub, Investigation.Status " _
										  & "FROM (CRS_Category AS a INNER JOIN Category ON a.CategoryID = Category.CategoryID) " _
										  & "LEFT JOIN Investigation ON (a.CategoryID = Investigation.CategoryID) AND (a.CRSID = Investigation.CRSID) " _
										  & "WHERE a.CRSID = '" & CRSID & "' " _
										  & "ORDER BY Category.Name, Category.Sub "
									set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
									dim tempCat, priCategory
                    	            do while not rs.eof
										if len(rs("Sub")) > 0 then
											tempCat = rs("Name") & " [" & rs("Sub") & "]"
										else
											tempCat = rs("Name")
										end if
										if len(rs("status")) > 0 then
											tempCat = tempCat & "&nbsp;&nbsp;(" & rs("status") & ")"
										else
											tempCat = tempCat & "&nbsp;&nbsp;(Not Started)"
										end if
										if rs("primary")="Y" then
											priCategory = rs("CategoryID")
											tempCat = tempCat & " (Primary)"
										end if
										response.write(tempCat & ";&nbsp;")
                                        rs.movenext
                                    loop
                                    call closeRS(rs)									  
                        	        %>
                                <br>     

                              </td>
                              <!-- STOP Category Table -->
                              
                            </tr>                            
                        
						</table>
   						<!-- END General Form Fields -->
                                                
                        
   						<!-- START Reporter Form Fields -->                        
						<table id="table:details:reporter" width="100%" class="printTable" cellpadding="0" cellspacing="0">                
                        
      						<tr>
      						  <td colspan="2" class="sectionHeader">Details: Reporter</td>
   						  	</tr>

                            <%
							if anonCaller = "True" then 
							%>
							<tr>
							  <td class="printLabel" style="border-top:none;">Identity:</td>
							  <td align=left nowrap style="border-top:none;">Anonymous</td>
						  	</tr>
							<% end if %>

							<% if hideOptional="false" then %>
                            <tr>
                              <td class="printLabel">Interpreter Used:</td>
                              <td align=left nowrap>                              
                              	<% 
								if interpreterused = "True" then
									response.Write("Yes")
								else
									response.Write("No")								
								end if								
								%>           							                      
                              </td>
                            </tr>
							<% end if %>
                            
                            <%
							if interpreterused="True" and hideOptional="false" then 
							%>
							<tr>
                                <td class="printLabel">Language:</td>
                                <td colspan="2" align=left nowrap><%=language%>&nbsp;
							  </td>
                            </tr>
							<%
							end if
							%>

                            <%
							if anonCaller = "False" then 
							%>
                            <tr id="tr_callerName" <% if len(callerFirst & callerLast)<=0 or isNull(callerFirst & callerLast) then response.write("style=""display:none;""") %>>
                              <td class="printLabel">Name:</td>
                              <td align=left nowrap><%=callerFirst%>&nbsp;<%=callerLast%></td>
                            </tr>
							<%
							end if
							%>

							<tr <% if len(callerType)<=0 or isNull(callerType) then response.write("style=""display:none;""") %>>
                                <td class="printLabel">Relationship:</td>
                                <td align=left nowrap><%=callerType%>&nbsp;</td>
                            </tr>

                            <%
							if anonCaller = "False" then 
							%>
                            
                                <tr id="tr_callerTitle" <% if len(callerTitle)<=0 or isNull(callerTitle) then response.write("style=""display:none;""") %> >
                                    <td class="printLabel">Job Title:</td>
                                    <td align=left nowrap><%=callerTitle%>&nbsp;</td>
                                </tr>

                                <% if len(callerAddr & callerCity & callerState & callerZip) > 0 then %>
                                <tr id="tr_callerAddress">
                                  <td class="printLabel">Address:</td>
                                  <td align=left nowrap>
                                    <div class="subLabel" <% if len(callerAddr)<=0 or isNull(callerAddr) then response.write("style=""display:none;""") %>>Street Address:</div><div><%=callerAddr%>&nbsp;</div>                                                               
                                    <div class="subLabel" <% if len(callerCity)<=0 or isNull(callerCity) then response.write("style=""display:none;""") %>>City: <%=callerCity%>&nbsp;</div>
                                    <div class="subLabel" <% if len(callerState)<=0 or isNull(callerState) then response.write("style=""display:none;""") %>>State/Province: <%=callerState%>&nbsp;</div>
                                    <div class="subLabel" <% if len(callerZip)<=0 or isNull(callerZip) then response.write("style=""display:none;""") %>>Postal Code: <%=callerZip%>&nbsp;</div>
                                    &nbsp;
                                  </td>
                                </tr>
                                <% end if %>

                                <% if len(callerHome & callerWork & callerCell & callerFax & callerPager & callerOther) > 0 then %>
                                <tr id="tr_callerPhone">
                                  <td class="printLabel">Phone Number(s):</td>
                                  <td align=left nowrap>
                                    <div class="subLabel" <% if len(callerHome)<=0 or isNull(callerHome) then response.write("style=""display:none;""") %>>Home Phone: <%=callerHome%>&nbsp;</div>
                                    <div class="subLabel" <% if len(callerWork)<=0 or isNull(callerWork) then response.write("style=""display:none;""") %>>Work Phone: <%=callerWork%>&nbsp;</div>
                                    <div class="subLabel" <% if len(callerCell)<=0 or isNull(callerCell) then response.write("style=""display:none;""") %>>Mobil Phone: <%=callerCell%>&nbsp;</div>
                                    <div class="subLabel" <% if len(callerFax)<=0 or isNull(callerFax) then response.write("style=""display:none;""") %>>Fax: <%=callerFax%>&nbsp;</div>
                                    <div class="subLabel" <% if len(callerPager)<=0 or isNull(callerPager) then response.write("style=""display:none;""") %>>Pager: <%=callerPager%>&nbsp;</div>
                                    <div class="subLabel" <% if len(callerOther)<=0 or isNull(callerOther) then response.write("style=""display:none;""") %>>Other Phone: <%=callerOther%>&nbsp;</div>    
                                    &nbsp;                          
                                  </td>
                                </tr>
                                <% end if %>                                                                

                                <tr id="tr_callerEmail" <% if len(callerEmail)<=0 or isNull(callerEmail) then response.write("style=""display:none;""") %> >
                                    <td class="printLabel">E-Mail:</td>
                                    <td align=left nowrap><%=callerEmail%>&nbsp;</td>
                                </tr>
                            
							<%
							end if
							%>
                            
                            <% if hideOptional="false" then %>
                            <tr>
                              <td class="printLabel">First Time:</td>
                              <td align=left nowrap>
                              	<% 
								if firstTime = "True" then
									response.Write("Yes")
								else
									response.Write("No")								
								end if								
								%>           							                                                                                  
                              </td>
                            </tr>
							<% end if %>
                        
						</table>
   						<!-- END Reporter Form Fields -->
        

						<!-- START Question section -->        
						<% if rptQuestionCount > 0 then %>                        
   						<table id="table:details:questions" width="100%" class="printTable" cellpadding="0" cellspacing="0">

      						<tr>
      						  <td colspan="2" class="sectionHeader">Details: Questions</td>
   						  	</tr>
                            
                       		<!--  Find all Questions -->	
    						<%
							mySQL = "SELECT CRS_Question.Question, CRS_Question.Answer " _
								  & "	FROM CRS_Question INNER JOIN Question ON CRS_Question.QuestionID = Question.QuestionID " _
								  & "	WHERE CRS_Question.CRSID='" & CRSID & "' " _
								  & "	ORDER BY Question.SortOrder "
							set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                       		do while not rs.eof
								%>
                                <tr valign="top">     	
                                  <td colspan="2" align=left style="padding-left:20px;">
                                    <div style="text-align:left; font-weight:bold; color:#35487B; padding-bottom:5px; white-space:normal;"><% =rs("question") %></div>
                                    <div style="padding-left:5px;"><% =rs("answer") %>&nbsp;</div>		
                                  </td>
                                </tr>                                
								<%
                           		rs.movenext
                           	loop
                           	call closeRS(rs)
                            %>							                           
                      	</table>
						<% end if %>                        
						<!-- STOP Question section -->               


						<!-- START Summary-Detail section -->        
   						<table id="table:details:summary" width="100%" class="printTable" cellpadding="0" cellspacing="0">

      						<tr>
      						  <td colspan="2" class="sectionHeader">Details: Summary</td>
   						  	</tr>
                                                        
                           	<tr>
                      			<td class="printLabel" style="border-top:none;">Summary:</td>
                              	<td align="left" style="border-top:none;"><%=rptSummary%>&nbsp;</td>
                            </tr>
                            <tr>
                      			<td class="printLabel">Details:</td>
                              	<td align="left"><%=rptDetails%>&nbsp;</td>
                            </tr>
                            <tr>
                      			<td class="printLabel">Addendum:</td>
                              	<td align="left"><%=rptAddendum%>&nbsp;</td>
                            </tr>
                            
                      	</table>
						<!-- STOP Summary-Detail section -->               
                        <% end if %>

                                                
					  	<!-- START Information Form Fields -->	                    
                        <% if inStr(selSections,"casenotes") then %>                        
					  	<table id="table:notes:information" width="100%" class="printTable" cellpadding="0" cellspacing="0">            

      						<tr>
      						  <td colspan="2" class="sectionHeader">Notes: Information</td>
   						  	</tr>

                            <tr>
                      			<td class="printLabel" style="border-top:none;">Status:</td>
                              	<td align="left" style="border-top:none;"><%=rptStatus%>&nbsp;</td>
                            </tr>

                            <tr>
                      			<td class="printLabel">Date Closed:</td>
                              	<td align="left"><% =rptDateClosed %>&nbsp;</td>
                            </tr>

                            <tr>
                      			<td class="printLabel">Source:</td>
                              	<td align="left"><%=rptSource%>&nbsp;</td>
                            </tr>

                            <tr>
                      			<td class="printLabel">Case Managers:</td>
                              	<td align="left">
                            		<!--  Find Users ALREADY assigned -->
                             		<%
                             		mySQL="SELECT a.LogID, b.FirstName, b.LastName, a.ReadOnly, b.Email " _
                                     	& "FROM   CRS_Logins a LEFT JOIN Logins b " _
                                      	& "ON     a.LogID = b.LogID " _
                                		& "WHERE  a.CRSID = '" & CRSID & "' " _
                                   		& "ORDER BY b.LastName, b.FirstName"
									set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                               		dim tempUser
                              		do while not rs.eof
                                  		if len(rs("LastName")) > 0 then tempUser = rs("LastName")
                                    	if len(rs("FirstName")) > 0 and len(tempUser) > 0 then tempUser = tempUser & ", " & rs("FirstName") else tempUser = rs("FirstName")
                                   		if len(rs("Email")) > 0 then tempUser = tempUser & " [" & rs("Email") & "]"
                                    	if rs("ReadOnly") = "Y" then tempUser = tempUser & " (read only)" else tempUser = tempUser & " (full access)"
                                   		'add user to list
                                     	response.write(tempUser & ";&nbsp;")
                                  		rs.movenext
                                  	loop
                                	call closeRS(rs)									  
                                 	%>
                                </td>
                            </tr>

                            <tr>
                      			<td class="printLabel">Notes:</td>
                              	<td align="left"><%=rptCaseNotes%>&nbsp;</td>
                            </tr>

						</table>
                        <% end if %>
   						<!-- STOP Information Form Fields -->                        


                        <!-- START Investigations Form Fields -->  
                        <% if inStr(selSections,"investigations") then %>                                                
                        <table width="100%" class="printTable" cellpadding="0" cellspacing="0">            

                   			<tr>
                       			<td class="sectionHeader">Notes: Investigation</td>
                       		</tr>

                        	<tr>
                            	<td style="border:none; padding:0px; margin:0px;">
									<%
									'Get all investigations since only ONE is being printed
    								if includeIssue = "no" then
										mySQL = "SELECT a.InvestigationID, a.CRSID, a.LOGID, a.CategoryID, a.Name, a.Status, a.DateClosed, a.ActionTaken, a.Notes, a.Outcome, a.RiskLevel, a.Substantiated, Logins.FirstName, Logins.LastName " _
											  & "FROM   Investigation a LEFT JOIN Logins ON a.LOGID = Logins.LOGID " _
											  & "WHERE  a.CRSID Like '" & left(CRSID,len(CRSID)-3) & "%' " _
											  & "ORDER By a.CRSID, InvestigationID "
									
									'Get specific investigation since ALL issues are bing printed
    								elseif includeIssue = "yes" then
										mySQL = "SELECT a.InvestigationID, a.CRSID, a.LOGID, a.CategoryID, a.Name, a.Status, a.DateClosed, a.ActionTaken, a.Notes, a.Outcome, a.RiskLevel, a.Substantiated, Logins.FirstName, Logins.LastName " _
											  & "FROM   Investigation a LEFT JOIN Logins ON a.LOGID = Logins.LOGID " _
											  & "WHERE  a.CRSID = '" & CRSID & "' " _
											  & "ORDER By a.CRSID, InvestigationID "
									
									end if
									'open recordset
                                    set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                    
                                    'make sure there ARE investigations to show...
                                    if not rs.eof then
            
                                        do while not rs.eof
                                        %>
                                            <table width="100%" class="printTable" cellpadding="0" cellspacing="0">            

                                                <tr>
                                                    <td class="printLabel" style="background-color:#F0F0F0;">Name:</td>
                                                    <td align="left" style="background-color:#F0F0F0"><%=rs("name")%>&nbsp;</td>
                                                </tr>

                                                <tr>
                                                    <td class="printLabel">Status:</td>
                                                    <td align="left"><%=rs("status")%>&nbsp;</td>
                                                </tr>

                                                <tr>
                                                    <td class="printLabel">Investigator:</td>
                                                    <td align="left">                                                    
														<%
														if len(rs("logid")) > 0 then
															dim rsMgr
															mySQL = "SELECT Logins.LOGID, Logins.FirstName, Logins.LastName " _
																  & "FROM CRS_Logins INNER JOIN Logins ON CRS_Logins.LOGID = Logins.LOGID " _
																  & "WHERE Logins.CustomerID='" & sCustomerID & "' AND CRS_Logins.CRSID Like '" & left(CRSID,len(CRSID)-3) & "%' AND CRS_Logins.Logid=" & rs("logid") & " "_
																  & "GROUP BY Logins.LOGID, Logins.FirstName, Logins.LastName " _
																  & "ORDER BY Logins.LastName, Logins.FirstName "
															set rsMgr = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
															if not rsMgr.eof then
																'show with first and last name												
																if len(rsMgr("firstname")) > 0 then
																	response.write(rsMgr("lastname") & ", " & rsMgr("firstname"))
																'last name only
																else
																	response.write(rsMgr("lastname"))
																end if
															else
																response.write("&nbsp;")												
															end if
															call closeRS(rsMgr)
														else
															response.write("&nbsp;")																											
														end if
														%>                                                                                                                       
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="printLabel">Category:</td>
                                                    <td align="left">
														<%
														if len(rs("categoryid")) > 0 then
															dim rsCat, catClass
															mySQL = "SELECT CRS_Category.CategoryID, Category.Name, Category.Sub " _
																  & "FROM CRS_Category INNER JOIN Category ON CRS_Category.CategoryID = Category.CategoryID " _
																  & "WHERE Category.CustomerID='" & sCustomerID & "' AND CRS_Category.CRSID Like '" & left(CRSID,len(CRSID)-3) & "%' AND CRS_Category.CategoryID=" & rs("categoryid") & " " _
																  & "GROUP BY CRS_Category.CategoryID, Category.Name, Category.Sub " _
																  & "ORDER BY Category.Name, Category.Sub"																  
															set rsCat = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
															if not rsCat.eof then
																if len(rsCat("sub")) > 0 then
																	response.write(rsCat("name") & " [" & rsCat("sub") & "]")
																else
																	response.write(rsCat("name"))
																end if
															else
																response.write("&nbsp;")
															end if
															call closeRS(rsCat)
														else
															response.write("&nbsp;")																																										
														end if
														%>                                                                                                         
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="printLabel">Date Closed:</td>
                                                    <td align="left"><%=rs("dateclosed")%>&nbsp;</td>
                                                </tr>

                                                <tr>
                                                    <td class="printLabel">Action Taken:</td>
                                                    <td align="left"><%=rs("actiontaken")%>&nbsp;</td>
                                                </tr>

                                                <tr>
                                                    <td class="printLabel">Validity:</td>
                                                    <td align="left">                                                    
														<% 
                                                        if lCase(rs("substantiated")) = "substantiated" then
                                                            response.Write("Substantiated")
                                                        elseif lCase(rs("substantiated")) = "unsubstantiated" then
                                                            response.Write("Unsubstantiated")						
														else
														    response.Write("Undetermined")						
                                                        end if								
                                                        %>           							                                                                          
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td class="printLabel">Risk Level:</td>
                                                    <td align="left"><%=rs("risklevel")%>&nbsp;</td>
                                                </tr>

                                                <tr>
                                                    <td class="printLabel">Notes:</td>
                                                    <td align="left"><%=rs("notes")%>&nbsp;</td>
                                                </tr>

                                                <tr>
                                                    <td class="printLabel">Outcome:</td>
                                                    <td align="left"><%=rs("outcome")%>&nbsp;</td>
                                                </tr>
                                    
                                            </table>
                                            
                                        <%
                                            rs.movenext
                                            
                                        loop
									else
									%>

                                    <tr>
                                        <td style="padding-left:15px;">No investigations have been started for this issue.</td>
                                    </tr>
                                        
                                    <%
                                    end if
                                	call closeRS(rs)									  									
                                    %>
                                </td>
                            </tr>
                        </table>
                        <% end if %>


					  	<!-- START Document table -->	                    
						<% if inStr(selSections,"documentation") then %>
					  	<table id="table:notes:documentation" width="100%" class="printTable" cellpadding="0" cellspacing="0">            

      						<tr>
      						  <td colspan="2" class="sectionHeader">Notes: documentation</td>
   						  	</tr>

                            <tr>
                      			<td class="printLabel" style="border-top:none;">Document(s):</td>
                              	<td align="left" style="border-top:none;">
                                                                
									<%                                
									dim viewObjects, Upload, Item, tmpItemCount
									dim Directory, Dir, extPos, extImg
											
									'Ignore any errors
									On Error Resume Next
									'Set directory and find all files		
									'''Directory = dataDir & "\profiles\" & sCustomerID & "\incoming\" & left(crsid,len(crsid)-3) & "-01" & "\"		
									'CUSTID/YYYY/MM/CRSID directory format
									Directory = dataDir & "\profiles\" & sCustomerID & "\incoming\" & "20" & left(crsid,2) & "\" & mid(crsid,3,2) & "\" & left(crsid,len(crsid)-3) & "-01" & "\"
												
									'Open new ASPUpload object		
									Set Upload = Server.CreateObject("Persits.Upload")			
									Set Dir = Upload.Directory( Directory & "*.*", , True)
										
									tmpItemCount = 0
										
									'Error 33 equal 'Directory not found'
									if Err.Number <> 33 then	
										'count number of files in directory											
										for each Item in Dir
										  	if not Item.IsSubdirectory then
												tmpItemCount = tmpItemCount + 1
											end if
										next
										'files found, show to user
										if tmpItemCount > 0 then
											For Each Item in Dir
											  if Not Item.IsSubdirectory Then
                                				 response.write(Server.HtmlEncode(Item.FileName))
 												 tmpItemCount = tmpItemCount - 1
												 if tmpItemCount > 1 then
												 	response.write("<br>")
												 end if
											  end if		
		
											'move to next document					
											next
										end if
									else
										response.write("No documents are attached to this issue.")
									end if
									set Upload = nothing
									%>
                                                                    
                                </td>
                            </tr>
						</table>
                        <% end if %>
					  	<!-- STOP Document table -->	                                            
                        
                        
						<!-- START Custom Fields section -->                        
                        <% if inStr(selSections,"userfields") then %>                        
                            <%
                            dim pUserField1, pUserField2, pUserField3, pUserField4, pUserField5
							dim pUserField6, pUserField7, pUserField8, pUserField9, pUserField10
                            dim pUserField11, pUserField12, pUserField13, pUserField14, pUserField15
							dim pUserField16, pUserField17, pUserField18, pUserField19, pUserField20
							
                            dim tmpValue, valueArr, i

							'Get current configuration settings from database
							mySQL = "SELECT pUserField1, pUserField1Value, " _
								  & "       pUserField2, pUserField2Value, " _
								  & "       pUserField3, pUserField3Value, " _
								  & "       pUserField4, pUserField4Value, " _
								  & "       pUserField5, pUserField5Value, " _
								  & "       pUserField6, pUserField6Value, " _
								  & "       pUserField7, pUserField7Value, " _
								  & "       pUserField8, pUserField8Value, " _
								  & "       pUserField9, pUserField9Value, " _
								  & "       pUserField10, pUserField10Value " _
								  & "       pUserField11, pUserField11Value, " _								  
								  & "       pUserField12, pUserField12Value, " _
								  & "       pUserField13, pUserField13Value, " _
								  & "       pUserField14, pUserField14Value, " _
								  & "       pUserField15, pUserField15Value, " _
								  & "       pUserField16, pUserField16Value, " _
								  & "       pUserField17, pUserField17Value, " _
								  & "       pUserField18, pUserField18Value, " _
								  & "       pUserField19, pUserField19Value, " _
								  & "       pUserField20, pUserField20Value " _								  
								  & "FROM   Customer " _
								  & "WHERE  CustomerID='" & sCustomerID & "' "
							'get UDF names
							'set rs = openRSexecuteDialog(mySQL)
							set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
							'assign field names, have to add a "" to the end so function len() works and field not NULL
                            pUserField1 = rs("pUserField1")
							pUserField2 = rs("pUserField2")
							pUserField3 = rs("pUserField3")
							pUserField4 = rs("pUserField4")
							pUserField5 = rs("pUserField5")
							pUserField6 = rs("pUserField6")
							pUserField7 = rs("pUserField7")
							pUserField8 = rs("pUserField8")
							pUserField9 = rs("pUserField9")
							pUserField10 = rs("pUserField10")
                            pUserField11 = rs("pUserField11")
							pUserField12 = rs("pUserField12")
							pUserField13 = rs("pUserField13")
							pUserField14 = rs("pUserField14")
							pUserField15 = rs("pUserField15")
							pUserField16 = rs("pUserField16")
							pUserField17 = rs("pUserField17")
							pUserField18 = rs("pUserField18")
							pUserField19 = rs("pUserField19")
							pUserField20 = rs("pUserField20")
                            call closeRS(rs)

                            if len(pUserField1 & pUserField2 & pUserField3 & pUserField4 & pUserField5 & pUserField6 & pUserField7 & pUserField8 & pUserField9 & pUserField10) > 0 then							
							%>

	   						<table id="table:notes:customfields" width="100%" class="printTable" cellpadding="0" cellspacing="0">
                            
                                <tr>
                                  <td colspan="2" class="sectionHeader">Notes: User Fields</td>
                                </tr>
                                                        
                                <tr <% if len(pUserField1)<=0 or isNull(pUserField1) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel" style="border-top:none;"><%=pUserField1%>:</td>
                                    <td align="left" style="border-top:none;"><% =rptUserField1 %>&nbsp;</td>
                                </tr>
                                <tr<% if len(pUserField2)<=0 or isNull(pUserField2) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField2%>:</td>
                                    <td align="left"><% =rptUserField2 %>&nbsp;</td>
                                </tr>
                                <tr <% if len(pUserField3)<=0 or isNull(pUserField3) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField3%>:</td>
                                    <td align="left"><% =rptUserField3 %>&nbsp;</td>
                                </tr>
                                <tr <% if len(pUserField4)<=0 or isNull(pUserField4) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField4%>:</td>
                                    <td align="left"><% =rptUserField4 %>&nbsp;</td>
                                </tr>
                                <tr <% if len(pUserField5)<=0 or isNull(pUserField5) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField5%>:</td>
                                    <td align="left"><% =rptUserField5 %>&nbsp;</td>
                                </tr>
                                <tr <% if len(pUserField6)<=0 or isNull(pUserField6) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField6%>:</td>
                                    <td align="left"><% =rptUserField6 %>&nbsp;</td>
                                </tr>
                                <tr <% if len(pUserField7)<=0 or isNull(pUserField7) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField7%>:</td>
                                    <td align="left"><% =rptUserField7 %>&nbsp;</td>
                                </tr>
                                <tr <% if len(pUserField8)<=0 or isNull(pUserField8) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField8%>:</td>
                                    <td align="left"><% =rptUserField8 %>&nbsp;</td>
                                </tr>
                                <tr <% if len(pUserField9)<=0 or isNull(pUserField9) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField9%>:</td>
                                    <td align="left"><% =rptUserField9 %>&nbsp;</td>
                                </tr>                            
                                <tr <% if len(pUserField10)<=0 or isNull(pUserField10) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField10%>:</td>
                                    <td align="left"><% =rptUserField10 %>&nbsp;</td>
                                </tr>
                                <tr <% if len(pUserField11)<=0 or isNull(pUserField11) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField11%>:</td>
                                    <td align="left"><% =rptUserField11 %>&nbsp;</td>
                                </tr>
                                <tr <% if len(pUserField12)<=0 or isNull(pUserField12) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField12%>:</td>
                                    <td align="left"><% =rptUserField12 %>&nbsp;</td>
                                </tr>
                                <tr <% if len(pUserField13)<=0 or isNull(pUserField13) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField13%>:</td>
                                    <td align="left"><% =rptUserField13 %>&nbsp;</td>
                                </tr>
                                <tr <% if len(pUserField14)<=0 or isNull(pUserField14) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField14%>:</td>
                                    <td align="left"><% =rptUserField14 %>&nbsp;</td>
                                </tr>
                                <tr <% if len(pUserField15)<=0 or isNull(pUserField15) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField15%>:</td>
                                    <td align="left"><% =rptUserField15 %>&nbsp;</td>
                                </tr>
                                <tr <% if len(pUserField16)<=0 or isNull(pUserField16) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField16%>:</td>
                                    <td align="left"><% =rptUserField16 %>&nbsp;</td>
                                </tr>
                                <tr <% if len(pUserField17)<=0 or isNull(pUserField17) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField17%>:</td>
                                    <td align="left"><% =rptUserField17 %>&nbsp;</td>
                                </tr>
                                <tr <% if len(pUserField18)<=0 or isNull(pUserField18) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField18%>:</td>
                                    <td align="left"><% =rptUserField18 %>&nbsp;</td>
                                </tr>
                                <tr <% if len(pUserField19)<=0 or isNull(pUserField19) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField19%>:</td>
                                    <td align="left"><% =rptUserField19 %>&nbsp;</td>
                                </tr>                            
                                <tr <% if len(pUserField20)<=0 or isNull(pUserField20) then response.write("style=""display:none;""") %>>
                                    <td class="printLabel"><%=pUserField20%>:</td>
                                    <td align="left"><% =rptUserField20 %>&nbsp;</td>
                                </tr>

							</table>                                
                        	<% end if %>
                        <% end if %>
					  	<!-- STOP Custom Fields section -->	                                            


						<!-- START Resolution section -->        
                        <% if inStr(selSections,"resolutions") then %>                                                
   						<table id="table:notes:resolution" width="100%" class="printTable" cellpadding="0" cellspacing="0">

      						<tr>
      						  <td colspan="2" class="sectionHeader">Notes: Resolution</td>
   						  	</tr>
                        
                            <tr>
                      			<td class="printLabel" style="border-top:none;">Resolution:</td>
                              	<td align="left" style="border-top:none;"><%=rptResolution%>&nbsp;</td>
                            </tr>

                            <tr>
                      			<td class="printLabel">Release to caller:</td>
                              	<td align="left">                                
                                	<%
									dim approvedByUser
									if rptResAppBy > 0 then
                                  		mySQL = "SELECT FirstName, LastName FROM Logins " _
											  & "WHERE Logid=" & rptResAppBy
                                      	'set rs = openRSexecuteDialog(mySQL)
										set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
										if not rs.eof then
											rptResApp = "True"
											approvedByUser = rs("lastname")
                                            if len(rs("LastName")) > 0 then approvedByUser = rs("LastName")
                                            if len(rs("FirstName")) > 0 and len(approvedByUser) > 0 then approvedByUser = approvedByUser & ", " & rs("FirstName") else approvedByUser = rs("FirstName")
										else
											rptResApp = "False"
											approvedByUser = "<em>waiting approval</em>"
										end if
                                     	call closeRS(rs)	
									else
										rptResApp = "False"
										approvedByUser = "<em>waiting approval</em>"																	
									end if								

                                    if rptResApp = "True" then
                                        response.Write("Yes&nbsp;&nbsp;&nbsp;[Approved by: " & approvedByUser & "]")
                                    else
                                        response.Write("No&nbsp;&nbsp;&nbsp;[Approved by: " & approvedByUser & "]")
                                    end if								
                                    %>           							                      
                                    
                                </td>
                            </tr>

						</table>
                        <% end if %>
						<!-- STOP Resolution section -->      

                        <!-- START END of issue notice -->  
                        <table width="100%" class="printTable" cellpadding="0" cellspacing="0">                    
                            <tr>
                                <td class="sectionHeader">-END-</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom:25px;">&nbsp;</td>
                            </tr>                            
                        </table>
                        <!-- STOP END of issue notice -->                          
            
            <%              
			rsMain.movenext
		
		'Next issue
		loop
		
		call closeRS(rsMain)		
		%>
                                    
	</div>                                               

</form>

</body>
</html>

<%
'close database connection
call closeDB()
%>

