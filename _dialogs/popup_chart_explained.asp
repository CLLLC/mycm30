<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, rsFUP

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************
	
%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Chart Explained</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    

	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/chart.png" title="Issues" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Chart Explained</span>
   	</div>                   


		<table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
			<tr>    
				<td align=left>
            		<img src="../_images/chart_explained.png" width="441" height="324" align="absmiddle">            
	            </td>
    	    </tr>
        </table>        
        

    <table class="formTable" style="width:100%;" border="0" cellspacing="0" cellpadding="0">        
		<tr>
			<td style="border:none; padding:0px; margin:0px;">&nbsp;</td>
      	</tr>            
		<tr>
			<td style="padding:0px; margin:0px;">&nbsp;</td>
      </tr>            
	</table>	

   	<!-- START Buttons -->
    <div> 	       		
    	<a class="myCMbutton" href="#" onClick="this.blur(); SimpleModal.close();" style="float:right;"><span class="cancel">Close</span></a>
    </div>
    <!-- STOP Buttons -->

</body>

</html>

<%
'close database connection
call closeDB()
%>

