<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
Server.ScriptTimeout =25000
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, pageSize

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim countString
dim optConfigure

dim idIssueNew

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 2 then	
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if

'get idIssue (1101-AAA-10001-01)
dim idIssue
idIssue = trim(Request.QueryString("recId"))
if len(idIssue) = 0 then
	idIssue = trim(Request.Form("recId"))
	if idIssue = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")
	end if
end if

'Get issue type
dim issueType : issueType = trim(Request.QueryString("issuetype"))
if len(issueType) = 0 then
	issueType = trim(Request.Form("issueType"))
	if len(issueType) = 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue Type.")	
	end if
end if

'Get action
dim action : action = trim(Request.QueryString("action"))
if len(action) = 0 then
	action = trim(Request.Form("action"))
end if
action = lCase(action)
if action <> "change" and action <> "submit" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if

'check user view status	for customer and issuetype
'based on vwLogins_IssueType and security sets
if action = "change" or action <> "submit" then
	if userViewIssue(idIssue,sLogid) = False then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to view this issue.")
	end if
end if

dim navEditURL : navEditURL = "../scripts/" & session(session("siteID") & "issuesEdit")

'perform UPDATE here
if action = "submit" then



end if

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Change Issue Type</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!--JQuery required components -->
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

	<!-- jQuery Prompt Replacements http://abeautifulsite.net/blog/2008/12/jquery-alert-dialogs/ -->
    <script src="../_jquery/prompt/jquery.alerts.js"></script>
    <link rel="stylesheet" type="text/css" href="../_jquery/prompt/jquery.alerts.css"/>

</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="popup_type_issue.asp" style="padding:0px; margin:0px;">
    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/page_white_lightning.png" title="Issues" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Change Issue Type</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Select an issue type below.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->


	<% if action = "submit" then %>

        <!-- START File Import -->
        <div id="step_1" style="padding-bottom:5px;"><img id="image_1" src="../_images/myloader.gif" align="absmiddle">&nbsp;Preparing to move issue</div>
        <div id="step_2" style="padding-bottom:5px;"><img id="image_2" src="../_images/bullet_yellow.png" align="absmiddle">&nbsp;Moving: <% =idIssue %></div>
        <div style="padding-bottom:15px;">
            <div id="step_3" style="float:left;"><img id="image_3" src="../_images/bullet_yellow.png" align="absmiddle">&nbsp;Target #:</div>
            <div id="step_3_sub"><em>&lt;unknown&gt;</em></div>
        </div>    
        <div id="step_4" style="padding-bottom:5px;">&nbsp;</div>    
    
        <!-- START Buttons -->
        <div style="float:right;"> 	       		
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); navigateNew(document.getElementById('step_3_sub').innerHTML);"><span class="cancel"><u>C</u>lose</span></a>
        </div>    
        <!-- STOP Buttons -->
    
        <%
        'push to screen NOW
        response.flush()
        %>    
    
    
        <!-- STEP 1 stored procedure -- MAKE Location table BACK-UP! -->
        <script language="javascript">
            document.getElementById('image_1').src = '../_images/myloader.gif';
        </script>    
        <%			
        response.flush()	
        'RUN OTHER STUFF HERE...
        %>
        <script language="javascript">
            document.getElementById('image_1').src = '../_images/check.png';
        </script>
        <%		
        response.flush()
        %>
    
    
        <!-- STEP 2 import uploaded file -->
        <script language="javascript">
            document.getElementById('image_2').src = '../_images/myloader.gif';
        </script>    
        <%			
        on error resume next	
        response.flush()
        set rs = openRSexecute(" EXEC sp_MoveIssue '" & idIssue & "', '" & customerID & "', " & issueType & ", " & sLogid)
        if err.number <> 0 then
            %>
            <script language="javascript">
                document.getElementById('image_2').src = '../_images/error.png';
            </script>        
            <%
            response.write("<strong>Error:</strong> " & err.Description)
            response.flush()
            response.end
        end if
        on error goto 0	
        %>
        <script language="javascript">
            document.getElementById('image_2').src = '../_images/check.png';
        </script>
        <%		
        response.flush()
        %>
    
    
        <!-- STEP 3 get new CRSID -->
        <script language="javascript">
            document.getElementById('image_3').src = '../_images/myloader.gif';
        </script>    
        <%			
        response.flush()
        mySQL = "SELECT CRSID FROM CRS WHERE Name='" & idIssue & "' "
        set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,0)	
        if rs.eof then
            %>
            <script language="javascript">
                document.getElementById('image_3').src = '../_images/error.png';
            </script>        
            <%
            response.write("<strong>Error:</strong> " & err.Description)
            response.flush()
            response.end
        else
            idIssueNew = rs("crsid")
        end if
        %>
        <script language="javascript">
            document.getElementById('image_3').src = '../_images/check.png';
            document.getElementById('step_3_sub').innerHTML = '<% =idIssueNew %>';		
        </script>
        <%		
        response.flush()
        %>
    
    
        <!-- STEP 4 Finalize -->
        <script language="javascript">
            document.getElementById('step_4').innerHTML = 'Move <strong>COMPLETE</strong>';
        </script>        
        <%
        response.flush()
        %>    
    
    <% else %>

        <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="formLabel" style="width:100px;">Issue Type:</td>
                <td align=left>
                    <%
                    'find all groups for this customer
                    mySQL = "SELECT Customer_IssueType.CustomerID, IssueType.IssueTypeID, IssueType.Name " _
                    & "	FROM Customer_IssueType INNER JOIN IssueType ON Customer_IssueType.IssueTypeID = IssueType.IssueTypeID " _
                    & "	WHERE Customer_IssueType.CustomerID='" & customerID & "' "
                    set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                    response.write("<select name=""issueType"" id=""issueType"" size=1 class=""inputMediumLong"">")
                    if not rs.eof then
                        do while not rs.eof
                            response.write("<option value=""" & rs("IssueTypeID") & """ " & checkMatch(issueType,rs("IssueTypeID")) & ">" & rs("Name") & "</option>")
                            rs.movenext
                        loop
                    else
                        response.write("<option value=''>-- Select --</option>")
                    end if
                    response.write("</select>")
                    call closeRS(rs)
                    %>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="formLabel">&nbsp;</td>
            </tr>
    
        </table>
    
        <input type=hidden name="cid" id="cid" value="<% =customerID %>">
        <input type=hidden name="recId" id="recId" value="<% =idIssue %>">    
        <input type=hidden name="action" id="action" value="submit">        
            
        <!-- START Buttons -->
        <div style="float:right;"> 	       		
            <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); moveIssueType();"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
        </div>
        <!-- STOP Buttons -->
    
	<% end if %>	
    
</form>
        
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script language="javascript">
	//for adding a single investigation using 
	//the ADD button
	function navigateNew(idIssue) {
		//trigger move
		topWin.parent.navigateAway('<% =navEditURL %>?action=edit&recid='+idIssue)
		//close window
   		SimpleModal.close();		
	}		
</script>


<script type="text/javascript"> 
	function moveIssueType(){
		//make sure user is ok with move
		jConfirm("You have elected to change the type of issue.<br /><br />Continue with change?", "myCM Alert", function(r) {
			if (r==true) {												
				document.frm.submit()
			}
			else {
				return false;
			}			
		})		
	}	
</script>
