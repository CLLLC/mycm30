<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'Database
dim mySQL, cn, rs, count

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim pageView : pageView = Request.QueryString("pageView")

'Get action type 
dim action : action = Request.Form("action")
if len(action) = 0 then
	action = Request.QueryString("action")
end if

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Choose a Location</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- HTML Table Filter Generator http://tablefilter.free.fr/ -->
    <link rel="stylesheet" type="text/css" href="../_tablefilter/filtergrid.css"/>
	<script type="text/javascript" src="../_tablefilter/tablefilter_all.js"></script>
	<script type="text/javascript" src="../_tablefilter/sortabletable.js"></script>
    <script type="text/javascript" src="../_tablefilter/tfAdapter.sortabletable.js"></script>        

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/globe.png" title="Locations" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Locations</span>
   	</div>                   

	<%
	if action = "search" then
		dim locSearch	
		dim locName : locName = Request.Form("locName")
		dim locAddress : locAddress = Request.Form("locAddress")		
		dim locCity : locCity = Request.Form("locCity")		
		dim locState : locState = Request.Form("locState")
		dim locZip : locZip = Request.Form("locZip")
		dim locCountry : locCountry = Request.Form("locCountry")		
		
		if len(locName) > 0 then locSearch = locSearch & "Location.Name Like '%" & locName & "%' AND "
		if len(locAddress) > 0 then locSearch = locSearch & "Address Like '%" & locAddress & "%' AND "
		if len(locCity) > 0 then locSearch = locSearch & "City Like '%" & locCity & "%' AND "
		if len(locState) > 0 then locSearch = locSearch & "State Like '%" & locState & "%' AND "
		if len(locZip) > 0 then locSearch = locSearch & "PostalCode Like '%" & locZip & "%' AND "
		if len(locCountry) > 0 then locSearch = locSearch & "Country Like '%" & locCountry & "%' AND "
		'clean up
		if right(locSearch,5) = " AND " then locSearch = left(locSearch,len(locSearch)-5)
		
		'check how many records are returned
		mySQL = "SELECT count(LocationID) AS [count] " _			 
			  & "	FROM   Location " _
			  & "	WHERE CustomerID = '" & customerID & "' "		
		if len(locSearch) > 0 then
			mySQL = mySQL & " AND " & locSearch		
		end if

	else
		'check how many records are returned
		mySQL = "SELECT count(LocationID) AS [count] " _			 
			  & "	FROM   Location " _
			  & "	WHERE CustomerID = '" & customerID & "' "	
	end if

	'open recordset
	set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)		
	count = rs("count")
	call closeRS(rs)
	%>

	<%
	'there are more then 1,000 locations, STOP and rethink...
	if count>1000 then	
		%>		
		<script language="javascript">
			//close window
			SimpleModal.close();				
			//open login dialog
			SimpleModal.open('../_dialogs/popup_location_search.asp?cid=<% =customerID %>&locname=<% =locname %>&locaddress=<% =locaddress %>&loccity=<% =loccity %>&locstate=<% =locstate %>&loczip=<% =loczip %>&loccountry=<% =loccountry %>&pageview=loctable&count=<% =count %>', 525, 700, 'no');
		</script>    
		<%				
	else
	%>

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Select a location by clicking within the 'Name' column.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Locations table -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
    
        <%
'		mySQL = "SELECT Location.Name, Location.Address, Location.City, Location.State, Location.PostalCode AS [Zip], Location.Country, " _
'			  & "		Location.City + ' ' + Location.State + ', ' + Location.PostalCode AS [City/State], " _
'			  & "		Location.Level_1, Location.Level_2, Location.Level_3, Location.Level_4, " _
'			  & "		Location.Level_5, Location.Level_6, Location.Level_7, Location.Level_8, " _
'			  & "		Location.Level_1 AS [Hierarchy], Directive.Notes, Location.LocationID " _
'			  & "			FROM Location LEFT OUTER JOIN " _
'			  & "				Directive ON Location.CustomerID = Directive.CustomerID AND Location.DirectiveID = Directive.DirectiveID " _
'			  & "			WHERE (Location.CustomerID = '" & customerID & "') "

		mySQL = "SELECT Location.Name, Location.Address, Location.City, Location.State, Location.PostalCode AS [Zip], Location.Country, " _
			  & "	LTRIM(CASE WHEN COALESCE(Location.City,'') = '' THEN '' ELSE Location.City END " _
			  & "	+ CASE WHEN COALESCE(Location.State,'') = '' THEN '' ELSE ' ' + Location.State + ' ' END " _
			  & "	+ CASE WHEN COALESCE(Location.PostalCode,'') = '' THEN '' ELSE ', ' + Location.PostalCode END) AS [City/State], " _
			  & "	Location.Level_1, Location.Level_2, Location.Level_3, Location.Level_4, " _
			  & "	Location.Level_5, Location.Level_6, Location.Level_7, Location.Level_8, " _
			  & "	Location.Level_1 AS [Hierarchy], Directive.Notes, Location.LocationID " _			  
			  & "		FROM Location LEFT OUTER JOIN " _
			  & "			Directive ON Location.CustomerID = Directive.CustomerID AND Location.DirectiveID = Directive.DirectiveID " _
			  & "	WHERE (Location.CustomerID = '" & customerID & "') "

		'limit result set
		if action = "search" then			  
			if len(locSearch) > 0 then
				mySQL = mySQL & " AND " & locSearch		
			end if							  
		end if
		mySQL = mySQL & " ORDER BY Location.Name "
		%>
        
        <tr>
        	<td>
			<%
			dim tableProperty
			'set _tablefilter/default.asp settings.
            tableProperty = "sort: true, sort_config:{sort_types:['String','String','String']}, filters_row_index: 1, " _
                          & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
                          & "paging: true, paging_length: 5, " _
						  & "highlight_keywords: true, " _
                          & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", " _
                          & "or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
                          & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"
						
			'make call to build grid found in ../_tablefilter/default.asp
			call buildGrid("locationTableID",mySQL,"","location-table","../_images/icons/16/magnifier.png",tableProperty)
        	%>
        	</td>
        </tr>
    </table>
	<!-- STOP Locations table -->

	<!-- START Cancel Button -->
	<div style="padding-top:5px; margin-bottom:0px;">
		<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>
    </div>
	<!-- STOP Cancel Button -->

	<%
	end if
	%>    
     
    
</body>
</html>

<%
'close database connection
call closeDB()
%>
