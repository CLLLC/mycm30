<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
Server.ScriptTimeout =25000
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, rsRes, pageSize
dim fieldsSQL, valueSQL

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim countString
dim optConfigure

dim purgeID
dim purgeCount, purgeTotal
dim purgeDate : purgeDate = Now()
dim purgeFrom
dim purgeBar

dim cusRetention

dim resTotal, resCount, resOwner
dim resID
dim resFirst
dim resLast
dim resEmail

dim includeOwner

dim copyUser
dim tempUser

dim errorText

dim userFirstName, userLastName, userEmail, userName, userPassword
					
dim modifiedDate : modifiedDate = Now()

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 3 and programAdmin <> "Y" then	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")		
end if


'Get action type 
dim action : action = Request.Form("action")
if len(action) = 0 then
	action = Request.QueryString("action")
end if
if lCase(action) <> "copy" and lCase(action) <> "save" then   	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Action Type." & action)	
end if


'Form validation
if lCase(action) = "save" then

	copyUser = Request.Form("copyUser")
	if len(copyUser) = 0 then   	
		errorText = errorText & "<li>Invalid User to Copy</li>"
	end if

	'user first name
	userFirstName = trim(Request.Form("userFirstName"))
	if len(userFirstName) <= 0 then
		errorText = errorText & "<li>Invalid First Name</li>"
	end if
		
	'user last name
	userLastName = trim(Request.Form("userLastName"))
	if len(userLastName) <= 0 then
		errorText = errorText & "<li>Invalid Last Name</li>"
	end if

	'user email
	userEmail = trim(Request.Form("userEmail"))
	if len(userEmail) <= 0 then
		errorText = errorText & "<li>Invalid Email</li>"			
	end if
		
	'user name
	userName = trim(Request.Form("userName"))
	if len(userName) <= 0 then
		errorText = errorText & "<li>Invalid User Name</li>"						
	end if

	'user name
	userPassword = trim(Request.Form("userPassword1"))
	if len(userPassword) <= 0 then
		errorText = errorText & "<li>Invalid Password</li>"						
	end if
	
	'reset action to Copy with errors	
	if len(errorText)>0 then
		action = "copy"
	end if
	
end if


'Get user count and password strenght
if lCase(action) = "copy" then

	'find password strength
	dim passwordStrength	
	mySQL="SELECT Customer.appPasswordStrength " _
		& "		FROM Customer " _
		& "		WHERE CustomerID='" & customerID & "' "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	
	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	else
		passwordStrength = rs("appPasswordStrength")
	end if
	call closeRS(rs)

	'get allowed user count from customer record
	dim appNumUsers
	mySQL="SELECT Customer.appNumUsers " _
		& "		FROM Customer " _
		& "		WHERE CustomerID='" & customerID & "' "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if not rs.eof then
		appNumUsers = rs("appNumUsers")
	end if
	if len(appNumUsers) <= 0 or isNull(appNumUsers) or appNumUsers = "" then
		appNumUsers = "0"
	end if

	'get current user count incase they want to add more
	dim appUserCount
	mySQL="SELECT Count(LogID) AS [UserCount]" _
		& "		FROM Logins " _
		& "		WHERE CustomerID='" & customerID & "' AND Active='Y' AND SecurityLevel>10 "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if not rs.eof then
		appUserCount = rs("UserCount")
	end if
	if len(appUserCount) <= 0 or isNull(appUserCount) or appUserCount = "" then
		appUserCount = "0"
	end if

end if


%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Copy User</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>
      
	<!-- HTML List/Table resorting functions (right now own) -->
	<!-- Example usage:
    		var table = document.getElementById('table-1');
			var tableDnD = new TableDnD();
			tableDnD.init(table);  -->
	<script type="text/javascript" src="../scripts/javascript/orderList.js"></script>

	<!--JQuery required components -->
    <!-- ONLY FOR suggest boxes and JSON data pulls, includes full JQuery library -->
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />
    <style>
        .ui-autocomplete-loading { background: white url('../_jquery/suggestbox/ui-anim_basic_16x16.gif') right center no-repeat; }
    </style>
    <style>
        .ui-autocomplete {
            max-height: 100px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
            /* add padding to account for vertical scrollbar */
            padding-right: 20px;
        }
        /* IE 6 doesn't support max-height
         * we use height instead, but this forces the menu to always be this tall
         */
        * html .ui-autocomplete {height: 100px;}
    </style>
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    <!-- ----------------------------------------- -->
    
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

	<!-- jQuery Prompt Replacements http://abeautifulsite.net/blog/2008/12/jquery-alert-dialogs/ -->
    <script src="../_jquery/prompt/jquery.alerts.js"></script>
    <link rel="stylesheet" type="text/css" href="../_jquery/prompt/jquery.alerts.css"/>
    
    <script language="JavaScript" src="../_jquery/password/jquery.simplePassMeter-0.4.js"></script>    
    <link rel="stylesheet" type="text/css" href="../_jquery/password/simplePassMeter.css"/>    
    
</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="popup_user_copy.asp" style="padding:0px; margin:0px;">
    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/drive_user.png" title="Retention" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Copy User</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Copy existing user using form below. Only 'Active' users may be copied.
                        </div>
                        
                        <% 	if len(errorText)>0 then %>
                            <div style="clear:both; padding-left: 15px;">
                                The following errors were found<br/>
                                <ul>
                                	<% response.Write(errorText) %>
                                </ul>
                            </div>                            
                        <% end if%>
                        
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<%
	'*************************************************************
    'COPY user to new user
	'*************************************************************	
    if lCase(action) = "save" then
	%>

		<!-- START File Import -->
		<div id="step_1" style="padding-bottom:5px;"><img id="image_1" src="../_images/myloader.gif" align="absmiddle">&nbsp;Copying user</div>
		<div id="step_3" style="padding-bottom:5px;">&nbsp;</div>    
		<div id="step_2" style="padding-bottom:5px;">&nbsp;</div>    
                
		<div id="step_6" style="padding-top:5px;">&nbsp;</div> 
		<div style="border-bottom:1px dotted #cccccc;">&nbsp;</div>
        
        <!-- START Buttons -->
        <div style="float:right; padding-top:13px;"> 	       		
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
        </div>    
        <!-- STOP Buttons -->
    
        <%
        'push to screen NOW
        response.flush()
        %>    
        
        <!-- STEP 1 stored procedure -->
        <script language="javascript">
            document.getElementById('image_1').src = '../_images/myloader.gif';
        </script>    
        <%			
        response.flush()	
		set rs = openRSexecute(" EXEC proc_CopyUser " & copyUser & ",'" & userFirstName & "','" & userLastName & "','" & userEmail & "','" & userPassword & "','" & userName & "' " )        
		%>
        <script language="javascript">
            document.getElementById('image_1').src = '../_images/check.png';
        </script>
        <%		
        response.flush()
        %>

        <!-- STEP 6 Finalize -->
        <script language="javascript">
            document.getElementById('step_6').innerHTML = 'Copy <strong>COMPLETE</strong><br><br>Please wait. Reload users to edit new user...';
        </script>        
        
        <%
        response.flush()
		'delay/wait command - EXCELLENT!
		'set rs = openRSexecute(" WAITFOR DELAY '000:00:02' SELECT '02 Second Delay' ")				

	'show form
    else
		
        %>

        <!-- START Copy Users table -->
		<% if (cLng(appUserCount)+1)<=cLng(appNumUsers) then %>        
            <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">        
                <tr>
                    <td class="formLabel" style="border-top: none; padding-top:0px; margin-top:0px;">Copy User:</td>
                    <td align=left style="border-top: none; padding-top:0px; margin-top:0px;">
                        <%
                        'Find Categories available TO BE assigned
                        mySQL = "SELECT LogID, FirstName, LastName, Active FROM Logins WHERE CustomerID = '" & customerID & "' AND Active='Y' Order By LastName, FirstName "					
                        set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                        'build combo box
                        response.write("<select name=""copyUser"" id=""copyUser"" size=1 class=""inputLong"">")
                        response.write("<option value="""">-- Select --</option>")
                        do until rs.eof			
    
                            tempUser = ""
                            if len(rs("LastName")) > 0 then tempUser = rs("LastName")
                            if len(rs("FirstName")) > 0 and len(tempUser) > 0 then tempUser = tempUser & ", " & rs("FirstName") else tempUser = rs("FirstName")
                            'show users status and setting on this issue
                            if rs("Active") = "C" then
                                tempUser = tempUser & " (contact)"
                            end if																		
                            response.Write "<option value=""" & rs("LogID") & """>" & tempUser & "</option>"																		
    
                            rs.movenext
                        loop
                        response.write("</select>")
                        call closeRS(rs)
                        %>                
                    </td>
                </tr>
            </table>
		<% end if %>
        <!-- STOP Copy Users table -->
        
        	
        <!-- START Users table -->
		<% if (cLng(appUserCount)+1)<=cLng(appNumUsers) then %>        
            <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">
                                   
                <tr>
                    <td class="formLabel"><span class="required">*</span>New User:</td>
                    <td align=left nowrap style="padding-bottom:20px;">                              
                        <div style="float:left;">
                            <div class="subLabel">First Name:</div>
                                <div><input name="userFirstName" id="userFirstName" class="inputMedium" value="<% =userFirstName %>" maxlength="20" /></div>
                            </div>                                
                            <div style="float:left;">
                                <div class="subLabel">Last Name:</div>
                            <div>
                            <div style="float:left;">
                                <input name="userLastName" id="userLastName" class="inputMedium" value="<% =userLastName %>" maxlength="20" />
                            </div>
                            </div>
                        </div>
                    </td>
                </tr>     
                
                <tr>
                    <td class="formLabel"><span class="required">*</span>E-Mail:</td>
                    <td align=left nowrap><input name="userEmail" id="userEmail" class="inputLong" type=text value="<% =userEmail %>" size=50 maxlength=50></td>
                </tr>
    
                <tr>
                    <td class="formLabel"><span class="required">*</span>Username:</td>
                    <td align=left nowrap >
                        <div class="subLabel">
                            <ul style="padding-left:15px; margin-left:0px; margin-bottom:5px;">
                                <li style="color:#666;">Must be at least 6 characters and no more than 25 characters.</li>
                                <li style="color:#666;">Cannot contain 'admin' or 'administrator'.</li>
                            </ul>
                        </div>
                        <div style="float:left;">
                            <input name="userName" id="userName" class="inputMedium" type=text value="<% =userName %>" size=20 maxlength=100>
                        </div>
                    </td>
                </tr>
                
                <tr>
                    <td class="formLabel"><span class="required">*</span>Password:</td>
                    <td align=left>
                                            
                                                <table>
                                                    <tr>
                                                        <td style="border:none; padding:0px; margin:0px;">
                                                    <div><input name="userPassword1" id="userPassword1" class="inputMedium" type="password" value="" size=20 maxlength=20></div>
                                                    <div class="subLabel" style="padding-top:5px; padding-bottom:5px;">Confirm by entering again:</div>
                                                    <div><input name="userPassword2" id="userPassword2" class="inputMedium" type="password" value="" size=20 maxlength=20></div>
                                                        </td>
                                                        <td style="border:none; padding:0px; margin:0px;">
                                                            <div id='passMeter'></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                    
                                                <!-- STRONG password -->
                                                <% if lCase(passwordStrength) = "strong" then %>
                                                    <script>
                                                        $('#userPassword1').simplePassMeter({
                                                            'requirements': {
                                                                'matchField': {'value': '#userPassword2'},
                                                                'minLength': {'value': 8},  // at least 8 characters
                                                                'lower': {'value': true},   // at least 1 lower-case letter
                                                                'upper': {'value': true},   // at least 1 upper-case letter
                                                                'special': {'value': true} 	// at least 1 special character
                                                            },
                                                            'offset': 10,
                                                            'container': '#passMeter'
                                                        });
                                                        $('#userPassword1').bind('score.simplePassMeter', function(jQEvent, score) {$('#passwordScore').text(score);});
                                                    </script>
            
                                                <!-- MEDIUM password -->                                        
                                                <% elseif lCase(passwordStrength) = "medium" then %>                                    
                                                    <script>
                                                        $('#userPassword1').simplePassMeter({
                                                            'requirements': {
                                                                'matchField': {'value': '#userPassword2'},
                                                                'minLength': {'value': 6},  // at least 6 characters
                                                                'lower': {'value': true},   // at least 1 lower-case letter
                                                                'upper': {'value': true},   // at least 1 upper-case letter
                                                                'special': {'value': false} // NOT least 1 special character
                                                            },
                                                            'offset': 10,
                                                            'container': '#passMeter'
                                                        });
                                                        $('#userPassword1').bind('score.simplePassMeter', function(jQEvent, score) {$('#passwordScore').text(score);});
                                                    </script>                                    
            
                                                <!-- WEAK password -->                                        
                                                <% else %>
                                                    <script>
                                                        $('#userPassword1').simplePassMeter({
                                                            'requirements': {
                                                                'matchField': {'value': '#userPassword2'}
                                                            },
                                                            'offset': 10,
                                                            'container': '#passMeter'												
                                                        });
                                                        $('#userPassword1').bind('score.simplePassMeter', function(jQEvent, score) {$('#passwordScore').text(score);});
                                                    </script>                                    
                                                <% end if %>                                    
            
                                                <span id='passwordScore' style="display:none;"></span>
            
                    </td>
                </tr>
    
                <tr>
                    <td colspan="2" align=left nowrap style="padding:10px; margin:10px;">&nbsp;</td>
                </tr>
                
            </table>
  		<% end if %>                                    
        <!-- STOP User table -->
    
    
		<!-- NO MORE USERS allow -->
		<% if (cLng(appUserCount)+1)>cLng(appNumUsers) then %>
			<table id="noNewUsersTable" width="100%" cellpadding="0" cellspacing="0" style="margin:10px 0;">
         		<tr>
             		<td align="left" class="clearFormat" style="padding-right:2px;">                                                                                                
                    	<% call systemMessageBox("maxedoutTableID","statusMessageALERT","You have exceeded the number of user accounts allowed on myCM.") %>
                  	</td>
                 </tr>
        	</table>
  		<% end if %>                                    
 		<!--STOP account active y/n/c table -->	                                               

    
        <input name="cid" type="hidden" value="<% =customerid %>">
        <input name="action" type="hidden" value="save">
            
        <!-- START Buttons -->
        <div style="float:right;"> 	       		
			<% if (cLng(appUserCount)+1)<=cLng(appNumUsers) then %>        
    	        <a class="myCMbutton" href="#" accesskey="E" onClick="this.blur(); if(validateForm()==true){confirmPurge()}; return false;"><span class="save" style="padding-right:7px;"><u>S</u>ave</span></a>
        	<% end if %>
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
        </div>
        <!-- STOP Buttons -->
    
		<%	
    end if
    %>

</form>
        
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script>
	function validateForm() {		
		var formPassed = true;	//retunred value
		var reqFields = "action,resUser";
		var temp = reqFields.split(",");
		var i;
		var el;		
		
		for(i=0; i<temp.length; i++) {			
			if (document.getElementById(temp[i])) {			
				el = document.getElementById(temp[i])
				if (!el.value) {			
					el.style.background = '#ffff9f'; //set to yellow			
					jAlert('<strong>Form Error!</strong><br/><br/>Please ensure all required fields have been entered.', 'myCM Alert'); //inform user of error
					formPassed = false; //return error
				}
				else {				
					if (el.style.background == '#ffff9f') {
						//alert('2:' + temp[i]);
						el.style.background = '#fff';	//set back to white
					}
				}		
			}
		}
		
		return formPassed;
		
	}	
	
	function confirmPurge(action) {		
		jConfirm('Copy user now?<br/><br/><strong>This action CANNOT be undone</strong>.<br/><br/>', 'Confirm Copy', function(r) {			
			if (r==true) {				
				document.frm.submit();
			}
			else {
				return false;
			}
		});					
	}
</script>

