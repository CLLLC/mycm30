<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs

'Work Fields
dim I
dim item
dim count
dim pageSize
dim totalPages
dim showArr
dim sortField
dim sAddress

dim curPage
dim showPhrase
dim showField
dim showStart


'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim pageView : pageView = Request.QueryString("pageView")

'get issue # -- optional since could be called from Add Issue
dim idIssue : idIssue = trim(Request.QueryString("recId"))

'get issue type -- mandatory
dim issueTypeID : issueTypeID = trim(Request.QueryString("tid"))
if issueTypeID = "" then	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Type ID.")
end if

'Get showPhrase
showPhrase = Request.Form("showPhrase")	
if len(showPhrase) = 0 then
	showPhrase = Request.QueryString("showPhrase")
end if

'get setting to show or not show sub-categories to RS
dim appPopUpSubCategoryRS
mySQL = "SELECT appPopUpSubCategoryRS "_ 
	  & "FROM   Customer " _
	  & "WHERE  CustomerID = '" & customerID & "' "
set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)	
appPopUpSubCategoryRS = rs("appPopUpSubCategoryRS")		

%>

<html>
<head>
<title>Choose a Category</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24//move_to_folder.png" title="Categories" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Categories</span>
   	</div>                   

	<!-- START Search box -->
	<table class="formTable" style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
          <form method="post" action="popup_category.asp?recid=<% =idIssue %>&cid=<% =customerID %>&tid=<% =issueTypeID %>" name="frm">
			<td>
                <fieldset style="border:1px solid #CCCCCC; background-color:#FFFFFF; ">
                    <legend>Search</legend>
                        <div align="left" style="padding:5px;">
                            <img src="../_images/icons/16/search.png" title="Search" width="16" height="16" align="absmiddle" style="margin-right:3px;">
                            <input type=text name="showPhrase" id="showPhrase" style="width:225px;" size=20 maxlength=50 value="<%=showPhrase%>">                            
							<input type="submit" name="btnSearch" id="btnSearch" value="Search" />
							<input type="button" name="btnClear" id="btnClear" value="Clear" onClick="this.form.showPhrase.value = ''; this.form.submit();" />                            
                        </div>            
                </fieldset>
			</td>
          </form>
        </tr>
    </table>	
	<!-- STOP Search box -->

	<!-- START Categories table -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    
        <%
		'limit to just primary with no sub for RS/RA
		if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
		
'			'Find Categories available TO BE assigned and NOT already assigned
'			mySQL="SELECT a.CustomerID, a.CategoryID, a.Name, a.Sub, a.DirectiveID " _
'				& "FROM Category AS a INNER JOIN Category_IssueType AS c ON a.CategoryID = c.CategoryID " _				
'				& "WHERE  CustomerID = '" & customerID & "' AND Active='Y' AND (Sub='' OR Sub is null) AND " _
'				& " 		NOT EXISTS " _
'				& "      	(SELECT b.CategoryID " _
'				& "       	 FROM   CRS_Category b " _
'				& "       	 WHERE  a.CategoryID=b.CategoryID " _
'				& "		  	 AND    b.CRSID = '" & idIssue & "' ) " _
'				& "     AND c.IssueTypeID=" & issueTypeID & " #ADD_QUERY# " _
'				& "ORDER BY a.Name, a.Sub "		

			mySQL = "SELECT a.CustomerID, a.CategoryID, a.Name, a.Sub, a.DirectiveID " _
					& "FROM Category AS a INNER JOIN Category_IssueType AS c ON a.CategoryID = c.CategoryID " _				
					& "WHERE  CustomerID = '" & customerID & "' AND Active='Y' AND LanguageCD='en-US' "

			if appPopUpSubCategoryRS = "N" or appPopUpSubCategoryRS = "" or isNull(appPopUpSubCategoryRS) then
				mySQL = mySQL & " AND (Sub='' OR Sub is null) AND "
			else
				mySQL = mySQL & " AND "
			end if
			
			mySQL = mySQL & "" _
					& " 		NOT EXISTS " _
					& "      	(SELECT b.CategoryID " _
					& "       	 FROM   CRS_Category b " _
					& "       	 WHERE  a.CategoryID=b.CategoryID " _
					& "		  	 AND    b.CRSID = '" & idIssue & "' ) " _
					& "     AND c.IssueTypeID=" & issueTypeID & " #ADD_QUERY# " _
					& "ORDER BY a.Name, a.Sub "			
							
		'customer gets full list
		else
			'Find Categories available TO BE assigned and NOT already assigned
			mySQL="SELECT a.CustomerID, a.CategoryID, a.Name, a.Sub, a.DirectiveID " _
				& "FROM Category AS a INNER JOIN Category_IssueType AS c ON a.CategoryID = c.CategoryID " _
				& "WHERE  CustomerID = '" & customerID & "' AND Active='Y' AND LanguageCD='en-US' AND " _
				& " 		NOT EXISTS " _
				& "      	(SELECT b.CategoryID " _
				& "       	 FROM   CRS_Category b " _
				& "       	 WHERE  a.CategoryID=b.CategoryID " _
				& "		  	 AND    b.CRSID = '" & idIssue & "' ) " _
				& "     AND c.IssueTypeID=" & issueTypeID & " #ADD_QUERY# " _
				& "ORDER BY a.Name, a.Sub "
		end if		
		
        'Search Phrase
        if len(showPhrase) > 0 then
			mySQL = replace(mySQL,"#ADD_QUERY#", " AND ((a.Name LIKE '%" & replace(showPhrase,"'","''") & "%') OR (a.Sub LIKE '%" & replace(showPhrase,"'","''") & "%'))  ") 
		else
			mySQL = replace(mySQL,"#ADD_QUERY#", "")
		end if							

        'Open recordset
        set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
        if rs.eof then
        %>
            <tr>
                <td align=center valign=middle>
                <select name="listCategory" id="listCategory" style="width:100%; height:200px; margin:0px; margin-bottom:5px;" disabled="disabled" multiple="multiple" >
                <option value="">No Categories Found.</option></select>
                </td>
            </tr>
        <%
        else
        %>	
        
            <tr>
                <td>                    
					<% 
					if pageView = "issue" or pageView = "issue:00" then
						response.write("<select name=""listCategory"" id=""listCategory"" ondblclick=""setCategoryList('listCategory');"" style=""width:100%; height:200px; margin:0px; margin-bottom:5px;"" multiple=""multiple"" >")
                    else
                    	response.write("<img src=""../_images/icons/16/help.png"" width=""16"" height=""16"" align=""absmiddle"" style=""margin-right:5px; margin-bottom:2px;""><span style=""color:#2d73b2;"">Double-click category to view details.</span>")
                        response.write("<br/>")
	                    response.write("<select name=""listCategory"" id=""listCategory"" ondblclick=""categoryLookUp('listCategory');"" style=""width:100%; height:200px; margin:0px; margin-bottom:5px;"" multiple=""multiple"" >")
                    end if
					
					dim tempCat
                    do while not rs.eof
						'show sub if exists
						if len(rs("Sub")) > 0 then
							tempCat = rs("Name") & " [" & rs("Sub") & "]"
						else
							tempCat = rs("Name")
						end if						
						'show directive attached if necessary
						if cLng(session(session("siteID") & "adminLoggedOn")) < 10 and rs("DirectiveID") > 0 then
							tempCat = tempCat & " (*directive)"
						end if						
						'add category to list
                        response.Write("<option value=""" & rs("categoryID") & """>" & tempCat & "</option>")            
                        rs.movenext
                    loop
                    %>
                    </select>
                </td>
            </tr>
        
        <%
        end if
        %>
        
    </table>
	<!-- STOP Categories table -->

	<!-- START Buttons -->
	<div style="float:left;">
		Select: <a href="javascript:selectAllOptions('listCategory',true);">All</a> | <a href="javascript:selectAllOptions('listCategory',false);">None</a>
	</div>            
	<div style="float:right;"> 	       		
        <a class="myCMbutton" href="#" accesskey="A" onClick="this.blur(); javascript:setCategoryList('listCategory');"><span class="add" style="padding-right:10px;"><u>A</u>dd</span></a>
    	<% if pageView = "issue" or pageView = "issue:00" then %>
			<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); topWin.parent.StartTheTimer(); return false;"><span class="cancel"><u>C</u>lose</span></a>
		<% else %>
			<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); return false;"><span class="cancel"><u>C</u>lose</span></a>
		<% end if %>        
	</div>
	<!-- STOP Buttons -->
     
</body>
</html>

<%
'close database connection
call closeDB()
%>

<% if pageView = "issue" or pageView = "issue:00" then %>
<script language="javascript">	
	// dependent on SimpleModal.js
	// note: topWin.parent is defined in SimpleModal.js and replaces "window.opener"
	function setCategoryList(obj) {
		//add selected options to parent listCategory
		var el = document.getElementById(obj);
		//make sure something was selected
		if (el!=null && el.options!=null) {			
			//cycle through all selected
			for(var i=0; el.options.length >i; i++) {
				if (el.options[i].selected == true) {					
					topWin.parent.setCategoryList(el.options[i].text,el.options[i].value);						
				}
			}
		}		

		//restart autosave timer
		topWin.parent.StartTheTimer();

		//close window
		SimpleModal.close();
	}		
</script>
<% else %>
<script language="javascript">	
	// dependent on SimpleModal.js
	// note: topWin.parent is defined in SimpleModal.js and replaces "window.opener"
	function setCategoryList(obj) {
		//add selected options to parent listCategory
		var el = document.getElementById(obj);
		//make sure something was selected
		if (el!=null && el.options!=null) {			
			//cycle through all selected
			for(var i=0; el.options.length >i; i++) {
				if (el.options[i].selected == true) {					
					topWin.parent.setCategoryList(el.options[i].text,el.options[i].value);						
				}
			}
		}		

		//close window
		SimpleModal.close();
	}		
</script>
<% end if %>

<script language="javascript">	
	function categoryLookUp(obj) {
		//used by child window to add selected Categories
		var i, x;
		var el = document.getElementById(obj);		
		var selIndex = el.selectedIndex;
		if (selIndex != -1) {

			x=0;
			//cycle through all categories to get selected count
			for(i=el.length-1; i>=0; i--) {
			  if(el.options[i].selected) {					  
				x=x+1;
			  }
			}
			//more that one 1 was choosen so STOP
			if(x>1) {
			  jAlert('More than one category was selected.', 'myCM Alert');
			  return false;
			}
			
			//all is good...process selection of primary			
			for(i=el.length-1; i>=0; i--) {
			  //found selected category
			  if(el.options[i].selected) {
				  
				  //alert(el.options[i].value);
				  SimpleModal.open('../_dialogs/popup_category_record.asp?recid=' + el.options[i].value + '&pageview=<% =pageView %>', 410, 450, 'no');
			  }	
			}				
			
		//no category selected
		}
		else {
			jAlert('No category was selected.', 'myCM Alert');			
		}
	}
</script>

