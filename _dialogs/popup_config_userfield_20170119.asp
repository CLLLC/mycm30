<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, pageSize
dim fieldsSQL, valueSQL

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim countString
dim optConfigure

dim modifiedDate : modifiedDate = Now()

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and programAdmin = "N" then	
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if

'Get action type 
dim action : action = Request.Form("action")
if len(action) = 0 then
	action = Request.QueryString("action")
end if
if lCase(action) <> "edit" and _
   lCase(action) <> "add" and _
   lCase(action) <> "del" then   	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Action Type.")	
end if

'Get record ID if editing or deleting
if lCase(action) = "edit" or lCase(action) = "del" then
	dim recId : recId = Request.Form("recId")
	if len(recId) = 0 then
		recId = Request.QueryString("recid")
	end if
	if len(recId) = 0 or isNull(recId) or recId = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid User Field ID.")	
	end if
end if

'Get type of custom field
dim fieldType : fieldType = Request.Form("type")
if len(fieldType) = 0 then
	fieldType = Request.QueryString("type")
end if
if len(fieldType) = 0 or isNull(fieldType) or fieldType = "" then		
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Field Type.")	
end if

'Get order of custom field
dim fieldOrder : fieldOrder = Request.Form("order")
if len(fieldOrder) = 0 then
	fieldOrder = Request.QueryString("order")
end if


'form variables
dim optName
dim optValue
dim optType
dim optOrder
dim optGroup
dim optDefault

dim expI

'set sort order on save
if request.form("save") = "save" then

	'get new user field values
	optName = request.form("optName")
	optValue = request.form("optValue")
	optType = request.form("optType")
	optOrder = request.form("optOrder")

	optGroup = trim(Request.Form("list_checkBox"))
	optGroup = replace(optGroup," ","")
	optGroup = "*|*" & replace(optGroup,",","*|*") & "*|*"

	optDefault = request.form("optDefault")
		
	'save existing user field
	if action = "edit" then
	
		mySQL = "UPDATE Customer Set " _
				& "p" & fieldType & "Field" & recId & "='"			& replace(optName,"'","''") 	& "'," _
				& "p" & fieldType & "Field" & recId & "Value='" 	& replace(optValue,"'","''")	& "'," _
				& "p" & fieldType & "Field" & recId & "Type='" 		& optType 						& "'," _
				& "p" & fieldType & "Field" & recId & "Order='" 	& optOrder 						& "'," _
				& "p" & fieldType & "Field" & recId & "Group='" 	& optGroup 						& "'," _
				& "p" & fieldType & "Field" & recId & "Default='"	& optDefault 					& "'," _
			    & "ModifiedBy="  	& sLogid														& ", " _
			    & "ModifiedDate='"	& modifiedDate													& "' "
		'finalize update query
		mySQL = mySQL & "WHERE CustomerID='" & customerID & "' "  
		'execute
		set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

		session(session("siteID") & "okMsg") = "User Field has been saved successfully."
	
	'add new user field -- have NOT added the adding of new INV Fields yet
	elseif action = "add" then
	
		recId = 0 'reset recordID
		
		'discover the next available user field
       	for expI = 1 to 20	
			mySQL = "SELECT pUserField" & expI & " " _
				  & "	FROM Customer " _
				  & "	WHERE Customer.CustomerID='" & customerID & "' "
			set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
			if len(rs("pUserField" & expI)) <= 0 or rs("pUserField" & expI) = "" or isNull(rs("pUserField" & expI)) then
				recId = expI	'assign new user field
				expI = 21		'exit for
			end if
		next
		
		if recId = 0 then			
			response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("No more user fields available.")
			response.end
		end if
		
		mySQL = "UPDATE Customer Set " _
				& "pUserField" & recId & "='"			& replace(optName,"'","''") 	& "'," _
				& "pUserField" & recId & "Value='" 		& replace(optValue,"'","''")	& "'," _
				& "pUserField" & recId & "Type='" 		& optType 						& "'," _
				& "pUserField" & recId & "Order='" 		& optOrder 						& "'," _
				& "pUserField" & recId & "Group='" 		& optGroup 						& "'," _
				& "pUserField" & recId & "Default='"	& optDefault					& "'," _				
			    & "ModifiedBy="  	& sLogid											& ", " _
			    & "ModifiedDate='"	& modifiedDate										& "' "
		'finalize update query
		mySQL = mySQL & "WHERE CustomerID='" & customerID & "' "  
		'execute
		set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

		session(session("siteID") & "okMsg") = "User Field was added."

	end if		
	%>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
	
	<script type='text/javascript'>
		topWin.parent.pageReload('<% =lCase(customerID) %>');
		SimpleModal.close();
    </script>	
	
<%
elseif action = "del" then

	mySQL = "UPDATE Customer Set " _
			& "p" & fieldType & "Field" & recId & "="		& "Null" 	& ", " _
			& "p" & fieldType & "Field" & recId & "Value=" 	& "Null"	& ", " _
			& "p" & fieldType & "Field" & recId & "Type=" 	& "Null" 	& ", " _
			& "p" & fieldType & "Field" & recId & "Order=" 	& "Null" 	& ", " _
			& "p" & fieldType & "Field" & recId & "Group=" 	& "Null" 	& ", " _
		    & "ModifiedBy="  	& sLogid								& ", " _
		    & "ModifiedDate='"	& modifiedDate							& "' "
	'finalize update query
	mySQL = mySQL & "WHERE CustomerID='" & customerID & "' "  
	'execute
	set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	
	session(session("siteID") & "okMsg") = "User Field was deleted."
	response.redirect "../profiles/userfields.asp?cid=" & customerID	
	
end if

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>User Field</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>
      
	<!-- HTML List/Table resorting functions (right now own) -->
	<!-- Example usage:
    		var table = document.getElementById('table-1');
			var tableDnD = new TableDnD();
			tableDnD.init(table);  -->
	<script type="text/javascript" src="../scripts/javascript/orderList.js"></script>

	<!--JQuery required components -->
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />
    
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

	<!-- jQuery Prompt Replacements http://abeautifulsite.net/blog/2008/12/jquery-alert-dialogs/ -->
    <script src="../_jquery/prompt/jquery.alerts.js"></script>
    <link rel="stylesheet" type="text/css" href="../_jquery/prompt/jquery.alerts.css"/>
    
</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="popup_config_userfield.asp" style="padding:0px; margin:0px;">
    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/option_list.png" title="Categories" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">User Field</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Configure selected user field below.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Category table -->
    <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">
    
        <%
		if lCase(action) = "edit" then
			
			'CCI Admin/DBAdmin (bryan or steve)
			if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then			
				mySQL = "SELECT p" & fieldType & "Field" & recId & " AS [Name], p" & fieldType & "Field" & recId & "Value AS [Value], p" & fieldType & "Field" & recId & "Type AS [Type], p" & fieldType & "Field" & recId & "Order AS [Order], p" & fieldType & "Field" & recId & "Group AS [Group], p" & fieldType & "Field" & recId & "Default AS [Default] " _
					  & "	FROM Customer " _
					  & "	WHERE CustomerID='" & CustomerID & "' "
					  	  
			'everyone else
			else
				mySQL = "SELECT CustomerID, p" & fieldType & "Field" & recId & " AS [Name], p" & fieldType & "Field" & recId & "Value AS [Value], p" & fieldType & "Field" & recId & "Type AS [Type], p" & fieldType & "Field" & recId & "Order AS [Order], p" & fieldType & "Field" & recId & "Group AS [Group], p" & fieldType & "Field" & recId & "Default AS [Default] " _				
					  & "	FROM Customer " _
					  & "	WHERE Customer.CustomerID='" & customerID & "' AND " _ 
					  & "			EXISTS ( " _
					  & "			SELECT vwLogins_IssueType.CustomerID, vwLogins_IssueType.LogID " _
					  & "				FROM vwLogins_IssueType " _
					  & "				WHERE (vwLogins_IssueType.CustomerID=Customer.CustomerID) AND (vwLogins_IssueType.LogID=" & sLogid & ") ) "
					  
			end if				  
			'open customer table...				  
	        set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
			
			if rs.eof then				
				response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")	
			end if		

			optName = rs("Name")
			optValue = rs("Value")
			optType = rs("Type")
			optOrder = rs("Order")
			optGroup = rs("Group")
			optDefault = rs("Default")

		end if
        %>

		<tr>
       		<td class="formLabel" style="border-top:none; margin-top:0px; padding-top:0px;"><span class="required">*</span>Name:</td>
       		<td align=left style="border-top:none; margin-top:0px; padding-top:0px;">
           		<input name="optName" id="optName" class="inputLong" style="width:285px;" value="<% =optName %>" maxlength="150" />
       		</td>
   		</tr>

		<tr>
       		<td class="formLabel">Type:</td>
       		<td align=left>
 				<select name="optType" id="optType" size=1 class="inputMedium" style="width:175px;" onChange="setupValues();">
	                <option value="">-- Select --</option>                                    
             		<option value="Date" <%=checkMatch(optType,"Date")%>>Calendar Date</option>                                    
              		<option value="Option" <%=checkMatch(optType,"Option")%>>Option list</option>                                        
            		<option value="Memo" <%=checkMatch(optType,"Memo")%>>Memo (unlimited)</option>
              		<option value="Text" <%=checkMatch(optType,"Text")%>>Text box</option>                                        
      			</select>
                
                <span style="font-weight:bold; color:#35487B;">Order:</span>
                <input name="optOrder" id="optOrder" class="inputShort" style="width:50px;" value="<% =optOrder %>" maxlength="10" />
                
       		</td>
   		</tr>

		<tr>
       		<td class="formLabel"><div id="optValue:Label">Options:</div></td>
       		<td align=left>
				<div class="subLabel" style="padding-bottom:5px;">Separate each option with a carriage return.</div>
            	<div id="optValueBox">
					<textarea name="optValue" id="optValue" style="width:285px; height:57px;"><% =optValue %></textarea>
                    <div style="margin-top:5px;">Default: 
                        <%
						dim optArr
						if lCase(action) = "edit" then
							optArr = getDropDownOpt("p" & fieldType & "Field" & recId & "Value",customerID)
						end if
						'build combo box					
						call buildSelect(optArr,optDefault,"optDefault","inputMediumLong","Y")						
                        %>
                    </div>                    
                </div>
       		</td>
   		</tr>

		<!-- START Group Table -->    
		<tr>
        	<td class="formLabel">Groups:</td>    
            <td align=left> 

    			<div id="field_list_container">
              		<div id="field_list" style="width:285px; border: 1px solid #ccc; padding: 2px; height:57px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">
                  		<!-- table used for javascript slider -->
                  		<table id="table:field:list" width="95%" border="0" cellspacing="0" cellpadding="0">
                     		<%
                        	dim checkIssue, groupI, groupLinked, groupArray

							' find any linked groups
							groupLinked = ""
							groupArray = split(optGroup,"*|*")
							for groupI = LBound(groupArray) to UBound(groupArray)
								if len(groupArray(groupI))>0 then
									groupLinked = groupLinked & groupArray(groupI) & ","
								end if
							next
							if right(groupLinked,1) = "," then ' clean up
								groupLinked = left(groupLinked,len(groupLinked)-1)
							end if	
							if left(groupLinked,1) = "," then ' clean up
								groupLinked = right(groupLinked,len(groupLinked)-1)
							end if	

                         	'find all groups for this customer
                          	mySQL = "SELECT GroupID, Name, CustomerID " _
                        		  & "	FROM Groups " _
                        		  & "	WHERE CustomerID='" & customerID & "'"
							if groupLinked <> "" then
								mySQL = mySQL & " OR GroupID IN(" & groupLinked & ")"
							end if
								  
                      		set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
							if not rs.eof then
								do while not rs.eof
									if instr(optGroup, "*|*" & rs("GroupID") & "*|*") > 0 then
										checkIssue = " checked "
									else
										checkIssue = " "
									end if
									response.write("<tr><td class=""clearFormat"" style=""border:none; padding:0px; margin:0px;"" align=""left"">")                                                    
									response.write("	<div id=""field_list_item_Name"" class=""checkItem"">")											
									response.write("		<input name=""list_checkBox"" id=""field_list_checkBox"" type=""checkbox"" style=""cursor:default;"" value=""" & rs("GroupID") & """ " & checkIssue & " />")
									response.write("		<label for=""field_list_checkBox"" style=""cursor:move;"" onclick=""javascript:return false;"">" & rs("name") & "</label>")
									if uCase(rs("CustomerID")) <> uCase(customerID) then
										response.write(" [linked: " & rs("CustomerID") & "]")
									end if
									response.write("	</div>")    
									response.write("</td></tr>")
									rs.movenext
								loop
							else
								response.write("<tr><td class=""clearFormat"" style=""border:none; padding:0px; margin:0px;"" align=""left"">")                                                    
								response.write("</td></tr>")          							
							end if
                           	call closeRS(rs)
                          	%>
                   	</table>
           			<!-- Initiate listOrder.js for moving fields in order -->
             		<script type="text/javascript">
						var table = document.getElementById('table:field:list');
						var tableDnD = new TableDnD();
						tableDnD.init(table);
         			</script>
          			</div>
    			</div>
                    				
 				<div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frm','field_list_checkBox','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frm','field_list_checkBox',''); return false;">None</a></div>

 			</td>

     	</tr>                       
		<!-- STOP Group Table -->
        
		<tr>
           	<td colspan="2" align=left nowrap style="padding:0px; margin:0px;">&nbsp;</td>
       	</tr>
        
    </table>
	<!-- STOP Category table -->

	<input name="recid" type="hidden" value="<% =recid %>">
    <input name="action" type="hidden" value="<% =action %>"> 
    <input name="type" type="hidden" value="<% =fieldType %>">   
    <input name="cid" type="hidden" value="<% =customerid %>">    
    <input name="save" type="hidden" value="save">
        
	<!-- START Buttons -->
	<div style="float:right;"> 	       		
        <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); if(validateForm()==true){document.frm.submit()};"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Buttons -->

</form>
        
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script>
	function validateForm() {
		var formPassed = true;	//retunred value
		var reqFields = "optName";
		var temp = reqFields.split(",");
		var i;
		var el;		
		
		for(i=0; i<temp.length; i++) {			
			if (document.getElementById(temp[i])) {			
				el = document.getElementById(temp[i])
				if (!el.value) {			
					el.style.background = '#ffff9f'; //set to yellow			
					jAlert('<strong>Form Error!</strong><br/><br/>Please ensure all required fields have been entered.', 'myCM Alert'); //inform user of error
					formPassed = false; //return error
				}
				else {				
					if (el.style.background == '#ffff9f') {
						//alert('2:' + temp[i]);
						el.style.background = '#fff';	//set back to white
					}
				}		
			}
		}
		return formPassed;
		
	}	
	
	function setupValues() {
		if (document.getElementById("optType").value=="Option") {
			document.getElementById("optValue:Label").style.color = "#35487B";
	        $('#optValueBox :input').removeAttr('disabled');					
		}
		else {					
			document.getElementById("optValue:Label").style.color = "#666";					
			$('#optValueBox :input').attr('disabled', true);	//disable elements in div
		}	
	}	
	//run on page load
	setupValues();
</script>



