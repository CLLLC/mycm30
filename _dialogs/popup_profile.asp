<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'Database
dim mySQL, cn, rs
dim count

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim pageView : pageView = Request.QueryString("pageView")

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Choose a Location</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- HTML Table Filter Generator http://tablefilter.free.fr/ -->
    <link rel="stylesheet" type="text/css" href="../_tablefilter/filtergrid.css"/>
	<script type="text/javascript" src="../_tablefilter/tablefilter_all.js"></script>
	<script type="text/javascript" src="../_tablefilter/sortabletable.js"></script>
    <script type="text/javascript" src="../_tablefilter/tfAdapter.sortabletable.js"></script>        

    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/building_go.png" title="Locations" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Customers</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Select <% =lCase(accountLabelSingle) %> by clicking within the 'Name' column.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Profile table -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
    
        <%
		'CCI Admin/DBAdmin (bryan or steve)
		if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then			
			mySQL = "SELECT Customer.Name, Customer.CustomerID AS [ID], Customer.City, Customer.State, Customer.CustomerID " _
				  & "	FROM Customer " _
				  & "	WHERE Canceled=0 " _
				  & "	ORDER BY Name "		
					  		
		'CCI staff only...this is so users DO NOT have to be assigned to
		'a group in order view a customer profile
		elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then					
			mySQL = "SELECT Customer.Name, Customer.CustomerID AS [ID], Customer.City, Customer.State, Customer.CustomerID " _
				  & "	FROM Customer " _
				  & "	WHERE Canceled=0 AND " _
				  & "	 	EXISTS ( " _
				  & "			SELECT Customer_IssueType.CustomerID " _
				  & "				FROM Customer_IssueType " _
				  & "				WHERE (Customer_IssueType.CustomerID=Customer.CustomerID) AND (Customer_IssueType.IssueTypeID=1 OR Customer_IssueType.IssueTypeID=2) ) " _
				  & "	ORDER BY Name "
				  
		'everyone else
		else
			mySQL = "SELECT Customer.Name, Customer.CustomerID AS [ID], Customer.City, Customer.State, Customer.CustomerID " _
				  & "	FROM Customer " _
				  & "	WHERE Canceled=0 AND " _
				  & "	 	EXISTS ( " _
				  & "			SELECT vwLogins_IssueType.CustomerID, vwLogins_IssueType.LogID " _
				  & "				FROM vwLogins_IssueType " _
				  & "				WHERE (vwLogins_IssueType.CustomerID=Customer.CustomerID) AND (vwLogins_IssueType.LogID=" & sLogid & ") AND (vwLogins_IssueType.Active='Y') ) " _
				  & "	ORDER BY Name "			  
		
		end if
		%>
        
        <tr>
        	<td>
			<%
			dim tableProperty
			'set _tablefilter/default.asp settings.
            tableProperty = "sort: true, sort_config:{sort_types:['String','String','String','String']}, filters_row_index: 1, " _
                          & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
                          & "paging: true, paging_length: 6, " _
						  & "highlight_keywords: true, " _
                          & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", " _
                          & "or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
                          & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"
			
			'make call to build grid found in ../_tablefilter/default.asp
			call buildGrid("profileTableID",mySQL,"","customer-popup","../_images/icons/16/building_link.png",tableProperty) 
        	%>
        	</td>
        </tr>
    </table>
	<!-- STOP Locations table -->

	<!-- START Cancel Button -->
	<div style="padding-top:5px; margin-bottom:0px;">
		<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Cancel Button -->

</body>
</html>

<%
'close database connection
call closeDB()
%>

<% if pageView = "custom:edit" then %>
	<script language="javascript">	
		// dependent on SimpleModal.js
		// note: topWin.parent is defined in SimpleModal.js and replaces "window.opener"
		function addIssue(customerID,security) {
			$.ajax({
			  	url: "../_jquery/suggestbox/json-data.asp?view=customer&field=customername&term="+customerID,
			  	cache: false,
			  	async: false,
			  	dataType: "json",
			  	success: function(data) {
					topWin.parent.document.getElementById('profilesName').value = data;
					topWin.parent.document.getElementById('profiles').value = customerID;
			}});				
			//close window
       		SimpleModal.close();
		}
	</script>
<% elseif pageView = "issue:tools" then %>
	<script language="javascript">	
		// dependent on SimpleModal.js
		// note: topWin.parent is defined in SimpleModal.js and replaces "window.opener"
		function addIssue(customerID,security) {
			$.ajax({
			  	url: "../_jquery/suggestbox/json-data.asp?view=customer&field=customername&term="+customerID,
			  	cache: false,
			  	async: false,
			  	dataType: "json",
			  	success: function(data) {
					topWin.parent.document.getElementById('profilesName').value = data;
					topWin.parent.document.getElementById('profiles').value = customerID;
			}});				
			//trigger move
			topWin.parent.moveIssue()			
			//close window
       		SimpleModal.close();
		}
	</script>
    
<% elseif pageView = "customers:users" then 

	if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
	%>
    	<script language="javascript">	   
			function addIssue(customerID,security) {
	            SimpleModal.close();				
				SimpleModal.open('../_dialogs/popup_user_table.asp?cid='+ customerID + '&pageview=customers', 500, 700, 'no');

			}
		</script>        
    <% else %>    
		<script language="javascript">	    
            function addIssue(customerID,security) {	
                topWin.parent.navigateAway("../profiles/users.asp?cid="+customerID)
                SimpleModal.close();			
            }
        </script>    
	<% 
	end if
	
elseif pageView = "customers:locations" then 

	if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
	%>
    	<script language="javascript">	   
			function addIssue(customerID,security) {	
	            SimpleModal.close();										
				SimpleModal.open('../_dialogs/popup_location_table.asp?cid='+ customerID + '&pageview=customers', 500, 675, 'no');
			}
		</script>        
    <% else %>    
		<script language="javascript">	    
            function addIssue(customerID,security) {	
                topWin.parent.navigateAway("../profiles/location.asp?cid="+customerID)
                SimpleModal.close();			
            }
        </script>    
	<% 
	end if
    
else %>
	<script language="javascript">	
        // dependent on SimpleModal.js
        // note: topWin.parent is defined in SimpleModal.js and replaces "window.opener"
        function addIssue(customerID,security) {
            //push selected location to parent Issue window
            topWin.parent.addIssue(customerID,security)
            SimpleModal.close();
        }
    </script>
<% end if %>

