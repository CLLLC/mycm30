<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim pageView : pageView = Request.QueryString("pageView")
'if pageView = "loctable" then
'	pageView = "popup_location_table.asp"
'else
'	pageView = "popup_location.asp"
'end if
pageView = "popup_issue.asp"


'Work Fields
dim pageSize
dim I
dim item
dim count
dim countString
dim optConfigure

'Get specific customerID
dim customerID : customerID = Request.Form("cid")
if len(customerID) = 0 then
	customerID = Request.QueryString("cid")
end if

'Get master issue
dim idIssue : idIssue = Request.Form("recId")
if len(idIssue) = 0 then
	idIssue = Request.QueryString("recId")
end if

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Issue Search</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- Session variables used in Javascript functions and default Values -->
	<script language="Javascript1.2">
		<!--
		var sessionLogid=<%= session(session("siteID") & "logid")%>; //this value is used in DHTML menu,specifically the "Profile" mentu item
		//-->
	</script> 
       
	<!-- MENU TOP PAGE Sothink DHTML menu http://www.sothink.com/ -->		
	<script type="text/javascript" src="../scripts/menu/stmenu.js"></script>
	<script type="text/javascript" src="../scripts/tree/stlib.js"></script>

	<!--JQuery required components -->
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />

    <!-- ONLY FOR suggest boxes and JSON data pulls, includes full JQuery library -->
    <style>
        .ui-autocomplete-loading { background: white url('../_jquery/suggestbox/ui-anim_basic_16x16.gif') right center no-repeat; }
    </style>
    <style>
        .ui-autocomplete {
            max-height: 100px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
            /* add padding to account for vertical scrollbar */
            padding-right: 20px;
        }
        /* IE 6 doesn't support max-height
         * we use height instead, but this forces the menu to always be this tall
         */
        * html .ui-autocomplete {height: 100px;}
    </style>
    <!-- ----------------------------------------- -->
    
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

    
</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="<% =pageView %>" style="padding:0px; margin:0px;">
    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/table_tab_search.png" title="Alerts" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Issue Search</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
        
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
                        	<% if cLng(request.querystring("count")) > 1000 then %>
		            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="padding:5px;"><span class="required"><% =formatNumber(request.querystring("count"),0) %> records found.</span> Please narrow search to less than 1,000 issues.
    						<% else %>
	                           	<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="padding:5px;"><span class="required">More than 1,000 records found.</span> Use form below to limit issue list.
                           	<% end if %>
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Alert table -->
    <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">

		<tr>
       		<td class="formLabel">Issue #:</td>
       		<td align=left>
           		<input name="locCRSID" id="locCRSID" class="inputLong" style="width:250px;" value="<% =request.querystring("locCRSID") %>" maxlength="150" />
                <div class="subLabel">Enter all or a portion of the issue number.</div>
       		</td>
   		</tr>

		<tr>                            
			<td class="formLabel">Location:</td>
         	<td align=left nowrap>                                       
        		<div>                                
        	    	<div class="subLabel">Name:</div>
                    	<input name="locName" id="locName" class="inputLong" value="<% =request.querystring("locName") %>" maxlength="255" />
                   	</div>				                                                            
                  	<div class="subLabel">Street Address:</div>
                   	<div>
                		<textarea name="locAddress" id="locAddress" style="width:300px; height:25px;"><% =request.querystring("locAddress") %></textarea>
                 	</div>                               
                  	<div style="float:left;">
	           			<div class="subLabel">City:</div>
                    		<input name="locCity" id="locCity" class="inputShort" value="<% =request.querystring("locCity") %>" maxlength="255" />
               		</div>                                
                  	<div style="float:left;">
    	        		<div class="subLabel">State/Province:</div>
                    		<input name="locState" id="locState" class="inputShort" value="<% =request.querystring("locState") %>" maxlength="255" />
                	</div>                                
                 	<div>                                
        	     		<div class="subLabel">Postal Code:</div>
                   		<input name="locZip" id="locZip" class="inputShort" value="<% =request.querystring("locZip") %>" maxlength="255" />
         		</div>                                			                                                            
			</td>                              
		</tr>

		<tr>
      		<td class="formLabel">Caller:</td>
        	<td align=left nowrap>                              
         		<div style="float:left;">
               		<div class="subLabel">First Name:</div>
              		<div>
                    	<input name="locFirstName" id="locFirstName" class="inputMedium" value="<% =request.querystring("locFirstName") %>" maxlength="20" />
                    </div>
            	</div>                                
               	<div style="float:left;">
                 	<div class="subLabel">Last Name:</div>
                  	<div>
                   		<div style="float:left;">
                  			<input name="locLastName" id="locLastName" class="inputMedium" value="<% =request.querystring("locLastName") %>" maxlength="20" />
                 		</div>
                 	</div>
             	</div>
            </td>
		</tr>     

		<tr>
       		<td class="formLabel">Summary</td>
       		<td align=left>
                <textarea name="locSummary" id="locSummary" style="width:600px; height:50px;"><% =request.querystring("locSummary") %></textarea>
       		</td>
   		</tr>

   		<tr>
       		<td colspan="2" style="padding-bottom:0px; margin-bottom:0px;">&nbsp;</td>
  		</tr>                            
        
    </table>
	<!-- STOP Alert table -->

    <input name="action" type="hidden" value="search">
    <input name="cid" type="hidden" value="<% =customerID %>">    
    <input name="recid" type="hidden" value="<% =idIssue %>">    
        
	<!-- START Buttons -->
	<div style="float:right;"> 	       		
        <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); document.forms['frm'].submit();"><span class="save" style="padding-right:10px;"><u>S</u>earch</span></a>
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Buttons -->

</form>
    
</body>
</html>

<%
'close database connection
call closeDB()
%>

		<script>
			//JSON QUERY BUILDER used to search database
			buildJQuery('location','name','locName','<% =customerID %>');		
			buildJQuery('location','city','locCity','<% =customerID %>');		
			buildJQuery('location','state','locState','<% =customerID %>');					
			buildJQuery('country','country','locCountry','<% =customerID %>');

			function buildJQuery(table,field,form_field,cid) {
																
				$(function() {
					function split( val ) {
						return val.split( /,\s*/ );
					}
					function extractLast( term ) {
						return split( term ).pop();
					}                                                            
					$("#"+form_field)
						// don't navigate away from the field on tab when selecting an item
						.bind( "keydown", function( event ) {
                      		if ( event.keyCode === $.ui.keyCode.TAB &&
                            	$( this ).data( "autocomplete" ).menu.active ) {
                               		event.preventDefault();
                             }
                        })
                     	.autocomplete({
                      		source: function( request, response ) {
                          		$.getJSON( "../_jquery/suggestBox/json-data.asp?view="+table+"&field="+field+"&cid="+cid, {
                             		term: extractLast( request.term )
                           		}, response );
                          	},
                       		search: function() {
                        		// custom minLength
                             	var term = extractLast( this.value );
                           		if ( term.length < 1 ) {
                            		return false;
                         		}
                      		},
                      		focus: function() {
                    			// prevent value inserted on focus
                      			return false;
                         	}
                  		});
				});																																
			}
		</script>                                                            														
