<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
Server.ScriptTimeout =25000
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, pageSize

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim countString
dim optConfigure

dim idIssueNew

'make sure user can update this page
if uCase(viewToolBox) = "Y" then
	'good to go
else
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if


'get idIssue (1101-AAA-10001-01)
dim idIssue
idIssue = trim(Request.QueryString("recId"))
if len(idIssue) = 0 then
	idIssue = trim(Request.Form("recId"))
	if idIssue = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")
	end if
end if

'Get issue type
dim issueType : issueType = trim(Request.QueryString("issuetype"))
if len(issueType) = 0 then
	issueType = trim(Request.Form("issuetype"))
	if len(issueType) = 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue Type.")	
	end if
end if

'Get action
dim action : action = trim(Request.QueryString("action"))
if len(action) = 0 then
	action = trim(Request.Form("action"))
end if
action = lCase(action)
if action <> "move" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if

'check user view status	for customer and issuetype
'based on vwLogins_IssueType and security sets
if action = "move" then
	if userViewIssue(idIssue,sLogid) = False then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to view this issue.")
	end if
end if

dim navEditURL : navEditURL = "../scripts/" & session(session("siteID") & "issuesEdit")

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Move Issue</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/transform_move.png" title="Issues" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Move Issue</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">The status of your move is listed below.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->


	<!-- START File Import -->
	<div id="step_1" style="padding-bottom:5px;"><img id="image_1" src="../_images/myloader.gif" align="absmiddle">&nbsp;Preparing to move issue</div>
	<div id="step_2" style="padding-bottom:5px;"><img id="image_2" src="../_images/bullet_yellow.png" align="absmiddle">&nbsp;Moving: <% =idIssue %></div>
    <div style="padding-bottom:15px;">
	  	<div id="step_3" style="float:left;"><img id="image_3" src="../_images/bullet_yellow.png" align="absmiddle">&nbsp;Target #:</div>
        <div id="step_3_sub"><em>&lt;unknown&gt;</em></div>
	</div>    
	<div id="step_4" style="padding-bottom:5px;">&nbsp;</div>    

	<!-- START Buttons -->
    <div style="float:right;"> 	       		
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); navigateNew(document.getElementById('step_3_sub').innerHTML);"><span class="cancel"><u>C</u>lose</span></a>
    </div>    
	<!-- STOP Buttons -->

	<%
	'push to screen NOW
	response.flush()
	%>    


	<!-- STEP 1 stored procedure -- MAKE Location table BACK-UP! -->
	<script language="javascript">
		document.getElementById('image_1').src = '../_images/myloader.gif';
   	</script>    
	<%			
	response.flush()	
	'RUN OTHER STUFF HERE...
	%>
	<script language="javascript">
		document.getElementById('image_1').src = '../_images/check.png';
   	</script>
	<%		
	response.flush()
	%>


	<!-- STEP 2 import uploaded file -->
	<script language="javascript">
		document.getElementById('image_2').src = '../_images/myloader.gif';
   	</script>    
	<%			
   	on error resume next	
	response.flush()
	set rs = openRSexecute(" EXEC sp_MoveIssue '" & idIssue & "', '" & uCase(customerID) & "', " & issueType & ", " & sLogid)
	if err.number <> 0 then
		%>
		<script language="javascript">
            document.getElementById('image_2').src = '../_images/error.png';
        </script>        
        <%
		response.write("<strong>Error:</strong> " & err.Description)
		response.flush()
		response.end
	end if
	on error goto 0	
	%>
	<script language="javascript">
		document.getElementById('image_2').src = '../_images/check.png';
   	</script>
	<%		
	response.flush()
	%>


	<!-- STEP 3 get new CRSID -->
	<script language="javascript">
		document.getElementById('image_3').src = '../_images/myloader.gif';
   	</script>    
	<%			
	response.flush()
	mySQL = "SELECT CRSID FROM CRS WHERE Name='" & idIssue & "' "
	set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,0)	
	if rs.eof then
		%>
		<script language="javascript">
            document.getElementById('image_3').src = '../_images/error.png';
        </script>        
        <%
		response.write("<strong>Error:</strong> " & err.Description)
		response.flush()
		response.end
	else
		idIssueNew = rs("crsid")
	end if
	%>
	<script language="javascript">
		document.getElementById('image_3').src = '../_images/check.png';
		document.getElementById('step_3_sub').innerHTML = '<% =idIssueNew %>';		
   	</script>
	<%		
	response.flush()
	%>


    <!-- STEP 4 Finalize -->
	<script language="javascript">
		document.getElementById('step_4').innerHTML = 'Move <strong>COMPLETE</strong>';
    </script>        
    <%
	response.flush()
	%>

        
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script language="javascript">
	//for adding a single investigation using 
	//the ADD button
	function navigateNew(idIssue) {
		//trigger move
		topWin.parent.navigateAway('<% =navEditURL %>?action=edit&recid='+idIssue)
		//close window
   		SimpleModal.close();		
	}		
</script>

