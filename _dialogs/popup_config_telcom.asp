<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, pageSize

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim countString
dim optConfigure

'make sure user can update this page, CCI Admins ONLY
if cLng(session(session("siteID") & "adminLoggedOn")) > 2 then	
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if


'Get action type 
dim action : action = Request.Form("action")
if len(action) = 0 then
	action = Request.QueryString("action")
end if
if lCase(action) <> "edit" and _
   lCase(action) <> "add" and _
   lCase(action) <> "del" then	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Action Type.")	
end if

'Get record ID if editing or deleting
if lCase(action) = "edit" or lCase(action) = "del" then
	dim recId : recId = Request.Form("recId")
	if len(recId) = 0 then
		recId = Request.QueryString("recid")
	end if
	if len(recId) = 0 or isNull(recId) or recId = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Record ID.")	
	end if
end if

dim activeCalendars : activeCalendars = "'optBegins','optEnds'"

dim modifiedDate : modifiedDate = Now()

'form variables
dim optNumber
dim optDNIS
dim optPrompt
dim optGreeting
	
'set sort order on save
if request.form("save") = "save" then

	'get new telcom values
	optNumber = request.form("optNumber")
	optDNIS = request.form("optDNIS")
	optPrompt = request.form("optPrompt")
	optGreeting = request.form("optGreeting")
	
	'save existing telcom
	if action = "edit" then
		mySQL = "UPDATE Telcom Set " _
				& "Number='" 		& optNumber 					& "'," _
				& "DNIS='" 			& optDNIS 						& "'," _
				& "Answer='" 		& replace(optPrompt,"'","''") 	& "'," _	
				& "Greeting='" 		& replace(optGreeting,"'","''") & "'," _
				& "ModifiedBy=" 	& sLogid 						& ", " _	
				& "ModifiedDate='" 	& modifiedDate 					& "' " _	
				& "WHERE  TelcomID=" & recId & " AND CustomerID='" & customerID & "' "   
		set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

		session(session("siteID") & "okMsg") = "Telcom entry has been saved successfully."
		
	'add new telcom			
	elseif action = "add" then
		mySQL = "INSERT INTO Telcom (" _
			  & "CustomerID, Number, DNIS, Answer, Greeting, ModifiedBy, ModifiedDate" _
			  & ") VALUES (" _
			  & "'" & uCase(customerID)				& "'," _
			  & "'" & optNumber 					& "'," _
			  & "'" & optDNIS 						& "'," _
			  & "'" & optPrompt 					& "'," _
			  & "'" & replace(optGreeting,"'","''") & "'," _
			  & " " & sLogid 						& ", " _
			  & "'" & modifiedDate 					& "' " _
			  & ")"
		set rs = openRSexecuteDialog(mySQL)

		session(session("siteID") & "okMsg") = "Telcom entry was added."
			
	end if		
	%>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
	
	<script type='text/javascript'>
		topWin.parent.pageReload('<% =lCase(customerID) %>');
		SimpleModal.close();
    </script>	
	
<%
elseif action = "del" then

	'Delete Record from Categories_Issues
	mySQL = "DELETE FROM Telcom WHERE TelcomID=" & recId & " "
	set rs = openRSexecuteDialog(mySQL)	
	session(session("siteID") & "okMsg") = "Telcom entry was deleted."
	response.redirect "../profiles/telcom.asp?cid=" & customerID	
	
end if
%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Telcom</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- Session variables used in Javascript functions and default Values -->
	<script language="Javascript1.2">
		<!--
		var sessionLogid=<%= session(session("siteID") & "logid")%>; //this value is used in DHTML menu,specifically the "Profile" mentu item
		//-->
	</script> 
       
	<!-- MENU TOP PAGE Sothink DHTML menu http://www.sothink.com/ -->		
	<script type="text/javascript" src="../scripts/menu/stmenu.js"></script>
	<script type="text/javascript" src="../scripts/tree/stlib.js"></script>

	<!--JQuery required components -->
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

	<!-- jQuery Prompt Replacements http://abeautifulsite.net/blog/2008/12/jquery-alert-dialogs/ -->
    <script src="../_jquery/prompt/jquery.alerts.js"></script>
    <link rel="stylesheet" type="text/css" href="../_jquery/prompt/jquery.alerts.css"/>
    
</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="popup_config_telcom.asp" style="padding:0px; margin:0px;">
    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/phone.png" title="Telcom" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Telcom</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Configure selected telcom entry below.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Telcom table -->
    <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">
    
        <%
		if lCase(action) = "edit" then
		
			'CCI Admin/DBAdmin (bryan or steve)
			if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then			
				mySQL = "SELECT Telcom.*  " _
					  & "	FROM Telcom " _
					  & "	WHERE TelcomID=" & recId & " " _
					  & "	ORDER BY Number "
			'everyone else
			else
				mySQL = "SELECT Telcom.*  " _
					  & "	FROM Telcom " _
					  & "	WHERE EXISTS ( " _
					  & "			SELECT vwLogins_IssueType.CustomerID, vwLogins_IssueType.LogID " _
					  & "				FROM vwLogins_IssueType " _
					  & "				WHERE (vwLogins_IssueType.CustomerID=Telcom.CustomerID) AND (vwLogins_IssueType.LogID=" & sLogid & ") ) " _
					  & "		  AND (TelcomID=" & recId & ") " _
					  & "	ORDER BY Number "
			end if				  
			'open telcom table...				  
	        set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
			
			if rs.eof then				
				response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Telcom ID.")	
			end if		
			
			customerID 	= rs("customerid")
			optNumber 	= rs("number")
			optDNIS 	= rs("dnis")
			optPrompt 	= rs("answer")
			optGreeting = rs("greeting")

		end if
        %>

		<tr>
       		<td align=left>                                
       			<div style="float:left;">
               		<div style="color:#35487B; font-weight:bold; padding:3px;"><span class="required">*</span>Number</div>
             		<input name="optNumber" id="optNumber" class="inputMedium" value="<% =optNumber %>" maxlength="20" />
                </div>
              	<div style="float:left;">
              		<div style="color:#35487B; font-weight:bold; padding:3px;"><span class="required">*</span>DNIS</div>
             		<input name="optDNIS" id="optDNIS" class="inputShort" value="<% =optDNIS %>" maxlength="5" />
              	</div>                                
    		</td>
    	</tr>

     	<tr>
     		<td align="left">
            	<div style="color:#35487B; font-weight:bold; padding:3px;">Prompt</div>
                <input name="optPrompt" id="optPrompt" class="inputLong" value="<% =optPrompt %>" maxlength="250" />
             </td>
		</tr>
        
     	<tr>
     		<td align="left">
            	<div style="color:#35487B; font-weight:bold; padding:3px;">Greeting</div>
      			<textarea name="optGreeting" id="optGreeting" style="width:98%; height:100px; margin-bottom:12px;" ><%=server.HTMLEncode(optGreeting & "")%></textarea>
             </td>
		</tr>
        
    </table>
	<!-- STOP Telcom table -->

	<input name="recid" type="hidden" value="<% =recid %>">
    <input name="action" type="hidden" value="<% =action %>">    
    <input name="save" type="hidden" value="save">
    <input name="cid" type="hidden" value="<% =customerid %>">
        
	<!-- START Buttons -->
	<div style="float:right;"> 	       		
        <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); if(validateForm()==true){document.frm.submit()};"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Buttons -->

</form>
        
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script>
	function validateForm() {
		var formPassed = true;	//retunred value
		var reqFields = "optNumber,optDNIS";
		var temp = reqFields.split(",");
		var i;
		var el;		
		
		for(i=0; i<temp.length; i++) {			
			if (document.getElementById(temp[i])) {			
				el = document.getElementById(temp[i])
				if (!el.value) {			
					el.style.background = '#ffff9f'; //set to yellow			
					jAlert('<strong>Form Error!</strong><br/><br/>Please ensure all required fields have been entered.', 'myCM Alert'); //inform user of error
					formPassed = false; //return error
				}
				else {				
					if (el.style.background == '#ffff9f') {
						//alert('2:' + temp[i]);
						el.style.background = '#fff';	//set back to white
					}
				}		
			}
		}
		return formPassed;
		
	}
</script>

