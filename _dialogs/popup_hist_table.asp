<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement, Pro, Lite, Metrics
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'Database
dim mySQL, cn, rs, count

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim dataView
dataView = trim(Request.QueryString("dataView"))
if dataView = "" then
	dataView = trim(Request.Form("dataView"))
	if dataView = "" then
		dataView = "production"
	end if	
end if

dim historyTable
historyTable = trim(Request.QueryString("hist"))
if historyTable = "" then
	historyTable = trim(Request.Form("hist"))
	if historyTable = "" then
		historyTable = "Issue"
	end if	
end if

dim idIssue
idIssue = trim(Request.QueryString("recId"))
if idIssue = "" then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid History ID.")
end if

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>myCM Audit Log</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- HTML Table Filter Generator http://tablefilter.free.fr/ -->
    <link rel="stylesheet" type="text/css" href="../_tablefilter/filtergrid.css"/>
	<script type="text/javascript" src="../_tablefilter/tablefilter_all.js"></script>
	<script type="text/javascript" src="../_tablefilter/sortabletable.js"></script>
    <script type="text/javascript" src="../_tablefilter/tfAdapter.sortabletable.js"></script>        

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/database_table.png" title="Audit Log" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Audit Log: <% =idIssue %></span>
   	</div>                   

	<%

	if lcase(dataView) = "archive" then
		'close database connection
		call closeDB()
		'Open Database Connection
		call openArchivedb()
		
	else
		' do NOTHING...already on production
		
	end if

	'------------------------
	'get history record count
	'------------------------	
	if lCase(historyTable) = "issue" then
		mySQL="SELECT count([History ID]) AS [count] " _			 
			 & "	FROM   vwHistory_CRS " _
			 & "	WHERE [Issue #] = '" & idIssue & "' "
		'add progress level for prevention of viewing draft issues
		if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then
			mySQL = mySQL & " and SecurityLevel>=10 "
		end if

	elseif lCase(historyTable) = "category" then
		mySQL="SELECT count([History ID]) AS [count] " _			 
			 & "	FROM   vwHistory_CRS_Category " _
			 & "	WHERE [Issue #] = '" & idIssue & "' "
		'add progress level for prevention of viewing draft issues
		if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then
			mySQL = mySQL & " and SecurityLevel>=10 "
		end if

	elseif lCase(historyTable) = "investigation" then
		mySQL="SELECT count([History ID]) AS [count] " _			 
			 & "	FROM   vwHistory_Investigation " _
			 & "	WHERE [Issue #] = '" & idIssue & "' "
	
	elseif lCase(historyTable) = "managers" then
		mySQL="SELECT count([History ID]) AS [count] " _			 
			 & "	FROM   vwHistory_CRS_Logins " _
			 & "	WHERE [Issue #] = '" & idIssue & "' "
		'add progress level for prevention of viewing draft issues
		if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then
			mySQL = mySQL & " and SecurityLevel>=10 "
		end if
	
	elseif lCase(historyTable) = "e-mails" then
		mySQL="SELECT count([History ID]) AS [count] " _			 
			 & "	FROM   vwHistory_MailLog " _
			 & "	WHERE [Issue #] = '" & idIssue & "' "

	elseif lCase(historyTable) = "resolutions" then
		mySQL="SELECT count([History ID]) AS [count] " _			 
			 & "	FROM   vwHistory_Resolution " _
			 & "	WHERE [Issue #] = '" & idIssue & "' "

	elseif lCase(historyTable) = "telecom" then
		'close database connection
		call closeDB()
		'Open Database Connection
		call openPBXdb()
		mySQL="SELECT count([ID]) AS [count] " _			 
			 & "	FROM   calllog " _
			 & "	WHERE [DIDNumber] = '" & idIssue & "' "
	
	end if
	'open record set
	set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)		
	count = rs("count")
	call closeRS(rs)
	'------------------------	
		
	'there are more then 2,000 locations, STOP and rethink...
	if count>2000 then	
		response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
		response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
        call systemMessageBox("locationMessage","statusMessageALERT","Over 2,000 history records returned. Please contact CCI Support at support@ccius.com.")
		response.write("   </td>")
		response.write("</tr></table>")			
	else
	%>

	<!-- START Status box -->
	<table class="formTable" style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
          <form method="post" action="popup_hist_table.asp?recid=<% =idIssue %>" name="frm">
			<td>
                <fieldset style="border:1px solid #CCCCCC; background-color:#FFFFFF; ">
                    <legend>Logs</legend>
                        <div align="left" style="padding:5px;">
							Select log to view:
                            
                            <select name="dataView" id="dataView">
                                <option value="production" <% response.write(checkMatch("production",dataView)) %>>Production (0-30 days)</option>
                                <option value="archive" <% response.write(checkMatch("archive",dataView)) %>>Archive (31+ days)</option>
                            </select>                            
                            <%							
							dim optArr
       	                    'Build array for dropdown selections
							optArr = "Issue,Investigation,Category,Managers,E-mails,Resolutions"
							if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then 
								optArr = optArr & ",Telecom"
							end if							
							optArr = split(optArr,",")
							call buildSelect(optArr,historyTable,"hist","inputMediumLong","Y") 
							%>
							<input type="submit" name="btnLog" value="Open" />
                        </div>            
                </fieldset>
			</td>
          </form>
        </tr>
    </table>	
	<!-- STOP Status box -->

	<!-- START Locations table -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
    
        <%
		'--------------------------------------------------
		'get history record...
		'--------------------------------------------------	
		if lCase(historyTable) = "issue" then
			mySQL = "SELECT [History ID], [Modified By], [Modified Date], [Modifed Action] " _
				  & "	FROM vwHistory_CRS " _
				  & "	WHERE [Issue #] = '" & idIssue & "' "
				  'add progress level for prevention of viewing draft issues
				  if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then
					mySQL = mySQL & " and SecurityLevel>=10 "
				  end if
				  mySQL = mySQL & "	ORDER BY [Modified Date] DESC "			

		elseif lCase(historyTable) = "category" then
			mySQL = "SELECT [History ID], [Modified By], [Modified Date], [Modifed Action] " _
				  & "	FROM vwHistory_CRS_Category " _
				  & "	WHERE [Issue #] = '" & idIssue & "' " _
				  'add progress level for prevention of viewing draft issues
				  if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then
					mySQL = mySQL & " and SecurityLevel>=10 "
				  end if
				  mySQL = mySQL & "	ORDER BY [Modified Date] DESC "				

		elseif lCase(historyTable) = "investigation" then
			mySQL = "SELECT [History ID], [Modified By], [Modified Date], [Modifed Action] " _
				  & "	FROM vwHistory_Investigation " _
				  & "	WHERE [Issue #] = '" & idIssue & "' " _
				  & "	ORDER BY [Modified Date] DESC "			
		
		elseif lCase(historyTable) = "managers" then
			mySQL = "SELECT [History ID], [Modified By], [Modified Date], [Modifed Action] " _
				  & "	FROM vwHistory_CRS_Logins " _
				  & "	WHERE [Issue #] = '" & idIssue & "' " _
				  'add progress level for prevention of viewing draft issues
				  if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then
					mySQL = mySQL & " and SecurityLevel>=10 "
				  end if
				  mySQL = mySQL & "	ORDER BY [Modified Date] DESC "				
		
		elseif lCase(historyTable) = "e-mails" then
			mySQL = "SELECT [History ID], [Mail Date], [From], [To] " _
				  & "	FROM vwHistory_MailLog " _
				  & "	WHERE [Issue #] = '" & idIssue & "' " _
				  & "	ORDER BY [Mail Date] DESC "			

		elseif lCase(historyTable) = "resolutions" then
			mySQL = "SELECT [History ID], [Modified By], [Modified Date], [Modifed Action] " _
				  & "	FROM vwHistory_Resolution " _
				  & "	WHERE [Issue #] = '" & idIssue & "' " _
				  & "	ORDER BY [Modified Date] DESC "			

		elseif lCase(historyTable) = "telecom" then
			mySQL = "SELECT ID as [History ID], StartTime as [Start], StopTime as [Stop], AnsweredbyFullName as [Answered By] " _
				 & "	FROM   calllog " _
				 & "	WHERE [DIDNumber] = '" & idIssue & "' " _
			  	 & "	ORDER BY [StartTime] DESC "							 
		
		end if			   			  
		%>
        
        <tr>
        	<td>
			<%
			dim tableProperty
			'set _tablefilter/default.asp settings.
            tableProperty = "sort: true, sort_config:{sort_types:['String','String','String','String']}, filters_row_index: 1, " _
                          & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
                          & "paging: true, paging_length: 5, " _
						  & "highlight_keywords: true, " _
						  & "status_bar: true, " _						  
                          & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", " _
                          & "or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
                          & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"
			
			'make call to build grid found in ../_tablefilter/default.asp
			call buildGrid("histTableID",mySQL,"","hist-" & historyTable,"../_images/icons/16/magnifier.png",tableProperty) 
        	%>
        	</td>
        </tr>
    </table>
	<!-- STOP Locations table -->

	<!-- START Cancel Button -->
	<div style="padding-top:5px; margin-bottom:0px;">
		<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); topWin.parent.StartTheTimer(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Cancel Button -->

	<%
	end if
	%>    
     
</body>
</html>

<%
'close database connection
call closeDB()
%>

