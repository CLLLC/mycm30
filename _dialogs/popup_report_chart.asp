<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%
'Database
dim mySQL, cn, rs, count

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim pageView
pageView = "reports:edit"

'Issue Fields on DETAILS tab
'MAY NOT NEED THESE BELOW...
dim optID
dim optName
dim optDataset
dim optTable
dim optType
dim optFields
dim optFilter
dim optSQLValue

'Chart options
dim optChart
dim optChartXAxis
dim optChartYAxis
dim optChartSize
dim optChartLegend

'Work Fields
dim action
dim idReport

dim ModifiedDate : ModifiedDate = Now()

'Check User Security
if trim(session(session("siteID") & "viewStats")) = "N" then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("You are not authorized to view this page.")	
end if

'Get idIssue
idReport = trim(Request.QueryString("recId"))
if len(idReport) = 0 then
	idReport = trim(Request.Form("idReport"))
end if
if idReport = "" then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Report ID.")
end if

'Get action
action = trim(Request.QueryString("action"))
if len(action) = 0 then
	action = trim(Request.Form("action"))
end if
action = lCase(action)
if  action <> "edit" _
and action <> "save"  then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if


'******************************
'save chart settings
if action = "save" then	
		
	'------------------------------------------------
	'find all chart elements
	'------------------------------------------------	
	optChart		= Request.Form("optChart")
	optChartLegend	= Request.Form("optChartLegend")

	'update filter record
	mySQL = "UPDATE Reports SET " _
		  & "ChartType='"		& optChart			& "'," _		  
		  & "ChartLegend='"		& optChartLegend	& "'," _			  
		  & "ModifiedBy="		& sLogid			& ", " _
		  & "ModifiedDate='"    & ModifiedDate		& "' "		  		    		  		  		  	
	mySQL = mySQL & "WHERE ReportID = " & idReport
	'execute query									
	set rs = openRSexecuteDialog(mySQL)
		
	session(session("siteID") & "okMsg") = "Chart settings have been saved successfully."	
	%>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
    	
	<script type='text/javascript'>
		//topWin.parent.pageReload('<% =idReport %>');
		topWin.parent.pageReload();
		SimpleModal.close();
    </script>	
	
<%	
end if
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Choose a Location</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!--JQuery required components -->
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="popup_report_chart.asp" style="padding:0px; margin:0px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/chart_pie.png" title="Fi" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Chart Settings</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Modify selected chart using the form below.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Chart table -->
    <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">

        <%
		'get report record
		mySQL="SELECT Reports.ReportID, Reports.ChartType, Reports.ChartXAxis, Reports.ChartYAxis, Reports.ChartLegend, " _
			& "       Reports.QueryFields, Reports.QueryFilter, Reports.SQL " _
			& "   FROM (Reports INNER JOIN Dataset ON (Reports.DatasetID = Dataset.DatasetID) AND (Reports.CustomerID = Dataset.CustomerID)) LEFT JOIN Logins ON (Reports.CustomerID = Logins.CustomerID) AND (Reports.LogID = Logins.LOGID) " _
			& "	  WHERE ReportID = " & idReport & " AND Reports.CustomerID = '" & sCustomerID & "' AND (Reports.LOGID=" & sLogid & ")"								
		set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
			
		if rs.eof then
			response.redirect "../reports/reports.asp"
		else
			optID			= rs("ReportID")
			optChart		= rs("ChartType") & ""
			optChartLegend	= rs("ChartLegend") & ""
				
		end if
		call closeRS(rs)
        %>

		<tr>
        	<td class="formLabel"><span class="required">*</span>Style:</td>
            <td align=left nowrap>
            	<div>
                	<select name="optChart" id="optChart" size=1 style="width:285px;">
                    	<%
						Response.Write "<option value=""Area"" " & checkMatch(optChart,"Area") & ">Area</option>"
	                    Response.Write "<option value=""Bar"" " & checkMatch(optChart,"Bar") & ">Bar</option>"
						Response.Write "<option value=""Bar stacked"" " & checkMatch(optChart,"Bar stacked") & ">Bar stacked</option>"
						Response.Write "<option value=""Column"" " & checkMatch(optChart,"Column") & ">Column</option>"		
						Response.Write "<option value=""Line"" " & checkMatch(optChart,"Line") & ">Line</option>"	                                            
						%>
                     </select>                                                                            
            	</div>
				<div class="subLabel" style="width:200px; margin-bottom:6px;">Type of chart used on your report</div>									
       		</td>
    	</tr>                            

		<tr>
      		<td class="formLabel">Legend:</td>
       		<td align=left nowrap>
          		<div>
             		<select name="optChartLegend" id="optChartLegend" size=1 style="width:285px;">
	           			<option value="" selected="selected">-- No Legend --</option>
    	            		<%
							response.Write "<option value=""Top-Left"" " 		& checkMatch(optChartLegend,"Top-Left") 	& ">Top, Left</option>"
	        	            response.Write "<option value=""Top-Center"" " 		& checkMatch(optChartLegend,"Top-Center") 	& ">Top, Center</option>"
							response.Write "<option value=""Top-Right"" " 		& checkMatch(optChartLegend,"Top-Right") 	& ">Top, Right</option>"
							response.Write "<option value=""Middle-Left"" " 	& checkMatch(optChartLegend,"Center-Left") 	& ">Middle, Left</option>"		
							response.Write "<option value=""Middle-Center"" " 	& checkMatch(optChartLegend,"Center-Center")& ">Middle, Center</option>"	                                            
							response.Write "<option value=""Middle-Right"" " 	& checkMatch(optChartLegend,"Center-Right") & ">Middle, Right</option>"
							response.Write "<option value=""Bottom-Left"" " 	& checkMatch(optChartLegend,"Bottom-Left") 	& ">Bottom, Left</option>"
							response.Write "<option value=""Bottom-Center"" " 	& checkMatch(optChartLegend,"Bottom-Center") & ">Bottom, Center</option>"
							response.Write "<option value=""Bottom-Right"" " 	& checkMatch(optChartLegend,"Bottom-Right") 	& ">Bottom, Right</option>"										
							%>
             		</select>                                                                            
      			</div>
				<div class="subLabel" style="width:200px; margin-bottom:6px;">Placement of legend (vertical, horizontal)</div>									
      		</td>
     	</tr>                            

    </table>
	<!-- STOP Chart table -->

   	<input type="hidden" name="idReport" value="<% =optID %>">
    <input type="hidden" name="action" value="save">

	<!-- START Buttons -->
	<div style="float:right; padding-top:15px; margin-bottom:0px;"> 	       		
        <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); document.forms['frm'].submit(); return false;"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); return false;"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Buttons -->

</form>
    
</body>
</html>

<%
'close database connection
call closeDB()
%>
