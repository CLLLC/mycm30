
THIS IS NOT COMPLETE...USE ONLY TO PULL THE SQL AND PAGING STUFF!!!!

<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'Database
dim mySQL, cn, rs

'Work Fields
dim I
dim item
dim count
dim pageSize
dim totalPages
dim bolRecordsFound
dim curPage
dim showPhrase
dim sortField
dim sortOrder

dim columnArray
dim colCount
dim columnSQL
dim xCol
dim field

dim rowColor, col1, col2,rowSet
col1 = "#F0F0F0"
col2 = "#FFFFFF"

pageSize = 5

'Open Database Connection
call openDB()

'Store Configuration
if loadConfig() = false then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Could not load Configuration settings.")
end if

%>

<html>
<head>
<title>Choose a Location</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- HTML Table Filter Generator http://tablefilter.free.fr/ -->
    <link rel="stylesheet" type="text/css" href="../_tablefilter/filtergrid.css"/>
	<script type="text/javascript" src="../_tablefilter/tablefilter_all.js"></script>
	<script type="text/javascript" src="../_tablefilter/sortabletable.js"></script>
    <script type="text/javascript" src="../_tablefilter/tfAdapter.sortabletable.js"></script>        

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/company.24.png" title="Issues" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Locations</span>
   	</div>                   

	<!-- START Search box -->
	<table class="formTable" style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>

			<td>
                <fieldset style="border:1px solid #CCCCCC; background-color:#FFFFFF; ">
                    <legend>Information</legend>
                        <div align="left" style="padding:5px;">
                        	<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px;">Select a location by clicking within the 'Name' column.
                        </div>            
                </fieldset>
			</td>

        </tr>
    </table>	
	<!-- STOP Search box -->

	<!-- START location discovery in SQL -->      
    <%
	'Find locations available TO BE assigned
'	mySQL="SELECT a.Name, a.Address, a.City, a.State, a.PostalCode AS [Zip], a.Country, " _
'		 & "  a.Level_1 & ':' & a.Level_2 & ':' & a.Level_3 & ':' & a.Level_4 & ':' & a.Level_5 " _
'		 & "  & ':' & a.Level_6 & ':' & a.Level_7 & ':' & a.Level_8 AS [Hierarchy] " _			 
'		 & "FROM   Location a " _
'      	 & "WHERE 1=1 "
		 
	mySQL="SELECT a.Name, a.Address & '<br>' & a.City & ' ' & a.State & ' ' & a.PostalCode & '<br>' & a.Country AS [Full Address], " _
		 & "  a.Address, a.City, a.State, a.PostalCode, a.Country, " _
		 & "  a.Level_1 & ':' & a.Level_2 & ':' & a.Level_3 & ':' & a.Level_4 & ':' & a.Level_5 " _
		 & "  & ':' & a.Level_6 & ':' & a.Level_7 & ':' & a.Level_8 AS [Hierarchy] " _			 
		 & "FROM   Location a " _
       	 & "WHERE 1=1 "
		 
    'Phrase
   	if len(showPhrase) > 0 then mySQL = mySQL & "AND (" & showPhrase & ") "
	'Sort Order
	mySQL = mySQL & "ORDER BY a.Name "

	'execute SQL statement
	set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
				
	'check for records returned
	if rs.eof then
		bolRecordsFound = False
		curPage = 0
		totalPages = 0
	else
		bolRecordsFound = True
		rs.MoveFirst
		rs.PageSize	= pageSize
		totalPages 	= rs.PageCount
	
		'In case a negative (-) number is returned
		if curPage <= 0 then curPage = 1
		'In case a number larger than totalPages is returned
		if curPage > totalPages then curPage = totalPages
	
		'set current page to be viewed				
		rs.AbsolutePage	= curPage
	end if				 
	%>
	<!-- STOP location discovery in SQL -->      

	<!-- START Grid Header and Pagination -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    	<tr>
		  <form method="post" action="popup_location.asp<% =navQueryStr(curPage) %>" name="formIssuesPaging">        
        	<td align="left" valign="top" style="background:#f4f4f4 url(../_images/bg_infDiv.jpg) left bottom repeat-x; height:23px; border:1px solid #ccc;">

            <div style="float:left; width:150px; position:inherit; text-align:left; padding-left:7px; padding-top:1px;">	        	
                <%
				if curPage = 1 then
					if rs.recordcount > pageSize then
						response.write("Rows: 1-" & curPage*pageSize & " / " & rs.recordcount)
					else
						'number of rows <= record count (i.e. 1-10 / 10)
						response.write("Rows: 1-" & rs.recordcount & " / " & rs.recordcount)					
					end if
				elseif curPage = TotalPages then
					if bolRecordsFound = True then				
						response.write("Rows: " & (((curPage*pageSize)-pageSize)+1) & "-" & rs.recordcount & " / " & rs.recordcount)					
					else
						'no records found curPage=0, TotalPages=0 
						response.write("Rows: 0-0 / 0")					
					end if						
				else
					response.write("Rows: " & (((curPage*pageSize)-pageSize)+1) & "-" & curPage*pageSize & " / " & rs.recordcount)
				end if
				%>
            </div>
            
            <div style="float:left; width:188px; position:inherit; text-align:left; padding:1px 0; border-left:1px solid #ccc; height:22px;">&nbsp;</div>

			<!-- START Pagination -->
            <div style="float:left; position:inherit; text-align:center; padding-left:38px; padding-top:3px; border-left:1px solid #ccc; height:22px;">
				<%        
                'move backwards
                response.write("<a href=""popup_location.asp" & navQueryStr(curPage-1) & """ class=""pagerLink""><img src=""../_images/btn_previous_page.png"" width=""19"" height=""19"" border=""0"" align=""absmiddle"" /></a>")
                'move to first page
                response.write("<a href=""popup_location.asp" & navQueryStr(1) & """ class=""pagerLink""><img src=""../_images/btn_first_page.png"" style=""margin-left:2px; margin-right:2px;"" width=""19"" height=""19"" border=""0"" align=""absmiddle"" /></a>")
                %>                            
                Page <input name="curPage" style="width:25px; height:12px; font-size:8pt; vertical-align:middle; text-align:center; margin-left:6px;" value="<% =curPage %>"> of&nbsp;&nbsp;<% =TotalPages %>                
                <%        
                'move to last page
                response.write("<a href=""popup_location.asp" & navQueryStr(TotalPages) & """ class=""pagerLink""><img src=""../_images/btn_last_page.png"" style=""margin-left:6px; margin-right:2px;"" width=""19"" height=""19"" border=""0"" align=""absmiddle"" /></a>")
                'move forwards
                response.write("<a href=""popup_location.asp" & navQueryStr(curPage+1) & """ class=""pagerLink""><img src=""../_images/btn_next_page.png"" width=""19"" height=""19"" border=""0"" align=""absmiddle"" /></a>")
                %>           
            </div>
			<!-- STOP Pagination -->
            
            <div style="float:right; position:inherit; text-align:right; padding-top:2px; height:22px; padding-right:3px;">
            	<a href="popup_location.asp?resetCookie=1&curPage=1" class="pagerLink" style="padding-right:2px;"><img src="../_images/icons/12/btn_filter.png" width="50" height="16" border="0" /></a>
            </div>

			</td>
          </form>
        </tr>
    </table>
	<!-- STOP Grid Header and Pagination -->
    

	<!-- START Locations Table -->
	<%		
	for each field in rs.fields
		colCount = colCount + 1
	next	
	
	response.write("<table width=""100%"" id=""tableIssues"" cellpadding=""0"" cellspacing=""0"">")

		'no records found!
		if bolRecordsFound = False then
        	response.write("<tr><td align=center valign=middle>&nbsp;</td></tr>")
            response.write("<tr><td align=center valign=middle style=""padding-right:2px;"">")
            call systemMessageBox("systemMessage","statusMessageERROR","No locations matched your search criteria.")
            response.write("</td></tr>")
          
		'records found...build table
        else

			'set alternating row colors
            rowColor = col1

			'Reset count
			xCol = 0
			'set starting count
			count = 1

			'*******************************************
			'loop through all records found
			'*******************************************
            do while not rs.eof and count < rs.pageSize	

				'start new row
				response.write("<tr valign=""middle"">")
				
				'loop through all columns
				for each field in rs.fields
	
					'*******************************************
					'build/print column HEADER(s) (count = row#)
					'*******************************************
					if count = 1 then

						'first column
						if xCol = 0 then 
							'OPEN ICON...
    	       				response.write("<td class=""listRowTop"" nowrap=""nowrap"" width=""15"">&nbsp;</td>")

							'Location Name...
              				response.write("<td class=""listRowTop"" nowrap=""nowrap"" width=""25%"" align=""left"" style=""cursor:pointer;"" ") 
							response.write("onClick=""window.location='popup_location.asp?sortField=name&sortOrder=")
							if sortField="name" and sortOrder="asc" then 
								response.write("desc") 
							else 
								response.write("asc")								
							end if
							response.write("'"">")
							response.write(field.name)
							checkSortImg(field.name)
							response.write("</td>")
													
						end if

						'don't do anything for these headers
						if lCase(field.name) <> "name" and _
						   lCase(field.name) <> "address" and _
						   lCase(field.name) <> "city" and _
						   lCase(field.name) <> "state" and _
						   lCase(field.name) <> "postalcode" and _
						   lCase(field.name) <> "country" then
	
							'LAST column header, different class setting
							if xCol = (colCount - 1) then
	              				response.write("<td class=""listRowTopEnd"" nowrap=""nowrap"" align=""left"" style=""cursor:pointer;"" ") 
							'print header
							else							   
    	          				response.write("<td class=""listRowTop"" nowrap=""nowrap"" align=""left"" style=""cursor:pointer;"" ") 
							end if

							response.write("onClick=""window.location='popup_location.asp?sortField=" & field.name & "&sortOrder=")
							if sortOrder="asc" then response.write("desc") else response.write("asc")															
							response.write("'"">")
							response.write(field.name) 
							checkSortImg(field.name)
							response.write("</td>")

						end if

					'*******************************************
					'build/print field DATA value(s)
					'*******************************************
					else
					
						'first column of data row
						if xCol = 0 then
													
							'OPEN ICON...
							response.write("<td style=""background-color:" & rowColor &"; padding:5px; border-left:#969696 solid 1px; border-bottom:#969696 solid 1px;"" height=""20"" width=""15"" align=""center"" nowrap>")
							response.write("<a href=""issues_edit.asp?action=edit&recid=" & rs("name") & """>")
							response.write("<img src=""../_images/icons/16/page_blue_edit.png"" height=""16"" width=""16"" border=""0"" title=""Edit Issue"" align=""absmiddle""></a>")
							response.write("</td>")
														
							'CRSID...clickable TD to issues_edit.asp
							response.write("<td width=""25%"" height=""20"" align=""left"" nowrap style=""background-color:" & rowColor &"; padding:5px; border-bottom:#969696 solid 1px; cursor:pointer;"" onClick=""window.location='issues_edit.asp?action=edit&recid=" & rs("name") & "'"">" & rs("name"))
							response.write("</td>")

						end if
																			
						'don't do anything for these headers included in SQL
						if lCase(field.name) <> "name" and _
						   lCase(field.name) <> "address" and _
						   lCase(field.name) <> "city" and _
						   lCase(field.name) <> "state" and _
						   lCase(field.name) <> "postalcode" and _
						   lCase(field.name) <> "country" then
		
							'start TD for row, LAST column, different class setting
							if xCol = (colCount - 1) then
								response.write("<td align=""left"" style=""background-color:" & rowColor &"; padding:5px; border-bottom:#969696 solid 1px; border-right:#969696 solid 1px;"" height=""20"">")
							else							   
								response.write("<td align=""left"" style=""background-color:" & rowColor &"; padding:5px; border-bottom:#969696 solid 1px;"" height=""20"">")
							end if							

							'show/print nulls with empty space
							if isNull(rs(field.name)) or rs(field.name) = "" then
								response.write "&nbsp;"								
							'show/print values
							else
									
								'status change if NOT being printed
								if lCase(field.name) = "status" then
									call colorStatusBox(rs(field.name))
									response.write rs(field.name)
									
								'severity change
								elseif lCase(field.name) = "severity" then 			
									response.write( reformatSeverity(rs(field.name)) )
										
								'ALL others...
								else
									response.write(rs(field.name))
								end if								
																
							end if
							
							'close TD for row
							response.write("</td>")

						end if							
										
					end if
					
					xCol = xCol + 1
					'Reset for next row
					if xCol > (colCount - 1) then
						xCol = 0
					end if
					
				next
				
				Response.Write "</tr>"
				
				'Move to next record
				if count > 1 then
					rs.movenext
				end if
				count = count + 1
			
                'Switch Row Color
                if rowColor = col2 then rowColor = col1 else rowColor = col2

			'*******************************************
			'loop through all records returned
			'*******************************************
			loop		


		'main IF regarding were any records found?
		end if
	
	'close grid table		
	response.write "</table>"
	%>
	<!-- STOP Locations Table -->    



















	<!-- START Cancel Button -->
	<div style="padding-top:5px; margin-bottom:0px;"> 	       
		<a class="myCMbutton" href="#" onClick="this.blur(); javascript:window.close();" style="float:right;"><span class="cancel">Cancel</span></a>
	</div>
	<!-- STOP Cancel Button -->
     
<%
call closeRS(rs)
%>
    
</body>
</html>

<%
'*********************************************************************
'Make QueryString for Paging
'*********************************************************************
function navQueryStr(pageNum)

dim tmpField, tmpValue
    
	navQueryStr = "?curPage="		& server.URLEncode(pageNum) _
                & "&showPhrase="	& server.URLEncode(showPhrase)
				
end function

'*********************************************************************
'Make Cookie Value for Paging
'*********************************************************************
function navCookie(pageNum)

	navCookie = pageNum			& "*:*" _
              & showCRSID
                  
end function
%>

<script language="javascript">	
	function setLocation(sName,sAddress,sCity,sState,sPostal,sCountry,sHierarchy) {
		//push selected location to parent Issue window
		window.opener.setCurrentLocation(sName,sAddress,sCity,sState,sPostal,sCountry,sHierarchy)
		window.close();
	}
</script>

