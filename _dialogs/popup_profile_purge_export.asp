<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc.
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	         website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'validate security access
if loadUser(null) = false then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("An error has occurred.")
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim countString
dim optConfigure

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 3 then	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")	
end if

'Get action type 
dim customerID : customerID = Request.QueryString("cid")
if customerID = "" then   	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")	
end if

'Get action type 
dim action : action = Request.QueryString("action")
if lCase(action) <> "csv" then   	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Action Type.")	
end if

'Get action type 
dim recID : recID = Request.QueryString("recid")
if len(recID) <= 0 then   	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Purge ID.")	
end if


'*****************************************************************
'EXPORT CSV File
'*****************************************************************
if action = "csv" and len(recID) > 0 then
		
dim x, y
dim fldValue
dim listSQL

	''''listSQL	= "SELECT CRSID as [Issue #], [Date] FROM CRS WHERE CustomerID='" & customerID & "' AND PurgeID='" & recID & "' "
	listSQL	= "SELECT CRSID AS [Issue #], [Date] " _
			& "	FROM CRS " _
			& "	WHERE CustomerID = '" & customerID & "' " _
			& "	AND PurgeDate = (SELECT DISTINCT PurgeDate " _
			& "						FROM Purge " _
			& "						WHERE CustomerID = CRS.CustomerID " _
			& "						AND PurgeID = " & recID & ") "
	set rs = openRSexecute(listSQL)	

	' Save Output as a CSV file 
	response.ContentType = "text/csv"
	response.AddHeader "Content-Disposition", "filename=export.csv;"

	'Field Headers...
	for x = 0 to rs.fields.count-1 
		response.write """" & rs.fields(x).name & ""","
	next 
	response.write vbCrLf
		
	'Field detail/data
	while not rs.EOF 
		for y = 0 to rs.Fields.Count-1 
			fldValue = rs.fields(y).value
			'check for Text/Memo field
			if rs.fields(y).type = adLongVarWChar then
				'strip out html characters
				fldValue = stripHTML(fldValue)
				'strip out double quotes
				fldValue = replace(fldValue,"""","'")				
			end if												
			response.write """" & fldValue & ""","				
		next 
	 	response.write vbCrLf 
	 	rs.movenext 
	wend 

	call closeRS(rs)
	
end if

'close database connection
call closeDB()

%>

