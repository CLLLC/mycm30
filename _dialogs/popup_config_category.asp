<%@ Language=VBScript CodePage = 65001%>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs
dim fieldsSQL, valueSQL

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then	
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'Work Fields
dim pageSize
dim I
dim item
dim count
dim countString
dim optConfigure
dim modifiedDate : modifiedDate = Now()


'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 3 and programAdmin = "N" then	
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if


'Get action type 
dim action : action = Request.Form("action")
if len(action) = 0 then
	action = Request.QueryString("action")
end if
if lCase(action) <> "edit" and _
   lCase(action) <> "add" and _
   lCase(action) <> "del" then	   
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Action Type.")	
end if

'Get record ID if editing or deleting
if lCase(action) = "edit" or lCase(action) = "del" then
	dim recId : recId = Request.Form("recId")
	if len(recId) = 0 then
		recId = Request.QueryString("recid")
	end if
	if len(recId) = 0 or isNull(recId) or recId = "" then			
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Record ID.")	
	end if
end if


'form variables
dim optName
dim optSub
dim optType, arrType, expI
dim optDirectiveID
dim optSeverity1, optSeverity1Weekends
dim optSeverity2, optSeverity2Weekends
dim optSeverity3, optSeverity3Weekends
dim optSevDefault
dim optNotes
dim optLanguage

'set sort order on save
if request.form("save") = "save" then

	'get new category values
	optName = request.form("optName")
	optSub = request.form("optSub")
	optDirectiveID = request.form("optDirectiveID")

	optSeverity1 = request.form("optSeverity1")
	optSeverity1Weekends = request.form("optSeverity1Weekends")
	if len(optSeverity1Weekends) <=0 then optSeverity1Weekends = "0"
	if optSeverity1Weekends = "True" then optSeverity1Weekends = "1"
	if optSeverity1Weekends = "False" then optSeverity1Weekends = "0"

	optSeverity2 = request.form("optSeverity2")
	optSeverity2Weekends = request.form("optSeverity2Weekends")
	if len(optSeverity2Weekends) <=0 then optSeverity2Weekends = "0"
	if optSeverity2Weekends = "True" then optSeverity2Weekends = "1"
	if optSeverity2Weekends = "False" then optSeverity2Weekends = "0"
	
	optSeverity3 = request.form("optSeverity3")
	optSeverity3Weekends = request.form("optSeverity3Weekends")
	if len(optSeverity3Weekends) <=0 then optSeverity3Weekends = "0"	
	if optSeverity3Weekends = "True" then optSeverity3Weekends = "1"
	if optSeverity3Weekends = "False" then optSeverity3Weekends = "0"

	optType = trim(Request.Form("list_checkBox"))
	if (len(optType) = 0 or optType = "" or isNull(optType)) then			
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Issue Type Selection. At least one issue type must be selected.")
	end if
	arrType = split(optType,",")

	optNotes = request.form("optNotes")	
	optSevDefault = request.form("optSevDefault")
	optLanguage = request.form("optLanguage")
	
	'save existing category
	if action = "edit" then
	
		'save main fields
		mySQL = "UPDATE Category Set " _
				& "Name=N'" 			& replace(optName,"'","''") 	& "'," _
				& "Sub=N'" 			& replace(optSub,"'","''") 		& "'," _				
				& "Sev1=" 			& optSeverity1 					& ", " _
				& "Sev1Weekends=" 	& optSeverity1Weekends 			& ", " _				
				& "Sev2=" 			& optSeverity2 					& ", " _
				& "Sev2Weekends=" 	& optSeverity2Weekends 			& ", " _				
				& "Sev3=" 			& optSeverity3 					& ", " _
				& "Sev3Weekends=" 	& optSeverity3Weekends 			& ", " _		
				& "LanguageCD='"	& replace(optLanguage,"'","''")	& "'," _										
				& "Notes=N'" 		& replace(optNotes,"'","''")	& "'," _										
			    & "ModifiedBy="  	& sLogid						& ", " _
			    & "ModifiedDate='"	& modifiedDate					& "' "
								
		'check for special existance of default severity  
		if len(optSevDefault) > 0 then mySQL = mySQL & ",Severity=" & optSevDefault & " " else mySQL = mySQL & ",Severity=NULL "

		'check for special existance of dates  
		if len(optDirectiveID) > 0 then mySQL = mySQL & ",DirectiveID=" & optDirectiveID & " " else mySQL = mySQL & ",DirectiveID=NULL "
				
		'finalize update query
		mySQL = mySQL & "WHERE  CategoryID=" & recId & " AND CustomerID='" & customerID & "' "  

'response.redirect "../error/default.asp?errMsg=" & server.URLEncode(mySQL)
			
		'execute
		set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

		'delete existing records from Category_IssueType
		mySQL = "DELETE FROM Category_IssueType " _
			  & "WHERE CategoryID=" & recId & " "
		set rs = openRSexecuteDialog(mySQL)

		'add all issue types selected by user
		for expI = 0 to UBound(arrType)
			mySQL = "INSERT INTO Category_IssueType (" _
				  & "CategoryID,IssueTypeID,ModifiedBy,ModifiedDate" _
				  & ") VALUES (" _
				  & "'" & recId & "'," & arrType(expI) & "," & sLogid & ",'" & modifiedDate & "' " _
				  & ")"
			set rs = openRSexecuteDialog(mySQL)			
		next

		session(session("siteID") & "okMsg") = "Category '" & optName & "' has been saved successfully."
		
	'add new category			
	elseif action = "add" then
	
		'save main fields
		fieldsSQL = "INSERT INTO Category (" _
			  	  & "CustomerID, Name, Sub, Sev1, Sev1Weekends, Sev2, Sev2Weekends, Sev3, Sev3Weekends, Active, LanguageCD, Notes, ModifiedBy, ModifiedDate"
			  
		'check for empty dates	      
		if len(optSevDefault) > 0 then fieldsSQL = fieldsSQL & ",Severity"		  		
		if len(optDirectiveID) > 0 then fieldsSQL = fieldsSQL & ",DirectiveID"		  
		fieldsSQL = fieldsSQL & ") "			  
			  
		valueSQL = " VALUES (" _
			  & "'" & uCase(customerID) 			& "'," _
			  & "N'" & replace(optName,"'","''") 	& "'," _
			  & "N'" & replace(optSub,"'","''") 		& "'," _			  
			  & " " & optSeverity1 					& ", " _
			  & " " & optSeverity1Weekends 			& ", " _
			  & " " & optSeverity2 					& ", " _
			  & " " & optSeverity2Weekends 			& ", " _
			  & " " & optSeverity3 					& ", " _			  
			  & " " & optSeverity3Weekends 			& ", " _
			  & "'" & "Y" 							& "'," _
			  & "'" & replace(optLanguage,"'","''")	& "'," _
			  & "N'" & replace(optNotes,"'","''")	& "'," _
			  & " " & sLogid 						& ", " _
			  & "'" & modifiedDate 					& "' "

		'check for empty values
		if len(optSevDefault) > 0 then valueSQL = valueSQL & "," & optSevDefault & " "
		if len(optDirectiveID) > 0 then valueSQL = valueSQL & "," & optDirectiveID & " "
		valueSQL = valueSQL & ") "

		'set identity to pull back newly added record new record id is returned to newID
		mySQL = "SET NOCOUNT ON; " & fieldsSQL & valueSQL & " SELECT SCOPE_IDENTITY() AS newID;"
		set rs = openRSexecuteDialog(mySQL)		
		recId = rs("newID")

		'add all issue types selected by user
		for expI = 0 to UBound(arrType)
			mySQL = "INSERT INTO Category_IssueType (" _
				  & "CategoryID,IssueTypeID,ModifiedBy,ModifiedDate" _
				  & ") VALUES (" _
				  & "'" & recId & "'," & arrType(expI) & "," & sLogid & ",'" & modifiedDate & "' " _
				  & ")"
			set rs = openRSexecuteDialog(mySQL)			
		next	

		session(session("siteID") & "okMsg") = "Category was added."
			
	end if			
	%>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
	
	<script type='text/javascript'>
		topWin.parent.pageReload('<% =lCase(customerID) %>');
		SimpleModal.close();
    </script>	
	
<%
elseif action = "del" then

	'Mark for deletion from Categories_Issues
	mySQL = "UPDATE Category Set " _
			& "Active='N' " _	
			& "WHERE  CategoryID=" & recId & " AND CustomerID='" & customerID & "' "   
	set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)	
	
	session(session("siteID") & "okMsg") = "Category was deleted."
	response.redirect "../profiles/category.asp?cid=" & customerID	

elseif action = "add" then

	optSeverity1 = 1
	optSeverity2 = 3
	optSeverity3 = 14
		
end if

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Category</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>
      
	<!-- HTML List/Table resorting functions (right now own) -->
	<!-- Example usage:
    		var table = document.getElementById('table-1');
			var tableDnD = new TableDnD();
			tableDnD.init(table);  -->
	<script type="text/javascript" src="../scripts/javascript/orderList.js"></script>

	<!--JQuery required components -->
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />
    
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

	<!-- JQuery SPINNER https://github.com/btburnett3/jquery.ui.spinner/ -->
    <link type="text/css" rel="stylesheet" href="../_jquery/spinner/ui.spinner.css" />
	<script type="text/javascript" src="../_jquery/spinner/ui.spinner.js"></script>
    
</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="popup_config_category.asp" style="padding:0px; margin:0px;">
    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/move_to_folder.png" title="Categories" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Category</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%; padding-bottom:7px; margin-bottom:7px;">
       			<fieldset class="infoBox" style="padding:0px; margin:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Configure selected category below.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Category table -->
    <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">
    
        <%
		if lCase(action) = "edit" then
			
			'CCI Admin/DBAdmin (bryan or steve)
			if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then			
				mySQL = "SELECT Category.*  " _
					  & "	FROM Category " _
					  & "	WHERE CategoryID=" & recId & " " _
					  & "	ORDER BY Name "			  				  
			'everyone else
			else
				mySQL = "SELECT Category.* " _
					  & "	FROM Category " _
					  & "	WHERE EXISTS ( " _
					  & "			SELECT vwLogins_IssueType.CustomerID, vwLogins_IssueType.LogID " _
					  & "				FROM vwLogins_IssueType " _
					  & "				WHERE (vwLogins_IssueType.CustomerID=Category.CustomerID) AND (vwLogins_IssueType.LogID=" & sLogid & ") ) " _
					  & "		  AND (CategoryID=" & recId & ") " _
					  & "	ORDER BY Name "			  				  
			end if				  
			'open category table...				  
	        set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
			
			if rs.eof then							
				response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Category ID.")	
			end if		
			
			customerID = rs("customerid")
			optName = rs("name")
			optSub = rs("sub")
			optDirectiveID = rs("directiveid")
			
			optSeverity1 = rs("sev1")
			if len(optSeverity1) <=0 or isNull(optSeverity1) then optSeverity1 = 0
			optSeverity1Weekends = rs("sev1weekends")
			
			optSeverity2 = rs("sev2")
			if len(optSeverity2) <=0 or isNull(optSeverity2) then optSeverity2 = 0			
			optSeverity2Weekends = rs("sev2weekends")
			
			optSeverity3 = rs("sev3")
			if len(optSeverity3) <=0 or isNull(optSeverity3) then optSeverity3 = 0			
			optSeverity3Weekends = rs("sev3weekends")
			
			optNotes = rs("notes")			
			optSevDefault = rs("severity")
			optLanguage = rs("languagecd")
			
		end if
        %>

		<tr>
       		<td class="formLabel" >Name:</td>
       		<td align=left >
				<input name="optName" class="inputLong" style="width:285px;" value="<% =optName %>" maxlength="150" />
       		</td>
   		</tr>

		<tr>
       		<td class="formLabel" >Secondary:</td>
       		<td align=left >
				<input name="optSub" class="inputLong" style="width:285px;" value="<% =optSub %>" maxlength="150" />
       		</td>
   		</tr>


		<!-- START Issue Type Table -->    
		<tr>
        	<td class="formLabel">Issue Types:</td>    
            <td align=left> 

				<div id="field_list_container">
            		<div id="field_list" style="width:285px; border: 1px solid #ccc; padding: 2px; height:40px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">
						<!-- table used for javascript slider -->
						<table id="table:field:list" width="95%" border="0" cellspacing="0" cellpadding="0">
                    		<%
							dim arrIssues, checkIssue
														
							'find all issue types already assigned to category
							if lCase(action) = "edit" then
								mySQL = "SELECT Category_IssueType.CategoryID, Category_IssueType.IssueTypeID " _
									  & "	FROM Category_IssueType " _
									  & "	WHERE Category_IssueType.CategoryID=" & recId & " "
								set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
								do while not rs.eof
									arrIssues = arrIssues & "|" & rs("IssueTypeID")
									rs.movenext
								loop
								arrIssues = arrIssues & "|"
								call closeRS(rs)
							end if
							
							'find all issue types for this customer
							mySQL = "SELECT Customer_IssueType.CustomerID, Customer_IssueType.IssueTypeID, IssueType.Name " _
								  & "	FROM Customer_IssueType INNER JOIN IssueType ON Customer_IssueType.IssueTypeID =IssueType.IssueTypeID " _
								  & "	WHERE Customer_IssueType.CustomerID='" & customerID & "' AND Active='Y' " _
								  & "	ORDER BY IssueType.Name "													
							set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
							do while not rs.eof
								if instr(arrIssues, "|" & rs("IssueTypeID") & "|") > 0 then
									checkIssue = " checked "
								else
									checkIssue = " "
								end if								
								response.write("<tr><td class=""clearFormat"" style=""border:none; padding:0px; margin0px;"" align=""left"">")                                                    
								response.write("	<div id=""field_list_item_Name"" class=""checkItem"">")											
								response.write("		<input name=""list_checkBox"" id=""field_list_checkBox"" type=""checkbox"" style=""cursor:default;"" value=""" & rs("IssueTypeID") & """ " & checkIssue & " />")
								response.write("		<label for=""field_list_checkBox"" style=""cursor:move;"" onclick=""javascript:return false;"">" & rs("name") & "</label>")
								response.write("	</div>")    
								response.write("</td></tr>")          

								rs.movenext
							loop
							call closeRS(rs)
                            %>
						</table>
						<!-- Initiate listOrder.js for moving fields in order -->
						<script type="text/javascript">
                   			var table = document.getElementById('table:field:list');
                        	var tableDnD = new TableDnD();
                         	tableDnD.init(table);
                      	</script>
               		</div>
           		</div>

        		<div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frm','field_list_checkBox','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frm','list_checkBox',''); return false;">None</a></div>

 			</td>

     	</tr>                       
		<!-- STOP Issue Type Table -->
        
        <!-- Laguange -->
		<tr>
       		<td class="formLabel">Language:</td>
           	<td align=left nowrap style="padding-bottom:15px;">
           		<select name="optLanguage" id="optLanguage" size=1 style="width:285px;">
              		<option value="en-US" selected="selected">-- Select --</option>
                   		<%						
						mySQL="SELECT Language.LanguageCD, " _
							& "	CASE WHEN (CHARINDEX('-',LanguageName) > 0) THEN  LTRIM(SUBSTRING(LanguageName,CHARINDEX('-',LanguageName)+1,LEN(LanguageName)- CHARINDEX('-',LanguageName))) ELSE LanguageName END as [Language] " _
							& "	FROM MCR_Config INNER JOIN Language ON MCR_Config.LanguageCD = Language.LanguageCD " _
							& "	WHERE MCR_Config.CustomerID = '" & customerID & "' " _
							& "	ORDER BY Language.LanguageName "						
						set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                       	do while not rs.eof								
                   			Response.Write "<option value=""" & rs("LanguageCD") & """ " & checkMatch(optLanguage,rs("LanguageCD")) & ">" & rs("Language") & "</option>"
                           	rs.movenext
                        loop
                   		call closeRS(rs)
                       	%>
           		</select>
      		</td>
		</tr>                    
        
        <!-- Default Severity -->
		<tr>
       		<td class="formLabel">Default Severity:</td>
           	<td align=left nowrap style="padding-bottom:15px;">
           		<select name="optSevDefault" id="optSevDefault" size=1 style="width:285px;">
               		<option value="" selected="selected">-- Select --</option>					
                    <option value="3" <% response.write(checkMatch(optSevDefault,"3")) %>>Severity 3-Low</option>
                    <option value="2" <% response.write(checkMatch(optSevDefault,"2")) %>>Severity 2-Med</option>
                    <option value="1" <% response.write(checkMatch(optSevDefault,"1")) %>>Severity 1-High</option>                                        
           		</select>	                                                                                          
       		</td>
		</tr>                    

        <!-- STOP Default Severity -->        
                
        
        <!-- Severity Directives for CCI admins ONLY -->
        <% if cLng(session(session("siteID") & "adminLoggedOn")) <= 3 then %>

			<!-- Severity Details -->
            <tr>
                <td class="formLabel" style="padding-top:12px;">Severity 1:</td>
                <td align=left>		     
                     	Call back days <input name="optSeverity1" id="optSeverity1" class="inputShort" value="<% =optSeverity1 %>" maxlength="255" style="margin-right:15px; width:35px;"/>
						<label for="optSeverity1Weekends" style="padding-right:2px;">Omit Weekends?</label><input name="optSeverity1Weekends" id="optSeverity1Weekends" type="checkbox" style="cursor:default;" value="-1" <% if optSeverity1Weekends="True" then response.write("checked") %> />
                </td>
            </tr>
			<script type="text/javascript">
                jQuery().ready(function($) {
                    $('#optSeverity1').spinner({ min: 0, max: 99, step: 1, increment: 'fast' });			
                });
			</script>

            <tr>
                <td class="formLabel" style="padding-top:12px;">Severity 2:</td>
                <td align=left>	           		     
                     	Call back days <input name="optSeverity2" id="optSeverity2" class="inputShort" value="<% =optSeverity2 %>" maxlength="255" style="margin-right:15px; width:35px;"/>
						<label for="optSeverity2Weekends" style="padding-right:2px;">Omit Weekends?</label><input name="optSeverity2Weekends" id="optSeverity2Weekends" type="checkbox" style="cursor:default;" value="-1" <% if optSeverity2Weekends="True" then response.write("checked") %> />
                </td>
            </tr>
			<script type="text/javascript">
                jQuery().ready(function($) {
                    $('#optSeverity2').spinner({ min: 0, max: 99, step: 1, increment: 'fast' });			
                });
			</script>

            <tr>
                <td class="formLabel" style="padding-top:12px;">Severity 3:</td>
                <td align=left>	           		     
                     	Call back days <input name="optSeverity3" id="optSeverity3" class="inputShort" value="<% =optSeverity3 %>" maxlength="255" style="margin-right:15px; width:35px;"/>
						<label for="optSeverity3Weekends" style="padding-right:2px;">Omit Weekends?</label><input name="optSeverity3Weekends" id="optSeverity3Weekends" type="checkbox" style="cursor:default;" value="-1" <% if optSeverity3Weekends="True" then response.write("checked") %> />
                </td>
            </tr>
			<script type="text/javascript">
                jQuery().ready(function($) {
                    $('#optSeverity3').spinner({ min: 0, max: 99, step: 1, increment: 'fast' });			
                });
			</script>
        
        	<!-- Directive Link -->
			<tr>
         		<td class="formLabel">Link Directive:</td>
             	<td align=left nowrap style="padding-bottom:15px;">
               		<select name="optDirectiveID" id="optDirectiveID" size=1 style="width:285px;">
                   		<option value="" selected="selected">-- Select --</option>
                       		<%
							mySQL="SELECT DirectiveID, Name " _
								& "		FROM Directive " _
								& "	  	WHERE CustomerID = '" & customerID & "' " _
								& "ORDER By SortOrder, Name "
							set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                           	do while not rs.eof								
                       			Response.Write "<option value=""" & rs("directiveid") & """ " & checkMatch(optDirectiveID,rs("directiveid")) & ">" & rs("name") & "</option>"
                               	rs.movenext
                            loop
                       		call closeRS(rs)
                           	%>
               		</select>	                                                                                          
       			</td>
   			</tr>                    
            
            

            <tr>
                <td colspan="2" align="left">
                
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                		<tr>
                        	<td class="clearFormat">
			                    <div style="color:#35487B; font-weight:bold; padding:3px;">Notes/Description</div>
    	    		            <textarea name="optNotes" id="optNotes" style="width:98%; height:30px; " ><%=server.HTMLEncode(optNotes & "")%></textarea>
        					</td>
                        </tr>
                    </table>                        
                
                 </td>
            </tr>
                    
        <% else %>

            <tr>
                <td colspan="2" align="left">
                
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                		<tr>
                        	<td class="clearFormat">
			                    <div style="color:#35487B; font-weight:bold; padding:3px;">Notes/Description</div>
    	    		            <textarea name="optNotes" id="optNotes" style="width:98%; height:150px; " ><%=server.HTMLEncode(optNotes & "")%></textarea>
        					</td>
                        </tr>
                    </table>                        
                
                 </td>
            </tr>
        
        	<input name="optDirectiveID" type="hidden" value="<% =optDirectiveID %>">
            <input name="optSeverity1" type="hidden" value="<% =optSeverity1 %>">
            <input name="optSeverity1Weekends" type="hidden" value="<% =optSeverity1Weekends %>">
            <input name="optSeverity2" type="hidden" value="<% =optSeverity2 %>">
            <input name="optSeverity2Weekends" type="hidden" value="<% =optSeverity2Weekends %>">
            <input name="optSeverity3" type="hidden" value="<% =optSeverity3 %>">
            <input name="optSeverity3Weekends" type="hidden" value="<% =optSeverity3Weekends %>">            
            
		<% end if %>        

		<tr>
           	<td colspan="2" align=left nowrap style="padding:0px; margin:0px;">&nbsp;</td>
       	</tr>
        
    </table>
	<!-- STOP Category table -->

	<input name="recid" type="hidden" value="<% =recid %>">
    <input name="action" type="hidden" value="<% =action %>">    
    <input name="save" type="hidden" value="save">
    <input name="cid" type="hidden" value="<% =customerid %>">
        
	<!-- START Buttons -->
	<div style="float:right;"> 	       		
        <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); document.forms['frm'].submit();"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Buttons -->

</form>
       
</body>
</html>

<%
'close database connection
call closeDB()
%>


