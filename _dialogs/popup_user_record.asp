<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%
'Database
dim mySQL, cn, rs, field, count

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim pageView : pageView = Request.QueryString("pageView")

dim idLog
idLog = trim(Request.QueryString("recId"))
if idLog = "" then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid User ID.")
end if

'--------------------------------------------------
'get user record...
'--------------------------------------------------	
mySQL = "SELECT Logins.*, Customer.Name " _
	  & "	FROM Logins INNER JOIN Customer ON Logins.CustomerID = Customer.CustomerID " _
	  & "	WHERE  LogID=" & idLog & " "				
'open recordset   			  
set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
if rs.eof then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid User ID.")
end if
'--------------------------------------------------	

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Users &amp; Contacts</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    

	<style type="text/css">
    <!--
    .sectionHeader {
       padding:3px !important; 
	   margin:0px !important; 
	   background-color:#E2E2E2 !important; 
	   font-weight:bold !important; 
	   border-top:1px solid #5B5B5B !important; 
	   border-bottom:1px solid #5B5B5B !important;
       }
	 .printTable td {
		vertical-align:top;
		border-top:1px dotted #cccccc;
		padding-top:8px;
		padding-bottom:10px;
	 }	   

	.printTable .printLabel 			{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: none;}
	.printTable .printLabel:link 		{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: none;}
	.printTable .printLabel:link:hover 	{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: underline;}
	.printTable .printLabel:active 		{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: none;}
	.printTable .printLabel:visited 	{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: none;}
	.printTable .printLabel:hover 		{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: none;}
	 
	-->
    </style>

	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
   
</head>

	<body style="padding:20px;">

		<div style="text-align:left; margin-bottom:10px; float:left;">
    	   	<span style="color:#35487B; position:static; font-family:arial; font-size:14px; font-weight:bold; margin-bottom:10px;"><% =rs("Name") %></span>
	   	</div> 

        <!-- START Buttons -->
        <div style="text-align:right; padding-bottom:10px;">
        	<% if pageView = "issue" or pageView = "issue:00" then %>
	            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); topWin.parent.StartTheTimer(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>        
            <% else %>
	            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>        
            <% end if %>            
        </div>    
        <!-- STOP Buttons -->
        
        <!-- START Form Fields -->	                    
		<table id="table:details:general" width="100%" class="printTable" cellpadding="0" cellspacing="0">

            <tr>
                <td class="sectionHeader">User</td>
            </tr>                            
            
			<%
			response.write("<tr>")
			if len(rs("Title")) > 0 then
				response.write("	<td colspan=""2"" class=""printLabel"" style=""border-top:none; background-color:#FFFFC8;"">" & rs("FirstName") & " " & rs("LastName") & ", " & rs("Title") & "</td>")
			else
				response.write("	<td colspan=""2"" class=""printLabel"" style=""border-top:none; background-color:#FFFFC8;"">" & rs("FirstName") & " " & rs("LastName") & "</td>")			
			end if
			response.write("</tr>")
			%>

            <tr>
                <td>
	
                    <table id="table:details:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">
            
                        <tr <% if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then response.write("style=""display:none;""")%> >
                            <td class="formLabel" style="border-top:none;">Issue Delivery:</td>
                            <td align=left style="border-top:none;" nowrap>
                                <div style="float:left; width:150px; height:20px;">
                                    Priority: <% =rs("priority") %>
                                </div>                                    
                                <div style="height:20px;">
                                    Preference: <% =rs("delivery") %>
                                </div>
                            </td>                              
                        </tr>

                        <tr>                            
                            <td class="formLabel">Address:</td>
                            <td align=left nowrap><% =rs("address") %><br/><% =rs("city") %>,&nbsp;<% =rs("state") %>&nbsp;<% =rs("postalcode") %>&nbsp;</td>                              
                        </tr>

                        <tr>
                            <td class="formLabel">Number(s):</td>
                            <td align=left nowrap>                                                              
                                <div style="float:left; width:200px; height:20px;">
                                    Work: <% =rs("phone_work") %>
                                </div>                                    
                                <div style="height:20px;">
                                    Home: <% =rs("phone_home") %>
                                </div>
                                
                                <div style="float:left; width:200px; height:20px;">
                                    Cell: <% =rs("phone_cell") %>
                                </div>                                                   
                                <div style="height:20px;">
                                    Pager:<% =rs("phone_pager") %>
                                </div>
                                                                    
                                <div style="float:left; width:200px; height:20px;">
                                    Fax: <% =rs("phone_fax") %>
                                </div>                                                                                   
                                <div style="height:20px;">
                                    Other: <% =rs("phone_other") %>
                                </div>                                                           
                                                                                                   
                            </td>
                        </tr>
            
                        <tr>                            
                            <td class="formLabel">E-mail:</td>
                            <td align=left nowrap><% =rs("Email") %>&nbsp;</td>                              
                        </tr>
                                    
                    </table>
        
                    <table class="formTable" cellpadding="0" cellspacing="0" width="100%">
                        <tr <% if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then response.write("style=""display:none;""")%> >                            
                        	<td align="left" class="clearFormat">
                            	<div style="color:#35487B; font-weight:bold; padding:3px;">Notes</div>
								<div style="border:#CCC 1px solid; padding:5px; text-align:left; vertical-align:text-top; height:80px; overflow:auto; overflow-x: hidden; overflow-y: scroll;"><% =replace(rs("notes") & "",vbCRLF,"<br>") %>&nbsp;</div>
                           	</td>
                    	</tr>                    
                    </table>        
        
        		</td>
            
        	</tr>

			<%
			call closeRS(rs)
            %>        

		</table>
		<!-- END Form Fields -->
                       

		<!-- START END of issue notice -->  
		<table width="100%" class="printTable" style="margin-bottom:5px;" cellpadding="0" cellspacing="0">                                   
			<tr>
				<td class="sectionHeader" style="text-align:center">- End of Record -</td>
			</tr>
		</table>
		<!-- STOP END of issue notice -->                          

		<div style="text-align:left; float:left;">&nbsp;</div> 
        
        <!-- START Buttons -->
        <div style="text-align:right; padding-bottom:10px;">
        	<% if pageView = "issue" or pageView = "issue:00" then %>
	            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); topWin.parent.StartTheTimer(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>        
            <% else %>
	            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>        
            <% end if %>            
        </div>    
        <!-- STOP Buttons -->

	</body>
</html>

<%
'close database connection
call closeDB()
%>

