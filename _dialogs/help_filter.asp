<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs

'Work Fields
dim I
dim item
dim count
dim pageSize
dim totalPages
dim showArr
dim sortField
dim sAddress

dim curPage
dim showPhrase
dim showField
dim showStart

'Open Database Connection
call openDB()

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Grid Column Filters</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />

	<!-- HTML Table Filter Generator http://tablefilter.free.fr/ -->
    <link rel="stylesheet" type="text/css" href="../_tablefilter/filtergrid.css"/>
	<script type="text/javascript" src="../_tablefilter/tablefilter_all.js"></script>
	<script type="text/javascript" src="../_tablefilter/sortabletable.js"></script>
    <script type="text/javascript" src="../_tablefilter/tfAdapter.sortabletable.js"></script>        

	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
    
</head>

<body style="padding:20px;">

	<div style="text-align:left; padding-bottom:10px; float:left;">
   		<img src="../_images/icons/24/database_link.png" title="Issues" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Grid Column Filters</span>
   	</div>                   

    <!-- START Buttons -->
    <div style="text-align:right;">
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>
    </div>    
    <!-- STOP Buttons -->

	<!-- START help box -->
	<table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
      <tr> 
        <td style="padding-bottom:10px;"><img src="../_images/help_filter_example.png"></td>
      </tr>
    </table>   
    
	<table id="helpTable" style="width:590px;" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <th>Operator</th>
            <th>Description</th>
            <th>Type</th>
            <th>Example</th>
          </tr>
          <tr> 
            <td>&lt;</td>
            <td>Values lower than search term are matched</td>
            <td>number</td>
            <td>&lt;1412</td>
          </tr>
          <tr> 
            <td>&lt;=</td>
            <td>Values lower than or equal to search term are matched</td>
            <td>number</td>
            <td>&lt;=1412</td>
          </tr>
          <tr> 
            <td>&gt;</td>
            <td>Values greater than search term are matched</td>
            <td>number</td>
            <td>&gt;1412</td>
          </tr>
          <tr> 
            <td>&gt;=</td>
            <td>Values greater than or equal to search term are matched</td>
            <td>number</td>
            <td>&gt;=1412</td>
          </tr>
          <tr> 
            <td>=</td>
            <td>Exact match search: only the whole search term(s) is matched</td>
            <td>string / number</td>
            <td>=Sydney</td>
          </tr>
          <tr> 
            <td>*</td>
            <td>Data containing search term(s) is matched (default operator)</td>
            <td>string / number</td>
            <td>*Syd</td>
          </tr>
          <tr> 
            <td>!</td>
            <td>Data that doesn't contain search term(s) is matched</td>
            <td>string / number</td>
            <td>!Sydney</td>
          </tr>
          <tr> 
            <td>{</td>
            <td>Data starting with search term is matched</td>
            <td>string / number</td>
            <td>{S</td>
          </tr>
          <tr> 
            <td>}</td>
            <td>Data ending with search term is matched</td>
            <td>string / number</td>
            <td>}y</td>
          </tr>
          <tr> 
            <td>||</td>
            <td>Data containing at least one of the search terms is matched</td>
            <td>string / number</td>
            <td>Sydney || Adelaide</td>
          </tr>
          <tr> 
            <td>&amp;&amp;</td>
            <td>Data containing search terms is matched</td>
            <td>string / number</td>
            <td>&gt;4.3 &amp;&amp; &lt;25.3</td>
          </tr>
		  <tr> 
            <td>[empty]</td>
            <td>Empty data is matched</td>
            <td>&nbsp;</td>
            <td>[empty]</td>
          </tr>
		  <tr> 
            <td>[nonempty]</td>
            <td>Data which is not empty is matched</td>
            <td>&nbsp;</td>
            <td>[nonempty]</td>
          </tr>
    </table>	
	<!-- STOP Search box -->
	
	<!-- START Buttons -->
    <div style="text-align:right; padding-bottom:10px; padding-top:10px;">
         <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>        
    </div>    
	<!-- STOP Buttons -->
    
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script language="javascript" type="text/javascript">   															
	//<![CDATA[	
	var tableProp = { sort: true, grid: false, alternate_rows: true, themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]} };
	//initiate table setup
	var tf1 = setFilterGrid("helpTable",tableProp);
	//]]>
</script>

