<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, pageSize

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim countString
dim optConfigure

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and programAdmin = "N" then
	'all users can edit their own report categories
	if lCase(request.querystring("field")) <> "reportscategory" and lCase(request.form("optField")) <> "reportscategory" then	
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
	end if
end if

dim pageView : pageView = Request.QueryString("pageView")

'Get option type 
dim optType : optType = Request.Form("optType")
if len(optType) = 0 then
	optType = Request.QueryString("type")
end if
if lCase(optType) <> "udf" and _
   lCase(optType) <> "customers" and _
   lCase(optType) <> "system" and _
   lCase(optType) <> "logins" then	   
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Config Type.")	
end if

'Get option field
dim optField : optField = Request.Form("optField")
if len(optField) = 0 then
	optField = Request.QueryString("field")
end if

'Get option form name
dim optForm : optForm = Request.Form("optForm")
if len(optForm) = 0 then
	optForm = Request.QueryString("form")
end if


'set sort order on save
if request.form("action") = "save" then

	dim newList
	
	'get new config values
	optType = trim(Request.Form("optType"))
	optField = trim(Request.Form("optField"))		
	optForm = trim(Request.Form("optForm"))			
	optConfigure = trim(Request.Form("optConfigure"))

	newList = optConfigure
	newList = replace(newList, chr(13), "|")
	newList = replace(newList, chr(10), "")

	'USER DEFINED FIELDS...and...CUSTOMER SETTINGS
	if (lCase(optType) = "udf") or (lCase(optType) = "customers") then
		mySQL="Update Customer Set " & optField & "='" & replace(optConfigure,"'","''") & "' " _
			& "WHERE  CustomerID='" & customerID & "' "   
		set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	'LOGINS fields
	elseif lCase(optType) = "logins" then
		mySQL="Update Logins Set " & optField & "='" & replace(optConfigure,"'","''") & "' " _
			& "WHERE  Logid=" & sLogid & " AND CustomerID='" & customerID & "' "   
		set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	'SYSTEM fields
	elseif lCase(optType) = "system" then
		mySQL="Update Config Set configValLong='" & replace(optConfigure,"'","''") & "' " _
			& "WHERE  configVar='" & optField & "' "   
		set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	end if					
	
	session(session("siteID") & "okMsg") = "Option has been saved successfully."
	
	%>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
	
	<% if lCase(pageView) = "reports" or lCase(pageView) = "profile" then %>
		<script type='text/javascript'>
            //reconfig option
            topWin.parent.configDropDownOption("<% =optForm %>","<% =newList %>");
            //close window
            SimpleModal.close();
        </script>	
	<% elseif lCase(pageView) = "userfields" then %>
		<script type='text/javascript'>
            //reload window
			topWin.parent.pageReload('<% =lCase(customerID) %>');
            //close window
            SimpleModal.close();
        </script>	
	<% else %>    
		<script type='text/javascript'>
            //reconfig option
            topWin.parent.configDropDownOption("<% =optForm %>","<% =newList %>");
            //restart autosave timer
            topWin.parent.StartTheTimer();
            //close window
            SimpleModal.close();
        </script>	
    <% end if %>
    
    	
<%
else
	'nothing...	
end if
%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Issue List Sort Order</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="popup_config_options.asp?pageView=<% =pageView %>" style="padding:0px; margin:0px;">
    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/setting_tools.png" title="Issues" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Configure</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Configure selected option below.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Categories table -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    
        <%
		optConfigure = ""
		
		'USER DEFINED FIELDS...
		if (lCase(optType) = "udf") or (lCase(optType) = "customers") then
			mySQL="SELECT " & optField & " " _
				& "FROM   Customer " _
				& "WHERE  CustomerID='" & customerID & "' "                                       

		'LOGINS fields...	
		elseif lCase(optType) = "logins" then
			mySQL="SELECT " & optField & " " _
				& "FROM   Logins " _
				& "WHERE  Logid=" & sLogid & " AND CustomerID='" & customerID & "' "                                       

		'SYSTEM fields...	
		elseif lCase(optType) = "system" then
			mySQL="SELECT configValLong " _
				& "FROM   Config " _
				& "WHERE  configVar='" & optField & "' "

		end if		

		'Open recordset
		set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
		if not rs.eof then
			if lCase(optType) = "system" then
				optConfigure = rs("configValLong")
			else
				optConfigure = rs(optField)
			end if
		end if
		
        %>
        <tr>
            <td>
				<textarea name="optConfigure" id="optConfigure" style="width:98%; height:150px; margin-bottom:12px;" ><% =server.HTMLEncode(optConfigure & "") %></textarea>
            </td>
        </tr>        
        
    </table>
	<!-- STOP Categories table -->

	<input name="optType" type="hidden" value="<% =optType %>">
	<input name="optField" type="hidden" value="<% =optField %>">
	<input name="optForm" type="hidden" value="<% =optForm %>">    
    <input name="cid" type="hidden" value="<% =customerid %>">    
    <input name="action" type="hidden" value="save">
    
	<!-- START Buttons -->
	<div style="float:left;"> 	       		
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); sortConfig('optConfigure'); return false;"><span class="sort">Sort</span></a>    
	</div>        
    <% if lCase(pageView) = "reports" or lCase(pageView) = "profile" or lCase(pageView) = "userfields" then %>
        <div style="float:right;"> 	       		
            <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); document.forms['frm'].submit();"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
        </div>    
   	<% else %>
        <div style="float:right;"> 	       		
            <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); document.forms['frm'].submit();"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); topWin.parent.StartTheTimer();"><span class="cancel"><u>C</u>lose</span></a>
        </div>    
    <% end if %>    
	<!-- STOP Buttons -->

   </form>
        
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script>
	function sortConfig(obj){
		//sort textarea alphabetically A-Z
		var o = document.getElementById(obj);
		var v = o.value;
		var s = replaceCarriageReturn(v,"|")
		var temp = s.split("|");
		temp.sort();
		var result = "";
		var i;
		for(i=0; i<temp.length; i++) {
			if (temp[i].length>1) {
				result += temp[i] + "\n";
			}
		}
		o.value = result;
	}
</script>


