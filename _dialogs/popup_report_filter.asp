<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%
'Database
dim mySQL, cn, rs, count

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim pageView : pageView = "reports:edit"

'Issue Fields on DETAILS tab
dim optID
dim optName
dim optDataset
dim optTable
dim optType
dim optFields
dim optFilter
dim optSQLValue

dim listColumn, listFriendly

'Work Fields
dim action
dim idReport

dim field, arrFields, arrColumn, arrFriendly

dim ModifiedDate : ModifiedDate = Now()

'Check User Security
'if trim(session(session("siteID") & "viewStats")) = "N" then
'	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("You are not authorized to view this page.")	
'end if


'Get idIssue
idReport = trim(Request.QueryString("recId"))
if len(idReport) = 0 then
	idReport = trim(Request.Form("idReport"))
end if
if idReport = "" then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Report ID.")
end if


'Get action
action = trim(Request.QueryString("action"))
if len(action) = 0 then
	action = trim(Request.Form("action"))
end if
action = lCase(action)
if  action <> "edit" _
and action <> "save"  then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if


'******************************
'build filter XML and save
if action = "save" then	
	
	dim arrField, arrExp, arrValue, expI, tmpValue
		
	optFilter = "<?xml version='1.0' encoding='ISO-8859-1'?>"
	optFilter = optFilter & "<filters>"

	'------------------------------------------------
	'find all form elements in filter box
	'------------------------------------------------	
	'discover filtered fields
	arrField = split(Request.Form("idQueryField"),",")
	'discover operators on each filtered field
	arrExp = split(Request.Form("idQueryExp"),",")
	'discovery query values entered by user
	for i = 1 To request.form("idQueryValue").Count 
		arrValue = arrValue & request.form("idQueryValue")(i) & "|"
  	next 
	if right(arrValue,1) = "|" then arrValue = left(arrValue,len(arrValue)-1)
	arrValue = split(arrValue,"|")
	
	'------------------------------------------------
	'Loop through filter list and add one by one to XML
	'------------------------------------------------	
	for expI = LBound(arrField) to UBound(arrField)	
		'Clean up value and replace where needed
		if len(trim(Request.Form("idQueryValue"))) > 0 then
			tmpValue = trim(arrValue(expI))
			tmpValue = replace(tmpValue,"*","%")
			tmpValue = replace(tmpValue,"""","'")
		else
			tmpValue = ""
		end if
		'put string together
		optFilter = optFilter & "<filter>"
		optFilter = optFilter & "<field>" & trim(arrField(expI)) & "</field>"
		optFilter = optFilter & "<operator>" & Server.HTMLEncode(trim(arrExp(expI))) & "</operator>"		
		optFilter = optFilter & "<value>" & tmpValue & "</value>"
		optFilter = optFilter & "</filter>"		
	next	
	'close XML
	optFilter = optFilter & "</filters>"
	'------------------------------------------------

	'------------------------------------------------
	'update filter record
	'------------------------------------------------	
	'report/shortcut filter
	if idReport > 0 then
		mySQL = "UPDATE Reports SET " _
			  & "QueryFilter='"		& replace(optFilter,"'","''")		& "'," _		  
			  & "ModifiedBy="		& sLogid							& ", " _
			  & "ModifiedDate='"    & ModifiedDate					& "' "		  		    		  		  		  	
		'finalize update query
		mySQL = mySQL & "WHERE ReportID = " & idReport
									
	'issue list
	elseif idReport = 0 then
		mySQL = "UPDATE Logins SET " _
			  & "WhereClause='"		& replace(optFilter,"'","''")	& "'," _
			  & "ModifiedBy="		& sLogid						& ", " _
			  & "ModifiedDate='"    & ModifiedDate				& "' "		  					  
		'finalize update query
		mySQL = mySQL & "WHERE LogID = " & sLogid
	
	end if
	'execute update
	set rs = openRSexecuteDialog(mySQL)
		
	session(session("siteID") & "okMsg") = "Filter has been saved successfully."	
	%>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
    	
	<script type='text/javascript'>
		//topWin.parent.pageReload('<% =idReport %>');
		topWin.parent.pageReload();
		SimpleModal.close();
    </script>	
	
<%	
end if


'Get Issue Record... idReport 0 == Issue List on home page
if idReport > 0 then

	'get filter record
	mySQL = "SELECT Reports.ReportID, Dataset.Name AS Dataset, Dataset.[Table], Dataset.Type, Reports.Name, " _
		  & "       Reports.QueryFields, Reports.QueryFilter, Reports.SQL " _
	  	  & "FROM   Reports INNER JOIN " _
		  & "		Dataset ON Reports.DatasetID = Dataset.DatasetID " _
		  & "WHERE (Reports.ReportID = " & idReport & ") AND (Reports.LogID=" & sLogid & ") AND (Reports.CustomerID = '" & sCustomerID & "') "					
	'open recordset
	set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	
	if rs.eof then
		response.redirect "../reports/reports.asp"
	else
		optID			= rs("ReportID")
		optName			= rs("Name")
		optDataset		= rs("Dataset") & ""	
		optTable		= rs("Table") & ""				
		optType			= rs("Type") & ""
		optFields		= rs("QueryFields") & ""
		optFilter		= rs("QueryFilter") & ""
		optSQLValue		= rs("SQL") & ""				
		
	end if
	call closeRS(rs)
		
'Get info for main issue list
'FINISH THIS FOR MODIFYING ISSUES.ASP 
elseif idReport = 0 then

	'get customer record
	mySQL="SELECT IssuesColumns, WhereClause, SortField, SortOrder " _
		& "       FROM Logins " _
	    & "		  WHERE LogID = " & sLogid 						
	set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	
	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("User account not found.")
	else
		optID			= 0
		optName			= "Issue List" 
		optDataset		= "Issues" 
		optTable		= userIssuesView '--> points to CRS/Issues table
		optType			= "Register"		
		optFields		= rs("IssuesColumns") & ""
		optFilter		= rs("WhereClause") & ""		
	end if
	call closeRS(rs)
	
	'make sure something is assigned to Issue Fields
	if len(optFields) <= 0 then
		optFields = "[Issue #], [Date], [Severity], [Status], [Location Name]"
	end if
	
end if

'Get fields for list box...these are the fields the use selects
if (len(optDataset) > 0 and optDataset > "") and (lCase(optType) <> "sql") then

	'get customer record
	mySQL="SELECT * " _
		& "   FROM " & optTable & " " _
	    & "	  WHERE 1=2"								
	'set rs = openRSexecuteDialog(mySQL)
	set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	'loop through all columns
	for each field in rs.fields
		if lCase(field.name) = "logid" or lCase(field.name) = "viewedby" or lCase(field.name) = "customerid" or lCase(field.name) = "issuetypeid" or lCase(field.name) = "logidview" or inStr(lCase(field.name),"hierarchy_search") then
			'DO NOT ADD THIS FIELD...
		else
			listColumn = listColumn & "[" & field.name & "],"
			listFriendly = listFriendly & field.name & ","
		end if
	next
	
	'create array of available fields
	if right(listColumn,1) = "," then listColumn = left(listColumn,len(listColumn)-1)	
	arrColumn = split(listColumn,",")

	'create array of available fields WITHOUT "[ ]" brackets for display only	
	if right(listFriendly,1) = "," then listColumn = left(listFriendly,len(listFriendly)-1)	
	arrFriendly = split(listFriendly,",")

	'create array of fields already selected by user previously
	arrFields = split(optFields,",")

end if


dim labelX, labelTemp
                                                
' add fields ALREADY selected by user
for labelX = 0 to UBound(arrFields)  												  
	'temporary clean up for display only    
	labelTemp = arrFields(labelX)
	labelTemp = replace(labelTemp, "[","")
	labelTemp = replace(labelTemp, "]","")
	'change the display name...													
	if trim(lCase(labelTemp)) = "total" then
		labelTemp = "Total (sum)"													
	end if													                                          
next													
												
												
' add additional fields NOT selected by user
for labelX = 0 to UBound(arrColumn)												
	' make sure field was not already selected by user
	if inStr(optFields, arrColumn(labelX)) = 0 then		
		'change the display name...
		if trim(lCase(arrFriendly(labelX))) = "total" then
			labelTemp = "Total (sum)"
		else
			labelTemp = arrFriendly(labelX)
		end if
	end if
next

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Choose a Location</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- DHTML Calendar http://www.dhtmlx.com -->
	<link rel="stylesheet" type="text/css" href="../_calendar/dhtmlxCalendar/codebase/dhtmlxcalendar.css">
    <link rel="stylesheet" type="text/css" href="../_calendar/dhtmlxCalendar/codebase/skins/dhtmlxcalendar_simplegrey.css">
	<script type="text/javascript" src="../_calendar/dhtmlxCalendar/codebase/dhtmlxcommon.js"></script>
	<script type="text/javascript" src="../_calendar/dhtmlxCalendar/codebase/dhtmlxcalendar.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

    <!-- ONLY FOR suggest boxes and JSON data pulls, includes full JQuery library -->
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />
    <style>
        .ui-autocomplete-loading { background: white url('../_jquery/suggestbox/ui-anim_basic_16x16.gif') right center no-repeat; }
    </style>
    <style>
        .ui-autocomplete {
            max-height: 100px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
            /* add padding to account for vertical scrollbar */
            padding-right: 20px;
        }
        /* IE 6 doesn't support max-height
         * we use height instead, but this forces the menu to always be this tall
         */
        * html .ui-autocomplete {height: 100px;}
    </style>
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    <!-- ----------------------------------------- -->

</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="popup_report_filter.asp" style="padding:0px; margin:0px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/filter.png" title="Fi" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle"><% =optName %></span>
   	</div>                   

   	<input type="hidden" name="idReport" value="<% =optID %>">
    <input type="hidden" name="action" value="save">
	<input type=hidden name="optTable" id="optTable" value="<% =optTable %>">

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Modify selected filter using the form below.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Filter table -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>

			<!-- START filter section-->
            <td class="clearFormat" style="border:none; padding:0px;" align="left">									                                   
                                                                                         
				<!-- START field selection for filter -->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                	<tr>
						<td style="background-color:#C7CDE9; border:none; padding:7px;">
                        	<img src="../_images/icons/18/add.png" width="18" height="18" title="Add" align="absmiddle" style="margin-right:7px; cursor:pointer;" onClick="javascript:addQueryRow();" />
                            <select name="optQueryField" id="optQueryField" size=1 style="width:200px;">
                            	<%
                                'build dropdown of available fields for filter selection 999
                                for labelX = 0 to UBound(arrColumn)
                                   	response.write "<option value='" & arrColumn(labelX) & "'>" & replace(arrFriendly(labelX),"_"," ") & "</option>"
                                next
                                %>
                            </select>                                        
						</td>
                    </tr>
                </table>                                        
				<!-- STOP field selection for filter -->
                                                                        
				<!-- START WHERE clause filter -->                                                                        
                <table width="100%" class="formFilter" id="tblFilter" border="0" cellspacing="0" cellpadding="0">
                	<tbody id="tblFilterBody">
                                        
						<%
						'malformed XML needs ampersand "&" converted to "&amp;" to display correctly
						optFilter = replace(optFilter, " & ", " &amp; ")
										
						'prepare XML filter
						dim objPopCalendar
						dim objLst, subLst, subLstNext, i
						dim xmlDoc : set xmlDoc = Server.CreateObject("Microsoft.XMLDOM")												
						xmlDoc.async = False
						xmlDoc.loadXml(optFilter)
						set objLst = xmlDoc.getElementsByTagName("filter")
						for i = 0 to objLst.Length - 1						
							set subLst = objLst.item(i)											
							'date field go ahead and get next values
							if inStr(lCase(subLst.childNodes(0).text),"date") > 0 then
								set subLstNext = objLst.item(i+1)
							end if											
                            %>					
                            <tr id="rowFilter_<% =i %>">
                            	<td width="50">                                                       
									<div style="white-space:nowrap;">                                                   
                                    	<%
										'no date, create singe hidden field
										if inStr(lCase(subLst.childNodes(0).text),"date") = 0 then
                                           	response.write("<input type=hidden name=""idQueryField"" id=""idQueryField"" value=""" & subLst.childNodes(0).text & """>")
														
										'date field filtered on, create two (2) hidden fields
										else															
                                           	response.write("<input type=hidden name=""idQueryField"" id=""idQueryField"" value=""" & subLst.childNodes(0).text & """>")
                                           	response.write("<input type=hidden name=""idQueryField"" id=""idQueryField"" value=""" & subLstNext.childNodes(0).text & """>")																													
										end if                                                            

										'temporary clean up for display only    
										labelTemp = subLst.childNodes(0).text
										labelTemp = replace(labelTemp, "[","")
										labelTemp = replace(labelTemp, "]","")														
										%>
                                        <img src="../_images/icons/18/cancel.png" width="18" height="18" title="Delete" align="absmiddle" style="margin-right:7px; cursor:pointer;" onClick="javascript:delQueryRow('rowFilter_<% =i %>','<% =labelTemp %>','<% =subLst.childNodes(0).text %>');"/>
										<!-- field name here -->
										<% =replace(labelTemp,"_"," ")  %>                                                                                               

                                    </div>
                                </td>
                              	<td width="125">
                              		<%
									if inStr(lCase(subLst.childNodes(0).text),"date") = 0 then
                                      %>
                                      <select name="idQueryExp" id="idQueryExp_<% =i %>" style="width:125px;" onChange="javascript:changeElementStyle(this.id,this.value);">								
                                    	<option value="=" 			<%=checkMatch(subLst.childNodes(1).text,"=") %>>equals to</option>
                                        <option value="<>" 			<%=checkMatch(subLst.childNodes(1).text,"<>") %>>does not equal to</option>
                                        <option value="<=" 			<%=checkMatch(subLst.childNodes(1).text,"<=") %>>less than</option>
                                        <option value=">=" 			<%=checkMatch(subLst.childNodes(1).text,">=") %>>greater than</option>
                                        <option value="like" 		<%=checkMatch(subLst.childNodes(1).text,"like") %>>contains</option>
                                        <option value="not like" 	<%=checkMatch(subLst.childNodes(1).text,"not like") %>>does not contain</option>
                                        <option value="is null" 	<%=checkMatch(subLst.childNodes(1).text,"is null") %>>is null</option>
                                        <option value="is not null"	<%=checkMatch(subLst.childNodes(1).text,"is not null") %>>is not null</option>
                                        <option value="in" 			<%=checkMatch(subLst.childNodes(1).text,"in") %>>is one of</option>
                                        <option value="not in"		<%=checkMatch(subLst.childNodes(1).text,"not in") %>>is not one of</option>
                                      </select>                                                    
									  <%
									else
									  %>
                                      <select name="idQueryExp" style="width:125px; display:none;">								
                                      	<option value=">=" <%=checkMatch(subLst.childNodes(1).text,">=") %>>greater than</option>
                                      </select>
                                      <select name="idQueryExp" style="width:125px; display:none;">								
                                      	<option value="<=" <%=checkMatch(subLst.childNodes(1).text,"<=") %>>less than</option>
                                      </select>          
                                      <select name="idNOT-USED" style="width:125px;">								
                                      	<option value="">between</option>
                                      </select>                                                                                                            
									  <%
									end if
									%>
                                </td>
                                <td>
                                	<%
									'input boxes already selected by user
									if inStr(lCase(subLst.childNodes(0).text),"date") = 0 then
													
										'set height if IN or NOT IN operator is used
										'if (subLst.childNodes(1).text = "in") or (subLst.childNodes(1).text = "not in") then
										'	response.write("<textarea name=""idQueryValue"" id=""idQueryValue_" & i & """ style=""overflow-y: auto; height: 42px; width:150px;"">" & subLst.childNodes(2).text & "</textarea>")
                                        'else
										'	response.write("<textarea name=""idQueryValue"" id=""idQueryValue_" & i & """ style=""overflow-y: hidden; height: 13px; width:150px;"">" & subLst.childNodes(2).text & "</textarea>")														
										'end if

										'set height if IN or NOT IN operator is used
										if (subLst.childNodes(1).text = "in") or (subLst.childNodes(1).text = "not in") then
											response.write("<textarea name=""idQueryValue"" id=""idQueryValue_" & i & """ style=""overflow-y: auto; height: 42px; width:150px;"">" & subLst.childNodes(2).text & "</textarea>&nbsp;")
                                     	else
											'hide element...using IS NULL or IS NOT NULL
											if subLst.childNodes(1).text = "is null" or subLst.childNodes(1).text = "is not null" then													
												response.write("<textarea name=""idQueryValue"" id=""idQueryValue_" & i & """ style=""overflow-y:hidden; height:13px; width:150px; display:none;"">" & subLst.childNodes(2).text & "</textarea>&nbsp;")
											'normal element
											else
												response.write("<textarea name=""idQueryValue"" id=""idQueryValue_" & i & """ style=""overflow-y:hidden; height:13px; width:150px;"">" & subLst.childNodes(2).text & "</textarea>&nbsp;")
											end if
										end if
														
										'------------------------------------------
										'START JSON filter builter
										'------------------------------------------
										'dont allow JSON filtering on these fields
										if instr(labelTemp,"Caller")<=0 and instr("Total,CRSID,Issue #,Time,Location Address,Location Postal Code,CustomerID,Summary,Details,Addendum,Resolution,CaseNotes,Notes,Outcome",labelTemp) <= 0 then
										%>
											<script>
                                               	$(function() {
                                                   function split( val ) {
                                                        return val.split( /,\s*/ );
                                                    }
                                                    function extractLast( term ) {
                                                         return split( term ).pop();
                                                     }                                                            
                                                     $("#idQueryValue_<% =i %>")
                                                     	// don't navigate away from the field on tab when selecting an item
                                                        .bind( "keydown", function( event ) {
                                                         	if ( event.keyCode === $.ui.keyCode.TAB &&
                                                              	$( this ).data( "autocomplete" ).menu.active ) {
                                                                  	event.preventDefault();
                                                             }
                                                        })
                                                        .autocomplete({
                                                          	source: function( request, response ) {
                                                               	$.getJSON( "../_jquery/suggestBox/json-data.asp?view=<% =server.URLEncode(optTable) %>&field=<% =server.URLEncode(labelTemp) %>", {
                                                                   	term: extractLast( request.term )
                                                             }, response );
                                                         },
                                                         search: function() {
                                                         	// custom minLength
                                                            var term = extractLast( this.value );
                                                            if ( term.length < 1 ) {
                                                            	return false;
                                                            }
                                                         },
                                                         focus: function() {
                                                         	// prevent value inserted on focus
                                                            return false;
                                                        },
                                                        select: function( event, ui ) {
															//sets up for multiple responses...
															//only do this IF the operator equals IN or NOT IN
															if (document.getElementById("idQueryExp_<% =i %>").value == 'in' || document.getElementById("idQueryExp_<% =i %>").value == 'not in') {
																var terms = split( this.value );
																// remove the current input
																terms.pop();
																// add the selected item
																terms.push( ui.item.value );
																// add placeholder to get the comma-and-space at the end
																terms.push( "" );
																this.value = terms.join( ", " );
																return false;
															}																				
                                                        }
                                                    });
                                                });
											</script>                                                            														
                                            <%
											'END JSON filter builter
											'------------------------------------------
															
										end if		
																								
									else															
                                       	response.write("<input type=text name=""idQueryValue"" id=""idQueryValue_" & i & """ size=20 maxlength=75 style=""width:70px;"" value=""" & subLst.childNodes(2).text & """>")
										response.write("<input type=text name=""idQueryValue"" id=""idQueryValue_" & i+1 & """ size=20 maxlength=75 style=""width:70px; margin-left:5px;"" value=""" & subLstNext.childNodes(2).text & """>")														
										'used on dhtml calendar instances
										if right(objPopCalendar,1) = "'" then
											objPopCalendar = objPopCalendar & ",'" & "idQueryValue_" & i & "','" & "idQueryValue_" & i+1 & "'"
										else
											objPopCalendar = objPopCalendar & "'" & "idQueryValue_" & i & "','" & "idQueryValue_" & i+1 & "'"
										end if
														
									end if                                                            
									%>
                                                    
                                </td>
                            </tr>
                            						
                            <%										
							'date field so increase row count by 1 so as to not repeat the second date value
							if inStr(lCase(subLst.childNodes(0).text),"date") > 0 then
								i = i+1
							end if
											
                        next
                        %>

						<!-- START calendar instances -->
						<% if len(objPopCalendar) > 0 then %>                                        
                        	<script type="text/javascript">
                            	var dateCal;
                                window.onload = function() {
                                	//issue date calendar
                                    dateCal = new dhtmlxCalendarObject([<% =objPopCalendar %>], true, {isWinHeader:true, headerButtons:"XT", isWinDrag:false, isYearEditable:true, isMonthEditable:true });
                                    dateCal.setSkin("simplegrey");
                                    dateCal.setDateFormat("%m/%d/%Y");
                                    dateCal.setYearsRange(2000, 2020);
                                    dateCal.setHeaderText("Filter Date");
                                }			
                            </script>
                        <% end if %>
                        <!-- STOP calendar instances -->                                        

					<!-- THIS IS WHERE FILTER OPTIONS ARE APPENDED WITH JAVASCRIPT -->
                    <tr style="display:none;">                                   			
                    	<td></td><td></td><td></td>
                    </tr>			
                                        
                </tbody>
            </table>
			<!-- STOP WHERE clause filter -->

        </td>
		<!-- STOP filter section-->

    	</tr>
    </table>
	<!-- STOP Filter table -->

	<!-- START Buttons -->
	<div style="float:right; padding-top:15px; margin-bottom:0px;"> 	       		
        <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); document.forms['frm'].submit(); return false;"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); return false;"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Buttons -->

</form>
    
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script type="text/javascript">

  // Add new row to table listing all added filters. All filters are added to form.
  // When form is submittted idQueryField, idQueryExp, and idQueryValue are split
  // within counter_edit_exec.asp and added to datebase table ReportQuery_Filter
  function addQueryRow()
  {  	
  
  	var tempHTML;
	
	// create randon numbers used to create unique element IDs for idQueryValue form fields
	var valueID_1 = Math.floor(Math.random()*1001);
	var valueID_2 = Math.floor(Math.random()*1001);
  
  	// create table body
	var table = document.getElementById("tblFilter");
    var tbody = table.getElementsByTagName("tbody")[0];

	// ----- ADD ROW -----
	// create row element and assign unique ID
	var rowCount = table.rows.length;
	var rowID = "rowFilter_" + (rowCount+2);
    var row = document.createElement("tr");
	row.id = rowID;
	
	// ----- ADD CELL #1 -----
	var selValue = document.getElementById('optQueryField').value;					// get VALUE from field dropdown
	var selIndex = document.getElementById('optQueryField').selectedIndex;			// get selected INDEX from dropdown
	var selText = document.getElementById('optQueryField').options[selIndex].text;	// get selected TEXT not value from dropdown
    var td1 = document.createElement("td")
	td1.width = "50";
	
	// check if field selected has "Date" text in name
	if (selValue.indexOf("Date")==-1){
		tempHTML = "<div style=\"white-space:nowrap;\">";
		tempHTML = tempHTML + "<img src=\"../_images/icons/18/cancel.png\" width=\"18\" height=\"18\" alt=\"Delete\" align=\"absmiddle\" style=\"margin-right:7px; cursor:pointer;\" onClick=\"javascript:delQueryRow('" + rowID + "','" + selText + "','" + document.getElementById('optQueryField').value + "');\"/>";
		tempHTML = tempHTML + "<input type=hidden name=\"idQueryField\" id=\"idQueryField\" value=\"" + document.getElementById('optQueryField').value + "\">";
		tempHTML = tempHTML + selText + "</div>";
	}
	// date filed...add 2 hidden fields to hold field name
	else {
		tempHTML = "<div style=\"white-space:nowrap;\">";
		tempHTML = tempHTML + "<img src=\"../_images/icons/18/cancel.png\" width=\"18\" height=\"18\" alt=\"Delete\" align=\"absmiddle\" style=\"margin-right:7px; cursor:pointer;\" onClick=\"javascript:delQueryRow('" + rowID + "','" + selText + "','" + document.getElementById('optQueryField').value + "');\"/>";
		tempHTML = tempHTML + "<input type=hidden name=\"idQueryField\" id=\"idQueryField\" value=\"" + document.getElementById('optQueryField').value + "\">";
		tempHTML = tempHTML + "<input type=hidden name=\"idQueryField\" id=\"idQueryField\" value=\"" + document.getElementById('optQueryField').value + "\">";
		tempHTML = tempHTML + selText + "</div>";
	}	
	td1.innerHTML = tempHTML;
	
	//remove selected text from dropdown
///	document.getElementById('optQueryField').remove(selIndex);		
	
	
	// ----- ADD CELL #2 -----	
    var td2 = document.createElement("td")
	// check if field selected has "Date" in name
	if (selValue.indexOf("Date")==-1){
		tempHTML = "<select name=\"idQueryExp\" id=\"idQueryExp_" + valueID_1 + "\" style=\"width:125px;\" onchange=\"javascript:changeElementStyle(this.id,this.value);\" >";
		tempHTML = tempHTML + "<option value=\"=\" >equals to</option>";
		tempHTML = tempHTML + "<option value=\"<>\" >does not equal to</option>";	
		tempHTML = tempHTML + "<option value=\"<=\" >less than</option>";
		tempHTML = tempHTML + "<option value=\">=\" >greater than</option>";
		tempHTML = tempHTML + "<option value=\"like\" >contains</option>";
		tempHTML = tempHTML + "<option value=\"not like\" >does not contain</option>";
		tempHTML = tempHTML + "<option value=\"is null\" >is null</option>";
		tempHTML = tempHTML + "<option value=\"is not null\" >is not null</option>";
		tempHTML = tempHTML + "<option value=\"in\" >is one of</option>";	
		tempHTML = tempHTML + "<option value=\"not in\" >is not one of</option>";
		tempHTML = tempHTML + "</select>";
	}
	// date filed...add 2 options
	else {
		tempHTML = "<select name=\"idQueryExp\" id=\"idQueryExp_" + valueID_1 + "\" style=\"width:125px; display:none;\">";
		tempHTML = tempHTML + "<option value=\">=\" >greater than</option>";
		tempHTML = tempHTML + "</select>";
		tempHTML = tempHTML + "<select name=idQueryExp style=\"width:125px; display:none;\">";
		tempHTML = tempHTML + "<option value=\"<=\" >less than</option>";
		tempHTML = tempHTML + "</select>";
		tempHTML = tempHTML + "<select name=\"idNOT-USED\" id=\"idNOT-USED\" style=\"width:125px;\">";
		tempHTML = tempHTML + "<option value=\"\" >between</option>";
		tempHTML = tempHTML + "</select>";
	}
	td2.innerHTML = tempHTML;
	td2.width = "125";
	
	
	// ----- ADD CELL #3 -----
	var td3 = document.createElement("td")
	// not date...add textarea input
	if (selValue.indexOf("Date")==-1){	
		tempHTML = "<textarea name=\"idQueryValue\" id=\"idQueryValue_" + valueID_1 + "\" style=\"overflow-y:hidden; height:13px; width:150px;\"></textarea>&nbsp;";
	}	
	// date filed...add 2 inputs
	else {	
		tempHTML = "<input type=text name=\"idQueryValue\" id=\"idQueryValue_" + valueID_1 + "\" size=10 maxlength=10 value=\"\" style=\"width:70px;\">";
		tempHTML = tempHTML + "<input type=text name=\"idQueryValue\" id=\"idQueryValue_" + valueID_2 + "\" size=10 maxlength=10 value=\"\" style=\"width:70px; margin-left:5px;\">";
	}
	td3.innerHTML = tempHTML;


	// ----- FINILIZE AND APPEND NEW CELLS AND ROW -----
    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
		
    // append row to table
    tbody.appendChild(row);


	// ----- ADD CALENDAR INSTANCE TO DATE ELEMENTS -----
	if (selValue.indexOf("Date")!=-1){	
		var dateCal;
		dateCal = new dhtmlxCalendarObject(["idQueryValue_"+ valueID_1,"idQueryValue_"+ valueID_2], true, {isWinHeader:true, headerButtons:"XT", isWinDrag:false, isYearEditable:true, isMonthEditable:true });
		dateCal.setSkin("simplegrey");
		dateCal.setDateFormat("%m/%d/%Y");
		dateCal.setYearsRange(2000, 2020);
		dateCal.setHeaderText("Filter Date");
	}
	
	// ----- ADD AUTO JSON QUERY IF NOT DATE FIELD -----
	if (selValue.indexOf("Date")<=-1){
		var ignoreFields = "[Total],[CRSID],[Issue #],[Time],[Location Address],[Location Postal Code],[Caller Address],[Caller Postal Code],[CustomerID],[Summary],[Details],[Addendum],[Resolution],[CaseNotes],[Notes],[Outcome]";
		
		//make sure field selected is not one to be ignored		
		if (selValue.indexOf("Caller")<=-1 && ignoreFields.indexOf(selValue)<=-1) {
			var queryField = selValue;
			queryField = queryField.replace("[","");
			queryField = queryField.replace("]","");
			
			//JSON filter builder
			$(function() {
				function split( val ) {
					return val.split( /,\s*/ );
				}
				function extractLast( term ) {
					return split( term ).pop();
				}		
				$("#idQueryValue_"+ valueID_1)
					// don't navigate away from the field on tab when selecting an item
					.bind( "keydown", function( event ) {
						if ( event.keyCode === $.ui.keyCode.TAB &&
								$( this ).data( "autocomplete" ).menu.active ) {
							event.preventDefault();
						}
					})
					.autocomplete({
						source: function( request, response ) {
							$.getJSON( "../_jquery/suggestBox/json-data.asp?view="+ document.getElementById('optTable').value +"&field="+ escape(queryField) , {
								term: extractLast( request.term )
							}, response );
						},
						search: function() {
							// custom minLength
							var term = extractLast( this.value );
							if ( term.length < 1 ) {
								return false;
							}
						},
						focus: function() {
							// prevent value inserted on focus
							return false;
						},
						select: function( event, ui ) {
							//sets up for multiple responses...
							//only do this IF the operator equals IN or NOT IN
							if (document.getElementById("idQueryExp_" + valueID_1).value == 'in' || document.getElementById("idQueryExp_" + valueID_1).value == 'not in') {
								var terms = split( this.value );
								// remove the current input
								terms.pop();
								// add the selected item
								terms.push( ui.item.value );
								// add placeholder to get the comma-and-space at the end
								terms.push( "" );
								this.value = terms.join( ", " );
								return false;
							}
						}
					});
			});
						
		}
		
	// END JSON QUERY BUILDER	
	}

  }
</script>

<script type="text/javascript"> 

  // used to remove selected row from filter table
  //   obj = Row ID being removed
  function delQueryRow(obj,label,value)
  {
	var theRow = document.getElementById(obj);
	var theTable = document.getElementById("tblFilter");
	
    for (var i = 0; i < theTable.rows.length; i++)
	{
    	if (theRow == theTable.rows[i]) {
               theTable.deleteRow(i);
          }	
	}
	
	//add selected text back in dropdown
///	var selectBox = document.getElementById("optQueryField");
///	selectBox.options[selectBox.options.length] = new Option(label, value);
  }

  // used to set height and scroll bars
  // if user selected IN or NOT IN as the opearator 
  function changeElementStyle2(expID,expValue) {
	var theElement = document.getElementById( "idQueryValue_" + expID.substr(11,3) );
	// user selected IN or NOT IN
	if (expValue == "in" || expValue == "not in") {
		theElement.style.overflowY = "auto";
		theElement.style.height = "42px";
	}
	// regular operator...make like normal input box
	else {
		theElement.style.overflowY = "hidden";
		theElement.style.height = "13px";
	}	
  }
  
  // used to set height and scroll bars
  // if user selected IN or NOT IN as the opearator 
  function changeElementStyle(expID,expValue) {
	var theElement = document.getElementById( "idQueryValue_" + expID.substr(11,3) );
	// user selected IN or NOT IN
	if (expValue == "in" || expValue == "not in") {
		theElement.style.overflowY = "auto";
		theElement.style.height = "42px";
		
		theElement.style.display = "";		
	}
	// regular operator...make like normal input box
	else {
		theElement.style.overflowY = "hidden";
		theElement.style.height = "13px";
		
		
		if (expValue == "is null" || expValue == "is not null") {
			theElement.style.display = "none";
		}
		else {
			theElement.style.display = "";
		}
		
	}
  }	  
  
</script>

