<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, pageSize
dim fieldsSQL, valueSQL

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim countString
dim optConfigure

dim modifiedDate : modifiedDate = Now()


'Get action type 
dim action : action = Request.Form("action")
if len(action) = 0 then
	action = Request.QueryString("action")
end if
if lCase(action) <> "edit" and lCase(action) <> "add" then	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Action Type.")	
end if


dim recId
recId = trim(Request.QueryString("recId"))
if recId = "" then
	recId = request.form("recid")
	if recId = "" and action = "edit" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Subject ID.")
	elseif recId = "" and action = "add" then				
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")
	end if
end if


'form variables
dim subjectID
dim subCRSID 'for SanctionCheck search only
dim subLogid
dim subName
dim subTitle
dim subType
dim subActionTaken
dim subActionDate
dim subSanctionCheck, subSCDone
dim subNotes


if action = "add" then
	subName = trim(Request.QueryString("name"))
end if


'save resolution
if request.form("save") = "save" then

	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
	<%
	
	'get new resolution field values	
	customerID = request.form("cid")	
	subLogid = request.form("subLogid")
	if len(trim(subLogid)) or subLogid="" or isNull(subLogid) then
		subLogid = "0"
	end if
	subName = request.form("subName")
	subTitle = request.form("subTitle")
	subType = request.form("subType")	
	subActionTaken = request.form("subActionTaken")
	subActionDate = request.form("subActionDate")
	subSanctionCheck = request.form("subSanctionCheck")
	subNotes = request.form("subNotes")
	
	'given to reporter
	if subSanctionCheck="Y" then
		subSCDone = "Yes"
	else
		subSCDone = "No"
	end if
		
	'save existing user field
	if action = "edit" then
	
		mySQL = "UPDATE Subject Set " _
				& "CustomerID='" 	& uCase(customerID)			& "'," _
				& "LOGID="	 		& subLogid		 			& ", " _
				& "Name='"			& replace(subName,"'","''") & "'," _	
				& "Title='"			& replace(subTitle,"'","''")& "'," _	
				& "Type='" 			& subType 					& "'," _
				& "ActionTaken='"	& subActionTaken			& "'," _				
				& "SanctionCheck='" & subSanctionCheck 			& "'," _
				& "Notes='"			& replace(subNotes,"'","''")& "'," _	
				& "ModifiedBy="  	& sLogid					& ", " _
				& "ModifiedDate='" 	& modifiedDate	 			& "' "

		if len(subActionDate) > 0 then mySQL = mySQL & ",ActionDate='" & subActionDate & "' " else mySQL = mySQL & ",ActionDate=null "
														
		'finalize update query
		mySQL = mySQL & "WHERE SubjectID=" & recId & " " 			
		set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

		call closeRS(rs)

		%>
		<script type='text/javascript'>
			//add subject to parent grid
			topWin.parent.saveSubject('<% =recId %>','<% =subName %>','<% =subTitle %>','<% =subType %>','<% =subSanctioncheck %>');
			//restart autosave timer
			topWin.parent.StartTheTimer();
			//close window	
			SimpleModal.close();
		</script>			
		<%

	'add new resolution
	elseif lCase(action) = "add" then
	
		dim resolutionID
		'Add Issue with Callback
		fieldsSQL = "INSERT INTO Subject (" _
			  & "CustomerID,CRSID,LOGID,Name,Title,Type,ActionTaken,SanctionCheck,Notes,ModifiedBy,ModifiedDate "
			  
		'check for empty dates	      
		if len(subActionDate) > 0 then fieldsSQL = fieldsSQL & ",ActionDate"			  
		
		fieldsSQL = fieldsSQL & ") "
			  
		valueSQL = " VALUES (" _
			  & "'"    	& uCase(customerID)				& "'," _
			  & "'"    	& recId							& "'," _
			  & " "    	& sLogid						& ", " _
			  & "'"    	& replace(subName,"'","''")		& "'," _
			  & "'"    	& replace(subTitle,"'","''")	& "'," _
			  & "'"    	& subType						& "'," _			  
			  & "'"    	& subActionTaken				& "'," _			  			  			  
			  & "'"    	& subSanctionCheck				& "'," _			  
			  & "'"    	& replace(subNotes,"'","''")	& "'," _			  
			  & " "    	& sLogid						& ", " _
			  & "'"    	& modifiedDate					& "' "

		'check for empty dates
		if len(subActionDate) > 0 then valueSQL = valueSQL & ",'" & subActionDate & "'"
			  
		valueSQL = valueSQL & ") "
	
		'set identity to pull back newly added record new record id is returned to newID
		mySQL = "SET NOCOUNT ON; " & fieldsSQL & valueSQL & " SELECT SCOPE_IDENTITY() AS newID;"		
		set rs = openRSexecuteDialog(mySQL)		
		subjectID = rs("newID")
						
		call closeRS(rs)
	
		%>
		<script type='text/javascript'>
			//add subject to parent grid
			topWin.parent.addSubject('<% =subjectID %>','<% =subName %>','<% =subTitle %>','<% =subType %>','<% =subSanctioncheck %>');
			//restart autosave timer
			topWin.parent.StartTheTimer();
			//close window	
			SimpleModal.close();			
		</script>			
		<%
	
	end if		
	
end if

'Determin DHTML pop-up calendars to Initialize...will append Investigation TextAreas also
dim activeCalendars
activeCalendars = "'subActionDate'"

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Resolution</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>
      
	<!--JQuery required components -->
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />
    
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

	<!-- DHTML Calendar http://www.dhtmlx.com -->
	<link rel="stylesheet" type="text/css" href="../_calendar/dhtmlxCalendar/codebase/dhtmlxcalendar.css">
    <link rel="stylesheet" type="text/css" href="../_calendar/dhtmlxCalendar/codebase/skins/dhtmlxcalendar_simplegrey.css">
	<script type="text/javascript" src="../_calendar/dhtmlxCalendar/codebase/dhtmlxcommon.js"></script>
	<script type="text/javascript" src="../_calendar/dhtmlxCalendar/codebase/dhtmlxcalendar.js"></script>

	<!-- jQuery Prompt Replacements http://abeautifulsite.net/blog/2008/12/jquery-alert-dialogs/ -->
    <script src="../_jquery/prompt/jquery.alerts.js"></script>
    <link rel="stylesheet" type="text/css" href="../_jquery/prompt/jquery.alerts.css"/>

    <script src="../scripts/javascript/forms.js"></script>
        
</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="popup_config_subject.asp" style="padding:0px; margin:0px;">
    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/user_silhouette.png" title="Subject" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Subject</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px; margin-bottom:5px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Modify selected subject below.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Resolution table -->
    <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">
    
        <%
		if lCase(action) = "edit" then
						
			mySQL = "SELECT SubjectID, CRSID, LOGID, CustomerID, Name, Title, Type, ActionTaken, ActionDate, SanctionCheck, Notes " _
				  & "		FROM Subject " _
				  & "		WHERE SubjectID = " & recId & " " _
				  & "		ORDER BY Subject.Name "													  											  			
				  
	        set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
			if rs.eof then				
				response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Subject ID.")	
			end if		

			subCRSID = rs("CRSID") 'this is only for SanctionCheck searches, recId is used for processing issue #
			subLogid = rs("Logid")
			subName = rs("Name")
			subTitle = rs("Title")
			subType = rs("Type")
			subActionTaken = rs("ActionTaken")
			subActionDate = rs("ActionDate")			
			subSanctionCheck = rs("SanctionCheck")
			subNotes = rs("Notes")

		end if
        %>

		<!-- START Subject -->
		<tr>
			<td class="clearFormat" style="border:none; padding:0px; margin:0px;">

				<% if cLng(session(session("siteID") & "adminLoggedOn")) <= 3 then %>
				<table cellpadding="0" cellspacing="0" style="width:100%;">                      
					<tr>
						<td align="left" style="padding-top:5px; padding-bottom:0px; margin:0px; border-bottom:none; background-color:#FFFFC8;">
   							<div style="color:#35487B; font-weight:bold; padding-left:20px; padding-right:10px; padding-top:4px; float:left;">SanctionCheck</div>
                   			<label>
							  <input type="radio" name="subSanctionCheck" id="subSanctionCheckYes" value="Y" style="vertical-align:middle;" <%=checkRadio(subSanctionCheck,"Y")%>>Yes&nbsp;&nbsp;&nbsp;&nbsp;
                            </label>							    
							<label>
							  <input type="radio" name="subSanctionCheck" id="subSanctionCheckNo" value="N" style="vertical-align:middle;" <%=checkRadio(subSanctionCheck,"N")%>>No
                            </label>                                                                            
                        </td>                          
                  	</tr>        
					<tr>
						<td class="clearFormat" align="left" style="padding-top:0px; padding-bottom:5px; padding-left:20px; margin:0px; background-color:#FFFFC8;">
	                        <div class="subLabel">Signifies a positive <a href="https://app.sanctioncheck.com/Scripts/search.asp?action=search&showPhrase=<% =subName %>&arrData=sdb,epls&showSearchTop=5&showSearchType=INDIV&showNotes=<% =subCRSID %>" target="_blank">SanctionCheck</a> match.</div>
                        </td>                          
                  	</tr>                            
				</table>
				<% else %>
					<input name="subSanctionCheck" type="hidden" value="<% =subSanctionCheck %>">                
                <% end if %>
                
				<table cellpadding="0" cellspacing="0" style="width:100%;">                      
					<tr>
						<td class="formLabel" style="width:100px; <% if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then response.write("border-top:none;") %>">Name:</td>
						<td align="left" style="<% if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then response.write("border-top:none;") %>"><input name="subName" class="inputLong" value="<%=subName%>" maxlength="255" /></td>                          
                  	</tr>        
					<tr>
						<td class="formLabel" style="width:100px;">Title:</td>
						<td align="left"><input name="subTitle" class="inputLong" value="<%=subTitle%>" maxlength="255" /></td>                          
                  	</tr>        
					<tr>
						<td class="formLabel" style="width:100px;">Subject Type:</td>
						<td align="left">          
							<%
                            'Build array for Risk Level dropdown selections
                            dim optSubTypeArr
                            optSubTypeArr = getDropDownOpt("pSubjectType",customerID)
                            call buildSelect(optSubTypeArr,subType,"subType","inputLong","Y")
                            %>
						</td>                          
                  	</tr>        
                              

					<% if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then %>
                        <tr>
                            <td class="formLabel" style="width:100px;">Action Taken:</td>
                            <td align="left">
                                <%
                                'Build array for Action Taken dropdown selections
                                dim optActionArr
                                optActionArr = getDropDownOpt("pActionTaken",customerID)
                                call buildSelect(optActionArr,subActionTaken,"subActionTaken","inputLong","Y")
                                %>
                            </td>                          
                        </tr>        
                                  
                        <tr>
                            <td class="formLabel" style="width:100px;">Action Date:</td>
                            <td align="left">          
	                            <input name="subActionDate" id="subActionDate" class="inputShort" value="<% =subActionDate %>" maxlength="20" />
                            </td>                          
                        </tr>        
					<% else %>
                        <input name="subActionTaken" type="hidden" value="<% =subActionTaken %>">
                        <input name="subActionDate" type="hidden" value="<% =subActionDate %>">                            
                    <% end if %>
                    
				</table>

				<table cellpadding="0" cellspacing="0" style="width:100%;">                   
					<tr>
						<td align="left" style="padding-right:7px;">
							<div style="color:#35487B; font-weight:bold; padding:5px;">Notes</div>
							<textarea name="subNotes" style="width:100%; <% if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then response.write("height:50px;") else response.write("height:75px;") %>" ><%=server.HTMLEncode(subNotes & "")%></textarea>
						</td>
                  	</tr>                                                                            
				</table>

          	</td>
        </tr>
		<!-- STOP Subject -->
        

		<tr>
           	<td align=left nowrap style="padding:0px; margin:0px;">&nbsp;</td>
       	</tr>
        
    </table>
	<!-- STOP Resolution table -->

	<input name="recid" type="hidden" value="<% =recId %>">
	<input name="resLogid" type="hidden" value="<% =subLogid %>">        
    <input name="action" type="hidden" value="<% =action %>">    
    <input name="cid" type="hidden" value="<% =customerid %>">
    <input name="save" type="hidden" value="save">
        
	<!-- START Buttons -->
	<div style="float:right;"> 	       		
        <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); document.forms['frm'].submit();"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Buttons -->

</form>
        
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script type="text/javascript"> 
	// Initialize TinyMCE with the tab_focus option 
	var dateCal;
	dateCal = new dhtmlxCalendarObject([<% =activeCalendars %>], true, {isWinHeader:true, headerButtons:"XT", isWinDrag:false, isYearEditable:true, isMonthEditable:true });
	dateCal.setSkin("simplegrey");
	dateCal.setDateFormat("%m/%d/%Y");
	dateCal.setYearsRange(2000, 2020);
	dateCal.setHeaderText("Date");
</script>     


