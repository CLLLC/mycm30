<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
Server.ScriptTimeout =25000
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, rsRes, rsTemp, pageSize
dim fieldsSQL, valueSQL

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim countString
dim optConfigure

dim purgeID
dim purgeCount, purgeTotal
dim purgeDate : purgeDate = Now()
dim purgeFrom
dim purgeBar

dim cusRetention

dim resTotal, resCount, resOwner
dim resID
dim resFirst
dim resLast
dim resEmail
dim defaultView
dim includeOwner

dim resUser, resUserReplace
dim tempUser
					
dim modifiedDate : modifiedDate = Now()

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 3 and programAdmin <> "Y" then	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")		
end if

'Get action type 
dim action : action = Request.Form("action")
if len(action) = 0 then
	action = Request.QueryString("action")
end if
if lCase(action) <> "assignall" and lCase(action) <> "removeall" and left(lCase(action),7) <> "replace" then   	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Action Type." & action)	
end if

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Reassign Issues</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>
      
	<!--JQuery required components -->
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>

	<!-- JQuery SPINNER https://github.com/btburnett3/jquery.ui.spinner/ -->
    <link type="text/css" rel="stylesheet" href="../_jquery/spinner/ui.spinner.css" />
	<script type="text/javascript" src="../_jquery/spinner/ui.spinner.js"></script>
    
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

	<!-- jQuery Prompt Replacements http://abeautifulsite.net/blog/2008/12/jquery-alert-dialogs/ -->
    <script src="../_jquery/prompt/jquery.alerts.js"></script>
    <link rel="stylesheet" type="text/css" href="../_jquery/prompt/jquery.alerts.css"/>
    
</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="popup_user_reassign.asp" style="padding:0px; margin:0px;">
    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/group_go.png" title="Retention" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Reassign Issues</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Configure issue reassignments using form below.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<%
	'*************************************************************
    'ASSIGN user to ALL issues
	'*************************************************************	
    if request.form("save") = "save" and action = "assignall" then

		resUser = Request.Form("resUser")
		if len(resUser) = 0 then   	
			response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Assign User.")	
		end if

		'include Case Owners		
		includeOwner = Request.Form("includeOwner")
		%>

		<!-- START File Import -->
		<div id="step_1" style="padding-bottom:5px;"><img id="image_1" src="../_images/myloader.gif" align="absmiddle">&nbsp;Assigning all issues</div>
		<div id="step_3" style="padding-bottom:5px;">&nbsp;</div>    
		<div id="step_2" style="padding-bottom:5px;">&nbsp;</div>    

		<!-- PROGRESS BAR -->
        <div id="progressbarWrapper" style="height:10px;" class="ui-widget-default">
            <div id="container" style="height:100%;"></div>
        </div>
        <script>
           	$(function() {	
       	     	//call progress bar constructor
           	    $("#container").progressbar();
           	});  
        </script>
                
		<div id="step_6" style="padding-top:5px;">&nbsp;</div>    
		<div style="border-bottom:1px dotted #cccccc;">&nbsp;</div>
        
        <!-- START Buttons -->
        <div style="float:right; padding-top:13px;"> 	       		
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
        </div>    
        <!-- STOP Buttons -->
    
        <%
        'push to screen NOW
        response.flush()
        %>    
        
        <!-- STEP 1 stored procedure -->
        <script language="javascript">
            document.getElementById('image_1').src = '../_images/myloader.gif';
        </script>        

		<%			
        response.flush()			

		if includeOwner = "Y" then

			mySQL = "SELECT Count(CRSID) AS [OwnerCount] FROM CRS WHERE CustomerID = '" & uCase(customerID) & "' AND (LogID <> " & resUser & " or LogID Is Null) " 
			set rs = openRSexecuteDialog(mySQL)	
		
			resOwner = rs("OwnerCount") 
			
			'starge reassigning
			if rs("OwnerCount") > 0 then	
				mySQL = "UPDATE CRS Set " _
					& "LogID=" 			& resUser 			& ", " _
					& "ModifiedBy=" 	& sLogid 			& ", " _	
					& "ModifiedDate='" 	& Now()		 		& "' " _	
					& "WHERE CustomerID = '" & uCase(customerID) & "' AND (LogID <> " & resUser & " or LogID Is Null) " 
				set rsRes = openRSexecuteDialog(mySQL)		
				%>
				<script language="javascript">
					document.getElementById('step_3').innerHTML = '&nbsp;Reassigned Ownership : <% =resOwner %> of <% =resOwner %>';
				</script>        
				<%				
				response.flush()	
			else
				%>
				<script language="javascript">
					document.getElementById('step_3').innerHTML = '&nbsp;Reassigned Ownership : 0 of 0';
				</script>        
				<%				
				response.flush()				
			end if

		else
			%>
			<script language="javascript">
				document.getElementById('step_3').innerHTML = '&nbsp;Reassigned Ownership : not included';
			</script>        
			<%						
			response.flush()					
		end if
		
				
		'pull records to add user
		mySQL = "SELECT CRSID FROM CRS a " _
			  & "WHERE CustomerID = '" & uCase(customerID) & "' AND " _
			  & "	NOT (Exists (SELECT b.CustomerID, b.CRSID, c.LOGID " _
			  & "			  FROM CRS b INNER JOIN CRS_Logins c ON b.CRSID = c.CRSID " _
			  & "			 WHERE a.CRSID = b.CRSID AND a.CustomerID = b.CustomerID AND c.LogID=" & resUser & " )) "
		'Open recordset
		set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)					
			
		'starge purging
		if not rs.eof then	
		
			'get users default setting for READ-ONLY or NOT
			mySQL = "SELECT Max(Groups.ReadOnly) AS DefaultView " _
				  & "FROM Groups INNER JOIN Logins_Groups ON Groups.GroupID = Logins_Groups.GroupID " _
				  & "WHERE Logins_Groups.LOGID=" & resUser
			set rsTemp = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)					
			defaultView = trim(rsTemp("DefaultView"))
		
			resTotal = rs.recordcount

			'Set CursorLocation of the Connection Object to Client
			cn.CursorLocation = adUseClient
						
			do until rs.eof				
				mySQL = "INSERT INTO CRS_Logins (" _
					  & "CustomerID, CRSID, LogID, ReadOnly, ModifiedBy, ModifiedDate" _
					  & ") VALUES (" _
					  & "'" & uCase(customerID)	& "'," _
					  & "'" & rs("crsid")		& "'," _
					  & " " & resUser 			& ", " _					  
					  & "'" & defaultView 		& "'," _					  
					  & " " & sLogid 			& ", " _
					  & "'" & Now()		 		& "' " _
					  & ")"
				set rsRes = openRSexecuteDialog(mySQL)
					
				resCount = resCount + 1
				%>
				<script language="javascript">
					document.getElementById('step_2').innerHTML = '&nbsp;Reassigned Managers : <% =resCount %> of <% =resTotal %>';
				</script>        
                    
				<!-- PROGRESS BAR -->
                <script>                      
                   	//call progress bar constructor -- value range is converted to 0 to 100%
                   	$("#container").progressbar({ value: 100/parseInt('<% =resTotal%>') * parseInt('<% =resCount %>') });
                </script>
                    
				<%								
				response.flush()							
				rs.movenext
			loop	
		
		'nothing found to purge
		else		
			%>
			<script language="javascript">
				document.getElementById('progressbarWrapper').style.display = 'none';
				document.getElementById('step_2').innerHTML = '<img src="../_images/error.png" align="absmiddle">&nbsp;<strong>Error:</strong> No records found to assign.';
			</script>        
			<%										
		end if
		call closeRS(rs)

		%>
		<script language="javascript">
			document.getElementById('image_1').src = '../_images/check.png';
		</script>
		<%				
        response.flush()
        %>        
		
		<!-- STEP 6 Finalize -->
		<script language="javascript">
			document.getElementById('step_6').innerHTML = '&nbsp;Reassignments <strong>COMPLETE</strong>';
		</script>                 
    	<%
        response.flush()
        %>

		<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
		<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

		<%

	'*************************************************************		
    'REMOVE user from ALL issues
	'*************************************************************	
    elseif request.form("save") = "save" and action = "removeall" then

		resUser = Request.Form("resUser")
		if len(resUser) = 0 then   	
			response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Assign User.")	
		end if

		'include Case Owners		
		includeOwner = Request.Form("includeOwner")		
		%>

		<!-- START File Import -->
		<div id="step_1" style="padding-bottom:5px;"><img id="image_1" src="../_images/myloader.gif" align="absmiddle">&nbsp;Removing all assignments</div>
		<div id="step_3" style="padding-bottom:5px;">&nbsp;</div>            
		<div id="step_2" style="padding-bottom:5px;">&nbsp;</div>    

		<!-- PROGRESS BAR -->
        <div id="progressbarWrapper" style="height:10px;" class="ui-widget-default">
            <div id="container" style="height:100%;"></div>
        </div>
        <script>
           	$(function() {	
       	     	//call progress bar constructor
           	    $("#container").progressbar();
           	});  
        </script>
                
		<div id="step_6" style="padding-top:5px;">&nbsp;</div>    
		<div style="border-bottom:1px dotted #cccccc;">&nbsp;</div>
        
        <!-- START Buttons -->
        <div style="float:right; padding-top:13px;"> 	       		
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
        </div>    
        <!-- STOP Buttons -->
    
        <%
        'push to screen NOW
        response.flush()
        %>    
        
        <!-- STEP 1 stored procedure -->
        <script language="javascript">
            document.getElementById('image_1').src = '../_images/myloader.gif';
        </script>        

		<%			
        response.flush()			


		if includeOwner = "Y" then

			mySQL = "SELECT Count(CRSID) AS [OwnerCount] FROM CRS WHERE CustomerID = '" & uCase(customerID) & "' AND LogID = " & resUser & " " 
			set rs = openRSexecuteDialog(mySQL)	
		
			resOwner = rs("OwnerCount") 
			
			'starge reassigning
			if rs("OwnerCount") > 0 then	
				mySQL = "UPDATE CRS Set " _
					& "LogID=" 			& Null 			& ", " _
					& "ModifiedBy=" 	& sLogid		& ", " _	
					& "ModifiedDate='" 	& Now()			& "' " _	
					& "WHERE CustomerID = '" & uCase(customerID) & "' AND LogID = " & resUser & " " 
				set rsRes = openRSexecuteDialog(mySQL)		
				%>
				<script language="javascript">
					document.getElementById('step_3').innerHTML = '&nbsp;Reassigned Ownership : <% =resOwner %> of <% =resOwner %>';
				</script>        
				<%				
				response.flush()	
			else
				%>
				<script language="javascript">
					document.getElementById('step_3').innerHTML = '&nbsp;Reassigned Ownership : 0 of 0';
				</script>        
				<%				
				response.flush()				
			end if

		else
			%>
			<script language="javascript">
				document.getElementById('step_3').innerHTML = '&nbsp;Reassigned Ownership : not included';
			</script>        
			<%						
			response.flush()					
		end if
		
				
		mySQL = "SELECT CRSLogID " _
			  & "FROM CRS_Logins " _
			  & "WHERE CustomerID = '" & uCase(customerID) & "' AND LOGID = " & resUser & " "
		'Open recordset
		set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)					
			
		'starge purging
		if not rs.eof then	
			resTotal = rs.recordcount

			'Set CursorLocation of the Connection Object to Client
			cn.CursorLocation = adUseClient
				
			do until rs.eof			

				mySQL = "DELETE FROM CRS_Logins WHERE CRSLogID = " & rs("CRSLogID") & " "
				set rsRes = openRSexecuteDialog(mySQL)
				
				resCount = resCount + 1
				%>
				<script language="javascript">
					document.getElementById('step_2').innerHTML = '&nbsp;Removed : <% =resCount%> of <% =resTotal %>';
				</script>        
                    
				<!-- PROGRESS BAR -->
                <script>                      
                  	//call progress bar constructor -- value range is converted to 0 to 100%
                   	$("#container").progressbar({ value: 100/parseInt('<% =resTotal%>') * parseInt('<% =resCount%>') });
                </script>
                    
				<%								
				response.flush()							
				rs.movenext
			loop	
	
		'nothing found to purge
		else		
			%>
			<script language="javascript">
				document.getElementById('step_2').innerHTML = '<img src="../_images/error.png" align="absmiddle">&nbsp;<strong>Error:</strong> No records found to remove.';
			</script>        
			<%										
		end if
		call closeRS(rs)
			
		%>
		<script language="javascript">
			document.getElementById('image_1').src = '../_images/check.png';
		</script>
		<%				
        response.flush()
        %>        
	
		<!-- STEP 6 Finalize -->
		<script language="javascript">
			document.getElementById('step_6').innerHTML = '&nbsp;Reassignments <strong>COMPLETE</strong>';
		</script>                 
    	<%
        response.flush()
        %>

		<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
		<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>


	<%
	'*************************************************************
	'REPLACE one user from another
	'*************************************************************	
    elseif request.form("save") = "save" and left(lCase(action),7) = "replace" then

		'user to assign
		resUser = Request.Form("resUser")
		if len(resUser) = 0 then   	
			response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Assign User.")	
		end if
		
		'user to replace
		resUserReplace = trim(mid(action,9,len(action)))				

		'include Case Owners		
		includeOwner = Request.Form("includeOwner")
		%>

		<!-- START File Import -->
		<div id="step_1" style="padding-bottom:5px;"><img id="image_1" src="../_images/myloader.gif" align="absmiddle">&nbsp;Assigning all issues</div>
		<div id="step_3" style="padding-bottom:5px;">&nbsp;</div>            
		<div id="step_2" style="padding-bottom:5px;">&nbsp;</div>    

		<!-- PROGRESS BAR -->
        <div id="progressbarWrapper" style="height:10px;" class="ui-widget-default">
            <div id="container" style="height:100%;"></div>
        </div>
        <script>
           	$(function() {	
       	     	//call progress bar constructor
           	    $("#container").progressbar();
           	});  
        </script>
                
		<div id="step_6" style="padding-top:5px;">&nbsp;</div>    
		<div style="border-bottom:1px dotted #cccccc;">&nbsp;</div>
        
        <!-- START Buttons -->
        <div style="float:right; padding-top:13px;"> 	       		
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
        </div>    
        <!-- STOP Buttons -->
    
        <%
        'push to screen NOW
        response.flush()
        %>    
        
        <!-- STEP 1 stored procedure -->
        <script language="javascript">
            document.getElementById('image_1').src = '../_images/myloader.gif';
        </script>        

		<%			
        response.flush()			


		if includeOwner = "Y" then

			mySQL = "SELECT Count(CRSID) AS [OwnerCount] FROM CRS WHERE LogID = " & resUserReplace
			set rs = openRSexecuteDialog(mySQL)	
		
			resOwner = rs("OwnerCount") 
			
			'starge reassigning
			if rs("OwnerCount") > 0 then	
				mySQL = "UPDATE CRS Set " _
					& "LogID=" 			& resUser 			& ", " _
					& "ModifiedBy=" 	& sLogid 			& ", " _	
					& "ModifiedDate='" 	& Now()		 		& "' " _	
					& "WHERE CustomerID = '" & uCase(customerID) & "' AND LogID=" & resUserReplace	& " " 
				set rsRes = openRSexecuteDialog(mySQL)		
				%>
				<script language="javascript">
					document.getElementById('step_3').innerHTML = '&nbsp;Reassigned Ownership : <% =resOwner %> of <% =resOwner %>';
				</script>        
				<%				
				response.flush()	
			else
				%>
				<script language="javascript">
					document.getElementById('step_3').innerHTML = '&nbsp;Reassigned Ownership : 0 of 0';
				</script>        
				<%				
				response.flush()				
			end if

		else
			%>
			<script language="javascript">
				document.getElementById('step_3').innerHTML = '&nbsp;Reassigned Ownership : not included';
			</script>        
			<%						
			response.flush()					
		end if

				
		mySQL = "SELECT Count(CRSLogID) AS [PurgeCount] FROM CRS_Logins WHERE LogID = " & resUserReplace
		set rs = openRSexecuteDialog(mySQL)	

		'starge purging
		if rs("PurgeCount") > 0 then	
			
			'pull records to purge
			mySQL = "SELECT a.CRSLogID, a.CRSID, a.LogID, a.CustomerID FROM CRS_Logins a " _
				  & "WHERE CustomerID = '" & uCase(customerID) & "' AND LogID=" & resUserReplace & " AND " _
				  & "	NOT (Exists (SELECT b.CustomerID, b.CRSID, b.LOGID " _
				  & "			  FROM CRS_Logins b " _
				  & "			 WHERE a.CRSID = b.CRSID AND a.CustomerID = b.CustomerID AND b.LogID=" & resUser & " )) "				  
			'Open recordset
			set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)					
						
			resTotal = rs.recordcount

			'Set CursorLocation of the Connection Object to Client
			cn.CursorLocation = adUseClient
				
			do until rs.eof				
				mySQL = "UPDATE CRS_Logins Set " _
						& "LogID=" 			& resUser 			& ", " _
						& "ModifiedBy=" 	& sLogid 			& ", " _	
						& "ModifiedDate='" 	& Now()		 		& "' " _	
						& "WHERE CRSLogID=" & rs("CRSLogID")	& " " 
				set rsRes = openRSexecuteDialog(mySQL)
					
				resCount = resCount + 1
				%>
				<script language="javascript">
					document.getElementById('step_2').innerHTML = '&nbsp;Reassigned Managers : <% =resCount %> of <% =resTotal %>';
				</script>        
                    
				<!-- PROGRESS BAR -->
                <script>                      
                  	//call progress bar constructor -- value range is converted to 0 to 100%
                   	$("#container").progressbar({ value: 100/parseInt('<% =resTotal%>') * parseInt('<% =resCount %>') });
                </script>
                    
				<%								
				response.flush()							
				rs.movenext
			loop	
			
			%>            
			<script language="javascript">
				document.getElementById('step_2').innerHTML = '&nbsp;Reassigned Managers : <% =resCount %> of <% =resTotal %>&nbsp;--&nbsp;removing extra assignments...';
			</script>        
			<%
			response.flush()
			'remove any leftovers
			mySQL = "DELETE FROM CRS_Logins WHERE CustomerID = '" & uCase(customerID) & "' AND LogID=" & resUserReplace & " "
			set rs = openRSexecuteDialog(mySQL)
			%>
			<script language="javascript">
				document.getElementById('step_2').innerHTML = '&nbsp;Reassigned Managers : <% =resCount %> of <% =resTotal %>&nbsp;--&nbsp;removing extra assignments...DONE';
			</script>        
			<%
			response.flush()
				
		'nothing found to purge
		else		
			%>
			<script language="javascript">
				document.getElementById('progressbarWrapper').style.display = 'none';
				document.getElementById('step_2').innerHTML = '<img src="../_images/error.png" align="absmiddle">&nbsp;<strong>Error:</strong> No records found to assign.';
			</script>        
			<%										
		end if
		call closeRS(rs)

		%>
		<script language="javascript">
			document.getElementById('image_1').src = '../_images/check.png';
		</script>
		<%				
        response.flush()
        %>        
		
		<!-- STEP 6 Finalize -->
		<script language="javascript">
			document.getElementById('step_6').innerHTML = '&nbsp;Reassignments <strong>COMPLETE</strong>';
		</script>                 
    	<%
        response.flush()
        %>

		<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
		<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

		<%

	'show form
    else
		
        %>
	
        <!-- START Category table -->
        <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">
        
            <tr>
				<td class="formLabel" style="border-top: none; padding-top:0px; margin-top:0px;">Assign User:</td>
				<td align=left style="border-top: none; padding-top:0px; margin-top:0px;">
					<%
                    'Find Categories available TO BE assigned
					mySQL = "SELECT LogID, FirstName, LastName, Active FROM Logins WHERE CustomerID = '" & customerID & "' AND Active='Y' "					
                    set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                    'build combo box
                    response.write("<select name=""resUser"" id=""resUser"" size=1 class=""inputLong"">")
                    response.write("<option value="""">-- Select --</option>")
                    do until rs.eof			

						tempUser = ""
                        if len(rs("LastName")) > 0 then tempUser = rs("LastName")
                        if len(rs("FirstName")) > 0 and len(tempUser) > 0 then tempUser = tempUser & ", " & rs("FirstName") else tempUser = rs("FirstName")
						'show users status and setting on this issue
						if rs("Active") = "C" then
							tempUser = tempUser & " (contact only)"
						elseif rs("Active") = "N" then	
							tempUser = tempUser & " (disabled)"
						end if																		
                        response.Write "<option value=""" & rs("LogID") & """>" & tempUser & "</option>"																		

                        rs.movenext
                    loop
                    response.write("</select>")
                    call closeRS(rs)
                    %>                
				</td>
			</tr>

            <tr>
				<td class="formLabel" style="padding-bottom:20px;">Action:</td>
				<td align=left style="padding-bottom:20px;">
					<%
                    'Find Categories available TO BE assigned
					mySQL = "SELECT LogID, FirstName, LastName, Active FROM Logins WHERE CustomerID = '" & customerID & "' "					
                    set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                    'build combo box
                    response.write("<select name=""action"" id=""action"" size=1 class=""inputLong"">")
                    response.write("<option value="""">-- Select --</option>")
                    response.write("<option value=""assignall"">:: Assign to all issues</option>")					
                    response.write("<option value=""removeall"">:: Remove from all issues</option>")															
                    do until rs.eof			

						tempUser = ""
                        if len(rs("LastName")) > 0 then tempUser = rs("LastName")
                        if len(rs("FirstName")) > 0 and len(tempUser) > 0 then tempUser = tempUser & ", " & rs("FirstName") else tempUser = rs("FirstName")
						'show users status and setting on this issue
						if rs("Active") = "C" then
							tempUser = tempUser & " (contact only)"
						elseif rs("Active") = "N" then	
							tempUser = tempUser & " (disabled)"
						end if																		
                        response.Write "<option value=""replace:" & rs("LogID") & """>Replace: " & tempUser & "</option>"																		

                        rs.movenext
                    loop
                    response.write("</select>")
                    call closeRS(rs)
                    %>                
                    <div style="padding-top:10px;">
	                   	<input name="includeOwner" id="includeOwner" type="checkbox" value="Y" />
						<label for="includeOwner" style="cursor:default;" >Include <strong>Case Owner</strong> in reassignments</label>                
                   	</div>
				</td>
			</tr>                        
        
            <tr>
                <td colspan="2" align=left nowrap style="padding:10px; margin:10px;">&nbsp;</td>
            </tr>
            
        </table>
        <!-- STOP Category table -->
    
        <input name="cid" type="hidden" value="<% =customerid %>">
        <input name="save" type="hidden" value="save">
            
        <!-- START Buttons -->
        <div style="float:right;"> 	       		
            <a class="myCMbutton" href="#" accesskey="E" onClick="this.blur(); if(validateForm()==true){confirmPurge()}; return false;"><span class="save" style="padding-right:7px;"><u>S</u>ave</span></a>        
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
        </div>
        <!-- STOP Buttons -->
    
		<%	
    end if
    %>

</form>
        
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script>
	function validateForm() {		
		var formPassed = true;	//retunred value
		var reqFields = "action,resUser";
		var temp = reqFields.split(",");
		var i;
		var el;		
		
		for(i=0; i<temp.length; i++) {			
			if (document.getElementById(temp[i])) {			
				el = document.getElementById(temp[i])
				if (!el.value) {			
					el.style.background = '#ffff9f'; //set to yellow			
					jAlert('<strong>Form Error!</strong><br/><br/>Please ensure all required fields have been entered.', 'myCM Alert'); //inform user of error
					formPassed = false; //return error
				}
				else {				
					if (el.style.background == '#ffff9f') {
						//alert('2:' + temp[i]);
						el.style.background = '#fff';	//set back to white
					}
				}		
			}
		}
		
		return formPassed;
		
	}	
	
	function confirmPurge(action) {		
		jConfirm('Reassign issues now?<br/><br/><strong>This action CANNOT be undone</strong>.<br/><br/>', 'Confirm Reassignment', function(r) {			
			if (r==true) {				
				document.frm.submit();
			}
			else {
				return false;
			}
		});					
	}
</script>

