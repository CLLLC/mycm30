<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'Database
dim mySQL, cn, rs
dim count

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim pageView : pageView = Request.QueryString("pageView")

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>myCM Audit Log</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- HTML Table Filter Generator http://tablefilter.free.fr/ -->
    <link rel="stylesheet" type="text/css" href="../_tablefilter/filtergrid.css"/>
	<script type="text/javascript" src="../_tablefilter/tablefilter_all.js"></script>
	<script type="text/javascript" src="../_tablefilter/sortabletable.js"></script>
    <script type="text/javascript" src="../_tablefilter/tfAdapter.sortabletable.js"></script>        

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/briefcase.png" title="Audit Log" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Profile Snapshot</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Below is a snapshot of the selected profile.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Profile table -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>
        	<td>
    
				<%
                '--------------------------------------------------
                'get directive records
                '--------------------------------------------------	
                mySQL = "SELECT  Customer.* " _
                      & "	FROM Customer " _
                      & "	WHERE CustomerID = '" & customerID & "' "
                set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                
                if not rs.eof then			  
				%>
					
                    <!-- START Profile snapshot -->	                    
					<table id="table:details:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">

                   	<tr>
                     	<td class="formLabel">Name:</td>
                    	<td align=left nowrap><% =rs("name") %>&nbsp;[<% =rs("customerid") %>]</td>
                   	</tr>

                 	<tr style="display:none;">
                 		<td class="formLabel">Display As:</td>
                  		<td align=left nowrap>
                   			<div><% =rs("displayas") %></div>
                   			<div class="subLabel" style="padding-top:5px;">Display used during intake process.</div>
                 		</td>
             		</tr>

                 	<tr>                            
                 		<td class="formLabel">Address:</td>
                		<td align=left nowrap><% =rs("address") %><br/><% =rs("city") %>,&nbsp;<% =rs("state") %>&nbsp;<% =rs("postalcode") %><br/><% =rs("country") %></td>                              
            		</tr>

					<tr>
                  		<td class="formLabel">Industry:</td>
                 		<td align=left nowrap><% =rs("industry") %></td>
                 	</tr>

               		<tr>
                 		<td class="formLabel">Employee Count:</td>
                 		<td align=left><% if not isNull(rs("employeecount")) then response.write(FormatNumber(rs("employeecount"),0)) %></td>
                   	</tr>

                	<tr>
                    	<td class="formLabel">Phone Number(s):</td>
                    	<td align=left nowrap>                                                              
                            <div style="float:left; width:150px; height:20px;">
                                Work: <% =rs("phone_work") %>
                            </div>                                    
                            <div style="height:20px;">
                                Fax: <% =rs("phone_fax") %>
                            </div>                                                                                                    
                            <div style="float:left; width:150px; height:20px;">
                                Customer: <% =rs("phone_customer") %>
                            </div>                                    
                            <div style="height:20px;">
                                Other: <% =rs("phone_other") %>
                            </div>                                                                                                                              
                        </td>
                    </tr>
                                                           
                    <tr>
                        <td class="formLabel" style="background-color:#FFFFC8;">Contract Date:</td>
                        <td align=left style="background-color:#FFFFC8;"><% =rs("contractdate") %></td>
                    </tr>
        
                    <tr>
                        <td class="formLabel" style="background-color:#FFFFC8;">Start Date:</td>
                        <td align=left style="background-color:#FFFFC8;"><% =rs("startdate") %></td>
                    </tr>
        
                    <tr>
                        <td class="formLabel" style="background-color:#FFFFC8;">Retention:</td>
                        <td align=left style="background-color:#FFFFC8;"><% =rs("retention") %></td>
                    </tr>
    
                </table>
                <!-- END Account Info Form Fields -->                                                                                                                                       

				<% 
				end if 
				
				call closeRS(rs)
				%>
                
        	</td>
        </tr>
    </table>
	<!-- STOP Profile table -->

	<!-- START Cancel Button -->
	<div style="padding-top:5px; margin-bottom:0px;">
    	<% if pageView = "issue" or pageView = "issue:00" then %>
			<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); topWin.parent.StartTheTimer(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>
		<% else %>
			<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>
		<% end if %>        
    </div>
	<!-- STOP Cancel Button -->     
    
</body>
</html>

<%
'close database connection
call closeDB()
%>

