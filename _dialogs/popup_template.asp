<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'Database
dim mySQL, cn, rs, count, pagesize

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
end if

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>myCM Custom Templates</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- HTML Table Filter Generator http://tablefilter.free.fr/ -->
    <link rel="stylesheet" type="text/css" href="../_tablefilter/filtergrid.css"/>
	<script type="text/javascript" src="../_tablefilter/tablefilter_all.js"></script>
	<script type="text/javascript" src="../_tablefilter/sortabletable.js"></script>
    <script type="text/javascript" src="../_tablefilter/tfAdapter.sortabletable.js"></script>        

    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/document_template.png" title="Audit Log" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Custom Templates</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Select a custom template below to insert.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<%
	'--------------------------------------------------
	'get directive records
	'--------------------------------------------------	
	mySQL = "SELECT  TemplateID, Name, Template " _
		  & "	FROM Template " _
		  & "	WHERE CustomerID = '" & customerID & "' "		  
    set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)	  
	
	if not rs.eof then
		%>
    
        <!-- START Directive table -->
        <table id="customerTable" width="100%" style="background-color:#969696;" cellpadding=0 cellspacing=0>
                                
            <thead>
                <tr>
                    <th align="left" nowrap="nowrap">Name</th>
                </tr>
            </thead>
            
            <tbody>
                <%
                do until rs.eof
                                                    
                    'START current row
                    response.write("<tr>")									
                            
                    'ICON/NAME link
					response.Write("<td align=""left"" style=""cursor:pointer;"" onclick=""setTemplate(" & rs("TemplateID") & "); return false;"" >")									
                    response.Write("	<img src=""../_images/icons/16/insert_element.png"" title=""Insert Paragraph"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:2px;"" />")
                    response.write("	<a style=""color:#333333;"">" & rs("name") & "</a>")
                    response.Write("</td>")
    
                    'CLOSE current row						
                    response.write("</tr>")
                            
                    rs.movenext
                loop
                %>
            </tbody>
        </table>
        
        <%
        dim tableProperty
        tableProperty = "sort: true, sort_config:{sort_types:['String']}, filters_row_index: 1, " _
                      & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
                      & "status_bar: true, col_0: ""input"", " _
                      & "paging: true, paging_length: 8, " _
                      & "enable_non_empty_option: true, or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, " _
                      & "highlight_keywords: true, " _
                      & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
                      & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"						  
        %>
        <!-- STOP Directive table -->
        
        <!-- Fire table build -->
        <script language="javascript" type="text/javascript">
            //<![CDATA[
            var tableProp = {<% =tableProperty %>};
            //initiate table setup
            var tf1 = setFilterGrid("customerTable",tableProp);
            //]]>
        </script>
        
        <!-- Shadow table -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="5px" style="background:#D0D0D0;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
            </tr>
        </table>
        <!-- STOP Locations table -->                

	<%
	else

		response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
		response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
        call systemMessageBox("paragraphMessage","statusMessageALERT","No custom templates found.")
		response.write("   </td>")
		response.write("</tr></table>")			
	
	end if
	%>

	<!-- START Cancel Button -->
	<div style="padding-top:5px; margin-bottom:0px;">
    	<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); topWin.parent.StartTheTimer(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>
    </div>
	<!-- STOP Cancel Button -->
    
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script language="javascript">	
	// dependent on SimpleModal.js
	// note: topWin.parent is defined in SimpleModal.js and replaces "window.opener"
	function setTemplate(TemplateID) {		
		//insert/display associated paragraph/template
		$.ajax({
		  	url: "../_jquery/suggestbox/json-data.asp?view=template&field=templateid&term="+TemplateID,
		  	cache: false,
		  	async: false,
		  	dataType: "json",
		  	success: function(data) {
			  if(data!=null) {				  
				topWin.parent.tinyMCE.activeEditor.setContent(topWin.parent.tinyMCE.activeEditor.getContent() + "" + data + "");
			  }
		}});				
		//restart autosave
		topWin.parent.StartTheTimer();
		//close window
      	SimpleModal.close();
	}
</script>

