<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%
'Database
dim mySQL, cn, rs, field, count

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim idTelcom
idTelcom = trim(Request.QueryString("recId"))
if idTelcom = "" then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Telcom ID.")
end if

'--------------------------------------------------
'get user record...
'--------------------------------------------------	
mySQL = "SELECT * " _
	  & "	FROM Telcom " _
	  & "	WHERE  TelcomID=" & idTelcom & " "				
'open recordset   			  
set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
if rs.eof then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Telcom ID.")
end if
'--------------------------------------------------	

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Telcom</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    

	<style type="text/css">
    <!--
    .sectionHeader {
       padding:3px !important; 
	   margin:0px !important; 
	   background-color:#E2E2E2 !important; 
	   font-weight:bold !important; 
	   border-top:1px solid #5B5B5B !important; 
	   border-bottom:1px solid #5B5B5B !important;
       }
	 .printTable td {
		vertical-align:top;
		border-top:1px dotted #cccccc;
		padding-top:8px;
		padding-bottom:10px;
	 }	   

	.printTable .printLabel 			{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: none;}
	.printTable .printLabel:link 		{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: none;}
	.printTable .printLabel:link:hover 	{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: underline;}
	.printTable .printLabel:active 		{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: none;}
	.printTable .printLabel:visited 	{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: none;}
	.printTable .printLabel:hover 		{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: none;}
	 
	-->
    </style>

	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
   
</head>

	<body style="padding:20px;">

		<div style="text-align:left; margin-bottom:10px; float:left;">
    	   	<span style="color:#35487B; position:static; font-family:arial; font-size:14px; font-weight:bold; margin-bottom:10px;">Telcom</span>
	   	</div> 

        <!-- START Buttons -->
        <div style="text-align:right;"> 	       		
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>        
        </div>    
        <!-- STOP Buttons -->        
        
        <!-- START Form Fields -->	                    
		<table id="table:details:general" width="100%" class="printTable" cellpadding="0" cellspacing="0">

            <tr>
                <td class="sectionHeader">Telcom</td>
            </tr>                            
            
			<%
			response.write("<tr>")
			response.write("	<td colspan=""2"" class=""printLabel"" style=""border-top:none; background-color:#FFFFC8;"">" & rs("Number") & " [" & rs("DNIS") & "]" & "</td>")
			response.write("</tr>")
			%>

            <tr>
                <td>
	
                    <!-- START Telcom table -->
                    <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">                                    
                
                            <tr>
                                <td colspan="2" align="center" style="border:none;">
                                	<table width="95%">
                                    	<tr>
                                        	<td align="left" class="clearFormat">
                                				<div style="color:#35487B; font-weight:bold; padding:3px;">Answer</div>
                                                <div style="border:#CCC 1px solid; padding:5px; text-align:left; vertical-align:text-top;"><% =replace(rs("answer") & "",vbCRLF,"<br>") %>&nbsp;</div>
                            				</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        
                            <tr>
                                <td colspan="2" align="center">
                                	<table width="95%">
                                    	<tr>
                                        	<td align="left" class="clearFormat">
                                				<div style="color:#35487B; font-weight:bold; padding:3px;">Greeting</div>
												<div style="border:#CCC 1px solid; padding:5px; text-align:left; vertical-align:text-top; height:150px; overflow:auto; overflow-x: hidden; overflow-y: scroll;"><% =replace(rs("greeting") & "",vbCRLF,"<br>") %>&nbsp;</div>                                                                                                
                            				</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        
                    </table>
                    <!-- STOP Telcom table -->
                        
        		</td>
            
        	</tr>

			<%
			call closeRS(rs)
            %>        

		</table>
		<!-- END Form Fields -->
                       

		<!-- START END of issue notice -->  
		<table width="100%" class="printTable" style="margin-bottom:5px;" cellpadding="0" cellspacing="0">                                   
			<tr>
				<td class="sectionHeader" style="text-align:center">- End of Record -</td>
			</tr>
		</table>
		<!-- STOP END of issue notice -->                          

		<div style="text-align:left; float:left;">&nbsp;</div> 
        
        <!-- START Buttons -->
        <div style="text-align:right; padding-bottom:10px;"> 	       		
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>        
        </div>    
        <!-- STOP Buttons -->

	</body>
</html>

<%
'close database connection
call closeDB()
%>

