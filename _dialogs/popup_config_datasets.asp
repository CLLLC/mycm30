<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, field

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then			
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'Work Fields
dim pageSize
dim I
dim item
dim count
dim countString
dim optConfigure

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 2 then ' and programAdmin = "N" then	
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if

'Get action type 
dim action : action = Request.Form("action")
if len(action) = 0 then
	action = Request.QueryString("action")
end if
if lCase(action) <> "edit" and _
   lCase(action) <> "add" and _
   lCase(action) <> "del" then	   
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Action Type.")	
end if

'Get record ID if editing or deleting
if lCase(action) = "edit" or lCase(action) = "del" then
	dim recId : recId = Request.Form("recId")
	if len(recId) = 0 then
		recId = Request.QueryString("recid")
	end if
	if len(recId) = 0 or isNull(recId) or recId = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Record ID.")	
	end if
end if



'form variables
dim optName
dim optTable
dim optType
dim optListURLField
dim optSecurityLevel
dim optDescription

	
'set sort order on save
if request.form("save") = "save" then

	optName = request.form("optName")
	if len(optName) = 0 then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid dataset name.")	
	end if
	
	optTable = request.form("optTable")
	optType = request.form("optType")
	optListURLField = request.form("optListURLField")
	optSecurityLevel = request.form("optSecurityLevel")
	optDescription = request.form("optDescription")
	optDescription = replace(optDescription,chr(13)," ")
	optDescription = replace(optDescription,chr(10)," ")	
	optDescription = replace(optDescription,vbCrLf," ")
		
	'save existing alert
	if action = "edit" then
		mySQL = "UPDATE Dataset Set " _
				& "Name='" 			& replace(optName,"'","''") 		& "'," _
				& "[Table]='" 		& optTable 							& "'," _
				& "Type='" 			& optType 							& "'," _	
				& "ListURLField='" 	& optListURLField 					& "'," _	
				& "SecurityLevel=" 	& optSecurityLevel 					& ", " _	
				& "Description='" 	& replace(optDescription,"'","''")	& "'," _	
				& "ModifiedBy=" 	& sLogid 							& ", " _	
				& "ModifiedDate='" 	& Now()		 						& "' " _	
				& "WHERE  DatasetID=" & recId & " AND CustomerID='" & customerID & "' "   
		set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

		session(session("siteID") & "okMsg") = "Dataset has been saved successfully."
		
	'add new alert			
	elseif action = "add" then
		mySQL = "INSERT INTO Dataset (" _
			  & "CustomerID, Name, [Table], Type, ListURLField, SecurityLevel, Description, ModifiedBy, ModifiedDate" _
			  & ") VALUES (" _
			  & "'" & uCase(customerID)					& "'," _
			  & "'" & replace(optName,"'","''") 		& "'," _
			  & "'" & optTable 							& "'," _
			  & "'" & optType 							& "'," _
			  & "'" & optListURLField 					& "'," _
			  & "" & optSecurityLevel 					& ", " _			  
			  & "'" & replace(optDescription,"'","''")	& "'," _			  			  
			  & " " & sLogid 							& ", " _
			  & "'" & Now()		 						& "' " _
			  & ")"
		set rs = openRSexecuteDialog(mySQL)

		session(session("siteID") & "okMsg") = "Dataset was added."
			
	end if
	%>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
	
	<script type='text/javascript'>
		topWin.parent.pageReload('<% =customerID %>');
		SimpleModal.close();
    </script>	
	
<%
elseif action = "del" then

	'Delete Record from Categories_Issues
	mySQL = "DELETE FROM Dataset WHERE DatasetID=" & recId & " "
	set rs = openRSexecuteDialog(mySQL)

	session(session("siteID") & "okMsg") = "Dataset was deleted."
	response.redirect "../profiles/datasets.asp?cid=" & customerID	
	
end if
%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Dataset</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- Session variables used in Javascript functions and default Values -->
	<script language="Javascript1.2">
		<!--
		var sessionLogid=<%= session(session("siteID") & "logid")%>; //this value is used in DHTML menu,specifically the "Profile" mentu item
		//-->
	</script> 
       
	<!-- MENU TOP PAGE Sothink DHTML menu http://www.sothink.com/ -->		
	<script type="text/javascript" src="../scripts/menu/stmenu.js"></script>
	<script type="text/javascript" src="../scripts/tree/stlib.js"></script>

	<!-- DHTML Calendar http://www.dhtmlx.com -->
	<link rel="stylesheet" type="text/css" href="../_calendar/dhtmlxCalendar/codebase/dhtmlxcalendar.css">
    <link rel="stylesheet" type="text/css" href="../_calendar/dhtmlxCalendar/codebase/skins/dhtmlxcalendar_simplegrey.css">
	<script type="text/javascript" src="../_calendar/dhtmlxCalendar/codebase/dhtmlxcommon.js"></script>
	<script type="text/javascript" src="../_calendar/dhtmlxCalendar/codebase/dhtmlxcalendar.js"></script>

	<!--JQuery required components -->
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
    
</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="popup_config_datasets.asp" style="padding:0px; margin:0px;">
    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/database.png" title="Alerts" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Dataset</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Configure selected dataset below.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Alert table -->
    <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">
    
        <%
		if lCase(action) = "edit" then

			mySQL = "SELECT CustomerID, DatasetID, Name, [Table], Type, ListURLField, SecurityLevel, Description " _
				  & "FROM   Dataset " _
				  & "WHERE  DatasetID= " & recId & " "
			set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
			'check 
			if rs.eof then
				response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Dataset ID.")
			end if
						
			customerID = rs("customerid")
			optName = rs("name")
			optTable = rs("table")
			optType = rs("type")
			optListURLField = rs("listurlfield")
			optSecurityLevel = rs("securitylevel")
			optDescription = rs("description")

		end if
        %>



		<tr>
       		<td class="formLabel" style="border-top:none; margin-top:0px; padding-top:0px;"><span class="required">*</span>Name:</td>
       		<td align=left style="border-top:none; margin-top:0px; padding-top:0px;">
           		<input name="optName" id="optName" class="inputLong" style="width:285px;" value="<% =optName %>" maxlength="150" />
       		</td>
   		</tr>

		<tr>
       		<td class="formLabel">Style:</td>
       		<td align=left>
 				<select name="optType" id="optType" size=1 class="inputLong">
	                <option value="">-- Select --</option>                                    
             		<option value="Register" <%=checkMatch(optType,"Register")%>>Register</option>                                    
              		<option value="Calculated" <%=checkMatch(optType,"Calculated")%>>Calculated</option>                                        
            		<option value="SQL" <%=checkMatch(optType,"SQL")%>>SQL</option>
              		<option value="Crystal" <%=checkMatch(optType,"Crystal")%>>Crystal</option>                                        
      			</select>
       		</td>
   		</tr>

		<tr>
       		<td class="formLabel">Table/View:</td>
       		<td align=left>
				<%
				'Find Categories available TO BE assigned
				mySQL="SELECT Name FROM sys.views ORDER BY Name "                                                                    
				set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
				'build combo box
				response.write("<select name=""optTable"" id=""optTable"" size=1 class=""inputLong"">")
				response.write("<option value="""">-- Select --</option>")
				do until rs.eof			
					response.write("<option value=""" & rs("name") & """ " & checkMatch(optTable,rs("name")) & ">" & rs("name") & "</option>")
					rs.movenext
				loop
				response.write("</select>")
				call closeRS(rs)
				%>
       		</td>
   		</tr>

		<% if optType = "Register" then %>
            <tr>
                <td class="formLabel">URL Field:</td>
                <td align=left>
                    <%
                    'Find Categories available TO BE assigned
                    mySQL="SELECT * FROM " & optTable & " WHERE 1=2 "
                    set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                    'build combo box
                    response.write("<select name=""optListURLField"" id=""optListURLField"" size=1 class=""inputLong"">")
                    response.write("<option value="""">-- Select --</option>")
                    for each field in rs.fields
                        response.write("<option value=""" & field.name & """ " & checkMatch(lCase(optListURLField),lCase(field.name)) & ">" & field.name & "</option>")
                    next
                    response.write("</select>")
                    call closeRS(rs)
                    %>
                </td>
            </tr>
		<% end if %>
        
		<tr>
			<td class="formLabel">Security Level:</td>
			<td align=left>
				<input name="optSecurityLevel" id="optSecurityLevel" class="inputShort" value="<% =optSecurityLevel %>" maxlength="255" />
				<div class="subLabel" style="padding-top:5px;">Numeric entries only.</div>
			</td>
		</tr>
		<script>
			//forces numeric entries only
			jQuery('#optSecurityLevel').keyup(function () {this.value = this.value.replace(/[^0-9\.]/g,''); });
		</script>
        
     	<tr>
     		<td align="left" colspan="2">
            	<div style="color:#35487B; font-weight:bold; padding:3px;">Description</div>
      			<textarea name="optDescription" style="width:98%; height:50px; margin-bottom:12px;" ><%=server.HTMLEncode(optDescription & "")%></textarea>
             </td>
		</tr>
        
    </table>
	<!-- STOP Alert table -->

	<input name="recid" type="hidden" value="<% =recid %>">
    <input name="action" type="hidden" value="<% =action %>">    
    <input name="save" type="hidden" value="save">
    <input name="cid" type="hidden" value="<% =customerid %>">
        
	<!-- START Buttons -->
	<div style="float:right;"> 	       		
        <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); document.forms['frm'].submit();"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Buttons -->

</form>
    
<%
call closeRS(rs)
%>
    
</body>
</html>

<%
'close database connection
call closeDB()
%>

