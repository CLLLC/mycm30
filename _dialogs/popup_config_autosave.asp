<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'Work Fields
dim pageSize
dim I
dim item
dim count
dim countString
dim optConfigure

'Get action type 
dim action : action = Request.Form("action")
if len(action) = 0 then
	action = Request.QueryString("action")
end if
if lCase(action) <> "edit" then	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Action Type.")	
end if

dim modifiedDate : modifiedDate = Now()

'form variables
dim optAutoSave
	
'set sort order on save
if request.form("save") = "save" then

	'get new alert values
	optAutoSave = request.form("optAutoSave")
	
	'save existing alert
	mySQL = "UPDATE Logins Set " _
			& "AutoSave=" 		& optAutoSave 	& "," _
			& "ModifiedBy=" 	& sLogid 		& ", " _	
			& "ModifiedDate='" 	& modifiedDate 	& "' " _	
			& "WHERE  LogID=" 	& sLogid & " "   
	set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)	
	%>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
	
	<script type='text/javascript'>
		//reconfigure Autosave
		topWin.parent.configAutosave('<% =optAutoSave %>');
		//close window
		SimpleModal.close();
    </script>	

<%
end if
%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Autosave</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- Session variables used in Javascript functions and default Values -->
	<script language="Javascript1.2">
		<!--
		var sessionLogid=<%= session(session("siteID") & "logid")%>; //this value is used in DHTML menu,specifically the "Profile" mentu item
		//-->
	</script> 
       
	<!-- MENU TOP PAGE Sothink DHTML menu http://www.sothink.com/ -->		
	<script type="text/javascript" src="../scripts/menu/stmenu.js"></script>
	<script type="text/javascript" src="../scripts/tree/stlib.js"></script>

	<!--JQuery required components -->
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />
    
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

	<!-- JQuery SPINNER https://github.com/btburnett3/jquery.ui.spinner/ -->
    <link type="text/css" rel="stylesheet" href="../_jquery/spinner/ui.spinner.css" />
	<script type="text/javascript" src="../_jquery/spinner/ui.spinner.js"></script>
    
</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="popup_config_autosave.asp" style="padding:0px; margin:0px;">
    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/page_save.png" title="Alerts" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Autosave Feature</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Configure autosave feature below.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Alert table -->
    <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">
    
        <%
		if lCase(action) = "edit" then
			'open alerts table...
			mySQL = "SELECT Autosave  " _
				  & "	FROM Logins " _
				  & "	WHERE LogID = " & sLogid & " "		  				  
			set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
			if rs.eof then				
				response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid User ID.")	
			end if					
			optAutoSave = rs("Autosave")
			call closeRS(rs)
		end if
        %>

   		<tr>
       		<td style="padding-left:15px;">
            	<div>
	            	Set Autosave to occur every <input name="optAutoSave" id="optAutoSave" type="text" class="inputShort" style="width:40px;" value="<%=optAutoSave%>"> minutes.
                </div>
                <div class="subLabel" style="padding-top:5px;">
    	            <em>a setting of zero (0) disables Autosave.</em>
                </div>
            </td>
  		</tr>                            
		<script type="text/javascript">
            jQuery().ready(function($) {
                $('#optAutoSave').spinner({ min: 0, max: 60, increment: 'fast' });			
            });
        </script>

   		<tr>
       		<td>&nbsp;</td>
  		</tr>                            
        
    </table>
	<!-- STOP Alert table -->

    <input name="action" type="hidden" value="<% =action %>">
    <input name="save" type="hidden" value="save">
        
	<!-- START Buttons -->
	<div style="float:right;"> 	       		
        <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); document.forms['frm'].submit();"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Buttons -->

</form>
    
</body>
</html>

<%
'close database connection
call closeDB()
%>

