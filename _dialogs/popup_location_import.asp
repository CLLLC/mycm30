<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
Server.ScriptTimeout =25000
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, pageSize

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim countString
dim optConfigure

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and programAdmin = "N" then	
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if

dim importFile : importFile = Request.QueryString("file")

dim importDate : importDate = dateInt(Now())

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Import File</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/table_import.png" title="Issues" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">File Import</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">The status of your import is listed below.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->


	<!-- START File Import -->
	<div id="step_1" style="padding-bottom:5px;"><img id="image_1" src="../_images/myloader.gif" align="absmiddle">&nbsp;Backing up existing locations</div>
	<div id="step_2" style="padding-bottom:5px;"><img id="image_2" src="../_images/bullet_yellow.png" align="absmiddle">&nbsp;Creating temporary table</div>
	<div id="step_3" style="padding-bottom:5px;"><img id="image_3" src="../_images/bullet_yellow.png" align="absmiddle">&nbsp;Importing: <% =" " & left(importFile,50) %></div>
	<div id="step_4" style="padding-bottom:5px;"><img id="image_4" src="../_images/bullet_yellow.png" align="absmiddle">&nbsp;Applying changes and finalizing</div>
	<div id="step_5" style="padding-bottom:5px;"><img id="image_5" src="../_images/bullet_yellow.png" align="absmiddle">&nbsp;Removing temporary table</div>
	<div id="step_6" style="padding-bottom:5px;">&nbsp;</div>    

	<!-- START Buttons -->
    <div style="float:right;"> 	       		
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
    </div>    
	<!-- STOP Buttons -->

	<%
	'push to screen NOW
	response.flush()
	%>    


	<!-- STEP 1 stored procedure -- MAKE Location table BACK-UP! -->
	<script language="javascript">
		document.getElementById('image_1').src = '../_images/myloader.gif';
   	</script>    
	<%			
	response.flush()	
	set rs = openRSexecute(" EXEC sp_ImportLocations '" & customerID & "', '" & importDir & "\profiles\" & customerID & "\import" & "\location-" & importDate & "-backup.txt' ")
	%>
	<script language="javascript">
		document.getElementById('image_1').src = '../_images/check.png';
   	</script>
	<%		
	response.flush()
	%>


	<!-- STEP 2 import uploaded file -->
	<script language="javascript">
		document.getElementById('image_2').src = '../_images/myloader.gif';
   	</script>    
	<%			
	response.flush()
	'prepare sql	
	mySQL = "CREATE TABLE _temp_" & customerID & "_Import_Location_" & importDate & " ( " _
			& "[NameID] [nvarchar](255) NULL, " _
			& "[Name] [nvarchar](255) NULL, " _
			& "[Address] [nvarchar](255) NULL, " _
			& "[City] [nvarchar](255) NULL, " _
			& "[State] [nvarchar](255) NULL, " _
			& "[PostalCode] [nvarchar](255) NULL, " _
			& "[Country] [nvarchar](255) NULL, " _
			& "[Phone] [nvarchar](255) NULL, " _
			& "[Level_1] [nvarchar](255) NULL, " _
			& "[Level_2] [nvarchar](255) NULL, " _
			& "[Level_3] [nvarchar](255) NULL, " _
			& "[Level_4] [nvarchar](255) NULL, " _
			& "[Level_5] [nvarchar](255) NULL, " _
			& "[Level_6] [nvarchar](255) NULL, " _
			& "[Level_7] [nvarchar](255) NULL, " _
			& "[Level_8] [nvarchar](255) NULL, " _	
			& "[END] [nvarchar](50) NULL, " _				
			& ") "	
	set rs = openRSexecute(mySQL)
	%>
	<script language="javascript">
		document.getElementById('image_2').src = '../_images/check.png';
   	</script>
	<%		
	response.flush()
	%>


	<!-- STEP 3 import uploaded file -->
	<script language="javascript">
		document.getElementById('image_3').src = '../_images/myloader.gif';
   	</script>    
	<%			
   	on error resume next
	response.flush()	
	'prepare sql 	--  Use MAPPED DRIVE on SQL data server -- M:\profiles\aaa\import
	'original was 	-- 	FROM '" & dataDir & "\profiles\" & customerID & "\import\" & importFile & "' "
	mySQL = "BULK INSERT _temp_" & customerID & "_Import_Location_" & importDate & " " _
		  & "    FROM '" & importDir & "\profiles\" & customerID & "\import\" & importFile & "' " _
		  & "    WITH " _
		  & "    ( " _
		  & "		 FIRSTROW = 1, " _		  
		  & "        FIELDTERMINATOR = '\t', " _
		  & "		 MAXERRORS = 0, " _
		  & "        ROWTERMINATOR = '\n' " _
		  & "    )"		  
	'response.Write(mySQL)
	set rs = openRSexecute(mySQL)
	if err.number <> 0 then
		%>
		<script language="javascript">
            document.getElementById('image_3').src = '../_images/error.png';
        </script>        
        <%
		response.write("<strong>Error:</strong> " & err.Description)
		response.flush()
		response.end
	end if
	on error goto 0
	%>
	<script language="javascript">
		document.getElementById('image_3').src = '../_images/check.png';
   	</script>
	<%		
	response.flush()
	%>
       

	<!-- STEP 4 add CustomerID field -->    
	<script language="javascript">
		document.getElementById('image_4').src = '../_images/myloader.gif';
   	</script>    
	<%			
	on error resume next	
	response.flush()	
	'prepare sql	
	mySQL = "ALTER TABLE _temp_" & customerID & "_Import_Location_" & importDate & " ADD [CustomerID] [nvarchar](50) NULL, [ModifiedBy] [int] NULL, [ModifiedDate] [datetime] NULL "
	set rs = openRSexecute(mySQL)

	'update CustomerID field and replace any Double Quotes
	mySQL = "UPDATE _temp_" & customerID & "_Import_Location_" & importDate & " SET CustomerID='" & uCase(customerID) & "', Name=REPLACE(Name,CHAR(34),''), Address=REPLACE(Address,CHAR(34),''), City=REPLACE(City,CHAR(34),''), Level_1=REPLACE(Level_1,CHAR(34),''), Level_2=REPLACE(Level_2,CHAR(34),''), Level_3=REPLACE(Level_3,CHAR(34),''), Level_4=REPLACE(Level_4,CHAR(34),''), Level_5=REPLACE(Level_5,CHAR(34),''), ModifiedBy=" & sLogid & ", ModifiedDate='" & Now() & "' "
	set rs = openRSexecute(mySQL)

	'update and replace any Carriage Returns
	mySQL = "UPDATE _temp_" & customerID & "_Import_Location_" & importDate & " SET CustomerID='" & uCase(customerID) & "', Name=REPLACE(REPLACE(Name,CHAR(13),''),CHAR(10),''), City=REPLACE(REPLACE(City,CHAR(13),''),CHAR(10),''), Level_1=REPLACE(REPLACE(Level_1,CHAR(13),''),CHAR(10),''), Level_2=REPLACE(REPLACE(Level_2,CHAR(13),''),CHAR(10),''), Level_3=REPLACE(REPLACE(Level_3,CHAR(13),''),CHAR(10),''), Level_4=REPLACE(REPLACE(Level_4,CHAR(13),''),CHAR(10),''), Level_5=REPLACE(REPLACE(Level_5,CHAR(13),''),CHAR(10),''), ModifiedBy=" & sLogid & ", ModifiedDate='" & Now() & "' "
	set rs = openRSexecute(mySQL)

	'delete current locations
	mySQL = "DELETE FROM Location Where CustomerID='" & uCase(customerID) & "' "
	set rs = openRSexecute(mySQL)
	
	'append new Locations
	mySQL = "INSERT INTO Location (" _
			& "NameID, Name, Address, City, State, PostalCode, Country, Phone, Level_1, Level_2, Level_3, Level_4, Level_5, Level_6, Level_7, Level_8, CustomerID, ModifiedBy, ModifiedDate ) " _
			& "SELECT SUBSTRING(NameID, 1, 50), SUBSTRING(Name, 1, 150), SUBSTRING(Address, 1, 255), SUBSTRING(City, 1, 50), SUBSTRING(State, 1, 50), SUBSTRING(PostalCode, 1, 50), SUBSTRING(Country, 1, 150), SUBSTRING(Phone, 1, 50), SUBSTRING(Level_1, 1, 150), SUBSTRING(Level_2, 1, 150), SUBSTRING(Level_3, 1, 150), SUBSTRING(Level_4, 1, 150), SUBSTRING(Level_5, 1, 150), SUBSTRING(Level_6, 1, 150), SUBSTRING(Level_7, 1, 150), SUBSTRING(Level_8, 1, 150), CustomerID, ModifiedBy, ModifiedDate " _
			& "FROM _temp_" & customerID & "_Import_Location_" & importDate & " "
	set rs = openRSexecute(mySQL)
	if err.number <> 0 then
		%>
		<script language="javascript">
            document.getElementById('image_4').src = '../_images/error.png';
        </script>        
        <%
		response.write("<strong>Error:</strong> " & err.Description)
		response.flush()
		response.end
	end if
	on error goto 0	
	%>
	<script language="javascript">
		document.getElementById('image_4').src = '../_images/check.png';
   	</script>
	<%		
	response.flush()
	%>
    

	<!-- STEP 5 drop temp table -->    
	<script language="javascript">
		document.getElementById('image_5').src = '../_images/myloader.gif';
   	</script>    
	<%			
	on error resume next	
	response.flush()
	'get count before drop
	dim importCount
	mySQL = "SELECT CustomerID FROM _temp_" & customerID & "_Import_Location_" & importDate & " "		
	set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,0)	
	importCount = rs.recordcount	
	'prepare sql	
	mySQL = "DROP TABLE _temp_" & customerID & "_Import_Location_" & importDate & " "
	set rs = openRSexecute(mySQL)
	if err.number <> 0 then
		%>
		<script language="javascript">
            document.getElementById('image_5').src = '../_images/error.png';
        </script>        
        <%
		response.write("<strong>Error:</strong> " & err.Description)
		response.flush()
		response.end
	end if
	on error goto 0		
	%>
	<script language="javascript">
		document.getElementById('image_5').src = '../_images/check.png';
   	</script>
	<%		
	response.flush()
	%>    


    <!-- STEP 6 Finalize -->
	<script language="javascript">
		document.getElementById('step_6').innerHTML = 'Import <strong>COMPLETE</strong> [<% =importCount %> records]';
    </script>        
    <%
	response.flush()
	%>

        
</body>
</html>

<%
'close database connection
call closeDB()
%>

