<%@ Language=VBScript CodePage = 65001%>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, pageSize
dim fieldsSQL, valueSQL

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim countString

dim modifiedDate : modifiedDate = Now()

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and programAdmin = "N" then	
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if


'Get action type 
dim action : action = Request.Form("action")
if len(action) = 0 then
	action = Request.QueryString("action")
end if
if lCase(action) <> "edit" and _
   lCase(action) <> "add" and _
   lCase(action) <> "del" then	   
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Action Type.")	
end if

'Get record ID if editing or deleting
if lCase(action) = "edit" or lCase(action) = "del" then
	dim recId : recId = Request.Form("recId")
	if len(recId) = 0 then
		recId = Request.QueryString("recid")
	end if
	if len(recId) = 0 or isNull(recId) or recId = "" then			
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Question ID.")	
	end if
end if

'form variables
dim optType
dim optName
dim optValue
dim optRequired
dim optSortOrder
dim optLanguage

dim expI

'set sort order on save
if request.form("save") = "save" then

	'get new question values
	optType = request.form("optType")
	optName = request.form("optName")
	optValue = request.form("optValue")
	optRequired = request.form("optRequired")
	optSortOrder = request.form("optSortOrder")
	optLanguage = request.form("optLanguage")
		
	'save existing user field
	if action = "edit" then
	
		mySQL = "UPDATE Question Set " _
				& "Name=N'"			& replace(optName,"'","''") 	& "'," _
				& "IssueTypeID=" 	& optType						& ", " _
				& "Value=N'" 		& replace(optValue,"'","''") 	& "'," _
				& "Required='" 		& optRequired 					& "'," _
				& "SortOrder=" 		& optSortOrder 					& ", " _
				& "LanguageCD='"	& optLanguage 					& "'," _
			    & "ModifiedBy="  	& sLogid						& ", " _
			    & "ModifiedDate='"	& modifiedDate					& "' "
		'finalize update query
		mySQL = mySQL & "WHERE  QuestionID=" & recId & " AND CustomerID='" & customerID & "' "   
		'execute
		set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

		session(session("siteID") & "okMsg") = "Question has been saved successfully."
	
	'add new user field
	elseif action = "add" then
	
		mySQL = "INSERT INTO Question (" _
			  & "CustomerID, IssueTypeID, Name, Value, Required, SortOrder, Active, LanguageCD, ModifiedBy, ModifiedDate" _
			  & ") VALUES (" _
			  & "'" & uCase(customerID)				& "'," _
  			  & " " & optType						& ", " _			  
			  & "N'" & replace(optName,"'","''") 	& "'," _
			  & "N'" & replace(optValue,"'","''") 	& "'," _
			  & "'" & optRequired 					& "'," _
			  & " " & optSortOrder					& ", " _
			  & "'" & "Y"						 	& "'," _			  
			  & "'" & optLanguage 					& "'," _			  
			  & " " & sLogid 						& ", " _
			  & "'" & modifiedDate 					& "' " _
			  & ")"
		set rs = openRSexecuteDialog(mySQL)

		session(session("siteID") & "okMsg") = "Question was added."

	end if		
	%>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
	
	<script type='text/javascript'>
		topWin.parent.pageReload('<% =lCase(customerID) %>');
		SimpleModal.close();
    </script>	
	
<%
elseif action = "del" then

	'Delete Record from Categories_Issues
	'''mySQL = "DELETE FROM Question WHERE QuestionID=" & recId & " "
	'''set rs = openRSexecuteDialog(mySQL)

	mySQL = "UPDATE Question Set Active='N' "
	'finalize update query
	mySQL = mySQL & "WHERE  QuestionID=" & recId & " AND CustomerID='" & customerID & "' "   
	'execute
	set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		
	session(session("siteID") & "okMsg") = "Question was deleted."
	response.redirect "../profiles/questions.asp?cid=" & customerID	
	
end if

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Question</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>
      
	<!-- HTML List/Table resorting functions (right now own) -->
	<!-- Example usage:
    		var table = document.getElementById('table-1');
			var tableDnD = new TableDnD();
			tableDnD.init(table);  -->
	<script type="text/javascript" src="../scripts/javascript/orderList.js"></script>

	<!--JQuery required components -->
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />
    
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

	<!-- jQuery Prompt Replacements http://abeautifulsite.net/blog/2008/12/jquery-alert-dialogs/ -->
    <script src="../_jquery/prompt/jquery.alerts.js"></script>
    <link rel="stylesheet" type="text/css" href="../_jquery/prompt/jquery.alerts.css"/>

	<!-- JQuery SPINNER https://github.com/btburnett3/jquery.ui.spinner/ -->
    <link type="text/css" rel="stylesheet" href="../_jquery/spinner/ui.spinner.css" />
	<script type="text/javascript" src="../_jquery/spinner/ui.spinner.js"></script>
    
</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="popup_config_questions.asp" style="padding:0px; margin:0px;">
    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/question.png" title="Categories" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Question</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Configure selected question below.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Category table -->
    <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">
    
        <%
		if lCase(action) = "edit" then
			
			'CCI Admin/DBAdmin (bryan or steve)
			if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
				mySQL = "SELECT Question.QuestionID, Question.CustomerID, Question.IssueTypeID, Question.Name, Question.SortOrder, Question.Required, Question.Value, Question.LanguageCD " _
					  & "	FROM Question " _
					  & "	WHERE QuestionID=" & recId & " AND CustomerID='" & CustomerID & "' "
					  	  
			'everyone else
			else
				mySQL = "SELECT Question.QuestionID, Question.CustomerID, Question.IssueTypeID, Question.Name, Question.SortOrder, Question.Required, Question.Value, Question.LanguageCD " _
					  & "	FROM Question " _
					  & "	WHERE CustomerID='" & customerID & "' AND QuestionID=" & recId & " AND " _ 
					  & "			EXISTS ( " _
					  & "			SELECT vwLogins_IssueType.CustomerID, vwLogins_IssueType.LogID " _
					  & "				FROM vwLogins_IssueType " _
					  & "				WHERE (vwLogins_IssueType.CustomerID=Question.CustomerID) AND (vwLogins_IssueType.LogID=" & sLogid & ") ) "

			end if				  
			'open customer table...				  
	        set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
			
			if rs.eof then				
				response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Question ID.")	
			end if		

			optName = rs("Name")
			optType = rs("IssueTypeID")
			optValue = rs("Value")
			optRequired = rs("Required")
			optSortOrder = rs("SortOrder")
			customerID = rs("CustomerID")			
			optLanguage = rs("LanguageCD")			

		end if
        %>

		<tr>
       		<td class="formLabel"><span class="required">*</span>Name:</td>
       		<td align=left>
           		<input name="optName" id="optName" class="inputLong" style="width:285px;" value="<% =optName %>" maxlength="250" />
       		</td>
   		</tr>

		<tr>
       		<td class="formLabel"><span class="required">*</span>Issue Type:</td>
       		<td align=left>            
            	<%
				mySQL = "SELECT IssueType.IssueTypeID, IssueType.Name " _
					  & "	FROM Customer_IssueType INNER JOIN IssueType ON Customer_IssueType.IssueTypeID = IssueType.IssueTypeID " _
					  & "	WHERE Customer_IssueType.CustomerID='" & customerID & "' AND Customer_IssueType.Active='Y' " _
					  & "	ORDER BY IssueType.Name "
                set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
           		'build combo box
				response.write("<select name=""optType"" id=""optType"" size=1 class=""inputLong"">")
              	do until rs.eof							
					if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and rs("IssueTypeID") <> "3" then
						'do NOT add...cannot allow customer to add new questions in call center
					else				
						response.write("<option value='" & rs("IssueTypeID") & "' " & checkMatch(optType,rs("IssueTypeID")) & ">" & rs("Name") & "</option>")
					end if
                    rs.movenext
				loop
        	    response.write("</select>")
                call closeRS(rs)
            	%>
       		</td>
   		</tr>

        <!-- Laguange -->
		<tr>
       		<td class="formLabel">Language:</td>
           	<td align=left nowrap style="padding-bottom:15px;">
           		<select name="optLanguage" id="optLanguage" size=1 style="width:285px;">
              		<option value="en-US" selected="selected">-- Select --</option>
                   		<%						
						mySQL="SELECT Language.LanguageCD, " _
							& "	CASE WHEN (CHARINDEX('-',LanguageName) > 0) THEN  LTRIM(SUBSTRING(LanguageName,CHARINDEX('-',LanguageName)+1,LEN(LanguageName)- CHARINDEX('-',LanguageName))) ELSE LanguageName END as [Language] " _
							& "	FROM MCR_Config INNER JOIN Language ON MCR_Config.LanguageCD = Language.LanguageCD " _
							& "	WHERE MCR_Config.CustomerID = '" & customerID & "' " _
							& "	ORDER BY Language.LanguageName "						
						set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                       	do while not rs.eof								
                   			Response.Write "<option value=""" & rs("LanguageCD") & """ " & checkMatch(optLanguage,rs("LanguageCD")) & ">" & rs("Language") & "</option>"
                           	rs.movenext
                        loop
                   		call closeRS(rs)
                       	%>
           		</select>
      		</td>
		</tr>         

		<!-- Select Options -->
		<tr>
       		<td class="formLabel">
				Options:                                    
           	</td>
       		<td align=left>
				<textarea name="optValue" id="optValue" style="width:285px; height:75px;"><% =optValue %></textarea>
                <div class="subLabel">Separate each option with a carriage return.</div>
       		</td>
   		</tr>

		<!-- THIS WORKS AND IS CORRECT, I JUST DON'T HAVE THE CHECK ON ISSUE_EDIT.ASP TO MAKE SURE THE REQUIRED IS LOOKED AT -->
		<tr style="display:none;">
       		<td class="formLabel">
				Required:                                    
           	</td>
       		<td align=left>
      			<label>
              		<input type="radio" name="optRequired" id="optRequiredYes" value="Y" style="vertical-align:middle;" <%=checkRadio(optRequired,"Y")%>>Yes&nbsp;&nbsp;
         		</label>			
    
           		<label>
              		<input type="radio" name="optRequired" id="optRequiredNo" value="N" style="vertical-align:middle;" <%=checkRadio(optRequired,"N")%>>No
               	</label>
                <div class="subLabel">Required question to be populated.</div>
       		</td>
   		</tr>
        
		<tr>
       		<td class="formLabel">
				Sort Order:                                    
           	</td>
       		<td align=left>
      			<%
				'required or jQuery spinner doesn't work
				if isNull(optSortOrder) or optSortOrder = "" or len(optSortOrder) <= 0 then
					optSortOrder = "1"
				end if
				%>
           		<input name="optSortOrder" id="optSortOrder" class="inputShort" value="<% =optSortOrder %>" maxlength="255" />                                
          		<div class="subLabel" style="padding-top:5px;">Numeric entries only, no special characters</div>
       		</td>
   		</tr>
		<script type="text/javascript">
   			jQuery().ready(function($) {
     			$('#optSortOrder').spinner({ min: 1, step: 1, increment: 'fast' });			
     		});
		</script>
        
		<tr>
           	<td colspan="2" align=left nowrap style="padding:0px; margin:0px;">&nbsp;</td>
       	</tr>
        
    </table>
	<!-- STOP Category table -->

	<input name="recid" type="hidden" value="<% =recid %>">
    <input name="action" type="hidden" value="<% =action %>">    
    <input name="cid" type="hidden" value="<% =customerid %>">
    <input name="save" type="hidden" value="save">
        
	<!-- START Buttons -->
	<div style="float:right;"> 	       		
        <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); if(validateForm()==true){document.frm.submit()};"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Buttons -->

</form>
       
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script>
	function validateForm() {
		var formPassed = true;	//retunred value
		var reqFields = "optName,optType";
		var temp = reqFields.split(",");
		var i;
		var el;		
		
		for(i=0; i<temp.length; i++) {			
			if (document.getElementById(temp[i])) {			
				el = document.getElementById(temp[i])
				if (!el.value) {			
					el.style.background = '#ffff9f'; //set to yellow			
					jAlert('<strong>Form Error!</strong><br/><br/>Please ensure all required fields have been entered.', 'myCM Alert'); //inform user of error
					formPassed = false; //return error
				}
				else {				
					if (el.style.background == '#ffff9f') {
						//alert('2:' + temp[i]);
						el.style.background = '#fff';	//set back to white
					}
				}		
			}
		}
		return formPassed;
		
	}
</script>


			
