<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
Server.ScriptTimeout =25000
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, pageSize
dim fieldsSQL, valueSQL

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim countString
dim optConfigure

dim modifiedDate : modifiedDate = Now()

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 3 then	
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if

'Get action type 
dim action : action = Request.Form("action")
if len(action) = 0 then
	action = Request.QueryString("action")
end if
if lCase(action) <> "copy" then   	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Action Type.")	
end if

'form variables
dim cusName
dim cusID
dim optType
%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Copy Profile</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>
      
	<!-- HTML List/Table resorting functions (right now own) -->
	<!-- Example usage:
    		var table = document.getElementById('table-1');
			var tableDnD = new TableDnD();
			tableDnD.init(table);  -->
	<script type="text/javascript" src="../scripts/javascript/orderList.js"></script>

	<!--JQuery required components -->
    <!-- ONLY FOR suggest boxes and JSON data pulls, includes full JQuery library -->
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />
    <style>
        .ui-autocomplete-loading { background: white url('../_jquery/suggestbox/ui-anim_basic_16x16.gif') right center no-repeat; }
    </style>
    <style>
        .ui-autocomplete {
            max-height: 100px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
            /* add padding to account for vertical scrollbar */
            padding-right: 20px;
        }
        /* IE 6 doesn't support max-height
         * we use height instead, but this forces the menu to always be this tall
         */
        * html .ui-autocomplete {height: 100px;}
    </style>
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    <!-- ----------------------------------------- -->
    
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

	<!-- jQuery Prompt Replacements http://abeautifulsite.net/blog/2008/12/jquery-alert-dialogs/ -->
    <script src="../_jquery/prompt/jquery.alerts.js"></script>
    <link rel="stylesheet" type="text/css" href="../_jquery/prompt/jquery.alerts.css"/>
    
</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="popup_profile_copy.asp" style="padding:0px; margin:0px;">
    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/copy_profile.png" title="Categories" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Copy Profile</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Configure new profile below (limited = customer record only).
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<%
    'set sort order on save
    if request.form("save") = "save" and action = "copy" then

		'get new user field values
		cusName = request.form("cusName")
		cusID = request.form("cusID")	
		optType = request.form("optType")
		%>

		<!-- START File Import -->
		<div id="step_1" style="padding-bottom:5px;"><img id="image_1" src="../_images/myloader.gif" align="absmiddle">&nbsp;Copying Profile</div>
		<div id="step_6" style="padding-bottom:5px;">&nbsp;</div>    
		<div style="padding-bottom:52px; border-bottom:1px dotted #cccccc;">&nbsp;</div>
        
        <!-- START Buttons -->
        <div style="float:right; padding-top:10px;"> 	       		
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
        </div>    
        <!-- STOP Buttons -->
    
        <%
        'push to screen NOW
        response.flush()
        %>    
        
        <!-- STEP 1 stored procedure -->
        <script language="javascript">
            document.getElementById('image_1').src = '../_images/myloader.gif';
        </script>        

		<%			
        response.flush()			
		'delay/wait command - EXCELLENT!
		set rs = openRSexecute(" WAITFOR DELAY '000:00:05' SELECT '05 Second Delay' ")		

		'run copy - BU before adding replicate option
		set rs = openRSexecute(" EXEC sp_CreateProfile_New '" & customerID & "','" & cusID & "','" & cusName & "'," & sLogid & ",'" & optType & "'")	
		
        %>
        <script language="javascript">
            document.getElementById('image_1').src = '../_images/check.png';
        </script>
        <%		
        response.flush()
        %>        

        <!-- STEP 6 Finalize -->
        <script language="javascript">
            document.getElementById('step_6').innerHTML = 'Profile <strong>COMPLETE</strong><br><br>Please wait. Redirecting to new profile...';
        </script>        
        <%
        response.flush()
		'delay/wait command - EXCELLENT!
		set rs = openRSexecute(" WAITFOR DELAY '000:00:02' SELECT '02 Second Delay' ")				
        %>

		<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
		<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
		
		<script type='text/javascript'>
			topWin.parent.window.location = "../profiles/profiles_menu.asp?cid=<% =cusID %>"
			SimpleModal.close();
		</script>	
		
		<%	
    else
        %>
	
        <!-- START Category table -->
        <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">
        
            <tr>
                <td class="formLabel"><span class="required">*</span>Type:</td>
                <td align=left>
                    <select name="optType" id="optType" size=1 class="inputMedium" style="width:175px;">
                        <option value="N">Copy (limited)</option>                                    
                        <option value="Y">Replicate (full)</option>                                        
                    </select>
                </td>
            </tr>

            <tr>
                <td class="formLabel"><span class="required">*</span>Name:</td>
                <td align=left>
                    <input name="cusName" id="cusName" class="inputLong" style="width:285px;" value="" maxlength="255" />
                </td>
            </tr>
    
           <tr>
                <td class="formLabel"><span class="required">*</span>ID:</td>                              
                <td align=left nowrap>
                    <input name="cusID" id="cusID" class="inputShort" value="" style="text-transform: uppercase;" maxlength="5" />
                    <div class="subLabel" style="padding-top:5px;">Used when generating issue #'s: 1106-[<strong>ID</strong>]-10001-01</div>
                </td>
            </tr>
    
            <tr>
                <td colspan="2" align=left nowrap style="padding:0px; margin:0px;">&nbsp;</td>
            </tr>
            
        </table>
        <!-- STOP Category table -->
    
        <input name="action" type="hidden" value="copy">    
        <input name="cid" type="hidden" value="<% =customerid %>">
        <input name="save" type="hidden" value="save">
            
        <!-- START Buttons -->
        <div style="float:right;"> 	       		
            <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); if(validateForm()==true){document.frm.submit()}; return false;"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
        </div>
        <!-- STOP Buttons -->
    
		<%	
    end if
    %>

</form>
        
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script>
	function validateForm() {
		var formPassed = true;	//retunred value
		var reqFields = "cusName,cusID";
		var temp = reqFields.split(",");
		var i;
		var el;		
		
		for(i=0; i<temp.length; i++) {			
			if (document.getElementById(temp[i])) {			
				el = document.getElementById(temp[i])
				if (!el.value) {			
					el.style.background = '#ffff9f'; //set to yellow			
					jAlert('<strong>Form Error!</strong><br/><br/>Please ensure all required fields have been entered.', 'myCM Alert'); //inform user of error
					formPassed = false; //return error
				}
				else {				
					if (el.style.background == '#ffff9f') {
						//alert('2:' + temp[i]);
						el.style.background = '#fff';	//set back to white
					}
				}		
			}
		}
		return formPassed;
		
	}	
</script>

<script>
	//JSON QUERY BUILDER used to search Customer ID's
	buildJQuery('customer','customerid','cusID');

	function buildJQuery(table,field,form_field) {
																
				$(function() {
					function split( val ) {
						return val.split( /,\s*/ );						
					}
					function extractLast( term ) {
						return split( term ).pop();						
					}                                                            
					$("#"+form_field)
						// don't navigate away from the field on tab when selecting an item
						.bind( "keydown", function( event ) {
                      		if ( event.keyCode === $.ui.keyCode.TAB &&
                            	$( this ).data( "autocomplete" ).menu.active ) {
                               		event.preventDefault();
                             }							 
                        })
                     	.autocomplete({
                      		source: function( request, response ) {
                          		$.getJSON( "../_jquery/suggestBox/json-data.asp?view="+table+"&field="+field+"&cid=<% =customerID %>", {
                             		term: extractLast( request.term )
                           		}, response );
                          	},
                       		search: function() {
                        		// custom minLength
                             	var term = extractLast( this.value );
                           		if ( term.length < 1 ) {
                            		return false;
                         		}								
                      		},
                      		focus: function() {
                    			// prevent value inserted on focus
                      			return false;
                         	}
                  		});
				});					

	}
</script>                           
			
