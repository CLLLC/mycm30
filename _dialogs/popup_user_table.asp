<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'Database
dim mySQL, cn, rs, count

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim pageView : pageView = Request.QueryString("pageView")

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>myCM Audit Log</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- HTML Table Filter Generator http://tablefilter.free.fr/ -->
    <link rel="stylesheet" type="text/css" href="../_tablefilter/filtergrid.css"/>
	<script type="text/javascript" src="../_tablefilter/tablefilter_all.js"></script>
	<script type="text/javascript" src="../_tablefilter/sortabletable.js"></script>
    <script type="text/javascript" src="../_tablefilter/tfAdapter.sortabletable.js"></script>        

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/group.png" title="Audit Log" width="24" height="24" align="absmiddle"> 
   	   	<span class="popupTitle">Contacts & Users</span>
   	</div>                   

	<%
	'------------------------
	'get directive count
	'------------------------	
	mySQL="SELECT count([LogID]) AS [count] " _			 
		 & "	FROM Logins " _
		 & "	WHERE CustomerID = '" & customerID & "' "
	'open record set
	set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)		
	count = rs("count")
	call closeRS(rs)
	'------------------------	
		
	'there are more then 2,000 locations, STOP and rethink...
	if count>2000 then	
		response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
		response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
        call systemMessageBox("userMessage","statusMessageALERT","Over 2,000 users records returned. Please contact CCI Support at support@ccius.com.")
		response.write("   </td>")
		response.write("</tr></table>")			
	else
	%>

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Select a user to view their details.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Locations table -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
    
        <%
		'================================================
		'get users from LOGINS table
		'================================================
		mySQL = "SELECT Logins.LastName + ', ' + Logins.FirstName AS [Contact], Logins.Priority, Logins.Phone_Work AS [Work], Logins.Phone_Home AS [Home], Logins.Logid, " _
			  & "	CASE Logins.Active WHEN 'Y' THEN 'Enable' WHEN 'C' THEN 'Contact' ELSE 'Disabled' END AS [Active] " _
			  & "	FROM Logins " _
			  & "	WHERE CustomerID = '" & customerID & "' AND CASE Logins.Active WHEN 'Y' THEN 'Enable' WHEN 'C' THEN 'Contact' ELSE 'Disabled' END <> 'Disabled' AND Priority>' ' "

		'================================================
		'================================================
		'UNION QUERY - users linked from other profiles
		mySQL = mySQL & " UNION ALL "
		'================================================		
		'================================================
		
		'get users from linked GROUPS
		mySQL = mySQL & " SELECT Logins.LastName + ', ' + Logins.FirstName AS [Contact], Logins.Priority, Logins.Phone_Work AS [Work], Logins.Phone_Home AS [Home], Logins.Logid, " _
			  		  & "	CASE Logins.Active WHEN 'Y' THEN 'Enable' WHEN 'C' THEN 'Contact' ELSE 'Disabled' END AS [Active] " _
					  & "	FROM Logins INNER JOIN Logins_Groups ON Logins.LOGID = Logins_Groups.LOGID INNER JOIN Groups ON Logins_Groups.GroupID = Groups.GroupID " _
					  & "	WHERE (Logins.CustomerID <> '" & customerID & "') AND (Groups.CustomerID = '" & customerID & "') " _
					  &	"		AND CASE Logins.Active WHEN 'Y' THEN 'Enable' WHEN 'C' THEN 'Contact' ELSE 'Disabled' END <> 'Disabled' AND Priority>' ' "

		'================================================
		'ORDER BY  - union query
		'================================================		
		mySQL = mySQL & " ORDER BY Logins.Priority, Contact "
		%>
        
        <tr>
        	<td>
			<%
			dim tableProperty
			'set _tablefilter/default.asp settings.
            tableProperty = "sort: true, sort_config:{sort_types:['String','String','String','String']}, filters_row_index: 1, " _
                          & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
                          & "paging: true, paging_length: 5, " _
						  & "highlight_keywords: true, " _
						  & "status_bar: true, col_0: ""input"", col_1: ""select"", col_2: """", col_3: """", col_4: ""select"", " _						  
                          & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", " _
                          & "or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
                          & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"
			
			'make call to build grid found in ../_tablefilter/default.asp
			call buildGrid("contactTableID",mySQL,"","contact","../_images/icons/16/magnifier.png",tableProperty) 
        	%>
        	</td>
        </tr>
    </table>
	<!-- STOP Locations table -->

	<!-- START Cancel Button -->
	<div style="padding-top:5px; margin-bottom:0px;">
    	<% if pageView = "issue" or pageView = "issue:00" then %>
			<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); topWin.parent.StartTheTimer(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>
		<% else %>
			<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>
		<% end if %>        
    </div>
	<!-- STOP Cancel Button -->

	<%
	end if
	%>    
     
    
</body>
</html>

<%
'close database connection
call closeDB()
%>
