<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, rsTemp

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim pageSize
dim totalPages
dim showArr
dim sortField
dim sAddress

dim curPage
dim showGroupID, showPhrase
dim showField
dim showStart

dim tempUser, tempGroup
dim showRule 
					
'get issue # -- optional since could be called from Add Issue
dim idIssue : idIssue = trim(Request.QueryString("recId"))

dim pageView : pageView = trim(Request.QueryString("pageView"))


dim multiSelect
multiSelect = trim(Request.QueryString("multi"))
if len(multiSelect) <=0 then
	multiSelect = Request.Form("multi")		
end if


dim useRule
useRule = trim(Request.QueryString("rule"))
if len(useRule) <=0 then
	useRule = Request.Form("rule")		
end if
'could be coming from Reports owner assignment
if len(useRule) <=0 then
	useRule = "false"
end if


'Get showGroupID
showGroupID = Request.Form("showGroupID")				'Form
if len(showGroupID) = 0 then
	showGroupID = Request.QueryString("showGroupID")	'QueryString
end if


'Get showPhrase
showPhrase = Request.Form("showPhrase")					'Form
if len(showPhrase) = 0 then
	showPhrase = Request.QueryString("showPhrase")		'QueryString
end if


'Check for the existence of rules
'The lenght 62 refers to := <?xml version='1.0' encoding='ISO-8859-1'?><filters></filters>
'which checks for blank filter
mySQL = "SELECT Count(RuleID) AS [RuleCount] FROM [Rule] WHERE CustomerID='" & customerID & "' AND len(RuleXML)>62 "
'open rule sets
set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
if rs("RuleCount") > 0 then
	showRule = "Y"
else
	showRule = "N"
end if
'rules were found and NO search has been performed default to RULES
if len(showGroupID)=0 or showGroupID="" or isNull(showGroupID) then
	if showRule = "Y" then
		showGroupID = "rule"
	else
		showGroupID = "allusers"	
	end if
end if

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Choose a User</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />

	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
    	<% if lCase(multiSelect) = "false" then %>
	   		<img src="../_images/icons/24/user.png" title="Issues" align="absmiddle"> 
	       	<span class="popupTitle">Owner</span>            
        <% else %>
	   		<img src="../_images/icons/24/group.png" title="Issues" align="absmiddle"> 
	       	<span class="popupTitle">Case Managers</span>            
            <% end if %>        
   	</div>                   


	<!-- START Search box -->
    <form method="post" action="popup_user.asp?recid=<% =idIssue %>&cid=<% =customerID %>&pageView=<% =pageView %>" name="frm" style="padding:0px; margin:0px;">
	<table class="formTable" style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
                <fieldset style="border:1px solid #CCCCCC; background-color:#FFFFFF; padding:0px;">
                    <legend>Search</legend>
                        <div align="left" style="padding:5px;">
                            <img src="../_images/icons/16/search.png" title="Search" width="16" height="16" align="absmiddle" style="margin-right:3px;">
                            <!-- ALL profile groups -->
                            <select name="showGroupID" id="showGroupID" size=1 style="width:125px;">
	                            <% 
								if lCase(multiSelect) = "false" and lCase(useRule) = "false" then 
									'do NOT add rule based option
								else
									if showRule = "Y" then
										response.write("<option value=""rule"" " & checkMatch(showGroupID,"rule") & ">[Rule Based]</option>")
									end if
								end if
								%>
                                <option value="allusers" <% =checkMatch(showGroupID,"allusers") %>>[All Groups]</option>
                                    <%
                                    mySQL="SELECT GroupID, Name " _
                                        & "		FROM Groups " _
                                        & "	  	WHERE CustomerID = '" & customerID & "' " _
                                        & "ORDER By Name "
                                    set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                    do while not rs.eof								
                                        Response.Write "<option value=""" & rs("groupid") & """ " & checkMatch(showGroupID,rs("groupid")) & ">-" & rs("name") & "</option>"
                                        rs.movenext
                                    loop
                                    call closeRS(rs)
                                    %>
                            </select>	               
                            <!-- search user list -->                                                                                                       
                            <input type=text name="showPhrase" id="showPhrase" style="width:100px;" size=20 maxlength=50 value="<%=showPhrase%>">                            
							<input type="submit" name="btnSearch" id="btnSearch" value="Search" />
							<input type="button" name="btnClear" id="btnClear" value="Clear" onClick="this.form.showGroupID.value = ''; this.form.showPhrase.value = ''; this.form.submit();" />                            
                        </div>            
                </fieldset>               
               	<input name="multi" type="hidden" value="<% =multiSelect %>">
			</td>
        </tr>
    </table>	
    </form>
	<!-- STOP Search box -->


	<!-- START User/Contact table -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    
        <%
		'Rule Based assignments
		if showGroupID = "rule" then

			dim assignRule

			mySQL = "SELECT RuleID, RuleXML FROM [Rule] WHERE CustomerID = '" & customerID & "' AND len(RuleXML)>0 "
			
			'open rule sets
			set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
			'move through all available rules for customer
			do while not rs.eof
				'XML rule where clause
				'showRule = rs("RuleXML")						 
				'build custom SQL statement with user defined XML filter, used by the user to limit reports returned
				mySQL = BuildQuery("Issues", "[Issue #]", "", "", rs("RuleXML"), "vwIssues_Rule_Assignment", "Register","","")			
				mySQL = mySQL & " AND vwIssues_Rule_Assignment.[Issue #]='" & idIssue & "' "				
				mySQL = mySQL & " GROUP BY [Issue #] "												
				'see if issue mathces
				set rsTemp = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
				if rsTemp.eof then
					'nothing
				else			
					assignRule = assignRule & rs("RuleID") & ","
				end if
				rs.movenext
			loop
			
			'good-to-go pull in the assingable users
			if len(assignRule) > 0 then
				'Find Users available TO BE assigned
				mySQL = "SELECT Logins.LOGID, Logins.CustomerID AS [Logins.CustomerID], Logins.FirstName, Logins.LastName, Logins.Email, Logins.Priority, Logins.Active " _
					  & "FROM Logins INNER JOIN Logins_Rule ON Logins.LOGID = Logins_Rule.LOGID INNER JOIN [Rule] ON Logins_Rule.RuleID = [Rule].RuleID " _
					  & "WHERE [Rule].RuleID IN (" & left(assignRule,len(assignRule)-1) & ") "
						  
				'allow all users if called from Owner changes...
				mySQL = mySQL & " AND NOT EXISTS " _
							  & "      (SELECT CRS_Logins.LogID " _
							  & "       FROM   CRS_Logins " _
							  & "       WHERE  Logins.LogID = CRS_Logins.LogID " _
							  & "		  AND 	 CRS_Logins.CRSID = '" & idIssue & "' ) "			  
		
				'DBAdmins and CCI Admins
				if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
					mySQL = mySQL & " AND Active<>'N' AND SecurityLevel>10 "
				  
				'CCI staff RS/RA
				elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
					mySQL = mySQL & " AND Active<>'N' AND SecurityLevel>10 AND Priority>' ' "
		
				'don't allow customers to see disabled users
				else
					mySQL = mySQL & " AND Active<>'N' AND SecurityLevel>10 "
					
				end if		  

				'Search Phrase
				if len(showPhrase) > 0 then
					mySQL = mySQL & " AND ( (Logins.FirstName LIKE '%" & replace(showPhrase,"'","''") & "%') OR (Logins.LastName LIKE '%" & replace(showPhrase,"'","''") & "%') ) "
				end if							

				'add grouping to prevent same user listed multiple times
				mySQL = mySQL & " GROUP BY Logins.LOGID, Logins.CustomerID, Logins.FirstName, Logins.LastName, Logins.Email, Logins.Priority, Logins.Active "
				mySQL = mySQL & " HAVING (Logins.Active <> 'N') "
			
			else
				'no rules or no users found to be assigned
				mySQL = "SELECT Logins.LOGID FROM Logins WHERE 1=2 "
			
			end if
		
		'----------------------------
		'non-Rule Based assignments	
		'----------------------------			
		else

			'Find Users available TO BE assigned
			mySQL = "SELECT Logins.LOGID, Logins.CustomerID AS [Logins.CustomerID], Logins.FirstName, Logins.LastName, Logins.Email, Logins.Priority, Logins.Active " _
				  & "FROM Logins " _
				  & "WHERE  Logins.CustomerID = '" & customerID & "' "			  
			'allow all users if called from Owner changes...
			if lCase(multiSelect) = "false" then
				mySQL = mySQL & " AND Active='Y' "
			else
				mySQL = mySQL & " AND NOT EXISTS " _
							  & "      (SELECT CRS_Logins.LogID " _
							  & "       FROM   CRS_Logins " _
							  & "       WHERE  Logins.LogID = CRS_Logins.LogID " _
							  & "		  AND 	 CRS_Logins.CRSID = '" & idIssue & "' ) "			  
			end if
	
			'DBAdmins and CCI Admins
			if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
				'owner setting for CCI user
				if pageView="reports" then
					mySQL = mySQL & " AND Active<>'N' "
				'all other pages, only show specific customer list
				else
					mySQL = mySQL & " AND Active<>'N' AND SecurityLevel>10 "
				end if
			  
			'CCI staff RS/RA
			elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
				mySQL = mySQL & " AND Active<>'N' AND SecurityLevel>10 AND Priority>' ' "
	
			'don't allow customers to see disabled users
			else
				mySQL = mySQL & " AND Active<>'N' AND SecurityLevel>10 "
				
			end if		  
			  
			'finish sql statement
			mySQL = mySQL & " #ADD_GROUP# #ADD_SEARCH# "
			  
			'Search Group
			if len(showGroupID) > 0 and showGroupID <> "allusers" and showGroupID <> "rule" then
				tempGroup = " AND EXISTS " _
						  & "      (SELECT Logins_Groups.LogID " _
						  & "       FROM   Logins_Groups " _
						  & "       WHERE  Logins.LogID = Logins_Groups.LogID " _
						  & "		  AND 	 Logins_Groups.GroupID = " & showGroupID & " ) "
				mySQL = replace(mySQL,"#ADD_GROUP#", tempGroup) 			
			else
				mySQL = replace(mySQL,"#ADD_GROUP#", "")
			end if							
	
			'Search Phrase
			if len(showPhrase) > 0 then
				mySQL = replace(mySQL,"#ADD_SEARCH#", " AND ( (Logins.FirstName LIKE '%" & replace(showPhrase,"'","''") & "%') OR (Logins.LastName LIKE '%" & replace(showPhrase,"'","''") & "%') ) ") 
			else
				mySQL = replace(mySQL,"#ADD_SEARCH#", "")
			end if							
	
			'================================================
			'================================================
			'UNION QUERY - users linked from other profiles
			mySQL = mySQL & " UNION ALL "
			'================================================		
			'================================================
					
			mySQL = mySQL & " SELECT Logins.LOGID, Logins.CustomerID AS [Logins.CustomerID], Logins.FirstName, Logins.LastName, Logins.Email, Logins.Priority, Logins.Active " _
						  & "	FROM Logins INNER JOIN Logins_Groups ON Logins.LOGID = Logins_Groups.LOGID INNER JOIN Groups ON Logins_Groups.GroupID = Groups.GroupID " _
						  & "	WHERE (Logins.CustomerID <> '" & customerID & "') AND (Groups.CustomerID = '" & customerID & "') "
			'allow all users if called from Owner changes...
			if lCase(multiSelect) = "false" then
				mySQL = mySQL & " AND Active='Y' "
			else
				mySQL = mySQL & " AND NOT EXISTS " _
							  & "      (SELECT CRS_Logins.LogID " _
							  & "       FROM   CRS_Logins " _
							  & "       WHERE  Logins.LogID = CRS_Logins.LogID " _
							  & "		  AND 	 CRS_Logins.CRSID = '" & idIssue & "' ) "			  
			end if
	
			'DBAdmins and CCI Admins
			if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
				mySQL = mySQL & " AND Active<>'N' AND SecurityLevel>10 "
			  
			'CCI staff RS/RA
			elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
				mySQL = mySQL & " AND Active<>'N' AND SecurityLevel>10 AND Priority>' ' "
	
			'don't allow customers to see disabled users
			else
				mySQL = mySQL & " AND Active<>'N' AND SecurityLevel>10 "
				
			end if		  
			  
			'finish sql statement
			mySQL = mySQL & " #ADD_GROUP# #ADD_SEARCH# "
			  
			'Search Group
			if len(showGroupID) > 0 and showGroupID <> "allusers" and showGroupID <> "rule" then
				tempGroup = " AND EXISTS " _
						  & "      (SELECT Logins_Groups.LogID " _
						  & "       FROM   Logins_Groups " _
						  & "       WHERE  Logins.LogID = Logins_Groups.LogID " _
						  & "		  AND 	 Logins_Groups.GroupID = " & showGroupID & " ) "
				mySQL = replace(mySQL,"#ADD_GROUP#", tempGroup) 			
			else
				mySQL = replace(mySQL,"#ADD_GROUP#", "")
			end if							
	
			'Search Phrase
			if len(showPhrase) > 0 then
				mySQL = replace(mySQL,"#ADD_SEARCH#", " AND ( (Logins.FirstName LIKE '%" & replace(showPhrase,"'","''") & "%') OR (Logins.LastName LIKE '%" & replace(showPhrase,"'","''") & "%') ) ") 
			else
				mySQL = replace(mySQL,"#ADD_SEARCH#", "")
			end if							
	
			'================================================
			'ORDER BY  - union query
			'================================================		
			'CCI Staff needed
			if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
				mySQL = mySQL & " ORDER BY Logins.Priority, Logins.LastName, Logins.FirstName "
			'all others
			else			
				mySQL = mySQL & " ORDER BY Logins.LastName, Logins.FirstName "		  
			end if

		'----------------------------------
		'END of non-Rule Based assignments
		'----------------------------------
		end if		


        'Open recordset
        set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
		
		
        if rs.eof then
        %>
            <tr>
                <td align=center valign=middle>
                <select name="listUser" id="listUser" style="width:100%; height:170px; margin:0px; margin-bottom:8px;" disabled="disabled" multiple="multiple" >
                	<option value="">No Users Found.</option>
                </select>
                </td>
            </tr>
        <%
        elseif rs.recordcount > 500 then
        %>	
            <tr>
                <td align=center valign=middle>
                <select name="listUser" id="listUser" style="width:100%; height:170px; margin:0px; margin-bottom:8px;" disabled="disabled" multiple="multiple" >
                	<option value="">More than 500 users where found.</option>
                	<option value="">Use the Search box to limit results.</option>
                </select>
                </td>
            </tr>
        <%
        else
        %>	        
            <tr>
                <td>
                                
                    <!-- START list of available users -->
                    <%
					if lCase(multiSelect) = "false" then 
						if lCase(pageView) = "reports" then
							response.write("<select name=""listUser"" id=""listUser"" ondblclick=""setReportOwner('listUser');"" style=""width:100%; height:200px; margin:0px; margin-bottom:5px;"" size=""14"">")
						else
							response.write("<select name=""listUser"" id=""listUser"" ondblclick=""setIssueOwner('listUser');"" style=""width:100%; height:200px; margin:0px; margin-bottom:5px;"" size=""14"">")			
						end if
					else 
						response.write("<select name=""listUser"" id=""listUser"" ondblclick=""setUserList('listUser');"" style=""width:100%; height:200px; margin:0px; margin-bottom:5px;"" multiple=""multiple"">")								
					end if 
										
					do while not rs.eof
					
						if rs("Active") = "C" then
							tempGroup = " (contact only)"
						else
							tempGroup = ""
						end if
					
'						if len(rs("FirstName")) > 0 then
'							tempUser = rs("LastName") & ", " & rs("FirstName") & tempGroup
'						else
'							tempUser = rs("LastName") & tempGroup
'						end if

                        'NAME format
						tempUser = ""
						if len(rs("lastname")) <= 0 or rs("lastname") = "" or isNull(rs("lastname")) then
							tempUser = rs("firstname") & tempGroup
						elseif len(rs("firstname")) <= 0 or rs("firstname") = "" or isNull(rs("firstname")) then
							tempUser = rs("lastname") & tempGroup
						elseif (len(rs("lastname")) <= 0 and len(rs("firstname")) <= 0) or (rs("lastname") = "" and rs("firstname") = "") or (isNull(rs("lastname")) and isNull(rs("firstname"))) then
							tempUser = "<empty>" & tempGroup
						else
							tempUser = rs("lastname") & ", " & rs("firstname") & tempGroup
						end if						

						'show CCI staf priority contact
						if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
							if len(rs("Priority")) > 0 then
								tempUser = rs("Priority") & ": " & tempUser
							end if
						end if

						'show linked from different profile
						if lCase(customerID) <> lCase(rs("Logins.CustomerID")) then tempUser = tempUser & " ([linked:" & rs("Logins.CustomerID") & "])"

                        response.Write("<option value=""" & rs("logID") & """>" & tempUser & "</option>")            
                        rs.movenext
                    loop
					
					response.write("</select>")
                    %>                    
                    <!-- STOP list of available users -->
                                        
                </td>
            </tr>        
        <%
        end if
        %>
        
    </table>
	<!-- STOP User/Contact table -->
	
	<!-- START Buttons -->
	<div style="float:left;">
        <% 
		if lCase(multiSelect) = "true" then
			response.write("Select: <a href=""javascript:selectAllOptions('listUser',true);"">All</a> | <a href=""javascript:selectAllOptions('listUser',false);"">None</a>")
		else
			response.write("&nbsp;")
		end if
		%>
	</div>
	<div style="float:right;">
    	<% 
		if lCase(multiSelect) = "false" then 
			if lCase(pageView) = "reports" then
				%>    
	        	<a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); javascript:setReportOwner('listUser');"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
  				<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
				<%
			else
				%>
				<a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); javascript:setIssueOwner('listUser');"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
				<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); topWin.parent.StartTheTimer();"><span class="cancel"><u>C</u>lose</span></a>
				<%
			end if
    	else 
			%>    
	        <a class="myCMbutton" href="#" accesskey="A" onClick="this.blur(); javascript:setUserList('listUser');"><span class="add" style="padding-right:10px;"><u>A</u>dd</span></a>
    		<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); topWin.parent.StartTheTimer();"><span class="cancel"><u>C</u>lose</span></a>
			<% 
		end if 
		%>
	</div>
	<!-- STOP Buttons -->
    
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script language="javascript">	
	// dependent on SimpleModal.js
	// note: topWin.parent is defined in SimpleModal.js and replaces "window.opener"
	function setUserList(obj) {
		//add selected options to parent listUser
		var el = document.getElementById(obj);
		//make sure something was selected
		if (el!=null && el.options!=null) {			
			//cycle through all selected
			for(var i=0; el.options.length >i; i++) {
				if (el.options[i].selected == true) {
					topWin.parent.setUserList(el.options[i].text,el.options[i].value);						
				}
			}
		}		

		//restart autosave timer
		topWin.parent.StartTheTimer();

		//close window
		SimpleModal.close();
	}		

	// dependent on SimpleModal.js
	// note: topWin.parent is defined in SimpleModal.js and replaces "window.opener"
	function setIssueOwner(obj) {
		//add selected options to parent listUser
		var el = document.getElementById(obj);
		//make sure something was selected
		if (el!=null && el.options!=null) {			
			//cycle through all selected
			for(var i=0; el.options.length >i; i++) {
				if (el.options[i].selected == true) {
					topWin.parent.setOwner(el.options[i].text,el.options[i].value);						
				}
			}
		}		

		//restart autosave timer
		topWin.parent.StartTheTimer();

		//close window
		SimpleModal.close();
	}		

	// dependent on SimpleModal.js
	// note: topWin.parent is defined in SimpleModal.js and replaces "window.opener"
	function setReportOwner(obj) {
		//add selected options to parent listUser
		var el = document.getElementById(obj);
		//make sure something was selected
		if (el!=null && el.options!=null) {			
			//cycle through all selected
			for(var i=0; el.options.length >i; i++) {
				if (el.options[i].selected == true) {
					topWin.parent.setOwner(el.options[i].text,el.options[i].value);						
				}
			}
		}		

		//close window
		SimpleModal.close();
	}		
</script>

