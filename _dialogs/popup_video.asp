<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, rsFUP

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************
	
%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>myCM Videos</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    

	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/chart.png" title="Issues" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">myCM Videos</span>
   	</div>                   


	<table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>    
			<td align=left>
           		<object width="664" height="502" id="wistia_532064" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"><param name="movie" value="http://embed.wistia.com/flash/embed_player_v1.1.swf"/><param name="allowfullscreen" value="true"/><param name="allowscriptaccess" value="always"/><param name="wmode" value="opaque"/><param name="flashvars" value="videoUrl=http://embed.wistia.com/deliveries/938b0a8f4c71793267c613ecabc715593764636e.bin&stillUrl=http://embed.wistia.com/deliveries/dfd16039f6c17acc3e398a8348413d317df26cde.bin&unbufferedSeek=true&controlsVisibleOnLoad=true&autoPlay=true&endVideoBehavior=reset&playButtonVisible=true&embedServiceURL=http://distillery.wistia.com/x&accountKey=wistia-production_7843&mediaID=wistia-production_532064&mediaDuration=98.67"/><embed src="http://embed.wistia.com/flash/embed_player_v1.1.swf" width="664" height="502" name="wistia_532064" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" wmode="opaque" flashvars="videoUrl=http://embed.wistia.com/deliveries/938b0a8f4c71793267c613ecabc715593764636e.bin&stillUrl=http://embed.wistia.com/deliveries/dfd16039f6c17acc3e398a8348413d317df26cde.bin&unbufferedSeek=true&controlsVisibleOnLoad=true&autoPlay=true&endVideoBehavior=reset&playButtonVisible=true&embedServiceURL=http://distillery.wistia.com/x&accountKey=wistia-production_7843&mediaID=wistia-production_532064&mediaDuration=98.67"></embed></object><script src="http://embed.wistia.com/embeds/v.js" charset="ISO-8859-1"></script><script>if(!navigator.mimeTypes['application/x-shockwave-flash'] || navigator.userAgent.match(/Android/i)!==null)Wistia.VideoEmbed('wistia_532064',664,502,{videoUrl:'http://embed.wistia.com/deliveries/6c9cb2be83c373f2d100200e508c3b0ced9b4644.bin',stillUrl:'http://embed.wistia.com/deliveries/dfd16039f6c17acc3e398a8348413d317df26cde.bin',distilleryUrl:'http://distillery.wistia.com/x',accountKey:'wistia-production_7843',mediaId:'wistia-production_532064',mediaDuration:98.67})</script>
	        </td>
    	</tr>
	</table>        
        

    <table class="formTable" style="width:100%;" border="0" cellspacing="0" cellpadding="0">        
		<tr>
			<td style="border:none; padding:0px; margin:0px;">&nbsp;</td>
      	</tr>            
		<tr>
			<td style="padding:0px; margin:0px;">&nbsp;</td>
      </tr>            
	</table>	

   	<!-- START Buttons -->
    <div> 	       		
    	<a class="myCMbutton" href="#" onClick="this.blur(); SimpleModal.close();" style="float:right;"><span class="cancel">Close</span></a>
    </div>
    <!-- STOP Buttons -->

</body>

</html>

<%
'close database connection
call closeDB()
%>

