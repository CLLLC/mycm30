<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement, Pro, Lite, Metrics
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%
'Database
dim mySQL, cn, rs, field, count

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim historyTable
historyTable = trim(Request.QueryString("hist"))
if historyTable = "" then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid History Table.")
else
	'to remove the "hist-" in "hist-issues"
	historyTable = trim(mid(historyTable, 6, len(historyTable)))
end if

dim idHistory
idHistory = trim(Request.QueryString("recId"))
if idHistory = "" then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid History ID.")
end if

'--------------------------------------------------
'get history record...
'--------------------------------------------------	
if lCase(historyTable) = "issue" then
	mySQL = "SELECT * " _
		  & "	FROM vwHistory_CRS " _
		  & "	WHERE  [History ID]=" & idHistory & " "				
			
elseif lCase(historyTable) = "category" then
	mySQL = "SELECT * " _
		  & "	FROM vwHistory_CRS_Category " _
		  & "	WHERE  [History ID]=" & idHistory & " "					

elseif lCase(historyTable) = "investigation" then
	mySQL = "SELECT * " _
		  & "	FROM vwHistory_Investigation " _
		  & "	WHERE  [History ID]=" & idHistory & " "					
							
elseif lCase(historyTable) = "managers" then
	mySQL = "SELECT * " _
		  & "	FROM vwHistory_CRS_Logins " _
		  & "	WHERE  [History ID]=" & idHistory & " "					
							
elseif lCase(historyTable) = "e-mails" then
	mySQL = "SELECT * " _
		  & "	FROM vwHistory_MailLog " _
		  & "	WHERE  [History ID]=" & idHistory & " "			

elseif lCase(historyTable) = "resolutions" then
	mySQL = "SELECT * " _
		  & "	FROM vwHistory_Resolution " _
		  & "	WHERE  [History ID]=" & idHistory & " "					
			
else
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid History table.")		  		
end if
'open recordset   			  
set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
if rs.eof then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid History ID.")
end if
'--------------------------------------------------	

'Determin TinyMCE to Initialize...append all memo fields
dim activeElements

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>myCM Audit Logs</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    

	<style type="text/css">
    <!--
    .sectionHeader {
       padding:3px !important; 
	   margin:0px !important; 
	   background-color:#E2E2E2 !important; 
	   font-weight:bold !important; 
	   border-top:1px solid #5B5B5B !important; 
	   border-bottom:1px solid #5B5B5B !important;
       }
	 .printTable td {
		vertical-align:top;
		border-top:1px dotted #cccccc;
		padding-top:8px;
		padding-bottom:10px;
	 }	   

	.printTable .printLabel 			{text-align:right; font-weight:bold; color:#35487B; padding-right:6px; width:125px; white-space:normal; text-decoration: none;}
	.printTable .printLabel:link 		{text-align:right; font-weight:bold; color:#35487B; padding-right:6px; width:125px; white-space:normal; text-decoration: none;}
	.printTable .printLabel:link:hover 	{text-align:right; font-weight:bold; color:#35487B; padding-right:6px; width:125px; white-space:normal; text-decoration: underline;}
	.printTable .printLabel:active 		{text-align:right; font-weight:bold; color:#35487B; padding-right:6px; width:125px; white-space:normal; text-decoration: none;}
	.printTable .printLabel:visited 	{text-align:right; font-weight:bold; color:#35487B; padding-right:6px; width:125px; white-space:normal; text-decoration: none;}
	.printTable .printLabel:hover 		{text-align:right; font-weight:bold; color:#35487B; padding-right:6px; width:125px; white-space:normal; text-decoration: none;}
	 
	-->
    </style>

	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
   
</head>

	<body style="padding:20px;">

		<div style="text-align:left; margin-bottom:10px; float:left;">
    	   	<span style="color:#35487B; position:static; font-family:arial; font-size:14px; font-weight:bold; margin-bottom:10px;">Issue: <% =rs("Issue #") %></span>
	   	</div> 

        <!-- START Buttons -->
        <div style="text-align:right;"> 	       		
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>        
            <a class="myCMbutton" href="#" accesskey="P" onClick="this.blur(); javascript:window.print(); return false;" style="float:right;"><span class="print" style="padding-right:10px;"><u>P</u>rint</span></a>
        </div>    
        <!-- STOP Buttons -->        
        
        <!-- START Form Fields -->	                    
		<table id="table:details:general" width="100%" class="printTable" cellpadding="0" cellspacing="0">

            <tr>
                <td colspan="2" class="sectionHeader">Audit Record</td>
            </tr>                            

			<%
			count = 0
			
            'loop through all columns
            for each field in rs.fields
				if lCase(field.name) <> "history id" and lCase(field.name) <> "issue #" then
				
					'print first record...set a different style to others
					if count = 0 then
						'highlight these fields
						if lcase(field.name) = "modified by" or lcase(field.name) = "modified date" or lcase(field.name) = "modifed action" then						
							response.write("<tr>")
							response.write("	<td class=""printLabel"" style=""border-top:none; background-color:#FFFFC8;"">" & field.name & ":</td>")
							response.write("	<td align=left style=""border-top:none; background-color:#FFFFC8;"">" & rs(field.name) & "&nbsp;</td>")
							response.write("</tr>")
						else
							response.write("<tr>")
							response.write("	<td class=""printLabel"" style=""border-top:none;"">" & field.name & ":</td>")
							response.write("	<td align=left style=""border-top:none;"">" & rs(field.name) & "&nbsp;</td>")
							response.write("</tr>")						
						end if
												
					'check if field is MEMO/MAX field and truncate if necessary
					elseif field.type = adLongVarWChar then

						'HTML email...open in new window
						if inStr(rs(field.name),"DOCTYPE HTML") > 0 then
							response.write("<tr id=""memo" & count & "_Visible"">")
							response.write("	<td class=""printLabel"">" & field.name & ":</td>")
							response.write("	<td align=left><a href=""#"" onclick=""SimpleModal.open('../_dialogs/popup_hist_record_html.asp?hist=hist-" & lCase(historyTable) & "&recid=" & rs("history id") & "&field=" & field.name & "', 500, 700, 'yes'); return false;"" >HTML document</a>&nbsp;<a href=""#"" onclick=""SimpleModal.open('../_dialogs/popup_hist_record_html.asp?hist=hist-" & lCase(historyTable) & "&recid=" & rs("history id") & "&field=" & field.name & "', 500, 700, 'yes'); return false;"" ><img src=""../_images/icons/16/html_go.png"" width=""16"" height=""16"" border=""0"" align=""absmiddle"" title=""View HTML"" /></a></td>")
							response.write("</tr>")
							
						'PLAIN text...replace carriage returns with <BR>
						else
							response.write("<tr id=""memo" & count & "_Visible"">")
							response.write("	<td class=""printLabel""><a href=""#"" onclick=""changeObjectDisplay('memo" & count & "_Visible','none'); changeObjectDisplay('memo" & count & "_Hide',''); return false;"" title=""Show Details"" class=""printLabel""><img src=""../_images/icons/16/arrow2_right.png"" width=""16"" height=""16"" border=""0"" align=""absmiddle"" />" & field.name & ":</a></td>")
							response.write("	<td align=left><div>" & left(stripHTML(rs(field.name)), 100) & "&nbsp;</div></td>")
							response.write("</tr>")
							response.write("<tr id=""memo" & count & "_Hide"" style=""display:none;"">")
							response.write("	<td class=""printLabel""><a href=""#"" onclick=""changeObjectDisplay('memo" & count & "_Visible',''); changeObjectDisplay('memo" & count & "_Hide','none'); return false;"" title=""Show Details"" class=""printLabel""><img src=""../_images/icons/16/arrow2_down.png"" width=""16"" height=""16"" border=""0"" align=""absmiddle"" />" & field.name & ":</a></td>")
							response.write("	<td align=left><div style=""border:#CCC 1px solid; padding:5px; text-align:left; vertical-align:text-top;"">" & replace(rs(field.name) & "",vbCRLF,"<br>") & "&nbsp;</div></td>")
							response.write("</tr>")
							
						end if
						
					'all others...print as is					
					else					
					
						'highlight these fields
						if lcase(field.name) = "modified by" or lcase(field.name) = "modified date" or lcase(field.name) = "modifed action" then						
							response.write("<tr>")
							response.write("	<td class=""printLabel"" style=""background-color:#FFFFC8;"">" & field.name & ":</td>")
							response.write("	<td align=left style=""background-color:#FFFFC8;"">" & rs(field.name) & "&nbsp;</td>")
							response.write("</tr>")
						else
							response.write("<tr>")
							response.write("	<td class=""printLabel"">" & field.name & ":</td>")
							response.write("	<td align=left>" & rs(field.name) & "&nbsp;</td>")
							response.write("</tr>")						
						end if
						
					end if
					
					count = count + 1
					
				end if				
            next
			
			call closeRS(rs)
            %>        

		</table>
		<!-- END Form Fields -->
                       

		<!-- START END of issue notice -->  
		<table width="100%" class="printTable" style="margin-bottom:5px;" cellpadding="0" cellspacing="0">                                   
			<tr>
				<td class="sectionHeader" style="text-align:center">- End of Record -</td>
			</tr>
		</table>
		<!-- STOP END of issue notice -->                          

		<div style="text-align:left; float:left;">&nbsp;</div> 
        
        <!-- START Buttons -->
        <div style="text-align:right; padding-bottom:10px;"> 	       		
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>        
            <a class="myCMbutton" href="#" accesskey="P" onClick="this.blur(); javascript:window.print(); return false;" style="float:right;"><span class="print" style="padding-right:10px;"><u>P</u>rint</span></a>
        </div>    
        <!-- STOP Buttons -->

	</body>
</html>

<%
'close database connection
call closeDB()
%>

