<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs


'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim crs12, rfi12, cor12, mcr12
dim crsYTD, rfiYTD, corYTD, mcrYTD
dim total12, totalYTD

'get customer name
mySQL = "SELECT Customer.Name " _
	  & "	FROM Customer " _
	  & "	WHERE Customer.CustomerID='" & customerID & "' "
set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
if rs.eof then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
else
	customerName = rs("Name")
end if


dim action
action = trim(Request.QueryString("action"))
if len(action) = 0 then
	action = request.Form("action")
end if


'build statement for last 12 months
mySQL = "SELECT Name, Sum([CRS]) AS [CRS_Sum], Sum([RFI]) AS [RFI_Sum], Sum([COR]) AS [COR_Sum], Sum([MCR]) AS [MCR_Sum] " _
	  & "	FROM vwCustomerStats " _
	  & "	WHERE CustomerID='" & customerID & "' AND Date >= '" & dateadd("yyyy",-1,date()) & "' " _
	  & " 	GROUP BY Name "
set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
if rs.eof then
	crs12 = 0
	rfi12 = 0
	cor12 = 0
	mcr12 = 0
	total12 = 0
else
	crs12 = FormatNumber(rs("crs_sum"),0)
	rfi12 = FormatNumber(rs("rfi_sum"),0)
	cor12 = FormatNumber(rs("cor_sum"),0)
	mcr12 = FormatNumber(rs("mcr_sum"),0)
	total12 = rs("crs_sum") + rs("rfi_sum") + rs("cor_sum") + rs("mcr_sum")
	total12 = FormatNumber(total12,0)	
end if

'build statement for YTD
mySQL = "SELECT Name, Sum([CRS]) AS [CRS_Sum], Sum([RFI]) AS [RFI_Sum], Sum([COR]) AS [COR_Sum], Sum([MCR]) AS [MCR_Sum] " _
	  & "	FROM vwCustomerStats " _
	  & "	WHERE CustomerID='" & customerID & "' AND Year(Date) = '" & year(date()) & "' " _
	  & " 	GROUP BY Name "
set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
if rs.eof then
	crsYTD = 0
	rfiYTD = 0
	corYTD = 0
	mcrYTD = 0
	totalYTD = 0
else
	crsYTD = FormatNumber(rs("crs_sum"),0)
	rfiYTD = FormatNumber(rs("rfi_sum"),0)
	corYTD = FormatNumber(rs("cor_sum"),0)
	mcrYTD = FormatNumber(rs("mcr_sum"),0)
	totalYTD = rs("crs_sum") + rs("rfi_sum") + rs("cor_sum") + rs("mcr_sum")
	totalYTD = FormatNumber(totalYTD,0)
end if

'close recordset
call closeRS(rs)

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Customer Monitor</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />

	<style type="text/css">
	<!--
	.monitorLabel {
		text-align:right;
		font-weight:bold;
		color:#35487B;
		padding-right:6px;
		width:175px; white-space:normal;
		vertical-align:top;
		padding-top:5px;
		padding-bottom:5px;		
	}
	.monitorText {
		vertical-align:top; 
		border-top:1px dotted #cccccc; 
		padding-top:5px; 
		padding-right:50px;
	}
	-->
	</style>

	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- JQUERY Libraries -->
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

    <!-- jQuery Hightcharts JS http://http://www.highcharts.com -->
    <script type="text/javascript" src="../_jquery/chart/js/highcharts.js"></script>		

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/system_monitor.png" title="Audit Log" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle"><% =customerName %></span>
   	</div>                   

    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom:10px;">

        <tr>
            <td align=left class="monitorLabel" style="border-top:1px dotted #cccccc; padding-top:5px;">&nbsp;</td>        
            <td align=right class="monitorLabel" style="vertical-align:top; border-top:1px dotted #cccccc; padding-top:5px; padding-right:50px;">Last 12 Months</td>
            <td align=right class="monitorLabel" style="vertical-align:top; border-top:1px dotted #cccccc; padding-top:5px; padding-right:50px;">Year to Date</td>            
        </tr>

        <tr>
            <td align=left class="monitorLabel" style="border-top:1px dotted #cccccc; padding-top:5px;">Compliance Report:</td>        
            <td align=right class="monitorText"><% =crs12 %>&nbsp;(<% if crs12>"0" then response.write(FormatPercent(crs12/total12,0)) else response.write("0%") %>)</td>
            <td align=right class="monitorText"><% =crsYTD %>&nbsp;(<% if crsYTD>"0" then response.write(FormatPercent(crsYTD/totalYTD,0)) else response.write("0%") %>)</td>            
        </tr>

        <tr>
            <td align=left class="monitorLabel" style="border-top:1px dotted #cccccc; padding-top:5px;">Request for Information:</td>        
            <td align=right class="monitorText"><% =rfi12 %>&nbsp;(<% if crs12>"0" then response.write(FormatPercent(rfi12/total12,0)) else response.write("0%") %>)</td>
            <td align=right class="monitorText"><% =rfiYTD %>&nbsp;(<% if rfiYTD>"0" then response.write(FormatPercent(rfiYTD/totalYTD,0)) else response.write("0%") %>)</td>            
        </tr>

        <tr>
            <td align=left class="monitorLabel" style="border-top:1px dotted #cccccc; padding-top:5px;">Compliance Office:</td>        
            <td align=right class="monitorText"><% =cor12 %>&nbsp;(<% if crs12>"0" then response.write(FormatPercent(cor12/total12,0)) else response.write("0%") %>)</td>
            <td align=right class="monitorText"><% =corYTD %>&nbsp;(<% if corYTD>"0" then response.write(FormatPercent(corYTD/totalYTD,0)) else response.write("0%") %>)</td>            
        </tr>

        <tr>
            <td align=left class="monitorLabel" style="border-top:1px dotted #cccccc; padding-top:5px;">MyComplianceReport:</td>        
            <td align=right class="monitorText"><% =mcr12 %>&nbsp;(<% if crs12>"0" then response.write(FormatPercent(mcr12/total12,0)) else response.write("0%") %>)</td>
            <td align=right class="monitorText"><% =mcrYTD %>&nbsp;(<% if mcrYTD>"0" then response.write(FormatPercent(mcrYTD/totalYTD,0)) else response.write("0%") %>)</td>            
        </tr>

        <tr>
            <td align=left class="monitorLabel" style="border-top:1px dotted #cccccc; padding-top:5px;">Total Issues:</td>        
            <td align=right class="monitorText"><strong><% =total12 %></strong></td>
            <td align=right class="monitorText"><strong><% =totalYTD %></strong></td>            
        </tr>

	</table>    

	<%
	'----------------------------------------------	
	'-- START Chart activity --
	'----------------------------------------------
	dim field, colCount, xCol, xRow, tmpValue	
	dim labelTop15
	dim seriesAdjust : seriesAdjust = 2
					
	'stored procedure
	set rs = openRSexecuteDialog(" EXEC sp_Crosstab 'SELECT CRSID, Issue_Type, Month FROM vwIssuesSum_CallCenter where [Year]=" & year(date()) & " and CustomerID=^" & CustomerID & "^', Null, Null, 'Issue_Type', 'CRSID', 'COUNT', Null, 1 ")

	'sql executed and closed
	if rs.State = adStateClosed then
		'do nothing...
			
	'recordset open...keep going
	else

		dim strChartCategories, strChartSeries, strChartSeriesValues, theURL, list_field, list_field_type
		dim strChartCaptions, bolGetCaptions, arrSeriesCount, seriesCount, tempSeries, tempValue, strValue, tempLabel
		dim seriesX, arrSeriesName, arrSeriesValue
		
		dim arrChartSeriesName, arrChartSericesValue
		
		'get number of columns returned from stored procudure
		'PUT -2 IF USING TOTAL COLUMN???????
		for each field in rs.fields
			'colCount = colCount + 1
			seriesCount = seriesCount + 1
		next						
			
		redim arrSeriesName(seriesCount)
		redim arrSeriesValue(seriesCount)

		'cycle through all records to build series and values
		'only chart the TOP 15!!!!
		do while not rs.eof and xRow < 15
			
			tempSeries = ""
			tempValue = ""
			tempLabel = ""
			xCol = 0		'keep up with column count
			xRow = xRow + 1 'keep up with row count
				
			for each field in rs.fields		
				
				'first column/label
				if xCol = 0 then					
					strChartCategories = strChartCategories & rs(field.name) & "','"						
				else
					'pull from 1st row only
					if xRow = 1 then
						arrSeriesName(xCol) = field.name
					end if
					arrSeriesValue(xCol) = arrSeriesValue(xCol) & rs(field.name) & ","						
				end if
			
				xCol = xCol + 1
					
			next
			
			rs.Movenext
				
		loop
		
		'only continue if records found/returned
		if xRow > 0 then
		
			'show sub-title "Top 15 Charted"
			if xRow >= 14 then
				labelTop15 = "Top 15 Charted"
			end if
	
	
			'clean up and remove extra "," and "*|*"			
			strChartCategories = "'" & left(strChartCategories,len(strChartCategories)-3) & "'"
	
			'the "-2" is because "ALL_COUNT" total column is turned on in sp_Crosstab call
			'change back to "-1" if total column is desired		
			for seriesX = 1 to (seriesCount - seriesAdjust)
				tempSeries = tempSeries & "{name: '" & arrSeriesName(seriesX) & "', data: [" & left(arrSeriesValue(seriesX),len(arrSeriesValue(seriesX))-1) & "]}"
				if seriesX + 1 < (seriesCount) then
					tempSeries = tempSeries & ", "
				end if
			next		
			strChartSeries = tempSeries			
	
			'close recordset, no longer needed
			call closeRS(rs)
	
		end if
	
	end if


    if xRow > 0 then
    	%>
        
        <!-- START CHART table -->
        <table id="chartTable" width="100%" cellpadding="0" cellspacing="0" style="border:1px solid #B3B3B3; margin-bottom:15px;">
          <tr>
            <td>
    
            <!-- jQuery Hightcharts JS http://http://www.highcharts.com -->
            <script type="text/javascript" src="../_jquery/chart/js/highcharts.js"></script>
            
			<script type="text/javascript"> 
                Highcharts.theme = { colors: ['#4572A7'] };// prevent errors in default theme
                var highchartsOptions = Highcharts.getOptions(); 
            </script>
        
            <!-- initialize the chart on document ready -->
            <script type="text/javascript">			
        
                var chart;
                $(document).ready(function() {
                   chart = new Highcharts.Chart({
                      chart: {
                         renderTo: 'chartDiv',
                         defaultSeriesType: 'line',
						 marginTop: 25,
                         marginRight: 25,
                         marginBottom: 75
                      },			  
					  title: {
						 text: '',
						 x: -20 //center
					  },
					  subtitle: {
						 text: '',
						 x: -20
					  },					  
                      xAxis: {
                         categories: [<% =strChartCategories %>]
                      },
                      yAxis: {
  						 min: 0,						  
                         title: {
                            text: 'Issues'
                         },
                         plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                         }]
                      },
                      tooltip: {
                         formatter: function() {
                                   return this.x + ': ' + Highcharts.numberFormat(this.y, 0);
                         }
                      },
                      credits: {
                         enabled: false
                      },			  
                      series: [<% =strChartSeries %>]
                   });		   
                   
                });
            </script>
        
            
            <!-- DIV to hold new chart -->
            <div id="chartDiv" style="width: 100%; height: 240px; margin: 0 auto"></div>        
                
            </td>
          </tr>
        </table>    
        <!-- END CHART table -->
        
		<%
	else
		%>	
        <div style="border:1px solid #B3B3B3; margin-bottom:15px; text-align:center; padding:50px;"><em>No data available</em></div>
		<%
	end if	
	'----------------------------------------------	
	'-- STOP Chart activity --
	'----------------------------------------------
	%>
	
	<!-- START Buttons -->
	<div style="float:right;">
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Buttons -->    
     
</body>
</html>

<%
'close database connection
call closeDB()
%>


