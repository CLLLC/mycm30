<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement, Pro, Lite, Metrics
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'Work Fields
dim I
dim item
dim count
dim pageSize
dim emailAs
dim emailTo
dim listUser
dim emailSubject
dim emailMessage
dim emailAttachment
dim issueDetails
dim redactedDetails

'Issue Fields on DETAILS tab
dim CRSID
dim rptDate
dim rptTime
dim anonCaller
dim callBack
dim interpreterUsed
dim language
dim category
dim callerAddr
dim callerBestTime
dim callerCell
dim callerCity
dim callerEmail
dim callerFax
dim callerFirst
dim callerHome
dim callerLast
dim callerOther
dim callerPager
dim callerState
dim callerTitle
dim callerType
dim callerWork
dim callerZip
dim commTool
dim firstTime
dim locAddr
dim locCity
dim locCountry
dim locName
dim locState
dim locZip
dim locLevel_1
dim locLevel_2
dim locLevel_3
dim locLevel_4
dim locLevel_5
dim locLevel_6
dim locLevel_7
dim locLevel_8
dim locHierarchy
dim severityLevel
dim rptSummary
dim rptDetails
dim rptAddendum
dim caller_confidential
dim details_redacted


dim idIssue
idIssue = trim(Request.QueryString("recId"))
if len(idIssue) = 0 then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")	
end if

dim action
action = request.Form("action")

'send e-mail
if action = "sendemail" then

	listUser = Request.Form("listUser")
	emailTo = Request.Form("emailTo")
	if len(emailTo & listUser) <= 0 then
		response.write("<div style=""color: #333333; font-family: Verdana, Arial, helvetica; font-size: 8pt; font-weight:bold; padding:15px;"">ERROR: At least one recipient must be provided.</div>")
		response.write("<div style=""width:75%; text-align:left; padding:15px; color: #333333; font-family: Verdana, Arial, helvetica; font-size: 8pt;"">")
	    response.write("<a href=""javascript:history.back()"">&laquo;&laquo; Go Back</a>")
        response.write("</div>")
		response.end()
	end if
	
	emailTo = trim(emailTo & ";" & listUser)
	emailTo = replace(emailTo,",",";")
	if left(emailTo,1) = ";" then emailTo = right(emailTo,len(emailTo)-1)
	
	emailSubject = Request.Form("emailSubject")
	emailMessage = Request.Form("emailMessage")

	''' TURNED OFF FOR NOW... emailAttachment = "'SELECT CRS.CRSID FROM myCM.dbo.CRS WHERE CRS.CRSID='" & idIssue & "'"
	emailAttachment = ""

	''' TURNED OFF... with GMAIL we could no longer send as someone else.
	'different FROM address than user logged in
	'if lCase(email) <> lCase(emailFrom) then 
	'	emailMessage = "** Please DO NOT reply to this email. Send all correspondence to original sender: " & session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname") & " [" & email & "]" & vbCrLf & vbCrLf & emailMessage
	'end if
	
	emailAs = session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname") & "<" & emailFrom & ">"
	
	emailMessage = "** Please DO NOT reply to this email. Send all correspondence to original sender: " & session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname") & " [" & email & "]" & vbCrLf & vbCrLf & emailMessage
	
	mySQL = "INSERT INTO MailLog (" _
		  & "CustomerID, CRSID, mailDate, fromEmail, toEmail, subject, body, attachmentsql, conttype" _
		  & ") VALUES (" _
		  & "'" & uCase(customerID) & "'," _
		  & "'" & idIssue & "'," _
		  & "'" & Now() & "'," _
		  & "'" & emailAs & "'," _
		  & "'" & emailTo & "'," _
		  & "'" & replace(emailSubject,"'","''") & "'," _
		  & "'" & replace(emailMessage,"'","''") & "'," _
		  & "'" & replace(emailAttachment,"'","''") & "'," _
		  & "'" & "TEXT" & "'" _
		  & ")"
	set rs = openRSexecuteDialog(mySQL)

	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
	<%
	response.write("<div style=""color: #333333; font-family: Verdana, Arial, helvetica; font-size: 8pt; font-weight:bold; padding:15px"">Message sent at " & Now() & ".</div>")
	response.write("<div style=""width:75%; text-align:left; padding:15px; color: #333333; font-family: Verdana, Arial, helvetica; font-size: 8pt;"">")
    response.write("<a href=""#"" onclick=""SimpleModal.close(); return false;"">Close this window</a>")
    response.write("</div>")
	response.end()
	
else
	
	'Get Issue Record
	if len(idIssue) > 0 then
		mySQL="SELECT CRS.* " _
			& "FROM   CRS " _
			& "WHERE  CRSID='" & idIssue & "' AND CustomerID='" & uCase(customerID) & "' "				
		set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		if rs.eof then
			response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Issue ID.")
			call closeRS(rs)
		else
			CRSID 					= rs("crsid")
			rptDate    				= rs("date")
			rptTime    				= rs("time")
			severityLevel   		= rs("severity")
			firstTime	  			= rs("firsttimeuser")
			commTool	  			= rs("communicationtool")
			callBack	  			= rs("callback")
			anonCaller	  			= rs("anonymous")
			interpreterUsed			= rs("interpreterused")
			language				= rs("language")				
			locName	  				= rs("location_name")
			locAddr	  				= rs("location_address")
			locCity	  				= rs("location_city")
			locState	  			= rs("location_state")
			locZip	  				= rs("location_postalcode")
			locCountry 				= rs("location_country")		
			locLevel_1				= rs("location_level_1")
			locLevel_2				= rs("location_level_2")		
			locLevel_3				= rs("location_level_3")
			locLevel_4				= rs("location_level_4")		
			locLevel_5				= rs("location_level_5")
			locLevel_6				= rs("location_level_6")		
			locLevel_7				= rs("location_level_7")
			locLevel_8				= rs("location_level_8")					
			callerType				= rs("caller_type")
			callerFirst				= rs("caller_firstname")
			callerLast				= rs("caller_lastname")
			callerAddr				= rs("caller_address")
			callerTitle				= rs("caller_title")
			callerCity				= rs("caller_city")
			callerState				= rs("caller_state")
			callerZip				= rs("caller_postalcode")
			callerHome				= rs("caller_home")
			callerWork				= rs("caller_work")
			callerCell				= rs("caller_cell")
			callerPager				= rs("caller_pager")
			callerFax				= rs("caller_fax")
			callerOther				= rs("caller_other")
			callerEmail				= rs("caller_email")		
			callerBestTime			= rs("caller_besttime")					
			rptSummary				= rs("summary")		
			rptDetails				= rs("details")	
			rptAddendum				= rs("addendum")
			caller_confidential		= rs("caller_confidential")
			details_redacted		= rs("details_redacted")
	
			'build loccation hierarchy for display
			locHierarchy = locLevel_1 & "*|*" & locLevel_2 & "*|*" & locLevel_3 & "*|*" & locLevel_4 _
									  & "*|*" & locLevel_5 & "*|*" & locLevel_6 & "*|*" & locLevel_7 & "*|*" & locLevel_8
									  
			'change to proper value to be stored in database
			locHierarchy = reformatHierarchy(locHierarchy,"string")
		
			call closeRS(rs)
	
			'****************************************
			'Build Issue Body for email attachement
			'****************************************						
			issueDetails = ""
			issueDetails = issueDetails & "------------------------------------------------------------" & vbCrLf
			issueDetails = issueDetails & "Issue: " & idIssue & vbCrLf
			issueDetails = issueDetails & "------------------------------------------------------------" & vbCrLf
			issueDetails = issueDetails & "Date/Time: " & FormatDateTime(rptDate,vbShortDate) & " " & FormatDateTime(rptTime,3) & vbCrLf
			issueDetails = issueDetails & "Severity: " & reformatSeverity(severityLevel) & vbCrLf
			issueDetails = issueDetails & "Call Back: " & callBack & vbCrLf
			issueDetails = issueDetails & "Awareness: " & commTool & vbCrLf & vbCrLf
			
			issueDetails = issueDetails & "Issue Location: " & locName & vbCrLf
			issueDetails = issueDetails & "Street Address: " & locAddr & vbCrLf
			issueDetails = issueDetails & "City: " & locCity & vbCrLf
			issueDetails = issueDetails & "State/Province: " & locState & vbCrLf
			issueDetails = issueDetails & "Country: " & locCountry & vbCrLf			
			issueDetails = issueDetails & "Hierarchy: " & locHierarchy & vbCrLf & vbCrLf
			
			mySQL = "SELECT a.CRSID, a.[Primary], Category.CategoryID, Category.Name, Category.Sub, Investigation.Status " _
				  & "FROM (CRS_Category AS a INNER JOIN Category ON a.CategoryID = Category.CategoryID) " _
				  & "LEFT JOIN Investigation ON (a.CategoryID = Investigation.CategoryID) AND (a.CRSID = Investigation.CRSID) " _
				  & "WHERE a.CRSID = '" & idIssue & "' " _
				  & "ORDER BY Category.Name, Category.Sub "
			set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
			dim tempCat, priCategory
			do while not rs.eof
				if len(rs("Sub")) > 0 then
					tempCat = rs("Name") & " [" & rs("Sub") & "]"
				else
					tempCat = rs("Name")
				end if
				category = category & tempCat & ", "
				rs.movenext
			loop
			call closeRS(rs)
			issueDetails = issueDetails & "Categories: " & category & vbCrLf & vbCrLf
			
			issueDetails = issueDetails & "- Reporter Information -------------------------------------" & vbCrLf

			issueDetails = issueDetails & "Identity: "
			if anonCaller = "True" then 
				issueDetails = issueDetails & "Anonymous"
			else 
				issueDetails = issueDetails & "Identified"
			end if 
			issueDetails = issueDetails & vbCrLf
			
			issueDetails = issueDetails & "Interpreter Used: "
			if interpreterused = "True" then 
				issueDetails = issueDetails & "Yes"
			else 
				issueDetails = issueDetails & "No"
			end if 
			issueDetails = issueDetails & vbCrLf
			
			issueDetails = issueDetails & "Language: " & language & vbCrLf & vbCrLf


			'prep redactedDetails before adding caller info
			redactedDetails = redactedDetails & issueDetails
			redactedDetails = redactedDetails & "Reporter information only visible to Administrators." & vbCrLf & vbCrLf
			
			
			if anonCaller = "False" then 
				issueDetails = issueDetails & "Reporter Name: " & callerFirst & " " & callerLast & vbCrLf
				issueDetails = issueDetails & "Relationship: " & callerType & vbCrLf
				issueDetails = issueDetails & "Job Title: " & callerTitle & vbCrLf
				issueDetails = issueDetails & "Address: " & callerAddr & vbCrLf
				issueDetails = issueDetails & "City: " & callerCity & vbCrLf
				issueDetails = issueDetails & "State/Province: " & callerState & vbCrLf
				issueDetails = issueDetails & "Postal Code: " & callerZip & vbCrLf & vbCrLf		
				issueDetails = issueDetails & "Home Phone: " & callerHome & vbCrLf
				issueDetails = issueDetails & "Work Phone: " & callerWork & vbCrLf
				issueDetails = issueDetails & "Mobil Phone: " & callerCell & vbCrLf
				issueDetails = issueDetails & "Fax: " & callerFax & vbCrLf
				issueDetails = issueDetails & "Pager: " & callerPager & vbCrLf
				issueDetails = issueDetails & "Other Phone: " & callerOther & vbCrLf & vbCrLf
				issueDetails = issueDetails & "E-mail: " & callerEmail & vbCrLf 
			end if
			
			issueDetails = issueDetails & "First Time Caller: "

			if firstTime = "True" then 
				issueDetails = issueDetails & "Yes"
			else 
				issueDetails = issueDetails & "No"
			end if 
			issueDetails = issueDetails & vbCrLf & vbCrLf
			
			issueDetails = issueDetails & "- Summary ------------------------------------------------" & vbCrLf
			issueDetails = issueDetails & stripHTML(replace(rptSummary & "", chr(34), "'")) & vbCrLf & vbCrLf
			
			issueDetails = issueDetails & "- Details ------------------------------------------------" & vbCrLf
			issueDetails = issueDetails & stripHTML(replace(rptDetails & "", chr(34), "'")) & vbCrLf & vbCrLf			 
			
			issueDetails = issueDetails & "- Addendum -----------------------------------------------" & vbCrLf
			issueDetails = issueDetails & stripHTML(replace(rptAddendum & "", chr(34), "'")) & vbCrLf & vbCrLf
						
			issueDetails = issueDetails & "- end of report ------------------------------------------" & vbCrLf

			'redacted verion
			redactedDetails = redactedDetails & "- Details ------------------------------------------------" & vbCrLf
			redactedDetails = redactedDetails & stripHTML(replace(Details_Redacted & "", chr(34), "'")) & vbCrLf & vbCrLf			 

			redactedDetails = redactedDetails & "- end of report ------------------------------------------" & vbCrLf
			
		end if
		
	end if

end if


%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>myCM E-mail</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />

	<style type="text/css">
	<!--
	.emailLabel {
		text-align:right;
		font-weight:bold;
		color:#35487B;
		padding-right:6px;
		width:60px; white-space:normal;
		vertical-align:top;
		padding-top:5px;
		padding-bottom:5px;		
	}
	-->
	</style>

	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!--JQuery required components -->
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />

    <!-- ONLY FOR suggest boxes and JSON data pulls, includes full JQuery library -->
    <style>
        .ui-autocomplete-loading { background: white url('../_jquery/suggestbox/ui-anim_basic_16x16.gif') right center no-repeat; }
    </style>
    <style>
        .ui-autocomplete {
            max-height: 100px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
            /* add padding to account for vertical scrollbar */
            padding-right: 20px;
        }
        /* IE 6 doesn't support max-height
         * we use height instead, but this forces the menu to always be this tall
         */
        * html .ui-autocomplete {height: 100px;}
    </style>
    <!-- ----------------------------------------- -->

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

<form method="post" action="popup_email.asp?recid=<% =idIssue %>&cid=<% =customerID %>" name="frm" style="padding:0px; margin:0px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/32/email_to_friend.png" title="Issues" width="32" height="32" align="absmiddle"> 
       	<span class="popupTitle">Send E-mail</span></div>                   
    
    
	<input name="issueDetails" id="issueDetails" type="hidden" value="<% =issueDetails %>">
    <input name="redactedDetails" id="redactedDetails" type="hidden" value="<% =redactedDetails %>">
	<input name="issueNotes" id="issueNotes" type="hidden" value="<% ="here are the notes..." %>">
	<input name="action" type="hidden" value="sendemail">

	<!-- START User/Contact table -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0">

        <tr>
            <td align=left class="emailLabel" style="border-top:1px dotted #cccccc; padding-top:5px;">From:</td>        
            <td align=left style="vertical-align:top; border-top:1px dotted #cccccc; padding-top:5px;"><% if len(appSendFromEmail) > 0 then response.write("<del>" & email & "</del>&nbsp;" & appSendFromEmail & "") else response.write(email) %></td>
        </tr>

        <tr>
            <td align=left class="emailLabel" style="border-top:1px dotted #cccccc; padding-top:5px;">To:</td>        
            <td align=left style="border-top:1px dotted #cccccc; padding-top:5px; padding-right:10px; margin-right:10px;">
            	<textarea name="emailTo" id="emailTo" onKeyDown="checkEmailLen();" style="overflow-y:hidden; height:13px; width:100%;"></textarea>                
            </td>
        </tr>

    
        <%
		'Find all assigned users
		mySQL = "SELECT Logins.LOGID, Logins.FirstName, Logins.LastName, Logins.Email " _
			  & "FROM CRS_Logins INNER JOIN Logins ON CRS_Logins.LOGID = Logins.LOGID " _
			  & "WHERE CRS_Logins.CRSID = '" & idIssue & "' " _
			  & "ORDER BY Logins.LastName, Logins.FirstName "

        'Open recordset
        set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
        if rs.eof then
        %>
            <tr>
            	<td align=left valign=middle>&nbsp;</td>                    
                <td align=center valign=middle style="padding-bottom:5px; padding-right:5px; margin-right:5px;">
                <select name="listUser" id="listUser" style="width:100%; height:55px; margin-top:5px; margin-bottom:5px;" disabled multiple="multiple" >
                	<option value="">No Users Found.</option>
                </select>
                </td>
            </tr>
        <%
        else
        %>	        
            <tr>
            	<td align=left valign=middle style="padding-bottom:5px;">&nbsp;</td>                                
                <td style="padding-bottom:5px; padding-right:5px; margin-right:5px;">
                    <!-- START list of available users -->
                    <select name="listUser" id="listUser" style="width:100%; height:55px; margin-top:5px; margin-bottom:5px;" multiple="multiple" >
                    <%
					dim tempUser, tempGroup
                    do while not rs.eof					
						if len(rs("FirstName")) > 0 then
							tempUser = rs("LastName") & ", " & rs("FirstName") 
						else
							tempUser = rs("LastName") 
						end if
                        response.Write("<option value=""" & rs("email") & """>" & tempUser & "</option>")            
                        rs.movenext
                    loop
                    %>
                    </select>
                    <!-- STOP list of available users -->
                    
                    <div style="float:left;">
                        Select: <a href="javascript:selectAllOptions('listUser',true);">All</a> | <a href="javascript:selectAllOptions('listUser',false);">None</a>
                    </div>
                                        
                </td>
            </tr>        
        <%
        end if
        %>

        <tr>
            <td align=left class="emailLabel" style="border-top:1px dotted #cccccc; padding-top:5px; padding-bottom:5px;">Subject:</td>        
            <td align=left style="border-top:1px dotted #cccccc; padding-top:5px; padding-bottom:5px; padding-right:10px; margin-right:10px;">
                <input name="emailSubject" id="emailSubject" type=text style="width:100%;" value="<% response.write("Issue: " & idIssue)%>">
            </td>
        </tr>
       
    </table>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
    	<tr>
        	<td align=left style="border-top:1px dotted #cccccc; padding-top:5px; padding-bottom:5px; padding-right:10px; margin-right:10px;">
            	<div style="float:left; font-weight:bold; color:#35487B; padding-right:5px;">Message</div>
                <div>Insert: <a href="#" onClick="insertText('issueDetails'); return false;">Issue Details</a>
					<%
                    if Caller_Confidential ="Y" then
                        response.Write("<a href=""#"" onClick=""insertText('redactedDetails'); return false;"">[Redacted]</a>")
                    end if				
                    %>
                </div>
                
                
           		<textarea name="emailMessage" id="emailMessage" style="width:100%; height:175px; margin-top:5px; margin-bottom:5px;"></textarea>                                
            </td>
	</table>
	<!-- STOP User/Contact table -->
	
	<!-- START Buttons -->
	<div style="float:right;">
        <a class="myCMbutton" href="#" accesskey="A" onClick="this.blur(); document.frm.submit(); return false;"><span class="emailsend" style="padding-right:7px;"><u>S</u>end</span></a>
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Buttons -->    
 
<%
call closeRS(rs)
%>

</form>
    
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script language="javascript">	
	function insertText(obj) {
	  //add selected options to parent listUser
	  var msg = document.getElementById('emailMessage');
	  var el = document.getElementById(obj);		
	  //append selected text to message box
	  msg.value = msg.value + el.value + "\n";	  
}		
</script>


		<script>
			//JSON QUERY BUILDER used to search database
			buildJQuery('logins','email','emailTo','<% =customerID %>');

			function buildJQuery(table,field,form_field,cid) {
																
				$(function() {
					function split( val ) {
						return val.split( /,\s*/ );
					}
					function extractLast( term ) {
						return split( term ).pop();
					}                                                            
					$("#"+form_field)
						// don't navigate away from the field on tab when selecting an item
						.bind( "keydown", function( event ) {
                      		if ( event.keyCode === $.ui.keyCode.TAB &&
                            	$( this ).data( "autocomplete" ).menu.active ) {
                               		event.preventDefault();
                             }
                        })
                     	.autocomplete({
                      		source: function( request, response ) {
                          		$.getJSON( "../_jquery/suggestBox/json-data.asp?view="+table+"&field="+field+"&cid="+cid, {
                             		term: extractLast( request.term )
                           		}, response );
                          	},
                       		search: function() {
                        		// custom minLength
                             	var term = extractLast( this.value );
                           		if ( term.length < 1 ) {
                            		return false;
                         		}
                      		},
                      		focus: function() {
                    			// prevent value inserted on focus
                      			return false;
                         	},
							select: function( event, ui ) {
								//sets up for multiple responses...
								var terms = split( this.value );
								// remove the current input
								terms.pop();
								// add the selected item
								terms.push( ui.item.value );
								// add placeholder to get the comma-and-space at the end
								terms.push( "" );
								this.value = terms.join( ", " );
								return false;
							}
                  		});
				});																																
			}
			
		  	// used to set height and scroll bars
		  	// if user selected IN or NOT IN as the opearator 
		  	function checkEmailLen() {
				var theElement = document.getElementById("emailTo");			
				
				//alert($("#emailTo").val().length);
				
				if($("#emailTo").val().length > 50) {
					//alert("1");
					theElement.style.overflowY = "auto";
					theElement.style.height = "42px";
				} else {
					//alert("2");
					theElement.style.overflowY = "hidden";
					theElement.style.height = "13px";
				}
		  	}

	</script>                                                            														


