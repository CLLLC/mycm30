<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, pageSize

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim countString
dim arrIssueList

'set sort order on save
if request.form("action") = "save" then
	
	dim updateI, sortNumber
	
	'get set order by user
	arrIssueList = Request.Form("idIssueList")
	arrIssueList = split(arrIssueList,",")

	'Loop through list of records and delete one by one
	for updateI = 0 to UBound(arrIssueList)
		
		sortNumber = sortNumber + 1
		
		'Make sure they have the right to delete
		mySQL = "Update Reports Set SortOrder=" & sortNumber & " " _
			  & "WHERE ReportID = " &  trim(arrIssueList(updateI))
		set rs = openRSexecuteDialog(mySQL)
				
	next
	%>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
	
	<script type='text/javascript'>
       SimpleModal.close();
    </script>	
	
<%
else
	'nothing...	
end if
%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Issue List Sort Order</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- HTML List/Table resorting functions (right now own) -->
	<!-- Example usage:
    		var table = document.getElementById('table-1');
			var tableDnD = new TableDnD();
			tableDnD.init(table);  -->
	<script type="text/javascript" src="../scripts/javascript/orderList.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

	<form name="frm" id="frm" method="post" action="popup_issuelist_sort.asp" style="padding:0px; margin:0px;">
    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/32/sort_ascending.png" title="Shortcuts" align="absmiddle"> 
       	<span class="popupTitle">Sort Order</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Drag the items below to set the sort order.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Categories table -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    
        <%
		'Find Categories available TO BE assigned
		mySQL="SELECT Reports.ReportID, Reports.Name " _
			& "FROM   Reports " _
			& "WHERE  (IssueList='Y') AND (LOGID=" & sLogid & ") AND (Reports.CustomerID = '" & sCustomerID & "') " _
			& "ORDER BY Reports.SortOrder "
                                        
        'Open recordset
        set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
        if rs.eof then
        %>
            <tr>
                <td align=center valign=middle>


                    <div id="field_list_container">
                        <div id="field_list" style="width:97%; border: 1px solid #ccc; padding: 4px; height:200px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">
                
                            <table name="tableSort" id="tableSort" width="100%" border="0" cellspacing="0" cellpadding="0">                    
                            	<tr>
                                	<td>No Issue Lists Found.
                                    </td>
                                </tr>
                            </table>

                       </div>
                   </div>

                </td>
            </tr>
        <%
        else
        %>	
        
            <tr>
                <td>

                    <div id="field_list_container">
                        <div id="field_list" style="width:97%; border: 1px solid #ccc; padding: 4px; height:200px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">
                
                            <table name="tableSort" id="tableSort" width="100%" border="0" cellspacing="0" cellpadding="0">                    
                                <%
                                do while not rs.eof
                                    count = count + 1
                                    'reformat count display
                                    if count < 10 then countString = "0" & count else countString = count 						
                                    response.write("<tr><td>")
                                    response.write("	<div id=""field_list_item_Name"" style=""padding:3px;"">")	
                                    response.write("       <input type=hidden name=""idIssueList"" id=""idIssueList"" value=""" & rs("reportid") & """>" & countString & ": " & rs("name"))
                                    response.write("	</div>")    
                                    response.write("</td></tr>")
                                    rs.movenext
                                loop
                                %>                    
                            </table>
        
                            <!-- Initiate listOrder.js for moving fields in order -->
                            <!-- THIS WORKS....BUT THE MOVED VALUE IS LOST IN FIREFOX!!! -->
                            <script type="text/javascript">
                                var table = document.getElementById('tableSort');
                                var tableDnD = new TableDnD();
                                tableDnD.init(table);
                            </script>

                       </div>
                   </div>
                
                </td>
            </tr>
        
        <%
        end if
        %>
        
    </table>
	<!-- STOP Categories table -->

    <input name="action" type="hidden" value="save">
    
	<!-- START Buttons -->
	<div style="float:right; margin-top:10px;"> 	       		
        <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); document.forms['frm'].submit();"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Buttons -->

    </form>
        
</body>
</html>

<%
'close database connection
call closeDB()
%>

