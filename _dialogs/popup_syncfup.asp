<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, rsFUP

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim recID

dim rowColor, col1, col2
col1 = "#F0F0F0"
col2 = "#FFFFFF"

dim idIssue : idIssue = trim(Request.QueryString("recId"))

dim customerID
dim masterStatus
dim allIssuesID
dim errorMsg, infoMsg

dim userFullName
dim Deadline, DateClosed, CaseNotes

if len(idIssue) > 0 then

	'make sure pulling original issue only
	idIssue = left(idIssue,len(idIssue)-2) & "01"	

	'set for pulling ALL issues in LIKE statement, trim off last 2 digits
    allIssuesID = left(idIssue,len(idIssue)-3)	

	'find original issue status
	mySQL = "SELECT CustomerID, Status " _
		  & "FROM   CRS " _
		  & "WHERE  CRSID = '" & idIssue & "' "
	'set rs = openRSexecuteDialog(mySQL)
	set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	customerID = rs("CustomerID")
	masterStatus = rs("Status")
	call closeRS(rs)

else
	errorMsg = "Invalid Issue ID was provided."
end if

dim action
action = trim(Request.Form("action"))
if action <> "syncALL" then
	infoMsg = "Complete form then click 'Update' to syncronize all issues."
elseif action = "syncALL" then
	masterStatus = trim(Request.Form("rptStatus"))
	Deadline = trim(Request.Form("Deadline"))
	DateClosed = trim(Request.Form("DateClosed"))
	CaseNotes = trim(Request.Form("CaseNotes"))	
	userFullName = session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")
end if
	
'Determin DHTML pop-up calendars to Initialize...will append Investigation TextAreas also
dim activeCalendars
activeCalendars = "'Deadline','DateClosed'"

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Syncronize Status</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    

	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    

	<!-- DHTML Calendar http://www.dhtmlx.com -->
	<link rel="stylesheet" type="text/css" href="../_calendar/dhtmlxCalendar/codebase/dhtmlxcalendar.css">
    <link rel="stylesheet" type="text/css" href="../_calendar/dhtmlxCalendar/codebase/skins/dhtmlxcalendar_simplegrey.css">
	<script type="text/javascript" src="../_calendar/dhtmlxCalendar/codebase/dhtmlxcommon.js"></script>
	<script type="text/javascript" src="../_calendar/dhtmlxCalendar/codebase/dhtmlxcalendar.js"></script>

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/update.png" title="Issues" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Syncronize: <% =allIssuesID %></span>
   	</div>                   

	<!-- START Status box -->
	<table class="formTable" style="width:100%;" border="0" cellspacing="0" cellpadding="0">
    	<tr>
        	<td>
            	Use the form below to synchronize all related issues.
            </td>
        </tr>
    
		<tr>
          <form method="post" action="popup_syncfup.asp?recid=<% =idIssue %>" name="frm">
			<td>
                <fieldset style="border:1px solid #CCCCCC; background-color:#FFFFFF; padding:0px;">
                    	<table>
                        	<tr>
                            	<td class="formLabel" style="border-top:none; padding-bottom:0px; width: 150px;">Status:</td>
                                <td style="border-top:none; padding-bottom:0px;">
	                                <div style="float:left; margin-right:5px;">
										<%							
                                        dim optArr
                                        'Build array for dropdown selections
                                        optArr = getDropDownOpt("pCaseStatus",customerID)
                                        call buildSelect(optArr,masterStatus,"rptStatus","inputMediumLong","Y") 
                                        %>
                                    </div>
                                    
                                    <% 
									'take this code out once sync is live
									if lcase(sCustomerID)<>"demo" and lcase(sCustomerID)<>"cha" and lcase(sCustomerID)<>"bsw" and lcase(sCustomerID)<>"bswh" then %>
                                        <div style="float:left; margin-bottom: 5px;">
    	                                    <input type=hidden name="action" id="action" value="syncALL">                                        
        	                                <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); document.forms['frm'].submit();"><span class="sync"><u>S</u>yncronize</span></a>
                                    	</div>                                                                        
                                    <% 
									'take out...
									end if %>
                                    
                                </td>
							</tr>
                            
                            <% if cLng(session(session("siteID") & "adminLoggedOn")) <= 30 and (lcase(sCustomerID)="demo" or lcase(sCustomerID)="cha" or lcase(sCustomerID)="bsw" or lcase(sCustomerID)="bswh") then %>
                                <tr>
                                    <td class="formLabel" style="border-top:none; padding-bottom:0px;  width: 150px;">Issue Deadline:</td>
                                    <td style="border-top:none; padding-bottom:0px;">
                                        <input name="Deadline" id="Deadline" class="inputShort" value="<% =Deadline %>" maxlength="20" />
                                        <img style="cursor:pointer;" onClick="document.getElementById('Deadline').onclick();" src="../_images/icons/16/calendar.png" align="absmiddle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="formLabel" style="border-top:none; padding-bottom:0px;  width: 150px;">Date Closed:</td>
                                    <td style="border-top:none; padding-bottom:0px;">
                                        <input name="DateClosed" id="DateClosed" class="inputShort" value="<% =DateClosed %>" maxlength="20" />
                                        <img style="cursor:pointer;" onClick="document.getElementById('DateClosed').onclick();" src="../_images/icons/16/calendar.png" align="absmiddle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="formLabel" style="border-top:none; padding-bottom:10px;  width: 150px;">Append Owner Note:</td>
                                    <td style="border-top:none; padding-bottom:10px;">
                                    	<div style="float:left; margin-right:5px;">
		                                    <input name="CaseNotes" id="CaseNotes" class="inputMediumLong" value="<% =CaseNotes %>" maxlength="255" />
    									</div>
                                        <div style="float:left;">
    	                                    <input type=hidden name="action" id="action" value="syncALL">                                        
        	                                <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); document.forms['frm'].submit();"><span class="sync"><u>S</u>yncronize</span></a>
                                    	</div>
                                    </td>
                                </tr>
                            <% end if %>
                            
                        </table>
                </fieldset>
			</td>
          </form>
        </tr>
    </table>	
	<!-- STOP Status box -->

	<%
	if len(trim(errorMsg)) > 0 then
		response.write("<table class=""formTable"" width=""100%"" cellpadding=""0"" cellspacing=""0"">")
		response.write(" <tr><td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
        call systemMessageBox("syncMessage","statusMessageERROR",errorMsg)
		response.write("  </td></tr>")
		response.write("</table>")				
	elseif len(trim(infoMsg)) > 0 then
		response.write("<table class=""formTable"" width=""100%"" cellpadding=""0"" cellspacing=""0"">")
		response.write(" <tr><td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
        call systemMessageBox("syncMessage","statusMessageINFO",infoMsg)
		response.write("  </td></tr>")
		response.write("</table>")				
	else	

		response.write("<table class=""formTable"" width=""100%"" cellpadding=""0"" cellspacing=""0"">")
		response.write(" <tr><td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
        call systemMessageBox("syncMessage","statusMessageOK","All issues have been syncronized.")
		response.write("  </td></tr>")
		response.write("</table>")				

	%>
		<table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
			<tr>    
				<td align=left>
            
					<%
                    'prepare for searching Like statement on CRSID
                    'mySQL = "SELECT CRSID " _
                    '      & "FROM   CRS " _
                    '      & "WHERE  CRSID Like '" & allIssuesID & "%' "

					'mySQL = "SELECT CRS.CRSID, CRS.Status" _
					'		  & "	FROM CRS " _
					'		  & "	WHERE CRS.CRSID Like '" & allIssuesID & "%' " _							  
					'		  & " UNION " _
					'		  & "SELECT CRS.CRSID, CRS.Status" _
					'		  & "	FROM CRS INNER JOIN CRS_Related ON CRS.CRSID = CRS_Related.CRSID " _
					'		  & "	WHERE CRS_Related.RelatedCRSID = '" & idIssue & "' " _							  
					'		  & " UNION " _
					'		  & "SELECT CRS.CRSID, CRS.Status" _
					'		  & "	FROM CRS INNER JOIN CRS_Related ON CRS.CRSID = CRS_Related.RelatedCRSID " _
					'		  & "	WHERE CRS_Related.CRSID = '" & idIssue & "' " _
					'		  & "   ORDER BY [CRSID] ASC "


						mySQL = "SELECT CRS.CRSID, CRS.Status " _
							  & "	FROM CRS " _
							  & "	WHERE CRS.CRSID Like '" & left(idIssue,len(idIssue)-3) & "%' " _							  
							  & " UNION " _
							  & "SELECT CRS.CRSID, CRS.Status " _
							  & "	FROM CRS INNER JOIN CRS_Related ON CRS.CRSID = CRS_Related.CRSID " _
							  & "	WHERE CRS_Related.RelatedCRSID = '" & idIssue & "' " _							  
							  & " UNION " _
							  & "SELECT CRS.CRSID, CRS.Status " _
							  & "	FROM CRS INNER JOIN CRS_Related ON CRS.CRSID = CRS_Related.RelatedCRSID " _
							  & "	WHERE CRS_Related.CRSID = '" & idIssue & "' " _
							  & "ORDER BY [CRSID] ASC "
						mySQL = mySQL	

					mySQL = mySQL	
					set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                    
                    if rs.eof then
                        response.write("No issues found.")
                    else
    
						'set alternating row colors
            			rowColor = col2

                        response.write("<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">")
                        response.write("  <tr>")
                        response.write("    <td class=""listRowTop"">Issue #</td>")
                        response.write("    <td class=""listRowTop"" align=center>Status was</td>")
                        response.write("    <td class=""listRowTopEnd"" align=center>Status is</td>")
                        response.write("  </tr>")

                        do until rs.eof    

							'update record in table
							mySQL = "UPDATE CRS SET Status = '" & masterStatus & "' "
								  								  
							if len(CaseNotes) > 0 then mySQL = mySQL & ",CaseNotes= (CASE WHEN [CaseNotes] IS NOT NULL THEN [CaseNotes] + '<br>' ELSE '' END) + '[" & userFullName & ", " & Now() &" ET] " & replace(CaseNotes,"'","''") & "' "
							if len(DateClosed) > 0 then mySQL = mySQL & ",DateClosed='" & DateClosed & "' "
							if len(Deadline) > 0 then mySQL = mySQL & ",DateDeadline='" & Deadline & "' "	
								  
							'finalize update query
							mySQL = mySQL & " WHERE CRSID = '" & rs("crsid") & "' "
							set rsFUP = openRSexecuteDialog(mySQL)
		
                            response.write("<tr>")
                            response.write("  <td height=22 style=""background-color:" & rowColor & "; border-bottom:1px solid #969696; border-left:1px solid #969696; padding:3px;"">" & rs("crsid") & "</td>")
                            response.write("  <td height=22 style=""background-color:" & rowColor & "; border-bottom:1px solid #969696; padding:3px;"" align=center>" & rs("status") & "&nbsp;</td>")
                            response.write("  <td height=22 style=""background-color:" & rowColor & "; border-bottom:1px solid #969696; border-right:1px solid #969696; padding:3px;"" align=center>" & masterStatus & "&nbsp;</td>")
                            response.write("</tr>")
                            rs.movenext
    
	                		'Switch Row Color
                			if rowColor = col2 then rowColor = col1 else rowColor = col2
							
                        loop
                    
                        response.write("</table>")
                                    
                    end if		
                    
                    call closeRS(rs)						
                    %>            
            
	            </td>
    	    </tr>
        </table>        
        
    <%
	end if
	%>

    <table class="formTable" style="width:100%;" border="0" cellspacing="0" cellpadding="0">        
		<tr>
			<td style="border:none; padding:0px; margin:0px;">&nbsp;</td>
      	</tr>            
		<tr>
			<td style="padding:0px; margin:0px;">&nbsp;</td>
      </tr>            
	</table>	

	<!-- START Buttons -->
	<div style="float:right;"> 	       		        
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Buttons -->

</body>
</html>

<%
'close database connection
call closeDB()
%>

<script type="text/javascript"> 
	// Initialize TinyMCE with the tab_focus option 
	var dateCal;
	dateCal = new dhtmlxCalendarObject([<% =activeCalendars %>], true, {isWinHeader:true, headerButtons:"XT", isWinDrag:false, isYearEditable:true, isMonthEditable:true });
	dateCal.setSkin("simplegrey");
	dateCal.setDateFormat("%m/%d/%Y");
	dateCal.setYearsRange(2000, 2020);
	dateCal.setHeaderText("Date");
</script>     
