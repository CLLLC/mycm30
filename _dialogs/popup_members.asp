<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim pageSize
dim totalPages
dim showArr
dim sortField
dim sAddress
dim curPage
dim showPhrase
dim showField
dim showStart

'get issue # -- optional since could be called from Add Issue
dim GroupID
GroupID = trim(Request.QueryString("recId"))

dim pageView
pageView = trim(Request.QueryString("pageView"))

'Get showPhrase
showPhrase = Request.Form("showPhrase")					'Form
if len(showPhrase) = 0 then
	showPhrase = Request.QueryString("showPhrase")		'QueryString
end if

dim userLink
userLink = trim(Request.QueryString("userlink"))
if userLink = "true" and cLng(session(session("siteID") & "adminLoggedOn")) >= 3 then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Choose a User</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />

	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/group.png" title="Issues" align="absmiddle">
       	<span class="popupTitle">Group Members</span>
   	</div>

	<!-- START Search box -->
    <form method="post" action="popup_members.asp?recid=<% =GroupID %>&cid=<% =customerID %>&pageView=<% =pageView %>&userlink=<% =userLink %>" name="frm" style="padding:0px; margin:0px;">
	<table class="formTable" style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
                <fieldset style="border:1px solid #CCCCCC; background-color:#FFFFFF; padding:0px;">
                    <legend>Search</legend>
                        <div align="left" style="padding:5px;">
                            <img src="../_images/icons/16/search.png" title="Search" width="16" height="16" align="absmiddle" style="margin-right:3px;">
                            <input type=text name="showPhrase" id="showPhrase" style="width:225px;" size=20 maxlength=50 value="<%=showPhrase%>">
							<input type="submit" name="btnSearch" id="btnSearch" value="Search" />
							<input type="button" name="btnClear" id="btnClear" value="Clear" onClick="this.form.showPhrase.value = ''; this.form.submit();" />
                        </div>
                </fieldset>
			</td>
        </tr>
    </table>	
    </form>
	<!-- STOP Search box -->


	<!-- START User/Contact table -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    
        <%
		'Find Users available TO BE assigned
		mySQL = "SELECT Logins.LOGID, Logins.CustomerID, Logins.FirstName, Logins.LastName, Logins.Email, Logins.Priority, Logins.Active, " _		
			  & "	CASE CAST(SecurityLevel AS varchar(2)) WHEN NULL THEN '--' WHEN '10' THEN 'db admin' WHEN '20' THEN '--' WHEN '30' THEN 'administrator' WHEN '40' THEN 'standard' WHEN '50' THEN 'restricted' WHEN '60' THEN 'disabled' ELSE '--' END AS [Security] " _
			  & "FROM Logins " _
		
		'for CCI Admins ONLY!
		if userLink = "true" and cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
			  mySQL = mySQL & " WHERE 1=1 "
		'all others...limit to CUSTOMERID
		else
			  mySQL = mySQL & " WHERE Logins.CustomerID = '" & customerID & "' "
		end if
		
		'DBAdmins and CCI Admins
		if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
			mySQL = mySQL & " AND Active<>'N' "		  
		'don't allow customers to see disabled users
		else
			mySQL = mySQL & " AND Active<>'N' AND SecurityLevel>10 "			
		end if		  
		
		'only pull users NOT already assigned
	  	mySQL = mySQL & " AND NOT EXISTS " _
			  & "      (SELECT Logins_Groups.LogID " _
		  	  & "       FROM   Logins_Groups " _
		  	  & "       WHERE  Logins.LogID = Logins_Groups.LogID " _
		  	  & "		  AND 	 Logins_Groups.GroupID = '" & GroupID & "' ) "

		'NOT sure about the level>10, dont know if i need to see operators
		'mySQL = mySQL & " AND Active<>'N' AND SecurityLevel>10 "
		  
		'finish sql statement
		mySQL = mySQL & " #ADD_QUERY# "

        mySQL = mySQL & " ORDER BY Logins.LastName, Logins.FirstName "
				  		  
        'Search Phrase
        if len(showPhrase) > 0 then
			mySQL = replace(mySQL,"#ADD_QUERY#", " AND ( (Logins.FirstName LIKE '%" & replace(showPhrase,"'","''") & "%') OR (Logins.LastName LIKE '%" & replace(showPhrase,"'","''") & "%') OR (CASE CAST(SecurityLevel AS varchar(2)) WHEN NULL THEN '--' WHEN '10' THEN 'DB Admin' WHEN '20' THEN '--' WHEN '30' THEN 'Administrator' WHEN '40' THEN 'Standard' WHEN '50' THEN 'Restricted' WHEN '60' THEN 'Disabled' ELSE '--' END LIKE '%" & replace(showPhrase,"'","''") & "%') ) ") 
		else
			mySQL = replace(mySQL,"#ADD_QUERY#", "")
		end if							
			
        'Open recordset
        set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
        if rs.eof then
        %>
            <tr>
                <td align=center valign=middle>
                <select name="listUser" id="listUser" style="width:100%; height:170px; margin:0px; margin-bottom:8px;" disabled="disabled" multiple="multiple" >
                	<option value="">No Users Found.</option>
                </select>
                </td>
            </tr>
        <%
        elseif rs.recordcount > 500 then
        %>	
            <tr>
                <td align=center valign=middle>
                <select name="listUser" id="listUser" style="width:100%; height:170px; margin:0px; margin-bottom:8px;" disabled="disabled" multiple="multiple" >
                	<option value="">More than 500 users where found.</option>
                	<option value="">Use the Search box to limit results.</option>
                </select>
                </td>
            </tr>
        <%
        else
        %>	        
            <tr>
                <td>
                                
                    <!-- START list of available users -->
                    <%
					dim tempUser, tempGroup
					
					response.write("<select name=""listUser"" id=""listUser"" style=""width:100%; height:200px; margin:0px; margin-bottom:5px;"" multiple=""multiple"">")
					
					do while not rs.eof					
						'setup name
						if len(rs("FirstName")) > 0 then
							tempUser = rs("LastName") & ", " & rs("FirstName")
						else
							tempUser = rs("LastName")
						end if
						'setup linked notice for CCI Admins ONLY!
						if userLink = "true" then
							if rs("Active") = "C" then tempUser = tempUser & " (contact only [linked:" & rs("CustomerID") & "])" else tempUser = tempUser & " (" & rs("Security") & " [linked:" & rs("CustomerID") & "])"
						'all other users
						else
							if rs("Active") = "C" then tempUser = tempUser & " (contact only)" else tempUser = tempUser & " (" & rs("Security") & ")"
						end if
						
                        response.Write("<option value=""" & rs("logID") & """>" & tempUser & "</option>")            
                        rs.movenext
                    loop
					
					response.write("</select>")
                    %>                    
                    <!-- STOP list of available users -->
                                        
                </td>
            </tr>        
        <%
        end if
        %>
        
    </table>
	<!-- STOP User/Contact table -->
	
	<!-- START Buttons -->
	<div style="float:left;">
        <% 
		response.write("Select: <a href=""javascript:selectAllOptions('listUser',true);"">All</a> | <a href=""javascript:selectAllOptions('listUser',false);"">None</a>")
		%>
	</div>
	<div style="float:right;">
        <a class="myCMbutton" href="#" accesskey="A" onClick="this.blur(); javascript:setUserList('listUser');"><span class="add" style="padding-right:10px;"><u>A</u>dd</span></a>
   		<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Buttons -->
        
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script language="javascript">	
	// dependent on SimpleModal.js
	// note: topWin.parent is defined in SimpleModal.js and replaces "window.opener"
	function setUserList(obj) {
		//add selected options to parent listUser
		var el = document.getElementById(obj);
		//make sure something was selected
		if (el!=null && el.options!=null) {			
			//cycle through all selected
			for(var i=0; el.options.length >i; i++) {
				if (el.options[i].selected == true) {
					topWin.parent.setUserList(el.options[i].text,el.options[i].value);						
				}
			}
		}		
		//close window
		SimpleModal.close();
	}		
</script>

