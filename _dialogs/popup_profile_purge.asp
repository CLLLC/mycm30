<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
Server.ScriptTimeout =25000
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, rsPurge, pageSize
dim fieldsSQL, valueSQL

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim countString
dim optConfigure

dim purgeID
dim purgeCount, purgeTotal
dim purgeDate : purgeDate = Now()
dim purgeFrom
dim purgeBar

dim cusRetention

dim modifiedDate : modifiedDate = Now()

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 3 then	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")		
end if

'Get action type 
dim action : action = Request.Form("action")
if len(action) = 0 then
	action = Request.QueryString("action")
end if
if lCase(action) <> "purge" and lCase(action) <> "test" then   	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Action Type.")	
end if




%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Record Retention</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>
      
	<!--JQuery required components -->
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>

	<!-- JQuery SPINNER https://github.com/btburnett3/jquery.ui.spinner/ -->
    <link type="text/css" rel="stylesheet" href="../_jquery/spinner/ui.spinner.css" />
	<script type="text/javascript" src="../_jquery/spinner/ui.spinner.js"></script>
    
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

	<!-- jQuery Prompt Replacements http://abeautifulsite.net/blog/2008/12/jquery-alert-dialogs/ -->
    <script src="../_jquery/prompt/jquery.alerts.js"></script>
    <link rel="stylesheet" type="text/css" href="../_jquery/prompt/jquery.alerts.css"/>
    
</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="popup_profile_purge.asp" style="padding:0px; margin:0px;">
    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/bin_closed.png" title="Retention" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Record Retention</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Configure and run record retention using form below.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<%
    'set sort order on save
    if request.form("save") = "save" and action = "purge" then

		cusRetention = Request.Form("cusRetention")
		if len(cusRetention) = 0 then   	
			response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Retention Period.")	
		end if
		%>

		<!-- START File Import -->
		<div id="step_1" style="padding-bottom:5px;"><img id="image_1" src="../_images/myloader.gif" align="absmiddle">&nbsp;Purging records</div>
		<div id="step_2" style="padding-bottom:5px;">&nbsp;</div>    

		<!-- PROGRESS BAR -->
        <div id="progressbarWrapper" style="height:10px;" class="ui-widget-default">
            <div id="container" style="height:100%;"></div>
        </div>
        <script>
           	$(function() {	
       	     	//call progress bar constructor
           	    $("#container").progressbar();
           	});  
        </script>
                
		<div id="step_6" style="padding-top:5px;">&nbsp;</div>    
		<div style="border-bottom:1px dotted #cccccc;">&nbsp;</div>
        
        <!-- START Buttons -->
        <div style="float:right; padding-top:13px;"> 	       		
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
        </div>    
        <!-- STOP Buttons -->
    
        <%
        'push to screen NOW
        response.flush()
        %>    
        
        <!-- STEP 1 stored procedure -->
        <script language="javascript">
            document.getElementById('image_1').src = '../_images/myloader.gif';
        </script>        

		<%			
        response.flush()			
		
		'make sure there is something to purge
		mySQL = "SELECT COUNT(CRSID) AS [PurgeTotal] FROM CRS WHERE (PurgeID < 0 OR PurgeID IS NULL) AND CustomerID = '" & uCase(customerID) & "' AND [Date] < '" & dateAdd("m" , -1*cusRetention, Date()) & "' "
        'Open recordset
        set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)	
		purgeTotal = rs("PurgeTotal")
		
		'records found...keep going
		if purgeTotal > 0 then
			
			'create purge record and unique ID
			mySQL = "INSERT INTO Purge (" _
				  & "CustomerID, Logid, PurgeDate, Records, ModifiedBy, ModifiedDate" _
				  & ") VALUES (" _
				  & "'" & uCase(customerID)	& "'," _
				  & " " & sLogid 			& ", " _
				  & "'" & purgeDate			& "'," _
				  & " " & purgeTotal 		& ", " _
				  & " " & sLogid 			& ", " _
				  & "'" & modifiedDate 		& "' " _
				  & ")"		
			'set identity to pull back newly added record new record id is returned to newID
			mySQL = "SET NOCOUNT ON; " & mySQL & " SELECT SCOPE_IDENTITY() AS newID;"
			set rs = openRSexecute(mySQL)		
			purgeID = rs("newID")		  				
		
			'pull records to purge
			mySQL = "SELECT CRSID FROM CRS WHERE (PurgeID < 0 OR PurgeID IS NULL) AND CustomerID = '" & uCase(customerID) & "' AND [Date] < '" & dateAdd("m" , -1*cusRetention, Date()) & "' "
			'Open recordset
			set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)					
			
			'starge purging
			if not rs.eof then	
				purgeTotal = rs.recordcount

				'Set CursorLocation of the Connection Object to Client
				cn.CursorLocation = adUseClient
				
				do until rs.eof
				
					'BEGIN Transaction
					'cn.BeginTrans

					mySQL = "UPDATE CRS Set " _
							& "Caller_Password = Null," _
							& "Caller_FirstName = Null," _
							& "Caller_LastName = Null," _
							& "Caller_Title = Null," _
							& "Caller_Address = Null," _
							& "Caller_City = Null," _
							& "Caller_State = Null," _ 
							& "Caller_PostalCode = Null," _ 
							& "Caller_Home = Null," _ 
							& "Caller_Work = Null," _ 
							& "Caller_Cell = Null," _ 
							& "Caller_Pager = Null," _ 
							& "Caller_Fax = Null," _ 
							& "Caller_Other = Null," _ 
							& "Caller_Email = Null," _ 
							& "Caller_BestTime = Null," _					
							& "Summary = '[purged]'," _
							& "Details = '[purged]'," _
							& "Addendum = '[purged]'," _
							& "PurgeID = "	& purgeID & " " _
							& "WHERE CRSID='" & rs("CRSID") & "' "   
					set rsPurge = openRSexecute(mySQL)				

					'END Transaction
					'cn.CommitTrans
					
					purgeCount = purgeCount + 1
					%>
					<script language="javascript">
						document.getElementById('step_2').innerHTML = '&nbsp;Purging : <% =purgeCount%> of <% =purgeTotal %>';
					</script>        
                    

					<!-- PROGRESS BAR -->
                    <script>                      
                      	//call progress bar constructor -- value range is converted to 0 to 100%
                      	$("#container").progressbar({ value: 100/parseInt('<% =purgeTotal%>') * parseInt('<% =purgeCount%>') });
                    </script>
                    
					<%								
					response.flush()							
					rs.movenext
				loop	
			end if		
			call closeRS(rs)
		
		'nothing found to purge
		else		
			%>
			<script language="javascript">
				document.getElementById('step_2').innerHTML = '<img src="../_images/error.png" align="absmiddle">&nbsp;<strong>Error:</strong> No records found to purge';
			</script>        
			<%										
		end if

		%>
		<script language="javascript">
			document.getElementById('image_1').src = '../_images/check.png';
		</script>
		<%				
        response.flush()
        %>        


		<%
		if purgeTotal > 0 then
			%>
			<!-- STEP 6 Finalize -->
			<script language="javascript">
				document.getElementById('step_6').innerHTML = '&nbsp;Purge <strong>COMPLETE</strong>&nbsp;&nbsp;[<a href=\"popup_profile_purge_export.asp?cid=<% =customerID %>&action=csv&recid=<% =purgeID %>\">download records</a>]';
			</script>                 
	    	<%
		else
			%>
			<!-- STEP 6 Finalize -->
			<script language="javascript">
				document.getElementById('step_6').innerHTML = '&nbsp;Purge <strong>STOPED</strong>';
			</script>                 		
			<%
		end if
        response.flush()
        %>

		<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
		<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

		<%
		
    'TEST only
    elseif request.form("save") = "save" and action = "test" then

		cusRetention = Request.Form("cusRetention")
		if len(cusRetention) = 0 then   	
			response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Retention Period.")	
		end if

		'make sure there is something to purge
		mySQL = "SELECT COUNT(CRSID) AS [PurgeTotal] FROM CRS WHERE (PurgeID < 0 OR PurgeID IS NULL) AND CustomerID = '" & uCase(customerID) & "' AND [Date] < '" & dateAdd("m" , -1*cusRetention, Date()) & "' "
        'Open recordset
        set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)	
		purgeTotal = rs("PurgeTotal")

		'make sure there is something to purge
		mySQL = "SELECT MIN([Date]) AS [PurgeFrom] FROM CRS WHERE (PurgeID < 0 OR PurgeID IS NULL) AND CustomerID = '" & uCase(customerID) & "' AND [Date] < '" & dateAdd("m" , -1*cusRetention, Date()) & "' "
        'Open recordset
        set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)	
		purgeFrom = rs("PurgeFrom")		
		%>

		<!-- START File Import -->
        <div id="step_1" style="padding-bottom:5px;">&nbsp;Retention (mos): <strong><% =cusRetention %></strong></div>
		<div id="step_1" style="padding-bottom:5px;">&nbsp;Purge dates: <strong><% =purgeFrom %></strong> to <strong><% =  dateAdd("d", -1, dateAdd("m" , -1*cusRetention, Date()) ) %></strong></div>
		<div id="step_2" style="padding-bottom:8px;">&nbsp;Purge count: <strong><% =purgeTotal %></strong></div>            
		<div style="padding-bottom:7px; border-bottom:1px dotted #cccccc;">&nbsp;</div>
		<div id="step_6">&nbsp;</div>            
        
        <!-- START Buttons -->
        <div style="float:right;"> 	       		
            <a class="myCMbutton" href="#" accesskey="E" onClick="this.blur(); if(validateForm()==true){confirmPurge('purge')}; return false;"><span class="setup" style="padding-right:7px;"><u>E</u>xecute</span></a>        
            <a class="myCMbutton" href="#" onClick="this.blur(); if(validateForm()==true){confirmPurge('test')}; return false;"><span class="search" style="padding-right:7px;">Test</span></a>
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
        </div>
        <!-- STOP Buttons -->
        
        <input name="action" id="action" type="hidden" value="purge">    
        <input name="cid" type="hidden" value="<% =customerid %>">
        <input name="cusRetention" type="hidden" value="<% =cusRetention %>">        
        <input name="save" type="hidden" value="save">
    								
	<%	
	'show form
    else
	
		mySQL = "SELECT Retention FROM Customer WHERE CustomerID = '" & customerID & "' "
        'Open recordset
        set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)	
		'Assign retention policy
		if not rs.eof then	
			cusRetention = rs("retention")
		else
			response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")		
		end if		
		call closeRS(rs)
        %>
	
        <!-- START Category table -->
        <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">
        
            <tr>
				<td class="formLabel" style="padding-bottom:30px;">Retention:</td>
				<td align=left style="padding-bottom:30px;">
					<%
					'required or jQuery spinner doesn't work
					if isNull(cusRetention) or cusRetention = "" or len(cusRetention) <= 0 then
						cusRetention = "0"
					end if
					%>                                  
					<input name="cusRetention" id="cusRetention" class="inputShort" value="<% =cusRetention %>" maxlength="255" />&nbsp;month(s)
					<div class="subLabel" style="padding-top:5px;">Numeric entries only, no special characters</div>
				</td>
			</tr>
			<script type="text/javascript">
				jQuery().ready(function($) {
					$('#cusRetention').spinner({ min: 0, step: 1, increment: 'fast' });
				});
			</script>

            </tr>
        
            <tr>
                <td colspan="2" align=left nowrap style="padding:0px; margin:0px;">&nbsp;</td>
            </tr>
            
        </table>
        <!-- STOP Category table -->
    
        <input name="action" id="action" type="hidden" value="purge">    
        <input name="cid" type="hidden" value="<% =customerid %>">
        <input name="save" type="hidden" value="save">
            
        <!-- START Buttons -->
        <div style="float:right;"> 	       		
            <a class="myCMbutton" href="#" accesskey="E" onClick="this.blur(); if(validateForm()==true){confirmPurge('purge')}; return false;"><span class="setup" style="padding-right:7px;"><u>E</u>xecute</span></a>        
            <a class="myCMbutton" href="#" onClick="this.blur(); if(validateForm()==true){confirmPurge('test')}; return false;"><span class="search" style="padding-right:7px;">Test</span></a>
            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
        </div>
        <!-- STOP Buttons -->
    
		<%	
    end if
    %>

</form>
        
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script>
	function validateForm() {		
		var formPassed = true;	//retunred value
		var reqFields = "cusRetention";
		var temp = reqFields.split(",");
		var i;
		var el;		
		
		for(i=0; i<temp.length; i++) {			
			if (document.getElementById(temp[i])) {			
				el = document.getElementById(temp[i])
				if (!el.value) {			
					el.style.background = '#ffff9f'; //set to yellow			
					jAlert('<strong>Form Error!</strong><br/><br/>Please ensure all required fields have been entered.', 'myCM Alert'); //inform user of error
					formPassed = false; //return error
				}
				else {				
					if (el.style.background == '#ffff9f') {
						//alert('2:' + temp[i]);
						el.style.background = '#fff';	//set back to white
					}
				}		
			}
		}
		
		return formPassed;
		
	}	
	
	function confirmPurge(action) {		
		if (action=='test') {
			document.getElementById("action").value = action;
			document.frm.submit();			
		}
		else if (action=='purge') {
			jConfirm('Are you sure you want to purge these records?<br/><br/><strong>This action CANNOT be undone</strong>.<br/><br/>', 'Confirm Purge', function(r) {			
				if (r==true) {				
					document.frm.submit();
				}
				else {
					return false;
				}
			});					
		}
	}
</script>

