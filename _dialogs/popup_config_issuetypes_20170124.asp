<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, pageSize
dim fieldsSQL, valueSQL

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

'work fields
dim I
dim item
dim count
dim countString
dim optConfigure
dim optArr

dim modifiedDate : modifiedDate = Now()

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and programAdmin = "N" then	
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if

'Get action type 
dim action : action = Request.Form("action")
if len(action) = 0 then
	action = Request.QueryString("action")
end if
if lCase(action) <> "edit" and _
   lCase(action) <> "add" and _
   lCase(action) <> "del" then   	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Action Type.")	
end if

'Get record ID if editing or deleting
if lCase(action) = "edit" or lCase(action) = "del" then
	dim recId : recId = Request.Form("recId")
	if len(recId) = 0 then
		recId = Request.QueryString("recid")
	end if
	if len(recId) = 0 or isNull(recId) or recId = "" then		
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Issue Type ID.")	
	end if
end if

'form variables
dim optName
dim optResolutions
dim optDeadlineDays
dim optInvDeadlineDays
dim optCloseOn
dim optInvCloseOn

dim expI

'set sort order on save
if request.form("save") = "save" then

	'get new user field values
	'optName = request.form("optName")
	optResolutions = request.form("optResolutions")
	optDeadlineDays = request.form("optDeadlineDays")
	optInvDeadlineDays = request.form("optInvDeadlineDays")
	optCloseOn = request.form("optCloseOn")
	optInvCloseOn = request.form("optInvCloseOn")
	
	'save existing user field
	if action = "edit" then
	
		mySQL = "UPDATE Customer_IssueType Set " _
				& "Resolutions='"	& optResolutions	& "'," _
				& "CloseOn='"		& optCloseOn		& "'," _				
				& "InvCloseOn='"	& optInvCloseOn		& "'," _								
			    & "ModifiedBy="  	& sLogid			& ", " _
			    & "ModifiedDate='"	& modifiedDate		& "' "
		'check for deadline days
		if len(optDeadlineDays) > 0 then mySQL = mySQL & ",DeadlineDays=" & trim(optDeadlineDays) & " " else mySQL = mySQL & ",DeadlineDays=null "										
		if len(optInvDeadlineDays) > 0 then mySQL = mySQL & ",InvDeadlineDays=" & trim(optInvDeadlineDays) & " " else mySQL = mySQL & ",InvDeadlineDays=null "												
		'finalize update query
		mySQL = mySQL & "WHERE CustIssueTypeID=" & recId & " "  
		'execute
		set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

		session(session("siteID") & "okMsg") = "Issue Type has been saved successfully."
	
	'add new user field
	elseif action = "add" then

' -- DON'T NEED THIS AS OF YET.. STILL ADDING ISSUE TYPES WITHIN CUSTOMER PROFILE			
'		mySQL = "UPDATE Customer_IssueType Set " _
'			    & "ModifiedBy="  	& sLogid		& ", " _
'			    & "ModifiedDate='"	& modifiedDate	& "' "
'		'finalize update query
'		mySQL = mySQL & "WHERE CustomerID='" & customerID & "' "  
'		'execute
'		set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
'
'		session(session("siteID") & "okMsg") = "Issue Type was added."

	end if		
	%>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
	
	<script type='text/javascript'>
		topWin.parent.pageReload('<% =lCase(customerID) %>');
		SimpleModal.close();
    </script>	
	
<%
elseif action = "del" then

' -- DON'T NEED THIS AS OF YET.. STILL ADDING ISSUE TYPES WITHIN CUSTOMER PROFILE			

'	mySQL = "UPDATE Customer Set " _
'			& "pUserField" & recId & "="		& "Null" 	& ", " _
'			& "pUserField" & recId & "Value=" 	& "Null"	& ", " _
'			& "pUserField" & recId & "Type=" 	& "Null" 	& ", " _
'			& "pUserField" & recId & "Group=" 	& "Null" 	& ", " _
'		    & "ModifiedBy="  	& sLogid					& ", " _
'		    & "ModifiedDate='"	& modifiedDate				& "' "
'	'finalize update query
'	mySQL = mySQL & "WHERE CustomerID='" & customerID & "' "  
'	'execute
'	set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
'	
'	session(session("siteID") & "okMsg") = "User Field was deleted."
'	response.redirect "../profiles/userfields.asp?cid=" & customerID	
	
end if

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Issue Type</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>
      
	<!-- HTML List/Table resorting functions (right now own) -->
	<!-- Example usage:
    		var table = document.getElementById('table-1');
			var tableDnD = new TableDnD();
			tableDnD.init(table);  -->
	<script type="text/javascript" src="../scripts/javascript/orderList.js"></script>

	<!--JQuery required components -->
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />
    
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

	<!-- jQuery Prompt Replacements http://abeautifulsite.net/blog/2008/12/jquery-alert-dialogs/ -->
    <script src="../_jquery/prompt/jquery.alerts.js"></script>
    <link rel="stylesheet" type="text/css" href="../_jquery/prompt/jquery.alerts.css"/>

	<!-- JQuery SPINNER https://github.com/btburnett3/jquery.ui.spinner/ -->
    <link type="text/css" rel="stylesheet" href="../_jquery/spinner/ui.spinner.css" />
	<script type="text/javascript" src="../_jquery/spinner/ui.spinner.js"></script>
    
</head>

<body style="padding:20px;">

<form name="frm" id="frm" method="post" action="popup_config_issuetypes.asp" style="padding:0px; margin:0px;">
    
	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/server_components.png" title="Categories" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Issue Type</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Configure selected issue type below.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Category table -->
    <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">
    
        <%
		if lCase(action) = "edit" then
			
			mySQL = "SELECT Customer.CustomerID, IssueType.Name, Customer_IssueType.CustIssueTypeID, Customer_IssueType.Resolutions, " _
				  & "Customer_IssueType.DeadlineDays, Customer_IssueType.InvDeadlineDays, Customer_IssueType.CloseOn, Customer_IssueType.InvCloseOn " _
				  & "	FROM Customer INNER JOIN " _
				  & "		 Customer_IssueType ON Customer.CustomerID = Customer_IssueType.CustomerID INNER JOIN " _
				  & "		 IssueType ON Customer_IssueType.IssueTypeID = IssueType.IssueTypeID " _
				  & "	WHERE Customer_IssueType.CustIssueTypeID = " & recId & " "								  
			'open customer table...				  
	        set rs = openRSopenDialog(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
			
			if rs.eof then				
				response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Issue Type ID.")	
			end if		

			optName 			= rs("Name")
			optResolutions 		= rs("Resolutions")
			optDeadlineDays 	= rs("DeadlineDays")
			optInvDeadlineDays 	= rs("InvDeadlineDays")
			optCloseOn 			= rs("CloseOn")
			optInvCloseOn 		= rs("InvCloseOn")

		end if
        %>

		<tr>
       		<td class="formLabel" style="border-top:none; margin-top:0px; padding-top:0px;"><span class="required">*</span>Name:</td>
       		<td align=left style="border-top:none; margin-top:0px; padding-top:0px;"><% =optName %><input name="optName" id="optName" type="hidden" value="<% =optName %>"></td>
   		</tr>

		<tr>
       		<td class="formLabel">Use Resolutions:</td>
       		<td align=left>
            	<div>
                    <label>
                        <input type="radio" name="optResolutions" id="optResolutionsYes" value="Y" style="vertical-align:middle;" <%=checkRadio(optResolutions,"Y")%> >Yes&nbsp;&nbsp;
                    </label>			      
                    <label>
                        <input type="radio" name="optResolutions" id="optResolutionsNo" value="N" style="vertical-align:middle;" <%=checkRadio(optResolutions,"N")%>>No
                    </label>            
                </div>
				<div class="subLabel" style="padding-top:5px;">Sets visiblity to 'Resolution' side tab.</div>                 
       		</td>
   		</tr>

		<tr>
       		<td class="formLabel">Deadlines:</td>
       		<td align=left>
				<div style="float:left;">
       				<div class="subLabel">Issues</div>
                	<input name="optDeadlineDays" id="optDeadlineDays" class="inputShort" value="<% =optDeadlineDays %>" maxlength="20" />
                 </div>                                    
             	<div>
              		<div class="subLabel">Investigations</div>
                	<input name="optInvDeadlineDays" id="optInvDeadlineDays" class="inputShort" value="<% =optInvDeadlineDays %>" maxlength="20" />
                </div>                                    
				<div class="subLabel" style="padding-top:5px;">Sets default number of days for deadlines.</div>
       		</td>
   		</tr>
		<script>
			//forces numeric entries only
			jQuery('#optDeadlineDays').keyup(function () {this.value = this.value.replace(/[^0-9\.]/g,''); });
		</script>
		<script>
			//forces numeric entries only
			jQuery('#optInvDeadlineDays').keyup(function () {this.value = this.value.replace(/[^0-9\.]/g,''); });
		</script>

		<tr>
       		<td class="formLabel">Issues Closed On:</td>
       		<td align=left>
            	<%
				optArr = getDropDownOpt("pCaseStatus",customerID)
				call buildSelect(optArr,optCloseOn,"optCloseOn","inputMediumLong","Y")				
				%>
				<div class="subLabel" style="padding-top:5px;">Sets 'close date' on issue.</div>                 
       		</td>
   		</tr>

		<tr>
       		<td class="formLabel">Investigations Closed On:</td>
       		<td align=left>
            	<%
				optArr = getDropDownOpt("pInvestigationStatus",customerID)
				call buildSelect(optArr,optInvCloseOn,"optInvCloseOn","inputMediumLong","Y")				
				%>
				<div class="subLabel" style="padding-top:5px;">Sets 'close date' on investigation.</div>                 
       		</td>
   		</tr>


        
		<tr>
           	<td colspan="2" align=left nowrap style="padding:0px; margin:0px;">&nbsp;</td>
       	</tr>
        
    </table>
	<!-- STOP Category table -->

	<input name="recid" type="hidden" value="<% =recid %>">
    <input name="action" type="hidden" value="<% =action %>">    
    <input name="cid" type="hidden" value="<% =customerid %>">
    <input name="save" type="hidden" value="save">
        
	<!-- START Buttons -->
	<div style="float:right;"> 	       		
        <a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); if(validateForm()==true){document.frm.submit()};"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
	</div>
	<!-- STOP Buttons -->

</form>
        
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script>
	function validateForm() {
		var formPassed = true;	//retunred value
		var reqFields = "optName";
		var temp = reqFields.split(",");
		var i;
		var el;		
		
		for(i=0; i<temp.length; i++) {			
			if (document.getElementById(temp[i])) {			
				el = document.getElementById(temp[i])
				if (!el.value) {			
					el.style.background = '#ffff9f'; //set to yellow			
					jAlert('<strong>Form Error!</strong><br/><br/>Please ensure all required fields have been entered.', 'myCM Alert'); //inform user of error
					formPassed = false; //return error
				}
				else {				
					if (el.style.background == '#ffff9f') {
						//alert('2:' + temp[i]);
						el.style.background = '#fff';	//set back to white
					}
				}		
			}
		}
		return formPassed;		
	}		
</script>



