<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%
'Database
dim mySQL, cn, rs, field
dim count


'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim pageView : pageView = Request.QueryString("pageView")

dim idCategory
idCategory = trim(Request.QueryString("recId"))
if idCategory = "" then	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Category ID.")
end if

'--------------------------------------------------
'get user record...
'--------------------------------------------------	
mySQL = "SELECT Category.Name, Category.Sub, Category.Sev1, Category.Sev2, Category.Sev3, Category.Notes, Directive.DirectiveID, Directive.Name AS [Directive] " _
	  & "	FROM Category LEFT JOIN Directive ON (Category.CustomerID = Directive.CustomerID) AND (Category.DirectiveID = Directive.DirectiveID) " _
	  & "	WHERE  CategoryID=" & idCategory & " AND Active='Y' "
	  	
'open recordset   			  
set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
if rs.eof then	
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Category ID.")
end if
'--------------------------------------------------	

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Category</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    

	<style type="text/css">
    <!--
    .sectionHeader {
       padding:3px !important; 
	   margin:0px !important; 
	   background-color:#E2E2E2 !important; 
	   font-weight:bold !important; 
	   border-top:1px solid #5B5B5B !important; 
	   border-bottom:1px solid #5B5B5B !important;
       }
	 .printTable td {
		vertical-align:top;
		border-top:1px dotted #cccccc;
		padding-top:8px;
		padding-bottom:10px;
	 }	   

	.printTable .printLabel 			{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: none;}
	.printTable .printLabel:link 		{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: none;}
	.printTable .printLabel:link:hover 	{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: underline;}
	.printTable .printLabel:active 		{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: none;}
	.printTable .printLabel:visited 	{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: none;}
	.printTable .printLabel:hover 		{text-align:left; font-weight:bold; color:#35487B; padding-left:6px; padding-right:6px; white-space:normal; text-decoration: none;}
	 
	-->
    </style>

	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
   
</head>

	<body style="padding:20px;">

		<div style="text-align:left; margin-bottom:10px; float:left;">
    	   	<span style="color:#35487B; position:static; font-family:arial; font-size:14px; font-weight:bold; margin-bottom:10px;">Category</span>
	   	</div> 

        <!-- START Buttons -->
        <div style="text-align:right; padding-bottom:10px;">
        	<% if pageView = "issue" or pageView = "issue:00" then %>
	            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); topWin.parent.StartTheTimer(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>        
            <% else %>
	            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>        
            <% end if %>            
        </div>    
        <!-- STOP Buttons -->
        
        <!-- START Form Fields -->	                    
		<table id="table:details:general" width="100%" class="printTable" cellpadding="0" cellspacing="0">

            <tr>
                <td class="sectionHeader">Category</td>
            </tr>                            
            
			<%
			response.write("<tr>")
			response.write("	<td colspan=""2"" class=""printLabel"" style=""border-top:none; background-color:#FFFFC8;"">" & rs("Name") & "</td>")
			response.write("</tr>")
			%>

            <tr>
                <td style="border:none; padding:0px; margin:0px;">
	
                    <table id="table:details:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">
                        
						<% if len(rs("sub"))>0 then %>
                            <tr>
                                <td class="formLabel" style="width:65px; border:none; margin:5px; padding:5px;">Sub:</td>
                                <td align=left style="border:none; margin:5px; padding:5px;"><% =rs("sub") %>&nbsp;</td>
                            </tr>
						<% end if %>                                

						<!-- show Directive for CCI staff -->
						<% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>
                            <tr>                            
                                <td colspan="2" align="center" style="margin:0px; padding:0px;">
                                
                                    <table width="95%">    
                                        <tr>
                                            <td class="formLabel" style="width:65px; border:none; margin:5px; padding:5px;">Directive:</td>
                                            <% if rs("directiveid") > 0 then %>
	                                            <td align=left style="border:none; margin:5px; padding:5px;"><a href="#" onClick="SimpleModal.open('../_dialogs/popup_directive_record.asp?recid=<% =rs("directiveid") %>', 500, 625, 'yes'); return false;"><img src="../_images/icons/16/magnifier.png" border="0" align="absmiddle" style="padding-right:5px;"/><% =rs("directive") %>&nbsp;</a></td>
                                            <% else %>
											    <td align=left style="border:none; margin:5px; padding:5px;"><em>no special instructions</em></td>
                                            <% end if %>
                                        </tr>    
                                    </table>
                                    
                                </td>                                                 
                            </tr>                            
						<% end if %>                                
                
                        <!-- START Issue Type Table -->    
                        <tr <% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then response.write("style=""display:none;""") %>>
                            <td class="formLabel" style="width:90px;">Issue Types:</td>    
                            <td align=left>
                            
                                <div id="field_list_container">
                                    <div id="field_list" style="width:285px; border: 1px solid #ccc; padding: 2px; height:60px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">
                                        <!-- table used for javascript slider -->
                                        <table id="table:field:list" width="95%" border="0" cellspacing="0" cellpadding="0">
                                            <%
											dim rsTemp											  
												  
											mySQL = "SELECT Category.CategoryID, Category.Name, Category.Sub, IssueType.Name " _
												  & "	FROM (Category INNER JOIN Category_IssueType ON Category.CategoryID = Category_IssueType.CategoryID) INNER JOIN " _
												  & "		IssueType ON Category_IssueType.IssueTypeID = IssueType.IssueTypeID " _
												  & "	WHERE Category.CategoryID=" & idCategory & " AND Active='Y' " _
												  & "	ORDER BY IssueType.Name "
												  
												  				
                                            set rsTemp = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                            do while not rsTemp.eof
                                                response.write("<tr><td class=""clearFormat"" style=""border:none; padding:0px; margin0px;"" align=""left"">")                                                    
                                                response.write("	<div>" & rsTemp("Name") & "</div>")											
                                                response.write("</td></tr>")          
                                                rsTemp.movenext
                                            loop
                                            call closeRS(rsTemp)
                                            %>
                                        </table>
                                    </div>
                                </div>
                                
                            </td>
                
                        </tr>                       
                        <!-- STOP Issue Type Table -->

						<!-- START Severity Time Frames -->
                      	<tr <% if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then response.write("style=""display:none;""") %>>                           
                     		<td colspan="2" align="center" style="margin:0px; padding:0px;">
                                
                     			<table width="95%">    
                              		<tr>
                            			<td class="formLabel" style="width:65px; border:none; margin:5px; padding:5px;">Call Back:</td>
                                        <td align=left style="border:none; margin:5px; padding:5px;">Severity 1: <% =rs("sev1") %> days<br />Severity 2: <% =rs("sev2") %> days<br />Severity 3: <% =rs("sev3") %> days</td>
                                    </tr>    
                                </table>
                                    
                            </td>                                                 
                        </tr>
						<!-- STOP Severity Time Frames -->
                        
                        <!-- START Definition -->
                        <tr>                            
                            <td colspan="2" align="center" style="margin:0px; padding:0px;">
                              	<table width="95%">
                                  	<tr>
                                      	<td align="left" style="border:none; margin-top:0px; margin-bottom:10px; padding-top:0px; padding-bottom:10px;">
                               				<div style="color:#35487B; font-weight:bold; padding:3px;">Notes</div>
											<% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>
	                                            <div style="border:#CCC 1px solid; padding:5px; text-align:left; vertical-align:text-top; height:100px; overflow:auto; overflow-x: hidden; overflow-y: scroll;"><% =replace(rs("notes") & "",vbCRLF,"<br>") %>&nbsp;</div>
                                            <% else %>
	                                            <div style="border:#CCC 1px solid; padding:5px; text-align:left; vertical-align:text-top; height:70px; overflow:auto; overflow-x: hidden; overflow-y: scroll;"><% =replace(rs("notes") & "",vbCRLF,"<br>") %>&nbsp;</div>
											<% end if %>
                           				</td>
                                    </tr>
                                </table>
                            </td>                                                 
                        </tr>
                        <!-- STOP Definition -->
                                                                
                    </table>
        
        		</td>
            
        	</tr>

			<%
			call closeRS(rs)
            %>        

		</table>
		<!-- END Form Fields -->
                       

		<!-- START END of issue notice -->  
		<table width="100%" class="printTable" style="margin-bottom:5px;" cellpadding="0" cellspacing="0">                                   
			<tr>
				<td class="sectionHeader" style="text-align:center">- End of Record -</td>
			</tr>
		</table>
		<!-- STOP END of issue notice -->                          

		<div style="text-align:left; float:left;">&nbsp;</div> 
        
        <!-- START Buttons -->
        <div style="text-align:right; padding-bottom:10px;">
        	<% if pageView = "issue" or pageView = "issue:00" then %>
	            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); topWin.parent.StartTheTimer(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>        
            <% else %>
	            <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>        
            <% end if %>            
        </div>    
        <!-- STOP Buttons -->

	</body>

</html>

<%
'close database connection
call closeDB()
%>


