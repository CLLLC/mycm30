<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'Database
dim mySQL, cn, rs, count

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim pageView : pageView = Request.QueryString("pageView")

'Get action type 
dim action : action = Request.Form("action")
if len(action) = 0 then
	action = Request.QueryString("action")
end if

'Get master issue
dim idIssue : idIssue = Request.Form("recId")
if len(idIssue) = 0 then
	idIssue = Request.QueryString("recId")
end if

'Get related issue
dim idRelated : idRelated = Request.Form("recRelated")
if len(idRelated) = 0 then
	idRelated = Request.QueryString("recRelated")
end if


'*********************************************************
'Add newly linked issue
if action = "add" then
	'can't link to itself
	if lcase(idIssue) <> lcase(idRelated) then	
		mySQL = "INSERT INTO CRS_Related (" _
			  & "CRSID, RelatedCRSID, ModifiedBy, ModifiedDate" _
			  & ") VALUES (" _
			  & "'" & idIssue 		& "'," _
			  & "'" & idRelated		& "'," _
			  & " " & sLogid 		& ", " _
			  & "'" & Now()	& "' " _
			  & ")"
		set rs = openRSexecuteDialog(mySQL)
		session(session("siteID") & "okMsg") = "Issue linked."
	else
		session(session("siteID") & "okMsg") = "Error linking issue."
	end if
	%>       
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>					
	<script type='text/javascript'>
		topWin.parent.pageReload();
		SimpleModal.close();
	</script>	
	%>
<%
end if


%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Link an Issue</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- HTML Table Filter Generator http://tablefilter.free.fr/ -->
    <link rel="stylesheet" type="text/css" href="../_tablefilter/filtergrid.css"/>
	<script type="text/javascript" src="../_tablefilter/tablefilter_all.js"></script>
	<script type="text/javascript" src="../_tablefilter/sortabletable.js"></script>
    <script type="text/javascript" src="../_tablefilter/tfAdapter.sortabletable.js"></script>        

    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/arrow_switch.png" title="Issues" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Link Issue</span>
   	</div>                   

	<%
	if action = "search" then
	
		dim locSearch	
		dim locCRSID : locCRSID = Request.Form("locCRSID")
		dim locName : locName = Request.Form("locName")
		dim locAddress : locAddress = Request.Form("locAddress")		
		dim locCity : locCity = Request.Form("locCity")		
		dim locState : locState = Request.Form("locState")
		dim locZip : locZip = Request.Form("locZip")
		dim locFirstName : locFirstName = Request.Form("locFirstName")		
		dim locLastName : locLastName = Request.Form("locLastName")		
		dim locSummary : locSummary = Request.Form("locSummary")		

		if len(locCRSID) > 0 then locSearch = locSearch & "CRSID Like '%" & locCRSID & "%' AND "
		if len(locName) > 0 then locSearch = locSearch & "Location_Name Like '%" & locName & "%' AND "
		if len(locAddress) > 0 then locSearch = locSearch & "Location_Address Like '%" & locAddress & "%' AND "
		if len(locCity) > 0 then locSearch = locSearch & "Location_City Like '%" & locCity & "%' AND "
		if len(locState) > 0 then locSearch = locSearch & "Location_State Like '%" & locState & "%' AND "
		if len(locZip) > 0 then locSearch = locSearch & "Location_PostalCode Like '%" & locZip & "%' AND "
		if len(locFirstName) > 0 then locSearch = locSearch & "Caller_FirstName Like '%" & locFirstName & "%' AND "
		if len(locLastName) > 0 then locSearch = locSearch & "Caller_LastName Like '%" & locLastName & "%' AND "
		if len(locSummary) > 0 then locSearch = locSearch & "Summary Like '%" & locSummary & "%' AND "		

		'clean up
		if right(locSearch,5) = " AND " then locSearch = left(locSearch,len(locSearch)-5)
		
		'check how many records are returned
		mySQL = "SELECT count(IssueID) AS [count] " _			 
			  & "	FROM   CRS " _
			  & "	WHERE CustomerID = '" & customerID & "' "		
		if len(locSearch) > 0 then
			mySQL = mySQL & " AND " & locSearch		
		end if

	else
		'check how many records are returned
		mySQL = "SELECT count(IssueID) AS [count] " _			 
			  & "	FROM   CRS " _
			  & "	WHERE CustomerID = '" & customerID & "' "	
			  
	end if

	'open recordset
	set rs = openRSopenDialog(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)		
	count = rs("count")
	call closeRS(rs)
	%>

	<%
	'there are more then 1000 issues, STOP and rethink...
	if count>1000 then	
		%>		
		<script language="javascript">
			//close window
			SimpleModal.close();				
			//open login dialog
			SimpleModal.open('../_dialogs/popup_issue_search.asp?cid=<% =customerID %>&recid=<% =idIssue %>&loccrsid=<% =locCRSID %>&locname=<% =locname %>&locaddress=<% =locaddress %>&loccity=<% =loccity %>&locstate=<% =locstate %>&loczip=<% =loczip %>&locfirstname=<% =locfirstname %>&loclastname=<% =loclastname %>&locsummary=<% =locsummary %>&pageview=<% =pageView %>&count=<% =count %>', 525, 800, 'no');
		</script>    
		<%				
	else
	%>

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Select an issue by clicking within the 'Issue #' column.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Issues table -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
    
        <%
		mySQL = "SELECT CRSID AS [Issue #], [Date], Callback, Status, Caller_FirstName + ' ' + Caller_LastName AS [Reporter], " _
			  & "	LTRIM(CASE WHEN COALESCE(Location_Name,'') = '' THEN '' ELSE Location_Name + '<BR>' END " _
			  & "	+ CASE WHEN COALESCE(Location_Address,'') = '' THEN '' ELSE Location_Address + '<BR>' END " _
			  & "	+ CASE WHEN COALESCE(Location_City,'') = '' THEN '' ELSE Location_City END " _
			  & "	+ RTRIM(CASE WHEN COALESCE(Location_State,'') = '' THEN '' ELSE ' ' + Location_State + ' ' END " _
			  & "	+ CASE WHEN COALESCE(Location_PostalCode,'') = '' THEN '' ELSE ', ' + Location_PostalCode END) " _			  
			  & "	+ CASE WHEN COALESCE(Location_Country,'') = '' THEN '' ELSE '<BR>' + Location_Country END) AS [Location], " _
			  & "	Location_Level_1 AS [Level_1], Location_Level_2 AS [Level_2], Location_Level_3 AS [Level_3], Location_Level_4 AS [Level_4], " _
			  & "	Location_Level_5 AS [Level_5], Location_Level_6 AS [Level_6], Location_Level_7 AS [Level_7], Location_Level_8 AS [Level_8], " _
			  & "	Location_Level_1 AS [Hierarchy] " _
			  & "		FROM CRS " _
			  & "	WHERE (CustomerID = '" & customerID & "') "
			  
		'limit result set
		if action = "search" then			  
			if len(locSearch) > 0 then
				mySQL = mySQL & " AND " & locSearch		
			end if							  
		end if
		mySQL = mySQL & " ORDER BY CRSID "
		%>
        
        <tr>
        	<td>
			<%
			dim tableProperty
			dim pageLength 
			pageLength = 4

			closeRS(rs)				
			'----------------------------------------------------------------------------------------------
						
			'set _tablefilter/default.asp settings.
            tableProperty = "sort: true, sort_config:{sort_types:['String','String','String']}, filters_row_index: 1, " _
                          & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
                          & "paging: true, paging_length: " & pageLength & ", " _
						  & "highlight_keywords: true, " _
                          & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", " _
                          & "or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
                          & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"
						
			'make call to build grid found in ../_tablefilter/default.asp
			call buildGrid("issueTableID",mySQL,"","issue-popup","../_images/icons/16/building_link.png",tableProperty)			
        	%>
        	</td>
        </tr>
    </table>
	<!-- STOP Issues table -->

	<!-- START Cancel Button -->
	<div style="padding-top:5px; margin-bottom:0px;">
    	<% if pageView = "issue" or pageView = "issue:00" then %>
			<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); topWin.parent.StartTheTimer(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>
		<% else %>
			<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>
		<% end if %>        
    </div>
	<!-- STOP Cancel Button -->     

	<%
	end if
	%>    
         
</body>
</html>

<%
'close database connection
call closeDB()
%>


<script language="javascript">	
	// --NOT CURRENTLY USED--
	// --CAN DELETE IF NEEDED--
    // dependent on SimpleModal.js
    // note: topWin.parent is defined in SimpleModal.js and replaces "window.opener"
    function addIssue(sIssue,sDate,sCallback,sStatus) {
        //push selected location to parent Issue window
        topWin.parent.addLinkedIssue(sIssue,sDate,sCallback,sStatus,'Sub');
        //close window
        SimpleModal.close();
    }
</script>
