<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->

<%	
'Database
dim mySQL, cn, rs, rsFUP

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim recID
dim idIssue : idIssue = trim(Request.QueryString("recId"))


dim rptQuestionCount
if len(idIssue) > "" then
	mySQL = "SELECT Count(CRSQuestionID) AS QuestionCount " _
		  & "	FROM CRS_question INNER JOIN Customer ON CRS_question.CustomerID = Customer.CustomerID " _
		  & "	WHERE CRS_Question.CRSID = '" & idIssue & "' AND Customer.appShowIssueQuestions='Y' "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	rptQuestionCount = rs("QuestionCount")
	call closeRS(rs)
end if


dim Caller_Confidential 'TEMP --> should be field "Confidential", SQL would not save at time of demo
if len(idIssue) > "" then
	mySQL = "SELECT Caller_Confidential " _
		  & "	FROM CRS " _
		  & "	WHERE CRSID = '" & idIssue & "' "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	Caller_Confidential = rs("Caller_Confidential")
	call closeRS(rs)
end if



%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Print Issue</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/32/printer.png" title="Print" width="32" height="32" align="absmiddle"> 
       	<span class="popupTitle">Print: <% =idIssue %></span>
   	</div>                   

	<table class="formTable" style="width:100%;" border="0" cellspacing="0" cellpadding="0">
        
        <form name="frm" id="frm" method="post" action="../_dialogs/popup_issue_print_window.asp" target="_blank">        
        <input name="idIssue" type="hidden" value="<% =idIssue %>">        

		<%
		dim issueReport
		if Caller_Confidential="Y" and cLng(session(session("siteID") & "adminLoggedOn")) > 30 then 
			issueReport = "issue_redacted.rpt"
		else
			issueReport = "issue.rpt"		
		end if
		%>
        <input name="fileName" type="hidden" value="<% =issueReport %>">
        
		<tr>    
        
        	<!-- START Selection box -->
			<td align=left>
				<div class="subLabel" style="text-transform:uppercase; margin-bottom:5px;"><strong>Selection</strong></div>                   
				<div class="subLabel" style="width:175px; margin-bottom:6px;">Issue sections to include:</div> 
				<div id="field_list_container">
					<div id="field_list" style="width:175px; border: 1px solid #ccc; padding: 4px; height:160px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">

						<div id="field_list_item_Name" class="checkItem">
							<input name="list_checkBox" id="field_list_checkBox" type="checkbox" value="details" checked/>
							<label for="field_list_checkBox">Details</label>
						</div>
                        
                        
						<% if lcase(sCustomerID)="demo" or lcase(sCustomerID)="cha" then %>                        	
						<% 
						'only Admins have a choice
						if Caller_Confidential="Y" then 
							if cLng(session(session("siteID") & "adminLoggedOn")) <= 30 then
							%>
                            <div id="field_list_item_Name" class="checkItem">
                                <input name="list_checkBox" id="field_list_checkBox" type="checkbox" value="redacted"/>
                                <label for="field_list_checkBox">Details (Redacted)</label>
                            </div>                        
                        	<% 
							end if
						end if %>                                                
						<% end if %>                        
                        
                        
                        <% 
						if rptQuestionCount <= 0 then 
							'do nothing...
						else
						%>
                            <div id="field_list_item_Name" class="checkItem">
                                <input name="list_checkBox" id="field_list_checkBox" type="checkbox" value="questions"/>
                                <label for="field_list_checkBox">Questions</label>
                            </div>                        
                        <% end if %>
                        
						<div id="field_list_item_Name" class="checkItem">
							<input name="list_checkBox" id="field_list_checkBox" type="checkbox" value="subjects"/>
							<label for="field_list_checkBox">Subjects</label>
						</div>                                       
                        
                        
						<% if programAdmin = "Y" or uCase(viewCaseNotes) = "Y" then %>
                            <div id="field_list_item_Name" class="checkItem">
                                <input name="list_checkBox" id="field_list_checkBox" type="checkbox" value="casenotes"/>
                                <label for="field_list_checkBox">Case Notes</label>
                            </div>
                            
							<% 
							if uCase(viewInvestigations) = "N" then 
								'do nothing...
							else
							%>                         
                                <div id="field_list_item_Name" class="checkItem">
                                    <input name="list_checkBox" id="field_list_checkBox" type="checkbox" value="investigations"/>
                                    <label for="field_list_checkBox">Investigations</label>
                                </div>
                            <% end if %>

							<% 
							if uCase(viewDocuments) = "N" then 
								'do nothing...
							else
							%>                         
                                <div id="field_list_item_Name" class="checkItem">
                                    <input name="list_checkBox" id="field_list_checkBox" type="checkbox" value="documentation"/>
                                    <label for="field_list_checkBox">Documents</label>
                                </div>
                            <% end if %>

							<% 
							if uCase(viewCaseUserFields) = "N" then 
								'do nothing...
							else
							%>                         
                                <div id="field_list_item_Name" class="checkItem">
                                    <input name="list_checkBox" id="field_list_checkBox" type="checkbox" value="userfields"/>
                                    <label for="field_list_checkBox">User Fields</label>
                                </div>
                            <% end if %>

							<% 
							if uCase(editCaseResolution) = "N" then
								'do nothing...
							else
							%>                         
                                <div id="field_list_item_Name" class="checkItem">
                                    <input name="list_checkBox" id="field_list_checkBox" type="checkbox" value="resolutions"/>
                                    <label for="field_list_checkBox">Resolutions</label>
                                </div>
                            <% end if %>                            
                            
                        <% end if %>
                        
					</div>
				</div> 
               	<div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frm','field_list_checkBox','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frm','list_checkBox',''); return false;">None</a></div>
            
            </td>
        	<!-- STOP Selection box -->

        	<!-- START Export -->
			<td>
				<div class="subLabel" style="text-transform:uppercase; margin-bottom:5px;"><strong>File Type</strong></div>
                <div class="subLabel" style="margin-bottom:6px;">Print file as:</div> 
                <div style="margin-bottom:15px;">
                    <select name="type" id="type" size=1 class="inputMedium" onChange="javascript:formAction();">
                        <option value="PDF:issue" >Adobe PDF</option>
                        <option value="Word:issue" >MS Word</option>
                        <option value="RTF:issue" >Rich Text</option>
                        <option value="window" selected>Window</option>                        
                    </select>
				</div>

				<div class="subLabel" style="text-transform:uppercase; margin-bottom:5px;"><strong>Related Issues</strong></div>
                <div class="subLabel" style="margin-bottom:6px;">Issues to include:</div> 
                <div style="margin-bottom:15px;">
                    <select name="includeIssue" size=1 class="inputMedium">
                        <option value="no" selected>Only this issue</option>
                        <option value="yes">All related issues</option>
                    </select>
				</div>
                
            </td>
            <!-- START Export box -->
                                                
   	    </tr>
        </form>
    </table>        
                
   	<!-- START Buttons -->
	<div style="float:right; padding-top:10px;"> 	       		
		<a class="myCMbutton" href="#" accesskey="P" onClick="document.forms['frm'].submit(); SimpleModal.close();"><span class="print" style="padding-right:10px;"><u>P</u>rint</span></a>
        <a class="myCMbutton" href="#" accesskey="C" onClick="SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>
	</div>    
    <!-- STOP Buttons -->

</body>
</html>

<%
'close database connection
call closeDB()
%>

<script>
	function fieldCheckList(frm,obj,option) {
	  // Used to check or uncheck on checkboxes found on list boxes
	  // in Issue Lists and Report forms
		var elm = document[frm].elements[obj]; 
		for (var i=0; i <elm.length; i++) { 
			elm[i].checked = option
		} 
	}
	
	function formAction() {
	  // Set form action to appropriate page
	  // depending on what user wants to print
	  var i
	  var frm = document.getElementById('frm');
	  var el = document.getElementById('type');
	
	  for(i=el.length-1; i>=0; i--) {
	    if(el.options[i].selected) {
		  if(el.options[i].value=="window") {			  
			 frm.action = "../_dialogs/popup_issue_print_window.asp";			  
		  }
		  else {
			 frm.action = "../reports/crystal/execute.asp";
		  }
	    }
	  }
	  
	}

</script>



