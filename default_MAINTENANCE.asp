<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>myCM 3.0 : <% =pageTitle %></TITLE>

	<link rel="shortcut icon" href="../_images/favicon.ico" >

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />
    <link type="text/css" rel="stylesheet" href="../_css/tabs.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/sidePanel.css" />
    	
    <!--[if IE]>
    <style>
    fieldset.infoBox
    {
        border:clear;
        background:none;
    }
    
    fieldset legend
    {
        background-color:#FFFFFF;
        padding:0px 4px;
    }
    
    fieldset div
    {
        margin:4px 8px 8px 8px;
    }
    </style>
    <![endif]-->
    

	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- Tab (top/side) Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/tabs.js"></script>

	<!-- Form Validation Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/forms.js"></script>
    
	<!-- Session variables used in Javascript functions and default Values -->
	<script language="Javascript1.2">
		<!--
		var sessionLogid=<%= session(session("siteID") & "logid")%>; //this value is used in DHTML menu,specifically the "Profile" mentu item
		//-->
	</script> 

	<!-- This is used by the TinyMCE component WYSIWYG control -->
	<script type="text/javascript" src="../scripts/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
        
	<!-- MENU TOP PAGE Sothink DHTML menu http://www.sothink.com/ -->		
	<script type="text/javascript" src="../scripts/menu/stmenu.js"></script>
	<script type="text/javascript" src="../scripts/tree/stlib.js"></script>

	<!-- HTML Table Filter Generator http://tablefilter.free.fr/ -->
    <link rel="stylesheet" type="text/css" href="../_tablefilter/filtergrid.css"/>
	<script type="text/javascript" src="../_tablefilter/tablefilter_all_min.js"></script>
	<script type="text/javascript" src="../_tablefilter/sortabletable.js"></script>
    <script type="text/javascript" src="../_tablefilter/tfAdapter.sortabletable.js"></script>        

	<!-- DHTML Calendar http://www.dhtmlx.com -->
	<link rel="stylesheet" type="text/css" href="../_calendar/dhtmlxCalendar/codebase/dhtmlxcalendar.css">
    <link rel="stylesheet" type="text/css" href="../_calendar/dhtmlxCalendar/codebase/skins/dhtmlxcalendar_simplegrey.css">
	<script type="text/javascript" src="../_calendar/dhtmlxCalendar/codebase/dhtmlxcommon.js"></script>
	<script type="text/javascript" src="../_calendar/dhtmlxCalendar/codebase/dhtmlxcalendar.js"></script>

    <link rel="stylesheet" href="../_accordion/style.css" type="text/css" />
    <script type="text/javascript" src="../_accordion/accordion.js"></script>

	<!-- HTML List/Table resorting functions (right now own) -->
	<!-- Example usage:
    		var table = document.getElementById('table-1');
			var tableDnD = new TableDnD();
			tableDnD.init(table);  -->
	<script type="text/javascript" src="../scripts/javascript/orderList.js"></script>

	<!-- COOKIE CRUMB Navigation http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/cookieCrumbs.js"></script>

	<!-- JQUERY Libraries -->
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />
        
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/code-bank/simple-modal/ -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
    
    <!-- ONLY FOR suggest boxes and JSON data pulls, includes full JQuery library -->
    <style>
        .ui-autocomplete-loading { background: white url('../_jquery/suggestbox/ui-anim_basic_16x16.gif') right center no-repeat; }
    </style>
    <style>
        .ui-autocomplete {
            max-height: 100px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
            /* add padding to account for vertical scrollbar */
            padding-right: 20px;
        }
        /* IE 6 doesn't support max-height
         * we use height instead, but this forces the menu to always be this tall
         */
        * html .ui-autocomplete {height: 100px;}
    </style>
    <!-- ----------------------------------------- -->

	<!-- JGROWL Notifications http://stanlemon.net/projects/jgrowl.html -->
    <script src="../_jquery/jgrowl/jquery.jgrowl.js"></script>
    <link rel="stylesheet" type="text/css" href="../_jquery/jgrowl/jquery.jgrowl.css"/>

	<!-- jQuery Prompt Replacements http://abeautifulsite.net/blog/2008/12/jquery-alert-dialogs/ -->
    <script src="../_jquery/prompt/jquery.alerts.js"></script>
    <link rel="stylesheet" type="text/css" href="../_jquery/prompt/jquery.alerts.css"/>

	<!-- jQuery Watermark http://digitalbush.com/projects/watermark-input-plugin/ -->
    <script src="../_jquery/watermark/jquery.watermarkinput.js"></script>

	<!-- JQuery SPINNER https://github.com/btburnett3/jquery.ui.spinner/ -->
    <link type="text/css" rel="stylesheet" href="../_jquery/spinner/ui.spinner.css" />
	<script type="text/javascript" src="../_jquery/spinner/ui.spinner.js"></script>

	<!-- to prevent user from using BACK button. -->
	<script type="text/javascript">
        window.history.forward();
    </script>
    
</head>

<body>

<br><br><br><br><br>

<table border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td><img src="Maintenance.png"></td>
  </tr>
</table>

</body>
</html>
