<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>

<style type="text/css">
<!--
.AccordionTitle, .AccordionContent, .AccordionContainer
{
  position:relative;
  width:100%;
}

.AccordionTitle
{
  height:20px;
  overflow:hidden;
  cursor:pointer;
  color: #333333; 
  font-family: Verdana, Arial, helvetica; 
  font-size: 8pt;
  background-color:#f6f6f6;
  vertical-align:middle;
  text-align:left;
  border-top: solid 1px #BBBBBB;
  border-left: solid 1px #BBBBBB;  
  border-right: solid 1px #BBBBBB;  
  -moz-user-select:none;
}

.AccordionContent
{
  height:0px;
  overflow:auto;
  display:none; 
  border-top: solid 1px #BBBBBB;
  border-left: solid 1px #BBBBBB;  
  border-right: solid 1px #BBBBBB;    
}

.AccordionContainer
{
  border-bottom: solid 1px #BBBBBB;
}
-->
</style>



<script type="text/javascript">
var ContentHeight = 200;
var TimeToSlide = 250.0;

var openAccordion = '';

function runAccordion(index)
{
  var nID = "Accordion" + index + "Content";
  if(openAccordion == nID)
    nID = '';
    
  setTimeout("animate(" + new Date().getTime() + "," + TimeToSlide + ",'" 
      + openAccordion + "','" + nID + "')", 33);
  
  openAccordion = nID;
}

function animate(lastTick, timeLeft, closingId, openingId)
{  
  var curTick = new Date().getTime();
  var elapsedTicks = curTick - lastTick;
  
  var opening = (openingId == '') ? null : document.getElementById(openingId);
  var closing = (closingId == '') ? null : document.getElementById(closingId);
 
  if(timeLeft <= elapsedTicks)
  {
    if(opening != null)
      opening.style.height = ContentHeight + 'px';
    
    if(closing != null)
    {
      closing.style.display = 'none';
      closing.style.height = '0px';
    }
    return;
  }
 
  timeLeft -= elapsedTicks;
  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight);

  if(opening != null)
  {
    if(opening.style.display != 'block')
      opening.style.display = 'block';
    opening.style.height = (ContentHeight - newClosedHeight) + 'px';
  }
  
  if(closing != null)
    closing.style.height = newClosedHeight + 'px';

  setTimeout("animate(" + curTick + "," + timeLeft + ",'" 
      + closingId + "','" + openingId + "')", 33);
}

</script>


</head>

<body>

<div style="width:100%; background-color:#BBBBBB">hello...</div>

<div id="AccordionContainer" class="AccordionContainer">

  <div onclick="runAccordion(1);">
    <div class="AccordionTitle" onselectstart="return false;">
      Accordion 1
    </div>
  </div>
  <div id="Accordion1Content" class="AccordionContent">
    I Am Accordion 1.
  </div>

  <div onclick="runAccordion(2);">
    <div class="AccordionTitle" onselectstart="return false;">
      Accordion 2
    </div>
  </div>
  <div id="Accordion2Content" class="AccordionContent">
    I Am Accordion 2.
  </div>

  <div onclick="runAccordion(3);">
    <div class="AccordionTitle" onselectstart="return false;">
      Accordion 3
    </div>
  </div>
  <div id="Accordion3Content" class="AccordionContent">
    I Am Accordion 3.
  </div>

  <div onclick="runAccordion(4);">
    <div class="AccordionTitle" onselectstart="return false;">
      Accordion 4
    </div>
  </div>
  <div id="Accordion4Content" class="AccordionContent">
    I Am Accordion 4.
  </div>

  <div onclick="runAccordion(5);">
    <div class="AccordionTitle" onselectstart="return false;">
      Accordion 5
    </div>
  </div>
  <div id="Accordion5Content" class="AccordionContent">
    I Am Accordion 5.
  </div>

</div>
will it push me down.


</body>
</html>
