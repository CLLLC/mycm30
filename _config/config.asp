<%
'********************************************************************
' Product  : MyComplianceManagement, Pro, Lite, Metrics
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************

'---------------------------------------------------------------------
' URL of website after login...MUST end with forward slash "/"
'---------------------------------------------------------------------
dim siteURL : siteURL  = "https://www.mycompliancemanagement.com/"

'---------------------------------------------------------------------
' Directory on website.
'---------------------------------------------------------------------
dim sysDir  : sysDir  = "C:\inetpub\wwwroot\myCM"
dim dataDir : dataDir = "C:\inetpub\wwwroot\data"
dim mcrDir : mcrDir = "\\172.168.1.25\templates"
dim importDir : importDir = "\\172.168.1.24\data"

'---------------------------------------------------------------------
' Database Connection String.
'
' EXAMPLES : 
' 	MS Access 	= "driver={Microsoft Access Driver (*.mdb)};DBQ=C:\inetpub\wwwroot\data\myCM.mdb;"
'	SQL 2005	= "Provider=SQLNCLI;Server=100.100.10.209;Database=myCM;Uid=mycm4cci;Pwd=L3tmycm1n;"
'	SQL 2008	= "Provider=SQLNCLI10;Server=100.100.10.209;Database=myCM;Uid=sa;Pwd=0p3nup4m3W0n;"
'	All Version = "driver={SQL Server};server=100.100.10.209;uid=sa;pwd=0p3nup4m3W0n;database=myCM"
' 	ODBC 		= "dsn=NAME_OF_YOUR_DSN"
'---------------------------------------------------------------------
dim connString  : connString = "Provider=SQLNCLI10;Server=100.100.10.134;Database=myCM;Uid=sa;Pwd=0p3nup4m3W0n;"
dim connStringArchive  : connStringArchive = "Provider=SQLNCLI10;Server=100.100.10.134;Database=myCMarchive;Uid=sa;Pwd=0p3nup4m3W0n;"
dim connPBX  : connPBX = "Provider=SQLNCLI10;Server=100.100.10.10;Database=GDR;Uid=mycm;Pwd=l3tm31n;"

'---------------------------------------------------------------------
' Database Type (0=Access ; 1=SQL Server)
'---------------------------------------------------------------------
const dbType = 1

'---------------------------------------------------------------------
' Store ID. If you are going to be hosting multiple stores on the same 
' web (or virtual web), you should assign a different store ID to each 
' of them to prevent sessions from being shared. The storeID can be 
' any combination of alpha (a-z) and numeric (0-9) characters with a
' maximum of 10 characters (eg. "cisco"), and a minimum of 1
' character. No spaces are allowed.
'---------------------------------------------------------------------
dim siteID : siteID = "myCM" '''' "_" & Int((rnd*999))+1 '& cStr(cLng(rnd*999)+1)

%>


