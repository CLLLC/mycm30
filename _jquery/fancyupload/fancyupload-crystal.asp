<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement, Pro, Lite, Metrics
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../../_includes/_INCconfig_.asp"-->
<!--#include file="../../_includes/_INCappDBConn_.asp"-->
<!--#include file="../../_includes/_INCappFunctions_.asp"-->
<!--#include file="../../_includes/_INCsecurity_.asp"-->

<%
'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim customerID
customerID = Request.Querystring("cid")
if len(customerID) <= 0 then
	customerID = Request.Form("customerID")
end if

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title>Upload Documents</title>

	<link type="text/css" rel="stylesheet" href="../../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../../_css/buttons.css" />    

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../../scripts/javascript/simpleModal.js"></script>

	<!-- FANCY UPLOAD http://digitarald.de/project/fancyupload/ -->
	<script type="text/javascript" src="source/mootools.js"></script>
	<script type="text/javascript" src="source/Swiff.Uploader.js"></script>
	<script type="text/javascript" src="source/Fx.ProgressBar.js"></script>
	<script type="text/javascript" src="source/FancyUpload2.js"></script>

	<!-- See script.js -->
	<script type="text/javascript">
		//<![CDATA[

		 /**
		 * FancyUpload Showcase
		 *
		 * @license		MIT License
		 * @author		Harald Kirschner <mail [at] digitarald [dot] de>
		 * @copyright	Authors
		 */
		
		window.addEvent('domready', function() { // wait for the content
		
			// our uploader instance 
			
			var up = new FancyUpload2($('demo-status'), $('demo-list'), { // options object
				//single file upload only
			  	multiple: false,

				typeFilter: {
							'Crystal Report (*.rpt)': '*.rpt'
						},

				// we console.log infos, remove that in production!!
				verbose: false,
				
				// url is read from the form, so you just have to change one place
				url: $('form-demo').action,
				
				// path to the SWF file
				path: 'source/Swiff.Uploader.swf',
								
				// this is our browse button, *target* is overlayed with the Flash movie
				target: 'demo-browse',
				
				// graceful degradation, onLoad is only called if all went well with Flash
				onLoad: function() {
					$('demo-status').removeClass('hide'); // we show the actual UI
					$('demo-fallback').destroy(); // ... and hide the plain form
					
					// We relay the interactions with the overlayed flash to the link
					this.target.addEvents({
						click: function() {
							return false;
						},
						mouseenter: function() {
							this.addClass('hover');
						},
						mouseleave: function() {
							this.removeClass('hover');
							this.blur();
						},
						mousedown: function() {
							this.focus();
						}
					});
		
					// Interactions for the 2 other buttons					
					$('demo-clear').addEvent('click', function() {
						up.remove(); // remove all files
						return false;
					});
		
					$('demo-upload').addEvent('click', function() {
						up.start(); // start upload
						return false;
					});
				},
				
				// Edit the following lines, it is your custom event handling
				
				/**
				 * Is called when files were not added, "files" is an array of invalid File classes.
				 * 
				 * This example creates a list of error elements directly in the file list, which
				 * hide on click.
				 */ 
				onSelectFail: function(files) {
					files.each(function(file) {
						new Element('li', {
							'class': 'validation-error',
							html: file.validationErrorMessage || file.validationError,
							title: MooTools.lang.get('FancyUpload', 'removeTitle'),
							events: {
								click: function() {
									this.destroy();
								}
							}
						}).inject(this.list, 'top');
					}, this);
				},
				
				/**
				 * This one was directly in FancyUpload2 before, the event makes it
				 * easier for you, to add your own response handling (you probably want
				 * to send something else than JSON or different items).
				 */
				onFileSuccess: function(file, response) {
					var json = new Hash(JSON.decode(response, true) || {});
					
					if (json.get('status') == '1') {
						file.element.addClass('file-success');
						
						//file.info.set('html', '<strong>File was uploaded:</strong> ' + json.get('name') );
						file.info.set('html', 'File was uploaded successfully');

						//put file on Issue table
						topWin.parent.addDocUploaded(json.get('name'),json.get('date'),json.get('size'),json.get('extension'))
						
					} else {
						file.element.addClass('file-failed');
						file.info.set('html', '<strong>An error occured:</strong> ' + (json.get('error') ? (json.get('error') + ' #' + json.get('code')) : response));
					}
				},
				
				/**
				 * onFail is called when the Flash movie got bashed by some browser plugin
				 * like Adblock or Flashblock.
				 */
				onFail: function(error) {
					switch (error) {
						case 'hidden': // works after enabling the movie and clicking refresh
							alert('To enable the embedded uploader, unblock it in your browser and refresh (see Adblock).');
							break;
						case 'blocked': // This no *full* fail, it works after the user clicks the button
							alert('To enable the embedded uploader, enable the blocked Flash movie (see Flashblock).');
							break;
						case 'empty': // Oh oh, wrong path
							alert('A required file was not found, please be patient and we fix this.');
							break;
						case 'flash': // no flash 9+ :(
							alert('To enable the embedded uploader, install the latest Adobe Flash plugin.')
					}
				}
				
			});
			
		});
		//]]>
	</script>


	<!-- See style.css -->
	<style type="text/css">
				/**
		 * FancyUpload Showcase
		 *
		 * @license		MIT License
		 * @author		Harald Kirschner <mail [at] digitarald [dot] de>
		 * @copyright	Authors
		 */
		
		/* CSS vs. Adblock tabs */
		.swiff-uploader-box a {
			display: none !important;
		}
		
		/* .hover simulates the flash interactions */
		a:hover, a.hover {
			/* BR removed: color: red; */
		}
		
		#demo-status {
			padding-top: 10px; 
			padding-left: 15px;
			height: 100px;
			width: 100%;
			border: 1px solid #CCCCCC;
		}
		
		#demo-status .progress {
			background: url(assets/progress-bar/progress.gif) no-repeat;
			background-position: +50% 0;
			margin-right: 0.5em;
			vertical-align: middle;
		}
		
		#demo-status .progress-text {
			font-size: 0.9em;
			font-weight: bold;
		}
		
		#demo-list { 
			list-style: none;
			width: 100%;
			padding-left: 6px; 	/*removes indent in Firefox/Mozilla */
			padding-top: 6px;
			margin: 0px;		/*removes indent in IE */
		}
				
		#demo-list li.validation-error {
			padding-left: 44px;
			display: block;
			clear: left;
			line-height: 40px;
			color: #8a1f11;
			cursor: pointer;
			border-bottom: 1px solid #fbc2c4;
			background: #fbe3e4 url(assets/failed3.png) no-repeat 4px 4px;
		}
		
		#demo-list li.file {
			border-bottom: 1px solid #CCCCCC;
			background: url(assets/file3.png) no-repeat 4px 4px;
			overflow: auto;
		}
		#demo-list li.file.file-uploading {
			background-image: url(assets/uploading3.png);
			background-color: #D9DDE9;
		}
		#demo-list li.file.file-success {
			background-image: url(assets/success4.png);
		}
		#demo-list li.file.file-failed {
			background-image: url(assets/failed3.png);
		}
		
		#demo-list li.file .file-name {
			font-size: 1.1em;
			margin-left: 44px;
			display: block;
			clear: left;
			/* BR took out: line-height: 40px; */
			/* BR took out: height: 40px; */
			font-weight: bold;
		}
		#demo-list li.file .file-size {
			font-size: 0.9em;
			line-height: 18px;
			float: right;
			margin-top: 2px;
			margin-right: 6px;
		}
		#demo-list li.file .file-info {
			display: block;
			margin-left: 44px;
			font-size: 0.9em;			
			/* BR took out: line-height: 20px; */
			clear
		}
		#demo-list li.file .file-remove {
			clear: right;
			float: right;
			line-height: 18px;
			margin-right: 6px;
		}	
	</style>

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../../_images/icons/32/file_extension_rpt.png" title="Issues" width="32" height="32" align="absmiddle">
       	<span class="popupTitle">Crystal Report</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px; margin-bottom: 5px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Upload Crystal Report using form below.
                        </div>
        		</fieldset>                                    
        	</td>
      	</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Upload box -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
    		<td style="padding-right:18px;">
       
                <div class="container">
            
                    <div>
                    
                        <form id="form-demo" action="../../_upload/upload.asp?pageView=crystal&cid=<% =customerID %>" method="post" enctype="multipart/form-data" style="margin: 0px; padding: 0px;">
            
                            <fieldset id="demo-fallback" style="display:none;">
                                <legend>File Upload</legend>
                                <p>
                                    This form is just an example fallback for the unobtrusive behaviour of FancyUpload.
                                    If this part is not changed, something must be wrong with your code.
                                </p>
                                <label for="demo-photoupload">
                                    Upload a file:
                                    <input type="file" name="Filedata" />
                                </label>
                            </fieldset>				
                            
                            <div id="demo-status" class="hide">
                            
                                <!-- START Buttons -->
                                <!-- note: on the browse button had to remove href="#" because in FF it made you click twice before firing browse -->
                                <a id="demo-browse" class="myCMbutton" onClick="this.blur();"><span class="folder">Browse Files</span></a>
                                <a id="demo-clear"  href="#" class="myCMbutton" onClick="this.blur();"><span class="clearlist">Clear List</span></a>
                                <a id="demo-upload" href="#" class="myCMbutton" onClick="this.blur();"><span class="start">Start Upload</span></a>
                                <a id="demo-close"  href="#" class="myCMbutton" onClick="this.blur(); SimpleModal.close();"><span class="cancel">Close</span></a><br /><br />
                                <!-- STOP Buttons --> 
                                
                                <div>
                                    <strong class="overall-title"></strong><br />
                                    <img src="assets/progress-bar/bar.gif" class="progress overall-progress" />
                                </div>
                                <div>
                                    <strong class="current-title"></strong><br />
                                    <img src="assets/progress-bar/bar.gif" class="progress current-progress" />
                                </div>
                                <div class="current-text"></div>
                            </div>
                        
                            <ul id="demo-list"></ul>
            
                        </form>		
                        
                    </div>
            
                </div>

        	</td>
      	</tr>

	</table>
	<!-- STOP Upload box -->
    
</body>
</html>

<%
'close database connection
call closeDB()
%>










