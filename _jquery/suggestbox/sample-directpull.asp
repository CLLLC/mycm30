<%@ Language=VBScript %>
<!-- 
	FOR HELP SEE the following sites: 
		http://code.google.com/p/aspjson/ 
    	http://jqueryui.com/demos/autocomplete/#multiple (source code)
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Suggestion Box Sample</title>

    <link type="text/css" rel="stylesheet" href="../jquery-ui-1.8.11.custom.css" />

	<style type="text/css">
	<!--
	BODY, B, TD, P {COLOR: #333333; FONT-FAMILY: Verdana, Arial, helvetica; FONT-SIZE: 8pt;}
	-->
	</style>
        
	<style>
		.ui-autocomplete-loading { background: white url('ui-anim_basic_16x16.gif') right center no-repeat; }
	</style>
    
	<style>
		.ui-autocomplete {
			max-height: 100px;
			overflow-y: auto;
			/* prevent horizontal scrollbar */
			overflow-x: hidden;
			/* add padding to account for vertical scrollbar */
			padding-right: 20px;
		}
		/* IE 6 doesn't support max-height
		 * we use height instead, but this forces the menu to always be this tall
		 */
		* html .ui-autocomplete {height: 100px;}
	</style>

	<script language="JavaScript" src="../jquery-1.5.1.min.js"></script>
	<script language="JavaScript" src="../jquery-ui-1.8.11.custom.min.js"></script>
    
</head>

<body>

<script>	
//	var jqxhr = $.getJSON("json-data.asp?cid=demo&view=customer&field=customerid&term=demo", function(data) {alert(data);});    

//	var jqxhr = $.getJSON("json-data.asp?cid=cra&view=directive&field=directiveid&term=302373", function(data) {alert(data);});    

	///var jqxhr = $.getJSON("json-data.asp?cid=cra&view=directive&field=directiveid&term=300000", function(data) {ok(data);} );    
	
	function ok(result){
		document.getElementById('locDirective').innerHTML = result;
	}


	$.ajax({
	  url: "json-data.asp?cid=cra&view=directive&field=directiveid&term=300000",
	  cache: false,
	  dataType: "json",
	  success: function(data) {
		ok(data);
	}});



</script>

<div id="locDirective">here is directive</div>

</body>
</html>







