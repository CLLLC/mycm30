<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement, Pro, Lite, Metrics
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true

%>

<!--#include file="../../_includes/_INCconfig_.asp"-->
<!--#include file="../../_includes/_INCappFunctions_.asp"-->
<!--#include file="../../_includes/_INCsecurity_.asp"-->
<!--#include file="../../_includes/_INCappDBConn_.asp"-->

<%
'Database
dim mySQL, mySQL_Temp, cn, rs

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate security access
if loadUser(null) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim msg

dim queryCustID, queryView, queryField, recordType, arrayValue, arrayIndex

'used to pull profile/account specific information -- optional
queryCustID = trim(Request.QueryString("cid"))
'prepare for usage with "IN" operator 
if len(queryCustID) > 0 then
	queryCustID = "'" & queryCustID & "'"
	queryCustID = replace(queryCustID,"''","'")
end if

queryView = trim(Request.QueryString("view"))
if len(queryView) = 0 then
	response.write("[""""]")
	response.end
end if

queryField = trim(Request.QueryString("field"))
if len(queryField) = 0 then
	response.write("[""""]")
	response.end
end if

dim term
term = trim(Request.QueryString("term"))
if len(term) = 0 then
	term = ""
end if

dim output, col

	'----------------------------------------------------------------------
	'ALL possible data to query and provide in JSON format for query lookup
	'----------------------------------------------------------------------

	'TOOK OUT...CAUSE NOW MONTH IS 2011:1, QUARTER IS 2011:4
	   'lCase(queryField) <> "month" and _
	   'lCase(queryField) <> "quarter" and _
	   'lCase(queryField) <> "year" and _	
		
	'query filed in recordset
	if lCase(queryField) <> "status" and _
	   lCase(queryField) <> "inv status" and _
	   lCase(queryField) <> "issue type" and _
	   lCase(queryField) <> "postalcode" and _
	   lCase(queryField) <> "directiveid" and _
	   lCase(queryField) <> "configvallong" and _	   
	   lCase(queryField) <> "paragraphid" and _	   
	   lCase(queryField) <> "templateid" and _	   	   	   
	   lCase(queryField) <> "customername" and _
	   lCase(queryField) <> "callback" and _	   	   
	   lCase(queryField) <> "resolutions #" and _	   	   
	   lCase(queryField) <> "resolutions approved #" and _	   	   
	   lCase(queryField) <> "country" then

		recordType = "recordset"
		term = replace(term,"*","%")
		
		'CCI staff only..
		if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
			'looking at ALL customer profiles
			if len(queryCustID) = 0 then
				mySQL = "SELECT [" & queryField & "] " _
					  & "	FROM " & queryView & " " _
					  & "	WHERE [" & queryField & "] > '' AND [" & queryField & "] like '%" & term & "%' " _
					  & "	GROUP BY [" & queryField & "] " _
					  & "	ORDER BY [" & queryField & "] "
					  
			'looking at a particular profile
			else
				mySQL = "SELECT [" & queryField & "] " _
					  & "	FROM " & queryView & " " _
					  & "	WHERE CustomerID IN (" & queryCustID & ") AND [" & queryField & "] > '' AND [" & queryField & "] like '%" & term & "%' " _
					  & "	GROUP BY [" & queryField & "] " _
					  & "	ORDER BY [" & queryField & "] "
			
			end if
						
		'ALL customers NOT CCI staff
		else
			mySQL = "SELECT [" & queryField & "] " _
				  & "	FROM " & queryView & " " _
				  & "	WHERE CustomerID IN (" & profileIDs & ") AND [" & queryField & "] > '' AND [" & queryField & "] like '%" & term & "%' " _
				  & "	GROUP BY [" & queryField & "] " _
				  & "	ORDER BY [" & queryField & "] "
	
			if programAdmin = "N" then
				if lCase(queryView) <> "logins" and lCase(queryField) <> "email" then					
					
					'turned OFF 1-23-2013
					'mySQL = insertWhereClause(mySQL,"LOGID=" & sLogid)
					
					'added so filter would pull all available options from CRS
					mySQL_Temp =  " ( (EXISTS (SELECT CRS_Logins.CRSID " _
								  & "        FROM CRS_Logins " _
								  & "	     WHERE " & queryView & ".[Issue #]=CRS_Logins.CRSID AND " & queryView & ".CustomerID=CRS_Logins.CustomerID AND CRS_Logins.LOGID=" & sLogid & ")) " _
								  & "	     OR (" & queryView & ".LOGID = " & sLogid & ") " _
								  & "	  ) "
								  
					'add filter to limit only to what user can see
					mySQL = insertWhereClause(mySQL, mySQL_Temp)
					
				end if
			end if

		end if
		
	end if

	'ADD to WHERE for user search
	'searching logins...only active users returned
	if lCase(queryView) = "logins" and lCase(queryField) = "email" then
		mySQL = insertWhereClause(mySQL,"Active<>'N'")
	end if


	'issue status
	if lCase(queryField) = "status" then
		recordType = "array"		

		'CCI staff only..
		if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
			'looking at ALL customer profiles
			if len(queryCustID) = 0 then
				mySQL = "SELECT pCaseStatus FROM Customer WHERE "
			'looking at a particular profile
			else
				mySQL = "SELECT pCaseStatus FROM Customer WHERE  CustomerID IN (" & queryCustID & ") "
			end if
						
		'ALL customers NOT CCI staff
		else
			mySQL = "SELECT pCaseStatus FROM Customer WHERE  CustomerID IN (" & profileIDs & ") "
		end if

'		ORIGINAL				
'		mySQL = "SELECT pCaseStatus " _
'			  & "	FROM   Customer " _
'			  & "	WHERE  CustomerID IN (" & profileIDs & ") "
			  			  			  
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		if not rs.eof then
			arrayValue = rs("pCaseStatus")
		else
			arrayValue = ""
		end if
		call closeRS(rs)
		arrayValue = Split(arrayValue,vbCrLf)				

	'investigation status
	elseif lCase(queryField) = "inv status" then
		recordType = "array"
				
		'CCI staff only..
		if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
			'looking at ALL customer profiles
			if len(queryCustID) = 0 then
				mySQL = "SELECT pInvestigationStatus FROM Customer WHERE "
			'looking at a particular profile
			else
				mySQL = "SELECT pInvestigationStatus FROM Customer WHERE  CustomerID IN (" & queryCustID & ") "
			end if
						
		'ALL customers NOT CCI staff
		else
			mySQL = "SELECT pInvestigationStatus FROM Customer WHERE CustomerID IN (" & profileIDs & ") "
		end if

'		ORIGINAL		
'		mySQL = "SELECT pInvestigationStatus " _
'			  & "	FROM   Customer " _
'			  & "	WHERE  CustomerID IN (" & profileIDs & ") "
			  
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		if not rs.eof then
			arrayValue = rs("pInvestigationStatus")
		else
			arrayValue = ""
		end if
		call closeRS(rs)
		arrayValue = Split(arrayValue,vbCrLf)				

	'issue type
	elseif lCase(queryField) = "issue type" then	
		recordType = "recordset"
			
		'CCI staff only..
		if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
			'looking at ALL customer profiles
			if len(queryCustID) = 0 then
				mySQL = "SELECT IssueType.Name " _
					  & "	FROM IssueType INNER JOIN Customer_IssueType ON IssueType.IssueTypeID = Customer_IssueType.IssueTypeID " _
					  & "	WHERE IssueType.Name > '' AND IssueType.Name like '%" & term & "%' " _
					  & "	GROUP BY IssueType.Name " _
					  & "	ORDER BY IssueType.Name "
				
			'looking at a particular profile
			else
				mySQL = "SELECT IssueType.Name " _
					  & "	FROM IssueType INNER JOIN Customer_IssueType ON IssueType.IssueTypeID = Customer_IssueType.IssueTypeID " _
					  & "	WHERE Customer_IssueType.CustomerID IN (" & queryCustID & ") AND IssueType.Name > '' AND IssueType.Name like '%" & term & "%' " _
					  & "	GROUP BY IssueType.Name " _
					  & "	ORDER BY IssueType.Name "
				
			end if
						
		'ALL customers NOT CCI staff
		else
			mySQL = "SELECT IssueType.Name " _
				  & "	FROM IssueType INNER JOIN Customer_IssueType ON IssueType.IssueTypeID = Customer_IssueType.IssueTypeID " _
				  & "	WHERE Customer_IssueType.CustomerID IN (" & profileIDs & ") AND IssueType.Name > '' AND IssueType.Name like '%" & term & "%' " _
				  & "	GROUP BY IssueType.Name " _
				  & "	ORDER BY IssueType.Name "

		end if
					
'		ORIGINAL		
'		mySQL = "SELECT IssueType.Name " _
'			  & "	FROM IssueType INNER JOIN Customer_IssueType ON IssueType.IssueTypeID = Customer_IssueType.IssueTypeID " _
'			  & "	WHERE Customer_IssueType.CustomerID IN (" & profileIDs & ") AND IssueType.Name > '' AND IssueType.Name like '%" & term & "%' " _
'			  & "	GROUP BY IssueType.Name " _
'			  & "	ORDER BY IssueType.Name "
			  	
	'build pre-populated list of options
	elseif lCase(queryField) = "month STOP" then	
		recordType = "array"
		arrayValue = "1,2,3,4,5,6,7,8,9,10,11,12"
		arrayValue = Split(arrayValue,",")				

	'quarter
	elseif lCase(queryField) = "quarter STOP" then	
		recordType = "array"
		arrayValue = "1,2,3,4"
		arrayValue = Split(arrayValue,",")	

	'year
	elseif lCase(queryField) = "year STOP" then	
		recordType = "array"
		arrayValue = "2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015"
		arrayValue = Split(arrayValue,",")				

	elseif lCase(queryField) = "country" then	
		recordType = "recordset"
		if len(queryCustID) = 0 then
			mySQL = "SELECT WorldMap.Country " _
				  & "	FROM WorldMap " _
				  & "	GROUP BY WorldMap.Country " _
				  & "	HAVING WorldMap.Country like '%" & term & "%' " _
				  & "	ORDER BY WorldMap.Country "
					  
		'looking at a particular profile
		else
			mySQL = "SELECT Location.Country " _
				  & "	FROM Location " _
				  & "	WHERE Location.CustomerID IN (" & queryCustID & ") " _
				  & "	GROUP BY Location.Country " _
				  & "	HAVING Location.Country like '%" & term & "%' " _
				  & "	ORDER BY Location.Country "		
				  
		end if

	elseif lCase(queryField) = "customerid" then	
		recordType = "recordset"
		mySQL = "SELECT Customer.CustomerID + ': ' + Customer.Name AS [AlreadyUsed] " _
			  & "	FROM Customer " _
			  & "	WHERE Customer.CustomerID like '%" & term & "%' " _
			  & "	ORDER BY Customer.CustomerID "

	elseif lCase(queryField) = "customername" then	
		recordType = "recordset"
		mySQL = "SELECT Name " _
			  & "	FROM Customer " _
			  & "	WHERE Customer.CustomerID = '" & term & "' "

	elseif lCase(queryField) = "directiveid" then	
		recordType = "recordset"
		mySQL = "SELECT Notes " _
			  & "	FROM Directive " _
			  & "	WHERE DirectiveID = " & term & " "

	elseif lCase(queryField) = "configvallong" then	
		recordType = "recordset"
		mySQL = "SELECT configValLong " _
			  & "	FROM Config " _
			  & "	WHERE ConfigVar = '" & term & "' "

	elseif lCase(queryField) = "paragraphid" then	
		recordType = "recordset"
		mySQL = "SELECT Paragraph " _
			  & "	FROM Paragraph " _
			  & "	WHERE ParagraphID = " & term & " "

	elseif lCase(queryField) = "templateid" then	
		recordType = "recordset"
		mySQL = "SELECT Template " _
			  & "	FROM Template " _
			  & "	WHERE TemplateID = " & term & " "

	elseif lCase(queryField) = "callback" then	
		recordType = "array"
		'get currently assigned Severity Level
		mySQL = "SELECT Severity " _
			  & "	FROM   CRS " _
			  & "	WHERE  CRSID='" & term & "' "
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		if not rs.eof then
			'MIGHT NEED TO ADD... Min(Category.Sev" & rs("Severity") & "Weekends) AS weekEnds ...LATER FOR DAYOFWEEK CALC.
			'USE THIS.. DateAddW(Date(),rs("minSev")) .. if you do
			'get severity level push out date
			mySQL = "SELECT Min(Category.Sev" & rs("Severity") & ") AS minSev " _
				  & "	FROM CRS_Category INNER JOIN Category ON CRS_Category.CategoryID = Category.CategoryID " _
				  & "	WHERE CRS_Category.CRSID='" & term & "' "
			set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
			if not rs.eof then
				if cLng(rs("minSev")) > 0 then
					arrayValue = dateAdd("d",rs("minSev"),Date())
				else
					arrayValue = Date()
				end if
			else
				arrayValue = ""
			end if
		else
			arrayValue = ""
		end if
		call closeRS(rs)
		arrayValue = Split(arrayValue,vbCrLf)				

	'THIS WORKS IT'S JUST KINDA SLOW...NEED TO WORK ON IT SOME MORE
	'WOULD BE GOOD TO HAVE A ZIP CODE POPUP LOOK UP WINDOW FOR THE RA
	'TO SEARCH WHEN SOMEONE IDENTIFIES THEMSELVES.
	elseif lCase(queryField) = "postalcode" then	
		recordType = "recordset"
		mySQL = "SELECT TOP 25 CAST(WorldMap.PostalCode AS NVARCHAR(5)) + ': ' + WorldMap.City AS [WorldLookUp] " _
			  & "	FROM WorldMap " _
			  & "	WHERE PostalCode like '%" & term & "%' " _
			  & "	GROUP BY CAST(WorldMap.PostalCode AS NVARCHAR(5)) + ': ' + WorldMap.City " _			  
			  & "	ORDER BY CAST(WorldMap.PostalCode AS NVARCHAR(5)) + ': ' + WorldMap.City "
	
	end if
	
	
	'--------------------------------------------------------------
	'put it all together in properly formated JSON data: 
	'	example: ("[""Blue"",""Hello"",""Hello Again"",""Red""]")
	'--------------------------------------------------------------
		
	'use recordset to build output
	if recordType = "recordset" then
		dim tempOutput
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		'too many results...inform user
		if rs.recordcount > 500 then
			output = "-- too many results returned (" & rs.recordcount & ") --" & ""","""		
		'no records found
		elseif rs.eof then
			output = "-- no matches found --" & ""","""		
		'alls good-to-go
		else
			while not (rs.eof Or rs.bof)
				for each col In rs.fields
					tempOutput = col.value
					tempOutput = replace(tempOutput,vbCrLf,"")	'remove carriage returns or IE throws FATAL error!
					tempOutput = replace(tempOutput,"""","\""")	'will NOT work if double quotes are not appended with "\"
					tempOutput = replace(tempOutput," \"," \")	'try to find any backward slash inside of text
					output = output & tempOutput & ""","""
				next
				rs.movenext
			wend
		end if		
		if len(output) > 0 then
			output = "[""" & left(output,len(output)-2) & "]"		
		else
			output = ""		
		end if
		call closeRS(rs)
				
	'user array to build output
	elseif recordType = "array" then
		if isArray(arrayValue) then	
			for arrayIndex = 0 to UBound(arrayValue)
				output = output & arrayValue(arrayIndex) & ""","""
			next
			output = "[""" & left(output,len(output)-2) & "]"
		else
			output = "[""" & arrayValue & """]"
		end if		
	
	end if
			
	'put on page for data retrieval
	response.write(output)

	'close databas connection
	call closeDB()	
%>
