<%@ Language=VBScript %>
<!-- 
	FOR HELP SEE the following sites: 
		http://code.google.com/p/aspjson/ 
    	http://jqueryui.com/demos/autocomplete/#multiple (source code)
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Suggestion Box Sample</title>

    <link type="text/css" rel="stylesheet" href="../jquery-ui-1.8.11.custom.css" />

	<style type="text/css">
	<!--
	BODY, B, TD, P {COLOR: #333333; FONT-FAMILY: Verdana, Arial, helvetica; FONT-SIZE: 8pt;}
	-->
	</style>
        
	<style>
		.ui-autocomplete-loading { background: white url('ui-anim_basic_16x16.gif') right center no-repeat; }
	</style>
    
	<style>
		.ui-autocomplete {
			max-height: 100px;
			overflow-y: auto;
			/* prevent horizontal scrollbar */
			overflow-x: hidden;
			/* add padding to account for vertical scrollbar */
			padding-right: 20px;
		}
		/* IE 6 doesn't support max-height
		 * we use height instead, but this forces the menu to always be this tall
		 */
		* html .ui-autocomplete {height: 100px;}
	</style>

	<script language="JavaScript" src="../jquery-1.5.1.min.js"></script>
	<script language="JavaScript" src="../jquery-ui-1.8.11.custom.min.js"></script>
    
</head>

<body>

  <!-- PROGRESS BAR -->
  <div id="container"></div>
  <script>
	$(function() {	
	  //call progress bar constructor
	  $("#container").progressbar();
	});  
	
	//call progress bar constructor
	$("#container").progressbar({ value: 50 });
  </script>
  

   
	<input id="mybox" class="ui-autocomplete-input" />
	<!-- Simple suggestion with no multiple responses -->
	<script>
		$("#mybox").autocomplete({source: "json-data.asp?view=vwLocationsSum_DEMO&field=division", minLength: 2, highlight: true });	
	</script>

	<br>       

    <input id="mybox2" size="50" />
	<!-- Comples suggestion with multiple responses --> 
	<script>
	$(function() {
		function split( val ) {
			return val.split( /,\s*/ );
		}
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#mybox2" )
			// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
						$( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				source: function( request, response ) {
					$.getJSON( "json-data.asp?view=vwLocationsSum_DEMO&field=division", {
						term: extractLast( request.term )
					}, response );
				},
				search: function() {
					// custom minLength
					var term = extractLast( this.value );
					if ( term.length < 2 ) {
						return false;
					}
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( ", " );
					return false;
				}
			});
	});
	</script>

</body>
</html>







