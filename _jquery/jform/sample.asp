<%@ Language=VBScript %>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Form Submit Sample</title>

	<link type="text/css" rel="stylesheet" href="../../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../../_css/buttons.css" />    
    <link type="text/css" rel="stylesheet" href="../../_css/statusMessage.css" />
    <link type="text/css" rel="stylesheet" href="../../_css/tabs.css" />    
    <link type="text/css" rel="stylesheet" href="../../_css/sidePanel.css" />
	<link type="text/css" rel="stylesheet" href="../../_css/icons.css" />    
            
	<script language="JavaScript" src="../jquery-1.5.1.min.js"></script>
	<script language="JavaScript" src="../jquery-ui-1.8.11.custom.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../jquery-ui-1.8.11.custom.css" />
    
    <script src="../../_jquery/prompt/jquery.alerts.js"></script>
    <link rel="stylesheet" type="text/css" href="../../_jquery/prompt/jquery.alerts.css"/>

    <script src="../../_jquery/jgrowl/jquery.jgrowl.js"></script>
    <link rel="stylesheet" type="text/css" href="../../_jquery/jgrowl/jquery.jgrowl.css"/>

	<!-- This is used by the TinyMCE component WYSIWYG control -->
	<script type="text/javascript" src="../../scripts/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>

	<script type="text/javascript" src="jquery.form.js"></script>  
    <script type="text/javascript"> 
        // wait for the DOM to be loaded 
//        $(document).ready(function() { 
            // bind 'frm' and provide a simple callback function 
//            $('#frm').ajaxForm(function() { 
//                alert("Thank you for your comment!"); 
//            }); 
//        }); 

		$(document).ready(function() { 
			var options = { 
				success: showResponse  // post-submit callback 
			}; 		 
			// bind form using 'ajaxForm' 
			$('#frm').ajaxForm(options); 
		}); 
		 
		// post-submit callback 
		function showResponse(responseText, statusText, xhr, $form)  { 		 
			// alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + '\n\nThe output div should have already been updated with the responseText.');

			//jAlert(statusText, 'myCM Alert');
			

			if (responseText.indexOf("Error:") > -1) {
				document.getElementById('systemMessage').className = 'statusMessageERROR';
			}
			else {
				document.getElementById('systemMessage').className = 'statusMessageOK';	
			}
			document.getElementById('systemMessageTxt').innerHTML = responseText;
			
			document.getElementById('systemMessage').style.display = '';


			//GOOD...WORKS!!!
			$('#systemAlerts').jGrowl( responseText , {header: 'System Alert', sticky: false } );

		} 
		
		//for sending submit from <a> tag
		function autoPostForm() { 
			tinyMCE.triggerSave();
			$("#frm").submit();
        } 				

    </script>    


	<style type="text/css">
	<!--	
	.saveButton {
		color: #444;
		text-align:left;
		border: none;
		background: transparent url('../../_images/myCM_save.jpg') no-repeat;
		background-position: top left;		
		font-family: Verdana, Geneva, sans-serif;
		font-size:11px;
		font-style:normal;	
		height: 22px;
		margin-right: 6px;
		text-decoration: none;
		line-height: 12px;
		padding: 5px 0 5px 23px;
		width: 104px;
		cursor: pointer;
		}
	
	.saveButtonFocus {
		color: #444;
		text-align:left;
		border: none;
		background: transparent url('../../_images/myCM_save.jpg') no-repeat;
		background-position: bottom left;		
		font-family: Verdana, Geneva, sans-serif;
		font-size:11px;
		font-style:normal;	
		height: 22px;
		margin-right: 6px;
		text-decoration: none;
		line-height: 12px;		
	    padding: 6px 0 4px 23px; /* push text down 1px */		
		width: 104px;
		cursor: pointer;
		}

	button::-moz-focus-inner { 
		/* FF padding fix */
		border: 0;
		padding: 0;
	}




	body {
		margin-left: 25px;
	}
	-->
	</style>


    
</head>

<%
'Determin TinyMCE to Initialize...will append Investigation TextAreas also
dim activeElements : activeElements = "rptSummary,rptDetails"
%>

<body>

    <div id="systemAlerts" class="bottom-right"></div>


		<div id="systemMessage" class="statusMessageOK" style="display:none;">
			<div class="innerBorder">
		    	<div class="pad">
		    		<div class="icon" style="float:left;"></div>
		        	<div style="margin-left:24px; margin-bottom:0px; text-align:left; height:100%;">
		            	<span class="title">Notice</span><br><span id="systemMessageTxt" class="description">message...</span>
		            </div>
		    	</div>
			</div>
		</div>  


    <form name="frm" id="frm" action="submit.asp" method="post"> 
    
        Summary: <textarea name="rptSummary" id="rptSummary"></textarea> 		<br/><br/>           
        Details: <textarea name="rptDetails" id="rptDetails"></textarea> 		<br/><br/>           
        Addendum: <textarea name="rptAddendum" id="rptAddendum"></textarea>       <br/><br/>           
                
		<br/><br/>            

		<input name="FILE1" class="inputLong" size="30" type="file">

		<br/><br/>            

		<div style="float:left;">        
		  	<button type="submit" accesskey="S" class="saveButton" onFocus="this.className='saveButtonFocus';" onBlur="this.className='saveButton';" onClick="this.blur(); resetSystemMsg('systemMessage');">Save Button</button>
		</div>
        <div>
        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); navigateAway('../default.asp'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>
        </div>

		<a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); resetSystemMsg('systemMessage'); autoPostForm(); return false;"><span class="save" style="padding-right:7px;"><u>S</u>ave Issue</span></a>        

		<a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); autoPostForm(); return false;"><span class="save" style="padding-right:7px;"><u>S</u>ave Issue!</span></a>        
                
    </form>
    
    
</body>
</html>

<script type="text/javascript"> 
	// Initialize TinyMCE with the tab_focus option 
	tinyMCE.init({ 
		mode : "exact",
		elements : "<% =activeElements %>",
		auto_resize : true,
		theme : "advanced",
		content_css : "../../_css/tinyMCE.css",
		plugins : "tabfocus,searchreplace,fullscreen,insertdatetime,paste",
		theme_advanced_buttons1 : "bold,italic,underline,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,fontsizeselect,forecolor,backcolor,separator,search,separator,pasteword,separator,insertdate,inserttime,separator,fullscreen",
		theme_advanced_buttons2 : "", 
		theme_advanced_buttons3 : "", 
		theme_advanced_toolbar_location : "top", 
		theme_advanced_toolbar_align : "center", 
		plugin_insertdate_dateFormat : "%A, %B %d, %Y",
		plugin_insertdate_timeFormat : "%I:%M %p",
		tab_focus : ':prev,:next'
	}); 
</script>

