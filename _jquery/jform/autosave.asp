<!--  These function use Ajax calls to post form data                  		-->
<!-- jQuery Form Submit http://jquery.malsup.com/form/#getting-started 		-->

<!--  NOTE: might want to user jqForm to validate the fields before    		-->
<!--  sending to issues_exec.asp or issues_exec_00.asp, values are ret as:	-->
<!--  var usernameValue = $('input[name=username]').fieldValue();      		-->

<script type="text/javascript">
	<!--
	
		var showPopUpWin = 'no'; 									//default to no popup window
		var arrClearElements = 'arrSubjectAdded,arrActivityAdded';	//elements passed to clear values after saved		
		
		// prepare the form when the DOM is ready 
		$(document).ready(function() { 
			var options = { 
				beforeSubmit: showRequest,	// pre-submit callback
				success: showResponse 		// post-submit callback
			}; 		 
			// bind form using 'ajaxForm' 
			$('#frm').ajaxForm(options); 
		});

		// pre-submit callback 
		function showRequest(formData, jqForm, options) { 
			if (showPopUpWin != 'yes') {
				//show status that record is in process of saving...
				document.getElementById('systemMessage').style.display = '';			
				document.getElementById('systemMessage').className = 'statusMessageINFO';
				document.getElementById('systemMessageTxt').innerHTML = "<img src='../_jquery/jform/myloader.gif' align='absmiddle' style='padding-right: 5px;'>Saving...please wait";
			}
			// here we could return false to prevent the form from being submitted; 
			// returning anything other than false will allow the form submit to continue 
			return true; 
		}

		// post-submit callback, where messages are delivered
		function showResponse(responseText, statusText, xhr, $form) {
			//jGrowl pop-up window
			if (showPopUpWin == 'yes') {
				//don't fire pop-up, but regular system message
				if (responseText.indexOf("Error:") > -1) {
					document.getElementById('systemMessage').className = 'statusMessageERROR';
					document.getElementById('systemMessageTxt').innerHTML = responseText;
					document.getElementById('systemMessage').style.display = '';
					$('#systemAlerts').jGrowl( 'Autosave: Error during save {' + CurrentDate() + ' ' + CurrentTime() + '}' , {header: 'System Alert', sticky: false } );
				}
				//show popup save window with date/time
				else {				
					$('#systemAlerts').jGrowl( 'Autosave: ' + responseText + '{' + CurrentDate() + ' ' + CurrentTime() + '}' , {header: 'System Alert', sticky: false } );
				}
				
			}
			//system message
			else {			
				//change message class
				if (responseText.indexOf("Error:") > -1) {
					document.getElementById('systemMessage').className = 'statusMessageERROR';
				}
				else {
					document.getElementById('systemMessage').className = 'statusMessageOK';				
				}
				//display system message response
				document.getElementById('systemMessageTxt').innerHTML = responseText;			
				document.getElementById('systemMessage').style.display = '';			
			}			
			
			//reset autosave flag notice type popup or table
			showPopUpWin = 'no';
			
			//update ALL form default values IF good save
			//so prompt not fired if navigateAway(url) is called
			if (responseText.indexOf("Error:") <= -1) {
				FormSaved(document.frm,arrClearElements);				
			}
			
		}

		//for sending submit from <a> tag
		//auto: triggers jGrowl pop-up to inform user Yes/No.
		function autoPostForm(auto) {				
			//trigger save/convert of all TinyMCE text areas for proper save
			tinyMCE.triggerSave();				
			//called via autosave
			if (auto == 'yes') {
				//make sure there is something that needs to be saved
				//prevents multiple entries in Audit SQL tables
				if(isFormChanged(document.frm)==true) {					
					showPopUpWin = auto;	//message type under showResponse()
					$("#frm").submit();		//submit form for POST
				}				
				//inform user there is nothing to save
				//this prevents repeated Audit history records in SQL DB
				else {
					$('#systemAlerts').jGrowl( 'Autosave: No changes to save.' + '{' + CurrentDate() + ' ' + CurrentTime() + '}' , {header: 'System Alert', sticky: false } );
				}
			}
			//called via button/href click
			else {
				$("#frm").submit();		//submit form for POST
			}
        }
		
		//****************************************
		//setup autosave timer
		//****************************************
		var timerID = null;				//timer ID
		var auto = 'no';				//trigger that starts timer or not yes/no
        var secs = '<% =autoSave %>';	//pulled from Logins table, user setting in minutes converted here to milliseconds
		
		if (secs != null && secs != '0' && secs != '' ) {
			auto = 'yes';
			secs = parseInt(secs) * 60000 //convert minutes to milliseconds
		}
		else {
			auto = 'no';
		}

        function StartTheTimer() {
			if (auto == 'yes') {
				//pass a 'yes' so we know its from the auto timer for 
				//setting message type under showResponse()
	            timerID = setInterval("autoPostForm('yes');", secs);
			}
        }		
        function StopTheTimer() {
            clearTimeout(timerID);
        }
		//stops timer immediately and does not come back on till page refersh
        function Aggressive_StopTheTimer() {
			secs = '0';
			auto = 'no';
            clearTimeout(timerID);
        }				
	//-->
</script>


