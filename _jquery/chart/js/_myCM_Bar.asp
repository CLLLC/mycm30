
	<script type="text/javascript"> 
        Highcharts.theme = { colors: ['#4572A7'] };// prevent errors in default theme
        var highchartsOptions = Highcharts.getOptions(); 
    </script>

	<!-- initialize the chart on document ready -->
	<script type="text/javascript">			
			
		var chart;
		$(document).ready(function() {
		   chart = new Highcharts.Chart({
			  chart: {
				 renderTo: 'chartDiv',
				 defaultSeriesType: 'bar'
			  },
			  title: {
				 text: '<% =mySQLName %>'
			  },
  			  subtitle: {
				 text: '<% =labelTop15 %>'
			  },			  
			  xAxis: {
				 categories: [<% =strChartCategories %>],
				 title: {
					text: null
				 }
			  },
			  yAxis: {
				 min: 0,
				 title: {
					text: 'Issues',
					align: 'high'
				 }
			  },
			  tooltip: {
				 formatter: function() {
					return ''+
						this.series.name +': '+ this.y +'';
				 }
			  },
			  plotOptions: {
				 bar: {
					dataLabels: {
					   enabled: true
					}
				 }
			  },			  
			  legend: {
				enabled: <% =chartLegendEnabled %>,
			   	backgroundColor: Highcharts.theme.legendBackgroundColor || '#FFFFFF',
				align: '<% =chartLegendAlign %>',
				verticalAlign: '<% =chartLegendVertical %>',
				layout: '<% =chartLegendLayout %>',
				shadow: true,
				floating: <% =chartLegendFloat %>,
				x: <% =chartLegendX %>,
				y: <% =chartLegendY %>
			  },			  
			  credits: {
				 enabled: false
			  },
				   series: [<% =strChartSeries %>]
		   });		   
		   
		});   
   	</script>
    
   