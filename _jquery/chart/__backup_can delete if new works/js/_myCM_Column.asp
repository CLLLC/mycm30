
	<script type="text/javascript"> 
        Highcharts.theme = { colors: ['#4572A7'] };// prevent errors in default theme
        var highchartsOptions = Highcharts.getOptions(); 
    </script>

	<!-- initialize the chart on document ready -->
	<script type="text/javascript">			
			
		var chart;
		$(document).ready(function() {
			chart = new Highcharts.Chart({
				chart: {
					renderTo: 'chartDiv',
					defaultSeriesType: 'column'
				},
				title: {
					text: '<% =mySQLName %>'
				},
			    subtitle: {
					 text: '<% =labelTop15 %>'
				  },			  				
				xAxis: {
					categories: [<% =strChartCategories %>],
					title: {
						text: null
					}
				},
				yAxis: {
					min: 0,
					title: {
						text: 'Issues'
					},
					labels: {
						formatter: function() {
							return Highcharts.numberFormat(this.value, 0);
						}
					}
				},
				tooltip: {
					formatter: function() {
						return ''+
							this.x +': '+ this.y +' issues';
					}
				},
				plotOptions: {
					bar: {
						dataLabels: {
							enabled: true
						}
					}
				},
				legend: {
					enabled: <% =chartLegendEnabled %>,
				   	backgroundColor: Highcharts.theme.legendBackgroundColor || '#FFFFFF',
					align: '<% =chartLegendAlign %>',
					verticalAlign: '<% =chartLegendVertical %>',
					layout: '<% =chartLegendLayout %>',
					shadow: true,
					floating: <% =chartLegendFloat %>,
					x: <% =chartLegendX %>,
					y: <% =chartLegendY %>
				},			  
				credits: {
					enabled: false
				},
			    series: [<% =strChartSeries %>]
			});
							
		});						
   	</script>
    