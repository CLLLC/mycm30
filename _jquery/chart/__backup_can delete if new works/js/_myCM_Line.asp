
	<script type="text/javascript"> 
        Highcharts.theme = { colors: ['#4572A7'] };// prevent errors in default theme
        var highchartsOptions = Highcharts.getOptions(); 
    </script>

	<!-- initialize the chart on document ready -->
	<script type="text/javascript">			

		var chart;
		$(document).ready(function() {
		   chart = new Highcharts.Chart({
			  chart: {
				 renderTo: 'chartDiv',
				 defaultSeriesType: 'line',
				 marginRight: 130,
				 marginBottom: 25
			  },
			  title: {
				 text: '<% =mySQLName %>',
				 x: -20 //center
			  },
			  subtitle: {
					 text: '<% =labelTop15 %>'
				  },			  
			  xAxis: {
				 categories: [<% =strChartCategories %>]
			  },
			  yAxis: {
				 title: {
					text: 'Issues'
				 },
				 plotLines: [{
					value: 0,
					width: 1,
					color: '#808080'
				 }]
			  },
			  tooltip: {
				 formatter: function() {
						   return this.x + ': ' + Highcharts.numberFormat(this.y, 0);
				 }
			  },
			  legend: {
				enabled: <% =chartLegendEnabled %>,
			   	backgroundColor: Highcharts.theme.legendBackgroundColor || '#FFFFFF',
				align: '<% =chartLegendAlign %>',
				verticalAlign: '<% =chartLegendVertical %>',
				layout: '<% =chartLegendLayout %>',
				shadow: true,
				floating: <% =chartLegendFloat %>,
				x: <% =chartLegendX %>,
				y: <% =chartLegendY %>
			  },
			  credits: {
				 enabled: false
			  },			  
			  series: [<% =strChartSeries %>]
		   });		   
		   
		});
   	</script>
    
    