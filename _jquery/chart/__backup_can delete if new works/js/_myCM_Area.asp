
	<script type="text/javascript"> 
        Highcharts.theme = { colors: ['#4572A7'] };// prevent errors in default theme
        var highchartsOptions = Highcharts.getOptions(); 
    </script>

	<!-- initialize the chart on document ready -->
	<script type="text/javascript">			
			
		var chart;
		$(document).ready(function() {
		   chart = new Highcharts.Chart({
			  chart: {
				 renderTo: 'chartDiv', 
				 defaultSeriesType: 'area'
			  },
			  title: {
				 text: '<% =mySQLName %>'
			  },
			  subtitle: {
					 text: '<% =labelTop15 %>'
				  },			  			  
			  xAxis: {
				 labels: {
					formatter: function() {
					   return this.value; // clean, unformatted number for year
					}
				 }                     
			  },
			  yAxis: {
				 title: {
					text: 'Issues'
				 },
				 labels: {
					formatter: function() {
					   return Highcharts.numberFormat(this.value, 0);
					}
				 }
			  },
			  tooltip: {
				 formatter: function() {
					return '<b>' + Highcharts.numberFormat(this.y, 0) + '</b> ' + this.series.name + '<br/>were created in ' + this.x;
				 }
			  },
			  plotOptions: {
				 area: {
					pointStart: 1999,
					marker: {
					   enabled: false,
					   symbol: 'circle',
					   radius: 2,
					   states: {
						  hover: {
							 enabled: true
						  }
					   }
					}
				 }
			  },
			  legend: {
				enabled: <% =chartLegendEnabled %>,
			   	backgroundColor: Highcharts.theme.legendBackgroundColor || '#FFFFFF',
				align: '<% =chartLegendAlign %>',
				verticalAlign: '<% =chartLegendVertical %>',
				layout: '<% =chartLegendLayout %>',
				shadow: true,
				floating: <% =chartLegendFloat %>,
				x: <% =chartLegendX %>,
				y: <% =chartLegendY %>
			  },			  
			  credits: {
				 enabled: false
			  },			  
			  series: [<% =strChartSeries %>]				  
		   });
		   		   
		});
   	</script>
    