<%@ Language=VBScript %>
<!-- 
	FOR HELP SEE the following sites: 
		http://code.google.com/p/aspjson/ 
    	http://jqueryui.com/demos/autocomplete/#multiple (source code)
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Suggestion Box Sample</title>

    <link type="text/css" rel="stylesheet" href="../jquery-ui-1.8.11.custom.css" />

	<style type="text/css">
	<!--
	BODY, B, TD, P {COLOR: #333333; FONT-FAMILY: Verdana, Arial, helvetica; FONT-SIZE: 8pt;}
	-->
	</style>

	<style>
		.column { width: 170px; float: left; padding-bottom: 100px; }
		.portlet { margin: 0 1em 1em 0; }
		.portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; }
		.portlet-header .ui-icon { float: right; }
		.portlet-content { padding: 0.4em; }
		.ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
		.ui-sortable-placeholder * { visibility: hidden; }
	</style>
        
	<script language="JavaScript" src="../jquery-1.5.1.min.js"></script>
	<script language="JavaScript" src="../jquery-ui-1.8.11.custom.min.js"></script>
    
</head>

<body>
	
	<script>
	$(function() {
		$( ".column" ).sortable({
			connectWith: ".column"
		});

		$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
			.find( ".portlet-header" )
				.addClass( "ui-widget-header ui-corner-all" )
				.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
				.end()
			.find( ".portlet-content" );

		$( ".portlet-header .ui-icon" ).click(function() {
			$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
			$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
		});

		$( ".column" ).disableSelection();
	});
	</script>


    <div class="demo">
    
        <div class="column">
        
            <div class="portlet">
                <div class="portlet-header">Feeds</div>
                <div class="portlet-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
            </div>
            
            <div class="portlet">
                <div class="portlet-header">News</div>
                <div class="portlet-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
            </div>
        
        </div>
        
        <div class="column">
        
            <div class="portlet">
                <div class="portlet-header">Shopping</div>
                <div class="portlet-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
            </div>
        
        </div>
        
        <div class="column">
        
            <div class="portlet">
                <div class="portlet-header">Links</div>
                <div class="portlet-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
            </div>
            
            <div class="portlet">
                <div class="portlet-header">Images</div>
                <div class="portlet-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
            </div>
        
        </div>
    
    </div>

</body>
</html>







