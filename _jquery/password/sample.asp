<%@ Language=VBScript %>
<!-- 
	FOR HELP SEE the following sites: 
	http://jordankasper.com/jquery/meter/examples.php
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Password Verify</title>

    <link type="text/css" rel="stylesheet" href="../jquery-ui-1.8.11.custom.css" />

	<style type="text/css">
	<!--
	BODY, B, TD, P {COLOR: #333333; FONT-FAMILY: Verdana, Arial, helvetica; FONT-SIZE: 8pt;}
	-->
	</style>
        
	<script language="JavaScript" src="../jquery-1.5.1.min.js"></script>
	<script language="JavaScript" src="../jquery-ui-1.8.11.custom.min.js"></script>
    
	<link rel="stylesheet" type="text/css" href="simplePassMeter.css"/>    
    <script language="JavaScript" src="jquery.simplePassMeter-0.4.js"></script>    

</head>
<body>

	<input type='password' id='myPassword' name='myPassword' />
    
    <script>	
		$('#myPassword').simplePassMeter({
		  'requirements': {
			'minLength': {'value': 8},  // at least 10 characters
			'lower': {'value': true},   // at least 1 lower-case letter
			'upper': {'value': true},   // at least 1 upper-case letter
			'special': {'value': true}  // at least 1 special character
		  }
		});
    </script>


</body>
</html>


