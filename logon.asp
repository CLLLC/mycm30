<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement, Pro, Lite, Metrics
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="_includes/_INCconfig_.asp"-->
<!--#include file="_includes/_INCappFunctions_.asp"-->
<!--#include file="_includes/_INCappDBConn_.asp"-->

<%
'Database
dim mySQL, cn, rs, cmd
dim msg


'*********************************************************
'Open Database Connection
call openDB()

'Site Configuration
if loadConfig() = false then
	response.redirect "error/default.asp?errMsg=" & server.URLEncode("Could not load Configuration settings.")
end if
'*********************************************************


dim urlLink
urlLink = request.querystring("redirect")
if len(urlLink) <= 0 then
	urlLink = request.form("redirect")
end if

if request.form("action") = "logon" then

	'*********************************************
	'save cookie for Remember Me	
	if Request.form("rememberMe") ="1" Then
		Response.Cookies("myCM3-UsernameCookie") = Request.Form("adminUser")
		Response.Cookies("myCM3-RememberMeCookie") = "1"
		Response.Cookies("myCM3-UsernameCookie").expires = Now() + 60
		Response.Cookies("myCM3-RememberMeCookie").expires = Now() + 60
	else			
		Response.Cookies("myCM3-UsernameCookie") = "" 
		Response.Cookies("myCM3-RememberMeCookie") = "" 
	end if	

	'*********************************************
	'Check UserID and Password for Login
	if len(trim(Request.Form("adminUser"))) = 0 _
	or len(trim(Request.Form("adminPass"))) = 0 then
	
		'Give error
		session.abandon
		msg= "Invalid User name or Password."
	
	else		

		'to create a RANDOM session for this user
		Randomize
		siteID = siteID & "_" & Int((rnd*999))+1		
		session("siteID") = siteID
		
		'Open Database Connection
		call openDB()
	
		'Open Recordset of Users
		mySQL = "SELECT Logins.* " _
			  & "	FROM Logins INNER JOIN Customer ON Logins.CustomerID = Customer.CustomerID " _
			  & "	WHERE Customer.Canceled=0 AND Username = '" & validSQL(trim(Request.Form("adminUser")),"A") & "' "
		set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,50)	
			
		'User found in database
		if not rs.eof then

			dim encryptedPass 
			call openStoredProc("proc_GetPassword")					
			cmd.Parameters.Append(cmd.CreateParameter("@UserName", adVarWChar, adParamInput, 50, validSQL(trim(Request.Form("adminUser")),"A")))    
			cmd.Parameters.Append(cmd.CreateParameter("@Passwrd", adVarWChar, adParamOutput, 50))
			cmd.Execute
			encryptedPass = cmd.Parameters("@Passwrd").Value
			
			'PASSWORD matches database	
			'if (rs("password") = trim(Request.Form("adminPass"))) and cLng(rs("LoginAttempts")) < 5 and rs("active") = "Y" and rs("expires") > Date() Then	
			'''if (rs("password") = validSQL(trim(Request.Form("adminPass")),"A")) and cLng(rs("LoginAttempts")) < 5 and rs("active") = "Y" and rs("expires") > Date() Then
			
			if (encryptedPass = validSQL(trim(Request.Form("adminPass")),"A")) and cLng(rs("LoginAttempts")) < 5 and rs("active") = "Y" and rs("expires") > Date() Then

				'set user's session LOGID
				session(session("siteID") & "logid") = rs("logid")

				'*********************************************
				'account needs to be RESET, send before assigning any other session variables
				if rs("PasswordReset") <= date() then					
					response.redirect "logon_reset.asp?action=reset&recid=" & rs("logid") & "&redirect=" & urlLink
				end if
				'*********************************************
										
				'set user session variables
				session(session("siteID") & "adminLoggedOn") = rs("SecurityLevel")
				if cLng(session(session("siteID") & "adminLoggedOn")) <= 0 then											
					msg = "Invalid permission settings. Please contact CCI at 1-800-617-0415."				
					session.abandon	
					response.redirect ("logon.asp")			
				end if
				
				session(session("siteID") & "firstname")	 = rs("FirstName")
				session(session("siteID") & "lastname")		 = rs("LastName")
				session(session("siteID") & "customerid")	 = rs("customerid")
				session(session("siteID") & "homepage")	 	 = rs("homepage")				
				session(session("siteID") & "homepagekey") 	 = rs("homepagekey")	

				'*********************************************
				' Update last time user logged in
				Dim tmpDate						
				tmpDate = Now()			
				'Update Record
				mySQL = "UPDATE Logins SET " _
					  & "LoginDate= '" & Now() & "', " _					  
					  & "LoginAttempts = 0, " _
					  & "Expires= '" & DateAdd("m",6,Date()) & "' " _
					  & "WHERE LogID = " & session(session("siteID") & "logid")						  
				set rs = openRSexecute(mySQL)
				'*********************************************

				'***************************************************
				'Used to diirect user to proper issues_edit.asp page
				'	issues_edit.asp =  users level >= 10
				'	issues_edit_00.asp = users level < 10
				if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
					session(session("siteID") & "issuesEdit") = "issues_edit_00.asp"
				else
					session(session("siteID") & "issuesEdit") = "issues_edit.asp"
				end if		
				'***************************************************


				'Send to issues page
				if len(urlLink) > 0 then
					response.redirect siteURL & "scripts/" & session(session("siteID") & "issuesEdit") & "?action=edit&recid=" & urlLink
				else
					if session(session("siteID") & "homepage") = "profiles.asp" or session(session("siteID") & "homepage") = "profiles/profiles.asp" then
						response.redirect siteURL & "profiles/profiles.asp"
					elseif session(session("siteID") & "homepage") = "issues.asp" then						
						response.redirect siteURL & "scripts/issues.asp"
					elseif len(session(session("siteID") & "homepage")) > 0 then						
						response.redirect siteURL & session(session("siteID") & "homepage")
					end if
					
				end if		
			
			'PASSWORD does NOT match OR Login Attempts >= 5 OR not active OR Expires > today
			else

				'*********************************************
				' Update number of attempts login
				mySQL = "UPDATE Logins SET " _
					  & "LoginAttempts=[LoginAttempts]+1 " _
					  & "WHERE Username = '" & validSQL(trim(Request.Form("adminUser")),"A") & "' AND Active = 'Y'"
				set rs = openRSexecute(mySQL)
	
				'Open Recordset of Users
				mySQL = "SELECT Active, LoginAttempts, Expires FROM Logins WHERE Username = '" & validSQL(trim(Request.Form("adminUser")),"A") & "' "
				set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,50)	
				
				'account is INACTIVE
				if rs("Active") = "N" or rs("Expires") <= Date() then					
					response.redirect "logon_locked.asp?t=inactive"
				
				'account is LOCKED
				elseif cLng(rs("LoginAttempts")) >= 5 then					
					response.redirect "logon_locked.asp?t=locked"
				
				'wrong credintials
				else
					call closeRS(rs)						
					msg = "Invalid login credentials. Please try again."				
					session.abandon
					
				end if			

			end if
						
		'User NOT found in database
		else			
			call closeRS(rs)						
			msg = "Invalid login credentials. Please try again."
			session.abandon
			
		end if
				
	end if	

'*********************************************	
'Logoff
elseif lCase(trim(request.QueryString("action"))) = "logoff" then 
	'kill all session variables and cookies
	session.abandon 	
	%>        
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="scripts/javascript/common.js"></script>   
    <script language="javascript">	
       	destroyAllCookies();
	</script>
    <%	

end if
%>

<html>

<head>
	<title>myCM 3.0</TITLE>

	<link rel="shortcut icon" href="_images/favicon.ico" >
    	
	<link type="text/css" rel="stylesheet" href="_css/default.css" />
	<link type="text/css" rel="stylesheet" href="_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="_css/text.css" />
    <link type="text/css" rel="stylesheet" href="_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="_css/statusMessage.css" />

	<!-- Specialized dialog boxes from  www.leigeber.com -->
	<link rel="stylesheet" type="text/css" href="_dialogs/common/dialog_box.css" />
	<script type="text/javascript" src="_dialogs/common/dialog_box.js"></script>
</head>

<body topmargin="0" OnLoad="document.logonForm.adminUser.focus();">


<form METHOD="POST" name="logonForm" id="logonForm" action="logon.asp" style="padding:0px; margin:0px;">

  <div style="margin-bottom:25px;">&nbsp;</div>

  <div id="content">
    
    <table width="375" cellpadding="0" cellspacing="0" border="0" align="center">
    
		<% if (len(systemNotice)>0 and cDate(systemNoticeExpire)>date()) and len(msg)<=0 then %>
          <tr>
            <td style="padding:2px;">
	        <% call systemMessageBox("systemNotice","statusMessageINFO",systemNotice) %>      
            </td>
          </tr>
		<% end if %>

    	<% if len(msg)>0 then %>        
          <tr>
            <td style="padding:2px;">    
                <% call systemMessageBox("systemMessage","statusMessageERROR",msg) %>        
            </td>
          </tr>
		<% end if %>
    
      <tr>
        <td style="background-color: #3B5998;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="padding-left:10px;"><b><font size=3 color="#FFFFFF">Log On</font></b></td>
                    <td align="right"><img src="_images/login2.jpg"></td>
                </tr>
            </table>
        </td>
      </tr>
      
      <tr>  
        <td style="border: 1px solid #3B5998; padding:10px; background:url(_images/login_bg.jpg) repeat;">
            <table width="375" border="0" align="center" cellpadding="5" cellspacing="0">
              <tr>
                <td colspan="2" style="padding-top: 5px; padding-bottom: 15px; BORDER-bottom: 1px dashed #CCCCCC;">Enter your myCM (2.0) Server username and password to log in. </td>
                </tr>
              <tr>
                <td width="75" style="padding-top: 10px;"><strong>User name:</strong></td>
                <td style="padding-top: 10px;"><input class="boxLight" type=text name="adminUser" id="adminUser" size=20 value="<% =Request.Cookies("myCM3-UsernameCookie") %>">&nbsp;&nbsp; 
                  <label>
                    <input name="rememberMe" type="checkbox" id="rememberMe" value="1" tabindex="-1" <% if Request.Cookies("myCM3-RememberMeCookie") = "1" then response.Write("checked") %>> Remember me
                  </label></td>
              </tr>
              <tr>
                <td width="75" style="padding-top: 15px;"><strong>Password:</strong></td>
                <td style="padding-top: 10px;"><input class="boxLight" type=password name=adminPass size=20></td>
              </tr>
              <tr>
                <td style="padding-top: 10px;">
                	<input type="hidden" name="action" value="logon">
	                <input type="hidden" name="redirect" value="<% =urlLink %>">
                </td>
                <td style="padding-top: 10px;"><input class="submitButton" type="submit" name="Submit" value="Log On">&nbsp;&nbsp;[&nbsp;<a href="logon_lost.asp">Forgot Password</a>&nbsp;]</td>
              </tr>
              <tr>
                <td colspan="2" style="padding-top: 10px;">&nbsp;</td>
               </tr>
            </table>
        </td>
      </tr>
	  <tr>
	    <td style="padding-top:10px;">
        	<div style="float:left;">Copyright &copy; <% =year(date()) %>***Matelio Test ***</div>
            <div style="float:right;"><a href="http://www.complianceline.com" target="_blank">ComplianceLine, Inc.</a></div>        
        </td>
      </tr>
    </table>

	<!-- there are necessary to wash out entire login box if error... -->
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>

  </div>

</form>

</body>
</html>

<%
'close database connection
call closeDB()
%>

<script language="JavaScript">
	<!--
//	if ((screen.width>=1024) && (screen.height>=760)) {
//	 	document.logonForm.adminUser.focus();
//	}
//	else {
//		showDialog('Warning','Your screen resolution appears to be less than 1024x768. This site is best viewed on a screen equal to or larger than 1024x768.<br><br>It is recommened that you change your screen size before using this site.<br><br>ComplianceLine Support','warning');
//	}
	//-->
</script>
