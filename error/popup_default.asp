<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
const helpID = 0 ' 0 equals NO help...
%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>myCM 3.0 : System Error</TITLE>

	<link rel="shortcut icon" href="../_images/favicon.ico" >

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->

<body>

<!-- ANCHOR for links -->
<div id="top"></div>

<table style="background:#3B5998" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
  
   	<!-- START Top-Left Box -->
    <td width="190" align="right" height="41"><a href="../default.asp"><img src="../_images/header.jpg" title="myCM" width="146" height="41" border="0"></a></td>
    <td valign="bottom">
    
    <!-- START Top-Middle Box -->
    <table id="simpleSearchBox" width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>

        <!-- START Search Box -->
        <td style="padding:3px;">&nbsp;</td>
       	<!-- END Search Box -->                  
        
        <!-- START DHTML TOP menu -->						
        <td align="right">&nbsp;</td>
		<!-- END DHTML TOP menu -->					
            
      </tr>
    </table>
    <!-- END Top-Middle Box -->
    
    </td>
    <td width="2%">&nbsp;</td>
  </tr>
</table>

<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">

	<tr>
    	<td align="center">
        
        	<div style="width:75%; margin-top:50px;">
                <% call systemMessageBox("systemMessage","statusMessageERROR","See system error messsage below.") %>
            </div>
            
            <div style="width:75%; text-align:left; margin-top:25px;">
	            <% response.Write("<strong>" & Request.QueryString("errMsg") & "</strong>") %>
            </div>
            
            <div style="width:75%; text-align:left; margin-top:25px; margin-bottom:25px;">
		        <a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close();"><span class="cancel"><u>C</u>lose</span></a>                   
            </div>

        </td>
	</tr>

</table>
<!-- END Page -->

</body>
</html>
