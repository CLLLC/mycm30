<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement, Pro, Lite, Metrics
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"

dim urlLink : urlLink = request.QueryString("redirect")
dim adminLoggedOn : adminLoggedOn = session(session("siteID") & "adminLoggedOn")

if len(urlLink) > 0 and (isEmpty(adminLoggedOn) or isNull(adminLoggedOn) or not isNumeric(adminLoggedOn)) then
	response.redirect "logon.asp?redirect=" & urlLink
elseif (isEmpty(adminLoggedOn) or isNull(adminLoggedOn) or not isNumeric(adminLoggedOn)) then
	response.redirect "logon.asp"
elseif len(urlLink) > 0 then
	response.redirect "scripts/" & session(session("siteID") & "issuesEdit") & "?action=edit&recid=" & urlLink
else
	
	if session(session("siteID") & "homepage") = "profiles.asp" then
		response.redirect "profiles/profiles.asp"
	elseif session(session("siteID") & "homepage") = "issues.asp" then
		response.redirect "scripts/issues.asp"
	elseif len(session(session("siteID") & "homepage")) > 0 then
		response.redirect session(session("siteID") & "homepage")
	end if
	
end if

%>