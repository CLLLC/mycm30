<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement, Pro, Lite, Metrics
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="_includes/_INCconfig_.asp"-->
<!--#include file="_includes/_INCappFunctions_.asp"-->
<!--#include file="_includes/_INCappDBConn_.asp"-->

<%
'Database
dim mySQL, cn, rs

'*********************************************************
'Open Database Connection
call openDB()

'Site Configuration
if loadConfig() = false then	
	response.redirect "error/popup_default.asp?errMsg=" & server.URLEncode("Could not load Configuration settings.")
end if
'*********************************************************

'used in conjunction with topWin.parent.navigateAway('logon.asp'); 
'on calling page to navigate back to main logon.asp
dim accountStatus : accountStatus = "good"

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>myCM 3.0</TITLE>

	<link rel="shortcut icon" href="_images/favicon.ico" >
    	
	<link type="text/css" rel="stylesheet" href="_css/default.css" />
	<link type="text/css" rel="stylesheet" href="_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="_css/text.css" />
    <link type="text/css" rel="stylesheet" href="_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="_css/statusMessage.css" />

	<!-- Specialized dialog boxes from  www.leigeber.com -->
	<link rel="stylesheet" type="text/css" href="_dialogs/common/dialog_box.css" />
	<script type="text/javascript" src="_dialogs/common/dialog_box.js"></script>

	<!--JQuery required components -->
    <script language="JavaScript" src="_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="_jquery/jquery-ui-1.8.11.custom.min.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/code-bank/simple-modal/ -->
	<script type="text/javascript" src="scripts/javascript/simpleModal.js"></script>

	<!-- jQuery Prompt Replacements http://abeautifulsite.net/blog/2008/12/jquery-alert-dialogs/ -->
    <script src="_jquery/prompt/jquery.alerts.js"></script>
    <link rel="stylesheet" type="text/css" href="_jquery/prompt/jquery.alerts.css"/>   

</head>

<body topmargin="0">

<%
if request.form("action") = "logon" then
	
	'*********************************************
	'save cookie for Remember Me	
	if Request.form("rememberMe") ="1" Then
		Response.Cookies("myCM3-UsernameCookie") = Request.Form("adminUser")
		Response.Cookies("myCM3-RememberMeCookie") = "1"
		Response.Cookies("myCM3-UsernameCookie").expires = Now() + 60
		Response.Cookies("myCM3-RememberMeCookie").expires = Now() + 60
	else			
		Response.Cookies("myCM3-UsernameCookie") = "" 
		Response.Cookies("myCM3-RememberMeCookie") = "" 
	end if	
	
	'*********************************************
	'Check UserID and Password for Login
	if len(trim(Request.Form("adminUser"))) = 0 _
	or len(trim(Request.Form("adminPass"))) = 0 then
	
		'Give error
		session.abandon
		%>
		<script type="text/javascript">
			jAlert('Invalid User name or Password.', 'myCM Alert');
		</script>
		<%										
	
	else		

		'to create a RANDOM session for this user
		Randomize
		siteID = siteID & "_" & Int((rnd*999))+1		
		session("siteID") = siteID
		
		'Open Database Connection
		call openDB()
	
		'Open Recordset of Users
		mySQL = "SELECT * FROM Logins WHERE Username = '" & trim(Request.Form("adminUser")) & "' "
		set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,50)	
			
		'User found in database
		if not rs.eof then
			
			'PASSWORD matches database	
			if (rs("password") = trim(Request.Form("adminPass"))) and cLng(rs("LoginAttempts")) < 5 and rs("active") = "Y" Then	
										
				'set user session variables
				session(session("siteID") & "adminLoggedOn") = rs("SecurityLevel")
				session(session("siteID") & "firstname")	 = rs("FirstName")
				session(session("siteID") & "lastname")		 = rs("LastName")
				session(session("siteID") & "customerid")	 = rs("customerid")
				session(session("siteID") & "homepage")	 	 = rs("homepage")				
				session(session("siteID") & "logid")		 = rs("logid")

				'Session.Timeout = 180 	' equals 3 hours
				
				'*********************************************
				'account needs to be RESET
				if rs("PasswordReset") <= date() then
					accountStatus = "reset"
				
				else
				
					'*********************************************
					' Update last time user logged in
					Dim tmpDate						
					tmpDate = Now()			
					'Update Record
					mySQL = "UPDATE Logins SET " _
						  & "LoginDate= '" & Now() & "', " _
						  & "LoginAttempts = 0 " _
						  & "WHERE LogID = " & session(session("siteID") & "logid")						  
					set rs = openRSexecute(mySQL)
					'*********************************************

					'GOOD LOGIN -- Close form and clear any error message
					%>
					<script type='text/javascript'>
						SimpleModal.close();
					</script>
					<%
					
				end if
				
			'PASSWORD does NOT match OR Login Attempts >= 5 OR not active
			else

				'*********************************************
				' Update number of attempts login
				mySQL = "UPDATE Logins SET " _
					  & "LoginAttempts=[LoginAttempts]+1 " _
					  & "WHERE Username = '" & trim(Request.Form("adminUser")) & "' AND Active = 'Y'"
				set rs = openRSexecute(mySQL)
	
				'Open Recordset of Users
				mySQL = "SELECT Active, LoginAttempts FROM Logins WHERE Username = '" & trim(Request.Form("adminUser")) & "' "
				set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,50)	
				
				'account is INACTIVE
				if rs("Active") = "N" then
					call closeRS(rs)
					accountStatus = "expired"
				
				'account is LOCKED
				elseif cLng(rs("LoginAttempts")) >= 5 then
					call closeRS(rs)			
					accountStatus = "locked"
										
				'wrong credintials
				else
					call closeRS(rs)						
					session.abandon
					%>
					<script type="text/javascript">
						jAlert('Invalid login credentials. Please try again.', 'myCM Alert');
					</script>
					<%										
					
				end if			

			end if
						
		'User NOT found in database
		else			
			call closeRS(rs)						
			session.abandon
			%>
			<script type="text/javascript">
				jAlert('Invalid login credentials. Please try again.', 'myCM Alert');
			</script>
			<%										
			
		end if
		
	end if	

'*********************************************	
'Logoff
elseif lCase(trim(request.QueryString("action"))) = "logoff" then 
	'this should never be called.
	
end if
%>

<form METHOD="POST" name="logonForm" id="logonForm" action="logon_dialog.asp" style="padding:0px; margin:0px;">

  <!-- OK to show login form -->
  <% if accountStatus = "good" then %>

    <table width="375" cellpadding="0" cellspacing="0" border="0" align="center">
        
      <tr>
        <td style="background-color: #3B5998;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="padding-left:10px;"><b><font size=3 color="#FFFFFF">Log On</font></b></td>
                    <td align="right"><img src="_images/login2.jpg"></td>
                </tr>
            </table>
        </td>
      </tr>
      
      <tr>  
        <td style="padding:10px; background:url(_images/login_bg.jpg) repeat;">
            <table width="375" border="0" align="center" cellpadding="5" cellspacing="0">
              <tr>
                <td colspan="2" style="padding-top: 5px; padding-bottom: 15px; BORDER-bottom: 1px dashed #CCCCCC;"><span class="required">Your session has expired. Please log back in.</span></td>
                </tr>
              <tr>
                <td width="75" style="padding-top: 10px;"><strong>User name:</strong></td>
                <td style="padding-top: 10px;"><input class="boxLightDialog" type=text name="adminUser" id="adminUser" size=20 value="<% =Request.Cookies("myCM3-UsernameCookie") %>">&nbsp;&nbsp; 
                  <label>
                    <input name="rememberMe" type="checkbox" id="rememberMe" value="1" tabindex="-1" <% if Request.Cookies("myCM3-RememberMeCookie") = "1" then response.Write("checked") %>> Remember me
                  </label></td>
              </tr>
              <tr>
                <td width="75" style="padding-top: 15px;"><strong>Password:</strong></td>
                <td style="padding-top: 10px;"><input class="boxLightDialog" type=password name=adminPass size=20></td>
              </tr>
              <tr>
                <td style="padding-top: 10px;">
                	<input type="hidden" name="action" value="logon">
                </td>
                <td style="padding-top: 10px;"><input class="submitButton" type="submit" name="Submit" value="Log On"></td>
              </tr>
              <tr>
                <td colspan="2" style="padding-top: 10px;">&nbsp;</td>
               </tr>
            </table>
        </td>
      </tr>
    </table>

  <!-- NOT OK to show login form -->
  <% else %>

    <table width="375" cellpadding="0" cellspacing="0" border="0" align="center">    
   
      <tr>
        <td style="background-color: #3B5998;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="padding-left:10px;"><b><font size=3 color="#FFFFFF">Login</font></b></td>
                    <td align="right"><img src="_images/login2.jpg"></td>
                </tr>
            </table>
        </td>
      </tr>
      
      <tr>  
        <td style="padding-top:10px; padding-left:10px; padding-right:10px; padding-bottom:60px; background:url(_images/login_bg.jpg) repeat;">

            <table width="375" border="0" align="center" cellpadding="5" cellspacing="0">
        		<tr>                    
					<% if accountStatus = "reset" then %>                                               
               			<td style="padding-top:10px; padding-left:30px; padding-right:30px;"><b><font size=3>Password Reset</font></b><br><br>Your password has expired or needs to be reset.<br><br>Please contact 1-800-617-2111.</td>
					<% elseif accountStatus = "expired" then %>
            	 		<td style="padding-top:10px; padding-left:30px; padding-right:30px;"><b><font size=3>Account Expired!</font></b><br><br>Your account is inactive or has expired.<br><br>Please contact 1-800-617-2111.</td>
					<% elseif accountStatus = "locked" then %>
            			<td style="padding-top:10px; padding-left:30px; padding-right:30px;"><b><font size=3>Account Locked!</font></b><br><br>Due to invalid login attempts, your account is temporarily locked.<br><br>Please contact 1-800-617-2111.</td>
               		<% end if %>
           		</tr>                
        		<tr>
        			<td style="padding-top:10px; padding-left:30px; padding-right:30px;"><a href="#" onClick="SimpleModal.close(); topWin.parent.navigateAway('logon.asp'); return false;">Return to Login</a></td>
        		</tr>
           		<tr>
       				<td style="padding-top:10px; padding-left:30px; padding-right:30px;">&nbsp;</td>
           		</tr>
  			</table>                           
            
        </td>        
      </tr>
	</table>

  <% end if %>

</form>

</body>
</html>

<%
'close database connection
call closeDB()
%>

