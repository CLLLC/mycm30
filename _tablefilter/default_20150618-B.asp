<%
'===============================================
' This function is based off the HTML Table/Filter
' generator published here http://tablefilter.free.fr
' visit website for configuration parameters
'===============================================
sub buildGrid(gridID,gridSQL,gridTitle,editLink,editIcon,tableProperty)

'response.redirect "../error/default.asp?errMsg=" & server.URLEncode("test 5")

'Declare variables
dim mySQL, cn, rs
dim field, count, fldValue
dim recordCount

dim gridCounter
dim colCount
dim xCol

dim bolVisible
dim tempNameID, tempName, tempAddress, tempCity, tempZip, tempCountry, tempHierarchy, tempDirective
dim tempLevel_1, tempLevel_2, tempLevel_3, tempLevel_4, tempLevel_5, tempLevel_6, tempLevel_7, tempLevel_8

dim rowCount

'Create unique table ID. This is used for
'generating table grid and paging.
dim idTable
if len(gridID) = 0 then
	randomize	
	idTable = "table_" & cLng(rnd*99)+1
else
	idTable = gridID
end if
'********************************************************************

'Open RecordSet
set rs = openRSopen(gridSQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	'Reset Counter
	count = 1

	if rs.State = adStateClosed then
		response.write("<table width='100%' cellpadding=0 cellspacing=0>")
        response.write("<tr><td align=center valign=middle style=""padding-right:2px;"">")
        call systemMessageBox("systemMessage","statusMessageINFO","No records matched your search criteria.")
		response.Write("</td></tr></table>")
        		
	else
	
		'get row count
		rowCount = rs.recordcount

		'no results were returned
		if rs.eof then
		
			Response.Write("<table width='100%' cellpadding=0 cellspacing=0>")
            response.write("<tr><td align=center valign=middle style=""padding-right:2px;"">")
            call systemMessageBox("systemMessage","statusMessageINFO","No records matched your search criteria.")
			response.Write("</td></tr></table>")
		
		'if called from Dashboard and more than 1,000 records don't show.
		elseif rowCount > 2000 and lCase(editLink)<>"caller-popup" then

			Response.Write("<table width='100%' cellpadding=0 cellspacing=0>")
            response.write("<tr><td align=center valign=middle style=""padding-right:2px;"">")
            call systemMessageBox("systemMessage","statusMessageINFO","<b>Over 2,000 records returned.</b> Limit the results with a filter and re-run.")
			response.Write("</td></tr></table>")
			
		'everything good, display...
		else

			'show/print grid title
			if len(gridTitle) > 0 then
				Response.Write("<table width=""100%"" cellpadding=0 cellspacing=0>")
				Response.Write("<tr><td class=""clearFormat"" align=""left"" style=""color:#35487B; font-weight:bold; padding:5px;"">" & gridTitle & "</td></tr>")
				Response.Write("</table>")
			end if
					
			for each field in rs.fields
				colCount = colCount + 1
			next	

			'***************************
			'START main Grid table
			'***************************			
			Response.Write "<table width='100%' id='" & idTable & "' style='background-color:#969696;' cellpadding=0 cellspacing=0>"
		
			'Reset count
			xCol = 0
			
			do while not rs.eof
			
				'THEAD/TBODY on table
				if count = 1 then
					response.Write("<thead>")
				elseif count = 2 then
					response.Write("<tbody>")
				end if

				
				'TR start new row
				Response.Write "<tr>"
				for each field in rs.fields
					
					'reset column visibility...use to hide some columns
					bolVisible = true


					'use this to HIDE any unwanted columns...
					'special circumstances for dialog/popup_issue
					if lCase(editLink) = "issue-popup" then
						if lCase(field.name) = "callback" _ 
						   or lCase(field.name) = "status" _
						   or lCase(field.name) = "level_1" _
						   or lCase(field.name) = "level_2" _
						   or lCase(field.name) = "level_3" _
						   or lCase(field.name) = "level_4" _
						   or lCase(field.name) = "level_5" _
						   or lCase(field.name) = "level_6" _
						   or lCase(field.name) = "level_7" _
						   or lCase(field.name) = "level_8" then
							'hide column
							bolVisible = false
						end if
					end if

					'use this to HIDE any unwanted columns...
					'special circumstances for dialog/popup_location
					if lCase(editLink) = "location-popup" or lCase(editLink) = "location-table" then
						if lCase(field.name) = "city" _
						   or lCase(field.name) = "locationid" _
						   or lCase(field.name) = "nameid" _
						   or lCase(field.name) = "directiveid" _
						   or lCase(field.name) = "state" _
						   or lCase(field.name) = "zip" _
						   or lCase(field.name) = "country" _						   
						   or lCase(field.name) = "level_1" _
						   or lCase(field.name) = "level_2" _
						   or lCase(field.name) = "level_3" _
						   or lCase(field.name) = "level_4" _
						   or lCase(field.name) = "level_5" _
						   or lCase(field.name) = "level_6" _
						   or lCase(field.name) = "level_7" _
						   or lCase(field.name) = "level_8" _
						   or lCase(field.name) = "notes" then
							'hide column
							bolVisible = false
						end if
					end if

					'use this to HIDE any unwanted columns...
					'special circumstances for dialog/popup_location
					if lCase(editLink) = "caller-popup" then
						if lCase(field.name) = "city" _
						   or lCase(field.name) = "firstname" _
						   or lCase(field.name) = "lastname" _						
						   or lCase(field.name) = "callerid" _
						   or lCase(field.name) = "state" _
						   or lCase(field.name) = "zip" _
						   or lCase(field.name) = "phone_home" _								   
						   or lCase(field.name) = "phone_work" _						   
						   or lCase(field.name) = "notes" then
							'hide column
							bolVisible = false
						end if
					end if

					if lCase(editLink) = "directive" then
						if lCase(field.name) = "directiveid" then
							'hide column
							bolVisible = false
						end if
					end if

					if lCase(editLink) = "contact" then
						if lCase(field.name) = "logid" then
							'hide column
							bolVisible = false
						end if
					end if

					if lCase(editLink) = "telcom" then
						if lCase(field.name) = "telcomid" then
							'hide column
							bolVisible = false
						end if
					end if

					if lCase(editLink) = "category" then
						if lCase(field.name) = "categoryid" then
							'hide column
							bolVisible = false
						end if
					end if

					if lCase(editLink) = "customer-popup" then
						if lCase(field.name) = "customerid" then
							'hide column
							bolVisible = false
						end if
					end if


'HERE=========================
				'severity color change
				if lCase(field.name) = "severity" then 		
					if rs(field.name) = "1-High" or rs(field.name) = "2-Med" then
						foreColor = "color:#F00;"
					else
						foreColor = "color:#333333;"						
					end if
				else
					foreColor = "color:#333333;"						
				end if
'HERE=========================
					
					'************************************
					'TH print column HEADERS(s), first row only
					'************************************
					if count = 1 and (bolVisible = true) then

						'set first column of header row width
						response.Write("<th align=""left"" nowrap=""nowrap"">")						
							'show column names...defined in table, query or db view													
							if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and profileCount = 1 then
								response.write(setFriendlyName(replace(field.name,"_"," ") ,sCustomerID ,"No"))
							else
								response.Write(replace(field.name,"_"," "))
							end if							
						'close column header
						response.Write("</th>")
											
					'************************************	
					'TD print field VALUE(s), all rows but 1st one
					'************************************
					elseif (bolVisible = true) then

						'FIRST COLUMN -- for setting edit links and icons
						if xCol = 0 then

							'hyperlink on <TD> if field "CRSID"
							if lCase(editLink)<>"print" and lCase(editLink)<>"issue-popup" and (lCase(field.name) = "crsid" or lCase(field.name) = "issue #" or lCase(field.name) = "issue_#") then
								response.Write("<td nowrap=""nowrap"" align=""left"" style=""cursor:pointer; " & foreColor & """ onClick=""window.location='" & session(session("siteID") & "issuesEdit") & "?action=edit&recid=" & rs(field.name) & "'"">")

							'user change (i.e. 4101:Rainey, Bryan), take out name and ":" and only user ID
							elseif (lCase(editLink) = "full_name" and lCase(field.name) = "full_name") or (lCase(editLink) = "user" and lCase(field.name) = "user") or (lCase(editLink) = "case manager" and lCase(field.name) = "case manager") then
								response.Write("<td align=""left"" style=""cursor:pointer; " & foreColor & """ onClick=""window.location='issues.asp?showField=show" & editLink & "&showValue=" & server.URLEncode( left(rs(field.name),instr(rs(field.name),":")-1) ) & "'"">")

							'for passing selected location back to parent page on Issues_edit.asp
							elseif lCase(editLink) = "customer-popup" and lCase(field.name) = "name" then
								response.Write("<td nowrap=""nowrap"" align=""left"" style=""cursor:pointer; " & foreColor & """ onClick=""addIssue('" & rs("customerid") & "'," & cLng(session(session("siteID") & "adminLoggedOn")) & ")"">")

							'for passing selected table and history record to popup_hist_record.asp
							elseif inStr(lCase(editLink),"hist-999")>0 and lCase(field.name) = "history id" then
								response.Write("<td nowrap=""nowrap"" align=""left"" style=""cursor:pointer; " & foreColor & """ onclick=""javascript:window.open('../_dialogs/popup_hist_record.asp?hist=" & editLink & "&recid=" & rs("history id") & "')""; return false;"">")
								
							elseif inStr(lCase(editLink),"hist-")>0 and lCase(field.name) = "history id" then
								response.Write("<td nowrap=""nowrap"" align=""left"" style=""cursor:pointer; " & foreColor & """ onclick=""SimpleModal.open('../_dialogs/popup_hist_record.asp?hist=" & editLink & "&recid=" & rs("history id") & "', 500, 700, 'yes'); return false;"">")

							elseif lCase(editLink) = "directive" and lCase(field.name) = "directive" then
								response.Write("<td nowrap=""nowrap"" align=""left"" style=""cursor:pointer; " & foreColor & """ onclick=""SimpleModal.open('../_dialogs/popup_directive_record.asp?recid=" & rs("directiveid") & "', 500, 900, 'yes'); return false;"">")

							elseif lCase(editLink) = "contact" and lCase(field.name) = "contact" then
								response.Write("<td nowrap=""nowrap"" align=""left"" style=""cursor:pointer; " & foreColor & """ onclick=""SimpleModal.open('../_dialogs/popup_user_record.asp?recid=" & rs("logid") & "', 500, 700, 'yes'); return false;"">")

							elseif lCase(editLink) = "telcom" and lCase(field.name) = "number" then
								response.Write("<td nowrap=""nowrap"" align=""left"" style=""cursor:pointer; " & foreColor & """ onclick=""SimpleModal.open('../_dialogs/popup_telcom_record.asp?recid=" & rs("telcomid") & "', 500, 625, 'yes'); return false;"">")

							elseif lCase(editLink) = "category" and lCase(field.name) = "name" then
								response.Write("<td nowrap=""nowrap"" align=""left"" style=""cursor:pointer; " & foreColor & """ onclick=""SimpleModal.open('../_dialogs/popup_category_record.asp?recid=" & rs("categoryid") & "', 500, 675, 'no'); return false;"">")
								
							'for passing selected location back to parent page on Issues_edit.asp OR viewing from side panel
							elseif (lCase(editLink) = "location-popup" or lCase(editLink) = "location-table") and lCase(field.name) = "name" then
								
								if lCase(editLink) = "location-table" then
									response.Write("<td align=""left"" style=""cursor:pointer; " & foreColor & """ onclick=""SimpleModal.open('../_dialogs/popup_location_record.asp?recid=" & rs("locationid") & "', 525, 700, 'yes'); return false;"">")
								
								elseif lCase(editLink) = "location-popup" then

									'prepare location full address
									tempAddress = rs("address") & ""
									tempAddress = replace(tempAddress,"'","\'") 	'replaces all single quotes with proper Javascript \'
									tempAddress = replace(tempAddress,vbCrLf,"\n")	'replaces all line feeds with proper Javascript \n								
									'prepare location hierarchy
									tempHierarchy = rs("level_1") & "*|*" & rs("level_2") & "*|*" & rs("level_3") & "*|*" & rs("level_4") & "*|*" & rs("level_5") & "*|*" & rs("level_6") & "*|*" & rs("level_7") & "*|*" & rs("level_8")
									tempHierarchy = reformatHierarchy(tempHierarchy,"value")
									tempHierarchy = replace(tempHierarchy,"'","\'")	'replaces all single quotes with proper Javascript \'
									
									'no special coding for hierarch just yet
									'tempLevel_1, tempLevel_2, tempLevel_3, tempLevel_4, tempLevel_5, tempLevel_6, tempLevel_7, tempLevel_8
									
									tempDirective = rs("directiveid")
									if isNull(tempDirective) then
										tempDirective = 0
									end if															

									tempNameID = rs("nameid") & ""
									tempNameID = replace(tempNameID,"\","\\")
									tempNameID = replace(tempNameID,"'","\'")									
									tempNameID = replace(tempNameID,chr(34)," ")

									tempName = rs("name") & ""
									tempName = replace(tempName,"\","\\")
									tempName = replace(tempName,"'","\'")									
									tempName = replace(tempName,chr(34)," ")

									tempCity = rs("city") & ""
									tempCity = replace(tempCity,"\","\\")									
									tempCity = replace(tempCity,"'","\'")
									tempCity = replace(tempCity,chr(34)," ")

									tempZip = rs("zip") & ""
									tempZip = replace(tempZip,"\","\\")									
									tempZip = replace(tempZip,"'","\'")
									tempZip = replace(tempZip,chr(34)," ")

									tempCountry = rs("country") & ""
									tempCountry = replace(tempCountry,"\","\\")									
									tempCountry = replace(tempCountry,"'","\'")
									tempCountry = replace(tempCountry,chr(34)," ")

									'write output to screen and table
									response.Write("<td align=""left"" style=""cursor:pointer; " & foreColor & """ onClick=""setLocation( encodeURI('" & tempNameID & "'), encodeURI('" & tempName & "'), encodeURI('" & tempAddress & "'), encodeURI('" & tempCity & "'),'" & rs("state") & "', encodeURI('" & tempZip & "'), encodeURI('" & tempCountry & "'), '" & tempHierarchy & "', " & tempDirective & " );"">")
								end if

							'for passing selected caller back to parent page on Issues_edit.asp OR veiwing from side panel
							elseif lCase(editLink) = "caller-popup" and lCase(field.name) = "name" then
								'prepare location full address
								tempAddress = rs("address") & ""
								tempAddress = replace(tempAddress,"'","\'") 	'replaces all single quotes with proper Javascript \'
								tempAddress = replace(tempAddress,vbCrLf,"\n")	'replaces all line feeds with proper Javascript \n																
								response.Write("<td align=""left"" style=""cursor:pointer; " & foreColor & """ onClick=""setCaller('" & replace(rs("firstname") & "","'","\'") & "','" & replace(rs("lastname") & "","'","\'") & "','" & replace(rs("title") & "","'","\'") & "','" & tempAddress & "','" & replace(rs("city") & "","'","\'") & "','" & rs("state") & "','" & rs("zip") & "','" & replace(rs("phone_home") & "","'","\'") & "','" & replace(rs("phone_work") & "","'","\'") & "');"">")															

							'for linking issues on related issues page
							elseif lCase(editLink) = "issue-popup" and lCase(field.name) = "issue #" then
								response.Write("<td nowrap=""nowrap"" align=""left"" style=""cursor:pointer; " & foreColor & """ onClick=""window.location='popup_issue.asp?action=add&cid=" & CustomerID & "&recid=" & idIssue & "&recrelated=" & rs("issue #") & "';"">")
								
							'hyperlink on <TD> if field editLink an Field Name are the same
							'''ORIGINAL elseif lCase(editLink) = lCase(field.name) then
							'''	response.Write("<td align=""left"" style=""cursor:pointer;"" onClick=""window.location='issues.asp?showField=" & editLink & "&showValue=" & server.URLEncode(rs(field.name) & "") & "'"">")							
							elseif len(editIcon)>0 then
								response.Write("<td align=""left"" style=""cursor:pointer; " & foreColor & """ onClick=""window.location='issues.asp?pageView=" & pageView & "&showType=register&showField=" & lCase(field.name) & "&showValue=" & server.URLEncode(rs(field.name) & "") & "'"">")
												
							'hyperlink on <TD> for ALL others
							else
								response.Write("<td align=""left"" style=""" & foreColor & """>")
							end if

							'show edit icon
							'if len(editIcon) > 0 and lCase(editLink) <> "sql" then
							if len(editIcon)>0 then
								if (lCase(editLink)<>"print" and lCase(editLink)<>"sql") or (lCase(editLink)="sql" and (lCase(field.name)="crsid" or lCase(field.name)="issue #")) then
									
									'related table is shown
									'if lCase(editLink) = "related-issues" then
									'	response.Write("<img style=""padding-top:1px; padding-bottom:1px; margin-right:7px; vertical-align:middle;"" src=""../_images/icons/16/cancel_disabled.png"" title=""Open Item"" border=""0"" align=""absmiddle"">")
									'end if
									
									response.Write("<img style=""padding-top:1px; padding-bottom:1px; margin-right:5px; vertical-align:middle;"" src=""" & editIcon & """ title=""Open Item"" border=""0"" align=""absmiddle"">")
								end if
							end if
							
						'ALL OTHER colums, but 1st one
						else						
							response.Write("<td align=""left"" style=""" & foreColor & """>")
						end if					


						'-----------------------------------------
						'SHOW column VALUES nulls with empty space
						'-----------------------------------------						
						if isNull(rs(field.name)) then
							response.write "&nbsp;"
						
						'show/print values
						else

							'1ST COLUMN ONLY...turn text into Link so underline shows up...
							if xCol=0 and len(editIcon)>0 then
								if (lCase(editLink)<>"print" and lCase(editLink)<>"sql") or (lCase(editLink)="sql" and (lCase(field.name)="crsid" or lCase(field.name)="issue #")) then
									response.write("<a style=""color:#333333;"">")
								end if
							end if
							
							'status change if NOT being printed
							if len(editIcon) > 0 and (lCase(field.name) = "status" or lCase(field.name) = "investigation status" or lCase(field.name) = "issue status" or lCase(field.name) = "inv status") then
								call colorStatusBox(rs(field.name))
								response.write rs(field.name)
								
							'severity change
							elseif lCase(field.name) = "severity" then 		
								response.write( reformatSeverity(rs(field.name)) )
								
							'user change (i.e. 4101:Rainey, Bryan), take out the ID and ":"
							elseif lCase(field.name) = "user" or lCase(field.name) = "full_name" then 			
								response.write( mid(rs(field.name),instr(rs(field.name),":")+1,len(rs(field.name))) & " " )
								
							'location hierarchy change
							elseif lCase(field.name) = "hierarchy" then							
								tempHierarchy = rs("level_1") & "*|*" & rs("level_2") & "*|*" & rs("level_3") & "*|*" & rs("level_4") & "*|*" & rs("level_5") & "*|*" & rs("level_6") & "*|*" & rs("level_7") & "*|*" & rs("level_8")
								tempHierarchy = reformatHierarchy(tempHierarchy,"string")
								response.write( tempHierarchy )
								
							'for showing address correctly with carriage returns
							elseif (lCase(editLink) = "location-popup" or lCase(editLink) = "location-table") and lCase(field.name) = "address" then
								response.write( replace(rs("address"),vbCrLf,"<br>") )

							'for showing address correctly with carriage returns
							elseif lCase(editLink) = "caller-popup" and lCase(field.name) = "address" then
								response.write( replace(rs("address"),vbCrLf,"<br>") )

							'ALL others...
							else

									if (field.type = adLongVarWChar) then
										if (len(rs(field.name)) > 150) then
											response.write( left( stripHTML(rs(field.name)), 150) & "..." )
										else
											response.write( stripHTML(rs(field.name)) )
										end if 
										
									'all good...print as is
									else
										'total with numbers provided...probably from calculated report
										if lCase(field.name) = "total" then
											response.write( formatNumber(rs(field.name),0) )
										'all other fields/values
										else
											'setFriendlyValue turns True to Yes, False to No etc...
											response.write( setFriendlyValue(rs(field.name)) )
										end if										
										
									end if									
								
							end if								

							'1ST COLUMN ONLY...close Link so underline shows up...only if edit link
							if xCol=0 and len(editIcon)>0 then
								if (lCase(editLink)<>"print" and lCase(editLink)<>"sql") or (lCase(editLink)="sql" and (lCase(field.name)="crsid" or lCase(field.name)="issue #")) then
									response.write("</a>")
								end if
							end if
																												
						end if
						
						'close data column
						response.Write("</td>")
						
					end if	 
					
					xCol = xCol + 1
					'Reset for next row
					if xCol > colCount - 1 then
						xCol = 0
					end if
					
				next
				'close current row
				Response.Write "</tr>"
				
				'Move to next record
				if count > 1 then
					rs.movenext
				end if
				count = count + 1

				'set <THEAD> and <TBODY> for sortable table
				if count = 2 then
					response.Write("</thead>")
				elseif (count-2) = rs.recordcount then
					response.Write("</tbody>")
				end if
				
			loop		

			Response.Write "</table>"
			'STOP main Grid table
			'***************************
			
			'shading table at bottom of data table
			Response.Write("<table id=""shadingTable"" width=""100%"" cellpadding=""0"" cellspacing=""0"">")
            Response.Write("<tr>")
            Response.Write("<td class=""clearFormat"" style=""background:#D0D0D0"" height=""5px;""><img src=""../_images/x_cleardot.gif"" width=""1"" height=""1""></td>")
            Response.Write("</tr>")
            Response.Write("</table>")

			%>
            
            <!-- Fire table build -->
			<script language="javascript" type="text/javascript">   															
				//<![CDATA[	
				var tableProp = {<% =tableProperty %>};
				//initiate table setup
				var tf1 = setFilterGrid("<% =idTable %>",tableProp);
				//]]>
			</script>
            
			<%				

		end if
	
		'Close RecordSet
		call closeRS(rs)
	
	end if

end sub


'===============================================
' This function is based off the HTML Table/Filter
' generator published here http://tablefilter.free.fr
' visit website for configuration parameters
'===============================================
sub buildCrosstab(gridID,gridSQL,gridTitle,editLink,editIcon,tableProperty,groupBy)

'Declare variables
dim mySQL, cn, rs
dim field, count, fldValue
dim recordCount

dim gridCounter
dim colCount
dim xCol

dim bolVisible
dim tempAddress, tempHierarchy, tempDirective

dim rowCount

'Create unique table ID. This is used for
'generating table grid and paging.
dim idTable
if len(gridID) = 0 then
	randomize	
	idTable = "table_" & cLng(rnd*99)+1
else
	idTable = gridID
end if
'********************************************************************

'Open RecordSet to get row count
'''DNN - moved to counting records in Crosstab
'''set rs = openRSopen(replace(gridSQL,"^","'"),3,adOpenStatic,adLockReadOnly,adCmdText,0)

	'open crosstab recordset
'	if rs.State <> adStateClosed then
		'get row count
'		rowCount = rs.recordcount
		'''if rowCount < 1001 then

'	response.redirect "../error/default.asp?errMsg=" & server.URLEncode(listSQL)	

'	response.write(" EXEC sp_Crosstab '" & gridSQL & "', Null, Null, '" & groupBy & "', 'CRSID', 'COUNT', Null, 1 ")
'	response.End()
	


	on error resume next	
	set rs = openRSexecute(" EXEC sp_Crosstab '" & gridSQL & "', Null, Null, '" & groupBy & "', 'CRSID', 'COUNT', Null, 1 ")	
	if err.number <> 0 then
		response.write("<table width='100%' cellpadding=0 cellspacing=0>")
        response.write("<tr><td align=center valign=middle style=""padding-right:2px;"">")
        call systemMessageBox("systemMessage","statusMessageERROR","ERROR: An error occurred with your report (#" & err.number & " " & err.Description & ")")
		response.Write("</td></tr></table>")			
		set rs = openRSexecute(" SELECT CRSID FROM CRS WHERE 1=2 ")
		closeRS(rs)
	end if
	on error goto 0	

	
		'''end if
'	end if
	
	'Reset Counter
	count = 1

	if rs.State = adStateClosed then
		response.write("<table width='100%' cellpadding=0 cellspacing=0>")
        response.write("<tr><td align=center valign=middle style=""padding-right:2px;"">")
        call systemMessageBox("systemMessage","statusMessageINFO","No records matched your search criteria.")
		response.Write("</td></tr></table>")
        		
	else
	
		rowCount = rs.recordcount
	
		'no results were returned
		if rs.eof then
		
			Response.Write("<table width='100%' cellpadding=0 cellspacing=0>")
            response.write("<tr><td align=center valign=middle style=""padding-right:2px;"">")
            call systemMessageBox("systemMessage","statusMessageINFO","No records matched your search criteria.")
			response.Write("</td></tr></table>")
		
		'if called from Dashboard and more than 1,000 records don't show.
		elseif rowCount > 2000 and lCase(editLink)<>"caller-popup" then

			Response.Write("<table width='100%' cellpadding=0 cellspacing=0>")
            response.write("<tr><td align=center valign=middle style=""padding-right:2px;"">")
            call systemMessageBox("systemMessage","statusMessageINFO","<b>Over 2,000 records returned.</b> Limit the results with a filter and re-run.")
			response.Write("</td></tr></table>")
			
		'everything good, display...
		else

			'show/print grid title
			if len(gridTitle) > 0 then
				Response.Write("<table width=""100%"" cellpadding=0 cellspacing=0>")
				Response.Write("<tr><td class=""clearFormat"" align=""left"" style=""color:#35487B; font-weight:bold; padding:5px;"">" & gridTitle & "</td></tr>")
				Response.Write("</table>")
			end if
					
			for each field in rs.fields
				colCount = colCount + 1
			next	

			'***************************
			'START main Grid table
			'***************************
			
			Response.Write "<table width='100%' id='" & idTable & "' style='background-color:#969696;' cellpadding=0 cellspacing=0>"
		
			'Reset count
			xCol = 0
			
			do while not rs.eof
			
				'THEAD/TBODY on table
				if count = 1 then
					response.Write("<thead>")
				elseif count = 2 then
					response.Write("<tbody>")
				end if
				
				'TR start new row
				Response.Write "<tr>"
				for each field in rs.fields
					
					'reset column visibility...use to hide some columns
					bolVisible = true

					'use this to HIDE any unwanted columns...
					'special circumstances for dialog/popup_location
					if (lCase(editLink) = "location-popup" or lCase(editLink) = "location-table") then
						if lCase(field.name) = "city" _
						   or lCase(field.name) = "locationid" _
						   or lCase(field.name) = "nameid" _
						   or lCase(field.name) = "directiveid" _ 
						   or lCase(field.name) = "state" _
						   or lCase(field.name) = "zip" _
						   or lCase(field.name) = "country" _						   
						   or lCase(field.name) = "level_1" _
						   or lCase(field.name) = "level_2" _
						   or lCase(field.name) = "level_3" _
						   or lCase(field.name) = "level_4" _
						   or lCase(field.name) = "level_5" _
						   or lCase(field.name) = "level_6" _
						   or lCase(field.name) = "level_7" _
						   or lCase(field.name) = "level_8" _
						   or lCase(field.name) = "notes" then
							'hide column
							bolVisible = false
						end if
					end if

					'use this to HIDE any unwanted columns...
					'special circumstances for dialog/popup_location
					if lCase(editLink) = "caller-popup" then
						if lCase(field.name) = "city" _
						   or lCase(field.name) = "firstname" _
						   or lCase(field.name) = "lastname" _						
						   or lCase(field.name) = "callerid" _
						   or lCase(field.name) = "state" _
						   or lCase(field.name) = "zip" _
						   or lCase(field.name) = "phone_home" _			
						   or lCase(field.name) = "phone_work" _									   			   						   
						   or lCase(field.name) = "notes" then
							'hide column
							bolVisible = false
						end if
					end if

					if lCase(editLink) = "directive" then
						if lCase(field.name) = "directiveid" then
							'hide column
							bolVisible = false
						end if
					end if
					
					'************************************
					'TH print column HEADERS(s), first row only
					'************************************
					if count = 1 and (bolVisible = true) then

						'set first column of header row width
						response.Write("<th align=""left"" nowrap=""nowrap"">")						
							'show column names...defined in table, query or db view
							'column "ALL_COUNT" comes from sp_Crosstab setting
							if lCase(field.name) = "all_count" then
								response.Write("Total")
							else
								'show column names...defined in table, query or db view													
								if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and profileCount = 1 then
									response.write(setFriendlyName(replace(field.name,"_"," ") ,sCustomerID ,"No"))
								else
									response.Write(replace(field.name, "_", " "))
								end if
							end if
						'close column header
						response.Write("</th>")
											
					'************************************	
					'TD print field VALUE(s), all rows but 1st one
					'************************************
					elseif (bolVisible = true) then

						'FIRST COLUMN for setting edit links and icons
						if xCol = 0 then

							'hyperlink on <TD> if field "CRSID"
							if lCase(editLink)<>"print" and (lCase(field.name) = "crsid" or lCase(field.name) = "issue #") then
								response.Write("<td nowrap=""nowrap"" align=""left"" style=""cursor:pointer;"" onClick=""window.location='" & session(session("siteID") & "issuesEdit") & "?action=edit&recid=" & rs(field.name) & "'"">")

							'user change (i.e. 4101:Rainey, Bryan), take out name and ":" and only user ID
							elseif (lCase(editLink) = "full_name" and lCase(field.name) = "full_name") or (lCase(editLink) = "user" and lCase(field.name) = "user") or (lCase(editLink) = "case manager" and lCase(field.name) = "case manager") then
								response.Write("<td align=""left"" style=""cursor:pointer;"" onClick=""window.location='issues.asp?showField=show" & editLink & "&showValue=" & server.URLEncode( left(rs(field.name),instr(rs(field.name),":")-1) ) & "'"">")

							'for passing selected location back to parent page on Issues_edit.asp
							elseif lCase(editLink) = "customer-popup" and lCase(field.name) = "name" then
								response.Write("<td nowrap=""nowrap"" align=""left"" style=""cursor:pointer;"" onClick=""addIssue('" & rs("customerid") & "'," & cLng(session(session("siteID") & "adminLoggedOn")) & ")"">")

							'for passing selected table and history record to popup_hist_record.asp
							elseif inStr(lCase(editLink),"hist-999")>0 and lCase(field.name) = "history id" then
								response.Write("<td nowrap=""nowrap"" align=""left"" style=""cursor:pointer;"" onclick=""javascript:window.open('../_dialogs/popup_hist_record.asp?hist=" & editLink & "&recid=" & rs("history id") & "')""; return false;"">")
								
							elseif inStr(lCase(editLink),"hist-")>0 and lCase(field.name) = "history id" then
								response.Write("<td nowrap=""nowrap"" align=""left"" style=""cursor:pointer;"" onclick=""SimpleModal.open('../_dialogs/popup_hist_record.asp?hist=" & editLink & "&recid=" & rs("history id") & "', 500, 700, 'yes'); return false;"">")

							elseif lCase(editLink) = "directive" and lCase(field.name) = "directive" then
								response.Write("<td nowrap=""nowrap"" align=""left"" style=""cursor:pointer;"" onclick=""SimpleModal.open('../_dialogs/popup_directive_record.asp?recid=" & rs("directiveid") & "', 500, 900, 'yes'); return false;"">")
								
							'for passing selected location back to parent page on Issues_edit.asp OR viewing from side panel
							elseif (lCase(editLink) = "location-popup" or lCase(editLink) = "location-table") and lCase(field.name) = "name" then
								
								if lCase(editLink) = "location-table" then
									response.Write("<td align=""left"" style=""cursor:pointer;"" onclick=""SimpleModal.open('../_dialogs/popup_location_record.asp?recid=" & rs("locationid") & "', 525, 700, 'yes'); return false;"">")
								
								elseif lCase(editLink) = "location-popup" then

									'prepare location full address
									tempAddress = rs("address") & ""
									tempAddress = replace(tempAddress,"'","\'") 	'replaces all single quotes with proper Javascript \'
									tempAddress = replace(tempAddress,vbCrLf,"\n")	'replaces all line feeds with proper Javascript \n								
									'prepare location hierarchy
									tempHierarchy = rs("level_1") & "*|*" & rs("level_2") & "*|*" & rs("level_3") & "*|*" & rs("level_4") & "*|*" & rs("level_5") & "*|*" & rs("level_6") & "*|*" & rs("level_7") & "*|*" & rs("level_8")
									tempHierarchy = reformatHierarchy(tempHierarchy,"value")
									tempHierarchy = replace(tempHierarchy,"'","\'")	'replaces all single quotes with proper Javascript \'
									
									'no special coding for hierarch just yet
									'tempLevel_1, tempLevel_2, tempLevel_3, tempLevel_4, tempLevel_5, tempLevel_6, tempLevel_7, tempLevel_8
									
									tempDirective = rs("directiveid")
									if isNull(tempDirective) then
										tempDirective = 0
									end if															

									tempNameID = rs("nameid") & ""
									tempNameID = replace(tempNameID,"\","\\")
									tempNameID = replace(tempNameID,"'","\'")									
									tempNameID = replace(tempNameID,chr(34)," ")

									tempName = rs("name") & ""
									tempName = replace(tempName,"\","\\")
									tempName = replace(tempName,"'","\'")									
									tempName = replace(tempName,chr(34)," ")

									tempCity = rs("city") & ""
									tempCity = replace(tempCity,"\","\\")									
									tempCity = replace(tempCity,"'","\'")
									tempCity = replace(tempCity,chr(34)," ")

									tempZip = rs("zip") & ""
									tempZip = replace(tempZip,"\","\\")									
									tempZip = replace(tempZip,"'","\'")
									tempZip = replace(tempZip,chr(34)," ")

									tempCountry = rs("country") & ""
									tempCountry = replace(tempCountry,"\","\\")									
									tempCountry = replace(tempCountry,"'","\'")
									tempCountry = replace(tempCountry,chr(34)," ")

									'write output to screen and table
									response.Write("<td align=""left"" style=""cursor:pointer;"" onClick=""setLocation( encodeURI('" & tempNameID & "'), encodeURI('" & tempName & "'), encodeURI('" & tempAddress & "'), encodeURI('" & tempCity & "'),'" & rs("state") & "', encodeURI('" & tempZip & "'), encodeURI('" & tempCountry & "'), '" & tempHierarchy & "', " & tempDirective & " );"">")
								end if

							'for passing selected caller back to parent page on Issues_edit.asp OR veiwing from side panel
							elseif lCase(editLink) = "caller-popup" and lCase(field.name) = "name" then
								'prepare location full address
								'sFirst,sLast,sAddress,sCity,sState,sPostal,sHome,sWork
								tempAddress = rs("address") & ""
								tempAddress = replace(tempAddress,"'","\'") 	'replaces all single quotes with proper Javascript \'
								tempAddress = replace(tempAddress,vbCrLf,"\n")	'replaces all line feeds with proper Javascript \n																
								response.Write("<td align=""left"" style=""cursor:pointer;"" onClick=""setCaller('" & replace(rs("firstname") & "","'","\'") & "','" & replace(rs("lastname") & "","'","\'") & "','" & replace(rs("title") & "","'","\'") & "','" & tempAddress & "','" & replace(rs("city") & "","'","\'") & "','" & rs("state") & "','" & rs("zip") & "','" & replace(rs("phone_home") & "","'","\'") & "','" & replace(rs("phone_work") & "","'","\'") & "');"">")															
																
							'hyperlink on <TD> if field editLink an Field Name are the same
							'''elseif lCase(editLink) = lCase(field.name) then
							'''	response.Write("<td align=""left"" style=""cursor:pointer;"" onClick=""window.location='issues.asp?showField=" & editLink & "&showValue=" & server.URLEncode(rs(field.name) & "") & "'"">")
							elseif len(editIcon)>0 then
								response.Write("<td align=""left"" style=""cursor:pointer;"" onClick=""window.location='issues.asp?pageView=" & pageView & "&showType=register&showField=" & lCase(field.name) & "&showValue=" & server.URLEncode(rs(field.name) & "") & "'"">")
												
							'hyperlink on <TD> for ALL others
							else
								response.Write("<td align=""left"">")														
							end if

							'show edit icon
							'if len(editIcon) > 0 and lCase(editLink) <> "sql" then
							if len(editIcon)>0 then
								if (lCase(editLink)<>"print" and lCase(editLink)<>"sql") or (lCase(editLink)="sql" and (lCase(field.name)="crsid" or lCase(field.name)="issue #")) then
									response.Write("<img style=""padding-top:1px; padding-bottom:1px; margin-right:5px; vertical-align:middle;"" src=""" & editIcon & """ title=""Open Item"" border=""0"" align=""absmiddle"">")
								end if
							end if
							
						'ALL OTHER colums 2ND through --> ?? , but 1st one
						else						
							response.Write("<td align=""left"">")
						end if					
																						
						'-----------------------------------------
						'SHOW column VALUES nulls with empty space
						'-----------------------------------------						
						if isNull(rs(field.name)) then
							response.write "&nbsp;"
						
						'show/print values
						else

							'1ST COLUMN ONLY...turn text into Link so underline shows up...
							if xCol=0 and len(editIcon)>0 then
								if (lCase(editLink)<>"print" and lCase(editLink)<>"sql") or (lCase(editLink)="sql" and (lCase(field.name)="crsid" or lCase(field.name)="issue #")) then
									response.write("<a style=""color:#333333;"">")
								end if
							end if
							
							'status change if NOT being printed
							if len(editIcon) > 0 and (lCase(field.name) = "status" or lCase(field.name) = "investigation status" or lCase(field.name) = "issue status" or lCase(field.name) = "inv status") then
								call colorStatusBox(rs(field.name))
								response.write rs(field.name)
								
							'severity change
							elseif lCase(field.name) = "severity" then 		
								response.write( reformatSeverity(rs(field.name)) )
								
							'user change (i.e. 4101:Rainey, Bryan), take out the ID and ":"
							elseif lCase(field.name) = "user" or lCase(field.name) = "full_name" then 			
								response.write( mid(rs(field.name),instr(rs(field.name),":")+1,len(rs(field.name))) )
								
							'location hierarchy change
							elseif lCase(field.name) = "hierarchy" then							
								tempHierarchy = rs("level_1") & "*|*" & rs("level_2") & "*|*" & rs("level_3") & "*|*" & rs("level_4") & "*|*" & rs("level_5") & "*|*" & rs("level_6") & "*|*" & rs("level_7") & "*|*" & rs("level_8")
								tempHierarchy = reformatHierarchy(tempHierarchy,"string")
								response.write( tempHierarchy )
								
							'for showing address correctly with carriage returns
							elseif lCase(editLink) = "location-popup" and lCase(field.name) = "address" then
								response.write( replace(rs("address"),vbCrLf,"<br>") )

							'for showing address correctly with carriage returns
							elseif lCase(editLink) = "caller-popup" and lCase(field.name) = "address" then
								response.write( replace(rs("address"),vbCrLf,"<br>") )

							'ALL others...
							else
																									
								if (field.type = adLongVarWChar) then
									if (len(rs(field.name)) > 150) then
										response.write( left( stripHTML(rs(field.name)), 150) & "..." )
									else
										response.write( stripHTML(rs(field.name)) )
									end if 
									
								'all good...print as is
								else
									'total with numbers provided...probably from calculated report
									if lCase(field.name) = "all_count" then
										response.write( formatNumber(rs(field.name),0) )
									'all other fields/values
									else									
										response.write( setFriendlyValue(rs(field.name)) )										
									end if																														
										
								end if									
								
							end if								

							'1ST COLUMN ONLY...close Link so underline shows up...only if edit link
							if xCol=0 and len(editIcon)>0 then
								if (lCase(editLink)<>"print" and lCase(editLink)<>"sql") or (lCase(editLink)="sql" and (lCase(field.name)="crsid" or lCase(field.name)="issue #")) then
									response.write("</a>")
								end if
							end if
																												
						end if
						
						'close data column
						response.Write("</td>")
						
					end if	 
					
					xCol = xCol + 1
					'Reset for next row
					if xCol > colCount - 1 then
						xCol = 0
					end if
					
				next
				'close current row
				Response.Write "</tr>"
				
				'Move to next record
				if count > 1 then
					rs.movenext
				end if
				count = count + 1

				'set <THEAD> and <TBODY> for sortable table
				if count = 2 then
					response.Write("</thead>")
				elseif (count-2) = rs.recordcount then
					response.Write("</tbody>")
				end if
				
			loop		

			Response.Write "</table>"
			'STOP main Grid table
			'***************************
			
			'shading table at bottom of data table
			Response.Write("<table id=""shadingTable"" width=""100%"" cellpadding=""0"" cellspacing=""0"">")
            Response.Write("<tr>")
            Response.Write("<td class=""clearFormat"" style=""background:#D0D0D0"" height=""5px;""><img src=""../_images/x_cleardot.gif"" width=""1"" height=""1""></td>")
            Response.Write("</tr>")
            Response.Write("</table>")

			%>
            
            <!-- Fire table build -->
			<script language="javascript" type="text/javascript">   															
				//<![CDATA[	
				var tableProp = {<% =tableProperty %>};
				//initiate table setup
				var tf1 = setFilterGrid("<% =idTable %>",tableProp);
				//]]>
			</script>
            
			<%				

		end if
	
		'Close RecordSet
		call closeRS(rs)
	
	end if

end sub
%>
