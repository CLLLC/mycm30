        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Configuration div -->
        <div id="tab:configure">        
        
            <!-- START Configuration table -->   
	        <table id="table_configure" class="formTable" cellpadding="0" cellspacing="0" style="width:100%;">
				
              <tr>
                <td align="left" width="49%" valign="middle" style="padding:5px;">
                
                    <div class="subLabel" style="text-transform:uppercase;"><img src="../_images/icons/16/page.png" width="16" height="16" align="absmiddle" style="margin-right:7px; vertical-align:middle;" /><strong>Report</strong></div>
                                    					
                    <!-- THE Preview image -->
                    <% if instr(1,strParameters,"chart") then %>  
                        <div>
                            <img src="images/chart.png" width="125" height="125" align="absmiddle" style="margin-top:3px; border:solid 1px #B3B3B3; vertical-align:middle;">
                        </div>
					<% elseif instr(1,strParameters,"register") then %>  
                        <div>
                            <img src="images/register.png" width="125" height="125" align="absmiddle" style="margin-top:3px; border:solid 1px #B3B3B3; vertical-align:middle;">
                        </div>                    
                    <% end if %>


					<!-- START links to download types -->
					<div style="margin-top:10px; padding:5px; background-color:#C7CDE9;">This report can be opened in the following formats.</div>

					<div style="float:left; width:100%; background-color:#F0F0F0; border-bottom:solid 1px #969696;">
                    
                    	<% if crystalFileError = "Y" then %>
                            <!-- PDF -->	                    
                            <div style="float:left; padding:10px; width:50px; text-align:center;">                    
                            	<img src="../_images/icons/24/file_extension_pdf.png" width="24" height="24" title="Excel" /><br>Adobe (.pdf)
                            </div>                    
                            <!-- XLS -->
                            <div style="float:left; padding:10px; width:50px; text-align:center;">
                                <img src="../_images/icons/24/file_extension_xls.png" width="24" height="24" title="Excel" /><br>Excel (.xls)
                            </div>                    
                            <!-- DOC -->
                            <div style="float:left; padding:10px; width:50px; text-align:center;">
                                <img src="../_images/icons/24/file_extension_doc.png" width="24" height="24" title="Excel" /><br>Word (.doc)
                            </div>                    
                            <!-- XLS data -->
                            <div style="float:left; padding:10px; width:50px; text-align:center;">
                                <img src="../_images/icons/24/file_extension_dat.png" width="24" height="24" title="Excel" /><br>Data (.xls)
                            </div>                                            
                            
                        <% elseif cLng(sLogid) = cLng(rptLogid) then %>
                            <!-- PDF -->	                    
                            <div style="float:left; padding:10px; width:50px; text-align:center;">                    
                                <a href="#" onClick="setCrystalCookie(); runCrystal('PDF'); return false;">                    
                                <img src="../_images/icons/24/file_extension_pdf.png" width="24" height="24" title="Excel" /><br>Adobe (.pdf)
                                </a>
                            </div>                    
                            <!-- XLS -->
                            <div style="float:left; padding:10px; width:50px; text-align:center;">
                                <a href="#" onClick="setCrystalCookie(); runCrystal('Excel'); return false;"> 
                                <img src="../_images/icons/24/file_extension_xls.png" width="24" height="24" title="Excel" /><br>Excel (.xls)
                                </a>
                            </div>                    
                            <!-- DOC -->
                            <div style="float:left; padding:10px; width:50px; text-align:center;">
                                <a href="#" onClick="setCrystalCookie(); runCrystal('Word'); return false;"> 
                                <img src="../_images/icons/24/file_extension_doc.png" width="24" height="24" title="Excel" /><br>Word (.doc)
                                </a>
                            </div>                    
                            <!-- XLS data -->
                            <div style="float:left; padding:10px; width:50px; text-align:center;">
                                <a href="#" onClick="setCrystalCookie(); runCrystal('ExcelData'); return false;"> 
                                <img src="../_images/icons/24/file_extension_dat.png" width="24" height="24" title="Excel" /><br>Data (.xls)
                                </a>
                            </div>                    

                        <% else 
							'report is being view by a DIFFERNT user...cannot run right now, maybe later.
							%>
                            <!-- PDF -->	                    
                            <div style="float:left; padding:10px; width:50px; text-align:center;">                    
                                <img src="../_images/icons/24/file_extension_pdf.png" width="24" height="24" title="Excel" /><br>Adobe (.pdf)
                            </div>                    
                            <!-- XLS -->
                            <div style="float:left; padding:10px; width:50px; text-align:center;">
                                <img src="../_images/icons/24/file_extension_xls.png" width="24" height="24" title="Excel" /><br>Excel (.xls)
                            </div>                    
                            <!-- DOC -->
                            <div style="float:left; padding:10px; width:50px; text-align:center;">
                                <img src="../_images/icons/24/file_extension_doc.png" width="24" height="24" title="Excel" /><br>Word (.doc)
                            </div>                    
                            <!-- XLS data -->
                            <div style="float:left; padding:10px; width:50px; text-align:center;">
                                <img src="../_images/icons/24/file_extension_dat.png" width="24" height="24" title="Excel" /><br>Data (.xls)
                            </div>                    
                            
						<% end if %>              
                        
              		</div>
					<!-- STOP links to download types -->
                    
                </td>
                <td class="clearFormat" width="5" style="padding:0px; border:none;"><img src="../_images/x_cleardot.gif" width="1" height="1" /></td>

				<!-- START filters... -->
                <td align="left" width="50%" valign="middle" style="padding:5px;">
                    
                    <div class="subLabel" style="text-transform:uppercase;"><img src="../_images/icons/16/filter.png" width="16" height="16" align="absmiddle" style="margin-right:7px; vertical-align:middle;" /><strong>Filters</strong></div>                                  					
                    <div class="subLabel" style="margin-bottom:6px;">Set your desired criteria. <span class="required"><em>Filters are not saved.</em></span></div>
                       					
                	<table class="formTable" width="100%" border="0" cellspacing="0" cellpadding="0">

	                    <!-- REPORT TITLE Parameter -->
						<% if instr(1,strParameters,"reporttitle") then %>                        
                        <tr>
                            <td class="formLabel" style="width:100px;">Report Title:</td>
                            <td nowrap="nowrap">    
                                <div>
                                   	<div style="float:left;"><input name="reporttitle" id="reporttitle" class="inputMedium" value="" maxlength="255" /></div>
                               	</div>							  
                            </td>
                      	</tr>
						<% end if %>

	                    <!-- REPORT COMMENT Parameter -->
						<% if instr(1,strParameters,"reportcomment") then %>                        
                        <tr>
                            <td class="formLabel" style="width:100px;">Comments:</td>
                            <td nowrap="nowrap">    
                                <div>
                                   	<div style="float:left;"><textarea name="reportcomment" id="reportcomment" class="inputLong" style="width:250px; height:35px;"></textarea></div>
                               	</div>							  
                            </td>
                      	</tr>
						<% end if %>
                    
	                    <!-- DATE RANGE Parameter -->
						<% if instr(1,strParameters,"fromdate") then %>
                        <tr>
                            <td class="formLabel" style="width:100px;">Date Range:</td>
                            <td nowrap="nowrap">
    
                                <div style="float:left;">
                                    <div class="subLabel">From</div>
                                    <input name="fromDate" id="fromDate" class="inputShort" value="<% =fromStart %>" maxlength="20" />
                                </div>
                                <div>
                                    <div class="subLabel">To</div>
                                    <input name="toDate" id="toDate" class="inputShort" value="<% =toStart %>" maxlength="20" />
                                </div>
                                            
                            </td>
                      	</tr>
                        <% else ''' --> needed, but now I've disabled at all times if data param doesn't exist --> elseif crystalFileError = "Y" then %>
                        <tr>
                            <td class="formLabel" style="width:100px;">Date Range:</td>
                            <td nowrap="nowrap">
    
                                <div style="float:left;">
                                    <div class="subLabel">From</div>
                                    <input name="fromDate" id="fromDate" class="inputShort" value="<% =fromStart %>" maxlength="20" disabled="disabled" />
                                </div>
                                <div>
                                    <div class="subLabel">To</div>
                                    <input name="toDate" id="toDate" class="inputShort" value="<% =toStart %>" maxlength="20" disabled="disabled" />
                                </div>
                                            
                            </td>
                      	</tr>
						<% end if %>
                        

	                    <!-- PROFILES/CUSTOMERID Parameter -->
						<% if rptProfileCount > 1 and rptProfileCount <= 10 then %>
                        <tr>
                            <td class="formLabel" style="width:100px;"><% =accountLabelPlural %>:</td>
                            <td nowrap="nowrap">
                                <div id="field_list_container">
                                    <div id="field_list" style="width:200px; border: 1px solid #ccc; padding: 4px; height:132px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">                
                                        <%
										dim myProfiles, myProfilesID
										mySQL = "SELECT Customer.Name, Customer.CustomerID " _
											  & "	FROM Customer " _
											  & "	WHERE Canceled=0 AND " _
											  & "	 	EXISTS ( " _
											  & "			SELECT vwLogins_IssueType.CustomerID, vwLogins_IssueType.LogID " _
											  & "				FROM vwLogins_IssueType " _
											  & "				WHERE (vwLogins_IssueType.CustomerID=Customer.CustomerID) AND (vwLogins_IssueType.LogID=" & rptLogid & ") AND (vwLogins_IssueType.Active='Y') ) " _
											  & "	ORDER BY Name "
										set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                        do while not rs.eof	
											myProfiles = myProfiles & "|" & rs("Name")
											myProfilesID = myProfilesID & "," & rs("CustomerID")
                                            rs.movenext
                                        loop
										if left(myProfiles,1) = "|" then myProfiles = right(myProfiles,len(myProfiles)-1)
										if left(myProfilesID,1) = "," then myProfilesID = right(myProfilesID,len(myProfilesID)-1)
                                        call closeRS(rs)
										myProfiles = Split(myProfiles,"|")
										myProfilesID = Split(myProfilesID,",")
										if isArray(myProfiles) then
                                        	for optArrIndex = 0 to UBound(myProfiles)
                                                response.write("<div id=""field_list_item_Name"" class=""checkItem"">")
                                                response.write("	<input name=""profiles"" id=""profiles"" type=""checkbox"" value=""" & myProfilesID(optArrIndex) & """ />")
                                                response.write("	<label for=""profiles"">" & myProfiles(optArrIndex) & "</label>")
                                                response.write("</div>")
                                            next	
										end if	
                                        %>                                            
                                    </div>                                    
                                </div>                 
                                <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frmCrystal','profiles','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frmCrystal','profiles',''); return false;">None</a></div>                                        
                        	</td>
                      	</tr>
                        						
                        <% elseif rptProfileCount > 10 then %>
                        <tr>
                            <td class="formLabel" style="width:100px;"><% =accountLabelSingle %>:</td>
                            <td nowrap="nowrap">    
                                <div>
                                   	<div style="float:left;">
                                    	<input name="profilesName" id="profilesName" class="inputMedium" value="" maxlength="255" />
                                        <input name="profiles" id="profiles" type="hidden" value="" />                
                                    </div>
                                   	<div><a class="myCMbutton" href="#" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_profile.asp?pageView=custom:edit', 430, 700, 'no'); resetSystemMsg('systemMessage'); return false;"><span class="search" style="padding-right:10px;">Look up</span></a></div>
                               	</div>							  
                            </td>
                      	</tr>
						<% end if %>

	                    <!-- INDUSTRY Parameter -->
						<% if instr(1,strParameters,"industry") then 
						
							dim cusIndustry
							
							mySQL = "SELECT Industry FROM Customer " _
								  & "WHERE CustomerID = '" & customerID & "' "
							set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
							if not rs.eof then
								cusIndustry = rs("industry")							
							end if

                        	if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then 
								%>
                                <tr>
                                    <td class="formLabel" style="width:100px;">Industry:</td>
                                    <td nowrap="nowrap">    
                                        <%
                                        'Build array for dropdown selections
                                        optArr = getDropDownConfig("pIndustry")
                                        call buildSelect(optArr,cusIndustry,"industry","inputLong","Y")
                                        %>                            
                                    </td>
                                </tr>
								<% 
							else 
								%>                                
                                <tr>
                                    <td class="formLabel" style="width:100px;">Industry:</td>
                                    <td nowrap="nowrap">    
                                        <%
										response.write(cusIndustry)
                                        response.write("<input name=""industry"" id=""industry"" type=""hidden"" value=""" & cusIndustry & """ />")
                                        %>                            
                                    </td>
                                </tr>                            
								<% 
							end if 
							
						end if %>


	                    <!-- LOCATION NAME Parameter -->
						<% if instr(1,strParameters,"location_name") then %>                        
                        <tr>
                            <td class="formLabel" style="width:100px;">Location Name:</td>
                            <td nowrap="nowrap">    
                                <div>
                                   	<div style="float:left;"><input name="location_name" id="location_name" class="inputMedium" value="" maxlength="255" /></div>
                                   	<div><a class="myCMbutton" href="#" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_location.asp?pageview=<% =pageView %>&cid=<% =customerID %>', 525, 700, 'no'); resetSystemMsg('systemMessage'); return false;"><span class="search" style="padding-right:10px;">Look up</span></a></div>
                               	</div>							  
                            </td>
                      	</tr>
						<% end if %>

	                    <!-- LOCATION NAME_ID Parameter -->
						<% if instr(1,strParameters,"location_nameid") then %>                        
                        <tr>
                            <td class="formLabel" style="width:100px;">Location ID:</td>
                            <td nowrap="nowrap">    
                                <div>
                                   	<div style="float:left;"><input name="location_nameid" id="location_nameid" class="inputMedium" value="" maxlength="255" /></div>
                               	</div>							  
                            </td>
                      	</tr>
						<% end if %>

	                    <!-- CATEGORY Parameter -->
						<% if instr(1,strParameters,"category") then %>                        
                        <tr>
                            <td class="formLabel" style="width:100px;">Category:</td>
                            <td nowrap="nowrap">    
                                <div>
                                   	<div style="float:left;"><input name="category" id="category" class="inputMedium" value="" maxlength="255" /></div>
                               	</div>							  
                            </td>
                      	</tr>
						<% end if %>
                        

	                    <!-- SUB CATEGORY Parameter -->
						<% if instr(1,strParameters,"subcategory") then %>                        
                        <tr>
                            <td class="formLabel" style="width:100px;">Category (sub):</td>
                            <td nowrap="nowrap">    
                                <div>
                                   	<div style="float:left;"><input name="subcategory" id="subcategory" class="inputMedium" value="" maxlength="255" /></div>
                               	</div>							  
                            </td>
                      	</tr>
						<% end if %>
                        
						<!-- IssueType Parameter -->
						<% if instr(1,strParameters,"issuetype") then %>
                      	<tr>
                            <td class="formLabel" style="width:100px;">Issue Type:</td>
                            <td nowrap="nowrap">
                                <div id="field_list_container">
                                    <div id="field_list" style="width:200px; border: 1px solid #ccc; padding: 4px; height:132px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">                
                                        <%
										dim myTypes
										mySQL="SELECT IssueType.Name " _
											& "FROM (Customer INNER JOIN Customer_IssueType ON " _
											& "Customer.CustomerID = Customer_IssueType.CustomerID) " _
											& "INNER JOIN IssueType ON Customer_IssueType.IssueTypeID = IssueType.IssueTypeID " _
											& "WHERE  Customer_IssueType.CustomerID='" & customerID & "' AND Customer_IssueType.Active='Y' " _
                                            & "ORDER By IssueType.Name "
										set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                        do while not rs.eof	
											myTypes = myTypes & "," & rs("name")
                                            rs.movenext
                                        loop
										if left(myTypes,1) = "," then myTypes = right(myTypes,len(myTypes)-1)
                                        call closeRS(rs)
										myTypes = Split(myTypes,",")
										if isArray(myTypes) then
                                        	for optArrIndex = 0 to UBound(myTypes)
                                                response.write("<div id=""field_list_item_Name"" class=""checkItem"">")
                                                response.write("	<input name=""issuetype"" id=""issuetype"" type=""checkbox"" value=""" & myTypes(optArrIndex) & """ />")
                                                response.write("	<label for=""issuetype"">" & myTypes(optArrIndex) & "</label>")
                                                response.write("</div>")
                                            next	
										end if	
                                        %>                                            
                                    </div>                                    
                                </div>                 
                                <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frmCrystal','issuetype','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frmCrystal','issuetype',''); return false;">None</a></div>                                        
                        	</td>
                      	</tr>
						<% end if %>

						<!-- STATUS Parameter -->
						<% if instr(1,strParameters,"status") then %>
                      	<tr>
                            <td class="formLabel" style="width:100px;">Status:</td>
                            <td nowrap="nowrap">
                                <div id="field_list_container">
                                    <div id="field_list" style="width:200px; border: 1px solid #ccc; padding: 4px; height:132px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">                
                                        <%
										optArr = getDropDownOpt("pCaseStatus",customerID)
										if isArray(optArr) then
                                        	for optArrIndex = 0 to UBound(optArr)
                                                response.write("<div id=""field_list_item_Name"" class=""checkItem"">")
                                                response.write("	<input name=""status"" id=""status"" type=""checkbox"" value=""" & optArr(optArrIndex) & """ />")
                                                response.write("	<label for=""status"">" & optArr(optArrIndex) & "</label>")
                                                response.write("</div>")
                                            next	
										end if	
                                        %>
                                            
                                    </div>
                                </div>                 
                                <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frmCrystal','status','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frmCrystal','status',''); return false;">None</a></div>                                        
                        	</td>
                      	</tr>
						<% end if %>

						<!-- SOURCE Parameter -->
						<% if instr(1,strParameters,"source") then %>
                      	<tr>
                            <td class="formLabel" style="width:100px;">Source:</td>
                            <td nowrap="nowrap">
                                <div id="field_list_container">
                                    <div id="field_list" style="width:200px; border: 1px solid #ccc; padding: 4px; height:132px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">
                                        <%
										optArr = getDropDownOpt("pReportSource",customerID)
										if isArray(optArr) then
                                        	for optArrIndex = 0 to UBound(optArr)
                                                response.write("<div id=""field_list_item_Name"" class=""checkItem"">")
                                                response.write("	<input name=""source"" id=""source"" type=""checkbox"" value=""" & optArr(optArrIndex) & """ />")
                                                response.write("	<label for=""source"">" & optArr(optArrIndex) & "</label>")
                                                response.write("</div>")
                                            next	
										end if	
                                        %>
                                            
                                    </div>
                                </div>                 
                                <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frmCrystal','source','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frmCrystal','source',''); return false;">None</a></div>                                        
                        	</td>
                      	</tr>
						<% end if %>

						<!-- Location Level 1 Parameter -->
						<% if instr(1,strParameters,"location_level_1") then %>
                      	<tr>
                            <td class="formLabel" style="width:100px;">Loc. Level 1:</td>
                            <td nowrap="nowrap">
                                <div id="field_list_container">
                                    <div id="field_list" style="width:200px; border: 1px solid #ccc; padding: 4px; height:132px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">                
                                        <%
										dim myLevel_1  										

										if lCase(customerID) = "ghc" then
											mySQL="SELECT CRS.Location_Level_1 " _
												& "FROM CRS " _
												& "WHERE (CRS.CustomerID='GHC' or CRS.CustomerID='GER' or CRS.CustomerID='GDL' or CRS.CustomerID='SHG' or CRS.CustomerID='CFL' or CRS.CustomerID='GII' or CRS.CustomerID='GIL') AND CRS.Location_Level_1 > ' ' and CRS.Date >= '" & optParamDate & "' " _
												& "GROUP BY CRS.Location_Level_1 " _
												& "ORDER BY CRS.Location_Level_1 "
										else
											mySQL="SELECT CRS.Location_Level_1 " _
												& "FROM CRS " _
												& "WHERE CRS.CustomerID='" & customerID & "' AND CRS.Location_Level_1 > ' ' and CRS.Date >= '" & optParamDate & "' " _																							
												& "GROUP BY CRS.Location_Level_1 " _
												& "ORDER BY CRS.Location_Level_1 "
										end if

										set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                        do while not rs.eof	
											myLevel_1 = myLevel_1 & "," & rs("Location_Level_1")
                                            rs.movenext
                                        loop
										if left(myLevel_1,1) = "," then myLevel_1 = right(myLevel_1,len(myLevel_1)-1)
                                        call closeRS(rs)
										myLevel_1 = Split(myLevel_1,",")
										if isArray(myLevel_1) then
                                        	for optArrIndex = 0 to UBound(myLevel_1)
                                                response.write("<div id=""field_list_item_Name"" class=""checkItem"">")
                                                response.write("	<input name=""location_level_1"" id=""location_level_1"" type=""checkbox"" value=""" & myLevel_1(optArrIndex) & """ />")
                                                response.write("	<label for=""location_level_1"">" & myLevel_1(optArrIndex) & "</label>")
                                                response.write("</div>")
                                            next	
										end if	
                                        %>                                            
                                    </div>                                    
                                </div>                 
                                <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frmCrystal','location_level_1','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frmCrystal','location_level_1',''); return false;">None</a></div>                                        
                        	</td>
                      	</tr>
						<% end if %>


						<!-- Location Level 2 Parameter -->
						<% if instr(1,strParameters,"location_level_2") then %>
                      	<tr>
                            <td class="formLabel" style="width:100px;">Loc. Level 2:</td>
                            <td nowrap="nowrap">
                                <div id="field_list_container">
                                    <div id="field_list" style="width:200px; border: 1px solid #ccc; padding: 4px; height:132px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">                
                                        <%
										dim myLevel_2
																				
										if lCase(customerID) = "ghc" then
											mySQL="SELECT CRS.Location_Level_2 " _
												& "FROM CRS " _
												& "WHERE (CRS.CustomerID='GHC' or CRS.CustomerID='GER' or CRS.CustomerID='GDL' or CRS.CustomerID='SHG' or CRS.CustomerID='CFL' or CRS.CustomerID='GII' or CRS.CustomerID='GIL') AND CRS.Location_Level_2 > ' ' and CRS.Date >= '" & optParamDate & "' " _
												& "GROUP BY CRS.Location_Level_2 " _
												& "ORDER BY CRS.Location_Level_2 "
										else
											mySQL="SELECT CRS.Location_Level_2 " _
												& "FROM CRS " _
												& "WHERE CRS.CustomerID='" & customerID & "' AND CRS.Location_Level_2 > ' ' and CRS.Date >= '" & optParamDate & "' " _
												& "GROUP BY CRS.Location_Level_2 " _
												& "ORDER BY CRS.Location_Level_2 "
										end if																				
										set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
										
                                        do while not rs.eof	
											myLevel_2 = myLevel_2 & "," & rs("Location_Level_2")
                                            rs.movenext
                                        loop
										if left(myLevel_2,1) = "," then myLevel_2 = right(myLevel_2,len(myLevel_2)-1)
                                        call closeRS(rs)
										myLevel_2 = Split(myLevel_2,",")
										if isArray(myLevel_2) then
                                        	for optArrIndex = 0 to UBound(myLevel_2)
                                                response.write("<div id=""field_list_item_Name"" class=""checkItem"">")
                                                response.write("	<input name=""location_level_2"" id=""location_level_2"" type=""checkbox"" value=""" & myLevel_2(optArrIndex) & """ />")
                                                response.write("	<label for=""location_level_2"">" & myLevel_2(optArrIndex) & "</label>")
                                                response.write("</div>")
                                            next	
										end if	
                                        %>                                            
                                    </div>                                    
                                </div>                 
                                <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frmCrystal','location_level_2','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frmCrystal','location_level_2',''); return false;">None</a></div>                                        
                        	</td>
                      	</tr>
						<% end if %>


						<!-- Location Level 3 Parameter -->
						<% if instr(1,strParameters,"location_level_3") then %>
                      	<tr>
                            <td class="formLabel" style="width:100px;">Loc. Level 3:</td>
                            <td nowrap="nowrap">
                                <div id="field_list_container">
                                    <div id="field_list" style="width:200px; border: 1px solid #ccc; padding: 4px; height:132px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">                
                                        <%
										dim myLevel_3

										if lCase(customerID) = "ghc" then
											mySQL="SELECT CRS.Location_Level_3 " _
												& "FROM CRS " _
												& "WHERE (CRS.CustomerID='GHC' or CRS.CustomerID='GER' or CRS.CustomerID='GDL' or CRS.CustomerID='SHG' or CRS.CustomerID='CFL' or CRS.CustomerID='GII' or CRS.CustomerID='GIL') AND CRS.Location_Level_3 > ' ' and CRS.Date >= '" & optParamDate & "' " _
												& "GROUP BY CRS.Location_Level_3 " _
												& "ORDER BY CRS.Location_Level_3 "
										else
											mySQL="SELECT CRS.Location_Level_3 " _
												& "FROM CRS " _
												& "WHERE CRS.CustomerID='" & customerID & "' AND CRS.Location_Level_3 > ' ' and CRS.Date >= '" & optParamDate & "' " _
												& "GROUP BY CRS.Location_Level_3 " _
												& "ORDER BY CRS.Location_Level_3 "
										end if
										set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
										
                                        do while not rs.eof	
											myLevel_3 = myLevel_3 & "," & rs("Location_Level_3")
                                            rs.movenext
                                        loop
										if left(myLevel_3,1) = "," then myLevel_3 = right(myLevel_3,len(myLevel_3)-1)
                                        call closeRS(rs)
										myLevel_3 = Split(myLevel_3,",")
										if isArray(myLevel_3) then
                                        	for optArrIndex = 0 to UBound(myLevel_3)
                                                response.write("<div id=""field_list_item_Name"" class=""checkItem"">")
                                                response.write("	<input name=""location_level_3"" id=""location_level_3"" type=""checkbox"" value=""" & myLevel_3(optArrIndex) & """ />")
                                                response.write("	<label for=""location_level_3"">" & myLevel_3(optArrIndex) & "</label>")
                                                response.write("</div>")
                                            next	
										end if	
                                        %>                                            
                                    </div>                                    
                                </div>                 
                                <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frmCrystal','location_level_3','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frmCrystal','location_level_3',''); return false;">None</a></div>                                        
                        	</td>
                      	</tr>
						<% end if %>

						<!-- Location Level 4 Parameter -->
						<% if instr(1,strParameters,"location_level_4") then %>
                      	<tr>
                            <td class="formLabel" style="width:100px;">Loc. Level 4:</td>
                            <td nowrap="nowrap">
                                <div id="field_list_container">
                                    <div id="field_list" style="width:200px; border: 1px solid #ccc; padding: 4px; height:132px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">                
                                        <%
										dim myLevel_4										
										
										if lCase(customerID) = "ghc" then
											mySQL="SELECT CRS.Location_Level_4 " _
												& "FROM CRS " _
												& "WHERE (CRS.CustomerID='GHC' or CRS.CustomerID='GER' or CRS.CustomerID='GDL' or CRS.CustomerID='SHG' or CRS.CustomerID='CFL' or CRS.CustomerID='GII' or CRS.CustomerID='GIL') AND CRS.Location_Level_4 > ' ' and CRS.Date >= '" & optParamDate & "' " _																							
												& "GROUP BY CRS.Location_Level_4 " _
												& "ORDER BY CRS.Location_Level_4 "
										else
											mySQL="SELECT CRS.Location_Level_4 " _
												& "FROM CRS " _
												& "WHERE CRS.CustomerID='" & customerID & "' AND CRS.Location_Level_4 > ' ' and CRS.Date >= '" & optParamDate & "' " _																							
												& "GROUP BY CRS.Location_Level_4 " _
												& "ORDER BY CRS.Location_Level_4 "
										end if
										set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
										
                                        do while not rs.eof	
											myLevel_4 = myLevel_4 & "," & rs("Location_Level_4")
                                            rs.movenext
                                        loop
										if left(myLevel_4,1) = "," then myLevel_4 = right(myLevel_4,len(myLevel_4)-1)
                                        call closeRS(rs)
										myLevel_4 = Split(myLevel_4,",")
										if isArray(myLevel_4) then
                                        	for optArrIndex = 0 to UBound(myLevel_4)
                                                response.write("<div id=""field_list_item_Name"" class=""checkItem"">")
                                                response.write("	<input name=""location_level_4"" id=""location_level_4"" type=""checkbox"" value=""" & myLevel_4(optArrIndex) & """ />")
                                                response.write("	<label for=""location_level_4"">" & myLevel_4(optArrIndex) & "</label>")
                                                response.write("</div>")
                                            next	
										end if	
                                        %>                                            
                                    </div>                                    
                                </div>                 
                                <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frmCrystal','location_level_4','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frmCrystal','location_level_4',''); return false;">None</a></div>                                        
                        	</td>
                      	</tr>
						<% end if %>


						<!-- Location Level 5 Parameter -->
						<% if instr(1,strParameters,"location_level_5") then %>
                      	<tr>
                            <td class="formLabel" style="width:100px;">Loc. Level 4:</td>
                            <td nowrap="nowrap">
                                <div id="field_list_container">
                                    <div id="field_list" style="width:200px; border: 1px solid #ccc; padding: 4px; height:132px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">                
                                        <%
										dim myLevel_5										
										
										if lCase(customerID) = "ghc" then
											mySQL="SELECT CRS.location_level_5 " _
												& "FROM CRS " _
												& "WHERE (CRS.CustomerID='GHC' or CRS.CustomerID='GER' or CRS.CustomerID='GDL' or CRS.CustomerID='SHG' or CRS.CustomerID='CFL' or CRS.CustomerID='GII' or CRS.CustomerID='GIL') AND CRS.location_level_5 > ' ' and CRS.Date >= '" & optParamDate & "' " _																							
												& "GROUP BY CRS.location_level_5 " _
												& "ORDER BY CRS.location_level_5 "
										else
											mySQL="SELECT CRS.location_level_5 " _
												& "FROM CRS " _
												& "WHERE CRS.CustomerID='" & customerID & "' AND CRS.location_level_5 > ' ' and CRS.Date >= '" & optParamDate & "' " _																							
												& "GROUP BY CRS.location_level_5 " _
												& "ORDER BY CRS.location_level_5 "
										end if
										set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
										
                                        do while not rs.eof	
											myLevel_5 = myLevel_5 & "," & rs("location_level_5")
                                            rs.movenext
                                        loop
										if left(myLevel_5,1) = "," then myLevel_5 = right(myLevel_5,len(myLevel_5)-1)
                                        call closeRS(rs)
										myLevel_5 = Split(myLevel_5,",")
										if isArray(myLevel_5) then
                                        	for optArrIndex = 0 to UBound(myLevel_5)
                                                response.write("<div id=""field_list_item_Name"" class=""checkItem"">")
                                                response.write("	<input name=""location_level_5"" id=""location_level_5"" type=""checkbox"" value=""" & myLevel_5(optArrIndex) & """ />")
                                                response.write("	<label for=""location_level_5"">" & myLevel_5(optArrIndex) & "</label>")
                                                response.write("</div>")
                                            next	
										end if	
                                        %>                                            
                                    </div>                                    
                                </div>                 
                                <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frmCrystal','location_level_5','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frmCrystal','location_level_5',''); return false;">None</a></div>                                        
                        	</td>
                      	</tr>
						<% end if %>


						<!-- Location Level 6 Parameter -->
						<% if instr(1,strParameters,"location_level_6") then %>
                      	<tr>
                            <td class="formLabel" style="width:100px;">Loc. Level 4:</td>
                            <td nowrap="nowrap">
                                <div id="field_list_container">
                                    <div id="field_list" style="width:200px; border: 1px solid #ccc; padding: 4px; height:132px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">                
                                        <%
										dim myLevel_6										
										
										if lCase(customerID) = "ghc" then
											mySQL="SELECT CRS.Location_Level_6 " _
												& "FROM CRS " _
												& "WHERE (CRS.CustomerID='GHC' or CRS.CustomerID='GER' or CRS.CustomerID='GDL' or CRS.CustomerID='SHG' or CRS.CustomerID='CFL' or CRS.CustomerID='GII' or CRS.CustomerID='GIL') AND CRS.Location_Level_6 > ' ' and CRS.Date >= '" & optParamDate & "' " _																							
												& "GROUP BY CRS.Location_Level_6 " _
												& "ORDER BY CRS.Location_Level_6 "
										else
											mySQL="SELECT CRS.Location_Level_6 " _
												& "FROM CRS " _
												& "WHERE CRS.CustomerID='" & customerID & "' AND CRS.Location_Level_6 > ' ' and CRS.Date >= '" & optParamDate & "' " _																							
												& "GROUP BY CRS.Location_Level_6 " _
												& "ORDER BY CRS.Location_Level_6 "
										end if
										set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
										
                                        do while not rs.eof	
											myLevel_6 = myLevel_6 & "," & rs("Location_Level_6")
                                            rs.movenext
                                        loop
										if left(myLevel_6,1) = "," then myLevel_6 = right(myLevel_6,len(myLevel_6)-1)
                                        call closeRS(rs)
										myLevel_6 = Split(myLevel_6,",")
										if isArray(myLevel_6) then
                                        	for optArrIndex = 0 to UBound(myLevel_6)
                                                response.write("<div id=""field_list_item_Name"" class=""checkItem"">")
                                                response.write("	<input name=""location_level_6"" id=""location_level_6"" type=""checkbox"" value=""" & myLevel_6(optArrIndex) & """ />")
                                                response.write("	<label for=""location_level_6"">" & myLevel_6(optArrIndex) & "</label>")
                                                response.write("</div>")
                                            next	
										end if	
                                        %>                                            
                                    </div>                                    
                                </div>                 
                                <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frmCrystal','location_level_6','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frmCrystal','location_level_6',''); return false;">None</a></div>                                        
                        	</td>
                      	</tr>
						<% end if %>


						<!-- Location Level 7 Parameter -->
						<% if instr(1,strParameters,"location_level_7") then %>
                      	<tr>
                            <td class="formLabel" style="width:100px;">Loc. Level 7:</td>
                            <td nowrap="nowrap">
                                <div id="field_list_container">
                                    <div id="field_list" style="width:200px; border: 1px solid #ccc; padding: 4px; height:132px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">                
                                        <%
										dim myLevel_7										
										
										if lCase(customerID) = "ghc" then
											mySQL="SELECT CRS.location_level_7 " _
												& "FROM CRS " _
												& "WHERE (CRS.CustomerID='GHC' or CRS.CustomerID='GER' or CRS.CustomerID='GDL' or CRS.CustomerID='SHG' or CRS.CustomerID='CFL' or CRS.CustomerID='GII' or CRS.CustomerID='GIL') AND CRS.location_level_7 > ' ' and CRS.Date >= '" & optParamDate & "' " _																							
												& "GROUP BY CRS.location_level_7 " _
												& "ORDER BY CRS.location_level_7 "
										else
											mySQL="SELECT CRS.location_level_7 " _
												& "FROM CRS " _
												& "WHERE CRS.CustomerID='" & customerID & "' AND CRS.location_level_7 > ' ' and CRS.Date >= '" & optParamDate & "' " _																							
												& "GROUP BY CRS.location_level_7 " _
												& "ORDER BY CRS.location_level_7 "
										end if
										set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
										
                                        do while not rs.eof	
											myLevel_7 = myLevel_7 & "," & rs("location_level_7")
                                            rs.movenext
                                        loop
										if left(myLevel_7,1) = "," then myLevel_7 = right(myLevel_7,len(myLevel_7)-1)
                                        call closeRS(rs)
										myLevel_7 = Split(myLevel_7,",")
										if isArray(myLevel_7) then
                                        	for optArrIndex = 0 to UBound(myLevel_7)
                                                response.write("<div id=""field_list_item_Name"" class=""checkItem"">")
                                                response.write("	<input name=""location_level_7"" id=""location_level_7"" type=""checkbox"" value=""" & myLevel_7(optArrIndex) & """ />")
                                                response.write("	<label for=""location_level_7"">" & myLevel_7(optArrIndex) & "</label>")
                                                response.write("</div>")
                                            next	
										end if	
                                        %>                                            
                                    </div>                                    
                                </div>                 
                                <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frmCrystal','location_level_7','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frmCrystal','location_level_7',''); return false;">None</a></div>                                        
                        	</td>
                      	</tr>
						<% end if %>


						<!-- SEVERITY Parameter -->
						<% if instr(1,strParameters,"severity") then %>
                      	<tr>
                            <td class="formLabel" style="width:100px;">Severity:</td>
                            <td nowrap="nowrap">
                                <div id="field_list_container">
                                    <div id="field_list" style="width:200px; border: 1px solid #ccc; padding: 4px; height:75px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">                
                                    	
                                        <div id="field_list_item_Name" class="checkItem">
											<input name="severity" id="severity" type="checkbox" value="1" />
                                            <label for="rptSeverity">1-High</label>
										</div>
                                        <div id="field_list_item_Name" class="checkItem">
                                        	<input name="severity" id="severity" type="checkbox" value="2" />
                                          	<label for="rptSeverity">2-Med</label>
                                     	</div>
                                     	<div id="field_list_item_Name" class="checkItem">
                                        	<input name="severity" id="severity" type="checkbox" value="3" />
                                        	<label for="rptSeverity">3-Low</label>
                                      	</div>

                                    </div>
                                </div>                 
                                <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frmCrystal','severity','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frmCrystal','severity',''); return false;">None</a></div>                                        
                        	</td>
                      	</tr>
						<% end if %>

						<!-- pActionTaken Parameter -->
						<% if instr(1,strParameters,"actiontaken") then %>
                      	<tr>
                            <td class="formLabel" style="width:100px;">Action taken:</td>
                            <td nowrap="nowrap">
                                <div id="field_list_container">
                                    <div id="field_list" style="width:200px; border: 1px solid #ccc; padding: 4px; height:132px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">                
                                        <%
										optArr = getDropDownOpt("pActionTaken",customerID)
										if isArray(optArr) then
                                        	for optArrIndex = 0 to UBound(optArr)
                                                response.write("<div id=""field_list_item_Name"" class=""checkItem"">")
                                                response.write("	<input name=""actiontaken"" id=""actiontaken"" type=""checkbox"" value=""" & optArr(optArrIndex) & """ />")
                                                response.write("	<label for=""actiontaken"">" & optArr(optArrIndex) & "</label>")
                                                response.write("</div>")
                                            next	
										end if	
                                        %>
                                            
                                    </div>
                                </div>                 
                                <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frmCrystal','actiontaken','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frmCrystal','actiontaken',''); return false;">None</a></div>                                        
                        	</td>
                      	</tr>
						<% end if %>

						<!-- pCallerType Parameter -->
						<% if instr(1,strParameters,"callertype") then %>
                      	<tr>
                            <td class="formLabel" style="width:100px;">Relationship:</td>
                            <td nowrap="nowrap">
                                <div id="field_list_container">
                                    <div id="field_list" style="width:200px; border: 1px solid #ccc; padding: 4px; height:132px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">                
                                        <%
										optArr = getDropDownOpt("pCallerType",customerID)
										if isArray(optArr) then
                                        	for optArrIndex = 0 to UBound(optArr)
                                                response.write("<div id=""field_list_item_Name"" class=""checkItem"">")
                                                response.write("	<input name=""callertype"" id=""callertype"" type=""checkbox"" value=""" & optArr(optArrIndex) & """ />")
                                                response.write("	<label for=""callertype"">" & optArr(optArrIndex) & "</label>")
                                                response.write("</div>")
                                            next	
										end if	
                                        %>
                                            
                                    </div>
                                </div>                 
                                <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frmCrystal','callertype','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frmCrystal','callertype',''); return false;">None</a></div>                                        
                        	</td>
                      	</tr>
						<% end if %>

						<!-- pCommTool Parameter -->
						<% if instr(1,strParameters,"communicationtool") then %>
                      	<tr>
                            <td class="formLabel" style="width:100px;">Awareness:</td>
                            <td nowrap="nowrap">
                                <div id="field_list_container">
                                    <div id="field_list" style="width:200px; border: 1px solid #ccc; padding: 4px; height:132px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">                
                                        <%
										optArr = getDropDownOpt("pCommTool",customerID)
										if isArray(optArr) then
                                        	for optArrIndex = 0 to UBound(optArr)
                                                response.write("<div id=""field_list_item_Name"" class=""checkItem"">")
                                                response.write("	<input name=""communicationtool"" id=""communicationtool"" type=""checkbox"" value=""" & optArr(optArrIndex) & """ />")
                                                response.write("	<label for=""communicationtool"">" & optArr(optArrIndex) & "</label>")
                                                response.write("</div>")
                                            next	
										end if	
                                        %>
                                            
                                    </div>
                                </div>                 
                                <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frmCrystal','communicationtool','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frmCrystal','communicationtool',''); return false;">None</a></div>                                        
                        	</td>
                      	</tr>
						<% end if %>


						<!-- Validity/Substantiated Parameter -->
						<% if instr(1,strParameters,"validity") then %>
                      	<tr>
                            <td class="formLabel" style="width:100px;">Validity:</td>
                            <td nowrap="nowrap">
                                <div id="field_list_container">
                                    <div id="field_list" style="width:200px; border: 1px solid #ccc; padding: 4px; height:132px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">                
                                        <%
										optArr = getDropDownOpt("pValidity",customerID)
										if isArray(optArr) then
                                        	for optArrIndex = 0 to UBound(optArr)
                                                response.write("<div id=""field_list_item_Name"" class=""checkItem"">")
                                                response.write("	<input name=""validity"" id=""validity"" type=""checkbox"" value=""" & optArr(optArrIndex) & """ />")
                                                response.write("	<label for=""validity"">" & optArr(optArrIndex) & "</label>")
                                                response.write("</div>")
                                            next	
										end if	
                                        %>
                                            
                                    </div>
                                </div>                 
                                <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frmCrystal','validity','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frmCrystal','validity',''); return false;">None</a></div>                                        
                        	</td>
                      	</tr>
						<% end if %>


						<!-- RISK LEVEL Parameter -->
						<% if instr(1,strParameters,"risklevel") then %>
                      	<tr>
                            <td class="formLabel" style="width:100px;">Risk Level:</td>
                            <td nowrap="nowrap">
                                <div id="field_list_container">
                                    <div id="field_list" style="width:200px; border: 1px solid #ccc; padding: 4px; height:132px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">                
                                        <%
										optArr = getDropDownOpt("pRiskLevel",customerID)
										if isArray(optArr) then
                                        	for optArrIndex = 0 to UBound(optArr)
                                                response.write("<div id=""field_list_item_Name"" class=""checkItem"">")
                                                response.write("	<input name=""risklevel"" id=""risklevel"" type=""checkbox"" value=""" & optArr(optArrIndex) & """ />")
                                                response.write("	<label for=""risklevel"">" & optArr(optArrIndex) & "</label>")
                                                response.write("</div>")
                                            next	
										end if	
                                        %>
                                            
                                    </div>
                                </div>                 
                                <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frmCrystal','risklevel','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frmCrystal','risklevel',''); return false;">None</a></div>                                        
                        	</td>
                      	</tr>
						<% end if %>
                                                
                    </table>

                </td>                                
                
              </tr>

            
            </table>
            <!-- END Issues Details table -->
	
    	</div>
		<!-- STOP Details div -->        

