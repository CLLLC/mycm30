        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<%
       	dim labelX
		dim arrDisabled
		dim labelTemp
		dim valueTemp
		%>

		<!-- START Account div -->
        <div id="tab:details">
        
        
            <!-- START Group Detail table -->   
	        <table id="table_details" cellpadding="0" cellspacing="0" style="width:100%;">

                <tr>
                    
                    <td valign="top">
                                        
					  	<!-- START Details Form Fields -->	                    
						<table id="table:details:general" class="formTable" width="100%" ellpadding="0" cellspacing="0">             
                                      				
                       		<tr>                            
								<td class="formLabel"><span class="required">*</span>Name:</td>
								<td align=left>
                                	<div style="float:left;">
	                                	<input name="optName" onkeyup="updateTitleText(this,'titleText','E-mail:&nbsp;')" class="inputLong" style="width:285px;" value="<% =optName %>" maxlength="150" /> 
                                    </div>
                                    <div>
	                                    <a class="myCMbutton" href="#" title="Send Login" onclick="this.blur(); sendTest(); return false;"><span class="emailsend" style="padding-right:5px;">Send Test</span></a> 
                                   	</div>
                                </td>
                            </tr>

                            <tr>
                            	<td class="formLabel">Template:</td>
								<td align=left><textarea name="optTemplate" id="optTemplate" style="width:95%; height:300px; margin-bottom:12px;" ><%=server.HTMLEncode(optTemplate & "")%></textarea></td>                          
                            </tr>

                            <tr>
                            	<td class="formLabel">&nbsp;</td>
								<td align=left>

                                	<table>
										<tr>
                                           	<td class="clearFormat">
                                                <strong>Issue replacement tags:</strong>
                                                <table>
                                                    <tr><td class="clearFormat" style="padding-top:5px;">|CRS.Name|</td><td class="clearFormat" style="padding-top:5px; padding-left:5px;">Issue # (1201-DEMO-10001-01)</td></tr>
                                                    <tr><td class="clearFormat" style="padding-top:5px;">|Logins.Email|</td><td class="clearFormat" style="padding-top:5px; padding-left:5px;">Assigned user's e-mail address</td></tr>                                        
                                                    <tr><td class="clearFormat" style="padding-top:5px;">|URL.Link|</td><td class="clearFormat" style="padding-top:5px; padding-left:5px;">Valid URL link to referenced issue</td></tr>
                                                    <tr><td class="clearFormat" style="padding-top:5px;">|CRS.Date|</td><td class="clearFormat" style="padding-top:5px; padding-left:5px;">Issue date and time</td></tr>                                                                                            
                                                    <tr><td class="clearFormat" style="padding-top:5px;">|CRS.Severity|</td><td class="clearFormat" style="padding-top:5px; padding-left:5px;">Issue severity level</td></tr>                                                                                                                                                
                                                    <tr><td class="clearFormat" style="padding-top:5px;">|CRS.Status|</td><td class="clearFormat" style="padding-top:5px; padding-left:5px;">Issue status found under Case Notes</td></tr>                                        
													<tr><td class="clearFormat" style="padding-top:5px;">|CRS.LocationName|</td><td class="clearFormat" style="padding-top:5px; padding-left:5px;">Location Name associated with issue</td></tr>                                                    
                                                    <tr><td class="clearFormat" style="padding-top:5px;">|CRS.Location1|</td><td class="clearFormat" style="padding-top:5px; padding-left:5px;">Location Level 1</td></tr>
                                                    <tr><td class="clearFormat" style="padding-top:5px;">|CRS.Location2|</td><td class="clearFormat" style="padding-top:5px; padding-left:5px;">Location Level 2</td></tr>
                                                    <tr><td class="clearFormat" style="padding-top:5px;">|CRS.Location3|</td><td class="clearFormat" style="padding-top:5px; padding-left:5px;">Location Level 3</td></tr>                                                                                                        
                                                    <tr><td class="clearFormat" style="padding-top:5px;">|CRS.Summary|</td><td class="clearFormat" style="padding-top:5px; padding-left:5px;">Issue summary</td></tr>
                                                    <tr><td class="clearFormat" style="padding-top:5px;">|CRS.Details|</td><td class="clearFormat" style="padding-top:5px; padding-left:5px;">Issue details</td></tr>                                                                                            
                                                </table>                                            
                                            </td>
                                        	<td class="clearFormat" style="padding-left:20px;">
                                                <strong>Investigation replacement tags:</strong>
                                                <table>
                                                	<tr><td colspan="2" class="clearFormat" style="padding-top:5px;">All Issue replacement tags, plus...</td><td class="clearFormat" style="padding-top:5px; padding-left:5px;">&nbsp;</td></tr>
                                                    <tr><td class="clearFormat" style="padding-top:5px;">|Investigation.Name|</td><td class="clearFormat" style="padding-top:5px; padding-left:5px;">Name given to investigation</td></tr>
                                                    <tr><td class="clearFormat" style="padding-top:5px;">|Investigation.Manager|</td><td class="clearFormat" style="padding-top:5px; padding-left:5px;">Investigator assigned to investigation</td></tr>                                        
                                                    <tr><td class="clearFormat" style="padding-top:5px;">|Investigation.Status|</td><td class="clearFormat" style="padding-top:5px; padding-left:5px;">Investigation  status</td></tr>                                        
                                                    <tr><td class="clearFormat" style="padding-top:5px;">|Investigation.Category|</td><td class="clearFormat" style="padding-top:5px; padding-left:5px;">Category linked to investigation</td></tr>                                       
                                                </table>                                            
                                            </td>
                                        </tr>
                                    </table>
                                
                                </td>                          
                            </tr>
                            
                        </table>
   						<!-- END Details Form Fields -->
                        
                    </td>
                    
                </tr>
            
            </table>
            <!-- END User Account table -->
	
    	</div>
		<!-- STOP Account div -->        


