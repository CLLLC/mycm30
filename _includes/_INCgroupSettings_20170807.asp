        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Settings div -->
        <div id="tab:settings" style="display:none;">        
        
            <!-- START Settings table -->   
	        <table id="table_settings" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>

	                <!-- START Side Page Tabs [genearl] "id=table:notes:general" -->
                    <!-- [this = object makeing the call ] -->
                    <!-- [notes = current top tab tab ] -->
                    <!-- [information = selected side tab ] -->
                    <td rowspan="4" valign="top" style="width:100px; border-top:none; padding:0px; ">
                        <div class="tabSide" style="width:100px;">
                            <div id="permissions" class="on" onClick="clickSideTab('settings','permissions');">Permissions</div>
                            <div id="issue" class="off" onClick="clickSideTab('settings','issue');">Details</div>
                            <div id="casenotes" class="off" onClick="clickSideTab('settings','casenotes');">Case Notes</div>
                            <div class="empty">&nbsp;</div>
                        </div>                        
                    </td>
                    <!-- STOP Side Page Tabs -->

                    <!-- START Middle Section -->                        
                    <td valign="top">
                                        
					  	<!-- START Permissions Form Fields -->	                    
						<table id="table:settings:permissions" width="100%" class="formTable" cellpadding="0" cellspacing="0">                           

                            <!-- START Issue Types Table -->    
                            <tr>
								<td class="formLabel">Issue Types:</td>
								<td align="left"> 
	                                <div class="subLabel" style="padding-bottom:5px;">Defines what issue types group members will be able to access.</div>
                                    <div id="field_list_container">
                                        <div id="field_list" style="width:300px; border: 1px solid #ccc; padding: 2px; height:60px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">
                                            <!-- table used for javascript slider -->
                                            <table id="table:field:list" width="95%" border="0" cellspacing="0" cellpadding="0">
                                                <%
                                                dim arrIssues, checkIssue
                                                                            
                                                'find all issue types already assigned to category
                                                if lCase(action) = "edit" then
                                                    mySQL = "SELECT Groups_IssueType.GroupID, Groups_IssueType.CustIssueTypeID " _
                                                          & "	FROM Groups_IssueType " _
                                                          & "	WHERE Groups_IssueType.GroupID=" & recId & " "
                                                    set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                                    do while not rs.eof
                                                        arrIssues = arrIssues & "|" & rs("CustIssueTypeID")
                                                        rs.movenext
                                                    loop
                                                    arrIssues = arrIssues & "|"
                                                    call closeRS(rs)
                                                end if
                                                
                                                'find all issue types for this customer
                                                mySQL = "SELECT Customer_IssueType.CustIssueTypeID, Customer_IssueType.CustomerID, IssueType.Name " _
                                                      & "	FROM Customer_IssueType INNER JOIN IssueType ON Customer_IssueType.IssueTypeID =IssueType.IssueTypeID " _
                                                      & "	WHERE Customer_IssueType.CustomerID='" & customerID & "' AND Active='Y' " _
                                                      & "	ORDER BY IssueType.Name "													
                                                set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                                do while not rs.eof
													'marked CHECK if already selected or group is being ADDED
                                                    if (instr(arrIssues, "|" & rs("CustIssueTypeID") & "|") > 0) or (action = "add") then
                                                        checkIssue = " checked "
                                                    else
                                                        checkIssue = " "
                                                    end if								
                                                    response.write("<tr><td class=""clearFormat"" style=""border:none; padding:0px; margin0px;"" align=""left"">")                                                    
                                                    response.write("	<div id=""field_list_item_Name"" class=""checkItem"">")											
                                                    response.write("		<input name=""list_checkBox"" id=""field_list_checkBox"" type=""checkbox"" style=""cursor:default;"" value=""" & rs("CustIssueTypeID") & """ " & checkIssue & " />")
                                                    response.write("		<label for=""field_list_checkBox"" style=""cursor:move;"" onclick=""javascript:return false;"">" & rs("name") & "</label>")
                                                    response.write("	</div>")    
                                                    response.write("</td></tr>")          
                    
                                                    rs.movenext
                                                loop
                                                call closeRS(rs)
                                                %>
                                            </table>
                                            <!-- Initiate listOrder.js for moving fields in order -->
                                            <script type="text/javascript">
                                                var table = document.getElementById('table:field:list');
                                                var tableDnD = new TableDnD();
                                                tableDnD.init(table);
                                            </script>
                                        </div>
                                    </div>
                    
                                    <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frm','field_list_checkBox','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frm','list_checkBox',''); return false;">None</a></div>

								</td>
                            </tr>                            
							<!-- STOP Issue Type table-->

							<!-- START default view -->
                            <tr>                    
								<td class="formLabel" style="padding-top:10px;">Issue Security :</td>
								<td align=left>
                                <div id="optDocuments">
                                	<div>
                                  		<label>
                                     		<input type="radio" name="optReadOnly" id="optReadOnlyNo" value="" style="vertical-align:middle;" <%=checkRadio(optReadOnly,"N")%>>Full Access&nbsp;&nbsp;
                                      	</label>			      
                                     	<label>
                                     		<input type="radio" name="optReadOnly" id="optReadOnlyYes" value="Y" style="vertical-align:middle;" <%=checkRadio(optReadOnly,"Y")%>>Read Only
                                     	</label>                                    
                                    </div>
									<div class="subLabel" style="padding-top:5px;">Sets default security on assigned issues. Security can be modified/changed per-issue.</div>
                                </div>
                                </td>
                            </tr>
							<!-- STOP default view -->
                            
							<tr>
                          		<td class="formLabel">&nbsp;</td>
                          		<td align=left nowrap>&nbsp;</td>
                        	</tr>
                                    
						</table>
   						<!-- END Settings Form Fields -->


					  	<!-- START Details Form Fields -->	                    
                        <table id="table:settings:issue" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">                 

                            <tr>                    
								<td class="formLabel" style="padding-top:10px;">Summary:</td>
								<td align=left>
                                <div id="optSummary">
                                	<div>
                                  		<label>
                                     		<input type="radio" name="optEditDetailsSummary" id="optEditDetailsSummaryYes" value="Y" style="vertical-align:middle;" <%=checkRadio(optEditDetailsSummary,"Y")%> >Modify&nbsp;&nbsp;
                                      	</label>			      
                                     	<label>
                                     		<input type="radio" name="optEditDetailsSummary" id="optEditDetailsSummaryNo" value="N" style="vertical-align:middle;" <%=checkRadio(optEditDetailsSummary,"N")%>>Locked
                                     	</label>
                                    </div>
									<div class="subLabel" style="padding-top:5px;">Sets group access to Summary, Details and Addendum fields within 'Summary' side tab.</div>
                                </div>
                                </td>
                            </tr>

							<tr>
                          		<td class="formLabel">&nbsp;</td>
                          		<td align=left nowrap>&nbsp;</td>
                        	</tr>
                                                                      
						</table>
   						<!-- END Details Form Fields -->



					  	<!-- START Case Notes Form Fields -->	                    
					  	<table id="table:settings:casenotes" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">     


							<tr>
                          		<td class="formLabel" style="background-color:#FFFFC8;">Case Notes:</td>
                          		<td align=left nowrap style="background-color:#FFFFC8;">                                                                 
                             		<div>
                                        <label>
                                            <input type="radio" name="optViewCaseNotes" id="optViewCaseNotesYes" onclick="setupOptions();" value="Y" style="vertical-align:middle;" <%=checkRadio(optViewCaseNotes,"Y")%> >Visible&nbsp;&nbsp;
                                        </label>			      
                                        <label>
                                            <input type="radio" name="optViewCaseNotes" id="optViewCaseNotesNo" onclick="setupOptions();" value="N" style="vertical-align:middle;" <%=checkRadio(optViewCaseNotes,"N")%>>Hidden
                                        </label>    
                                    </div>
                                    <div class="subLabel" style="padding-top:5px;">Sets visiblity and group access to 'Case Notes' tab.</div>
                                    <div class="subLabel" style="padding-top:5px;"><img src="../_images/icons/16/help.png" width="16" height="16" align="absmiddle" style="margin-right:5px; margin-bottom:2px; vertical-align:middle;">Default Groups: <a href="#" onclick="groupType('ProgramAdmin'); return false;">Account Administrators</a>, <a href="#" onclick="groupType('Investigator'); return false;">Investigators</a>, <a href="#" onclick="groupType('myCMLite'); return false;">Restricted Users</a></div>
                                    
                        		</td>
                        	</tr>



                            <tr>                    
								<td class="formLabel">Information:</td>
								<td align=left>
                                	<div id="CaseNotesOption">
                                    
                                	<div style="float:left; width:155px;">[Field]</div>
                                    <div>[Setting]</div>                                                                        

                                	<div style="float:left; padding-top:3px; width:150px;">Case Status</div>
                                    <div>                                    	                                    	
                                   		<label>
                                     		<input type="radio" name="optEditCaseStatus" id="optEditCaseStatusYes" value="Y" style="vertical-align:middle;" <%=checkRadio(optEditCaseStatus,"Y")%> >Modify&nbsp;&nbsp;&nbsp;
                                     	</label>			      
                                      	<label>
                                    		<input type="radio" name="optEditCaseStatus" id="optEditCaseStatusLocked" value="L" style="vertical-align:middle;" <%=checkRadio(optEditCaseStatus,"L")%>>Locked&nbsp;&nbsp;&nbsp;
                                      	</label>
                                      	<label>
                                    		<input type="radio" name="optEditCaseStatus"  id="optEditCaseStatusNo" value="N" style="vertical-align:middle;" <%=checkRadio(optEditCaseStatus,"N")%>>Hidden
                                      	</label>                                        
                                    </div>                                                                        

                                	<div style="float:left; padding-top:3px; width:150px;">Case Owner</div>
                                    <div>                                    	                                    	
                                   		<label>
                                     		<input type="radio" name="optEditOwner" id="optEditOwnerYes" value="Y" style="vertical-align:middle;" <%=checkRadio(optEditOwner,"Y")%> >Modify&nbsp;&nbsp;&nbsp;
                                     	</label>			      
                                      	<label>
                                    		<input type="radio" name="optEditOwner" id="optEditOwnerLocked" value="L" style="vertical-align:middle;" <%=checkRadio(optEditOwner,"L")%>>Locked&nbsp;&nbsp;&nbsp;
                                      	</label>
                                      	<label>
                                    		<input type="radio" name="optEditOwner" id="optEditOwnerNo" value="N" style="vertical-align:middle;" <%=checkRadio(optEditOwner,"N")%>>Hidden
                                      	</label>                                        
                                    </div>                                                                        

                                	<div style="float:left; padding-top:3px; width:150px;">Date Closed</div>
                                    <div>                                    	                                    	
                                   		<label>
                                     		<input type="radio" name="optEditDateClosed" id="optEditDateClosedYes" value="Y" style="vertical-align:middle;" <%=checkRadio(optEditDateClosed,"Y")%> >Modify&nbsp;&nbsp;&nbsp;
                                     	</label>			      
                                      	<label>
                                    		<input type="radio" name="optEditDateClosed" id="optEditDateClosedLocked" value="L" style="vertical-align:middle;" <%=checkRadio(optEditDateClosed,"L")%>>Locked&nbsp;&nbsp;&nbsp;
                                      	</label>
                                      	<label>
                                    		<input type="radio" name="optEditDateClosed" id="optEditDateClosedNo" value="N" style="vertical-align:middle;" <%=checkRadio(optEditDateClosed,"N")%>>Hidden
                                      	</label>                                        
                                    </div>                                                                        

                                	<div style="float:left; padding-top:3px; width:150px;">Source</div>
                                    <div>                                    	                                    	
                                   		<label>
                                     		<input type="radio" name="optEditSource" id="optEditSourceYes" value="Y" style="vertical-align:middle;" <%=checkRadio(optEditSource,"Y")%> >Modify&nbsp;&nbsp;&nbsp;
                                     	</label>			      
                                      	<label>
                                    		<input type="radio" name="optEditSource" id="optEditSourceLocked" value="L" style="vertical-align:middle;" <%=checkRadio(optEditSource,"L")%>>Locked&nbsp;&nbsp;&nbsp;
                                      	</label>
                                      	<label>
                                    		<input type="radio" name="optEditSource" id="optEditSourceNo" value="N" style="vertical-align:middle;" <%=checkRadio(optEditSource,"N")%>>Hidden
                                      	</label>                                        
                                    </div>                                                                        
                                
                                	<div style="float:left; padding-top:3px; width:150px;">Case Managers</div>
                                    <div>                                    	                                    	
                                   		<label>
                                     		<input type="radio" name="optEditCaseMgr" id="optEditCaseMgrYes" value="Y" style="vertical-align:middle;" <%=checkRadio(optEditCaseMgr,"Y")%> >Modify&nbsp;&nbsp;&nbsp;
                                     	</label>			      
                                      	<label>
                                    		<input type="radio" name="optEditCaseMgr" id="optEditCaseMgrLocked" value="L" style="vertical-align:middle;" <%=checkRadio(optEditCaseMgr,"L")%>>Locked&nbsp;&nbsp;&nbsp;
                                      	</label>
                                      	<label>
                                    		<input type="radio" name="optEditCaseMgr" id="optEditCaseMgrNo" value="N" style="vertical-align:middle;" <%=checkRadio(optEditCaseMgr,"N")%>>Hidden
                                      	</label>                                        
                                    </div>                                                                        

                                	<div style="float:left; padding-top:3px; width:150px;">Owner Notes</div>
                                    <div>                                    	                                    	
                                   		<label>
                                     		<input type="radio" name="optEditOwnerNotes" id="optEditOwnerNotesYes" value="Y" style="vertical-align:middle;" <%=checkRadio(optEditOwnerNotes,"Y")%> >Visible&nbsp;&nbsp;&nbsp;
                                     	</label>			      
                                      	<label>
                                    		<input type="radio" name="optEditOwnerNotes" id="optEditOwnerNotesNo" value="N" style="vertical-align:middle;" <%=checkRadio(optEditOwnerNotes,"N")%>>Hidden
                                      	</label>
                                    </div>
                                    </div>

                                
                                </td>
                            </tr>

                            <tr>                    
								<td class="formLabel" style="padding-top:10px;">Investigations:</td>
								<td align=left>
                                    <div id="optInvestigations">
                                        <div style="float:left;">
                                            <label>
                                                <input type="radio" name="optViewInvestigations" id="optViewInvestigationsYes" onclick="setupOptions();" value="Y" style="vertical-align:middle;" <%=checkRadio(optViewInvestigations,"Y")%> >Visible&nbsp;&nbsp;
                                            </label>			      
                                            <label>
                                                <input type="radio" name="optViewInvestigations" id="optViewInvestigationsNo" onclick="setupOptions();" value="N" style="vertical-align:middle;" <%=checkRadio(optViewInvestigations,"N")%>>Hidden
                                            </label>
                                        </div>
                                        <!-- SET BACKWARDS Y equals NO AND N equals YES for presentation -->
                                        <div id="InvestigationOption:Label" style="float:left; color:#35487B; font-weight:bold; padding:3px; width:100px; text-align:right;">+Access:</div>
                                        <div id="InvestigationOption" style="float:left;">                                    	
                                            <label>
                                                <input type="radio" name="optViewAssignedInvOnly" id="optViewAssignedInvOnlyNo" value="N" style="vertical-align:middle;" <%=checkRadio(optViewAssignedInvOnly,"N")%> >All&nbsp;&nbsp;
                                            </label>			      
                                            <label>
                                                <input type="radio" name="optViewAssignedInvOnly" id="optViewAssignedInvOnlyYes" value="Y" style="vertical-align:middle;" <%=checkRadio(optViewAssignedInvOnly,"Y")%>>Assigned Only
                                            </label>
                                        </div>

                                        <div id="InvestigationOptionAdd:Label" style="float:left; color:#35487B; font-weight:bold; padding:3px; width:100px; text-align:right;">^Add New:</div>
                                        <div id="InvestigationOptionAdd">                                    	
                                            <label>
                                                <input type="radio" name="optAddInvestigations" id="optAddInvestigationsYes" value="Y" style="vertical-align:middle;" <%=checkRadio(optAddInvestigations,"Y")%> >Yes&nbsp;&nbsp;
                                            </label>			      
                                            <label>
                                                <input type="radio" name="optAddInvestigations" id="optAddInvestigationsNo" value="N" style="vertical-align:middle;" <%=checkRadio(optAddInvestigations,"N")%>>No
                                            </label>
                                        </div>
                                        <div class="subLabel" style="padding-top:5px;">Sets visiblity and group access to 'Investigations' side tab.<br/><strong>(+)</strong> setting is ignored if user is Owner of the issue.<br/><strong>(^)</strong> is always allowed if Investigations are Visible and user is Account Admin or Owner of the issue.</div>
                                        
                                    </div>
                                </td>
                            </tr>


                            <tr>                    
								<td class="formLabel" style="padding-top:10px;">Documents:</td>
								<td align=left>
                                <div id="optDocuments">
                                	<div>
                                  		<label>
                                     		<input type="radio" name="optViewDocuments" id="optViewDocumentsYes" value="Y" style="vertical-align:middle;" <%=checkRadio(optViewDocuments,"Y")%> >Visible&nbsp;&nbsp;
                                      	</label>			      
                                     	<label>
                                     		<input type="radio" name="optViewDocuments" id="optViewDocumentsNo" value="N" style="vertical-align:middle;" <%=checkRadio(optViewDocuments,"N")%>>Hidden
                                     	</label>
                                    </div>
									<div class="subLabel" style="padding-top:5px;">Sets visiblity and group access to 'Documents' side tab.</div>
                                </div>
                                </td>
                            </tr>


                            <tr>                    
								<td class="formLabel" style="padding-top:10px;">User Fields:</td>
								<td align=left>
                                <div id="optUserFields">
                                	<div style="float:left;">
                                  		<label>
                                     		<input type="radio" name="optViewCaseUserFields" id="optViewCaseUserFieldsYes" onclick="setupOptions();" value="Y" style="vertical-align:middle;" <%=checkRadio(optViewCaseUserFields,"Y")%> >Visible&nbsp;&nbsp;
                                      	</label>			      
                                     	<label>
                                     		<input type="radio" name="optViewCaseUserFields" id="optViewCaseUserFieldsNo" onclick="setupOptions();" value="N" style="vertical-align:middle;" <%=checkRadio(optViewCaseUserFields,"N")%>>Hidden
                                     	</label>
                                    </div>
                                    <div id="UserFieldsOption:Label" style="float:left; color:#35487B; font-weight:bold; padding:3px; width:100px; text-align:right;">Editable:</div>
                                	<div id="UserFieldsOption">                                        
                                        <label>
                                			<input type="radio" name="optEditCaseUserFields" id="optEditCaseUserFieldsYes" value="Y" style="vertical-align:middle;" <%=checkRadio(optEditCaseUserFields,"Y")%> >Modify&nbsp;&nbsp;
                                     	</label>			      
                                      	<label>
                                      		<input type="radio" name="optEditCaseUserFields" id="optEditCaseUserFieldsNo" value="N" style="vertical-align:middle;" <%=checkRadio(optEditCaseUserFields,"N")%>>Locked
                                      	</label>
                                    </div>
                                    <div class="subLabel" style="padding-top:5px;">Sets visiblity and group access to 'User Fields' side tab.</div>                                    
                                </div>
                                </td>
                            </tr>

                            <tr>                    
								<td class="formLabel" style="padding-top:10px;">Resolutions:</td>
								<td align=left>
                                <div id="optResolutions">
                                	<div style="float:left;">
                                  		<label>
                                     		<input type="radio" name="optEditCaseResolution" id="optEditCaseResolutionYes" onclick="setupOptions();" value="Y" style="vertical-align:middle;" <%=checkRadio(optEditCaseResolution,"Y")%> >Visible&nbsp;&nbsp;
                                      	</label>			      
                                     	<label>
                                     		<input type="radio" name="optEditCaseResolution" id="optEditCaseResolutionNo" onclick="setupOptions();" value="N" style="vertical-align:middle;" <%=checkRadio(optEditCaseResolution,"N")%>>Hidden
                                     	</label>
                                    </div>
                                    <div id="ResolutionOption:Label" style="float:left; color:#35487B; font-weight:bold; padding:3px; width:100px; text-align:right;">Approve:</div>                                    
                                	<div id="ResolutionOption">                                        
                                        <label>
                                			<input type="radio" name="optEditCaseResolutionApp" id="optEditCaseResolutionAppYes" value="Y" style="vertical-align:middle;" <%=checkRadio(optEditCaseResolutionApp,"Y")%> >Yes&nbsp;&nbsp;
                                     	</label>			      
                                      	<label>
                                      		<input type="radio" name="optEditCaseResolutionApp" id="optEditCaseResolutionAppNo" value="N" style="vertical-align:middle;" <%=checkRadio(optEditCaseResolutionApp,"N")%>>No
                                      	</label>
                                    </div>
                                    <div class="subLabel" style="padding-top:5px;">Sets visiblity and group access to 'Resolution' side tab.</div>                                                                        
                                </div>
                                </td>
                            </tr>

						</table>
   						<!-- END Case Notes Form Fields -->

                    </td>
                                        
                </tr>
            
            </table>
            <!-- END Settings table -->
	
    	</div>
		<!-- STOP Settings div -->        


		<script>
			function groupType(type) {

				if (document.getElementById("optViewCaseNotesNo").checked) {
					jAlert('You must first enable Case Notes.', 'myCM Alert');
					return false;
				}
										
				if (type=='ProgramAdmin') {
					$('#optEditCaseStatusYes').attr('checked', true);
					$('#optEditOwnerYes').attr('checked', true);
					$('#optEditDateClosedYes').attr('checked', true);
					$('#optEditSourceYes').attr('checked', true);
					$('#optEditCaseMgrYes').attr('checked', true);
					$('#optEditOwnerNotesYes').attr('checked', true);
					$('#optViewInvestigationsYes').attr('checked', true);
					$('#optViewAssignedInvOnlyNo').attr('checked', true);
					$('#optAddInvestigationsYes').attr('checked', true);
					$('#optViewDocumentsYes').attr('checked', true);
					$('#optViewCaseUserFieldsYes').attr('checked', true);
					$('#optEditCaseUserFieldsYes').attr('checked', true);
					$('#optEditCaseResolutionYes').attr('checked', true);
					$('#optEditCaseResolutionAppYes').attr('checked', true);
				}
				else if (type=='Investigator') {
					$('#optEditCaseStatusLocked').attr('checked', true);
					$('#optEditOwnerLocked').attr('checked', true);
					$('#optEditDateClosedLocked').attr('checked', true);
					$('#optEditSourceNo').attr('checked', true);
					$('#optEditCaseMgrLocked').attr('checked', true);
					$('#optEditOwnerNotesNo').attr('checked', true);
					$('#optViewInvestigationsYes').attr('checked', true);					
					$('#optViewAssignedInvOnlyYes').attr('checked', true);
					$('#optAddInvestigationsNo').attr('checked', true);					
					$('#optViewDocumentsYes').attr('checked', true);
					$('#optViewCaseUserFieldsNo').attr('checked', true);
					$('#optEditCaseUserFieldsNo').attr('checked', true);
					$('#optEditCaseResolutionYes').attr('checked', true);
					$('#optEditCaseResolutionAppNo').attr('checked', true);
				}
				else if (type=='myCMLite') {
					$('#optEditCaseStatusYes').attr('checked', true);
					$('#optEditOwnerNo').attr('checked', true);
					$('#optEditDateClosedYes').attr('checked', true);
					$('#optEditSourceNo').attr('checked', true);
					$('#optEditCaseMgrLocked').attr('checked', true);
					$('#optEditOwnerNotesYes').attr('checked', true);
					$('#optViewInvestigationsNo').attr('checked', true);
					$('#optViewAssignedInvOnlyYes').attr('checked', true);
					$('#optAddInvestigationsNo').attr('checked', true);										
					$('#optViewDocumentsNo').attr('checked', true);
					$('#optViewCaseUserFieldsNo').attr('checked', true);
					$('#optEditCaseUserFieldsNo').attr('checked', true);
					$('#optEditCaseResolutionYes').attr('checked', true);
					$('#optEditCaseResolutionAppYes').attr('checked', true);				}
				
				//make sure all options are set correctly
				setupOptions();				
			}
		</script>
        
		<script>
			function setupOptions() {
				
				//case notes options on ALL options
				if (document.getElementById("optViewCaseNotesYes").checked) {
			        $('#CaseNotesOption :input').removeAttr('disabled');	
			        $('#optInvestigations :input').removeAttr('disabled');	
			        $('#optDocuments :input').removeAttr('disabled');						
			        $('#optUserFields :input').removeAttr('disabled');	
			        $('#optResolutions :input').removeAttr('disabled');						
				}
				else {					
					$('#CaseNotesOption :input').attr('disabled', true);	
			        $('#optInvestigations :input').attr('disabled', true);	
			        $('#optDocuments :input').attr('disabled', true);	
			        $('#optUserFields :input').attr('disabled', true);	
			        $('#optResolutions :input').attr('disabled', true);	
				}
		
				//set sub-options on other parameters.
				//investigation sub-options
				if (document.getElementById("optViewInvestigationsYes").checked && document.getElementById("optViewCaseNotesYes").checked) {
					document.getElementById("InvestigationOption:Label").style.color = "#35487B";
			        $('#InvestigationOption :input').removeAttr('disabled');
					document.getElementById("InvestigationOptionAdd:Label").style.color = "#35487B";
			        $('#InvestigationOptionAdd :input').removeAttr('disabled');
				}
				else {					
					document.getElementById("InvestigationOption:Label").style.color = "#666";
					$('#InvestigationOption :input').attr('disabled', true);	//disable elements in div
					document.getElementById("InvestigationOptionAdd:Label").style.color = "#666";
					$('#InvestigationOptionAdd :input').attr('disabled', true);	//disable elements in div					
				}

				//user fields sub-options
				if (document.getElementById("optViewCaseUserFieldsYes").checked && document.getElementById("optViewCaseNotesYes").checked) {
					document.getElementById("UserFieldsOption:Label").style.color = "#35487B";
			        $('#UserFieldsOption :input').removeAttr('disabled');
				}
				else {					
					document.getElementById("UserFieldsOption:Label").style.color = "#666";
					$('#UserFieldsOption :input').attr('disabled', true);	//disable elements in div
				}
							
				//resolution sub-options
				if (document.getElementById("optEditCaseResolutionYes").checked && document.getElementById("optViewCaseNotesYes").checked) {
					document.getElementById("ResolutionOption:Label").style.color = "#35487B";
			        $('#ResolutionOption :input').removeAttr('disabled');
				}
				else {					
					document.getElementById("ResolutionOption:Label").style.color = "#666";
					$('#ResolutionOption :input').attr('disabled', true);	//disable elements in div
				}
												
			}
			
			setupOptions();
		</script>

