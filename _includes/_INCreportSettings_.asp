        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Settings div -->
        <div id="tab:settings" style="display:none;">        
        
            <!-- START Settings table -->   
	        <table id="table_settings" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>
                    
                    <td valign="top">
                                        
					  	<!-- START Settings Form Fields -->	                    
						<table id="table:settings:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">

      						<tr <% if optID=0 then response.write("style=""display:none;""") %>>
                              <td class="formLabel"><span class="required">*</span>Name:</td>
                              <td align=left nowrap>
                              	<input name="optName" id="optName" class="inputLong" value="<%=optName%>" maxlength="255" />
                              </td>
                            </tr>
                            
      						<tr <% if optID<>0 then response.write("style=""display:none;""") %>>
                              <td class="formLabel"><span class="required">*</span>Name:</td>
                              <td align=left nowrap>
							  	<%=optName%>
                               </td>
                            </tr>

      						<tr <% if optID=0 then response.write("style=""display:none;""") %>>
                              <td class="formLabel">Notes:</td>
                              <td align=left nowrap>
                              	<textarea name="optNotes" id="optNotes" style="width:85%; height:75px;" ><%=server.HTMLEncode(optNotes & "")%></textarea>
                              </td>
                            </tr>

      						<tr <% if optID=0 then response.write("style=""display:none;""") %>>
                              <td class="formLabel"><span class="required">*</span>Owner:</td>
                              <td align=left nowrap style="padding-bottom:0px;">                                                            	 
                              	<div style="float:left;">
                                	<input name="rptOwner" id="rptOwner" class="inputLong" value="<% =rptOwner %>" maxlength="255" readonly="readonly" />
                                    <input type="hidden" name="rptLogid" id="rptLogid" value="<% =rptLogid %>" >
                                </div>

                                <!-- ONLY for CCI ADMINs and Customers, not for Risk Specialists -->                                
                                <% if cLng(session(session("siteID") & "adminLoggedOn")) < 3 or cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then %>
	                                <div><a class="myCMbuttonSmall" href="#" title="Change Owner" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_user.asp?recid=<% =idReport %>&cid=<% =customerID %>&multi=false&pageView=reports', 380, 450, 'no'); resetSystemMsg();"><span class="user" style="padding-right:7px;">&nbsp;</span></a></div>
                                <% end if %>                                
                                
                              </td>
                            </tr>
      						<tr <% if optID=0 then response.write("style=""display:none;""") %>>
                              <td style="border-top:none; padding-bottom:5px;">&nbsp;</td>
                              <td align=left nowrap style="border-top:none; padding:0px;">                               
							 	<div class="subLabel" >Owner has full control over report and its settings.</div>
                              </td>
                            </tr>

      						<tr <% if optID=0 then response.write("style=""display:none;""") %>>
                              <td class="formLabel">
                              	<% if cLng(sLogid) = cLng(rptLogid) then %>
		                      		<!-- ALL users can edit their own report categories -->
    	                           	<a href="#" class="formLabelLinkImg" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=logins&field=ReportsCategory&form=optCategory&pageView=reports', 320, 375, 'no'); return false;" onmouseover="javascript:configImage('congif_Category','over');" onmouseout="javascript:configImage('congif_Category','out');"><img id="congif_Category" src="../_images/x_cleardot_16.gif" align="absmiddle" border="0" /></a>
        	                      	<a href="#" class="formLabelLink" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=logins&field=ReportsCategory&form=optCategory&pageView=reports', 320, 375, 'no'); return false;" onmouseover="javascript:configImage('congif_Category','over');" onmouseout="javascript:configImage('congif_Category','out');">Category:</a>
                                <%
								else
									response.write("Category:")
                                end if 
								%>
                              </td>
                              <td align=left nowrap style="padding-bottom:5px;">                                                            	 
                              	<div style="padding-bottom:3px;">
                                	<%
									dim arrCategory									  
                                    mySQL="SELECT ReportsCategory " _
                                        & "FROM   Logins " _
                                        & "WHERE  Logid=" & rptLogid & " AND CustomerID='" & customerID & "' "
                                    'set rs = openRSexecute(mySQL)
									set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
									if not rs.eof then
										arrCategory = rs("ReportsCategory")									
									end if
                                    call closeRS(rs)									
									call buildSelect(arrCategory,optCategory,"optCategory","inputMediumLong","Y")
                                    %>
                                </div>
                                <div class="subLabel">Allows for the organizing of reports.</div>
                              </td>
                            </tr>
                            
      						<tr <% if optID=0 then response.write("style=""display:none;""") %>>
                              <td class="formLabel">List Shortcut:</td>
                              <td align=left nowrap style="padding-bottom:0px;">                                                            	 
                              	<div style="float:left; margin-right:5px;">
                                    <select name="optIssueList" size=1 style="width:75px;">
                                        <option value="N"  <%=checkMatch(lCase(optIssueList),"n")%>>No</option>
                                        <option value="Y"  <%=checkMatch(lCase(optIssueList),"y")%>>Yes</option>
                                    </select>                                                                                        
                                </div>
                                <div><a class="myCMbuttonSmall" title="Sort List" href="#" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_issuelist_sort.asp', 400, 375, 'no'); resetSystemMsg();"><span class="sort" style="padding-right:7px;">&nbsp;</span></a></div>
                              </td>
                            </tr>
      						<tr <% if optID=0 then response.write("style=""display:none;""") %>>
                              <td style="border-top:none; padding-bottom:5px;">&nbsp;</td>
                              <td align=left nowrap style="border-top:none; padding:0px;">                               
							 	<div class="subLabel" >Makes report visible under <a href="../scripts/issues.asp">Issue Lists</a> menu tree. Not for reports with more than 1,000 records.</div>
                              </td>
                            </tr>

      						<tr <% if lCase(optType) = "sql" then response.write("style=""display:none;""") %>>
                              <td class="formLabel">Sort Field:</td>
                              <td align=left nowrap>
                              		<div style="float:left; margin-right:10px;">                                                                        
    		                        	<%
										if lCase(optType) <> "calculated" then										
											'clear [] from already selected Sort Field
											optSortField = replace(optSortField, "[","")
											optSortField = replace(optSortField, "]","")										
											
											'Build array for dropdown selections...get rid of fields NOT to be sorted on also
											optFields = replace(optFields, "[Total]","")
											optFields = replace(optFields, "[Summary]","")
											optFields = replace(optFields, "[Details]","")
											optFields = replace(optFields, "[Addendum]","")
											optFields = replace(optFields, "[Resolution]","")							
											optFields = replace(optFields, "[CaseNotes]","")													
											optFields = replace(optFields, "[","")
											optFields = replace(optFields, "]","")
											optArr = split(optFields,",")
											call buildSelect(optArr,optSortField,"optSortField","inputMedium","Y")
											
										elseif lCase(optType) = "calculated" then
											response.write("<select name=""optSortField"" size=1>")
											response.write("	<option value=""" & optChartXAxis & """   " & checkMatch(lCase(optSortField),lCase(optChartXAxis)) & ">" & replace(optChartXAxis,"_", " ") & "</option>")
											response.write("</select>")
										
										end if
	            	               		%>                                        
                                    </div>
                                    <div>                                    
                                    <select name="optSortOrder" size=1>
                                        <option value="asc"   <%=checkMatch(lCase(optSortOrder),"asc")	%>>Ascending</option>
                                        <option value="desc"  <%=checkMatch(lCase(optSortOrder),"desc") %>>Descending</option>
                                    </select>                                                    
                                    </div>
                                	<div class="subLabel" style="padding-top:5px;">Column used to set initial sort order.</div>
                              </td>
                            </tr>

      						<tr <% if optID=0 then response.write("style=""display:none;""") %>>
                            	<td class="formLabel" style="background-color:#FFFFC8;">Dataset:</td>
                              	<td align=left nowrap style="background-color:#FFFFC8;">
									<%
									'regular users non-CCI Admins
									if cLng(session(session("siteID") & "adminLoggedOn")) > 3 then
										response.write(optDataset)
										if lCase(optDatasetCustID)="mycm" then response.write("&nbsp;[Built-In]") else response.write("&nbsp;[User-Defined]")
    	                          		response.write("<div class=""subLabel"">" & optDatasetDesc & "</div>")
	                                    response.write("<input type=""hidden"" name=""optDatasetID"" id=""optDatasetID"" value=" & optDatasetID & " >")
									else
										mySQL = "SELECT DatasetID, Name, [Table], 'Built-In' AS [Type], SecurityLevel " _
											  & "FROM   Dataset " _
											  & "WHERE  CustomerID='MYCM' " _									
											  & "UNION ALL " _
											  & "SELECT DatasetID, Name, [Table], 'User-Defined' AS [Type], SecurityLevel " _
											  & "FROM   Dataset " _
											  & "WHERE  CustomerID='" & customerID & "' " _
											  & "ORDER BY Name, SecurityLevel "
										set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
										'build select
										response.write("<select name=""optDatasetID"" size=1 class=""inputLong"">")
										response.write("	<option value="""">-- Select --</option>")																				
										if not rs.eof then
											do until rs.eof
												response.write("<option value=""" & rs("DatasetID") & """ " & checkMatch(lCase(optDatasetID),rs("DatasetID")) & ">" & rs("Name") & " (" & lCase(rs("Type")) & ") (" & rs("SecurityLevel") & ")" & "</option>")
												rs.movenext
											loop
										end if
										response.write("</select>")									
										call closeRS(rs)									    	                          		
										response.write("<div class=""subLabel"" style=""padding-top:5px;"">Dataset used to execute report. Security Level should match user running report.</div>")
									end if									
                                    %>                                
                              	</td>
                            </tr>
                                          
						</table>
   						<!-- END Settings Form Fields -->

                    </td>
                                        
                </tr>
            
            </table>
            <!-- END Settings table -->
	
    	</div>
		<!-- STOP Settings div -->        


   		<script language="javascript">
			//hide/show caller related fields based on Anonynimity
			function urlFieldVisible(objVisible) {						
				changeObjectDisplay('tr_urlField', objVisible);
			}
		</script>
