        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Account div -->
        <div id="tab:details">
        
        
            <!-- START User Account table -->   
	        <table id="table_details" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>
                    
                    <!-- START Middle Section -->    
                    <td valign="top">
                                        
					  	<!-- START Account Info Fields -->	                    
						<table id="table:details:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">

                            <tr>
                              <td class="formLabel">Name:</td>
                              <td align=left nowrap>
                               	<% if cLng(session(session("siteID") & "adminLoggedOn")) > 2 then %>
                               		<input name="customerName" id="customerName" class="inputLong" value="<%=cusName%>" maxlength="255" disabled="disabled" />                               
                               		<input type=hidden name="cusName" id="cusName" value="<%=cusName%>">
                                <% else %>
	                                <input name="cusName" id="cusName" class="inputLong" value="<%=cusName%>" maxlength="255" />
                                <% end if %>                                                                                   
                              </td>
                            </tr>

                            <tr>
                              <td class="formLabel">ID:</td>
                              <td align=left nowrap>
                              	<div>
                                	<% if cLng(session(session("siteID") & "adminLoggedOn")) > 2 then %>
                                		<input name="customerID" id="customerID" class="inputShort" value="<% =uCase(customerID) %>" maxlength="50" disabled="disabled" />                               
                                		<input type=hidden name="cid" id="cid" value="<% =customerID %>">
                                    <% else %>
	                                    <input name="cid" id="cid" class="inputShort" value="<% =uCase(customerID) %>" style="text-transform: uppercase;" maxlength="5" />
                                    <% end if %>                   
                                    <!-- FINISH THIS WITH TESTING customerID against customerID_ORIGINAL to see if was changed -->
                                    <!-- and make sure there is not already another customer with the new ID                   -->
                                    <input type=hidden name="customerID_ORIGINAL" id="customerID_ORIGINAL" value="<% =customerID %>">             
                                </div>
                                <div class="subLabel" style="padding-top:5px;">Used when generating issue #'s: 1106-[<strong>ID</strong>]-10001-01</div>
                              </td>
                            </tr>

                            <tr style="display:none;">
                              <td class="formLabel">Display As:</td>
                              <td align=left nowrap>
                              	<div><input name="cusDisplayAs" id="cusDisplayAs" class="inputLong" value="<%=cusDisplayAs%>" maxlength="255" /></div>
                                <div class="subLabel" style="padding-top:5px;">Display used during intake process.</div>
                              </td>
                            </tr>

                            <tr>                            
                              <td class="formLabel">Address:</td>
                              <td align=left nowrap>                                       
                                <div class="subLabel">Street Address:</div>
                                <div>
                                    <textarea name="cusAddress" id="cusAddress" style="width:300px; height:50px;"><%=cusAddress%></textarea>
                                </div>                               
                                <div style="float:left;">
	                                <div class="subLabel">City:</div>
                                    <input name="cusCity" id="cusCity" class="inputShort" value="<%=cusCity%>" maxlength="255" />
                                </div>                                
                                <div style="float:left;">
    	                            <div class="subLabel">State/Province:</div>
                                    <input name="cusState" id="cusState" class="inputShort" value="<%=cusState%>" maxlength="255" />
                                </div>                                
                                <div>                                
        	                        <div class="subLabel">Postal Code:</div>
                                  <input name="cusZip" id="cusZip" class="inputShort" value="<%=cusZip%>" maxlength="255" />
                                </div>
                                <div>                                
        	                        <div class="subLabel">Country:</div>
                                    <input name="cusCountry" id="cusCountry" class="inputLong" value="<%=cusCountry%>" maxlength="255" />
                                </div>				                                                            

                              </td>                              
                            </tr>

							<tr>
                                <td class="formLabel">
	                                <!-- ONLY for db admins and CCI system admins -->
                                	<% if cLng(session(session("siteID") & "adminLoggedOn")) <= 2 then %>
	                                    <a href="#" class="formLabelLinkImg" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=system&field=pIndustry&form=cusIndustry&pageView=profile', 320, 375, 'no'); return false;" onmouseover="javascript:configImage('congif_Industry','over');" onmouseout="javascript:configImage('congif_Industry','out');"><img id="congif_Industry" src="../_images/x_cleardot_16.gif" align="absmiddle" border="0" /></a>
	                                    <a href="#" class="formLabelLink" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=system&field=pIndustry&form=cusIndustry&pageView=profile', 320, 375, 'no'); return false;" onmouseover="javascript:configImage('congif_Industry','over');" onmouseout="javascript:configImage('congif_Industry','out');">Industry:</a>
									<% else %>
										Industry:
									<% end if %>
                                </td>
                                <td align=left nowrap>
                                    <%
                                    'Build array for dropdown selections
                                    optArr = getDropDownConfig("pIndustry")
									call buildSelect(optArr,cusIndustry,"cusIndustry","inputLong","Y")
                                   	%>
							  </td>
                            </tr>

                            <tr>
                              <td class="formLabel">Employee Count:</td>
                              <td align=left>
                              	<%
								'required or jQuery spinner doesn't work
								if isNull(cusEmployeeCount) or cusEmployeeCount = "" or len(cusEmployeeCount) <= 0 then
									cusEmployeeCount = "0"
								end if
								%>
                              	<input name="cusEmployeeCount" id="cusEmployeeCount" class="inputShort" value="<% =cusEmployeeCount %>" maxlength="255" />                                
                                <div class="subLabel" style="padding-top:5px;">Numeric entries only, no special characters (# or commas)</div>
                              </td>
                            </tr>
							<script type="text/javascript">
                                //jQuery().ready(function($) {
                                //    $('#cusEmployeeCount').spinner({ min: 0, step: 100, increment: 'fast' });			
                                //});
                            </script>

                            <tr>
                              <td class="formLabel">Phone Number(s):</td>
                              <td colspan="3" align=left nowrap>
                                                              
                                <div style="float:left;">
                                	<div class="subLabel">Work Phone:</div>
                                    <input name="cusPhone_Work" id="cusPhone_Work" class="inputMedium" value="<% =cusPhone_Work %>" maxlength="255" />
                                </div>
                                
                                <div>
                                	<div class="subLabel">Fax:</div>
                                    <input name="cusPhone_Fax" id="cusPhone_Fax" class="inputMedium" value="<% =cusPhone_Fax %>" maxlength="255" />
                                </div>                                                                
                                
                                <div style="float:left;">
                                	<div class="subLabel">Customer Service:</div>
                                    <input name="cusPhone_Customer" id="cusPhone_Customer" class="inputMedium" value="<% =cusPhone_Customer %>" maxlength="255" />
                                </div>
                                
                                <div>
                                	<div class="subLabel">Other:</div>
                                    <input name="cusPhone_Other" id="cusPhone_Other" class="inputMedium" value="<% =cusPhone_Other %>" maxlength="255" />
                                </div>
                                                                                                                              
                              </td>
                            </tr>
                            
                            
             				<!-- ONLY for db admins and CCI system admins -->
                			<% if cLng(session(session("siteID") & "adminLoggedOn")) <= 3 then %>

                                <tr>
                                    <td class="formLabel">Contract Date:</td>
                                    <td align=left>                                
                                        <input name="cusContractDate" id="cusContractDate" class="inputShort" value="<% =cusContractDate %>" maxlength="20" />
                                    </td>
                                </tr>
    
                                <tr>
                                    <td class="formLabel">Start Date:</td>
                                    <td align=left>                                
                                        <input name="cusStartDate" id="cusStartDate" class="inputShort" value="<% =cusStartDate %>" maxlength="20" />
                                    </td>                                
                                </tr>
    
                                <tr>
                                  <td class="formLabel">Retention:</td>
                                  <td align=left>
									<%
                                    'required or jQuery spinner doesn't work
                                    if isNull(cusRetention) or cusRetention = "" or len(cusRetention) <= 0 then
                                        cusRetention = "0"
                                    end if
                                    %>                                  
                                    <input name="cusRetention" id="cusRetention" class="inputShort" value="<% =cusRetention %>" maxlength="255" />&nbsp;month(s)
                                    <div class="subLabel" style="padding-top:5px;">Numeric entries only, no special characters (# or commas)</div>
                                  </td>
                                </tr>
                                <script type="text/javascript">
                                    jQuery().ready(function($) {
                                        $('#cusRetention').spinner({ min: 0, step: 1, increment: 'fast' });
                                    });
                                </script>


                                <tr>
                                    <td colspan="2" align="center">
                                        <table width="95%">
                                            <tr>
                                                <td align="left" class="clearFormat">
                                                    <div style="color:#35487B; font-weight:bold; padding:3px;">Notes (CCI use only)</div>
                                                    <textarea name="cusNotes" id="cusNotes" style="width:100%; height:100px; margin-bottom:12px;" ><% =server.HTMLEncode(cusNotes & "") %></textarea>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

								<% if cLng(session(session("siteID") & "adminLoggedOn")) <= 2 then %>
                                    <tr>
                                        <td class="formLabel" style="background-color:#FFFFC8;"><% =accountLabelSingle %> Active:</td>
                                        <td align=left style="background-color:#FFFFC8;">                                
                                            <label>
                                              <input type="radio" name="cusCanceled" id="cusCanceled" value="0" style="vertical-align:middle;" <% =checkRadio(cusCanceled,"0") %>>Yes&nbsp;&nbsp;
                                            </label>
                                        
                                            <label>
                                              <input type="radio" name="cusCanceled" id="cusCanceled" value="-1" style="vertical-align:middle;" <% =checkRadio(cusCanceled,"-1") %>>No
                                            </label>    
                                            <div class="subLabel" style="padding-top:5px;">Disables <% =accountLabelSingle %> and all active users.</div>
                                        </td>                                
                                    </tr>
								<% else %>
	                                <input type=hidden name="cusCanceled" id="cusCanceled" value="<% =cusCanceled %>">                                
                                <% end if %>
                            
                            <!-- EVERYONE ELSE, non-admins -->
                            <% else %>

                                <tr>
                                    <td class="formLabel" style="background-color:#FFFFC8;">Contract Date:</td>
                                    <td align=left style="background-color:#FFFFC8;"><% =cusContractDate %>&nbsp;</td>
                                </tr>
    
                                <tr>
                                    <td class="formLabel" style="background-color:#FFFFC8;">Start Date:</td>
                                    <td align=left style="background-color:#FFFFC8;"><% =cusStartDate %>&nbsp;</td>
                                </tr>
    
                                <tr>
                                    <td class="formLabel" style="background-color:#FFFFC8;">Retention:</td>
                                    <td align=left style="background-color:#FFFFC8;">
                                        <% 
                                        if isNull(cusRetention) or cusRetention <= "0" then
                                            response.write("<em>undefined</em>")
                                        else
                                            response.write(cusRetention & "&nbsp;month(s)")
                                        end if
                                        %>                                                                        
                                    </td>
                                </tr>

                                <input type=hidden name="cusContractDate" id="cusContractDate" value="<% =cusContractDate %>">
                                <input type=hidden name="cusStartDate" id="cusStartDate" value="<% =cusStartDate %>">
                                <input type=hidden name="cusRetention" id="cusRetention" value="<% =cusRetention %>">
                                <input type=hidden name="cusCanceled" id="cusCanceled" value="<% =cusCanceled %>">                                                                
                                <input name="cusNotes" id="cusNotes" style="display:none;" value="<% =server.HTMLEncode(cusNotes & "") %>" maxlength="255" />

                            <% end if %>                            

						</table>
   						<!-- END Account Info Form Fields -->                                                                                                                                       

                    </td>
                    <!-- STOP Middle Section -->			                    
                                        
                </tr>
            
            </table>
            <!-- END User Account table -->
	
    	</div>
		<!-- STOP Account div -->        
        
		<script>
			//JSON QUERY BUILDER used to search Country database
			buildJQuery('worldmap','country','cusCountry');
			
			//JSON QUERY BUILDER used to search Customer ID's
			buildJQuery('customer','customerid','cid');

			function buildJQuery(table,field,form_field) {
																
				$(function() {
					function split( val ) {
						return val.split( /,\s*/ );						
					}
					function extractLast( term ) {
						return split( term ).pop();						
					}                                                            
					$("#"+form_field)
						// don't navigate away from the field on tab when selecting an item
						.bind( "keydown", function( event ) {
                      		if ( event.keyCode === $.ui.keyCode.TAB &&
                            	$( this ).data( "autocomplete" ).menu.active ) {
                               		event.preventDefault();
                             }							 
                        })
                     	.autocomplete({
                      		source: function( request, response ) {
                          		$.getJSON( "../_jquery/suggestBox/json-data.asp?view="+table+"&field="+field+"&cid=<% =customerID %>", {
                             		term: extractLast( request.term )
                           		}, response );
                          	},
                       		search: function() {
                        		// custom minLength
                             	var term = extractLast( this.value );
                           		if ( term.length < 1 ) {
                            		return false;
                         		}								
                      		},
                      		focus: function() {
                    			// prevent value inserted on focus
                      			return false;
                         	}
                  		});
				});					

				//THIS WORKS FOR BINDING THE RESULTS SO YOU CAN PARSE OUT WHAT IS
				//SELECTED. REALLY SLOW SO WE NEED TO GIVE IT SOME MORE WORK LATER
				//$("#"+form_field).bind( "autocompleteselect", function(event, ui) {
				//	if (form_field=="cusZip") {						
						//alert(ui.item.label);						
						//$( "#cusCity" ).val( ui.item.city );
						//$( "#cusState" ).val( ui.item.state );						
				//	}
				//});

			}
		</script>                                                            														

