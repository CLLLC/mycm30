        <!--
        Form Investigations...   
        __ ....notes
        __ ....notes
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<%
		dim invNumber, invBGColor, invDiaryEntry, arrInvID
		dim labelX		

   		'Build array for Status dropdown selections
		dim optStatusArr
       	optStatusArr = getDropDownOpt("pInvestigationStatus",customerID)

		'Build array for Action Taken dropdown selections
		dim optActionArr
		optActionArr = getDropDownOpt("pActionTaken",customerID)

		'Build array for Risk Level dropdown selections
		dim optValidityArr
		optValidityArr = getDropDownOpt("pValidity",customerID)

		'Build array for Risk Level dropdown selections
		dim optRiskArr
		optRiskArr = getDropDownOpt("pRiskLevel",customerID)

		'Build array for Investigators dropdown selections
'			  & "WHERE Logins.CustomerID='" & CustomerID & "' AND CRS_Logins.CRSID Like '" & left(idIssue,len(idIssue)-3) & "%' " _		
		dim optInvestigatorsArr	
		mySQL = "SELECT Logins.LOGID, Logins.FirstName, Logins.LastName " _
			  & "FROM CRS_Logins INNER JOIN Logins ON CRS_Logins.LOGID = Logins.LOGID " _
			  & "WHERE CRS_Logins.CRSID Like '" & left(idIssue,len(idIssue)-3) & "%' " _
			  & "GROUP BY Logins.LOGID, Logins.FirstName, Logins.LastName " _
			  & "ORDER BY Logins.LastName, Logins.FirstName "
		set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,0)
		if not rs.eof then
			redim optInvestigatorsArr(rs.recordcount,2)
			labelX = 0
			do until rs.eof			
				optInvestigatorsArr(labelX,0) = rs("logid")
				'show with first and last name												
				if len(rs("firstname")) > 0 then
					optInvestigatorsArr(labelX,1) = rs("lastname") & ", " & rs("firstname")
				'last name only
				else
					optInvestigatorsArr(labelX,1) = rs("lastname")
				end if
				labelX = labelX + 1
				rs.movenext
			loop         
		else
			redim optInvestigatorsArr(0,0)
		end if
		call closeRS(rs)															

		'Build array for Category dropdown selections
		dim optCategoryArr
		mySQL = "SELECT CRS_Category.CategoryID, Category.Name, Category.Sub " _
			  & "FROM CRS_Category INNER JOIN Category ON CRS_Category.CategoryID = Category.CategoryID " _
			  & "WHERE Category.CustomerID='" & CustomerID & "' AND CRS_Category.CRSID Like '" & left(idIssue,len(idIssue)-3) & "%' " _
			  & "GROUP BY CRS_Category.CategoryID, Category.Name, Category.Sub " _
			  & "ORDER BY Category.Name, Category.Sub"																  
		set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,0)
		if not rs.eof then
			redim optCategoryArr(rs.recordcount,2)
			labelX = 0
			do until rs.eof			
				optCategoryArr(labelX,0) = rs("categoryid")
				'show with sub category											
				if len(rs("sub")) > 0 then
					optCategoryArr(labelX,1) = rs("name") & " [" & rs("sub") & "]"
				'master only
				else
					optCategoryArr(labelX,1) = rs("name")
				end if
				labelX = labelX + 1
				rs.movenext
			loop     
		else
			redim optCategoryArr(0,0)
		end if
		call closeRS(rs)



		'get Investigation User field settings
	    dim tmpInvValue, valueInvArr
		dim pInvField1, pInvField1Type, pInvField1Default
		dim pInvField2, pInvField2Type, pInvField2Default		
		
		'Get current configuration settings from database
		mySQL = "SELECT pInvField1, pInvField1Value, pInvField1Type, pInvField1Default, " _
			  & "pInvField2, pInvField2Value, pInvField2Type, pInvField2Default " _
			  & "FROM   Customer " _
			  & "WHERE  CustomerID='" & customerID & "' "
		'get UDF names
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		
		'assign field names		
		pInvField1 = rs("pInvField1")
		pInvField1Type = rs("pInvField1Type")
		pInvField1Default = rs("pInvField1Default")

		'assign field names		
		pInvField2 = rs("pInvField2")
		pInvField2Type = rs("pInvField2Type")
		pInvField2Default = rs("pInvField2Default")
																
		call closeRS(rs)



		'============================================================================
		'discover already created investigations	
		'============================================================================					
		mySQL = "SELECT a.InvestigationID, a.CRSID, a.LOGID, a.CategoryID, a.Name, a.Status, a.DateAdded, a.DateDeadline, a.DateClosed, a.ActionTaken, a.Notes, a.Outcome, a.RiskLevel, a.Substantiated, Logins.FirstName, Logins.LastName, " _
			  & "InvField1, InvField2 " _
			  & "FROM   Investigation a LEFT JOIN Logins ON a.LOGID = Logins.LOGID " _
			  & "WHERE  a.CRSID Like '" & left(idIssue,len(idIssue)-3) & "%' "
	  	'if NOT owner, limit to assigned investigations only
		if rptLogID<>sLogid and viewAssignedInvOnly = "Y" then
			mySQL = mySQL & " AND a.LOGID=" & sLogid
		end if
		'finish up...	  
		mySQL = mySQL & "ORDER By a.CRSID, InvestigationID "
		
		'set rs = openRSexecute(mySQL)
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		
		'make sure there ARE investigations to show...
		if not rs.eof then
		%>
            
            <!-- START Investigation Header -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
					<td nowrap="nowrap" width="40%" align="left" class="clearFormat" style="padding-left:5px; padding-right:5px; background:#666; border-left: 1px solid #666; background:url(../_images/table-header-black.jpg); height:26px; color: #FFF; font-weight:bold; vertical-align:middle;">                       
                    	Name
                    </td>
					<td nowrap="nowrap" width="20%" align="left" class="clearFormat" style="padding-left:5px; padding-right:5px; background:#666; border-left: 1px solid #666; background:url(../_images/table-header-black.jpg); height:26px; color: #FFF; font-weight:bold; vertical-align:middle;">                       
                    	Issue #
                    </td>
					<td nowrap="nowrap" width="20%" align="left" class="clearFormat" style="padding-left:5px; padding-right:5px; background:#666; border-left: 1px solid #666; background:url(../_images/table-header-black.jpg); height:26px; color: #FFF; font-weight:bold; vertical-align:middle;">                       
                    	Status
                    </td>
					<td nowrap="nowrap" width="20%" align="left" class="clearFormat" style="padding-left:5px; padding-right:5px; background:#666; border-left: 1px solid #666; background:url(../_images/table-header-black.jpg); height:26px; color: #FFF; font-weight:bold; border-right:1px solid #666; vertical-align:middle;">                       
                    	Investigator
                    </td>
                </tr>
            </table>                           
            <!-- STOP Grid Header and Pagination -->

            <!-- START Investigation Master Table -->
            <table id="invMaterTable" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="border-left:1px solid #969696; border-right:1px solid #969696; border-top:none; padding:0px; margin:0px;" valign="top" nowrap="nowrap">

                        <!-- START Investigation Container -->        
                        <div id="AccordionContainer" class="AccordionContainer">

							<%							
                            'investigation count
                            invNumber = 0
                            
							'set diary entry template
							'NOT USED FOR NOW --> invDiaryEntry = "[" & session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname") & "]<BR>" & Now() & "<BR><BR>"
							
                            'set row color
                            invBGColor = col2
                            
                            do until rs.eof		
							
								'used for saving form elements
								arrInvID = arrInvID & rs("InvestigationID") & ","
			
                            %>
                                                    
                                <!-- Investigation Header -->
                                <div onclick="runAccordion(<% =invNumber %>);" style="padding-left:5px; padding-top:8px; padding-bottom:2px; background-color:<% =invBGColor %>; border-bottom:1px solid #969696; vertical-align:middle;">
                                    <div class="AccordionTitle" onselectstart="return false;">

	                                    <table width="100%" cellpadding="0" cellspacing="0">
                                    		<tr>
                                            	<td width="40%" style="padding:0px; margin:0px; border:none;">
                                                    <div style="float:left;">
                                                    	<img src="../_images/icons/16/explode.jpg" title="Open Investigation" width="15" height="15" border="0" align="absmiddle" style="margin-right:5px;"> 
													</div>
                                                    <div id="Accordion<% =invNumber %>Name">
														<%
                                                        if len(rs("name")) > 50 then 
                                                            response.write(left(rs("name"),50) & "...") 
                                                        else 
                                                            response.write(rs("name"))
                                                        end if
                                                        %>
                                                    </div>
												</td>
                                                <td width="20%" style="padding:0px; margin:0px; border:none;"><% =rs("crsid") %></td>                                                                                                
                                                <td width="20%" style="padding:0px; margin:0px; border:none;"><div id="Accordion<% =invNumber %>Status"><% response.write(colorStatusBox(rs("status")) & rs("status")) %></div></td>
                                                <td width="20%" style="padding:0px; margin:0px; border:none;">
													<div id="Accordion<% =invNumber %>Investigator">
														<%
                                                        'show with first and last name												
                                                        if len(rs("firstname")) > 0 then
                                                            response.write(rs("lastname") & ", " & rs("firstname"))
                                                        'last name only
                                                        elseif len(rs("lastname")) > 0 then
                                                            response.write(rs("lastname"))
                                                        elseif isNull(rs("logid")) then
                                                            response.write("&nbsp;")
                                                        end if
                                                        %>											
                                                    </div>		
                                                </td>                                                                                             
                                        	</tr>
                                        </table>
                                        
                                    </div>
                                </div>
                                
                                <!-- START Investigation Content -->
                                <div id="Accordion<% =invNumber %>Content" class="AccordionContent">
                
                                    <table id="investigation<% =invNumber %>:sub" width="100%" cellpadding="0" cellspacing="0">
                                        <tr> 
                                            <td height="4" nowrap="nowrap" style="background-color:#C7CDE9; border-top:none; padding:0px; margin:0px;"><img src="../_images/x_cleardot.gif" width="1" height="1" /></td>
                                        </tr>      
                                        <tr>
                                            <td align="center" style="border:0px; padding:0px; margin:0px;">
                                          
                                                <table class="formTable" width="100%" cellpadding="0" cellspacing="0">

                                                    <!-- Investigation Name -->
                                                    <tr> 
                                                        <td class="formLabel" style="border:0px;">Name:</td>
                                                        <td align=left style="border:0px;">
                                                            <input name="rptInvName<% =invNumber %>" id="rptInvName<% =invNumber %>" type=text class="inputExtraLong" onkeyup="update_invname(this,'<% =invNumber %>')" value="<% =rs("name") %>" size=10 maxlength=255>
                                                        </td>          	
                                                    </tr>      
                                                    
                                                    <!-- Investigation Status -->
                                                    <tr>
                                                        <td class="formLabel">
	                                                        <!-- DBAdmins and Program Admins -->
															<% if programAdmin = "Y" or cLng(session(session("siteID") & "adminLoggedOn")) < 2 then %>
                                                            	<a href="#" class="formLabelLinkImg" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=customers&field=pInvestigationStatus&form=rptInvStatus<% =invNumber %>', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_InvStatus<% =invNumber %>','over');" onmouseout="javascript:configImage('congif_InvStatus<% =invNumber %>','out');"><img id="congif_InvStatus<% =invNumber %>" src="../_images/x_cleardot_16.gif" align="absmiddle" border="0" /></a>
																<a href="#" class="formLabelLink" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=customers&field=pInvestigationStatus&form=rptInvStatus<% =invNumber %>', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_InvStatus<% =invNumber %>','over');" onmouseout="javascript:configImage('congif_InvStatus<% =invNumber %>','out');"><span class="emailTrigger">e</span>Status:</a>
                                                            <% else %>
                                                                <span class="emailTrigger">e</span>Status:
                                                            <% end if %>                                                                                                            
                                                        </td>
                                                        <td align=left>           
															<select name="rptInvStatus<% =invNumber %>" id="rptInvStatus<% =invNumber %>" onchange="update_status('<% =invNumber %>')" size=1 class="inputMediumLong" >
																<%									
																labelX = 0
																for labelX = 0 to UBound(optStatusArr)
																	response.write "<option value='" & optStatusArr(labelX) & "' " & checkMatch(rs("status"), optStatusArr(labelX)) & ">" & optStatusArr(labelX) & "</option>"
																next
																%>
															</select>                                                                            
                                                            <div class="subLabel" style="padding-top:5px;">All assigned users will be notified on status changes.</div>
                                                        </td>
                                                    </tr>
                                                    
                                                                              
                                                    <!-- Investigation Manager -->
                                                    <tr>
                                                        <td class="formLabel"><span class="emailTrigger">e</span>Investigator:</td>
                                                        <td align="left" style="padding-bottom:0px;"> 
							                              	<div style="float:left; margin-right:5px;">                                                                                                               
																<%															
                                                                'build combo box					
                                                                labelX = 0										
                                                                response.write("<select name=""rptInvManager" & invNumber & """ id=""rptInvManager" & invNumber & """ onchange=""update_investigator('" & invNumber & "')"" size=1 class=""inputLong"">")
                                                                response.write("  <option value="""" " & checkEmpty(rs("logid")) & ">-- Select --</option>")																														
                                                                for labelX = 0 to UBound(optInvestigatorsArr)
                                                                    if len(optInvestigatorsArr(labelX,0)) > 0 then
                                                                        response.write("<option value='" & optInvestigatorsArr(labelX,0) & "' " & checkMatch(rs("logid"),optInvestigatorsArr(labelX,0)) & ">" & optInvestigatorsArr(labelX,1) & "</option>")
                                                                    end if
                                                                next
                                                                response.write("</select>")                                                           	
                                                                %>        
                                                            </div>
                                                        </td>      
                                                    </tr>
                                                    <tr>
                                                        <td style="border-top:none; padding-bottom:5px;">&nbsp;</td>
                                                        <td align=left nowrap style="border-top:none; padding:0px;">                               
                                                            <div class="subLabel" style="padding-top:5px;">Case Manager responsible for this investigation.</div>
                                                        </td>
                                                    </tr>
                                        
                                                    
                                                    <!-- Investigation Category -->
                                                    <tr> 
                                                        <td class="formLabel">Category:</td>
                                                        <td align=left>
															<%															
                                                        	'build combo box					
                                                        	labelX = 0										
                                                          	response.write("<select name=""rptInvCategory" & invNumber & """ id=""rptInvCategory" & invNumber & """ size=1 class=""inputLong"">")
                                                          	response.write("  <option value="""" " & checkEmpty(rs("categoryid")) & ">-- Select --</option>")																														
                                                        	for labelX = 0 to UBound(optCategoryArr)
                                                          		if len(optCategoryArr(labelX,0)) > 0 then
                                                           			response.write("<option value='" & optCategoryArr(labelX,0) & "' " & checkMatch(rs("categoryid"),optCategoryArr(labelX,0)) & ">" & optCategoryArr(labelX,1) & "</option>")
                                                            	end if
                                                        	next
                                                          	response.write("</select>")                                                           	
                                                        	%>        
                                                            <div class="subLabel" style="padding-top:5px;">Scope of investigation.</div>
                                                        </td>          	
                                                    </tr>
                                                            
                                                    <!-- Investigation Date Added -->
                                                    <tr> 
                                                        <td class="formLabel">Start Date:</td>
                                                        <td align=left>
                                                        	<% 
															'used to activate DHTML calendars
															activeCalendars = "'" & "date:rptInvDateAdded" & invNumber & "'," & activeCalendars 
															%>
                                                            <input name="rptInvDateAdded<% =invNumber %>" id="date:rptInvDateAdded<% =invNumber %>" type=text class="inputShort" value="<% =rs("dateadded") %>" size=10 maxlength=10>
                                                            <div class="subLabel" style="padding-top:5px;">Date investigation began.</strong></div>
                                                        </td>          	
                                                    </tr>      

                                                    <!-- Investigation Date Due -->
                                                    <tr> 
                                                        <td class="formLabel"><span class="emailTrigger">e</span>Deadline:</td>
                                                        <td align=left>
                                                        	<% 
															'used to activate DHTML calendars
															activeCalendars = "'" & "date:rptInvDeadline" & invNumber & "'," & activeCalendars 
															%>
                                                            <input name="rptInvDeadline<% =invNumber %>" id="date:rptInvDeadline<% =invNumber %>" type=text class="inputShort" value="<% =rs("datedeadline") %>" size=10 maxlength=10>
                                                           	<div class="subLabel" style="padding-top:5px;">Date investigation is due to be completed: 
                                                            	<%
                                                                if len(useInvDeadlineDays)<=0 or useInvDeadlineDays="" or isNull(useInvDeadlineDays) then
                                                                    response.write("<i>&lt;default not set&gt;</i>")
                                                                else
                                                                    response.write("<strong>" & useInvDeadlineDays & " days</strong>")
                                                                end if														
                                                               %>
                                                            </div>                                                            
                                                        </td>          	
                                                    </tr>      

                                                    <!-- Investigation Date Closed -->
                                                    <tr> 
                                                        <td class="formLabel">Date Closed:</td>
                                                        <td align=left>
                                                        	<% 
															'used to activate DHTML calendars
															activeCalendars = "'" & "date:rptInvCloseDate" & invNumber & "'," & activeCalendars 
															%>
                                                            <input name="rptInvCloseDate<% =invNumber %>" id="date:rptInvCloseDate<% =invNumber %>" type=text class="inputShort" value="<% =rs("dateclosed") %>" size=10 maxlength=10>
                                                            <div class="subLabel" style="padding-top:5px;">Date investigation status was set to: <strong><% =pInvestigationStatusCloseOn %></strong></div>
                                                        </td>          	
                                                    </tr>      
                                                    
                                                    <!-- Investigation ActionTaken -->                                        
                                                    <tr> 
                                                        <td class="formLabel">
	                                                        <!-- DBAdmins and Program Admins -->
															<% if programAdmin = "Y" or cLng(session(session("siteID") & "adminLoggedOn")) < 2 then %>
                                                            	<a href="#" class="formLabelLinkImg" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=customers&field=pActionTaken&form=rptAction<% =invNumber %>', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_Action<% =invNumber %>','over');" onmouseout="javascript:configImage('congif_Action<% =invNumber %>','out');"><img id="congif_Action<% =invNumber %>" src="../_images/x_cleardot_16.gif" align="absmiddle" border="0" /></a>
	                                                       		<a href="#" class="formLabelLink" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=customers&field=pActionTaken&form=rptAction<% =invNumber %>', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_Action<% =invNumber %>','over');" onmouseout="javascript:configImage('congif_Action<% =invNumber %>','out');">Action Taken:</a>
                                                            <% else %>
                                                                Action Taken:
                                                            <% end if %>                                                                                                                                                                        
                                                        </td>
                                                        <td align=left>
                                                        	<%
															call buildSelect(optActionArr,rs("actiontaken"),"rptAction" & invNumber,"inputLong","Y")
                                                           	%>
                                                            <div class="subLabel" style="padding-top:5px;">Corrective action or sanctions taken at close of investigation.</div>
                                                        </td>          	
                                                    </tr>


                                                    <!-- Investigation Substantiated -->                                        
                                                    <tr> 
                                                     	<td class="formLabel">
	                                                        <!-- DBAdmins and Program Admins -->
															<% if programAdmin = "Y" or cLng(session(session("siteID") & "adminLoggedOn")) < 2 then %>
                                                            	<a href="#" class="formLabelLinkImg" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=customers&field=pValidity&form=rptSubstantiated<% =invNumber %>', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_Validity<% =invNumber %>','over');" onmouseout="javascript:configImage('congif_Validity<% =invNumber %>','out');"><img id="congif_Validity<% =invNumber %>" src="../_images/x_cleardot_16.gif" align="absmiddle" border="0" /></a>
	                                                        	<a href="#" class="formLabelLink" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=customers&field=pValidity&form=rptSubstantiated<% =invNumber %>', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_Validity<% =invNumber %>','over');" onmouseout="javascript:configImage('congif_Validity<% =invNumber %>','out');">Validity:</a>
                                                            <% else %>
                                                                Validity:
                                                            <% end if %>                                                                                                                
                                                        </td>
                                                      	<td align=left>                                                           
                                                       		<%
															call buildSelect(optValidityArr,rs("substantiated"),"rptSubstantiated" & invNumber,"inputLong","Y")
                                                           	%>
	                                                        <div class="subLabel" style="padding-top:5px;">Signifies if investigation was valid and substantiated.</div>                                                                                                                        
                                                      	</td>
                                                    </tr>


                                                    <!-- Investigation RiskLevel -->                                        
                                                    <tr> 
                                                        <td class="formLabel">
	                                                        <!-- DBAdmins and Program Admins -->
															<% if programAdmin = "Y" or cLng(session(session("siteID") & "adminLoggedOn")) < 2 then %>
                                                            	<a href="#" class="formLabelLinkImg" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=customers&field=pRiskLevel&form=rptRiskLevel<% =invNumber %>', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_RiskLevel<% =invNumber %>','over');" onmouseout="javascript:configImage('congif_RiskLevel<% =invNumber %>','out');"><img id="congif_RiskLevel<% =invNumber %>" src="../_images/x_cleardot_16.gif" align="absmiddle" border="0" /></a>
	                                                        	<a href="#" class="formLabelLink" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=customers&field=pRiskLevel&form=rptRiskLevel<% =invNumber %>', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_RiskLevel<% =invNumber %>','over');" onmouseout="javascript:configImage('congif_RiskLevel<% =invNumber %>','out');">Risk Level:</a>
                                                            <% else %>
                                                                Risk Level:
                                                            <% end if %>
                                                        </td>
                                                        <td align=left>
                                                       		<%
															call buildSelect(optRiskArr,rs("risklevel"),"rptRiskLevel" & invNumber,"inputLong","Y")
                                                           	%>
                                                            <div class="subLabel" style="padding-top:5px;">Issues's potential risk to the company.</div>
                                                        </td>          	
                                                    </tr>


                                                    <!-- Investigation User Fields -->                                                      
                                                    <%
													'FIELD #1
                                                    'make sure there are fields to show			
                                                    if len(pInvField1) > 0 then
                                                        call dispUFields(pInvField1,"pInvField1Value","rptInvField1" & invNumber,rs("InvField1"),pInvField1Type)																			
                                                    'no inv user fields have been setup								
                                                    else																	
                                                        response.Write("<input type=""hidden"" name=""rptInvField1" & invNumber & """ id=""rptInvField1" & invNumber & """ value=""" & rs("InvField1") & """ >")
                                                    end if

													'FIELD #2
                                                    'make sure there are fields to show			
                                                    if len(pInvField2) > 0 then
                                                    	call dispUFields(pInvField2,"pInvField2Value","rptInvField2" & invNumber,rs("InvField2"),pInvField2Type)																			
                                                    'no inv user fields have been setup								
                                                    else																	
                                                        response.Write("<input type=""hidden"" name=""rptInvField2" & invNumber & """ id=""rptInvField2" & invNumber & """ value=""" & rs("InvField2") & """ >")
                                                    end if

                                                    %>

												</table>
                                                
                                                <table class="formTable" width="100%" cellpadding="0" cellspacing="0">
                                                                    
                                                    <!-- Investigation Notes -->
                                                    <tr> 
                                                        <td align="left" style="padding-left:20px; padding-right:30px;">
                                                            <div style="padding:3px;">
                                                            	<span style="color:#35487B; font-weight:bold; margin-right:5px;">Notes</span><span class="subLabel">(Investigation notes and discovery)</span>
                                                            </div> 
                                                            <!-- TinyMCE init parameter -->
                                                            <% activeNotesElements = activeNotesElements & ",rptInvDiary" & invNumber %>                                                           
                                                            <textarea name="rptInvDiary<% =invNumber %>" id="rptInvDiary<% =invNumber %>" style="width:100%; height:150px;" ><%=server.HTMLEncode(rs("notes") & "")%></textarea>
                                                        </td>
                                                    </tr>

												</table>
                                                
                                                <table class="formTable" width="100%" cellpadding="0" cellspacing="0">

                                                    <!-- Investigation Outcome -->
                                                    <tr> 
                                                        <td align="left" style="padding-left:20px; padding-right:30px;">
                                                            <div style="padding:3px;">
                                                            	<span style="color:#35487B; font-weight:bold; margin-right:5px;">Outcome</span><span class="subLabel">(Internal resolution, not provided to reporter)</span>
                                                            </div>
                                                            <!-- TinyMCE init parameter -->
                                                            <% activeNotesElements = activeNotesElements & ",rptInvOutcome" & invNumber %>
                                                            <textarea name="rptInvOutcome<% =invNumber %>" id="rptInvOutcome<% =invNumber %>" style="width:100%; height:75px;" ><%=server.HTMLEncode(rs("outcome") & "")%></textarea>
                                                        </td>
                                                    </tr>
                                                               
                                                </table>                
                                                          
                                            </td>
                                        </tr>
                
                                        <!-- Bottom buttons close/delete -->
                                        <tr> 
                                            <td height="30" nowrap="nowrap" style="background-color:#C7CDE9; border-top:1px solid #969696; border-bottom:none; padding:0px; margin:0px;">
                                                <div style="float:left; padding-top:4px; padding-left:5px;">
                                                    <a class="myCMbutton" href="#" onclick="this.blur(); runAccordion(<% =invNumber %>); resetSystemMsg(); return false;"><span class="colapse" style="padding-right:10px;">Close</span></a>
                                                    <a class="myCMbutton" href="#" onClick="confirmPrompt('Confirm Delete','This action can not be undone.<br><br>Are you sure you want to delete this investigation?','redirect','../scripts/issues_exec.asp?recID=<% =idIssue %>&action=delinv&invID=<% =rs("InvestigationID") %>&top=notes&side=investigation'); return false;"><span class="delete" style="padding-right:10px;">Delete</span></a>                                                    
                                                </div>
                                            </td>
                                        </tr>      
                        
                                    </table>
                                    <!-- STOP Investigation Content table -->
                
                                </div>
                                <!-- STOP Investigation Line Item --> 


								<%
                                'Switch Row Color
                                if invBGColor = col2 then invBGColor = col1 else invBGColor = col2
    
                                invNumber = invNumber + 1
                                
                                'move to next investigation
                                rs.movenext
                                
                            loop						
                            %>
                                                    
                        </div>  
                        <!-- STOP Investigation Container -->
            
                    </td>
                </tr>
            </table>                           
			<!-- STOP Investigation Master Table -->

			<!-- START shanding table -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
			  		<td class="clearFormat" style="background:#D0D0D0" height="5px;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
				</tr>
			</table>
			<!-- STOP shanding table -->
                        
			<!-- FORM ELEMENT SAVING... -->
			<input name="arrInvestigation" id="arrInvestigation" type="hidden" value="<% =arrInvID %>" />
        
        <%
		else
		%>

            <!-- START Investigation Header -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
					<td nowrap="nowrap" width="40%" align="left" class="clearFormat" style="padding-left:5px; padding-right:5px; background:#666; border-left: 1px solid #666; background:url(../_images/table-header-black.jpg); height:26px; color: #FFF; font-weight:bold; vertical-align:middle;">                       
                    	Name
                    </td>
					<td nowrap="nowrap" width="20%" align="left" class="clearFormat" style="padding-left:5px; padding-right:5px; background:#666; border-left: 1px solid #666; background:url(../_images/table-header-black.jpg); height:26px; color: #FFF; font-weight:bold; vertical-align:middle;">                       
                    	Issue #
                    </td>
					<td nowrap="nowrap" width="20%" align="left" class="clearFormat" style="padding-left:5px; padding-right:5px; background:#666; border-left: 1px solid #666; background:url(../_images/table-header-black.jpg); height:26px; color: #FFF; font-weight:bold; vertical-align:middle;">                       
                    	Status
                    </td>
					<td nowrap="nowrap" width="20%" align="left" class="clearFormat" style="padding-left:5px; padding-right:5px; background:#666; border-left: 1px solid #666; background:url(../_images/table-header-black.jpg); height:26px; color: #FFF; font-weight:bold; border-right:1px solid #666; vertical-align:middle;">                       
                    	Investigator
                    </td>
                </tr>
            </table>                           
            <!-- STOP Grid Header and Pagination -->

			<!-- START shanding table -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
			  		<td class="clearFormat" style="background:#D0D0D0" height="5px;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
				</tr>
			</table>
			<!-- STOP shanding table -->
        
            <!-- show INFO that NO investigations were available -->
			<table width="100%" cellpadding="0" cellspacing="0" style="margin-top:10px;">
				<tr>
					<td align="left" class="clearFormat" style="padding-right:2px;">
						<% call systemMessageBox("investigationsID","statusMessageINFO","No investigations found. Click ""Add"" below.") %>
					</td>
				</tr>
			</table>
		
		<%
		'close the assurance there were investigations to display
		end if		
		
        call closeRS(rs)
        %>
        <!-- STOP Building/loading investigations -->
                                            

		<script language="javascript">
            function addInvestigation() {  
                var tbl = "invMaterTable";
                var tbody = document.getElementById(tbl).getElementsByTagName("tbody")[0];
                var row = document.createElement("tr");
                var td1 = document.createElement("td");
                
                row.appendChild(td1);
                tbody.appendChild(row);	            
            }
        </script>


		<script>                    
			//rename investigation name as user types
			//whatever is typed in the obj is reflected in #Accordion
			//truncates name to 50 characters to it fits in grid
			function update_invname(obj,investigation) {
				if ($(obj).attr('value').length > 50 ) {				
					$('#Accordion' + investigation + 'Name').text( $(obj).attr('value').substring(0,50)+'...' ); 
				}
				else {
					$('#Accordion' + investigation + 'Name').text( $(obj).attr('value') ); 					
				}
			}

			//show newly assigned investigator
			//whatever is selected in rptInvStatus is reflected in #Accordion
			function update_status(investigation) {
				if ($('#rptInvStatus' + investigation + ' :selected').text()=='Active' || $('#rptInvStatus' + investigation + ' :selected').text()=='Open') {
					$('#Accordion' + investigation + 'Status').html( "<img src=\"../_images/icons/12/clear-box.png\" class=\"colorBox-Red\" align=\"absmiddle\" width=\"12\" height=\"12\" />" + $('#rptInvStatus' + investigation + ' :selected').text() );
				}
				else if ($('#rptInvStatus' + investigation + ' :selected').text()=='Closed' || $('#rptInvStatus' + investigation + ' :selected').text()=='Complete') {
					$('#Accordion' + investigation + 'Status').html( "<img src=\"../_images/icons/12/clear-box.png\" class=\"colorBox-Green\" align=\"absmiddle\" width=\"12\" height=\"12\" />" + $('#rptInvStatus' + investigation + ' :selected').text() );
				}
				else {
					$('#Accordion' + investigation + 'Status').html( "<img src=\"../_images/icons/12/clear-box.png\" class=\"colorBox-Yellow\" align=\"absmiddle\" width=\"12\" height=\"12\" />" + $('#rptInvStatus' + investigation + ' :selected').text() );
				}
			}

			//show newly assigned investigator
			//whatever is selected in rptInvManager is reflected in #Accordion
			function update_investigator(investigation) {
				if ($('#rptInvManager' + investigation + ' :selected').text()=='-- Select --') {
					$('#Accordion' + investigation + 'Investigator').text( $('#rptInvManager' + investigation).attr('value') );
				}
				else {
					$('#Accordion' + investigation + 'Investigator').text( $('#rptInvManager' + investigation + ' :selected').text() );
				}				
			}
		</script>                  
