<%
'*************************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'*************************************************************************


'*************************************************************************
'SYSTEM Notice BOX - formatted box with icon, use the classes below
' styleClass = 	statusMessageOK
'				statusMessageINFO
'				statusMessageALERT
'				statusMessageERROR
'*************************************************************************
function systemMessageBox(elementID,styleClass,strMessage)

	'the Element ID MUST be set because some Javascript functions hide
	'the element when buttons/tabs are clicked.
	if len(trim(strMessage)) > 0 then
    
		response.write("<div id=""" & elementID & """ class=""" & styleClass & """>")
		response.write("	<div class=""innerBorder"">")
		response.write("    	<div class=""pad"">")
		response.write("    		<div class=""icon"" style=""float:left;""></div>")
		response.write("        	<div style=""margin-left:24px; margin-bottom:0px; text-align:left; height:100%; "">")
		response.write("            	<span class=""title"">Notice</span><br><span id=""systemMessageTxt"" class=""description"">" & strMessage & "</span>")
		response.write("            </div>")
		response.write("    	</div>")
		response.write("	</div>")
		response.write("</div>")    

	else
		response.write("&nbsp;")
	end if						
	
end function

function clearSessionMessages()

	session(session("siteID") & "okMsg") = ""
	session(session("siteID") & "errMsg") = ""
	session(session("siteID") & "infoMsg") = ""
	
end function

'**********************************************************************
'Determine Status and set box and background color
'**********************************************************************
function colorStatusBox(statusText)

	 if lcase(statusText) = "open" or lcase(statusText) = "active" then
		response.Write("<img src=""../_images/icons/12/clear-box.png"" class=""colorBox-Red"" align=""absmiddle"" width=""12"" height=""12"" style=""vertical-align:middle;"" />")			

	 elseif lcase(statusText) = "closed" or lcase(statusText) = "complete" then
		response.Write("<img src=""../_images/icons/12/clear-box.png"" class=""colorBox-Green"" align=""absmiddle"" width=""12"" height=""12"" style=""vertical-align:middle;"" />")

	 else
		if len(statusText) > 0 then
			response.Write("<img src=""../_images/icons/12/clear-box.png"" class=""colorBox-Yellow"" align=""absmiddle"" width=""12"" height=""12"" style=""vertical-align:middle;"" />")
		end if
	 end if

end function


'***********************************************
'special functions to reformat values and labels
function reformatSeverity(v)

dim tmpValue

	if v = "1" then
		tmpValue = v & "-High"
	elseif v = "2" then
		tmpValue = v & "-Med"
	elseif v = "3" then
		tmpValue = v & "-Low"
	else
		tmpValue = v
	end if

	reformatSeverity = tmpValue

end function							


'***********************************************
'special functions to reformat values and labels
function reformatHierarchy(v,style)

dim tmpValue
dim arrLoc, locI, locIndent

	tmpValue = v

	'remove any location hierarchy ending with "space >> space"
	'--levels 1 to 8
	if right(tmpValue,3) = "*|*" then tmpValue = left(tmpValue,len(tmpValue)-3)
	if right(tmpValue,3) = "*|*" then tmpValue = left(tmpValue,len(tmpValue)-3)
	if right(tmpValue,3) = "*|*" then tmpValue = left(tmpValue,len(tmpValue)-3)
	if right(tmpValue,3) = "*|*" then tmpValue = left(tmpValue,len(tmpValue)-3)
	if right(tmpValue,3) = "*|*" then tmpValue = left(tmpValue,len(tmpValue)-3)
	if right(tmpValue,3) = "*|*" then tmpValue = left(tmpValue,len(tmpValue)-3)
	if right(tmpValue,3) = "*|*" then tmpValue = left(tmpValue,len(tmpValue)-3)
	if right(tmpValue,3) = "*|*" then tmpValue = left(tmpValue,len(tmpValue)-3)

	'user calling for formated list with tree image
	if style = "div" and len(tmpValue)>0 then
		'add extra "space>>space" so first value is also pulled
		tmpValue = "*|*" & tmpValue
		'set padding for each <div>
		locIndent = 0
		'split Hierarchy values
		arrLoc = split(tmpValue,"*|*")
		tmpValue = ""
		'Build list of location levels separated into DIVs
		for locI = 1 to UBound(arrLoc)
			tmpValue = tmpValue & "<div style=""padding-left:" & locIndent &  "px;""><img src=""../_images/icons/16/tree_level.gif"" width=""16"" height=""16"" align=""absmiddle"" style=""margin-right:3px;"" />" & trim(arrLoc(locI)) & "</div>"			
			locIndent = locIndent + 5
		next		
	
	'change to proper string display
	elseif style = "html" and len(tmpValue)>0 then
		'add extra "space>>space" so first value is also pulled
		tmpValue = "*|*" & tmpValue
		'set padding for each <div>
		locIndent = 0
		'split Hierarchy values
		arrLoc = split(tmpValue,"*|*")
		tmpValue = ""
		'Build list of location levels separated into DIVs
		for locI = 1 to UBound(arrLoc)
			tmpValue = tmpValue & "<a href=""#"" onclick=""this.blur(); navigateAway('../scripts/issues.asp?showField=hierarchy_search_" & locI & "&showValue=" & server.URLEncode(trim(arrLoc(locI))) & "'); return false;"" >" & trim(arrLoc(locI)) & "</a>" & "&nbsp;&raquo;&nbsp;"			
		next		
		'trim off last >> digits
		if right(tmpValue,19) = "&nbsp;&raquo;&nbsp;" then tmpValue = left(tmpValue,len(tmpValue)-19)

	'change to proper string display
	elseif style = "string" then	
		'add extra "space>>space" so first value is also pulled
		tmpValue = "*|*" & tmpValue
		'split Hierarchy values
		arrLoc = split(tmpValue,"*|*")
		tmpValue = ""
		'Build list of location levels separated into DIVs
		for locI = 1 to UBound(arrLoc)
			if len(trim(arrLoc(locI))) <= 0 then
				tmpValue = tmpValue & "empty" & "&nbsp;&raquo;&nbsp;"			
			else
				tmpValue = tmpValue & trim(arrLoc(locI)) & "&nbsp;&raquo;&nbsp;"			
			end if			
		next		
		'trim off last >> digits
		if right(tmpValue,19) = "&nbsp;&raquo;&nbsp;" then tmpValue = left(tmpValue,len(tmpValue)-19)
	
		'''tmpValue = replace(tmpValue,"*|*","&nbsp;&raquo;&nbsp;")
	
	'change to value to be stored in database
	elseif style = "value" then	
		tmpValue = tmpValue
	
	end if
	
	'return new value
	reformatHierarchy = tmpValue

end function							

'******************************************************************
function insertWhereClause(sql,where)

	'sql = lCase(sql)
	if len(sql) > 0 and instr(1,lCase(sql),"select") then
		if instr(1,lCase(sql),"where") then
			sql = replace(sql,"WHERE", "where") 			'<-- replace statements are CASE SENSITIVE
			sql = replace(sql,"Where", "where")				'<-- replace statements are CASE SENSITIVE
			sql = replace(sql,"where", "where " & where & " and ")
			
		else
			if instr(1,lCase(sql),"group by") then
				sql = replace(sql,"GROUP BY", "group by") 	'<-- replace statements are CASE SENSITIVE
				sql = replace(sql,"Group By", "group by") 	'<-- replace statements are CASE SENSITIVE			
				sql = replace(sql,"group by", "where " & where & " group by ")
				
			elseif instr(1,lCase(sql),"having") then
				sql = replace(sql,"HAVING", "having") 		'<-- replace statements are CASE SENSITIVE
				sql = replace(sql,"Having", "having") 		'<-- replace statements are CASE SENSITIVE
				sql = replace(sql,"having", "where " & where & " having ")
				
			elseif instr(1,lCase(sql),"order by") then
				sql = replace(sql,"ORDER BY", "order by") 	'<-- replace statements are CASE SENSITIVE
				sql = replace(sql,"Order By", "order by") 	'<-- replace statements are CASE SENSITIVE			
				sql = replace(sql,"order by", "where " & where & " order by ")
				
			elseif instr(1,lCase(sql),"pivot") then
				sql = replace(sql,"PIVOT", "pivot") 		'<-- replace statements are CASE SENSITIVE
				sql = replace(sql,"Pivot", "pivot")			'<-- replace statements are CASE SENSITIVE			
				sql = replace(sql,"pivot", "where " & where & " pivot ")
				
			else
				sql = sql & " where " & where
			end if
		end if
		
	end if
							
	insertWhereClause = sql

end function

'******************************************************************
'Get values from Config user for Drop Downs throughout the app.
'RETURNS: Array with option values
'******************************************************************
function getDropDownOpt(optName,cusID)

'Declare Variables
dim mySQL, rsTemp, i
dim optValue

	'Get current configuration settings from database
	mySQL = "SELECT " & optName & " " _
		  & "FROM   Customer " _
		  & "WHERE  CustomerID='" & cusID & "' "
	'set rsTemp = openRSexecute(mySQL)
	set rsTemp = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if not rsTemp.eof then
		optValue = rsTemp(optName)
	end if
	call closeRS(rsTemp)
	
	'Build array if values were found
	if len(optValue) > 0 then
		getDropDownOpt = Split(optValue,vbCrLf)			
	else
		getDropDownOpt = ""
	end if
		
end function

'******************************************************************
'Get values from Config user for Drop Downs throughout the app.
'RETURNS: Array with option values
'******************************************************************
function getUserFieldGroup(optName)

'Declare Variables
dim mySQL, rsTemp, i

	'Get current configuration settings from database
	mySQL = "SELECT " & optName & " " _
		  & "FROM   Customer " _
		  & "WHERE  CustomerID='" & customerID & "' "
	set rsTemp = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if not rsTemp.eof then
		getUserFieldGroup = rsTemp(optName)
	else
		getUserFieldGroup = ""
	end if
	call closeRS(rsTemp)
			
end function

'******************************************************************
'Get values from Config user for Drop Downs throughout the app.
'RETURNS: Array with option values
'******************************************************************
function getCustomerName(cid)

'Declare Variables
dim mySQL, rsTemp, i

	'Get current configuration settings from database
	mySQL = "SELECT Name " _
		  & "FROM   Customer " _
		  & "WHERE  CustomerID='" & cid & "' "
	set rsTemp = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if not rsTemp.eof then
		getCustomerName = rsTemp("Name")
	else
		getCustomerName = ""
	end if
	call closeRS(rsTemp)
			
end function

'******************************************************************
'Get values from Config user for Drop Downs throughout the app.
'RETURNS: Array with option values
'******************************************************************
function getDropDownConfig(optName)

'Declare Variables
dim mySQL, rsTemp, i
dim optValue

	'Get current configuration settings from database
	mySQL = "SELECT configVar, configValLong " _
		  & "FROM   config " _
		  & "WHERE  adminType = 'C'"
	'set rsTemp = openRSexecute(mySQL)
	set rsTemp = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	do while not rsTemp.EOF
		select case trim(lCase(rsTemp("configVar")))
			case lCase(optName)
				optValue = rsTemp("configValLong")
		end select
		rsTemp.MoveNext
	loop
	call closeRS(rsTemp)
	
	'Build array if values were found
	if len(optValue) > 0 then
		getDropDownConfig = Split(optValue,vbCrLf)			
	else
		getDropDownConfig = ""
	end if
		
end function

'******************************************************************
'Get values from Config user for Drop Downs throughout the app.
'RETURNS: Array with option values
'******************************************************************
function getQuestion(questionID)

'Declare Variables
dim mySQL, rsTemp, i
dim optValue

	'find values associted with question for combo box
	mySQL = "SELECT QuestionID, Name, Value, Required " _
		  & "	FROM Question " _
		  & "	WHERE QuestionID = " & rs("QuestionID")
	set rsTemp = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if not rsTemp.eof then
		optValue = rsTemp("Value")
	end if
	call closeRS(rsTemp)
	
	'Build array if values were found
	if len(optValue) > 0 then
		getQuestion = Split(optValue,vbCrLf)			
	else
		getQuestion = ""
	end if
		
end function

'*********************************************************************
'Build select combo box
'*********************************************************************
function buildSelect(selectList,selectValue,selectName,selectLength,selectEnabled)

	dim optArrIndex, arrCategory, tempList
	dim noBlankEntryAtTop : noBlankEntryAtTop = "" ' --> "language,"
	
	'already array...just pass
	if isArray(selectList) then	
		arrCategory = selectList
	'not array...prepare for split	
	else
		if len(selectList)>0 then
			arrCategory = selectList
			'Build array if values were found
			if len(arrCategory) > 0 then										
				arrCategory = Split(selectList,vbCrLf)			
			else
				arrCategory = ""
			end if
		end if
	end if

	'disable select if necessary
	if uCase(selectEnabled) = "N" then
		selectEnabled = " disabled"
	else
		selectEnabled = ""	
	end if

	'build combo box
    response.write("<select name=""" & selectName & """ id=""" & selectName & """ size=1 class=""" & selectLength & """ " & selectEnabled & ">")
	'check if this element should have blank starter entry
	if inStr(noBlankEntryAtTop,selectName) <= 0 then
		response.write("	<option value="""">-- Select --</option>")
   	end if
	if isArray(arrCategory) then											
   		for optArrIndex = 0 to UBound(arrCategory)
			'tempList is used to put array back to list to check on missing values
			tempList = tempList & "," & arrCategory(optArrIndex)
			'make sure there is something to show before adding to combo box
			if len(trim(arrCategory(optArrIndex))) > 0 then
       			response.write("<option value='" & arrCategory(optArrIndex) & "' " & checkMatch(selectValue,arrCategory(optArrIndex)) & ">" & arrCategory(optArrIndex) & "</option>")
			end if
       	next
	else
		tempList = arrCategory
   	end if

	'check if source contains value...if NO then add to list
	if len(selectValue) > 0 then
		if inStr(tempList,selectValue) then
			'do nothing...
		else
			response.write("<option value='" & selectValue & "' selected>Missing:" & selectValue & "</option>")
		end if
	end if	
			
	'close/end select
  	response.write("</select>")

end function


'*********************************************************************
'Check if str1 returns empty "selected" if it does
'*********************************************************************
function checkEmpty(str1)
	if len(str1) <=0 or str1 = "" or IsNull(str1) then
		checkEmpty = " selected "
	else
		checkEmpty = ""
	end if
end function

'*********************************************************************
'Check if str1 and str2 matches and return "selected" if they do
'*********************************************************************
function checkMatch(str1,str2)
	if lCase(trim(str1)) = lCase(trim(str2)) then
		checkMatch = " selected "
	else
		checkMatch = ""
	end if
end function

'*********************************************************************
'Check if str1 and str2 matches and return "selected" if they do
'*********************************************************************
function checkExists(list,str1)
	if len(str1) > 0 then
		if inStr(list,str1) then
			checkExists = true
		else
			checkExists = false
		end if
	else
		checkExists = true
	end if	
end function

'*********************************************************************
'Check if str1 and str2 matches and return "selected" if they do
'*********************************************************************
function checkRadio(str1,str2)
	if lCase(trim(str1)) = lCase(trim(str2)) then
		checkRadio = " checked "
	else
		checkRadio = ""
	end if
end function

'*********************************************************************
'Check if str1 is found in str2 matches and return "checked" if so
'*********************************************************************
function checkInStr(str1,str2)
	
	if instr(1,str2,str1) then
		checkInStr = " checked "
	else
		checkInStr = ""
	end if
	
end function

'**********************************************************************
'change file size display
'**********************************************************************
function filesize(bytes) 

    Filesize = bytes / 1024 
    Filesize = Round(Filesize, 2) 

        If FileSize < 0.5 Then 
            Filesize = Filesize * 1024 
            Filesize = Int(FileSize) 
            Filesize = Filesize & " Bytes" 

        ElseIf Filesize > 1024 Then 
            FileSize = Filesize / 1024 
            FileSize = Round(FileSize, 2) 
            FileSize = Filesize & " Mb" 

        Else 
            filesize = filesize & " Kb" 
     
        END IF 

end function 

'**********************************************************************
'Display User Fields
'**********************************************************************
sub dispUFields(FieldName,FieldSelect,FormName,FormValue,FieldType)

dim showGroup
dim selectEnabled, inputEnabled

	if len(FieldName) > 0 then
	
		'get field values, if any
		optArr = getDropDownOpt(FieldSelect,customerID)
		
		'get groups who can view this UDF --> Change pUserField1[Value] to pUserField1[Group]
		showGroup = getUserFieldGroup(replace(FieldSelect,"Value","Group"))
		
		'do we show all built User Fields regardless of Group Assignment?
		'if a group is not assigend to a field it will be disabled
		if appShowUnassignedUserFields = "Y" or inStr(showGroup & "",userGroup & "") > 0 then
		
			'disable unassigned User Fields
			if inStr(showGroup & "",userGroup & "") <= 0 or isNull(showGroup) then
				selectEnabled = "N"
				inputEnabled = " disabled "
			else				
				selectEnabled = "Y"
				inputEnabled = ""				
			end if			
			%>
            
			<tr valign="top"> 
				<td class="formLabel">
					<%
					if isArray(optArr) then
						'only program admins can change option lists
						if programAdmin = "Y" or cLng(session(session("siteID") & "adminLoggedOn")) < 2 then							
							response.Write("<a href=""#"" class=""formLabelLinkImg"" title=""Configure Option"" onclick=""SimpleModal.open('../_dialogs/popup_config_options.asp?cid=" & customerID & "&type=udf&field=" & FieldSelect & "&form=" & FormName & "', 320, 375, 'no'); StopTheTimer(); return false;"" onmouseover=""javascript:configImage('congif_" & FormName & "','over');"" onmouseout=""javascript:configImage('congif_" & FormName & "','out');""><img id=""congif_" & FormName & """ src=""../_images/x_cleardot_16.gif"" align=""absmiddle"" border=""0"" /></a>")
							response.Write("<a href=""#"" class=""formLabelLink"" title=""Configure Option"" onclick=""SimpleModal.open('../_dialogs/popup_config_options.asp?cid=" & customerID & "&type=udf&field=" & FieldSelect & "&form=" & FormName & "', 320, 375, 'no'); StopTheTimer(); return false;"" onmouseover=""javascript:configImage('congif_" & FormName & "','over');"" onmouseout=""javascript:configImage('congif_" & FormName & "','out');"">" & FieldName & ":</a>")
						else
							response.Write(FieldName & ":")
						end if
					else
						response.Write(FieldName & ":")
					end if
					%>
				</td>
				<td align=left>
				<%
				'build combo box					
				if isArray(optArr) then
					call buildSelect(optArr,FormValue,FormName,"inputLong",selectEnabled)
				
				'no combo, just box
				else
					if lCase(FieldType) = "text" then
						response.Write("<input name=""" & FormName & """ id=""" & FormName & """ type=text class=""inputExtraLong"" value=""" & server.HTMLEncode(FormValue & "") & """ size=10 maxlength=255 " & inputEnabled & " >")
						response.write("&nbsp;<img title=""Clean Text"" style=""cursor:pointer;"" onclick=""cleanText('" & FormName & "');"" src=""../_images/icons/16/sort-alphabet.png"" align=""absmiddle"" />")
					
					elseif lCase(FieldType) = "date" then					
						'used for active calendar control
						activeCalendars = "'" & FormName & "'," & activeCalendars
						response.Write("<input name=""" & FormName & """ id=""" & FormName & """ type=text class=""inputShort"" value=""" & server.HTMLEncode(FormValue & "") & """ size=10 maxlength=255 " & inputEnabled & ">")
						response.write("&nbsp;<img style=""cursor:pointer;"" onclick=""document.getElementById('" & FormName & "').onclick();"" src=""../_images/icons/16/calendar.png"" align=""absmiddle"" />")					
					
					elseif lCase(FieldType) = "datetoday" then
						'used for active calendar control
						activeCalendarsToday = "'" & FormName & "'," & activeCalendarsToday
						response.Write("<input readonly=""readonly"" name=""" & FormName & """ id=""" & FormName & """ type=text class=""inputShort"" value=""" & server.HTMLEncode(FormValue & "") & """ size=10 maxlength=255 " & inputEnabled & ">")
						response.write("&nbsp;<img style=""cursor:pointer;"" onclick=""document.getElementById('" & FormName & "').onclick();"" src=""../_images/icons/16/calendar.png"" align=""absmiddle"" />")

					elseif lCase(FieldType) = "wysiwig" then
						'set Wysiwig toolbar													
						activeNotesElements = activeNotesElements & "," & FormName
						response.Write("<textarea name=""" & FormName & """ id=""" & FormName & """ style=""width:85%; height:100px; margin-bottom:12px;"" " & inputEnabled & ">" & server.HTMLEncode(FormValue & "") & "</textarea>")
					
					else		
						response.Write("<textarea name=""" & FormName & """ id=""" & FormName & """ style=""width:85%; height:100px; margin-bottom:12px;"" " & inputEnabled & ">" & server.HTMLEncode(FormValue & "") & "</textarea>")
						response.write("&nbsp;<img title=""Clean Text"" style=""cursor:pointer;"" onclick=""cleanText('" & FormName & "');"" src=""../_images/icons/16/sort-alphabet.png"" align=""top"" />")
					end if			
				end if
				%>
				<br>
				</td>
			</tr>
            
			<%
		end if
		
	end if
	
end sub

'**********************************************************************
'Display Text Configuration Settings
'**********************************************************************
sub dispQFields(FieldName,QuestionID,FormName,FormValue,FieldType)

	if len(FieldName) > 0 then
	%>
	<tr valign="top">     	
      <td colspan="2" align=left>
        <div style="text-align:left; font-weight:bold; color:#35487B; padding-bottom:5px; white-space:normal;"><% =FieldName %>:</div>
		<div style="padding-left:5px;">
		<%
		optArr = getQuestion(QuestionID)
		if isArray(optArr) then
			'build combo box		
			call buildSelect(optArr,FormValue,FormName,"inputLong","Y")
		else
			response.Write("<textarea name=""" & FormName & """ id=""" & FormName & """ style=""width:85%; height:45px; margin-bottom:12px;"">" & server.HTMLEncode(FormValue & "") & "</textarea>")
			'response.Write("<input name=" & FormName & " type=text class=""inputExtraLong"" value=""" & server.HTMLEncode(FormValue & "") & """ size=10 maxlength=255>")
		end if
		%>
        </div>		
	  </td>
	</tr>
	<%
	end if
	
end sub


'**********************************************************************
'Determine if User can View or Edit this Report
'**********************************************************************
function getIssueTypeName(IssueTypeID)

	mySQL = "SELECT Name " _
		 &  "	FROM IssueType " _
		 &  "	WHERE IssueTypeID = " & IssueTypeID & " "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if rs.eof then
		getIssueTypeName = ""
		call closeRS(rs)	
		exit function
	else
		getIssueTypeName = rs("name")
	end if	

	call closeRS(rs)

end function


'**********************************************************************
'Determine if User can View or Edit this Report
'**********************************************************************
function userViewIssue(crsid,UserID)

	dim progressLevel, issueType
	
	'Set flag
	userViewIssue = False

	if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
		mySQL = "SELECT CRS.CRSID, CRS.Progress, CRS.IssueTypeID " _
			 &  "	FROM CRS " _
			 &  "	WHERE CRS.CRSID = '" & crsid & "' " _
			 &  "   ORDER BY CRS.CRSID ASC "
	else		
		'check users access to view this type of issue
		mySQL = "SELECT CRS.CRSID, CRS.Progress, CRS.IssueTypeID " _
			 &  "	FROM CRS INNER JOIN vwLogins_IssueType ON (CRS.CustomerID = vwLogins_IssueType.CustomerID) AND (CRS.IssueTypeID = vwLogins_IssueType.IssueTypeID) " _
			 &  "	WHERE CRS.CRSID = '" & crsid & "' AND vwLogins_IssueType.LOGID = " & sLogid & " " _
			 &  "   ORDER BY CRS.CRSID ASC "
			 
	end if		 
	mySQL = mySQL
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if rs.eof then
		call closeRS(rs)	
		exit function
	else
		progressLevel = rs("progress")
		issueType = rs("issuetypeid")
	end if	
	
	'dont show to user if CCI has not finished procressing issue
	if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and progressLevel <> "3"  then
		call closeRS(rs)	
		exit function
	'internal issues not allowed for RA/RS
	elseif cLng(session(session("siteID") & "adminLoggedOn")) >= 3 and cLng(session(session("siteID") & "adminLoggedOn")) < 10 and issueType = "3"  then
		call closeRS(rs)	
		exit function
	end if
	
	'keep going...
	
	'programAdmin and all CCI staff, exit its okay
	if (programAdmin = "Y") or (cLng(session(session("siteID") & "adminLoggedOn")) < 10) then
		userViewIssue = True
		exit function
	end if
	
	'look up issue to determine if user can view
	mySQL="SELECT CRSID, LogID " _	
	    & "FROM   CRS_Logins " _
	    & "WHERE  CRSID='" & crsid & "' AND LogID=" & UserID
'	    & "WHERE  CustomerID='" & sCustomerID & "' AND CRSID='" & crsid & "' AND LogID=" & UserID
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	'Report not found...check to see if they are the owner
	if rs.eof then
		call closeRS(rs)
		
		'look up issue to determine if user might be owner
		mySQL="SELECT CRSID, LogID " _	
			& "FROM   CRS " _
			& "WHERE  CustomerID='" & sCustomerID & "' AND CRSID='" & crsid & "' AND LogID=" & UserID
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

		' it's a NO GO...user not allowed to view issue
		if rs.eof then	
			call closeRS(rs)	
			exit function
		' shew, it's OK...user not a manager, but is the owner
		else
			userViewIssue = True
		end if			
		
	else
		userViewIssue = True
	end if

	call closeRS(rs)

end function


'*************************************************************************
'Money Formatter - Use Store LCID
'*************************************************************************
function moneyS(aNumber)
	if isNumeric(aNumber) then
		dim tempNumber
		tempNumber   = CDbl(aNumber)
		session.LCID = pSiteLCID	'User configured format
		moneyS		 = formatNumber(round(tempNumber,2),2)
		session.LCID = 1033			'Default format
	else
		moneyS = aNumber
	end if
end function
'*************************************************************************
'Money Formatter - Use Default LCID
'*************************************************************************
function moneyD(aNumber)
	if isNumeric(aNumber) then
		moneyD = formatnumber(round(aNumber,2),2)
	else
		moneyD = aNumber
	end if
end function
'*************************************************************************
'Date formatter
'*************************************************************************
function formatTheDate(strDate)
	if isDate(strDate) then
		dim tempDate
		tempDate	  = CDate(strDate)
		session.LCID  = pSiteLCID	'User configured format
		formatTheDate = formatDateTime(tempDate,vbShortDate)
		session.LCID  = 1033		'Default format
	else
		formatTheDate = strDate
	end if
end function
'*************************************************************************
'Scan list of error FieldNames for possible match
'*************************************************************************
function checkFieldError(byVal FieldName, array1)
	dim i
	FieldName = Lcase(FieldName)
	for i = 0 to Ubound(array1)
		if LCase(array1(i)) = FieldName then
			checkFieldError = "<font color=red>*</font>"
			exit for
		end if
	next
end function
'*************************************************************************
'Substitute empty or null strings with something else
'*************************************************************************
function emptyString(tempStr,replaceWith)
	if len(trim(tempStr))=0 or isEmpty(tempStr) or isNull(tempStr) then
		emptyString = replaceWith
	else
		emptyString = trim(tempStr)
	end if
end function
'******************************************************************
'Format values entered into HTML form fields to prevent cross-site 
'scripting and other malicious HTML.
'******************************************************************
function validHTML(aString)

	'Declare Variables
	dim tempString
	tempString = trim(aString)
	
	'Check for empty values
	if isNull(tempString) or isEmpty(tempString) or len(tempString) = 0 then
		validHTML = ""
		exit function
	end if

	'Clean up HTML
	tempString = replace(tempString,"<", " ")
	tempString = replace(tempString,">", " ")
	tempString = replace(tempString,"""","'")
	validHTML  = trim(tempString)
	
end function
'******************************************************************
'Format values inserted into SQL statements before executing the 
'SQL statement. This is to prevent SQL injection attacks, and to 
'ensure that certain characters are interpreted correctly.
'******************************************************************
function validSQLORIGINAL(aString,aType)

	'Declare Variables
	dim tempString
	tempString = trim(aString)
	
	'Check for empty values
	if isNull(tempString) or isEmpty(tempString) or len(tempString) = 0 then
		validSQL = ""
		exit function
	end if
	
	'Clean up SQL
	if lCase(tempString) = "null" then				'Nulls
		validSQL = tempString
	else
		select case trim(UCase(aType))
		case "I"									'Integer
			validSQL = CLng(tempString)
		case "D"									'Double
			validSQL = CDbl(tempString)
		case else									'Alphanumeric
			tempString = replace(tempString,"--"," ")
			tempString = replace(tempString,"=="," ")
			tempString = replace(tempString,";", " ")
			tempString = replace(tempString,"'","''")
			validSQL   = tempString
		end select
	end if
	
end function
'******************************************************************
'Format values inserted into SQL statements before executing the 
'SQL statement. This is to prevent SQL injection attacks, and to 
'ensure that certain characters are interpreted correctly.
'******************************************************************
function validSQL(aString,aType)

	'Declare Variables
	dim tempString
	tempString = trim(aString)

	'For extreme testing.
	dim BlackList, s
	BlackList = Array("--", ";", "/*", "*/", "@@",_
                "char", "nchar", "varchar", "nvarchar",_
                "alter", "begin", "cast", "create", "cursor",_
                "declare", "delete", "drop", "end", "exec",_
                "execute", "fetch", "insert", "kill", "open",_
                "select", "sys", "sysobjects", "syscolumns",_
                "table", "update")
		
	'Check for empty values
	if isNull(tempString) or isEmpty(tempString) or len(tempString) = 0 then
		validSQL = ""
		exit function
	end if
	
	'Clean up SQL
	if lCase(tempString) = "null" then				'Nulls
		validSQL = tempString
	else
		select case trim(UCase(aType))
		case "I"									'Integer
			validSQL = CLng(tempString)
		case "D"									'Double
			validSQL = CDbl(tempString)
		case else									'Alphanumeric
			tempString = replace(tempString,"--"," ")
			tempString = replace(tempString,"=="," ")
			tempString = replace(tempString,";", " ")
			tempString = replace(tempString,"'","''")
			validSQL   = tempString
		end select
	end if

  	' Check if the string contains any patterns in our black list
	if UCase(aType) = "X" then
		for each s in BlackList	  
			if ( inStr(tempString, s) <> 0 ) Then
				validSQL = ""
				Exit Function
			end If	  
		next
	end if
		
end function
'******************************************************************
'Check a string for invalid characters
'******************************************************************
function invalidChar(aString,alphaNum,addChars)

	dim i, checkChar

	invalidChar = true 'Assume invalid chars unless proven otherwise

	select case alphaNum
		case 1		'Alphanumeric [a-z, 0-9] is valid
			addChars = lCase("abcdefghijklmnopqrstuvwxyz0123456789" & addChars)
		case 2		'Numeric [0-9] is valid
			addChars = lCase("0123456789" & addChars)
		case 3		'Alpha [a-z] is valid
			addChars = lCase("abcdefghijklmnopqrstuvwxyz" & addChars)
		case else	'Only characters in addChar is valid
	end select
	
	for i = 1 to len(aString)
		checkChar = lCase(mid(aString,i,1))
		if inStr(addChars,checkChar) = 0 then
			invalidChar = true
			exit function
		end if
	next

	invalidChar = false
		
end function
'******************************************************************
'Convert Date to Integer
'******************************************************************
function dateInt(strDate)

	dim qYear, qMonth, qDay, qHour, qMin, qSec
	
	qYear   = year(strDate)
	qMonth  = left("00",2-len(datePart("m",strDate))) & datePart("m",strDate)
	qDay    = left("00",2-len(datePart("d",strDate))) & datePart("d",strDate)
	qHour   = left("00",2-len(datePart("h",strDate))) & datePart("h",strDate)
	qMin    = left("00",2-len(datePart("n",strDate))) & datePart("n",strDate)
	qSec    = left("00",2-len(datePart("s",strDate))) & datePart("s",strDate)
	
	dateInt = qYear & qMonth & qDay & qHour & qMin & qSec

end function
'******************************************************************
'Convert Integer to Date
'******************************************************************
function intDate(strDate)

	dim qYear, qMonth, qDay, qHour, qMin, qSec

	qYear   = left(strDate,4)
	qMonth  = mid(strDate,5,2)
	qDay    = mid(strDate,7,2)
	qHour   = "00"
	qMin    = "00"
	qSec    = "00"
	
	intDate = formatTheDate(qMonth & "/" & qDay & "/" & qYear)
	
end function
'******************************************************************
'Add delimiters to a date if used in an SQL statement
'******************************************************************
function addDateDel(dateVal)
	if dbType = 0 then					'MS Access
		dateVal = "#" & dateVal & "#"
	else								'SQL Server
		dateVal = "'" & dateVal & "'"
	end if
	addDateDel = dateVal
end function

'******************************************************************
'Determine Report/CRSID type
'EXAMPLE: 0307-CCI-10003-02 would return;
'		  reportType = 1
'******************************************************************
function reportType(idIssue)
	reportType = Mid(idIssue,InStr(6,idIssue,"-")+1,1)	
end function
'**********************************************************************
'Send copy of email to Master User / Program Administrator
'**********************************************************************
sub sendAdminEmailBCC(fromName, fromEmail, toEmail, subject, body, contType)

dim bccMessage

	bccMessage = "-----------------------------------------------------" _
				& vbCrLf _
				& "The following is a copy of an email that was sent to: " _
				& vbCrLf _
				& toEmail _
				& vbCrLf _
				& "-----------------------------------------------------" _	
				& vbCrLf _
				& vbCrLf
	bccMessage = bccMessage & body

	call sendmail (fromName, fromEmail, pEmailAdmin, subject, bccMessage, contType)

end sub

'******************************************************************
'Export data into a .CSV/Excel file
'******************************************************************
function exportCSV(mySQL)

dim rsTemp
dim x, y
dim fldValue

	'Open recordset
	'set rsTemp = openRSexecute(mySQL)
	set rsTemp = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	' Save Output as a CSV file 
	response.ContentType = "text/csv"
	response.AddHeader "Content-Disposition", "filename=query.csv;" 

	for x = 0 to rsTemp.fields.count-1 
		response.write """" & rsTemp.fields(x).name & ""","
	next 
	response.write vbCrLf

	while not rsTemp.EOF 
		for y = 0 to rsTemp.Fields.Count-1 
			fldValue = rsTemp.fields(y).value		
			'check for Text/Memo field
			if rsTemp.fields(y).type = adLongVarWChar then
				fldValue = stripHTML(fldValue)
			end if
			'Find all users in field
			if lCase(rsTemp.fields(y).name) = "logids" then
				fldValue = getUserName(fldValue)
			end if									
			response.write """" & fldValue & ""","
	 	Next 
	 	response.write vbCrLf 
	 	rsTemp.movenext 
	wend 

	call closeRS(rsTemp)

end function
'******************************************************************
'Export data into a .XLS file
'******************************************************************
function exportXML(mySQL)

dim rsTemp
Dim xmlDoc 

	'Very Important : Set the ContentType property of the Response object to text/xml.
	response.contentType = "text/xml"
	'Open recordset
	set rsTemp = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	'Persist the Recorset in XML format to the ASP Response object. 
	'The constant value for adPersistXML is 1.
	rsTemp.Save Response, 1

	call closeRS(rsTemp)

end function
'*************************************************************************
'Used to remove all HTML tags from field
'RETURNS: String stripHTML
'*************************************************************************
Function stripHTML(strHTML)

Dim objRegExp, strOutput

  Set objRegExp = New Regexp

  on error resume next
  
  objRegExp.IgnoreCase = True
  objRegExp.Global = True
  objRegExp.Pattern = "<(.|\n)+?>"
  
  'Replace all HTML tag matches with the empty string
  strOutput = objRegExp.Replace(strHTML, "")  
  
  'Replace all < and > with &lt; and &gt;
  strOutput = Replace(strOutput, "<", "&lt;")
  strOutput = Replace(strOutput, ">", "&gt;")
  strOutput = Replace(strOutput, "&nbsp;", " ")
  
  strOutput = Replace(strOutput, "&ldquo;", """")
  strOutput = Replace(strOutput, "&rdquo;", """")
  strOutput = Replace(strOutput, "&rsquo;", "'")
  
  strOutput = Replace(strOutput, vbcrlf, " ")  
  strOutput = Replace(strOutput, chr(10), " ")   
  strOutput = Replace(strOutput, chr(13), " ")   
  
  stripHTML = strOutput
  
  Set objRegExp = Nothing
  
end function
'*************************************************************************
'Used to remove all HTML tags from field
'RETURNS: String stripHTML
'*************************************************************************
Function stripHTML(strHTML)

Dim objRegExp, strOutput

  Set objRegExp = New Regexp

  on error resume next
  
  objRegExp.IgnoreCase = True
  objRegExp.Global = True
  objRegExp.Pattern = "<(.|\n)+?>"
  
  'Replace all HTML tag matches with the empty string
  strOutput = objRegExp.Replace(strHTML, "")  
  
  'Replace all < and > with &lt; and &gt;
  strOutput = Replace(strOutput, "<", "&lt;")
  strOutput = Replace(strOutput, ">", "&gt;")
  strOutput = Replace(strOutput, "&nbsp;", " ")
  
  strOutput = Replace(strOutput, "&ldquo;", """")
  strOutput = Replace(strOutput, "&rdquo;", """")
  strOutput = Replace(strOutput, "&rsquo;", "'")
  
  strOutput = Replace(strOutput, vbcrlf, " ")  
  strOutput = Replace(strOutput, chr(10), " ")   
  strOutput = Replace(strOutput, chr(13), " ")   
  
  stripHTML = strOutput
  
  Set objRegExp = Nothing
  
end function
'*************************************************************************
'Used to remove all limited HTML tags from field
'RETURNS: String stripHTML
'*************************************************************************
Function stripHTMLlimited(strHTML)

Dim objRegExp, strOutput

  Set objRegExp = New Regexp

  on error resume next
  
  objRegExp.IgnoreCase = True
  objRegExp.Global = True
  'objRegExp.Pattern = "<(.|\n)+?>"
  objRegExp.Pattern = "<(?!\/?(p|br)(?=>|\s?.*>))\/?.*?>"
  
  'Replace all HTML tag matches with the empty string
  strOutput = objRegExp.Replace(strHTML, "")  
  
  strOutput = Replace(strOutput, "<br>", vbcrlf)
  strOutput = Replace(strOutput, "</p>", vbcrlf)  
  
  
  'Replace all < and > with &lt; and &gt;
'  strOutput = Replace(strOutput, "<", "&lt;")
'  strOutput = Replace(strOutput, ">", "&gt;")
  strOutput = Replace(strOutput, "&nbsp;", " ")
  strOutput = Replace(strOutput, "<p>", "")
  
  strOutput = Replace(strOutput, "&ldquo;", """")
  strOutput = Replace(strOutput, "&rdquo;", """")
  strOutput = Replace(strOutput, "&rsquo;", "'")
    
  'strOutput = Replace(strOutput, vbcrlf, " ")  
  'strOutput = Replace(strOutput, chr(10), " ")   
  'strOutput = Replace(strOutput, chr(13), " ")   
  
  stripHTMLlimited = strOutput
  
  Set objRegExp = Nothing
  
end function
'*************************************************************************
'Used to remove all HTML tags from field
'RETURNS: String getUserName with all User Names found (i.e. Bryan,John)
'*************************************************************************
function getUserName(strLogIDs)

dim arrUsers
dim usersI
dim rsTemp, mySQL

	if len(strLogIDs) > 0 or strLogIDs <> "" or not IsNull(strLogIDs) then	
		arrUsers = split(strLogIDs,"*|*")
		'Build list of Users already assigned to report
		for usersI = 1 to UBound(arrUsers) - 1														
			'Build list of users to be included in listAvailableUsers
			mySQL = "SELECT LogID, FirstName, LastName " _
				  & "FROM   Logins a " _
				  & "WHERE  LogID = " & arrUsers(usersI) & " "
			'set rsTemp = openRSexecute(mySQL)
			set rsTemp = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
			'If user found, add to list
			if not rsTemp.eof then
				if getUserName = "" then
					getUserName = rsTemp("firstname") & " " & rsTemp("lastname")
				else 
					getUserName = getUserName & "; " & rsTemp("firstname") & " " & rsTemp("lastname")
				end if					
			end if									
			call closeRS(rsTemp)								
		next		
	end if
	
end function

'*************************************************************************
'Used to remove all HTML tags from field
'RETURNS: String getUserName with all User Names found (i.e. Bryan,John)
'*************************************************************************
function BdateAdd(StartDate, Interval)

dim tempDate
dim sunInterval
dim satInterval

	if isNull(Interval) or Interval = 0 or Interval = "" then
		BdateAdd = StartDate
		exit function
	end if
	
	tempDate = dateAdd("d",Interval,StartDate)
	if cDate(tempDate) < cDate(StartDate) then
		sunInterval = "-2"
		satInterval = "-1"
	else
		sunInterval = "1"
		satInterval = "2"	
	end if
	
	select case weekday(tempDate)
		case 1 'Sunday
			BdateAdd = dateAdd("d",sunInterval,tempDate)		
		case 7 'Saturday
			BdateAdd = dateAdd("d",satInterval,tempDate)		
		case else
			BdateAdd = tempDate
	end select

end function
'*************************************************************************
'Used to remove all HTML tags from field
'RETURNS: String getUserName with all User Names found (i.e. Bryan,John)
'*************************************************************************
function BdateAdd2(StartDate, Interval)

dim tempDate, x
dim sunInterval
dim satInterval

	if isNull(Interval) or Interval = 0 or Interval = "" then
		BdateAdd2 = StartDate
		exit function
	end if

	tempDate = startDate 

	'Count backwards
	if cDate(dateAdd("d",Interval,StartDate)) < cDate(StartDate) then

		if (weekday(tempDate,1) = 1) then
			tempDate = dateAdd("d", -1, tempDate) 
		end if
		
		for x = Interval to(-1) 
			tempDate = dateAdd("w", -1, tempDate) 
			if (weekday(tempDate,1) = 1) OR (weekday(tempDate,1) = 7) then
				x = x - 1 
			end if
		next 

	'Count forwards
	else
	
		if (weekday(tempDate,1) = 7) then
			tempDate = dateAdd("d", 1, tempDate) 
		end if
		
		for x = 1 to(Interval) 
			tempDate = dateAdd("w", 1, tempDate) 
			if (weekday(tempDate,1) = 1) OR (weekday(tempDate,1) = 7) then
				x = x - 1 
			end if
		next 
	
	end if
	
	BdateAdd2 = tempDate 

end function
'*************************************************************************
'SQL Query Builder from XML: used on issues.asp to build SQL statement
'*************************************************************************
function BuildQuery(queryDataSet, queryFields, querySortField, querySortOrder, queryFilterXML, listTable, listType, chartXAxis, chartYAxis)

	dim tempSQL, tempWHERE, tempFIELDS, tempTYPE, tempVALUE
	dim arrField, tempField, tempOperator, tempDelimiter, arrValue
	dim objLst, subLst, i, x
	dim xmlDoc : set xmlDoc = Server.CreateObject("Microsoft.XMLDOM")

	'malformed XML needs ampersand "&" converted to "&amp;" to display correctly
	queryFilterXML = replace(queryFilterXML, " & ", " &amp; ")

	'CROSSTAB query
	if lCase(listType) = "calculated" and len(chartYAxis)>0 then
		tempSQL = "SELECT CRSID, " & chartXAxis & ", " & chartYAxis
	
	'GROUP BY query
	elseif lCase(listType) = "calculated" and (len(chartYAxis)=0 or isNull(chartYAxis) or chartYAxis="") then
		tempSQL = "SELECT " & chartXAxis & ", COUNT([CRSID]) AS Total "
	
	'SELECT statement
	else
		tempSQL = "SELECT " & queryFields
	end if

	'FROM clause
	tempSQL = trim(tempSQL) & " FROM " & listTable

	'start building WHERE clause
	xmlDoc.async = False
	xmlDoc.loadXml(queryFilterXML)
	set objLst = xmlDoc.getElementsByTagName("filter")
	
	'cycle through all filter items
	for i = 0 to objLst.Length - 1
		
		set subLst = objLst.item(i)

		'add AND/OR
		if len(tempWHERE) > 0 then
			tempWHERE = trim(tempWHERE) & " AND "
		end if

		'get field TYPE
		tempTYPE = getFieldType(subLst.childNodes(0).text, listTable)
		if tempTYPE = 11 then	'Bit or True/False field
			if lCase(subLst.childNodes(2).text) = "true" then
				tempVALUE = "-1"
			elseif lCase(subLst.childNodes(2).text) = "false" then
				tempVALUE = "0"
			else
				tempVALUE = subLst.childNodes(2).text
			end if
		else
			tempVALUE = replace(subLst.childNodes(2).text,"'","''")
		end if
			
		'get fields, operator and value
		tempField 		= subLst.childNodes(0).text									'get field that is being filtered on	
		tempOperator 	= lCase(subLst.childNodes(1).text)							'get operator (i.e. EQUAL or OR or LIKE or IN or NULL)
		tempDelimiter 	= getFieldDelimeter(subLst.childNodes(0).text, listTable) 	'find field delimeter (i.e. ' or " or #)
			
		'--------------------------------------
		'put it all together
		'--------------------------------------			
		'LIKE and NOT LIKE operator
		if tempOperator = "like" or tempOperator = "not like" then
			tempWHERE = tempWHERE & tempField & " " & tempOperator & " " & tempDelimiter & "%" & tempVALUE & "%" & tempDelimiter

		'NULL and NOT NULL operator
'		elseif tempOperator = "is null" or tempOperator = "is not null" then
'			tempWHERE = tempWHERE & tempField & " " & tempOperator

		'NULL operator
		elseif tempOperator = "is null" then
			tempWHERE = tempWHERE & "(" & tempField & " " & tempOperator & " or " & "len(" & tempField & ")<=0" & ")"

		'NOT NULL
		elseif tempOperator = "is not null" then
			tempWHERE = tempWHERE & "(" & tempField & " " & tempOperator & " and " & "len(" & tempField & ")>0" & ")"
			
		'IN and NOT IN operator
		elseif tempOperator = "in" or tempOperator = "not in" then
			arrValue = split(tempVALUE,",")
			tempVALUE = ""
			for x = LBound(arrValue) to UBound(arrValue)	
				tempVALUE = tempVALUE & tempDelimiter & trim(arrValue(x)) & tempDelimiter & ","
			next			
			if len(tempVALUE) > 0 then
				'clean up any begining or trailing commans and quotes
				if left(tempVALUE,1) = "," then tempVALUE = right(tempVALUE,len(tempVALUE)-1)	
				if left(tempVALUE,3) = "''," then tempVALUE = right(tempVALUE,len(tempVALUE)-3)					
				if right(tempVALUE,4) = ",''," then tempVALUE = left(tempVALUE,len(tempVALUE)-4)
				if right(tempVALUE,1) = "," then tempVALUE = left(tempVALUE,len(tempVALUE)-1)
				if right(tempVALUE,2) = "''" then tempVALUE = left(tempVALUE,len(tempVALUE)-2)
				if right(tempVALUE,1) = "," then tempVALUE = left(tempVALUE,len(tempVALUE)-1)
			else
				tempVALUE = "''"
			end if
			'add to query statement								 
			tempWHERE = tempWHERE & tempField & " " & tempOperator & " (" & tempVALUE & ")"
		
		'ALL other operators
		else
			tempWHERE = tempWHERE & tempField & " " & tempOperator & " " & tempDelimiter & tempVALUE & tempDelimiter
		
		end if
		'--------------------------------------

	next

	if len(tempWHERE) > 0 then
		tempWHERE = " WHERE " & tempWHERE
	end if
		
	'crosstab WHERE
	if lCase(listType) = "calculated" and len(chartYAxis)>0 then
		'carret used instead for single quote in SP_Crosstab
		tempSQL = trim(tempSQL) & replace(tempWHERE,"''","^^")
		tempSQL = replace(tempSQL,"'","^")

	'group by WHERE
	elseif lCase(listType) = "calculated" and (len(chartYAxis)=0 or isNull(chartYAxis) or chartYAxis="") then
		tempSQL = trim(tempSQL) & tempWHERE & " GROUP BY " & chartXAxis & " "	
	
	'straight WHERE for grid
	elseif lCase(listType) <> "calculated" then
		tempSQL = trim(tempSQL) & tempWHERE		
		
	end if 






	'piece together
'	tempSQL = trim(tempSQL) & tempWHERE




	
	'add where to select statement
'	if lCase(listType) = "calculated" and len(chartYAxis) > 0 then
'		'carret used instead for single quote in SP_Crosstab
'		tempSQL = trim(tempSQL) & replace(tempWHERE,"''","^")
'		tempSQL = replace(tempSQL,"'","^")
'	elseif lCase(listType) <> "calculated" then
'		tempSQL = trim(tempSQL) & tempWHERE			
'	end if



	'ORDER BY clause
	if len(querySortField) > 0 then
		if lCase(listType) = "calculated" then
			if chartYAxis="" or isNull(chartYAxis) then
				tempSQL = trim(tempSQL) & " ORDER BY " & chartXAxis & " " & querySortOrder
			end if
		else
			'this makes sure the sort field DOES exist in the SQL statement
			if instr(queryFields,querySortField) > 0 then
				tempSQL = trim(tempSQL) & " ORDER BY " & querySortField & " " & querySortOrder
			end if			
		end if
	end if

	'pass back final query
	BuildQuery = trim(tempSQL)
		
end function
'******************************************************************
'Get Field Type: 
'******************************************************************
function getFieldType(strField, listTable)

dim rsTemp
dim x

	'open recordset 
	'NOTE: need to add [ ] brackets to field name cause that's how it is stored in Reports table
	set rsTemp = openRSexecute("SELECT " & strField & " FROM " & listTable & " WHERE 1=2")
	for x = 0 to rsTemp.fields.count-1
		if lCase("[" & trim(rsTemp.fields(x).name) & "]") = lCase(trim(strField)) then	
			getFieldType = rsTemp.fields(x).type					
			exit for
		end if		
	next 
	
end function
'******************************************************************
'Get Field Delimeter: used on issues.asp to build filter from XML
'******************************************************************
function getFieldDelimeter(strField, listTable)

dim rsTemp
dim x

	'open recordset 
	'NOTE: need to add [ ] brackets to field name cause that's how it is stored in Reports table
	set rsTemp = openRSexecute("SELECT " & strField & " FROM " & listTable & " WHERE 1=2")
	for x = 0 to rsTemp.fields.count-1
		if lCase("[" & trim(rsTemp.fields(x).name) & "]") = lCase(trim(strField)) then	
			strField = rsTemp.fields(x).type					
			exit for
		end if		
	next 
		
	if strField = adDate or strField = adDBDate or _
	   strField = adDBTime or strField = adDBTimeStamp then
		if dbType = 0 then					'MS Access
			getFieldDelimeter = "#"
		else								'SQL Server
			getFieldDelimeter = "'"
		end if

	elseif strField = adBSTR or strField = adChar or strField = adVarChar or _
		   strField = adLongVarChar or strField = adWChar or strField = adVarWChar or _
		   strField = adLongVarWChar then
	
		getFieldDelimeter = "'"
		
	else
	
		getFieldDelimeter = ""
		
	end if	


end function
'*************************************************************************
'SQL Check XML for valid Query
'*************************************************************************
function ValidXMLQuery(listTable,queryFilterXML)

	dim tempSQL, tempWHERE, tempFIELDS, tempTYPE, tempVALUE
	dim arrField, tempOperator
	dim objLst, subLst, i
	dim xmlDoc : set xmlDoc = Server.CreateObject("Microsoft.XMLDOM")

	'malformed XML needs ampersand "&" converted to "&amp;" to display correctly
	queryFilterXML = replace(queryFilterXML, " & ", " &amp; ")
	
	'start building WHERE clause
	xmlDoc.async = False
	xmlDoc.loadXml(queryFilterXML)
	set objLst = xmlDoc.getElementsByTagName("filter")
	
	'cycle through all filter items
	for i = 0 to objLst.Length - 1
		set subLst = objLst.item(i)

			'add AND/OR
			if len(tempWHERE) > 0 then
				tempWHERE = trim(tempWHERE) & " AND "
			end if

			'get field TYPE
			tempTYPE = getFieldType(subLst.childNodes(0).text, listTable)
			if tempTYPE = 11 then	'Bit or True/False field
				if lCase(subLst.childNodes(2).text) = "true" then
					tempVALUE = "-1"
				elseif lCase(subLst.childNodes(2).text) = "false" then
					tempVALUE = "0"
				else
					tempVALUE = subLst.childNodes(2).text
				end if
			else
				tempVALUE = subLst.childNodes(2).text
			end if
			
			'put it all together
			tempOperator = lCase(subLst.childNodes(1).text)
			if tempOperator = "like" or tempOperator = "not like" then
				tempWHERE = tempWHERE & subLst.childNodes(0).text & " " & subLst.childNodes(1).text & " " & getFieldDelimeter(subLst.childNodes(0).text, listTable) & "%" & tempVALUE & "%" & getFieldDelimeter(subLst.childNodes(0).text, listTable)

			elseif tempOperator = "is null" or tempOperator = "is not null" then
				tempWHERE = tempWHERE & subLst.childNodes(0).text & " " & subLst.childNodes(1).text
				
			else
				tempWHERE = tempWHERE & subLst.childNodes(0).text & " " & subLst.childNodes(1).text & " " & getFieldDelimeter(subLst.childNodes(0).text, listTable) & tempVALUE & getFieldDelimeter(subLst.childNodes(0).text, listTable)
			
			end if

	next

	'WHERE statement
	if len(tempWHERE) > 0 then
		ValidXMLQuery = "Yes"
	else
		ValidXMLQuery = "No"
	end if

end function
'*********************************************************************
'Set Sort Image to show ASC or DESC
'*********************************************************************
sub checkSortImg(sField)
	
	if sortField = sField then
		if sortOrder = "asc" then
			response.write("&nbsp;<img src=""../_images/icons/16/arrow_up.png"" align=""absmiddle"" />")
		elseif sortOrder = "desc" then
			response.write("&nbsp;<img src=""../_images/icons/16/arrow_down.png"" align=""absmiddle"" />")
		end if
	end if
	
end sub
'*********************************************************************
'Set Friendly User Field Name
'*********************************************************************
function setFriendlyName(fieldName,cusID,addPrefix)

dim mySQL, rsTemp, tempName

	tempName = trim(lCase(fieldName))

	'user defined field passed
	if inStr(tempName,"user field ") > 0 then
	
		'reformat to proper field name
		tempName = replace(tempName,"user field ","pUserField")
		'Get current configuration settings from database
		mySQL = "SELECT " & tempName & " FROM Customer WHERE CustomerID='" & cusID & "' "				
		'open recordset
		set rsTemp = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		if not rsTemp.eof then			
			if lCase(addPrefix) = "yes" then
				if len(rsTemp(tempName)) > 0 then				
					setFriendlyName = "UDF: " & rsTemp(tempName)
				else
					setFriendlyName = "UDF: " & fieldName
				end if
			else
				if len(rsTemp(tempName)) > 0 then				
					setFriendlyName = rsTemp(tempName)
				else
					setFriendlyName = fieldName					
				end if
			end if
		else
			setFriendlyName = fieldName
		end if
		call closeRS(rsTemp)
		
	'send back as was given
	else		
		setFriendlyName = fieldName
		
	end if
	
end function

'*********************************************************************
'Set Sort Image to show ASC or DESC
'*********************************************************************
function setFriendlyValue(fieldValue)

dim tempValue

	if lCase(fieldValue) = "true" then
		tempValue = "Yes"
	elseif lCase(fieldValue) = "false" then
		tempValue = "No"
	else
		tempValue = fieldValue
	end if
	'return value
	setFriendlyValue = tempValue
	
end function

'*************************************************************************
'Used to repeat a character a certain number of times
'RETURNS: String 
'*************************************************************************
function RepeatString(strInput, intCount) 
        Dim arrTmp()
        ReDim arrTmp(intCount)
        RepeatString = Join(arrTmp, strInput)
end function

'*************************************************************************
'Used to generate random Hex colors in Charts
'RETURNS: String 
'*************************************************************************
function genHexColor() 
	Dim randomRGB, strRed, strBlue, strGreen
	
	'begin random function to create file name
	randomize	

	'Random Red
	randomRGB = cLng(rnd*255)+1				
	strRed = RepeatString(0, 2-len(hex(randomRGB))) & hex(randomRGB)
	'Random Green
	randomRGB = cLng(rnd*255)+1				
	strGreen = RepeatString(0, 2-len(hex(randomRGB))) & hex(randomRGB)
	'Random Blue
	randomRGB = cLng(rnd*255)+1				
	strBlue = RepeatString(0, 2-len(hex(randomRGB))) & hex(randomRGB)

	'Return newly created Hex color
	genHexColor = strRed & strGreen & strBlue

end function

'*************************************************************************
'Used to generate random Hex colors in Charts
'RETURNS: String 
'*************************************************************************
sub LogActiveUser()

dim strActiveUserList
dim intUserStart, intUserEnd
dim strUser
dim strDate

'Declare Variables
dim mySQL, rsTemp, i
dim optValue
	
	strActiveUserList = Application("ActiveUserList")
	if Instr(1, strActiveUserList, Session.SessionID) > 0 then
		Application.Lock
    	intUserStart = Instr(1, strActiveUserList, Session.SessionID)
    	intUserEnd = Instr(intUserStart, strActiveUserList, "|")
    	strUser = Mid(strActiveUserList, intUserStart, intUserEnd - intUserStart)      	
		
		strActiveUserList = Replace(strActiveUserList, strUser, Session.SessionID & ":" & Now() )

'		TRIED TO GET THIS TO WORK...BUT IT KEPT GROWING (BRYAN(BRYAN(BRYAN...COULDN'T GET IT TO RECOGNIZE
'		JUST THE ONE USER..TRY AGAIN LATER...
'		'Get current configuration settings from database
'		mySQL = "SELECT FirstName, LastName " _
'			  & "	FROM Logins " _
'			  & "	WHERE LogID=" & session(session("siteID") & "logid") & " "
'		set rsTemp = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
'		if not rsTemp.eof then
'			strActiveUserList = Replace(strActiveUserList, strUser, rsTemp("FirstName") & " " & rsTemp("LastName") & "(" & Session.SessionID & ")" & ":" & Now() )		
'		else
'			strActiveUserList = Replace(strActiveUserList, strUser, Session.SessionID & ":" & Now() )		
'		end if
'		closeRS(rsTemp)

     	Application("ActiveUserList") = strActiveUserList
      	Application.UnLock
		
  	else
    	Application.Lock
    	Application("ActiveUsers") = CInt(Application("ActiveUsers")) + 1
    	Application("ActiveUserList") = Application("ActiveUserList") & Session.SessionID & ":" & Now() & "|"
    	Application.UnLock
	end if
	
end sub
'*************************************************************************
'Used to generate random Hex colors in Charts
'RETURNS: String 
'*************************************************************************
sub ActiveUserCleanup()

dim ix
dim intUsers
dim strActiveUserList
dim aActiveUsers
dim intActiveUserCleanupTime
dim intActiveUserTimeout

intActiveUserCleanupTime = 1 'In minutes, how often should the ActiveUserList be cleaned up.
intActiveUserTimeout = 30 'In minutes, how long before a User is considered Inactive and is deleted from ActiveUserList

if Application("ActiveUserList") = "" Then Exit Sub

	if DateDiff("n", Application("ActiveUsersLastCleanup"), Now()) > intActiveUserCleanupTime then
	
		Application.Lock
		Application("ActiveUsersLastCleanup") = Now()
		Application.Unlock
	
		intUsers = 0
		strActiveUserList = Application("ActiveUserList")
		strActiveUserList = Left(strActiveUserList, Len(strActiveUserList) - 1)
	
		aActiveUsers = Split(strActiveUserList, "|")
	
		for ix = 0 To UBound(aActiveUsers)
			If DateDiff("n", Mid(aActiveUsers(ix), Instr(1, aActiveUsers(ix), ":") + 1, Len(aActiveUsers(ix))), Now()) > intActiveUserTimeout Then
				aActiveUsers(ix) = "XXXX"
			else
				intUsers = intUsers + 1
			end if 
		next
		
		strActiveUserList = Join(aActiveUsers, "|") & "|"
		strActiveUserList = Replace(strActiveUserList, "XXXX|", "")
	
		Application.Lock
		Application("ActiveUserList") = strActiveUserList
		Application("ActiveUsers") = intUsers
		Application.UnLock
	
	end If

end sub

'==========================================================  
' The DateAddW() function provides a workday substitute  
' for DateAdd("w", number, date). This function performs  
' error checking and ignores fractional Interval values.  
'==========================================================  
function DateAddW(TheDate,Interval)
  
	dim Weeks, OddDays, Temp
  
	if VarType(TheDate) <> 7 Or VarType(Interval) < 2 Or VarType(Interval)  > 5 Then  
    	DateAddW = TheDate  
	elseif Interval = 0 Then  
    	DateAddW = TheDate  
   	elseif Interval > 0 Then  
    	Interval = Int(Interval)  
  
		' Make sure TheDate is a workday (round down).    
	  	Temp = Weekday(TheDate)
      	if Temp = 1 Then  
        	TheDate = TheDate - 2  
      	elseif Temp = 7 Then  
        	TheDate = TheDate - 1  
      	end if  
  
   		' Calculate Weeks and OddDays.    
      	Weeks = Int(Interval / 5)  
      	OddDays = Interval - (Weeks * 5)  
      	TheDate = TheDate + (Weeks * 7)  
  
  		' Take OddDays weekend into account.    
      	if (DatePart("w", TheDate) + OddDays) > 6 Then  
        	TheDate = TheDate + OddDays + 2  
      	else  
        	TheDate = TheDate + OddDays  
      	end if  
  
		DateAddW = TheDate  
	else                         ' Interval is < 0  
    	Interval = Int(-Interval) ' Make positive & subtract later.  
  
   	  	' Make sure TheDate is a workday (round up).   
      	Temp = Format(TheDate, "ddd")  
      	if Temp = "Sun" Then  
        	TheDate = TheDate + 1  
      	elseIf Temp = "Sat" Then  
        	TheDate = TheDate + 2  
      	end if 
  
     	' Calculate Weeks and OddDays.    
      	Weeks = Int(Interval / 5)  
      	OddDays = Interval - (Weeks * 5)  
      	TheDate = TheDate - (Weeks * 7)  
  
   	  	' Take OddDays weekend into account.  
      	if (DatePart("w", TheDate) - OddDays) > 2 Then  
        	TheDate = TheDate - OddDays - 2  
      	else  
        	TheDate = TheDate - OddDays  
      	end if  
  
      	DateAddW = TheDate  
		
    end if  
  
end function 

'*****************************************************************************
' HIGHLIGHT function will search text for a specific string
' When string is found it will be surrounded by supplied strings
'
' NOTE: Unfortunately Replace() function does not preserve the original case 
' of the found string. This function does.
'
' Parameters:
' strText 	- string to search in
' strFind	- string to look for
' strBefore	- string to insert before the strFind
' strAfter 	- string to insert after the strFind
'
' Example: 
' This will make all the instances of the word "the" bold
'
' Response.Write Highlight(strSomeText, "the", "<b>", "</b>")
'
function highlight(strText, strFind)
	Dim nPos
	Dim nLen
	Dim nLenAll
	dim strBefore : strBefore = "<font style='background-color:#ffcc00;'>"
	dim strAfter : strAfter = "</font>"
		
	nLen = Len(strFind)
	nLenAll = nLen + Len(strBefore) + Len(strAfter) + 1

	highlight = strText

	if nLen > 0 And Len(Highlight) > 0 Then
		nPos = InStr(1, Highlight, strFind, 1)
		do while nPos > 0
			Highlight = Left(Highlight, nPos - 1) & _
				strBefore & Mid(Highlight, nPos, nLen) & strAfter & _
				Mid(Highlight, nPos + nLen)

			nPos = InStr(nPos + nLenAll, Highlight, strFind, 1)
		loop
	end if
	
end function

'
' Find Inline help code and return
'
function getInlineHelp(page,security)
	
	dim helpRS
	dim helpSQL
		
	if len(page) > 0 and len(security) > 0 then
		'get current page URL
		if page = "getPageURL" then page = getPageURL()
		'look for any available video help
		helpSQL = "SELECT EmbedCode FROM Help WHERE Page='" & page & "' AND Type='Inline' "
		'open reports table...
		set helpRS = openRSopen(helpSQL,0,adOpenStatic,adLockReadOnly,adCmdText,0)
		if not helpRS.eof then
			if len(helpRS("EmbedCode")) > 0 then
				getInlineHelp = " <img src=""../_images/icons/16/video.png"" width=""16"" height=""16"" align=""absmiddle"" style=""margin-right:3px; vertical-align:middle;"" /> " & helpRS("EmbedCode")
			else
				getInlineHelp = ""
			end if
		else
			getInlineHelp = ""				
		end if
	else
		getInlineHelp = ""
	end if

end function

'
' Retrieves current page URL and name
' 
function getPageURL()

	dim scr
	dim loc

    scr = Request.ServerVariables("SCRIPT_NAME")
    loc = instrRev(scr,"/") 
    scr = mid(scr, loc+1, len(scr) - loc) 
    getPageURL = trim(scr)

end function

%>
