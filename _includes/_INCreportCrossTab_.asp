        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Configuration div -->
        <div id="tab:configure">        
        
            <!-- START Configuration table -->   
	        <table id="table_configure" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>
                    
                    <!-- START Middle Section -->    
                    <td valign="top">

					  <%
					  'only show this table if NOT sql statement									
					  if lCase(optType) <> "sql" then
					  %>

					  	<!-- START Configuration Form Fields -->
						<table id="table:configure:general" width="100%" class="formTable" cellpadding="0" cellspacing="0" <% if lCase(optType) = "sql" then response.write("style=""display:none;""") %>>

      						<tr>
                            
                           		<!-- START field selection for dataset-->
                                <td class="clearFormat" style="border:none; padding-left:10px; margin-right:10px;" align="left" width="225" >
                                    
                                    <div class="subLabel" style="text-transform:uppercase; margin-bottom:5px;"><img src="../_images/icons/16/text_list_bullets.png" width="16" height="16" align="absmiddle" style="margin-right:7px;" /><strong>X-Axis</strong></div>                    
                                    <div class="subLabel" style="width:200px; margin-bottom:6px;">Select the field that you would like to appear on the X-axis of your report:</div>
                                    
                                    <div>

                                            <select name="optQueryField" id="optQueryField" size=1 style="width:200px;">
                                            	<%
                                                'build dropdown of available fields for filter selection
                                                '...do not include if already a part of XML filter
                                                for labelX = 0 to UBound(arrColumn)
                                                    if inStr(1,optFilter,arrColumn(labelX)) <= 0 then
                                                        Response.Write "<option value='" & arrColumn(labelX) & "'>" & arrFriendly(labelX) & "</option>"
                                                    end if
                                                next
                                                %>
                                            </select>                                        
                                    
                                    </div>
                     
                                    

                                </td>

								<!-- START filter section-->
                                <td class="clearFormat" style="border:none; padding:0px;" align="left">
									                                    
                                    <div class="subLabel" style="text-transform:uppercase; margin-bottom:5px;"><img src="../_images/icons/16/filter.png" width="16" height="16" align="absmiddle" style="margin-right:7px;" /><strong>Filters</strong></div>                    
                                    <div class="subLabel" style="margin-bottom:6px;">Only data that matches the criteria you specify below will be included in your report.</div>
                 
									<!-- START field selection for filter -->
                                    <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
										<td style="background-color:#C7CDE9; border:none; padding:7px;">
                                            <img src="../_images/icons/18/add.png" width="18" height="18" title="Add" align="absmiddle" style="margin-right:7px; cursor:pointer;" onClick="javascript:addQueryRow();" />
                                            <select name="optQueryField" id="optQueryField" size=1 style="width:200px;">
                                            	<%
                                                'build dropdown of available fields for filter selection
                                                '...do not include if already a part of XML filter
                                                for labelX = 0 to UBound(arrColumn)
                                                    if inStr(1,optFilter,arrColumn(labelX)) <= 0 then
                                                        Response.Write "<option value='" & arrColumn(labelX) & "'>" & arrFriendly(labelX) & "</option>"
                                                    end if
                                                next
                                                %>
                                            </select>                                        
										</td>
                                      </tr>
                                    </table>                                        
									<!-- STOP field selection for filter -->
                                                                        
									<!-- START WHERE clause filter -->                                                                        
                                    <table width="90%" class="formFilter" id="tblFilter" border="0" cellspacing="0" cellpadding="0">
                                        <tbody id="tblFilterBody">
                                        
										<%
										'malformed XML needs ampersand "&" converted to "&amp;" to display correctly
										optFilter = replace(optFilter, " & ", " &amp; ")
										
										'prepare XML filter
										dim objPopCalendar
										dim objLst, subLst, subLstNext, i
										dim xmlDoc : set xmlDoc = Server.CreateObject("Microsoft.XMLDOM")												
										xmlDoc.async = False
										xmlDoc.loadXml(optFilter)
										set objLst = xmlDoc.getElementsByTagName("filter")
										for i = 0 to objLst.Length - 1
											set subLst = objLst.item(i)
											
											'date field go ahead and get next values
											if inStr(lCase(subLst.childNodes(0).text),"date") > 0 then
												set subLstNext = objLst.item(i+1)
											end if
											
                                        %>					
                                             <tr id="rowFilter_<% =i %>">
                                                <td width="50">                                                       
													<div style="white-space:nowrap;">
                                                    
                                                        <%
														'no date, create singe hidden field
														if inStr(lCase(subLst.childNodes(0).text),"date") = 0 then
                                                        	response.write("<input type=hidden name=""idQueryField"" id=""idQueryField"" value=""" & subLst.childNodes(0).text & """>")
														
														'date field filtered on, create two (2) hidden fields
														else															
                                                        	response.write("<input type=hidden name=""idQueryField"" id=""idQueryField"" value=""" & subLst.childNodes(0).text & """>")
                                                        	response.write("<input type=hidden name=""idQueryField"" id=""idQueryField"" value=""" & subLstNext.childNodes(0).text & """>")																													
														end if                                                            

														'temporary clean up for display only    
														labelTemp = subLst.childNodes(0).text
														labelTemp = replace(labelTemp, "[","")
														labelTemp = replace(labelTemp, "]","")														
														%>
                                                       	<img src="../_images/icons/18/cancel.png" width="18" height="18" title="Delete" align="absmiddle" style="margin-right:7px; cursor:pointer;" onClick="javascript:delQueryRow('rowFilter_<% =i %>','<% =labelTemp %>','<% =subLst.childNodes(0).text %>');"/>
														<!-- field name here -->
														<% =labelTemp %>                                                        

                                                    </div>
                                                </td>
                                                <td width="125">
                                                    <%
													if inStr(lCase(subLst.childNodes(0).text),"date") = 0 then
                                                	%>
                                                        <select name="idQueryExp" id="idQueryExp_<% =i %>" style="width:125px;" onchange="javascript:changeElementStyle(this.id,this.value);">								
                                                            <option value="=" 			<%=checkMatch(subLst.childNodes(1).text,"=") %>>equals to</option>
                                                            <option value="<>" 			<%=checkMatch(subLst.childNodes(1).text,"<>") %>>does not equal to</option>
                                                            <option value="<=" 			<%=checkMatch(subLst.childNodes(1).text,"<=") %>>less than</option>
                                                            <option value=">=" 			<%=checkMatch(subLst.childNodes(1).text,">=") %>>greater than</option>
                                                            <option value="like" 		<%=checkMatch(subLst.childNodes(1).text,"like") %>>contains</option>
                                                            <option value="not like" 	<%=checkMatch(subLst.childNodes(1).text,"not like") %>>does not contain</option>
                                                            <option value="is null" 	<%=checkMatch(subLst.childNodes(1).text,"is null") %>>is null</option>
                                                            <option value="is not null"	<%=checkMatch(subLst.childNodes(1).text,"is not null") %>>is not null</option>
                                                            <option value="in" 			<%=checkMatch(subLst.childNodes(1).text,"in") %>>is one of</option>
                                                            <option value="not in"		<%=checkMatch(subLst.childNodes(1).text,"not in") %>>is not one of</option>
                                                        </select>                                                    
													<%
													else
													%>
                                                        <select name="idQueryExp" style="width:125px; display:none;">								
                                                            <option value=">=" <%=checkMatch(subLst.childNodes(1).text,">=") %>>greater than</option>
                                                        </select>
                                                       	<select name="idQueryExp" style="width:125px; display:none;">								
                                                            <option value="<=" <%=checkMatch(subLst.childNodes(1).text,"<=") %>>less than</option>
                                                        </select>          
                                                       	<select name="idNOT-USED" style="width:125px;">								
                                                            <option value="">between</option>
                                                        </select>                                                                                                            
													<%
													end if
													%>
                                                </td>
                                                <td>
                                                    <%
													'input boxes already selected by user
													if inStr(lCase(subLst.childNodes(0).text),"date") = 0 then
													
														'set height if IN or NOT IN operator is used
														if (subLst.childNodes(1).text = "in") or (subLst.childNodes(1).text = "not in") then
															response.write("<textarea name=""idQueryValue"" id=""idQueryValue_" & i & """ style=""overflow-y: auto; height: 42px; width:150px;"">" & subLst.childNodes(2).text & "</textarea>")
                                                        else
															response.write("<textarea name=""idQueryValue"" id=""idQueryValue_" & i & """ style=""overflow-y: hidden; height: 13px; width:150px;"">" & subLst.childNodes(2).text & "</textarea>")														
														end if
														
														'------------------------------------------
														'START JSON filter builter
														'------------------------------------------
														'dont allow JSON filtering on these fields
														if instr("Total,CRSID,Issue #,Time,Location Address,Location Postal Code,Caller Address,Caller Postal Code,CustomerID,Summary,Details,Addendum,Resolution,CaseNotes,Notes,Outcome",labelTemp) <= 0 then
														%>
															<script>
                                                            	$(function() {
                                                                    function split( val ) {
                                                                        return val.split( /,\s*/ );
                                                                    }
                                                                    function extractLast( term ) {
                                                                        return split( term ).pop();
                                                                    }                                                            
                                                                    $("#idQueryValue_<% =i %>")
                                                                        // don't navigate away from the field on tab when selecting an item
                                                                        .bind( "keydown", function( event ) {
                                                                            if ( event.keyCode === $.ui.keyCode.TAB &&
                                                                                    $( this ).data( "autocomplete" ).menu.active ) {
                                                                                event.preventDefault();
                                                                            }
                                                                        })
                                                                        .autocomplete({
                                                                            source: function( request, response ) {
                                                                                $.getJSON( "../_jquery/suggestBox/json-data.asp?view=<% =server.URLEncode(optTable) %>&field=<% =server.URLEncode(labelTemp) %>", {
                                                                                    term: extractLast( request.term )
                                                                                }, response );
                                                                            },
                                                                            search: function() {
                                                                                // custom minLength
                                                                                var term = extractLast( this.value );
                                                                                if ( term.length < 1 ) {
                                                                                    return false;
                                                                                }
                                                                            },
                                                                            focus: function() {
                                                                                // prevent value inserted on focus
                                                                                return false;
                                                                            },
                                                                            select: function( event, ui ) {
																				//sets up for multiple responses...
																				//only do this IF the operator equals IN or NOT IN
																				if (document.getElementById("idQueryExp_<% =i %>").value == 'in' || document.getElementById("idQueryExp_<% =i %>").value == 'not in') {
																					var terms = split( this.value );
																					// remove the current input
																					terms.pop();
																					// add the selected item
																					terms.push( ui.item.value );
																					// add placeholder to get the comma-and-space at the end
																					terms.push( "" );
																					this.value = terms.join( ", " );
																					return false;
																				}																				
                                                                            }
                                                                    	});
                                                                });
															</script>                                                            														
                                                        	<%
															'END JSON filter builter
															'------------------------------------------
															
														end if		
																								
													else															
                                                       	response.write("<input type=text name=""idQueryValue"" id=""idQueryValue_" & i & """ size=20 maxlength=75 style=""width:70px;"" value=""" & subLst.childNodes(2).text & """>")
														response.write("<input type=text name=""idQueryValue"" id=""idQueryValue_" & i+1 & """ size=20 maxlength=75 style=""width:70px; margin-left:5px;"" value=""" & subLstNext.childNodes(2).text & """>")														
														'used on dhtml calendar instances
														if right(objPopCalendar,1) = "'" then
															objPopCalendar = objPopCalendar & ",'" & "idQueryValue_" & i & "','" & "idQueryValue_" & i+1 & "'"
														else
															objPopCalendar = objPopCalendar & "'" & "idQueryValue_" & i & "','" & "idQueryValue_" & i+1 & "'"
														end if
														
													end if                                                            
													%>
                                                    
                                                </td>
                                            </tr>						
                                        <%
										
											'date field so increase row count by 1 so as to not repeat the second date value
											if inStr(lCase(subLst.childNodes(0).text),"date") > 0 then
												i = i+1
											end if
											
                                        next
                                    	%>

									    <!-- START calendar instances -->
										<% if len(objPopCalendar) > 0 then %>                                        
                                            <script type="text/javascript">
                                                var dateCal;
                                                window.onload = function() {
                                                    //issue date calendar
                                                    dateCal = new dhtmlxCalendarObject([<% =objPopCalendar %>], true, {isWinHeader:true, headerButtons:"XT", isWinDrag:false, isYearEditable:true, isMonthEditable:true });
                                                    dateCal.setSkin("simplegrey");
                                                    dateCal.setDateFormat("%m/%d/%Y");
                                                    dateCal.setYearsRange(2000, 2020);
                                                    dateCal.setHeaderText("Filter Date");
                                                }			
                                            </script>
                                        <% end if %>
                                        <!-- STOP calendar instances -->                                        

										<!-- THIS IS WHERE FILTER OPTIONS ARE APPENDED WITH JAVASCRIPT -->
                                  		<tr style="display:none;">                                   			
                                    		<td></td><td></td><td></td>
                                   		</tr>			
                                        
                                        </tbody>
                         			</table>
									<!-- STOP WHERE clause filter -->

                                </td>
								<!-- STOP filter section-->

                            </tr>
                                                                    
						</table>
					  	<!-- STOP Configuration Form Fields -->
                        
					  <%
					  end if
					  
					  'only show this table IF type is sql statement
					  if lCase(optType) = "sql" then
					  %>
                                            
                        <!-- START SQL Form Fields -->
						<table id="table:configure:sql" width="100%" class="formTable" cellpadding="0" cellspacing="0" <% if lCase(optType) <> "sql" then response.write("style=""display:none;""") %>>

      						<tr>
                                <td class="clearFormat" style="border:none; padding-left:10px; margin-right:10px;" align="left" >                                
                                    <div class="subLabel" style="text-transform:uppercase; margin-bottom:5px;"><strong>Query (SQL)</strong></div>                    
                                    <div class="subLabel" style="margin-bottom:4px;">Enter the <a href="http://msdn.microsoft.com/en-us/library/bb264565(SQL.90).aspx" target="_blank">MS SQL</a> statement to execute against the database:</div>
                      				<% if lCase(optType) = "sql" and session(session("siteID") & "adminLoggedOn") >= "20" then %>
                                        <div class="required" style="margin-bottom:6px;"><em>* Changes made to this SQL statement will not be saved.</em></div>
                                        <input name="optSQLValue" type="hidden" value="<% =optSQLValue %>" />
										<textarea style="width:90%; height:150px; margin-bottom:12px;" ><% =server.HTMLEncode(optSQLValue & "") %></textarea>                                    
                                    <% else %>
                                    	<textarea name="optSQLValue" style="width:90%; height:150px; margin-bottom:12px;" ><% =server.HTMLEncode(optSQLValue & "") %></textarea>
                                    <% end if %>
								</td>
                            </tr>

      						<tr>
                                <td class="clearFormat" style="border:none; padding-left:10px; margin-right:10px;" align="left" >                                
                                    <div class="subLabel" style="text-transform:uppercase; margin-bottom:5px;"><strong>Replacement Tags</strong></div>                    
                                    <div class="subLabel" style="margin-bottom:6px;">#LOGID# = User ID of current user logged in</div>
                                    <div class="subLabel" style="margin-bottom:6px;">#CUSID# = Customer ID current user logged in is associated with</div>                                    
								</td>
                            </tr>

						</table>
                        <!-- STOP SQL Form Fields -->
                        
					  <%
					  end if
					  %>
                      
                    </td>
                    <!-- START Page Details and Form Fields -->			                    
                                        
                </tr>
            
            </table>
            <!-- END Issues Details table -->
	
    	</div>
		<!-- STOP Details div -->        


<script type="text/javascript">

  // Add new row to table listing all added filters. All filters are added to form.
  // When form is submittted idQueryField, idQueryExp, and idQueryValue are split
  // within counter_edit_exec.asp and added to datebase table ReportQuery_Filter
  function addQueryRow()
  {  	
  
  	var tempHTML;
	
	// create randon numbers used to create unique element IDs for idQueryValue form fields
	var valueID_1 = Math.floor(Math.random()*1001);
	var valueID_2 = Math.floor(Math.random()*1001);
  
  	// create table body
	var table = document.getElementById("tblFilter");
    var tbody = table.getElementsByTagName("tbody")[0];

	// ----- ADD ROW -----
	// create row element and assign unique ID
	var rowCount = table.rows.length;
	var rowID = "rowFilter_" + (rowCount+2);
    var row = document.createElement("tr");
	row.id = rowID;
	
	// ----- ADD CELL #1 -----
	var selValue = document.getElementById('optQueryField').value;					// get VALUE from field dropdown
	var selIndex = document.getElementById('optQueryField').selectedIndex;			// get selected INDEX from dropdown
	var selText = document.getElementById('optQueryField').options[selIndex].text;	// get selected TEXT not value from dropdown
    var td1 = document.createElement("td")
	td1.width = "50";
	
	// check if field selected has "Date" text in name
	if (selValue.indexOf("Date")==-1){
		tempHTML = "<div style=\"white-space:nowrap;\">";
		tempHTML = tempHTML + "<img src=\"../_images/icons/18/cancel.png\" width=\"18\" height=\"18\" alt=\"Delete\" align=\"absmiddle\" style=\"margin-right:7px; cursor:pointer;\" onClick=\"javascript:delQueryRow('" + rowID + "','" + selText + "','" + document.getElementById('optQueryField').value + "');\"/>";
		tempHTML = tempHTML + "<input type=hidden name=\"idQueryField\" id=\"idQueryField\" value=\"" + document.getElementById('optQueryField').value + "\">";
		tempHTML = tempHTML + selText + "</div>";
	}
	// date filed...add 2 hidden fields to hold field name
	else {
		tempHTML = "<div style=\"white-space:nowrap;\">";
		tempHTML = tempHTML + "<img src=\"../_images/icons/18/cancel.png\" width=\"18\" height=\"18\" alt=\"Delete\" align=\"absmiddle\" style=\"margin-right:7px; cursor:pointer;\" onClick=\"javascript:delQueryRow('" + rowID + "','" + selText + "','" + document.getElementById('optQueryField').value + "');\"/>";
		tempHTML = tempHTML + "<input type=hidden name=\"idQueryField\" id=\"idQueryField\" value=\"" + document.getElementById('optQueryField').value + "\">";
		tempHTML = tempHTML + "<input type=hidden name=\"idQueryField\" id=\"idQueryField\" value=\"" + document.getElementById('optQueryField').value + "\">";
		tempHTML = tempHTML + selText + "</div>";
	}	
	td1.innerHTML = tempHTML;
	
	//remove selected text from dropdown
///	document.getElementById('optQueryField').remove(selIndex);		
	
	
	// ----- ADD CELL #2 -----	
    var td2 = document.createElement("td")
	// check if field selected has "Date" in name
	if (selValue.indexOf("Date")==-1){
		tempHTML = "<select name=\"idQueryExp\" id=\"idQueryExp_" + valueID_1 + "\" style=\"width:125px;\" onchange=\"javascript:changeElementStyle(this.id,this.value);\" >";
		tempHTML = tempHTML + "<option value=\"=\" >equals to</option>";
		tempHTML = tempHTML + "<option value=\"<>\" >does not equal to</option>";	
		tempHTML = tempHTML + "<option value=\"<=\" >less than</option>";
		tempHTML = tempHTML + "<option value=\">=\" >greater than</option>";
		tempHTML = tempHTML + "<option value=\"like\" >contains</option>";
		tempHTML = tempHTML + "<option value=\"not like\" >does not contain</option>";
		tempHTML = tempHTML + "<option value=\"is null\" >is null</option>";
		tempHTML = tempHTML + "<option value=\"is not null\" >is not null</option>";
		tempHTML = tempHTML + "<option value=\"in\" >is one of</option>";	
		tempHTML = tempHTML + "<option value=\"not in\" >is not one of</option>";
		tempHTML = tempHTML + "</select>";
	}
	// date filed...add 2 options
	else {
		tempHTML = "<select name=\"idQueryExp\" id=\"idQueryExp_" + valueID_1 + "\" style=\"width:125px; display:none;\">";
		tempHTML = tempHTML + "<option value=\">=\" >greater than</option>";
		tempHTML = tempHTML + "</select>";
		tempHTML = tempHTML + "<select name=idQueryExp style=\"width:125px; display:none;\">";
		tempHTML = tempHTML + "<option value=\"<=\" >less than</option>";
		tempHTML = tempHTML + "</select>";
		tempHTML = tempHTML + "<select name=\"idNOT-USED\" id=\"idNOT-USED\" style=\"width:125px;\">";
		tempHTML = tempHTML + "<option value=\"\" >between</option>";
		tempHTML = tempHTML + "</select>";
	}
	td2.innerHTML = tempHTML;
	td2.width = "125";
	
	
	// ----- ADD CELL #3 -----
	var td3 = document.createElement("td")
	// not date...add textarea input
	if (selValue.indexOf("Date")==-1){	
		tempHTML = "<textarea name=\"idQueryValue\" id=\"idQueryValue_" + valueID_1 + "\" style=\"overflow-y:hidden; height:13px; width:150px;\"></textarea>";
	}	
	// date filed...add 2 inputs
	else {	
		tempHTML = "<input type=text name=\"idQueryValue\" id=\"idQueryValue_" + valueID_1 + "\" size=10 maxlength=10 value=\"\" style=\"width:70px;\">";
		tempHTML = tempHTML + "<input type=text name=\"idQueryValue\" id=\"idQueryValue_" + valueID_2 + "\" size=10 maxlength=10 value=\"\" style=\"width:70px; margin-left:5px;\">";
	}
	td3.innerHTML = tempHTML;


	// ----- FINILIZE AND APPEND NEW CELLS AND ROW -----
    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
		
    // append row to table
    tbody.appendChild(row);


	// ----- ADD CALENDAR INSTANCE TO DATE ELEMENTS -----
	if (selValue.indexOf("Date")!=-1){	
		var dateCal;
		dateCal = new dhtmlxCalendarObject(["idQueryValue_"+ valueID_1,"idQueryValue_"+ valueID_2], true, {isWinHeader:true, headerButtons:"XT", isWinDrag:false, isYearEditable:true, isMonthEditable:true });
		dateCal.setSkin("simplegrey");
		dateCal.setDateFormat("%m/%d/%Y");
		dateCal.setYearsRange(2000, 2020);
		dateCal.setHeaderText("Filter Date");
	}
	
	// ----- ADD AUTO JSON QUERY IF NOT DATE FIELD -----
	if (selValue.indexOf("Date")<=-1){
		var ignoreFields = "[Total],[CRSID],[Issue #],[Time],[Location Address],[Location Postal Code],[Caller Address],[Caller Postal Code],[CustomerID],[Summary],[Details],[Addendum],[Resolution],[CaseNotes],[Notes],[Outcome]";
		
		//make sure field selected is not one to be ignored		
		if (ignoreFields.indexOf(selValue)<=-1) {
			var queryField = selValue;
			queryField = queryField.replace("[","");
			queryField = queryField.replace("]","");
			
			//JSON filter builder
			$(function() {
				function split( val ) {
					return val.split( /,\s*/ );
				}
				function extractLast( term ) {
					return split( term ).pop();
				}		
				$("#idQueryValue_"+ valueID_1)
					// don't navigate away from the field on tab when selecting an item
					.bind( "keydown", function( event ) {
						if ( event.keyCode === $.ui.keyCode.TAB &&
								$( this ).data( "autocomplete" ).menu.active ) {
							event.preventDefault();
						}
					})
					.autocomplete({
						source: function( request, response ) {
							$.getJSON( "../_jquery/suggestBox/json-data.asp?view="+ document.getElementById('optTable').value +"&field="+ escape(queryField) , {
								term: extractLast( request.term )
							}, response );
						},
						search: function() {
							// custom minLength
							var term = extractLast( this.value );
							if ( term.length < 1 ) {
								return false;
							}
						},
						focus: function() {
							// prevent value inserted on focus
							return false;
						},
						select: function( event, ui ) {
							//sets up for multiple responses...
							//only do this IF the operator equals IN or NOT IN
							if (document.getElementById("idQueryExp_" + valueID_1).value == 'in' || document.getElementById("idQueryExp_" + valueID_1).value == 'not in') {
								var terms = split( this.value );
								// remove the current input
								terms.pop();
								// add the selected item
								terms.push( ui.item.value );
								// add placeholder to get the comma-and-space at the end
								terms.push( "" );
								this.value = terms.join( ", " );
								return false;
							}
						}
					});
			});
						
		}
		
	// END JSON QUERY BUILDER	
	}

  }
</script>

<script type="text/javascript"> 

  // used to remove selected row from filter table
  //   obj = Row ID being removed
  function delQueryRow(obj,label,value)
  {
	var theRow = document.getElementById(obj);
	var theTable = document.getElementById("tblFilter");
	
    for (var i = 0; i < theTable.rows.length; i++)
	{
    	if (theRow == theTable.rows[i]) {
               theTable.deleteRow(i);
          }	
	}
	
	//add selected text back in dropdown
///	var selectBox = document.getElementById("optQueryField");
///	selectBox.options[selectBox.options.length] = new Option(label, value);
  }

  // used to set height and scroll bars
  // if user selected IN or NOT IN as the opearator 
  function changeElementStyle(expID,expValue) {
	var theElement = document.getElementById( "idQueryValue_" + expID.substr(11,3) );
	// user selected IN or NOT IN
	if (expValue == "in" || expValue == "not in") {
		theElement.style.overflowY = "auto";
		theElement.style.height = "42px";
	}
	// regular operator...make like normal input box
	else {
		theElement.style.overflowY = "hidden";
		theElement.style.height = "13px";
	}	
  }
</script>

