        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Settings div -->
        <div id="tab:settings" style="display:none;">        
        
            <!-- START Settings table -->   
	        <table id="table_settings" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>
                    
                    <td valign="top">
                                        
					  	<!-- START Settings Form Fields -->	                    
						<table id="table:settings:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">

      						<tr>
                              <td class="formLabel"><span class="required">*</span>Name:</td>
                              <td align=left nowrap>
                              	<input name="optName" id="optName" class="inputLong" value="<%=optName%>" maxlength="255" />
                              </td>
                            </tr>

      						<tr>
                              <td class="formLabel">Notes:</td>
                              <td align=left nowrap>
                              	<textarea name="optNotes" id="optNotes" style="width:85%; height:75px;" ><%=server.HTMLEncode(optNotes & "")%></textarea>
                              </td>
                            </tr>
                            
                            <% 
							'only users who can edit reports can set ownership
							if editReport="Y" then 
								%>
                                <tr>
                                  <td class="formLabel"><span class="required">*</span>Owner:</td>
                                  <td align=left nowrap style="padding-bottom:0px;">                                                            	 
                                    <div style="float:left;">
                                        <input name="rptOwner" id="rptOwner" class="inputLong" value="<% =rptOwner %>" maxlength="255" readonly="readonly" />
                                        <input type="hidden" name="rptLogid" id="rptLogid" value="<% =rptLogid %>" >
                                    </div>
                                    
                                    <!-- ONLY for CCI ADMINs and Customers, not for Risk Specialists -->
                                    <% if cLng(session(session("siteID") & "adminLoggedOn")) < 3 or cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then %>
                                        <div><a class="myCMbuttonSmall" title="Change Owner" href="#" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_user.asp?recid=<% =idReport %>&cid=<% =customerID %>&multi=false&pageView=reports', 380, 450, 'no'); resetSystemMsg();"><span class="user" style="padding-right:7px;">&nbsp;</span></a></div>                                
                                    <% end if %>
                                    
                                  </td>
                                </tr>
                                <tr>
                                  <td style="border-top:none; padding-bottom:5px;">&nbsp;</td>
                                  <td align=left nowrap style="border-top:none; padding:0px;">                               
                                    <div class="subLabel" >Owner has full control over report and its settings.</div>
                                  </td>
                                </tr>
								<% 
							else 
								%>
                                <input type="hidden" name="rptLogid" id="rptLogid" value="<% =rptLogid %>" >
								<% 
							end if
							%>

                            <% 
							'only users who can edit reports can see the RPT file
							'if editReport="Y" then 
							if 1=1 then
								%>                            
                                <tr>
                                  <td class="formLabel"><span class="required">*</span>Report File:</td>
                                  <td align=left nowrap>
                                  
                                    <%
									'list RPT avaialbe for selection
									if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
										%>
                                        <div style="margin-right:5px;">
                        
                                            <%
                                            dim viewObjects, Upload, tmpItemCount
                                            dim Directory, Dir, extPos, extImg, Item
                                                                
                                            'ignore any errors
                                            On Error Resume Next
                                            'Set directory and find all files
                                            Directory = dataDir & "\profiles\" & optDatasetCustID & "\crystal\"
                                                               
                                            'Open new ASPUpload object		
                                            Set Upload = Server.CreateObject("Persits.Upload")			
                                            Set Dir = Upload.Directory( Directory & "*.rpt", , True)
        
                                            %>									
                                            <select name="optFileName" id="optFileName" size=1 >
                                                <option value="">-- Select --</option>
                                                 <%                                                     
                                                'Error 33 equal 'Directory not found'
                                                if Err.Number <> 33 then		                  									
                                                    For Each Item in Dir
                                                        if Not Item.IsSubdirectory Then
                                                            Response.Write "<option value='" & Server.HtmlEncode(Item.FileName) & "' " & checkMatch(crystalFile,Server.HtmlEncode(Item.FileName)) & ">" & Server.HtmlEncode(Item.FileName) & "</option>"
                                                        end if		
                                                    'move to next document					
                                                    next
                                                end if         
        
                                                'reset error handling
                                                On error goto 0		  
                                                       
                                                %>                                
                                            </select>                                                                                                                              
                                        </div>

										<%
									else
										%>
	                                    <div id="ReportFile"><% =replace(replace(crystalFile,".rpt",""),"_", " ") %></div>				
						                <input type="hidden" name="optFileName" id="optFileName" value="<% =crystalFile %>">                                                        					
                                        <%
									end if
									%>
                                    
                                    <div class="subLabel">                                	
                                        <%
                                        if cLng(session(session("siteID") & "adminLoggedOn")) <= 10 then
                                            if crystalFile > "" then										
                                                response.write("<div style=""margin-top:5px;"">[ ")
                                                response.Write("<a href=""#"" onclick=""SimpleModal.open('../_jquery/fancyupload/fancyupload-crystal.asp?cid=" & optDatasetCustID & "', 400, 500, 'no'); resetSystemMsg('systemMessage'); return false;"">Upload</a> | ")
                                                response.Write("<a href=""../_upload/download.asp?Name=" & Server.URLEncode(crystalFile) & "&pageView=crystal&recID=1&cid=" & optDatasetCustID & """>Download</a> | ")
                                                response.Write("<a href=""#"" onClick=""confirmDocDelete('../_upload/delete.asp?upDel=Yes&pageView=crystal&recID=" & idReport & "&cid=" & optDatasetCustID & "&del=" & Server.URLEncode( crystalFile ) & "&top=settings'); return false;"">Delete</a>")
                                                response.write(" ]</div>")
                                            else
                                                response.write("<div style=""margin-top:5px;"">[ ")
                                                response.Write("<a href=""#"" onclick=""SimpleModal.open('../_jquery/fancyupload/fancyupload-crystal.asp?cid=" & optDatasetCustID & "', 400, 500, 'no'); resetSystemMsg('systemMessage'); return false;"">Upload</a> | ")
                                                response.Write("Download | Delete")
                                                response.write(" ]</div>")										
                                            end if
                                        else
                                            response.write("Crystal Report file for this report.")
                                        end if
                                        %>
                                    </div>                                                             
                                    
                                  </td>
                                </tr>                                                        
								<% 								
							end if
							%>

      						<tr>
                              <td class="formLabel">
                              	<% if cLng(sLogid) = cLng(rptLogid) then %>
		                      		<!-- ALL users can edit their own report categories -->
    	                           	<a href="#" class="formLabelLinkImg" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=logins&field=ReportsCategory&form=optCategory&pageView=reports', 320, 375, 'no'); return false;" onmouseover="javascript:configImage('congif_Category','over');" onmouseout="javascript:configImage('congif_Category','out');"><img id="congif_Category" src="../_images/x_cleardot_16.gif" align="absmiddle" border="0" /></a>
        	                      	<a href="#" class="formLabelLink" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=logins&field=ReportsCategory&form=optCategory&pageView=reports', 320, 375, 'no'); return false;" onmouseover="javascript:configImage('congif_Category','over');" onmouseout="javascript:configImage('congif_Category','out');">Category:</a>
                                <%
								else
									response.write("Category:")
                                end if 
								%>
                              </td>
                              <td align=left nowrap style="padding-bottom:5px;">                                                            	 
                              	<div style="padding-bottom:3px;">
                                	<%
									dim arrCategory									  
                                    mySQL="SELECT ReportsCategory " _
                                        & "FROM   Logins " _
                                        & "WHERE  Logid=" & rptLogid & " AND CustomerID='" & customerID & "' "
                                    'set rs = openRSexecute(mySQL)
									set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
									if not rs.eof then
										arrCategory = rs("ReportsCategory")									
									end if
                                    call closeRS(rs)									
									call buildSelect(arrCategory,optCategory,"optCategory","inputMediumLong","Y")
                                    %>
                                </div>
                                <div class="subLabel">Allows for the organizing of reports.</div>
                              </td>
                            </tr>
                            
                            
      						<tr>
                              <td class="formLabel"><span class="required">*</span>Parameter Date:</td>
                              <td align=left nowrap>                              	
                                <input name="optParamDate" id="optParamDate" class="inputShort" value="<% =optParamDate %>" maxlength="20" />
                                <div class="subLabel">Date used when populating location parameter selections.</div>
                              </td>
                            </tr>


      						<tr>
                              <td class="formLabel" style="background-color:#FFFFC8;">Dataset:</td>
                              <td align=left nowrap style="background-color:#FFFFC8;">
							  	<% =optDataset %><% if lCase(optDatasetCustID)="mycm" then response.write("&nbsp;[Built-In]") else response.write("&nbsp;[User-Defined]") %>
                              	<div class="subLabel"><% =optDatasetDesc %></div>
                              </td>
                            </tr>

						</table>
   						<!-- END Settings Form Fields -->

                    </td>
                                        
                </tr>
            
            </table>
            <!-- END Settings table -->
	
    	</div>
		<!-- STOP Settings div -->        

		<script language="javascript">
			//set new RPT file...called from fancyupload-crystal.asp
			function addDocUploaded(file,date,size,ext){
				var secLevel = parseInt('<% =cLng(session(session("siteID") & "adminLoggedOn")) %>');
				if (secLevel < 3 ) {					
					var fileName = document.getElementById('optFileName');
					addOptionsItems('optFileName',file,file);
					fileName.selectedIndex = fileName.options.length-1;
				} else {
					document.getElementById('ReportFile').innerHTML = file;
					document.getElementById('optFileName').value = file;
				}
				
			}

			function confirmDocDelete(file) {
				confirmPrompt('Confirm Delete','This action can not be undone.<br><br>Are you sure you want to continue?','redirect',file);
			}
		</script>

