<%@ Language=VBScript %>

 <%
 dim style, chart
 Dim myArray(4)
 
	' Create a new Swiff Chart object.
	Set chart= Server.CreateObject("SwiffChartObject.ChartObj")

	' Fill the chart with data stored in the query string
	chart.SetSeparators ";", true	
	chart.SetDataFromQuery

	' Get style
	style = Request.QueryString("loadstyle")
	chart.LoadStyle style

	'Set the horizontal axis title
	chart.SetAxisTitle 0, Request.QueryString("chartXAxis")

	'Set the vertical axis title
	chart.SetAxisTitle 1, Request.QueryString("chartYAxis")


'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
'COULDN'T GET THIS TO WORK. CHART STILL SHOWING 
'1-1-1-, 2-2-2- AND SO ON...
	'Set the min-max displayed values of the Y axis
'	chart.SetAxisMinValue 1, 0
'	chart.SetAxisMaxValue 1, cLng(Request.QueryString("chartYMaxValue"))
'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


	' Disable animation looping 
	chart.SetLooping False
	
	' Finally generate the Flash chart and provide the browser with it	
	chart.ExportAsResponse
	
	' Release the chart object
	Set chart= Nothing
	Response.End 

 %>
