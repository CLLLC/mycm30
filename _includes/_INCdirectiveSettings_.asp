        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Settings div -->
        <div id="tab:settings" style="display:none;">        
        
            <!-- START Settings table -->   
	        <table id="table_settings" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>
                    
                    <td valign="top">
                                        
					  	<!-- START Settings Form Fields -->	                    
						<table id="table:settings:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">
                           
      						<tr>
                                <td class="formLabel">Lifespan:</td>
                                <td align=left>                                
                                    <div style="float:left;">
                                    	<div class="subLabel">Begins</div>
                                    	<input name="dirBegins" id="dirBegins" class="inputShort" value="<% =dirBegins %>" maxlength="20" />
                                    </div>
                                  	<div>
                                    	<div class="subLabel">Ends</div>
                                    	<input name="dirEnds" id="dirEnds" class="inputShort" value="<% =dirEnds %>" maxlength="20" />
                                	</div>                                
                                    <div class="subLabel" style="padding-top:5px;">Date range directive will be available.</div>
                                </td>
                            </tr>

                            <tr>
                              <td class="formLabel">Sort Order:</td>
                              <td align=left>
                              	<input name="dirSortOrder" id="dirSortOrder" class="inputShort" value="<% =dirSortOrder %>" maxlength="255" />
                                <div class="subLabel" style="padding-top:5px;">Numeric entries only.</div>
                              </td>
                            </tr>
							<script>
								//forces numeric entries only
                                jQuery('#dirSortOrder').keyup(function () {this.value = this.value.replace(/[^0-9\.]/g,''); });
                            </script>

      						<tr>
                              <td class="formLabel">Event:</td>
                              <td align=left nowrap>
                              	<div>
                                    <select name="dirEvent" id="dirEvent" size=1>
                                    	<option value="" selected="selected">-- Select --</option>
                                      	<%
										if len(dirEvent) > 0 then
											response.write("<option value=""" & dirEvent & """ selected=""selected"">" & dirEvent & "</option>")
										end if										
										mySQL="SELECT Event " _
											& "FROM   Directive_Event a " _
											& "WHERE NOT EXISTS " _
											& "      	(SELECT b.Event " _
											& "       	 FROM   Directive b " _
											& "       	 WHERE  a.Event=b.Event " _
											& "		  	 AND    b.CustomerID = '" & customerID & "' ) " _
											& "ORDER By Event "
										set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                      	do while not rs.eof								
                                          	Response.Write "<option value=""" & rs("event") & """ " & checkMatch(dirEvent,rs("event")) & ">" & rs("event") & "</option>"				
                                          	rs.movenext
                                      	loop
                                      	call closeRS(rs)
                                      	%>
                                    </select>	                                                                                          
                                </div>
                                <div class="subLabel" style="padding-top:5px;">Used for setting when directive is shown during intake process.</div>
                              </td>
                            </tr>
                                          
						</table>
   						<!-- END Settings Form Fields -->

                    </td>
                                        
                </tr>
            
            </table>
            <!-- END Settings table -->
	
    	</div>
		<!-- STOP Settings div -->        


