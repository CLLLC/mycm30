        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Account div -->
        <div id="tab:configure">
                
            <!-- START User Account table -->   
	        <table id="table_configure" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>
                    
                    <!-- START Middle Section -->    
                    <td valign="top">
                                        
					  	<!-- START Account Info Fields -->	                    
						<table id="table:configure:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">

                            <tr>
                              <td class="formLabel"><span class="required">*</span>Name:</td>
                              <td align=left>
                              	<input name="temName" id="temName" onkeyup="updateTitleText(this,'titleText','Template:&nbsp;')" class="inputLong" value="<% =temName %>" maxlength="255" />
                              </td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center">
                                	<table width="95%">
                                    	<tr>
                                        	<td align="left" class="clearFormat">
                                				<div style="color:#35487B; font-weight:bold; padding:3px;">Template</div>
                                				<textarea name="temTemplate" id="temTemplate_<% =recId %>" style="width:100%; height:250px; margin-bottom:12px;" ><%=server.HTMLEncode(temTemplate & "")%></textarea>
                            				</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

						</table>
   						<!-- END Account Info Form Fields -->
                                                                        
                    </td>
                    <!-- STOP Middle Section -->			                    
                                        
                </tr>
            
            </table>
            <!-- END User Account table -->
	
    	</div>
		<!-- STOP Account div -->        
                                         														

