<%
'********************************************************************
' Product  : C-LIVE ComplianceLine Web Reports
' Version  : 2.0
' Modified : November 2004
' Copyright: Copyright (C) 2004 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'			 website, please contact webmaster@complianceline.com
'********************************************************************

'*************************************************************************
'Declare some standard ADO variables
'*************************************************************************
Const adOpenKeyset		= 1
Const adOpenDynamic		= 2
Const adOpenStatic		= 3
Const adLockReadOnly	= 1
Const adLockOptimistic	= 3
Const adStateClosed		= &H00000000
Const adUseServer		= 2
Const adUseClient		= 3
Const adCmdText			= &H0001
Const adCmdTable		= &H0002
Const adSearchForward 	= 1
Const adSearchBackward 	= -1
Const adCmdStoredProc	= 4
Const adParamInput		= 1
Const adParamOutput		= 2

'ADO DataTypeEnum Values
Const adEmpty 			= 0
Const adTinyInt 		= 16
Const adSmallInt 		= 2
Const adInteger 		= 3
Const adBigInt 			= 20
Const adSingle 			= 4
Const adDouble 			= 5
Const adCurrency 		= 6
Const adDecimal 		= 14
Const adNumeric 		= 131
Const adBoolean 		= 11
Const adError 			= 10
Const adUserDefined 	= 132
Const adVariant 		= 12
Const adIDispatch 		= 9
Const adIUnknown 		= 13
Const adGUID 			= 72
Const adDate 			= 7
Const adDBDate 			= 133
Const adDBTime 			= 134
Const adDBTimeStamp 	= 135
Const adBSTR 			= 8
Const adChar 			= 129
Const adVarChar 		= 200
Const adLongVarChar 	= 201
Const adWChar 			= 130
Const adVarWChar 		= 202
Const adLongVarWChar 	= 203
Const adBinary 			= 128
Const adVarBinary 		= 204
Const adLongVarBinary 	= 205

'*************************************************************************
'Open Database Connection
'*************************************************************************
function openDB()
	set cn = server.createobject("adodb.connection")
	on error resume next	
	cn.Open ConnString	
	if err.number <> 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Cannot open database: " & err.Description)
	end if
	on error goto 0	
end function

'*************************************************************************
'Open Stored Procedure
' Example use:
'	call openStoredProc("proc_GetPassword")					
'	cmd.Parameters.Append(cmd.CreateParameter("@UserName", adVarWChar, adParamInput, 50, "optional value"))    
'	cmd.Parameters.Append(cmd.CreateParameter("@Passwrd", adVarWChar, adParamOutput, 50, "optional value"))
'	cmd.Execute
'	passTest = cmd.Parameters("@Passwrd").Value
'*************************************************************************
function openStoredProc(myProcedure)
	set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = myProcedure
	cmd.ActiveConnection = cn
	cmd.NamedParameters = true
	cmd.CommandType = adCmdStoredProc
	on error resume next
	if err.number <> 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Cannot open stored procedure: " & err.Description)
	end if
	on error goto 0
end function

'*************************************************************************
'Open Database Connection to PBX
'*************************************************************************
function openPBXdb()
	set cn = server.createobject("adodb.connection")
	on error resume next	
	cn.Open connPBX	
	if err.number <> 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Cannot open database: " & err.Description)
	end if
	on error goto 0	
end function

'*************************************************************************
'Open Database Connection to Archive DB
'*************************************************************************
function openArchivedb()
	set cn = server.createobject("adodb.connection")
	on error resume next	
	cn.Open connStringArchive
	if err.number <> 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Cannot open database: " & err.Description)
	end if
	on error goto 0	
end function

'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
'TEMPORARY for CLIVE UPDATES
function openDBCLive(strMDB)
	set cnCLIVE = server.createobject("adodb.connection")
	on error resume next	
	cnCLIVE.Open "driver={Microsoft Access Driver (*.mdb)};DBQ=\\172.168.1.12\web\data\wrs\" & strMDB & "\" & strMDB & ".mdb;"	
	if err.number <> 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Cannot open database: " & err.Description)
	end if
	on error goto 0	
end function
'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
					
'*************************************************************************
'Close Database Connection
'*************************************************************************
function closeDB()
	on error resume next
	cn.close
	set cn = nothing
	on error goto 0
end function

'*************************************************************************
'Open RecordSet using "Execute" method
'*************************************************************************
function openRSexecute(mySQL)
	on error resume next
	set openRSexecute = cn.execute(mySQL)
	if err.number <> 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Cannot execute recordset: " & err.Description)
	end if
	on error goto 0
end function

function openRSexecuteAjax(mySQL)
	on error resume next
	set openRSexecuteAjax = cn.execute(mySQL)
	if err.number <> 0 then
		response.write("Error: Cannot execute recordset: " & err.Description)
		response.end
	end if
	on error goto 0
end function

function openRSexecuteDialog(mySQL)
	on error resume next
	set openRSexecuteDialog = cn.execute(mySQL)
	if err.number <> 0 then
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Cannot execute recordset: " & err.Description)
	end if
	on error goto 0
end function

function openRSCLIVEexecuteAjax(mySQL)
	on error resume next
	set openRSCLIVEexecuteAjax = cnCLIVE.execute(mySQL)
	if err.number <> 0 then
		response.write("Error: Cannot execute recordset: " & err.Description)
		response.end
	end if
	on error goto 0
end function

'*************************************************************************
'Open RecordSet using "Open" method
'*************************************************************************
function openRSopen(dbSource,dbCursorLoc,dbCursorType,dbLockType,dbOptions,dbCache)
	set openRSopen = Server.CreateObject("ADODB.Recordset")
	if dbCache > 0 then
		openRSopen.CacheSize = dbCache
	end if
	if dbCursorLoc > 0 then
		openRSopen.CursorLocation = dbCursorLoc
	end if
	on error resume next
	openRSopen.Open dbSource,cn,dbCursorType,dbLockType,dbOptions
	if err.number <> 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Cannot open recordset: " & err.Description)
	end if
	on error goto 0
end function

function openRSopenAjax(dbSource,dbCursorLoc,dbCursorType,dbLockType,dbOptions,dbCache)
	set openRSopenAjax = Server.CreateObject("ADODB.Recordset")
	if dbCache > 0 then
		openRSopenAjax.CacheSize = dbCache
	end if
	if dbCursorLoc > 0 then
		openRSopenAjax.CursorLocation = dbCursorLoc
	end if
	on error resume next
	openRSopenAjax.Open dbSource,cn,dbCursorType,dbLockType,dbOptions
	if err.number <> 0 then
		response.write("Error: Cannot open recordset: " & err.Description)
		response.end
	end if
	on error goto 0
end function

function openRSopenDialog(dbSource,dbCursorLoc,dbCursorType,dbLockType,dbOptions,dbCache)
	set openRSopenDialog = Server.CreateObject("ADODB.Recordset")
	if dbCache > 0 then
		openRSopenDialog.CacheSize = dbCache
	end if
	if dbCursorLoc > 0 then
		openRSopenDialog.CursorLocation = dbCursorLoc
	end if
	on error resume next
	openRSopenDialog.Open dbSource,cn,dbCursorType,dbLockType,dbOptions
	if err.number <> 0 then
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Cannot open recordset: " & err.Description)
	end if
	on error goto 0
end function

'*************************************************************************
'Open RecordSet using "Execute" method
'*************************************************************************
function openRSschema(mySchema)
	on error resume next
	set openRSschema = cn.OpenSchema(mySchema)
	if err.number <> 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Cannot open database schema: " & err.Description)
	end if
	on error goto 0
end function

'*************************************************************************
'Close Recordset
'*************************************************************************
function closeRS(rs)
	on error resume next
	rs.Close
	set rs = nothing
	on error goto 0
end function
%>