        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Account div -->
        <div id="tab:details">
                
            <!-- START User Account table -->   
	        <table id="table_details" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>

	                <!-- START Side Page Tabs [genearl] "id=table:details:general" --> 
                    <!-- [this = object makeing the call ] -->
                    <!-- [details = current top tab ] -->
                    <!-- [general = selected side tab ] -->                    
                    <td rowspan="4" valign="top" style="width:100px; border-top:none; padding:0px; ">
                        <div class="tabSide" style="width:100px;">                        
                            <div id="general" class="on" onClick="clickSideTab('details','general'); resetSystemMsg('systemMessage');">Contact</div>			                               
                            <div id="employee" class="off" onClick="clickSideTab('details','employee'); resetSystemMsg('systemMessage');">Employee</div>			                               
                            <div id="login" class="off" onClick="clickSideTab('details','login'); resetSystemMsg('systemMessage');">Login</div>                            
                            <div class="empty">&nbsp;</div>		
                        </div>                        
                    </td>
                    <!-- END Side Page Tabs -->

                    
                    <!-- START Middle Section -->    
                    <td valign="top">

   						<!-- START Contact Info Form Fields -->                        
						<table id="table:details:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">                

							<!-- DBAdmin and CCI Admin -->
							<% if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then %>
                                <tr>
                                  <td class="formLabel" style="background-color:#FFFFC8;">Issue Delivery:</td>
                                  <td align=left nowrap style="background-color:#FFFFC8;">
                                                                 
                                    <div style="float:left;">
                                        <div class="subLabel">Priority:</div>
                                        <input name="userPriority" id="userPriority" class="inputShort" value="<% =userPriority %>" maxlength="255" />
                                    </div>
                                    
                                    <div style="float:left;">
                                        <div class="subLabel">Preference:</div>
                                        <input name="userDelivery" id="userDelivery" class="inputMediumLong" value="<% =userDelivery %>" maxlength="255" />
                                    </div>
                                    
                                  </td>
                                </tr>
							<!-- All other -->                                
							<% else %>
	                            <input name="userPriority" id="userPriority" type="hidden" value="<% =userPriority %>" maxlength="255" />
    	                        <input name="userDelivery" id="userDelivery" type="hidden" value="<% =userDelivery %>" maxlength="255" />
							<% end if %>                            

      						<tr>
                              <td class="formLabel"><span class="required">*</span>Name:</td>
                              <td align=left nowrap>                              
                              	<div style="float:left;">
                              		<div class="subLabel">First Name:</div>
                                    <div><input name="userFirstName" id="userFirstName" class="inputMedium" value="<% =userFirstName %>" maxlength="20" /></div>
                              	</div>                                
                              	<div style="float:left;">
                              		<div class="subLabel">Last Name:</div>
                                    <div>
                                    	<div style="float:left;">
                                    		<input name="userLastName" id="userLastName" class="inputMedium" value="<% =userLastName %>" maxlength="20" />
                                        </div>
                                    </div>
                              	</div>
                              </td>
                            </tr>     

                            <tr>
                                <td class="formLabel">E-Mail:</td>
                                <td align=left nowrap><input name="userEmail" id="userEmail" class="inputLong" type=text value="<% =userEmail %>" size=50 maxlength=50></td>
                            </tr>

                            <tr>
                                <td class="formLabel">Job Title:</td>
                                <td align=left nowrap><input name="userTitle" id="userTitle" class="inputLong" type=text value="<% =userTitle %>" maxlength=50></td>
                            </tr>
                            
                            <tr>
                              <td class="formLabel">Address:</td>
                              <td align=left nowrap>
                              
                                <div class="subLabel">Street Address:</div>
                                <div>
                                    <textarea name="userAddress" id="userAddress" style="width:300px; height:50px;"><% =userAddress %></textarea>
                                </div>
                                
                                <div style="float:left;">
	                                <div class="subLabel">City:</div>
                                    <input name="userCity" id="userCity" class="inputShort" value="<% =userCity %>" maxlength="255" />
                                </div>
                                
                                <div style="float:left;">
    	                            <div class="subLabel">State/Province:</div>
                                    <input name="userState" id="userState" class="inputShort" value="<% =userState %>" maxlength="255" />
                                </div>
                                
                                <div>                                
        	                        <div class="subLabel">Postal Code:</div>
                                    <input name="userPostalCode" id="userPostalCode" class="inputShort" value="<% =userPostalCode %>" maxlength="255" />
                                </div>

                              </td>
                            </tr>
                                                        
                            <tr>
                              <td class="formLabel">Phone Number(s):</td>
                              <td align=left nowrap>
                                                              
                                <div style="float:left;">
                                <div class="subLabel">Home Phone:</div>
                                    <input name="userPhone_Home" id="userPhone_Home" class="inputShort" value="<% =userPhone_Home %>" maxlength="255" />
                                </div>
                                
                                <div style="float:left;">
                                <div class="subLabel">Work Phone:</div>
                                    <input name="userPhone_Work" id="userPhone_Work" class="inputShort" value="<% =userPhone_Work %>" maxlength="255" />
                                </div>                                                                
                                
                                <div class="subLabel">
                                	<div>Mobile Phone:</div>
                                    <input name="userPhone_Cell" id="userPhone_Cell" class="inputShort" value="<% =userPhone_Cell %>" maxlength="255" />
                                </div>
                                
                                <div style="float:left;">
                                <div class="subLabel">Fax:</div>
                                    <input name="userPhone_Fax" id="userPhone_Fax" class="inputShort" value="<% =userPhone_Fax %>" maxlength="255" />
                                </div>
                                
                                <div style="float:left;">
                                <div class="subLabel">Pager:</div>
                                    <input name="userPhone_Pager" id="userPhone_Pager" class="inputShort" value="<% =userPhone_Pager %>" maxlength="255" />
                                </div>
                                                                                                
                                <div>
                                	<div class="subLabel">Other Phone:</div>
                                    <input name="userPhone_Other" id="userPhone_Other" class="inputShort" value="<% =userPhone_Other %>" maxlength="255" />
                                </div>
                              
                              </td>
                            </tr>


							<!-- DBAdmin and CCI Admin -->
							<% if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then %>
                                <tr>
                                    <td colspan="2" align="center">
                                        <table width="95%">
                                            <tr>
                                                <td align="left" class="clearFormat">
                                                    <div style="color:#35487B; font-weight:bold; padding:3px;">Notes (CCI use only)</div>
                                                    <textarea name="userNotes" id="userNotes" style="width:100%; height:100px; margin-bottom:12px;" ><% =server.HTMLEncode(userNotes & "") %></textarea>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
							<!-- All other -->                                
							<% else %>
	                            <input name="userNotes" id="userNotes" style="display:none;" value="<% =server.HTMLEncode(userNotes & "") %>" maxlength="255" />
							<% end if %>                            

                        </table>
   						<!-- END Contact Info Form Fields -->


   						<!-- START Employee Info Form Fields -->                        
						<table id="table:details:employee" width="100%" class="formTable" cellpadding="0" cellspacing="0">                

                            <tr>
                                <td class="formLabel">Employee ID:</td>
                                <td align=left nowrap><input name="userEmployee_ID" id="userEmployee_ID" class="inputLong" type=text value="<% =userEmployee_ID %>" size=50 maxlength=50></td>
                            </tr>

                            <tr>
                                <td class="formLabel">Job Class:</td>
                                <td align=left nowrap><input name="userJob_Class" id="userJob_Class" class="inputLong" type=text value="<% =userJob_Class %>" size=50 maxlength=50></td>
                            </tr>

                            <tr>
                                <td class="formLabel">Division:</td>
                                <td align=left nowrap><input name="userDivision" id="userDivision" class="inputLong" type=text value="<% =userDivision %>" size=50 maxlength=50></td>
                            </tr>

                            <tr>
                                <td class="formLabel">Region:</td>
                                <td align=left nowrap><input name="userRegion" id="userRegion" class="inputLong" type=text value="<% =userRegion %>" size=50 maxlength=50></td>
                            </tr>

                            <tr>
                                <td class="formLabel">Department:</td>
                                <td align=left nowrap><input name="userDepartment" id="userDepartment" class="inputLong" type=text value="<% =userDepartment %>" size=50 maxlength=50></td>
                            </tr>
                            
                        </table>                            
   						<!-- END Employee Info Form Fields -->
                        
                                           
                        <!-- START Account Info Fields -->	                    
						<table id="table:details:login" width="100%" class="formTable" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="clearFormat">                                 
            
                                    <!-- account active y/n/c table -->	       
                                    <table width="100%" cellpadding="0" cellspacing="0">

                                        <tr>
                                          <td class="formLabel" style="border-bottom:1px dotted #cccccc; background-color:#FFFFC8;">User Account:</td>
                                          <td align=left nowrap style="border-bottom:1px dotted #cccccc; background-color:#FFFFC8;">        
                                            <% if programAdmin = "Y" or cLng(session(session("siteID") & "adminLoggedOn")) < 3 then %>                                                       
                                                <label>
                                                    <input type="radio" name="userActive" id="userActiveYes" onclick="setAccountDisplay('');" value="Y" style="vertical-align:middle;" <% if ((cLng(appUserCount)+1)>cLng(appNumUsers)) and userActive<>"Y" then response.write("disabled=""disabled""") %> <%=checkRadio(userActive,"Y")%> >Enable&nbsp;&nbsp;&nbsp;&nbsp;
                                                </label>			      
                                                <label>
                                                	<input type="radio" name="userActive" id="userActiveContact" onclick="setAccountDisplay('none');" value="C" style="vertical-align:middle;" <%=checkRadio(userActive,"C")%>>Contact Only&nbsp;&nbsp;&nbsp;&nbsp;
                                                </label>                                                
                                                <label>
                                                	<input type="radio" name="userActive" id="userActiveNo" onclick="setAccountDisplay('none');" value="N" style="vertical-align:middle;" <%=checkRadio(userActive,"N")%>>Disable
                                                </label>
												<%
                                                dim countMsg
                                                countMsg = "The numbers below are based on profile settings and<br/>your myCM contract. To increase your user limit please<br/>contact CCI at 1-800-617-0415 or<br/>e-mail support@ccius.com.<br/><br/>"
												countMsg = countMsg & "<strong>User limit details:</strong><br/>"
                                                countMsg = countMsg & "Current user count = " & FormatNumber(appUserCount,0) & "<br/>"
                                                countMsg = countMsg & "User count allowed = " & FormatNumber(appNumUsers,0) & "<br/><br/>"
                                                countMsg = countMsg & "<strong>Available users " & FormatNumber(cLng(appNumUsers)-cLng(appUserCount),0) & "</strong>"
                                               	%>                                                                                                                                
                                               	<div class="subLabel" style="padding-top:5px;">Users may be added depending on the current <a href="#" onclick="jAlert('<% =countMsg %>', 'Program Administrator'); return false;">User Count</a>.</div>                                                                                                                     
                                           		<%
                                            else
                                                if userActive = "Y" then 
													response.write("Enable") 
												elseif userActive = "C" then 
													response.write("Contact Only") 
												else 
													response.write("Disable")
												end if
                                                response.write("<input name=""userActive"" id=""userActive"" type=""hidden"" value=""" & userActive & """/>")
                                            end if
                                            %>                                                                    
                                          </td>
                                        </tr>
                                    </table>
                                    
									<!-- NO MORE USERS allow -->
									<% if (cLng(appUserCount)+1)>cLng(appNumUsers) and userActive<>"Y" and (programAdmin="Y" or cLng(session(session("siteID") & "adminLoggedOn")) < 3) then %>
                                        <table id="noNewUsersTable" width="95%" cellpadding="0" cellspacing="0" style="margin-top:10px;">
                                            <tr>
                                                <td align="left" class="clearFormat" style="padding-right:2px;">                                                                                                
                                                    <% call systemMessageBox("maxedoutTableID","statusMessageALERT","You have exceeded the number of user accounts allowed on myCM.") %>
                                                </td>
                                            </tr>
                                        </table>
                                    <% end if %>                                    
                                    <!--STOP account active y/n/c table -->	                                               

									<!-- NO MORE USERS allow -->									
                                    <table id="userDisableTable" width="95%" cellpadding="0" cellspacing="0" style="margin-top:10px; <% if userActive<>"N" then response.write("display:none;") %>">
                                        <tr>
                                            <td align="left" class="clearFormat" style="padding-right:2px;">                                                                                                
                                                <% call systemMessageBox("disableTableID","statusMessageINFO","User account set to 'Disable' and deactivated.<br/>&nbsp;&nbsp;&raquo;&nbsp;Once saved you will no longer have access to this user account.<br/>&nbsp;&nbsp;&raquo;&nbsp;Any existing security and site preferences will be reset.") %>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--STOP account active y/n/c table -->	                                               

									<!-- NO MORE USERS allow -->
                                    <table id="userContactTable" width="95%" cellpadding="0" cellspacing="0" style="margin-top:10px; <% if userActive<>"C" then response.write("display:none;") %>">
                                        <tr>
                                            <td align="left" class="clearFormat" style="padding-right:2px;">                                                                                                
                                                <% call systemMessageBox("contactTableID","statusMessageINFO","User account set to 'Contact Only'.<br/>&nbsp;&nbsp;&raquo;&nbsp;User can be assigned to issues for tracking purposed only.<br/>&nbsp;&nbsp;&raquo;&nbsp;Any existing security and site preferences will be reset.") %>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--STOP account active y/n/c table -->	                                               
                                                    
                                </td>                
                            </tr>

           
                            <tr>
                                <td class="clearFormat">
            
                                    <!-- account/login details table -->	
                                   	<!-- note: visiblity of this table is taken care of on window.onload -->	
                                  	<!-- 	   so spinner heights present properly						 -->	
                                    <table id="tableAccount" width="100%" cellpadding="0" cellspacing="0">                    
                                        <tr>
                                            <td class="formLabel" style="border-top:none;"><span class="required">*</span>Username:</td>
                                            <td align=left nowrap style="border-top:none;">
                                              <div class="subLabel">
                                                  <ul style="padding-left:15px; margin-left:0px; margin-bottom:5px;">
                                                      <li style="color:#666;">Must be at least 6 characters and no more than 25 characters.</li>
                                                      <li style="color:#666;">Cannot contain 'admin' or 'administrator'.</li>
                                                  </ul>
                                              </div>
                                              <div style="float:left;">
                                              	<input name="userName" id="userName" class="inputMedium" type=text value="<% =userName %>" size=20 maxlength=100>
                                              </div>
                                              <div>
												<a class="myCMbuttonSmall" href="#" title="Insert E-mail" onclick="this.blur(); fillUserName(); return false;"><span class="email" style="padding-right:7px;">&nbsp;</span></a>                                                
                                                <% if programAdmin = "Y" or cLng(session(session("siteID") & "adminLoggedOn")) < 3 then %>
	                                                <a class="myCMbutton" href="#" title="Send Login" onclick="this.blur(); sendLogin(); return false;"><span class="emailsend" style="padding-right:5px;">Send Login</span></a>
                                              	<% end if %>
                                              </div>
                                            </td>
                                        </tr>
            

					<% 
					'showing this was confusing users
					if 1=2 then %>
                                        <tr>
                                            <td class="formLabel"><span class="required">*</span>Password:</td>
                                            <td align=left>
                                            	<% 
												''''if action = "add" or action = "copy" or len(userPassword) <= 0 then 
												''''	response.write("<em>enter new password below</em>")
												''''else
												''''	response.write("********")												
												''''end if												
												%>
                                            </td>
                                        </tr>
					<% end if %>

            
                                        <tr>
                                            <td class="formLabel">New Password:</td>
                                            <td align=left>
                                            
                                                <table>
                                                    <tr>
                                                        <td style="border:none; padding:0px; margin:0px;">
                                                    <div><input name="userPassword1" id="userPassword1" class="inputMedium" type="password" value="" size=20 maxlength=20></div>
                                                    <div class="subLabel" style="padding-top:5px; padding-bottom:5px;">Confirm by entering again:</div>
                                                    <div><input name="userPassword2" id="userPassword2" class="inputMedium" type="password" value="" size=20 maxlength=20></div>
                                                        </td>
                                                        <td style="border:none; padding:0px; margin:0px;">
                                                            <div id='passMeter'></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                    
                                                <!-- STRONG password -->
                                                <% if lCase(passwordStrength) = "strong" then %>
                                                    <script>
                                                        $('#userPassword1').simplePassMeter({
                                                            'requirements': {
                                                                'matchField': {'value': '#userPassword2'},
                                                                'minLength': {'value': 8},  // at least 8 characters
                                                                'lower': {'value': true},   // at least 1 lower-case letter
                                                                'upper': {'value': true},   // at least 1 upper-case letter
                                                                'special': {'value': true} 	// at least 1 special character
                                                            },
                                                            'offset': 10,
                                                            'container': '#passMeter'
                                                        });
                                                        $('#userPassword1').bind('score.simplePassMeter', function(jQEvent, score) {$('#passwordScore').text(score);});
                                                    </script>
            
                                                <!-- MEDIUM password -->                                        
                                                <% elseif lCase(passwordStrength) = "medium" then %>                                    
                                                    <script>
                                                        $('#userPassword1').simplePassMeter({
                                                            'requirements': {
                                                                'matchField': {'value': '#userPassword2'},
                                                                'minLength': {'value': 6},  // at least 6 characters
                                                                'lower': {'value': true},   // at least 1 lower-case letter
                                                                'upper': {'value': true},   // at least 1 upper-case letter
                                                                'special': {'value': false} // NOT least 1 special character
                                                            },
                                                            'offset': 10,
                                                            'container': '#passMeter'
                                                        });
                                                        $('#userPassword1').bind('score.simplePassMeter', function(jQEvent, score) {$('#passwordScore').text(score);});
                                                    </script>                                    
            
                                                <!-- WEAK password -->                                        
                                                <% else %>
                                                    <script>
                                                        $('#userPassword1').simplePassMeter({
                                                            'requirements': {
                                                                'matchField': {'value': '#userPassword2'}
                                                            },
                                                            'offset': 10,
                                                            'container': '#passMeter'												
                                                        });
                                                        $('#userPassword1').bind('score.simplePassMeter', function(jQEvent, score) {$('#passwordScore').text(score);});
                                                    </script>                                    
                                                <% end if %>                                    
            
                                                <span id='passwordScore' style="display:none;"></span>
                                                <input name="userPassword" id="userPassword" type="hidden" value="<% =userPassword %>"/>
                                                <input name="passwordStrength" id="passwordStrength" type="hidden" value="<% =passwordStrength %>"/>
                                                <input name="passwordHistory" id="passwordHistory" type="hidden" value="<% =passwordHistory %>"/>                                  
            
                                            </td>
                                        </tr>
            
                                        <tr>
                                            <td class="formLabel"><span class="required">*</span>Password Reset:</td>
                                            <td align=left>
                                                <% if programAdmin="Y" or cLng(session(session("siteID") & "adminLoggedOn")) < 3 then %>
                                                    <input name="passwordReset" id="passwordReset" class="inputShort" value="<% =passwordReset %>" maxlength="20" />
                                                    <div class="subLabel" style="padding-top:5px;">Forces user to change password at logon.</div>
                                                <%
                                                else
                                                    response.write(passwordReset)
                                                    response.write("<input name=""passwordReset"" id=""passwordReset"" type=""hidden"" value=""" & passwordReset & """/>")
                                                end if
                                                %>                                
                                             </td>
                                        </tr>
            
                                        <tr>
                                            <td class="formLabel"><span class="required">*</span>Account Expires:</td>
                                            <td align=left>
                                                <% if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then %>
                                                    <input name="userExpires" id="userExpires" class="inputShort" value="<% =userExpires %>" maxlength="20" />
                                                    <div class="subLabel" style="padding-top:5px;">Deactivates user account.</div>
                                                <%
                                                else
                                                    if len(userExpires) > 0 then
                                                        response.write(userExpires)
                                                    else
                                                        response.write("Never Expires")
                                                    end if
                                                    response.write("<input name=""userExpires"" id=""userExpires"" type=""hidden"" value=""" & userExpires & """/>")
                                                end if
                                                %>                                
                                             </td>
                                        </tr>

                                        <tr>
                                            <td class="formLabel">Last Login:</td>
                                            <td align=left><% =userLoginDate %>&nbsp;</td>
                                        </tr>
            
                                    	<!-- login attempts -->                                                                        					    
					    <% if programAdmin="Y" or cLng(session(session("siteID") & "adminLoggedOn")) < 3 then %>
                                            <tr>
                                                <td class="formLabel" style="background-color:#FFFFC8;"><% if loginAttempts >= "5" then response.write("<span class=""required"">Account Locked</span>:") else response.write("Login Attempts:") %></td>
                                                <td align=left style="background-color:#FFFFC8;">
                                                    <input name="loginAttempts" id="loginAttempts" class="inputShort" value="<% =loginAttempts %>" maxlength="20" />
                                                    <div class="subLabel" style="padding-top:5px;">A setting of five (5) locks user account.</div>
                                                </td>
                                            </tr>
                                            <script type="text/javascript">
                                                jQuery().ready(function($) {
                                                    $('#loginAttempts').spinner({ min: 0, max: 60, increment: 'fast' });			
                                                });
                                            </script>                                                  
	                                   	<%
    	                                else
                                        	response.write("<input name=""loginAttempts"" id=""loginAttempts"" type=""hidden"" value=""" & loginAttempts & """/>")
	                                    end if
    	                                %>          
            
                                    </table>
                                    <!-- STOP account/login details table -->
                                                                        
                                </td>
                            </tr>
						</table>
						<!-- END Account Info Form Fields -->                                                


                    </td>
                    <!-- STOP Middle Section -->			                    
                                        
                </tr>
            
            </table>
            <!-- END User Account table -->
	
    	</div>
		<!-- STOP Account div -->        

		<script>
			function setAccountDisplay(display) {
				
				var securityOnEdit = parseFloat('<% =securityLevel %>');
				var securityDefault = 40;
				
				//reset all message boxes
				changeObjectDisplay('noNewUsersTable','none');
				changeObjectDisplay('userDisableTable','none');
				changeObjectDisplay('userContactTable','none');

				
				//document.getElementById('notes').style.display
				
				//show notices if necessary				
				if (document.getElementById('userActiveNo').checked) changeObjectDisplay('userDisableTable','');
				if (document.getElementById('userActiveContact').checked) changeObjectDisplay('userContactTable','');
				//set account visibility
				changeObjectDisplay('tableAccount',display);
				//permissions/security tab
				changeObjectDisplay('settings',display);
				//reports tab
				changeObjectDisplay('reports',display);
				
				//  set initial security setting
				//	null is nothing
				//	60 and 99 are settings for disabled users
				if ( (securityOnEdit==null || securityOnEdit==60 || securityOnEdit==99) && display=='' ) {
					//set values for activated user
					$( "#securityLevel" ).val( securityDefault );
					$( "#securitySlider" ).slider( "option", "value", securityDefault );
					//set default description shown to user
					$.ajax({			  	
						url: "../_jquery/suggestbox/json-data.asp?view=config&field=configValLong&term=SecurityLevel_"+securityDefault,
						cache: false,
						async: false,
						dataType: "json",
						success: function(data) {
							if(data!=null) {				  
								document.getElementById('securityDesc').innerHTML = data;
							} else {
								document.getElementById('securityDesc').innerHTML = "<none>";
							}
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(xhr.status);
							alert(thrownError);                 
						}  		
					});
				}
				
			}
		</script>

		<script>
			function fillUserName() {
				//insert user's email address into user name field
				document.getElementById('userName').value = document.getElementById('userEmail').value;
			}			
		</script>
		