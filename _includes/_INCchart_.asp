<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
%>

<%
sub buildChart(recID,fromPage,chartOutputAs,chartSQL,whereClause)

'Declare variables
dim mySQL, cn, rs
dim field, colCount, xCol, tmpValue

dim mySQLName
dim mySQLStatement
dim mySQLFields
dim mySQLGroup
dim mySQLGroupCalc

'Chart Settings
dim chartType
dim chartXAxis
dim chartXRotate
dim chartYAxis
dim chartYMaxValue
dim chartSize
dim	chartWidth
dim	chartHeight

dim tableWidth
		
	'Look for Query ID
	if recID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid chart reference.")
	end if
						
	'Get Chart Values from ReportQuery table
	if len(recID) > 0 then	
		
		'get report record
		mySQL="SELECT * " _
			& "       FROM Reports a " _
			& "		  WHERE a.ReportID = " & recID		
		'NEED USER LEVEL SECURITY????
		'set rs = openRSexecute(mySQL)
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		if rs.eof then
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid chart reference.")
		else
			mySQLName 		= rs("Name")
			mySQLFields 	= rs("QueryFields")  
			mySQLGroup 		= rs("QuerySumGroup")
			mySQLGroupCalc 	= rs("QuerySumCalculation")		
			mySQLStatement	= rs("SQL")
			chartType		= rs("ChartType")
			chartXAxis		= rs("ChartXAxis")
			chartYAxis		= rs("ChartYAxis")
			chartSize		= rs("ChartSize")				
		end if
		call closeRS(rs)	
	
'		'Update Run Date
'		mySQL = "UPDATE Reports SET " _
'			  & "LastRunDate='" & Now() & "' "
'		mySQL = mySQL & "WHERE ReportID = " & recID
'		set rs = openRSexecute(mySQL)
'		call closeRS(rs)
		
	end if


	'SQL statement already built and passed here...
	mySQLStatement = chartSQL



	'if necessary, refomat WHERE clause
	if len(whereClause) > 0 then
		mySQLStatement = insertWhereClause(mySQLStatement,whereClause)						
	end if
	
	'********************************************************************
	'Set Chart Params
	'********************************************************************
	'Set Chart Size
	if chartSize = "small" then
		chartWidth = 300
		chartHeight = 200
	elseif chartSize = "medium" then
		chartWidth = 450
		chartHeight = 300
	elseif chartSize = "large" then	
		chartWidth = 550
		chartHeight = 350
	elseif chartSize = "xlarge" then	
		chartWidth = 650
		chartHeight = 430	
	else
		chartWidth = 550
		chartHeight = 350
	end if
	
	'********************************************************************
	
	'open RecordSet
	set rs = openRSopen(mySQLStatement,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	
	'sql executed and closed
	if rs.State = adStateClosed then
		response.write("<table width=""100%"" cellpadding=0 cellspacing=0><tr><td style=""padding-right:2px; margin:0px;"">")
		call systemMessageBox("systemMessage","statusMessageINFO","SQL Command Executed.")	
		response.write("</td></tr></table>")
	
	'recordset open...keep going
	else

		dim strChartCategories, strChartSeriesValues, theURL, list_field, list_field_type
		dim strChartCaptions, bolGetCaptions, seriesCount, tempSeries, tempValue, strValue, tempLabel
	
		'no records found, inform user
		if rs.eof then
			'response.write("<table width=""100%"" cellpadding=0 cellspacing=0><tr><td style=""padding-right:2px; margin:0px;"">")
			'call systemMessageBox("systemMessage","statusMessageINFO","No records matched your search criteria for charting.")	
			'response.write("</td></tr></table>")
		
		else
		
			'<-- START building chart -->
			
			'get number of columns returned
			for each field in rs.fields
				colCount = colCount + 1
			next	
			
			'cycle through all records to build series and values
			do while not rs.eof
			
				tempValue = ""
				tempLabel = ""
				xCol = 0
				
				'cycle through all fields
				for each field in rs.fields		
					xCol = xCol + 1
					
					'this is NOT the LAST field so build series values
					if xCol < colCount then	

						list_field = field.name
						list_field_type = field.type
					
						'special case for "user" field
						if lCase(field.name) = "user" then		
							tempValue = mid(rs(field.name),1,inStr(1,rs(field.name),":"))
							tempLabel = tempLabel & mid(rs(field.name),instr(rs(field.name),":")+1,len(rs(field.name))) & " : "

						'all other fields
						else							
						
							'special case for True/Y and False/N column values
							if rs(field.name) = "True" or rs(field.name) = "Y" then
								tempValue = rs(field.name)
								tempLabel = tempLabel & "Yes" & " : "							
							elseif rs(field.name) = "False" or rs(field.name) = "N" then
								tempValue = rs(field.name)
								tempLabel = tempLabel & "No" & " : "								
							else							
								tempValue = rs(field.name)							
								tempLabel = tempLabel & rs(field.name) & " : "	
							end if							
							
						end if					
					
					'LAST column finalize series and build values										
					elseif xCol = colCount then
	
						'finish up series before moving to next record...remove trailing " : "
						strChartCategories = strChartCategories & left(tempLabel,len(tempLabel)-3) & ";"
						strChartSeriesValues = strChartSeriesValues & rs(field.name) & ";"
						'used in filter selection box
						strValue = strValue & tempValue & ";"
																							
					end if			
					
				next
				
				rs.Movenext

			loop
	
			'clean up and remove training ";"
			strChartCategories = left(strChartCategories,len(strChartCategories)-1)
			strChartSeriesValues = left(strChartSeriesValues,len(strChartSeriesValues)-1)
			'''strChartSeriesValues = "series0=" & strChartSeriesValues
			strValue = left(strValue,len(strValue)-1)
							
			'<-- STOP building chart -->


			'close recordset, no longer needed
			call closeRS(rs)

						
			'<-- START table to hold chart -->
			
			'WIDTH of table if not being printed...otherwise remove width
			if lCase(fromPage) <> "print" then tableWidth = "width=""100%""" else tableWidth = ""
			
			'TABLE for chart
			response.write "<table id=""chartTable"" " & tableWidth & " cellpadding=""0"" cellspacing=""0"" style=""border:1px solid #B3B3B3; margin-bottom:5px;"">"									
			response.write("<tr><form name=""frmList"" id=""frmList"" method=""post"" action=""issues.asp?pageView=" & pageView & "&action=reload"">")
			response.write("<td align=""left"" width=""" & chartWidth & """ border=""0"" cellspacing=""0"" cellpadding=""0"" style=""padding:0px;"">")
						
			'********************************************************
			'BUILD CHART...
			Response.Expires= 0
			Dim chart, chart_res
			' Set chart object
			Set chart= Server.CreateObject("SwiffChartObject.ChartObj")
			' Fill the categories
			chart.SetCategoriesFromString strChartCategories
			' Fill the first series named "Sales Y1"
			chart.SetSeriesValuesFromString 0, strChartSeriesValues ' "8;15;6;19"
			' The chart type is stored in the style file (*.scs)
			chart.LoadStyle chartType
			chart.SetWidth chartWidth
			chart.SetHeight chartHeight
			' No looping animation
			chart.SetLooping false
			' Output format
			if chartOutputAs = "swf" then
				chart.SetTitle listName
				chart.SetOutputFormat "SWF"
			elseif chartOutputAs = "png" then
				chart.SetOutputFormat "PNG"			
			end if
			' Get SWF output
			chart_res = chart.GetHTMLTag
			' Clean up
			Set chart= Nothing
			response.write(chart_res)
			'********************************************************
															
			response.Write("</td>")
			
			'SELECTION fields for reloading chart
			'TEMPORARILTY TUNRED OFF ---  I DON'T KNOW THAT I WANT THIS ANYMORE...
			if lCase(fromPage) <> "print" and 1=2 then
				response.write("<td align=""left"" nowrap=""nowrap"" valign=""top"" border=""0"" cellspacing=""0"" cellpadding=""0"" style=""padding-top:5px; padding-left:10px;"">")			
			
 					response.write("<div class=""subLabel"" style=""text-transform:uppercase; margin-bottom:5px;""><strong>Selection</strong></div>")                    
					response.write("<div class=""subLabel"" style=""width:200px; margin-bottom:6px;"">Values to include in this issue list:</div>")
 
					response.write("<div id=""field_list_container"">")
					response.write("	<div id=""field_list"" style=""width:200px; border: 1px solid #ccc; padding: 4px; height:132px; overflow:auto; overflow-x: hidden; overflow-y: scroll;"">")

					dim arrLabels, arrValues, labelX

					arrValues = split(strValue,";")
					arrLabels = split(strChartCategories,";")

					for labelX = 0 to UBound(arrLabels)
						response.write("	<div id=""field_list_item_Name"" class=""checkItem"">")
						response.write("		<input name=""list_checkBox"" id=""field_list_checkBox"" type=""checkbox"" value=""" & arrValues(labelX) & """ checked/>")
						response.write("		<label for=""field_list_checkBox"">" & arrLabels(labelX) & "</label>")
						response.write("	</div>")
					next		

					response.write("	</div>")
					response.write("</div>")
 
                	response.write("<div style=""margin-top:5px;"">Select: <a href=""#top"" onClick=""javascript:fieldCheckList('frmList','field_list_checkBox','checked');"">All</a> | <a href=""#top"" onClick=""javascript:fieldCheckList('frmList','list_checkBox','');"">None</a></div>")			

					'buttons Add Follow-Up and Sync Status
					response.write("<div style=""margin-top:10px;"">")
					response.write("  <a class=""myCMbutton"" href=""#top"" onClick=""this.blur(); document.frmList.submit();""><span class=""sync"" style=""padding-right:7px;"">Reload</span></a>")
					response.write("</div>")
			
					response.write("<input type=hidden name=""list_field"" value=""[" & list_field & "]"" >")
					response.write("<input type=hidden name=""list_field_type"" value=""" & list_field_type & """ >")

				response.write("</td>")
			end if
			
			response.write("</form></tr>")			
			Response.Write "</table>"
			'<-- STOP table to hold chart -->
			
		end if
	
	end if

end sub
%>
