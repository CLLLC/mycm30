        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Settings div -->
        <div id="tab:settings" style="display:none;">        
        
            <!-- START Settings table -->   
	        <table id="table_settings" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>
                    
                    <td valign="top">
                                        
					  	<!-- START Settings Form Fields -->	                    
						<table id="table:settings:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">                           

      						<tr>
                              <td class="formLabel">Link Directive:</td>
                              <td align=left nowrap>
                              	<div>
                                    <select name="locDirectiveID" id="locDirectiveID" size=1 style="width:300px;">
                                    	<option value="" selected="selected">-- Select --</option>
                                      	<%
										mySQL="SELECT DirectiveID, Name " _
											& "		FROM Directive " _
											& "	  	WHERE CustomerID = '" & customerID & "' " _
											& "ORDER By SortOrder, Name "
										set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                      	do while not rs.eof								
                                          	Response.Write "<option value=""" & rs("directiveid") & """ " & checkMatch(locDirectiveID,rs("directiveid")) & ">" & rs("name") & "</option>"
                                          	rs.movenext
                                      	loop
                                      	call closeRS(rs)
                                      	%>
                                    </select>	                                                                                          
                                </div>
                                <div class="subLabel" style="padding-top:5px;">Used for setting directive to be shown during intake process.</div>
                              </td>
                            </tr>
                                          
						</table>
   						<!-- END Settings Form Fields -->

                    </td>
                                        
                </tr>
            
            </table>
            <!-- END Settings table -->
	
    	</div>
		<!-- STOP Settings div -->        


