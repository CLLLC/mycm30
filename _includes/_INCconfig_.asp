<%
'*************************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'*************************************************************************

'*************************************************************************
'Set session default LCID to 1033 - US English
'*************************************************************************
session.LCID = 1033

'*************************************************************************
'Declare local configuration variables
'*************************************************************************
dim pSiteLCID
dim systemNotice, systemNoticeExpire

'*************************************************************************
'Get additional configuration settings from "config.asp"
'*************************************************************************
%>
<!--#include file="../_config/config.asp"-->
<%

'*************************************************************************
'Retrieve configuration settings from DB and load into local variables
'*************************************************************************
function loadConfig()

	dim mySQL, rsTemp

	on error resume next
	
	if isNull(pSiteLCID) or isEmpty(pSiteLCID) then
		pSiteLCID = "1033"
	end if	

	'Get configuration control record from database
	mySQL = "SELECT configVar, configValLong, configVal FROM Config "
	'set rsTemp = openRSexecute(mySQL)
	set rsTemp = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	do while not rsTemp.EOF				
		select case rsTemp("configVar")
			case "systemNotice"
				systemNotice = rsTemp("configValLong")
			case "systemNoticeExpire"
				systemNoticeExpire = rsTemp("configVal")
		end select
		rsTemp.MoveNext
	loop
	
	closeRS(rsTemp)
	
	if err.number = 0 then
		loadConfig = true
	else
		loadConfig = false
	end if
	
	on error goto 0	

end function

'*************************************************************************
'Retrieve configuration settings from DB for MCR !!! not used right now!!
'*************************************************************************
function mcrSiteActive()

dim	mcrStatus

	mySQL = "SELECT configVar, configVal " _
		  & "FROM   config " _
		  & "WHERE  configVar = 'MCRactive'"
	'set rs = openRSexecute(mySQL)
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)	
	mcrStatus = rs("configVal")
	call closeRS(rs)

	if mcrStatus = "Y" then
		mcrSiteActive = true
	else
		mcrSiteActive = false
	end if
	
end function
%>

