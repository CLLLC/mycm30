		<!-- THIS FILE IS SHARED BETWEEN ++ issues_edit.asp AND issues_edit_00.asp ++ -->

        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]

        __ AUTOSAVE Note: Any hidden form element that is changed/modified must be set to style="display:none;"
        __ and NOT be a hidden element otherwise FireFox will not recognize the element has changed and 
        __ WILL NOT be saved during the autosave process.
        -->

		<!-- START Details div -->
        <div id="tab:details">
        
        
            <!-- START Issue Details table -->   
	        <table id="table_details" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>

	                <!-- START Side Page Tabs [genearl] "id=table:details:general" --> 
                    <!-- [this = object makeing the call ] -->
                    <!-- [details = current top tab ] -->
                    <!-- [general = selected side tab ] -->                    
                    <td rowspan="4" valign="top" style="width:100px; border-top:none; padding:0px; ">
                        <div class="tabSide" style="width:100px;">
                            <div id="general" class="on" onClick="clickSideTab('details','general'); resetSystemMsg('systemMessage');">General</div>			
                            <div id="reporter" class="off" onClick="clickSideTab('details','reporter'); resetSystemMsg('systemMessage');">Reporter</div>
                            
							<% 
							if rptQuestionCount <= 0 then 
								'do nothing...
							else
							%>                         
    	                        <div id="questions" class="off" onClick="clickSideTab('details','questions'); resetSystemMsg('systemMessage');">Questions</div>
	                        <% end if %>
                            
                            
                            <%
							' turn off for Lite Users [50]
							if cLng(session(session("siteID") & "adminLoggedOn")) <= 40 then 
							%>
                            	<div id="subjects" class="off" onClick="clickSideTab('details','subjects'); resetSystemMsg('systemMessage');">Subjects</div>   
                            <% end if %>                        

							<% if lcase(sCustomerID)="demo" or lcase(sCustomerID)="cha" or lcase(sCustomerID)="bsw" or lcase(sCustomerID)="bswh" then %>
								<% if Caller_Confidential="N" then %>
									<div id="summary" class="off" onClick="clickSideTab('details','summary'); resetSystemMsg('systemMessage');">Summary</div>
                                <% else 
									if cLng(session(session("siteID") & "adminLoggedOn")) <= 30 then
									%>
										<div id="summary" class="off" onClick="clickSideTab('details','summary'); resetSystemMsg('systemMessage');">Confidential</div>
                                    <% else %>
										<div id="summary" class="off" style="display:none;" onClick="clickSideTab('details','summary'); resetSystemMsg('systemMessage');">Confidential</div>
									<% end if %>                                    
								<% end if %>                                
							<% else %>
								<div id="summary" class="off" onClick="clickSideTab('details','summary'); resetSystemMsg('systemMessage');">Summary</div>
							<% end if %>

                            
                            <% if (lcase(sCustomerID)="demo" or lcase(sCustomerID)="cha" or lcase(sCustomerID)="bsw" or lcase(sCustomerID)="bswh") and Caller_Confidential="Y" then %>
	                            <div id="confidential" class="off" onClick="clickSideTab('details','confidential'); resetSystemMsg('systemMessage');">Summary</div>                            
                            <% else %>
   	                            <div id="confidential" class="off" style="display:none;" onClick="clickSideTab('details','confidential'); resetSystemMsg('systemMessage');">Summary</div>
                            <% end if %>
                            
                            
                            <div class="empty">&nbsp;</div>		
                        </div>                        
                    </td>
                    <!-- END Side Page Tabs -->

                    
                    <!-- START Middle Section -->    
                    <td valign="top">
                                        
					  	<!-- START General Form Fields -->	                    
						<table id="table:details:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">

      						<tr>
                                <td class="formLabel"><span class="required">*</span>Date:</td>
                                <td align=left>
                                
                                    <div style="float:left;">
                                    	<div class="subLabel">Date</div>
                                    	<input name="rptDate" id="required:date:rptDate" class="inputShort" value="<% =rptDate %>" maxlength="20" />
                                    </div>
                                  	<div style="float:left;">
                                    	<div class="subLabel">Time (ET [-05])</div>
                                    	<input name="rptTime" id="rptTime" class="inputShort" value="<% if len(rptTime)>0 then response.write(FormatDateTime(rptTime,3)) %>" maxlength="20" />
										<img style="cursor:pointer;" onclick="document.getElementById('required:date:rptDate').onclick();" src="../_images/icons/16/calendar.png" align="absmiddle" />
                                	</div>
                                    
                                </td>
                            </tr>

							<tr>
                                <td class="formLabel"><span class="required">*</span>Severity:</td>
                                <td align=left>
                                    <select name="severityLevel" id="required:severityLevel" size=1 class="inputMediumLong">
                                        <option value="3" <%=checkMatch(severityLevel,"3")%>>3-Low</option>
                                        <option value="2" <%=checkMatch(severityLevel,"2")%>>2-Medium</option>
                                        <option value="1" <%=checkMatch(severityLevel,"1")%>>1-High</option>
                                    </select>
                                    <img title="Assign Severity Level" src="../_images/icons/12/refresh.png" onclick="setSeverity('<% =idIssue %>'); return false;" style="cursor:pointer; padding-right:5px;" />
                                </td>
                            </tr>
                                            
                            <tr>
                              <td class="formLabel">Call Back:</td>
                              <td align=left>
								<div>
	                              	<input name="callBack" id="date:callBack" type=text class="inputShort" value="<%=callBack%>" size=10 maxlength=10>                                    
                                    <!-- Allow CCI staff to remove Callback dates -->
                                    <% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>
	                                    <img title="Assign Call Back" src="../_images/icons/12/refresh.png" onclick="setCallback('<% =idIssue %>'); return false;" style="cursor:pointer; padding-right:5px;" />
	                                    <img title="Remove Call Back" src="../_images/icons/12/delete_2.png" onclick="document.getElementById('date:callBack').value='';" style="cursor:pointer;" />
                                    <% end if %>	
                                       	                            
                                    <% if action = "addfu" and cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>
	                                    <div class="subLabel" style="padding-top:5px;"><% =scheduledCallBack %></div>
									<% else %>
	                                    <div class="subLabel" style="padding-top:5px;">Date reporter will check-in to receive resolution or instruction.</div>
                                    <% end if %>                                    
                                </div>       
                               </td>
                            </tr>                            




										<% 
										if editSource<>"N" then %>                                                        
                                        <tr>
                                          <td class="formLabel">
                                                <!-- DBAdmins and Program Admins -->
                                                <% if programAdmin = "Y" or cLng(session(session("siteID") & "adminLoggedOn")) < 2 then %>
                                                    <a href="#" class="formLabelLinkImg" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=customers&field=pReportSource&form=rptSource', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_Source','over');" onmouseout="javascript:configImage('congif_Source','out');"><img id="congif_Source" src="../_images/x_cleardot_16.gif" align="absmiddle" border="0" style="vertical-align:middle;"/></a>
                                                    <a href="#" class="formLabelLink" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=customers&field=pReportSource&form=rptSource', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_Source','over');" onmouseout="javascript:configImage('congif_Source','out');">Source:</a>
                                                <% else %>
                                                    Source:
                                                <% end if %>                                                
                                          </td>
                                          <td align="left">
												<%
                                                'Build array for dropdown selections
                                                optArr = getDropDownOpt("pReportSource",customerID)												
                                                if (cLng(session(session("siteID") & "adminLoggedOn"))>=10) and (editSource<>"Y") and (programAdmin <> "Y") and (rptLogID <> sLogid) then 
                                                    call buildSelect(optArr,rptSource,"rptSource","inputMediumLong","N")
                                                else
                                                    call buildSelect(optArr,rptSource,"rptSource","inputMediumLong","Y")											
                                                end if
                                                %>
                                            	<div class="subLabel" style="padding-top:5px;">Where issue was originally reported.</div>
                                        	</td>
                                        </tr>                            
                                        <% else %>
                                            <!-- These elements MUST be set to style="display:none;" and NOT type="hidden" -->
                                            <!-- when set as "hidden" FireFox does not recognize any change has been made  -->                                                
                                            <input name="rptSource" id="rptSource" style="display:none;" value="<% =rptSource %>" >                                        
                                        <% end if %>

                            
						    <tr>
                                <td class="formLabel">
                                	<!-- DBAdmins, CCI Admins and Program Admins -->
                                	<% if programAdmin = "Y" or cLng(session(session("siteID") & "adminLoggedOn")) < 3 then %>
	                                    <a href="#" class="formLabelLinkImg" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=customers&field=pCommTool&form=commTool', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_Awareness','over');" onmouseout="javascript:configImage('congif_Awareness','out');"><img id="congif_Awareness" src="../_images/x_cleardot_16.gif" align="absmiddle" border="0" style="vertical-align:middle;" /></a>
	                                    <a href="#" class="formLabelLink" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=customers&field=pCommTool&form=commTool', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_Awareness','over');" onmouseout="javascript:configImage('congif_Awareness','out');">Awareness:</a>
									<% else %>
										Awareness:                                    
									<% end if %>                                    
                                </td>
                                <td align=left>
                                    <%
                                    'Build array for dropdown selections
                                    optArr = getDropDownOpt("pCommTool",customerID)
									call buildSelect(optArr,commTool,"commTool","inputMediumLong","Y")
                                    %>                                    
	                                <div class="subLabel" style="padding-top:5px;">How reporter discovered toll-free number or reporting process.</div>
                                </td>
                            </tr>
                        
                            <tr>
                              <td class="formLabel">
                              	<div id="imgLocation_Visible" style="height:22px;">
                                	<a href="#" onclick="locationVisible(''); return false;" title="Show Details" class="formLabelLink"><img src="../_images/icons/16/arrow2_right.png" width="16" height="16" border="0" align="absmiddle" style="vertical-align:middle;"/>Issue Location:</a>
                                </div>
                              	<div id="imgLocation_Hide" style="display:none;">
                                	<a href="#" onclick="locationVisible('none'); return false;" title="Hide Details" class="formLabelLink"><img src="../_images/icons/16/arrow2_down.png" width="16" height="16" border="0" align="absmiddle" style="vertical-align:middle;"/>Issue Location:</a>
                                </div>                              	
                              </td>
                              <td align=left nowrap style="padding-bottom:0px;">                              
                                <div>
                                   	<div style="float:left;">
                                    	<input name="locNameID" id="locNameID" value="<%=locNameID%>" type="hidden" />
                                    	<input name="locName" id="locName" class="inputLong" value="<%=locName%>" maxlength="150" />
                                    </div>
                                   	<div><a class="myCMbutton" href="#" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_location.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 525, 700, 'no'); resetSystemMsg('systemMessage'); StopTheTimer(); return false;"><span class="search" style="padding-right:10px;">Look up</span></a></div>
                                </div>							  
                              </td>
                            </tr>
                            
                            <tr id="tr_Location" style="display:none;">                            
                              <td style="border-top:none;">&nbsp;</td>
                              <td align=left nowrap style="border-top:none; padding-top:0px;">                               
                                <div class="subLabel">Street Address:</div>
                                <div>
                                    <textarea name="locAddr" id="locAddr" style="width:300px; height:50px;"><%=locAddr%></textarea>
                                </div>                               
                                <div style="float:left;">
	                                <div class="subLabel">City:</div>
                                    <input name="locCity" id="locCity" class="inputShort" value="<%=locCity%>" maxlength="50" />
                                </div>                                
                                <div style="float:left;">
    	                            <div class="subLabel">State/Province:</div>
                                    <input name="locState" id="locState" class="inputShort" value="<%=locState%>" maxlength="50" />
                                </div>                                
                                <div>                                
        	                        <div class="subLabel">Postal Code:</div>
                                  <input name="locZip" id="locZip" class="inputShort" value="<%=locZip%>" maxlength="50" />
                                </div>
                                <div>                                
        	                        <div class="subLabel">Country:</div>
                                    <input name="locCountry" id="locCountry" class="inputLong" value="<%=locCountry%>" maxlength="150" />
                                </div>				                                                            
                                <div>                                
        	                        <div class="subLabel">Hierarchy:</div>
                                    <div style="float:left; margin-top:3px;"><img src="../_images/icons/16/tree_level.gif" width="16" height="16" align="absmiddle" style="vertical-align:middle;"/>&nbsp;</div>
                                    <div id="divHierarchy" style="margin-top:3px;"><% if len(locHierarchy)<=0 then  response.write("<em>none assigned</em>") else response.write( reformatHierarchy(locHierarchy,"html") ) %></div>
                                </div>	                                                                                                                      
                                <input name="locHierarchy" id="locHierarchy" type="hidden" value="<% =locHierarchy %>">
                                			                                                            
                              </td>                              
                            </tr>

                            <tr>
                              <td class="formLabel">Categories:</td>    
                              
                              <!-- START Category Table -->                         
                              <td align=left> 
                               	<img src="../_images/icons/16/help.png" width="16" height="16" align="absmiddle" style="margin-right:5px; margin-bottom:2px; vertical-align:middle;"/><span style="color:#2d73b2;">Double-click on a category to view details.</span>
                                <br>                                                              
								<select name="listCategory" id="listCategory" ondblclick="categoryLookUp('listCategory'); StopTheTimer();" size="5" style="width:85%; height:100px; margin-bottom:5px;" multiple="multiple">
									<!--  Find Categories ALREADY assigned -->
                                  	<%
									dim tempFUPCategory
									dim tempCat, priCategory
											
									mySQL = "SELECT a.CRSID, b.CategoryID, b.Name, b.Sub, a.[Primary], b.DirectiveID, b.Active, Max(c.Status) AS [Status], " _
										  & " 		b.Sev1, b.Sev2, b.Sev3 " _
										  & "FROM (CRS_Category AS a INNER JOIN Category AS b ON a.CategoryID = b.CategoryID) LEFT JOIN Investigation AS c ON (a.CRSID = c.CRSID) AND (a.CategoryID = c.CategoryID) " _
										  & "GROUP BY a.CRSID, b.CategoryID, b.Name, b.Sub, a.[Primary], b.DirectiveID, b.Active, b.Sev1, b.Sev2, b.Sev3 "							
									'pull original categories
									if action = "addfu" and appAssignFUPCategory = "Y" then
										mySQL = mySQL & "HAVING a.CRSID = '" & Mid(idIssue,1,Len(idIssue)-2) & "01" & "' "
									'pull current categories
									else
										mySQL = mySQL & "HAVING a.CRSID = '" & idIssue & "' "
									end if
									'finish sql
									mySQL = mySQL & "ORDER BY b.Name, b.Sub "								
										  
									'open categories
									set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
									
									'loop through all categories found									
                    	            do while not rs.eof
										if len(rs("Sub")) > 0 then
											tempCat = rs("Name") & " [" & rs("Sub") & "]"
										else
											tempCat = rs("Name")
										end if
										'CUSTOMERs ONLY, check for investigations added
										if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then
											if len(rs("status")) > 0 then
												tempCat = tempCat & "&nbsp;&nbsp;(" & rs("status") & ")"
											else
												tempCat = tempCat & "&nbsp;&nbsp;(Not Started)"
											end if
										
										'MAY NOT NEED THIS...
										'CCI users ONLY, show call-back time frame
										'else
										'	tempCat = tempCat & "&nbsp;[S1:" & rs("sev1") & ";&nbsp;S2:" & rs("sev2") & ";&nbsp;S3:" & rs("sev3") & "]&nbsp;"
										end if			
																	
										if rs("primary")="Y" then
											priCategory = rs("CategoryID")
											tempCat = tempCat & " (primary)"
										end if
										if rs("active")="N" then
											tempCat = "Missing:" & tempCat
										end if
										'CCI staff only
										if cLng(session(session("siteID") & "adminLoggedOn")) < 10 and rs("DirectiveID") > 0 then
											tempCat = tempCat & " (*directive)"
										end if
										
										'add category to options list
										response.write "<option value=""" & rs("CategoryID") & """>" & tempCat & "</option>"

										'for adding category to follow-ups
										tempFUPCategory = tempFUPCategory & "," & rs("CategoryID")
										
                                        rs.movenext
                                    loop
                                    call closeRS(rs)									  
                        	        %>
								</select>
                                <!-- These elements MUST be set to style="display:none;" and NOT type="hidden" -->
                                <!-- when set as "hidden" FireFox does not recognize any change has been made  -->
                               	<% if action="addfu" then %>
	                           		<input name="listCategoryAdded" id="listCategoryAdded" style="display:none;" value="<% =tempFUPCategory %>" />
                                <% else %>
	                           		<input name="listCategoryAdded" id="listCategoryAdded" style="display:none;" value="" />
                              	<% end if %>                                            
                                
                                <input name="listCategoryRemoved" id="listCategoryRemoved" style="display:none;" value="" />
                                <input name="listCategoryInvAdd" id="listCategoryInvAdd" style="display:none;" value="" />
                                <input name="primaryOriginal" id="primaryOriginal" style="display:none;" value="<% =priCategory %>" />
                                <input name="primaryCategory" id="primaryCategory" style="display:none;" value="<% =priCategory %>" />
                                <br>     

                            	<div style="float:left; width:175px;">
									<a class="myCMbutton" href="#" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_category.asp?recid=<% =idIssue %>&cid=<% =customerID %>&tid=<% =issueType %>&pageview=<% =pageView %>', 410, 450, 'no'); resetSystemMsg('systemMessage'); StopTheTimer(); return false;"><span class="add" style="padding-right:10px;">Add</span></a>
									<a class="myCMbutton" href="#" onclick="this.blur(); javascript:removeCategory('listCategory'); resetSystemMsg('systemMessage'); return false;"><span class="delete" style="padding-right:7px;">Delete</span></a> 
                                </div>
                               	<div>
                                	<%
									if uCase(viewCaseNotes)="N" or cLng(session(session("siteID") & "adminLoggedOn")) < 10 or uCase(viewInvestigations) = "N" then
                                    	'do nothing
                                    else
                                    %>
										<a class="myCMbutton" href="#" onclick="this.blur(); javascript:addInvestigations('listCategory','<% =idIssue %>'); resetSystemMsg('systemMessage'); return false;"><span class="add" style="padding-right:5px;">Investigations</span></a>
                                    <% end if %>
									<a class="myCMbutton" href="#" onclick="this.blur(); javascript:setPrimary(); resetSystemMsg('systemMessage'); return false;"><span class="flag" style="padding-right:5px;">Primary</span></a>
                                </div>

                              </td>
                              <!-- STOP Category Table -->
                              
                            </tr>                            
                        
						</table>
   						<!-- END General Form Fields -->
                                                
                        
   						<!-- START Reporter Form Fields -->                        
						<table id="table:details:reporter" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">                

							<% if lcase(sCustomerID)="demo" or lcase(sCustomerID)="cha" or lcase(sCustomerID)="bsw" or lcase(sCustomerID)="bswh" then %>
                            
                              <% if cLng(session(session("siteID") & "adminLoggedOn")) <= 30 or Caller_Confidential="Y" then %>                            
                                <tr>
                                    <td class="formLabel" style="background-color:#FFFFC8;">Confidential:</td>
                                    <td align=left style="background-color:#FFFFC8;">                                    	
                                      	<% if cLng(session(session("siteID") & "adminLoggedOn")) <= 30 then %>
                                        	<div>
                                                <label>
                                                    <input type="radio" name="Caller_Confidential" id="Caller_Confidential" value="Y" onclick="confidentialVisible('');" style="vertical-align:middle;" <% =checkRadio(Caller_Confidential,"Y") %>>Yes&nbsp;&nbsp;
                                                </label>
                                                <label>
                                                  <input type="radio" name="Caller_Confidential" id="Caller_Confidential" value="N" onclick="confidentialVisible('none');" style="vertical-align:middle;" <% =checkRadio(Caller_Confidential,"N") %>>No
                                                </label>    
	                                        </div>
    	                                    <div class="subLabel" style="padding-top:5px;">Hides Reporter identity from all but Administrators [Level 30].</div>                                                                                        
										<% else 
											if Caller_Confidential="Y" then 
												response.Write("<b>Yes</b> - Reporter identity only visible to Administrators.")
											else 
												response.Write("<b>No</b> - Reporter identity visible to all users.")
											end if
											response.Write("<input name=""Caller_Confidential"" id=""Caller_Confidential"" type=""hidden"" value=""" & Caller_Confidential & """ />")
                                        end if 
										%>
                                    </td>                                
                                </tr>                                
                              <% else %>
                                <input name="Caller_Confidential" id="Caller_Confidential" type="hidden" value="<% =Caller_Confidential %>" />
                              <% end if %>    
                                                              
							<% end if %>
                            

							<tr>
							  <td class="formLabel"><span class="required">*</span>Identity:</td>
							  <td colspan="3" align=left nowrap>
	               				  <%
								  if cLng(session(session("siteID") & "adminLoggedOn")) > 30 and Caller_Confidential = "Y" then                                  
									if anonCaller="True" then 
										anonCaller = "-1"
										response.Write("Anonymous")
									else 
										anonCaller = "0"
										response.Write("Identified")
									end if
									response.Write("<input name=""anonCaller"" id=""anonCaller"" type=""hidden"" value=""" & anonCaller & """ />")								  
                                  else 
								  %>
                                    <label>
                                      <input type="radio" name="anonCaller" id="required:anonCaller" onclick="callerVisible('none');" value="-1" style="vertical-align:middle;" <%=checkRadio(anonCaller,"True")%>><span class="subLabel">Anonymous&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </label>
                                    
                                    <label>
                                      <input type="radio" name="anonCaller" id="required:anonCaller" onclick="callerVisible('');" value="0" style="vertical-align:middle;" <%=checkRadio(anonCaller,"False")%>><span class="subLabel">Identified</span>
                                    </label>
								<% end if %>           							                      
                             </td>
						  	</tr>

                            <tr>
                              <td class="formLabel">Interpreter Used:</td>
                              <td colspan="3" align=left nowrap>                              
                   				<label>
							      <input type="radio" name="interpreterused" id="interpreterusedYes" onclick="changeObjectDisplay('tr_language',''); changeObjectDisplay('tr_interpreter','');" value="-1" style="vertical-align:middle;" <%=checkRadio(interpreterused,"True")%>>Yes&nbsp;&nbsp;&nbsp;&nbsp;
                                </label>
							    
							    <label>
							      <input type="radio" name="interpreterused" id="interpreterusedNo" onclick="changeObjectDisplay('tr_language','none'); changeObjectDisplay('tr_interpreter','none');" value="0" style="vertical-align:middle;" <%=checkRadio(interpreterused,"False")%>>No
                                </label>
                                <div class="subLabel" style="padding-top:5px;">Signifies if an interpreter used during the intake of this issue.</div>                                                                                                
                              </td>
                            </tr>

							<tr id="tr_language" <% if interpreterused = "False" then response.write("style=""display:none;""") %>>
                                <td class="formLabel">                                
	                                <!-- DB Admins and CCI Admins ONLY! -->
                                	<% if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then %>
	                                    <a href="#" class="formLabelLinkImg" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=system&field=pLanguages&form=language', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_Language','over');" onmouseout="javascript:configImage('congif_Language','out');"><img id="congif_Language" src="../_images/x_cleardot_16.gif" align="absmiddle" border="0" style="vertical-align:middle;"/></a>
	                                    <a href="#" class="formLabelLink" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=system&field=pLanguages&form=language', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_Language','over');" onmouseout="javascript:configImage('congif_Language','out');">Language:</a>
									<% else %>
										Language:
									<% end if %>
                                </td>
                                <td colspan="3" align=left nowrap>
                                    <%
                                    'Build array for dropdown selections
                                    optArr = getDropDownConfig("pLanguages")
									call buildSelect(optArr,language,"language","inputMediumLong","Y")
                                   	%>
	                                <div class="subLabel" style="padding-top:5px;">Language used by reporter.</div>                                      
							  </td>
                            </tr>

							<tr id="tr_interpreter" <% if interpreterused = "False" then response.write("style=""display:none;""") %>>
                                <td class="formLabel">Interpreter #:</td>
                                <td colspan="3" align=left nowrap>
	                                <input name="interpreterNumber" id="interpreterNumber" class="inputMedium" type=text value="<% =interpreterNumber %>" size=50 maxlength=50>
	                                <div class="subLabel" style="padding-top:5px;">Language used by reporter.</div>                                      
							  </td>
                            </tr>

                            <tr id="tr_callerName" <% if anonCaller = "True" then response.write("style=""display:none;""") %>>
                              <td class="formLabel">Name:</td>
                              <td align=left nowrap>                              
                              	<div style="float:left;">
                              		<div class="subLabel">First Name:</div>
                                    <div><input name="callerFirst" id="callerFirst" class="inputMedium" value="<%=callerFirst%>" maxlength="20" /></div>
                              	</div>
                                
                              	<div style="float:left;">
                              		<div class="subLabel">Last Name:</div>
                                    <div>
                                    	<div style="float:left;">
                                    		<input name="callerLast" id="callerLast" class="inputMedium" value="<%=callerLast%>" maxlength="20" />
                                        </div>
	                                    <div style="float:left;">
                                        	<!-- THIS WAS A "LOOK UP" BUTTON FOR CALLER...WASN'T DONE SO I TOOK IT OUT -->&nbsp;
                                    	</div>
                                    </div>
                              	</div>
                              </td>
                            </tr>
							
							<tr>
                                <td class="formLabel">
	                                <!-- DBAdmins, CCI Admins and Program Admins -->
                                	<% if programAdmin = "Y" or cLng(session(session("siteID") & "adminLoggedOn")) < 3 then %>
	                                    <a href="#" class="formLabelLinkImg" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=customers&field=pCallerType&form=callerType', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_Caller','over');" onmouseout="javascript:configImage('congif_Caller','out');"><img id="congif_Caller" src="../_images/x_cleardot_16.gif" align="absmiddle" border="0" style="vertical-align:middle;"/></a>
	                                    <a href="#" class="formLabelLink" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=customers&field=pCallerType&form=callerType', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_Caller','over');" onmouseout="javascript:configImage('congif_Caller','out');">Relationship:</a>
									<% else %>
										Relationship:
									<% end if %>
                                </td>
                                <td colspan="3" align=left nowrap>
                                    <%
                                    'Build array for dropdown selections
                                    optArr = getDropDownOpt("pCallerType",customerID)
									call buildSelect(optArr,callerType,"callerType","inputMediumLong","Y")
                                   	%>
	                                <div class="subLabel" style="padding-top:5px;">Reporter's relationship to the organization.</div>                                      
							  </td>
                            </tr>
                            
                            <tr id="tr_callerTitle" <% if anonCaller = "True" then response.write("style=""display:none;""") %>>
                                <td class="formLabel">Job Title:</td>
                                <td colspan="3" align=left nowrap><input name="callerTitle" id="callerTitle" class="inputLong" type=text value="<%=callerTitle%>" maxlength=50></td>
                            </tr>
                            
                            <tr id="tr_callerAddress" <% if anonCaller = "True" then response.write("style=""display:none;""") %>>
                              <td class="formLabel">Address:</td>
                              <td colspan="3" align=left nowrap>
                              
                                <div class="subLabel">Street Address:</div>
                                <div>
                                    <textarea name="callerAddr" id="callerAddr" style="width:300px; height:50px;"><%=callerAddr%></textarea>
                                </div>
                                
                                <div style="float:left;">
	                                <div class="subLabel">City:</div>
                                    <input name="callerCity" id="callerCity" class="inputShort" value="<%=callerCity%>" maxlength="255" />
                                </div>
                                
                                <div style="float:left;">
    	                            <div class="subLabel">State/Province:</div>
                                    <input name="callerState" id="callerState" class="inputShort" value="<%=callerState%>" maxlength="255" />
                                </div>
                                
                                <div>                                
        	                        <div class="subLabel">Postal Code:</div>
                                    <input name="callerZip" id="callerZip" class="inputShort" value="<%=callerZip%>" maxlength="255" />
                                </div>

                              </td>
                            </tr>
                                                        
                            <tr id="tr_callerPhone" <% if anonCaller = "True" then response.write("style=""display:none;""") %>>
                              <td class="formLabel">Phone Number(s):</td>
                              <td colspan="3" align=left nowrap>
                                                              
                                <div style="float:left;">
                                <div class="subLabel">Home Phone:</div>
                                    <input name="callerHome" id="callerHome" class="inputShort" value="<%=callerHome%>" maxlength="255" />
                                </div>
                                
                                <div style="float:left;">
                                <div class="subLabel">Work Phone:</div>
                                    <input name="callerWork" id="callerWork" class="inputShort" value="<%=callerWork%>" maxlength="255" />
                                </div>                                                                
                                                                
                                    <div class="subLabel">
                                        <div>Mobile Phone:</div>
                                        <input name="callerCell" id="callerCell" class="inputShort" value="<%=callerCell%>" maxlength="255" <% if appDeliverySystem="CLive" then response.write("disabled=""disabled""") %> />
                                    </div>
                                    
                                    <div style="float:left;">
                                    <div class="subLabel">Fax:</div>
                                        <input name="callerFax" id="callerFax" class="inputShort" value="<%=callerFax%>" maxlength="255" <% if appDeliverySystem="CLive" then response.write("disabled=""disabled""") %> />
                                    </div>
                                    
                                    <div style="float:left;">
                                    <div class="subLabel">Pager:</div>
                                        <input name="callerPager" id="callerPager" class="inputShort" value="<%=callerPager%>" maxlength="255" <% if appDeliverySystem="CLive" then response.write("disabled=""disabled""") %> />
                                    </div>
                                                                                                    
                                    <div>
                                        <div class="subLabel">Other Phone:</div>
                                        <input name="callerOther" id="callerOther" class="inputShort" value="<%=callerOther%>" maxlength="255" <% if appDeliverySystem="CLive" then response.write("disabled=""disabled""") %> />
                                    </div>
                              
                              </td>
                            </tr>
                            
                            <tr id="tr_callerEmail" <% if anonCaller = "True" then response.write(" style=""display:none;""") %>>
                                <td class="formLabel">E-Mail:</td>
                                <td colspan="3" align=left nowrap><input name="callerEmail" id="callerEmail" class="inputLong" type=text value="<%=callerEmail%>" size=50 maxlength=50></td>
                            </tr>
                            
                            <tr>
                              <td class="formLabel">First Time:</td>
                              <td colspan="3" align=left nowrap>                              
                   				<label>
							      <input type="radio" name="firstTime" id="firstTimeYes" value="-1" style="vertical-align:middle;" <%=checkRadio(firstTime,"True")%>>Yes&nbsp;&nbsp;&nbsp;&nbsp;
                                </label>
							    
							    <label>
							      <input type="radio" name="firstTime" id="firstTimeNo" value="0" style="vertical-align:middle;" <%=checkRadio(firstTime,"False")%>>No
                                </label>
                                <div class="subLabel" style="padding-top:5px;">Signifies if this is the first issue ever filed by reporter.</div>                                    
                                                            
                              </td>
                            </tr>                        
                        
                            <tr>
                              <td class="formLabel">Password:</td>
                                <td colspan="3" align=left nowrap>
									<%
                                    'CCI Staff with SanctionCheck
									if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then																  
									%>
	                                	<input name="callerPassword" id="callerPassword" class="inputMedium" type=text value="<% =callerPassword %>" size=50 maxlength=50>
                                    <% else %>
	                                	<input name="callerPassword" id="callerPassword" class="inputMedium" type=password value="<% =callerPassword %>" size=50 maxlength=50>                                    
                                    <% end if%>   
                                    <div class="subLabel" style="padding-top:5px;">Used for additional security and verification.</div>
                                </td>
                            </tr>
                        
                        </table>
   						<!-- END Reporter Form Fields -->



						<!-- START Questions section -->                        
   						<table id="table:details:questions" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">
                    
						<%
						dim arrQesID, qesNumber

						'----------------------------
						'show questions on edit issue
						'----------------------------
						if action = "edit" and rptQuestionCount > 0 then

							mySQL = "SELECT CRS_Question.CRSQuestionID, Question.QuestionID, CRS_Question.Question, CRS_Question.Answer " _
								  & "	FROM CRS_Question INNER JOIN Question ON CRS_Question.QuestionID = Question.QuestionID " _
								  & "	WHERE CRS_Question.CRSID='" & idIssue & "' " _
								  & "	ORDER BY Question.SortOrder "
							set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

                            'investigation count
                            qesNumber = 0

							'make sure there ARE questions to show...
							if not rs.eof then
								
								do until rs.eof		
									
									response.write("<input name=""rptQuesName" & qesNumber & """ id=""rptQuesName" & qesNumber & """ type=""hidden"" value=""" & rs("Question") & """ />")
									'build field with possible dropdown
									call dispQFields(rs("Question"), rs("QuestionID"), "rptQuesAnswer" & qesNumber, rs("Answer"), "text")
									
									'used for saving form elements
									qesNumber = qesNumber + 1
									arrQesID = arrQesID & rs("CRSQuestionID") & ","
									
									rs.movenext
								loop
								%>
                                							
								<!-- FORM ELEMENT SAVING... -->
								<input name="arrQuestion" id="arrQuestion" type="hidden" value="<% =arrQesID %>" />
							
								<%
							
							'no questions with issue ARE there any for customer
							else				
							
								'not fields have been setup
                           		response.write("<tr><td>")
								response.write("<table width=""95%"" cellpadding=""0"" cellspacing=""0""<td><td class=""clearFormat"" style=""padding:5px;"" >")
								call systemMessageBox("questionsID","statusMessageINFO","No questions were added for this issue.")
                           		response.write("</td></tr></table>")										
								response.write("</td></tr>")										
								
							end if		
						
						'----------------------------
						'add questions for new issue
						'----------------------------
						elseif (action = "add" or action = "addfu") and rptQuestionCount > 0 then

							mySQL = "SELECT QuestionID, Name, Value, Required " _
								  & "	FROM Question " _
								  & "	WHERE CustomerID='" & customerID & "' AND IssueTypeID = " & issueType & " AND Active='Y' " _
								  & "	ORDER BY SortOrder "
							set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

                            'investigation count
                            qesNumber = 0

							'make sure there ARE questions to show...
							if not rs.eof then
								
								do until rs.eof		
									
									response.write("<input name=""rptQuesName" & qesNumber & """ id=""rptQuesName" & qesNumber & """ type=""hidden"" value=""" & rs("Name") & """ />")
									'build field with possible dropdown
									call dispQFields(rs("Name"), rs("QuestionID"), "rptQuesAnswer" & qesNumber, "", "text")
									
									'used for saving form elements
									qesNumber = qesNumber + 1
									arrQesID = arrQesID & rs("QuestionID") & ","
									
									rs.movenext
								loop
								%>
                                							
								<!-- FORM ELEMENT SAVING... -->
								<input name="arrQuestion" id="arrQuestion" type="hidden" value="<% =arrQesID %>" />
							
								<%
							
							'no questions with issue ARE there any for customer
							else				
							
								'not fields have been setup
                           		response.write("<tr><td>")
								response.write("<table width=""95%"" cellpadding=""0"" cellspacing=""0""<td><td class=""clearFormat"" style=""padding:5px;"" >")
								call systemMessageBox("questionsID","statusMessageINFO","No questions were added for this issue.")
                           		response.write("</td></tr></table>")										
								response.write("</td></tr>")										
								
							end if									
							
						end if					
								
						call closeRS(rs)						
						%>
						<!-- STOP Building/loading Questions -->
                            
                      	</table>
						<!-- STOP Questions section -->       
                        
                        
						<!-- START Subjects section -->                        
                        <% if cLng(session(session("siteID") & "adminLoggedOn")) <= 40 then %>
   						<table id="table:details:subjects" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">                    
							<tr>            	
                                <td align="center" valign="top" style="padding:0px; margin:0px;">

                                    <table id="table:resolve:resolution2" width="95%" class="formTable" cellpadding="0" cellspacing="0">                    
                                        <tr>            	
                                            <td align="center" style="border-top:none;">

                                                <div style="color:#35487B; width:100%; font-weight:bold; text-align:left; padding-bottom:5px;">Subjects</div>
            
                                                <!-- EDITING ISSUE look for existing uploaded documentation -->                                    
                                                <%
                                                dim arrSubjectID
                                                dim subjectCount : subjectCount = 0
                                                
                                                if action = "edit" then
                                                
                                                    '-----------------------------------
                                                    'LOOK for existing subjects
                                                    mySQL = "SELECT SubjectID, CRSID, CustomerID, Name, Title, Type, SanctionCheck " _
                                                          & "		FROM Subject " _
                                                          & "		WHERE Subject.CRSID = '" & idIssue & "' " _
                                                          & "		ORDER BY Subject.Name "													  											  			
                                                    set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)      
                                                    %>                                                
                                                    
                                                    <!-- START Subject table -->
                                                    <table id="subjectTable" width="100%" style="background-color:#969696;" cellpadding=0 cellspacing=0>
                                                                    
                                                       <thead>
                                                            <tr>
                                                            	<th align="left" nowrap="nowrap">&nbsp;</th>
                                                                <th align="left" nowrap="nowrap">Name</th>
                                                                <th align="left" nowrap="nowrap">Title</th>
                                                                <th align="left" nowrap="nowrap">Type</th>
                                                                <% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>
	                                                                <th align="left" nowrap="nowrap">SanctionCheck</th>
                                                                <% end if %>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                            
                                                        <%
                                                        do until rs.eof
                                                                
                                                        'to hide messages if no records
                                                        subjectCount = subjectCount + 1
                                                                
                                                            'START current row
                                                            response.write("<tr id=""subject:" & rs("subjectid") & "_tr"">")

															'ICON actions
															response.Write("<td id=""subject:" & rs("subjectid") & "_delete"" align=""left"" style=""width:40px;"">")
  	                                                        response.Write("<a href=""#"" onClick=""delSubject(" & rs("subjectid") & "); resetSystemMsg('systemMessage'); return false;"" ><img src=""../_images/icons/16/subject_delete.png"" title=""Delete Subject"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle; padding-right:7px;"" border=""0"" /></a>")
                                                            response.Write("<a href=""#"" onclick=""SimpleModal.open('../_dialogs/popup_config_subject.asp?recid=" & rs("subjectid") & "&cid=" & customerID & "&action=edit', 450, 450, 'no'); StopTheTimer(); resetSystemMsg('systemMessage'); return false;""><img src=""../_images/icons/16/subject_edit.png"" title=""Edit Subject"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle;"" border=""0"" /></a>")
															response.Write("</td>")
                                                                    
                                                            'NAME link
															response.Write("<td id=""subject:" & rs("subjectid") & "_name"" align=""left"" style=""cursor:pointer;"" onclick=""SimpleModal.open('../_dialogs/popup_config_subject.asp?recid=" & rs("subjectid") & "&cid=" & customerID & "&action=edit', 450, 450, 'no'); StopTheTimer(); resetSystemMsg('systemMessage'); return false;"">")
                                                            response.write("<a style=""color:#333333;"">" & rs("name") & "</a>")
                                                            response.Write("</td>")
															
                                                            'ADDITION fields
                                                            response.Write("<td id=""subject:" & rs("subjectid") & "_title"" align=""left"">" & rs("title") & "</td>")
                                                            response.Write("<td id=""subject:" & rs("subjectid") & "_type"" align=""left"">" & rs("type") & "</td>")
                                                            if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
	                                                            response.Write("<td id=""subject:" & rs("subjectid") & "_sanctioncheck"" align=""left"">" & rs("sanctioncheck") & "</td>")
															end if
                                                                    
                                                            'CLOSE current row						
                                                            response.write("</tr>")
                                                                
                                                            rs.movenext
                                                        loop
                                                        %>					
                                            
                                                        </tbody>
                                                    </table>
                
                                                    <%
                                                    dim tableProperty
                                                    tableProperty = "sort: true, sort_config:{sort_types:['String','String','String','String','String']}, filters_row_index: 1, " _
                                                                  & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
                                                                  & "status_bar: true, " _
                                                                  & "paging: true, paging_length: 5, " _
                                                                  & "enable_non_empty_option: true, or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, " _
                                                                  & "highlight_keywords: true, " _
                                                                  & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
                                                                  & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"
													'CCI Staff with SanctionCheck
													if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then																  
														tableProperty = tableProperty & ",col_0: """",col_1: """", col_2: """", col_3: ""select"", col_4: ""select"""
														tableProperty = tableProperty & ",col_width:[null,""25%"",null,null,null]"
													'NO SanctionCheck
													else
														tableProperty = tableProperty & ",col_0: """",col_1: """", col_2: """", col_3: ""select"""
														tableProperty = tableProperty & ",col_width:[null,""25%"",null,null]"
													end if
                                                    %>
                                                    <!-- STOP Location table -->
                                                
                                                    <!-- Fire table build -->
                                                    <script language="javascript" type="text/javascript">
                                                        //<![CDATA[
                                                        var tableProp = {<% =tableProperty %>};
                                                        //initiate table setup
                                                        var tfSubject = setFilterGrid("subjectTable",tableProp);
                                                        //]]>
                                                    </script>
                                                
                                                    <!-- Shadow table -->
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                      <tr>
                                                        <td height="5px" class="clearFormat" style="background:#D0D0D0; padding:0px; margin:0px;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
                                                      </tr>
                                                    </table>
            
            
                                                    <!-- No Activities found  -->
                                                    <table id="NoSubjectTable" width="100%" cellpadding="0" cellspacing="0" style="margin-top:10px; <% if subjectCount>0 then response.write("display:none;") %>">
                                                        <tr>
                                                            <td align="left" class="clearFormat" style="padding-right:2px;">                                                                                                
                                                                <% call systemMessageBox("subjectsID","statusMessageINFO","No subject found. Click ""Add"" below.") %>
                                                            </td>
                                                        </tr>
                                                    </table>												                                                                                      
                                                    <!-- STOP Subjects table  -->
            
                                                    <!-- ADD button  -->
                                                    <div style="float:left; padding-top:5px; width:125px;">
                                                        <a class="myCMbutton" href="#" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_config_subject.asp?recid=<% =idIssue %>&cid=<% =customerID %>&action=add', 450, 500, 'no'); StopTheTimer(); resetSystemMsg('systemMessage'); return false;"><span class="add" style="padding-right:10px;">Add</span></a>
                                                    </div>                
                                                    
                                                    <% closeRS(rs) %>
                                                    
                                                <!-- ADDING new issue...subjects not available yet -->
                                                <% else %>
                                                    <br>
                                                    <table id="NoSubjectsTable" width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left" class="clearFormat" style="padding-right:2px;">                                                                                                
                                                            <% call systemMessageBox("subjectsID","statusMessageINFO","Subject will become available after saving and reloading issue.") %>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                <% end if %>

                                       			<!-- FORM ELEMENT SAVING... -->
                                                <!-- These elements MUST be set to style="display:none;" and NOT type="hidden" -->
                                                <!-- when set as "hidden" FireFox does not recognize any change has been made  -->
                                                <input name="arrSubjectRemoved" id="arrSubjectRemoved" style="display:none;" value="" />

                                            </td>    
                                        </tr>                            
                                    </table>

                                </td>    
							</tr>                            
                      	</table>
                        <% end if %>
						<!-- STOP Subjects section -->       
                        
                                        
						<!-- START Summary-Detail section -->        
   						<table id="table:details:summary" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">
                                                        
                            <tr>
                                <td align="center" style="clearFormat">
                                	<table width="95%">
                                    	<tr>
                                        	<td align="left" class="clearFormat">
                                				<div style="color:#35487B; font-weight:bold; padding:3px;">Summary</div>                                                
                                				<textarea name="rptSummary" id="rptSummary_<% =idIssue %>" style="width:100%; height:100px; margin-bottom:12px;" ><%=server.HTMLEncode(rptSummary & "")%></textarea>
                            				</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                	<table width="95%">
                                    	<tr>
                                        	<td align="left" class="clearFormat">
                                				<div style="color:#35487B; font-weight:bold; padding:3px;">Details</div>
                                				<textarea name="rptDetails" id="rptDetails_<% =idIssue %>" style="width:100%; height:250px; margin-bottom:12px;" ><%=server.HTMLEncode(rptDetails & "")%></textarea>
                                                <% if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then %>
                                                  <div class="subLabel" style="padding-top:5px; font-weight: bold; background-color:#FFFFC8; padding: 7px;">
                                                	Content: <input name="rptContentQAVal" id="rptContentQAVal" class="inputSmall" style="margin-right: 10px" type=text value="<% =rptContentQAVal %>" size=5 maxlength=5 onkeypress="return isNumberKey(event)"/> 
                                                    Grammar: <input name="rptGrammarQAVal" id="rptGrammarQAVal" class="inputSmall" style="margin-right: 10px" type=text value="<% =rptGrammarQAVal %>" size=5 maxlength=5 onkeypress="return isNumberKey(event)"/>
                                                    Form: <input name="rptFormQAVal" id="rptFormQAVal" class="inputSmall" style="margin-right: 10px" type=text value="<% =rptFormQAVal %>" size=5 maxlength=5 onkeypress="return isNumberKey(event)"/>
                                                  </div>
                                                <% else %>
													<input type=hidden name="rptContentQAVal" id="rptContentQAVal" value="<% =rptContentQAVal %>">   
                                                    <input type=hidden name="rptGrammarQAVal" id="rptGrammarQAVal" value="<% =rptGrammarQAVal %>">   
                                                    <input type=hidden name="rptFormQAVal" id="rptFormQAVal" value="<% =rptFormQAVal %>">   
                                                <% end if %>
                            				</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                	<table width="95%">
                                    	<tr>
                                        	<td align="left" class="clearFormat">
                                				<div style="color:#35487B; font-weight:bold; padding:3px;">Addendum</div>
                                				<textarea name="rptAddendum" id="rptAddendum_<% =idIssue %>" style="width:100%; height:100px; margin-bottom:12px;" ><%=server.HTMLEncode(rptAddendum & "")%></textarea>
                            				</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            
                      	</table>
						<!-- STOP Summary-Detail section -->       



						<!-- START Summary-Detail section -->        
   						<table id="table:details:confidential" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">
                                                        
                            <tr>
                                <td align="center">
                                	<table width="95%">
                                    	<tr>
                                        	<td align="left" class="clearFormat">
                                				<div style="color:#35487B; font-weight:bold; padding:3px;">Details (redacted)</div>
                                				<textarea name="Details_Redacted" id="Details_Redacted_<% =idIssue %>" style="width:100%; height:375px; margin-bottom:12px;" ><%=server.HTMLEncode(Details_Redacted & "")%></textarea>
                            				</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            
                      	</table>
						<!-- STOP Summary-Detail section -->       
                        
                        

                    </td>
                    <!-- START Page Details and Form Fields -->			                    
                                        
                </tr>
            
            </table>
            <!-- END Issues Details table -->
	
    	</div>
		<!-- STOP Details div -->        

   		<script language="javascript">
			//hide/show caller related fields based on Anonynimity
			function locationVisible(objVisible) {						
				changeObjectDisplay('tr_Location', objVisible);
				if (objVisible == "none") {
					changeObjectDisplay('imgLocation_Visible', '');					
					changeObjectDisplay('imgLocation_Hide', 'none');										
				}
				else {
					changeObjectDisplay('imgLocation_Visible', 'none');					
					changeObjectDisplay('imgLocation_Hide', '');										
				}
			}
		
			//hide/show caller related fields based on Anonynimity
			function callerVisible(objVisible) {						
				changeObjectDisplay('tr_callerName', objVisible);
				changeObjectDisplay('tr_callerTitle', objVisible);
				changeObjectDisplay('tr_callerAddress', objVisible);
				changeObjectDisplay('tr_callerPhone', objVisible);
				changeObjectDisplay('tr_callerEmail', objVisible);			
			}						

			//hide/show confidential side-tab
			function confidentialVisible(objVisible) {
				var s = document.getElementById('summary');						
				changeObjectDisplay('confidential', objVisible);
				if (objVisible=='') {
					//alert('change');
					s.innerHTML = 'Confidential';
				} else if (objVisible=='none') {
					s.innerHTML = 'Summary';
				}				
			}
		</script>


		<script language="javascript">	
            function categoryLookUp(obj) {
                //used by child window to add selected Categories
                var i, x;
                var el = document.getElementById(obj);		
                var selIndex = el.selectedIndex;
                if (selIndex != -1) {
        
                    x=0;
                    //cycle through all categories to get selected count
                    for(i=el.length-1; i>=0; i--) {
                      if(el.options[i].selected) {					  
                        x=x+1;
                      }
                    }
                    //more that one 1 was choosen so STOP
                    if(x>1) {
                      jAlert('More than one category was selected.', 'myCM Alert');
                      return false;
                    }
                    
                    //all is good...process selection of primary			
                    for(i=el.length-1; i>=0; i--) {
                      //found selected category
                      if(el.options[i].selected) {
                          
                          //alert(el.options[i].value);
                          SimpleModal.open('../_dialogs/popup_category_record.asp?recid=' + el.options[i].value + '&pageView=<% =pageView %>', 410, 450, 'no');
                      }	
                    }				
                    
                //no category selected
                }
                else {
                    jAlert('No category was selected.', 'myCM Alert');			
                }
            }
        </script>
        
        <script>
			function addSubject(subjectid,name,title,type,sanctioncheck){
												
				var securityLevel = '<% =session(session("siteID") & "adminLoggedOn") %>';
				var tempHTML;

				// create table body
				var table = document.getElementById("subjectTable");
				var tbody = table.getElementsByTagName("tbody")[0];

				// ----- ADD ROW -----
				// create row element and assign unique ID
				var rowCount = table.rows.length;
				var rowID = "subject:" + subjectid + "_tr";
				var row = document.createElement("tr");
				row.id = rowID;

				// ----- ADD CELL #1 -----	
				var td1 = document.createElement("td")					
				td1.id = "subject:" + subjectid + "_delete";
				td1.style.textAlign = "left";			
				td1.style.width = "40px";					
				tempHTML = "<a href=\"#\" onClick=\"delSubject(\'" + subjectid + "\')\"); resetSystemMsg(\'systemMessage\'); return false;\" ><img src=\"../_images/icons/16/subject_delete.png\" title=\"Delete Subject\" width=\"16\" height=\"16\" align=\"absmiddle\" style=\"vertical-align:middle; padding-right:7px;\" border=\"0\" /></a>";
				tempHTML += "<a href=\"#\" onclick=\"SimpleModal.open(\'../_dialogs/popup_config_subject.asp?recid=" + subjectid + "&cid=<% =customerID %>&action=edit\', 450, 450, \'no\'); StopTheTimer(); resetSystemMsg(\'systemMessage\'); return false;\"><img src=\"../_images/icons/16/subject_edit.png\" title=\"Edit Subject\" width=\"16\" height=\"16\" align=\"absmiddle\" style=\"vertical-align:middle;\" border=\"0\" /></a>";
				td1.innerHTML = tempHTML;				

				// ----- ADD CELL #2 -----	
				var td2 = document.createElement("td")					
				td2.id = "subject:" + subjectid + "_name";
				td2.style.textAlign = "left";		
				td2.style.cursor = "pointer";
				tempHTML = "<div style=\"width:100%;\"  onclick=\"SimpleModal.open(\'../_dialogs/popup_config_subject.asp?recid=" + subjectid + "&cid=<% =customerID %>&action=edit\', 450, 450, \'no\'); StopTheTimer(); resetSystemMsg(\'systemMessage\'); return false;\">";
				tempHTML += "<a style=\"color:#333333;\">" + name + "</a>";
				tempHTML += "</div>";
				td2.innerHTML = tempHTML;				

				// ----- ADD CELL #3 -----
				var td3 = document.createElement("td")			
				td3.id = "subject:" + subjectid + "_title";				
				td3.style.textAlign = "left";
				td3.innerHTML = title;
				
				// ----- ADD CELL #4 -----	
				var td4 = document.createElement("td")			
				td4.id = "subject:" + subjectid + "_type";				
				td4.style.textAlign = "left";				
				td4.innerHTML = type;			

				// ----- ADD CELL #5 -----	
				if (parseInt(securityLevel)<10) {
					var td5 = document.createElement("td")			
					td5.id = "subject:" + subjectid + "_sanctioncheck";				
					td5.style.textAlign = "left";				
					td5.innerHTML = sanctioncheck;							
				}
				
				// ----- FINILIZE AND APPEND NEW CELLS AND ROW -----
				row.appendChild(td1);				
				row.appendChild(td2);
				row.appendChild(td3);
				row.appendChild(td4);
				if (parseInt(securityLevel)<10) {				
					row.appendChild(td5);
				}
						
				// append row to table
				tbody.appendChild(row);
							
				// hide "no documents" message if exists
				if (document.getElementById('NoSubjectTable')) {
					document.getElementById('NoSubjectTable').style.display = "none";				
				}
				//make sure table is visible
				if (document.getElementById('subjectTable').style.display == "none") {
					document.getElementById('subjectTable').style.display = "";
				}
				
				//refresh grid
				tfSubject.RefreshGrid();
			}		

			function saveSubject(subjectid,name,title,type,sanctioncheck) {

				var securityLevel = '<% =session(session("siteID") & "adminLoggedOn") %>';
				var tempHTML;
				var td2 = document.getElementById("subject:" + subjectid + "_name");
				var td3 = document.getElementById("subject:" + subjectid + "_title");
				var td4 = document.getElementById("subject:" + subjectid + "_type");
				var td5;
				
				//update existing columns
				tempHTML = "<div style=\"width:100%;\"  onclick=\"SimpleModal.open(\'../_dialogs/popup_config_subject.asp?recid=" + subjectid + "&cid=<% =customerID %>&action=edit\', 450, 450, \'no\'); StopTheTimer(); resetSystemMsg(\'systemMessage\'); return false;\">";
				tempHTML += "<a style=\"color:#333333;\">" + name + "</a>";
				tempHTML += "</div>";
				td2.innerHTML = tempHTML;
				
				td3.innerHTML = title;
				td4.innerHTML = type;
				
				if (parseInt(securityLevel)<4) {
					tempHTML = "";
					td5 = document.getElementById("subject:" + subjectid + "_sanctioncheck");				
					if (sanctioncheck=="Y") {
						tempHTML = "Match";
					}
					else if (sanctioncheck=="N") {
						tempHTML = "No Match";
					}
					td5.innerHTML = tempHTML;
				}
				
			}

			function delSubject(subjectid) {
				var theRow = document.getElementById("subject:" + subjectid + "_tr");
				var theTable = document.getElementById("subjectTable");		
				var deleteElement = document.getElementById("arrSubjectRemoved");
				
				jConfirm('This action cannot be undone.<br/><br/>Continue with deleting this subject?', 'Confirm Delete', function(r) {
					//user agrees, remove categories
					if (r==true) {
						var rowIndex = theRow.parentNode.parentNode.rowIndex;						
						//move through all rows and find the one to delete
						for (var i = 0; i < theTable.rows.length; i++) {
							if (theRow == theTable.rows[i]) {
								//this is the only way that works deleting rows
								//from a tableFilter table
								theTable.deleteRow(i);
								tfSubject.nbRows--;			//necessary for tableFitler to work
								tfSubject.RefreshGrid();	//necessary for tableFitler to work
								deleteElement.value += subjectid + ","	//used for deleting subject on Ajax form save
							}
						}								
						//show NO SUBJECTS table
						if (theTable.rows.length<=2) {
							document.getElementById('NoSubjectTable').style.display = "";
						}											
					}
					//user disagrees
					else {
						return false;
					}
				});						
			}
			
			
			function confidential_update() {			
				var securityLevel = '<% =session(session("siteID") & "adminLoggedOn") %>';						
				//confidential option disable for non-admins
				if (document.getElementById("Caller_Confidential").value == 'Y' && parseInt(securityLevel)>30) {
					callerVisible('none');
				}
			}
			confidential_update();
			
			
		</script>        

