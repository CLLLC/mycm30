        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->       

		<!-- START Settings div -->
        <div id="tab:settings">
                
            <!-- START Issue Notes table -->   
	        <table id="table_settings" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>

	                <!-- START Side Page Tabs [genearl] "id=table:settings:general" -->
                    <!-- [this = object makeing the call ] -->
                    <!-- [settings = current top tab tab ] -->
                    <!-- [preferences = selected side tab ] -->
                    <td rowspan="4" valign="top" style="width:100px; border-top:none; padding:0px; ">
                        <div class="tabSide" style="width:100px;">
                            <div id="preferences" class="on" onClick="clickSideTab('settings','preferences');">Preferences</div>
                            <div id="permissions" class="off" onClick="clickSideTab('settings','permissions');" <% if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and ProgramAdmin <> "Y" then response.write("style=""display:none;""") %>>Permissions</div>
                            <div class="empty">&nbsp;</div>
                        </div>                        
                    </td>
                    <!-- STOP Side Page Tabs -->
                    
                    <!-- START Middle Section -->    
                    <td valign="top">
                                        
					  	<!-- START Preferences Form Fields -->	                    
					  	<table id="table:settings:preferences" width="100%" class="formTable" cellpadding="0" cellspacing="0">                                                                       

               				<tr>
                           		<td class="formLabel" >E-mail Issue Assignments:</td>
                              	<td align=left nowrap >        
                                	<label>
                                 		<input type="radio" name="emailAssignment" id="emailAssignmentYes" onclick="setupOptions();" value="Y" style="vertical-align:middle;" <%=checkRadio(emailAssignment,"Y")%> >Yes&nbsp;&nbsp;&nbsp;&nbsp;
                                     </label>			      
                                  	<label>
                                   		<input type="radio" name="emailAssignment" id="emailAssignmentNo" onclick="setupOptions();" value="N" style="vertical-align:middle;" <%=checkRadio(emailAssignment,"N")%>>No
                                  	</label>
                                    <div class="subLabel" style="padding-top:5px;">Sends e-mail notification when user is assigned to issue.</div>
                              	</td>
                        	</tr>


                            <tr>
                                <td class="formLabel"><div id="EmailIssueType:Label">Assignment Types:</div></td>    
                                <td align=left>                     
	                                <div class="subLabel" style="padding-bottom:5px;">Send notification for these issue types if 'E-mail Assignments' is Yes.</div>
                                    <div id="field_list_container">
                                        <div id="field_list" style="width:285px; border: 1px solid #ccc; padding: 2px; height:57px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">
                                            <!-- table used for javascript slider -->
                                            <table id="table:email:list" width="95%" border="0" cellspacing="0" cellpadding="0">
                                                <%												
												'discovery issue types user has access to
												mySQL = "SELECT IssueType.Name, vwLogins_IssueType.IssueTypeID " _
													  & "FROM vwLogins_IssueType INNER JOIN IssueType ON vwLogins_IssueType.IssueTypeID = IssueType.IssueTypeID " _
													  & "WHERE vwLogins_IssueType.LOGID=" & userLogID & " AND vwLogins_IssueType.CustomerID='" & customerID & "' " _
													  & "ORDER BY IssueType.Name"
                                                set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                                issueTypeCount = 0
												do while not rs.eof
													issueTypeCount = issueTypeCount + 1
                                                    if instr("," & userEmailIssueTypes & ",", "," & rs("IssueTypeID") & ",") > 0 then
                                                        checkIssue = " checked "
                                                    else
                                                        checkIssue = " "
                                                    end if				
													response.write("<tr><td class=""clearFormat"" style=""border:none; padding:0px; margin:0px;"" align=""left"">")                                                    
													response.write("	<div id=""field_email_item_Name_" & issueTypeCount & """ class=""checkItem"">")		
													response.write("		<input name=""email_checkBox"" id=""field_email_checkBox"" type=""checkbox"" style=""cursor:default;"" value=""" & rs("IssueTypeID") & """ " & checkIssue & " />")
													response.write("		<label for=""field_email_checkBox"" style=""cursor:move;"" onclick=""javascript:return false;"">" & rs("Name") & "</label>")
													response.write("	</div>")    
													response.write("</td></tr>")          
													
                                                    rs.movenext
                                                loop
                                                call closeRS(rs)
												
												if issueTypeCount = 0 then
													response.write("<tr><td class=""clearFormat"" style=""border:none; padding:0px; margin:0px;"" align=""left"">")                                                    
													response.write("	<div id=""field_email_item_Name_0"" class=""checkItem"">")		
													response.write("		<em>no group assignment</em>")
													response.write("	</div>")    
													response.write("</td></tr>")          												
												end if																								
                                                %>
                                        </table>
                                        <!-- Initiate listOrder.js for moving fields in order -->
                                        <script type="text/javascript">
                                            var table = document.getElementById('table:email:list');
                                            var tableDnD = new TableDnD();
                                            tableDnD.init(table);
                                        </script>
                                        </div>
                                    </div>                      
                                    
                                    <% if cLng(session(session("siteID") & "adminLoggedOn")) < 3 and issueTypeCount > 0 then %>
                                    	<div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frm','field_email_checkBox','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frm','field_email_checkBox',''); return false;">None</a></div>                    
                                	<% end if %>
                                
                                </td>                    
                            </tr>                

                            <tr id="emailInvAssignmentTR">
                                <td class="formLabel" style="border-bottom:1px dotted #cccccc;">E-mail Investigation Assignments:</td>
                                <td align=left nowrap style="border-bottom:1px dotted #cccccc;">        
                                    <label>
                                        <input type="radio" name="emailInvAssignment" id="emailInvAssignmentYes" value="Y" style="vertical-align:middle;" <%=checkRadio(emailInvAssignment,"Y")%> >Yes&nbsp;&nbsp;&nbsp;&nbsp;
                                     </label>			      
                                    <label>
                                        <input type="radio" name="emailInvAssignment" id="emailInvAssignmentNo" value="N" style="vertical-align:middle;" <%=checkRadio(emailInvAssignment,"N")%>>No
                                    </label>
                                    <div class="subLabel" style="padding-top:5px;">Sends e-mail notification when user is assigned to investigation.</div>
                                </td>
                            </tr>
                            
            				<tr>
                           		<td class="formLabel" style="border-bottom:1px dotted #cccccc;">E-mail Status Changes:</td>
                              	<td align=left nowrap style="border-bottom:1px dotted #cccccc;">        
                                	<label>
                                 		<input type="radio" name="emailStatusChange" id="emailStatusChangeYes" value="Y" style="vertical-align:middle;" <%=checkRadio(emailStatusChange,"Y")%> >Yes&nbsp;&nbsp;&nbsp;&nbsp;
                                     </label>			      
                                  	<label>
                                   		<input type="radio" name="emailStatusChange" id="emailStatusChangeNo" value="N" style="vertical-align:middle;" <%=checkRadio(emailStatusChange,"N")%>>No
                                  	</label>
                                    <div class="subLabel" style="padding-top:5px;">Sends e-mail notification on issue status change.</div>
                              	</td>
                        	</tr>

                            <tr id="emailInvStatusChangeTR">
                                <td class="formLabel" style="border-bottom:1px dotted #cccccc;">E-mail Investigation Status Changes:</td>
                                <td align=left nowrap style="border-bottom:1px dotted #cccccc;">        
                                    <label>
                                        <input type="radio" name="emailInvStatusChange" id="emailInvStatusChangeYes" value="Y" style="vertical-align:middle;" <%=checkRadio(emailInvStatusChange,"Y")%> >Yes&nbsp;&nbsp;&nbsp;&nbsp;
                                     </label>			      
                                    <label>
                                        <input type="radio" name="emailInvStatusChange" id="emailInvStatusChangeNo" value="N" style="vertical-align:middle;" <%=checkRadio(emailInvStatusChange,"N")%>>No
                                    </label>
                                    <div class="subLabel" style="padding-top:5px;">Sends e-mail notification on investigation status change.</div>
                                </td>
                            </tr>

                            <tr>
                                <td class="formLabel" style="border-bottom:1px dotted #cccccc;">Deadline Notices:</td>
                                <td align=left nowrap style="border-bottom:1px dotted #cccccc;">        
                                    <label>
                                        <input type="radio" name="emailDeadlineNotice" id="emailDeadlineNoticeYes" value="Y" style="vertical-align:middle;" <%=checkRadio(emailDeadlineNotice,"Y")%> >Yes&nbsp;&nbsp;&nbsp;&nbsp;
                                     </label>			      
                                    <label>
                                        <input type="radio" name="emailDeadlineNotice" id="emailDeadlineNoticeNo" value="N" style="vertical-align:middle;" <%=checkRadio(emailDeadlineNotice,"N")%>>No
                                    </label>
                                    <div class="subLabel" style="padding-top:5px;">Sends e-mail notification 3 days prior to deadline date.</div>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="formLabel">Home Page:</td>
                                <td align=left nowrap>                                
									<%
                                    'Find Categories available TO BE assigned
                                    mySQL="SELECT Reports.ReportID, Reports.Name " _
                                        & "FROM   Reports " _
                                        & "WHERE  (IssueList='Y') AND (LOGID=" & userLogID & ") AND (Reports.CustomerID = '" & customerID & "') " _
                                        & "ORDER BY Reports.SortOrder "                                                                    
									set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
									'build combo box
									response.write("<select name=""homePage"" id=""homePage"" size=1 class=""inputLong"">")
									'default to issues page
									response.write("<option value=""issues.asp"">Issue List (default)</option>")									
									'add profile/customer option if allowed
									if (cLng(session(session("siteID") & "adminLoggedOn"))<10 or programAdmin="Y") and (cLng(securityLevel)<10 or userProgramAdmin="Y") then																		
										response.write("<option value=""profiles/profiles.asp"" " & checkMatch(homePage,"profiles/profiles.asp") & ">" & accountLabelPlural & "</option>")
									end if								
									'add the rest, shortcuts
									do until rs.eof			
										response.write("<option value=""scripts/issues.asp?pageView=" & rs("ReportID") & """ " & checkMatch(homePage,"scripts/issues.asp?pageView=" & rs("ReportID")) & ">" & rs("Name") & "</option>")
										rs.movenext
									loop
									response.write("</select>")
									call closeRS(rs)
									%>
                                    <div class="subLabel" style="padding-top:5px;">
                                    	<input name="homePageKey" id="homePageKey" type="checkbox" value="Y" <% if HomePageKey="Y" then response.write(" checked ") %> />
										<label for="homePageKey" style="cursor:default;" >Enable [Home] key navigation</label>
                                    </div>
                                </td>
                            </tr>

							<tr>
                                <td class="formLabel">Autosave:</td>
                              	<td align=left>
	                           		<input name="userAutosave" id="userAutosave" class="inputShort" value="<% =userAutosave %>" /> minutes
                                	<div class="subLabel" style="padding-top:5px;">A setting of zero (0) disables Autosave.</div>
                              	</td>
                            </tr>  						
                      		<script type="text/javascript">
                         		jQuery().ready(function($) {
                             		$('#userAutosave').spinner({ min: 0, max: 60, increment: 'fast' });			
                              	});
                        	</script>          
                                                                                                                
						</table>
   						<!-- STOP Preferences Form Fields -->


					  	<!-- START Permissions Form Fields -->	                    
					  	<table id="table:settings:permissions" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">     

							<%
							'Used to set Program Admin setting here, but now it's done through the Security Level setting under Details tab
							'Leaving this here just for kicks until I don't thing we need it anymore.
                            response.write("<input name=""userProgramAdmin"" type=""hidden"" value=""" & userProgramAdmin & """/>")							
							%>


										<!-- Security Level -->
                                        <!-- FOR NOW only for DBAdmins and CCI Admins and turned off to customers but can give it to them once tested. -->
                                        <% if (cLng(session(session("siteID") & "adminLoggedOn")) < 3 or programAdmin = "Y") and (cLng(securityLevel) >= 10 or action="add") then %>                                                        
                                            <tr>
                                                <td class="formLabel"><span class="required">*</span>Security Level:</td>
                                                <td align=left style="margin-top:10px; padding-top:10px;">
                                                
                                                	<div style="float:left; border:1px solid #ccc; padding:15px; margin-right:15px; text-align:center;">
														<div id="securitySlider" style="height:150px;"></div>
                                                    </div>
                                                    
                                                    <div>
                                                    	<div id="securityDesc" style="width:500px;">&nbsp;</div>
														<input type="hidden" name="securityLevel" id="securityLevel" value="<% =securityLevel %>" />
                                                    </div>                                                                                                        
                                                </td>
                                            </tr>                                            
											<script language="javascript">	
												var sliderValue = parseFloat('<% =securityLevel %>') + 0;
												var sliderMin = parseFloat('<% =cLng(session(session("siteID") & "adminLoggedOn")) %>') + 0;
												var sliderMax
												var sliderStep;
												
												//set minimum/max based on user logged in
												if (sliderMin<10) { sliderMin=10 };
												if (sliderMin>=10) { sliderMax=50 };

												//build slider
												$(function() {
													$( "#securitySlider" ).slider({
														range: "min",
														value: sliderValue,
														min: sliderMin,
														max: sliderMax,
														step: 10,
														orientation: 'vertical',
														slide: function( event, ui ) {
															//value saved with form on submit
															$( "#securityLevel" ).val( ui.value );
															//called after security element is updated to ensure fields visible are correct
															setupOptions();															
															//description shown to user pulled from DB using jSON
															$.ajax({			  	
																url: "../_jquery/suggestbox/json-data.asp?view=config&field=configValLong&term=SecurityLevel_" + ui.value,
																cache: false,
																async: false,
																dataType: "json",
																success: function(data) {
																  if(data!=null) {				  
																	document.getElementById('securityDesc').innerHTML = data;
																  } else {
																	document.getElementById('securityDesc').innerHTML = "<none>";
																  }
																},
																error: function(xhr, ajaxOptions, thrownError) {
																	alert(xhr.status);
																	alert(thrownError);                 
																}  		
															});
														}
													});
													
													// used to be where you would set initial value of slider...i moved it to below
													// $( "#securityDesc" ).html( $( "#securitySlider" ).slider( "value" ) );																																																				
													
												});
												//set default description shown to user on FORM LOAD
												$.ajax({			  	
													url: "../_jquery/suggestbox/json-data.asp?view=config&field=configValLong&term=SecurityLevel_"+sliderValue,
													cache: false,
													async: false,
													dataType: "json",
													success: function(data) {
													  if(data!=null) {				  
														document.getElementById('securityDesc').innerHTML = data;
													  } else {
														document.getElementById('securityDesc').innerHTML = "<none>";
													  }
													},
													error: function(xhr, ajaxOptions, thrownError) {
														alert(xhr.status);
														alert(thrownError);                 
													}  		
												});
                                            </script>
                                                
                                        <%
										'user being edited is CCI staff or admin
										elseif cLng(session(session("siteID") & "adminLoggedOn")) < 3 and cLng(securityLevel) < 10 then
										
                                            dim secMsg
                                            secMsg = "Modifying a Security Level can cause unwanted and potential<br/>exposure. Please be sure you are aware of the ramifications<br/>before making any changes.<br/><br/>"
                                            secMsg = secMsg & "&nbsp;&raquo;&nbsp;1 is for DB Admin<br/>"
                                            secMsg = secMsg & "&nbsp;&raquo;&nbsp;2 is for CCI Admin<br/>"
                                            secMsg = secMsg & "&nbsp;&raquo;&nbsp;3 is for Risk Analyst<br/>"                            
                                            secMsg = secMsg & "&nbsp;&raquo;&nbsp;4 (open)<br/>"
                                            secMsg = secMsg & "&nbsp;&raquo;&nbsp;5 is for Risk Specialist<br/>"
                                            secMsg = secMsg & "&nbsp;&raquo;&nbsp;6 to 9 (open)<br/>"
                                            secMsg = secMsg & "&nbsp;&raquo;&nbsp;10 to 30 is open<br/>"
                                            secMsg = secMsg & "&nbsp;&raquo;&nbsp;30 to 100 is for Customers"										
											%>                                            
                                            <tr>
                                                <td class="formLabel"><span class="required">*</span>Security Level:</td>
                                                <td align=left>
                                                    <input name="securityLevel" id="securityLevel" class="inputShort" value="<% =securityLevel %>" maxlength="20" />
                                                    <div class="subLabel" style="padding-top:5px;">&nbsp;&dagger;&nbsp;<span class="required">NEVER</span> use a <a href="#" onclick="jAlert('<% =secMsg %>', 'Security Level'); return false;">Security Level</a> of <strong>less than 30</strong> for a customer.</div>
                                                    <div class="subLabel" style="padding-top:5px;">&nbsp;&dagger;&nbsp;A Security Level change requires a <span class="required">re-login into myCM</span>.</div>                                                                                
                                                </td>
                                            </tr>
                                            <script type="text/javascript">
												var secLevel = "<% =cLng(session(session("siteID") & "adminLoggedOn")) %>";
												secLevel = parseInt(secLevel);
                                                jQuery().ready(function($) {
                                                    $('#securityLevel').spinner({ min: secLevel, max: 100, increment: 'fast' });			
                                                });
                                            </script>  
                                                    
										<%
                                        else
                                            response.write("<input name=""securityLevel"" id=""securityLevel"" type=""hidden"" value=""" & securityLevel & """/>")
                                        end if
                                        %>       


                                        <tr>
                                            <td class="formLabel">Assigned Groups:</td>    
                                            <td align=left> 
                                
                                                <div id="field_list_container">
                                                    <div id="field_list" style="width:285px; border: 1px solid #ccc; padding: 2px; height:57px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">
                                                        <!-- table used for javascript slider -->
                                                        <table id="table:field:list-groups" width="95%" border="0" cellspacing="0" cellpadding="0">
                                                            <%
                                                            if action = "edit" then
                                                            
                                                                dim userGroups, groupCount
                                                                'discovery issue types user has access to
                                                                mySQL = "SELECT GroupID " _
                                                                      & "FROM Logins_Groups " _
                                                                      & "WHERE LogID=" & userLogID & " "
                                                                set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                                                do while not rs.eof
                                                                    userGroups = userGroups & "," & rs("GroupID") & ","
                                                                    rs.movenext
                                                                loop
                
                                                                'discovery issue types user has access to
                                                                mySQL = "SELECT Groups.CustomerID, Groups.GroupID, Groups.Name " _
                                                                      & "	FROM Groups " _
                                                                      & "	WHERE CustomerID='" & customerID & "' " _													
																	  & " UNION ALL " _
																	  & "SELECT Groups.CustomerID, Groups.GroupID, Groups.Name " _
																	  & "	FROM Groups INNER JOIN Logins_Groups ON Groups.GroupID = Logins_Groups.GroupID " _
																	  & "	WHERE Logins_Groups.LogID=" & userLogID & " AND Groups.CustomerID<>'" & customerID & "' " _
																	  & " ORDER BY Name "																
                                                                set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                                                groupCount = 0
                                                                do while not rs.eof
                                                                    groupCount = groupCount + 1
                                                                    if instr("," & userGroups & ",", "," & rs("GroupID") & ",") > 0 then
                                                                        checkIssue = " checked "
                                                                    else
                                                                        checkIssue = " "
                                                                    end if				
                                                                    response.write("<tr><td class=""clearFormat"" style=""border:none; padding:0px; margin:0px;"" align=""left"">")                                                    
                                                                    response.write("	<div id=""field_list_item_Name"" class=""checkItem"">")		
                                                                    if action = "edit" and (programAdmin = "Y" or cLng(session(session("siteID") & "adminLoggedOn")) < 3) then
																	    response.write("		<input name=""list_checkBox-groups"" id=""field_list_checkBox-groups"" type=""checkbox"" style=""cursor:default;"" value=""" & rs("GroupID") & """ " & checkIssue & " />")
																		'show as linked group
																		if lCase(customerID) <> lCase(rs("CustomerID")) then
	                                                                        response.write("	<label for=""field_list_checkBox-groups"" style=""cursor:move;"" onclick=""javascript:return false;"">" & rs("Name") & " ([linked:" & rs("CustomerID") & "])" & "</label>")
																		'normal group
																		else
																			response.write("	<label for=""field_list_checkBox-groups"" style=""cursor:move;"" onclick=""javascript:return false;"">" & rs("Name") & "</label>")
																		end if
																	else
                                                                        if checkIssue = " checked " then
																			'show as linked group
																			if lCase(customerID) <> lCase(rs("CustomerID")) then
																				response.write(rs("Name") & " ([linked:" & rs("CustomerID") & "])")																			
																			else
																				response.write(rs("Name"))																			
																			end if																		                                                                            
                                                                            response.write("<input name=""list_checkBox-groups"" type=""hidden"" value=""" & rs("GroupID") & """ " & checkIssue & " />")
                                                                        end if
                                                                    end if
                                                                    response.write("	</div>")    
                                                                    response.write("</td></tr>")          
                                                                    
                                                                    rs.movenext
                                                                loop
                                                                call closeRS(rs)
                
                                                                if groupCount = 0 then
                                                                    response.write("<tr><td class=""clearFormat"" style=""border:none; padding:0px; margin:0px;"" align=""left"">")                                                    
                                                                    response.write("	<div id=""field_list_item_Name"" class=""checkItem"">")															
                                                                    response.write("		<em>no groups available</em>")													
                                                                    response.write("	</div>")    
                                                                    response.write("</td></tr>")          												
                                                                end if
                                                            
                                                            'for copy/add
                                                            else
                                                                response.write("<tr><td class=""clearFormat"" style=""border:none; padding:0px; margin:0px;"" align=""left"">")                                                    
                                                                response.write("	<div id=""field_list_item_Name"" class=""checkItem"">")															
                                                                response.write("		<em>groups available after adding user</em>")													
                                                                response.write("	</div>")    
                                                                response.write("</td></tr>")          																								
                                                            end if
                                                            %>
                                                    </table>
                                                    <!-- Initiate listOrder.js for moving fields in order -->
                                                    <script type="text/javascript">
                                                        var table = document.getElementById('table:field:list-groups');
                                                        var tableDnD = new TableDnD();
                                                        tableDnD.init(table);
                                                    </script>
                                                    </div>
                                                </div>                      
                                                
                                                <% if programAdmin = "Y" or cLng(session(session("siteID") & "adminLoggedOn")) < 3 then %>
                                                    <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frm','field_list_checkBox-groups','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frm','field_list_checkBox-groups',''); return false;">None</a></div>                    
                                                <% end if %>
                                            
                                            </td>                    
	                                    </tr>                


							<%
							'Used to set Program Admin setting here, but now it's done through the Security Level setting under Details tab
							'Leaving this here just for kicks until I don't thing we need it anymore.
                            response.write("<input name=""userAddReport"" type=""hidden"" value=""" & userAddReport & """/>")							
							%>

							<%
							'Used to set Program Admin setting here, but now it's done through the Security Level setting under Details tab
							'Leaving this here just for kicks until I don't thing we need it anymore.
                            response.write("<input name=""userEditReport"" type=""hidden"" value=""" & userEditReport & """/>")							
							%>
                            

							<!-- DB Admins ONLY -->
							<% if cLng(session(session("siteID") & "adminLoggedOn")) < 2 then %>
                                <tr>
                                    <td class="formLabel">Default SQL View:</td>
                                    <td align=left nowrap>
                                        <%
                                        'Find Categories available TO BE assigned
                                        mySQL="SELECT Name FROM sys.views " _
                                            & "ORDER BY Name "                                                                    
                                        set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                        'build combo box
                                        response.write("<select name=""issuesView"" id=""issuesView"" size=1 class=""inputLong"">")
                                        do until rs.eof			
                                            response.write("<option value=""" & rs("Name") & """ " & checkMatch(issuesView,rs("Name")) & ">" & rs("Name") & "</option>")
                                            rs.movenext
                                        loop
                                        response.write("</select>")
                                        call closeRS(rs)
                                        %>
                                    </td>
                                </tr>
							<% 
							else
								response.write("<input name=""issuesView"" id=""issuesView"" type=""hidden"" value=""" & issuesView & """ maxlength=""255"" />")
							end if
							%>
                            							
                            <tr>
                                <td class="formLabel">Delete Issues:</td>    
                                <td align=left> 
                    
                                    <div id="field_list_container">
                                        <div id="field_list" style="width:285px; border: 1px solid #ccc; padding: 2px; height:57px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">
                                            <!-- table used for javascript slider -->
                                            <table id="table:field:list" width="95%" border="0" cellspacing="0" cellpadding="0">
                                                <%
												'discovery issue types user has access to
												mySQL = "SELECT IssueType.Name, vwLogins_IssueType.IssueTypeID " _
													  & "FROM vwLogins_IssueType INNER JOIN IssueType ON vwLogins_IssueType.IssueTypeID = IssueType.IssueTypeID " _
													  & "WHERE vwLogins_IssueType.LOGID=" & userLogID & " AND vwLogins_IssueType.CustomerID='" & customerID & "' " _
													  & "ORDER BY IssueType.Name"
                                                set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                                issueTypeCount = 0
												do while not rs.eof
													issueTypeCount = issueTypeCount + 1
                                                    if instr("," & userDeleteIssues & ",", "," & rs("IssueTypeID") & ",") > 0 then
                                                        checkIssue = " checked "
                                                    else
                                                        checkIssue = " "
                                                    end if				
													response.write("<tr><td class=""clearFormat"" style=""border:none; padding:0px; margin:0px;"" align=""left"">")                                                    
													response.write("	<div id=""field_list_item_Name"" class=""checkItem"">")		
													if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then	
														response.write("		<input name=""list_checkBox"" id=""field_list_checkBox"" type=""checkbox"" style=""cursor:default;"" value=""" & rs("IssueTypeID") & """ " & checkIssue & " />")
														response.write("		<label for=""field_list_checkBox"" style=""cursor:move;"" onclick=""javascript:return false;"">" & rs("Name") & "</label>")
													else
														if checkIssue = " checked " then
															response.write(rs("Name"))
															response.write("<input name=""list_checkBox"" type=""hidden"" value=""" & rs("IssueTypeID") & """ " & checkIssue & " />")
														end if
													end if
													response.write("	</div>")    
													response.write("</td></tr>")          
													
                                                    rs.movenext
                                                loop
                                                call closeRS(rs)
												
												if issueTypeCount = 0 then
													response.write("<tr><td class=""clearFormat"" style=""border:none; padding:0px; margin:0px;"" align=""left"">")                                                    
													response.write("	<div id=""field_list_item_Name"" class=""checkItem"">")		
													response.write("		<em>no group assignment</em>")
													response.write("	</div>")    
													response.write("</td></tr>")          												
												end if												
                                                %>
                                        </table>
                                        <!-- Initiate listOrder.js for moving fields in order -->
                                        <script type="text/javascript">
                                            var table = document.getElementById('table:field:list');
                                            var tableDnD = new TableDnD();
                                            tableDnD.init(table);
                                        </script>
                                        </div>
                                    </div>                      
                                    
                                    <% if cLng(session(session("siteID") & "adminLoggedOn")) < 3 and issueTypeCount > 0 then %>
                                    	<div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frm','field_list_checkBox','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frm','field_list_checkBox',''); return false;">None</a></div>                    
                                	<% end if %>
                                
                                </td>                    
                            </tr>                
                                                                                                                                            
						</table>
   						<!-- STOP Permissions Form Fields -->
                                        
                    </td>
                    <!-- START Page Details and Form Fields -->			                    
                                        
                </tr>
            
            </table>
            <!-- STOP Settings table -->
	
    	</div>
        <!-- STOP Settings div -->
        
		<script>
			function setupOptions() {
				
				//case settings options
				if (document.getElementById("emailAssignmentYes").checked) {
					document.getElementById("EmailIssueType:Label").style.color = "#35487B";
			        //$('#field_email_item_Name :input').removeAttr('disabled');				
					
					//loop 10 times and the stop
					var i=0;
					for (i=0;i<=10;i++) {
						$('#field_email_item_Name_' + i + ' :input').removeAttr('disabled');
					}
					
				}
				else {					
					document.getElementById("EmailIssueType:Label").style.color = "#666";					
					//$('#field_email_item_Name :input').attr('disabled', true);	//disable elements in div
					
					//loop 10 times and the stop
					var i=0;
					for (i=0;i<=10;i++) {
						$('#field_email_item_Name_' + i + ' :input').attr('disabled', true);	//disable elements in div
					}
				}
								
				//sets visibility for a few fields NOT available to Restricted (50) users on FORM LOAD
				var securityValue = parseFloat(document.getElementById("securityLevel").value) + 0;				
				if (securityValue>=50) { 
					changeObjectDisplay('emailInvAssignmentTR','none');
					changeObjectDisplay('emailInvStatusChangeTR','none');
					changeObjectDisplay('emailInvStatusChangeTR','none');										
				}
					else {
					changeObjectDisplay('emailInvAssignmentTR','');
					changeObjectDisplay('emailInvStatusChangeTR','');																
				}
								
			}
			//initialize
			setupOptions();			
		</script>