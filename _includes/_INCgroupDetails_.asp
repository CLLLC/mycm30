        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Account div -->
        <div id="tab:details">
        
        
            <!-- START Group Detail table -->   
	        <table id="table_details" cellpadding="0" cellspacing="0" style="width:100%;">

                <tr>
                    
                    <td valign="top">
                                        
					  	<!-- START Details Form Fields -->	                    
						<table id="table:details:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">                           
				
                       		<tr>                            
								<td class="formLabel"><span class="required">*</span>Name:</td>
                                <td align=left>
                                	<input name="optName" onkeyup="updateTitleText(this,'titleText','Group:&nbsp;')" class="inputLong" style="width:285px;" value="<% =optName %>" maxlength="150" />
                                </td>
                            </tr>

                            <!-- START Members Table -->    
                            <tr>
								<td class="formLabel">Group Members:</td>
								<td align="left"> 
	                               	<img src="../_images/icons/16/help.png" width="16" height="16" align="absmiddle" style="margin-right:5px; margin-bottom:2px;"/><span style="color:#2d73b2;">Double-click on a user to view details.</span>
    	                            <br>                                                                                              
                            		<select name="listUser" id="listUser" ondblclick="userLookUp('listUser');" size="5" style="width:85%; height:100px; margin-bottom:5px;" multiple="multiple">
                                 		<%
										if action = "edit" then
											dim tempUser
											
											mySQL = "SELECT a.LOGID, a.CustomerID, g.CustomerID AS [Group.CustomerID], b.GroupID, a.FirstName, a.LastName, a.Active, " _
											  	  & "	CASE CAST(SecurityLevel AS varchar(2)) WHEN NULL THEN '--' WHEN '10' THEN 'db admin' WHEN '20' THEN '--' WHEN '30' THEN 'administrator' WHEN '40' THEN 'standard' WHEN '50' THEN 'restricted' WHEN '60' THEN 'disabled' ELSE '--' END AS [Security] " _
												  & "	FROM Logins AS a INNER JOIN Logins_Groups AS b ON a.LOGID = b.LOGID INNER JOIN Groups AS g ON b.GroupID = g.GroupID "
												  
											'find all existing members INCLUDING CCI Staff
											if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
												mySQL = mySQL & " WHERE b.GroupID=" & recId & " AND a.Active<>'N' "
											'find all existing members CUSTOMER USERS ONLY
											else
												mySQL = mySQL & " WHERE b.GroupID=" & recId & " AND a.Active<>'N' AND a.SecurityLevel>10 "
											end if
											
											mySQL = mySQL & " ORDER BY a.LastName, a.FirstName "																																	
											
											'open recordset
											set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)                                                
											do while not rs.eof
												'prepare name format
												tempUser = ""
												if len(rs("lastname")) <= 0 then
													tempUser = rs("firstname")
												elseif len(rs("firstname")) <= 0 then
													tempUser = rs("lastname")
												elseif len(rs("lastname")) <= 0 and len(rs("firstname")) <= 0 then
													tempUser = "<no name>"
												else
													tempUser = rs("lastname") & ", " & rs("firstname")
												end if																		
												'show status
												if rs("Active") = "C" then 
													tempUser = tempUser & " (contact only)" 
												elseif rs("Active") = "N" then 
													tempUser = tempUser & " (disabled)" 
												else 
													tempUser = tempUser & " (" & rs("security") & ")"
													
												end if
												'show linked from other profile
												if lCase(rs("CustomerID")) <> lCase(rs("Group.CustomerID")) then 
													tempUser = tempUser & " ([linked:" & rs("CustomerID") & "])" 
												end if																								
												'add user to list
												Response.Write "<option value=""" & rs("LogID") & """>" & tempUser & "</option>"
												rs.movenext
											loop
											call closeRS(rs)									  
										end if
                                     	%>
                                    </select>
                                                                
                                	<!-- These elements MUST be set to style="display:none;" and NOT type="hidden" -->
                                 	<!-- when set as "hidden" FireFox does not recognize any change has been made  -->                                                                                            
                                  	<input name="listUserAdded" id="listUserAdded" style="display:none;" value="" />                                            
                                 	<input name="listUserRemoved" id="listUserRemoved" style="display:none;" value="" />
                                   	<br>
                                	<div style="float:left; width:175px;">
                                    	<% if action = "edit" then %>
	                                 		<a class="myCMbutton" href="#" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_members.asp?recid=<% =recId %>&cid=<% =customerID %>&multi=true', 380, 450, 'no'); return false;"><span class="add" style="padding-right:10px;">Add</span></a>
    	                            		<a class="myCMbutton" href="#" onclick="this.blur(); javascript:removeUser('listUser'); resetSystemMsg('systemMessage'); return false;"><span class="delete" style="padding-right:7px;">Delete</span></a>
                                        <% else %>
	                                 		<a class="myCMbutton" href="#" onclick="this.blur(); jAlert('<strong>Memberships disabled.</strong><br/><br/>You can add new members after adding group.', 'myCM Alert'); return false;"><span class="add" style="padding-right:10px;">Add</span></a>
    	                            		<a class="myCMbutton" href="#" onclick="this.blur(); jAlert('<strong>Memberships disabled.</strong><br/><br/>You can remove members after adding group.', 'myCM Alert'); return false;"><span class="delete" style="padding-right:7px;">Delete</span></a>
                                        <% end if %>
                                   	</div>
                                    <div>
	                                    <% 
										'for CCI Admin use ONLY!
										if cLng(session(session("siteID") & "adminLoggedOn")) < 3 and action = "edit" then %>
											<a class="myCMbutton" href="#" onclick="this.blur(); linkUser(); return false;"><span class="userlink" style="padding-right:7px;">Link Users</span></a>                                    
                                        <% end if %>                                    
                                    </div>
                                </td>
                            </tr>
                            <!-- STOP Members Table -->      
                                                                                                
                            <tr>
                                <td colspan="2" align="center">
                                    <table width="95%">
                                        <tr>
                                            <td align="left" class="clearFormat" style="padding-right:5px;">
                                                <div style="color:#35487B; font-weight:bold; padding:3px;">Notes</div>
                                                <textarea name="optNotes" id="optNotes" style="width:100%; height:100px; margin-bottom:12px;" ><%=server.HTMLEncode(optNotes & "")%></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            
                        </table>
   						<!-- END Details Form Fields -->
                        
                    </td>
                    
                </tr>
            
            </table>
            <!-- END User Account table -->
	
    	</div>
		<!-- STOP Account div -->        

		<script>
			function linkUser() {
				//make sure user agrees to change
				jConfirm('Be <strong>EXTREMELY CAREFUL</strong> when linking<br/>users from another customer profile.<br/><br/>Continue with linking?', 'Confirm Link', function(r) {
					//user agrees, remove categories
					if (r==true) {
						//open link window
						SimpleModal.open('../_dialogs/popup_members.asp?recid=<% =recId %>&cid=<% =customerID %>&multi=true&userlink=true', 380, 450, 'no');							
					}
					//user disagrees
					else {
						return false;
					}
				});							    
			}        
        </script>
        
        
        
        