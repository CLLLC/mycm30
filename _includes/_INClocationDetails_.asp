        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Account div -->
        <div id="tab:details">
        
        
            <!-- START User Account table -->   
	        <table id="table_details" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>
                    
                    <!-- START Middle Section -->    
                    <td valign="top">
                                        
					  	<!-- START Account Info Fields -->	                    
						<table id="table:details:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">

                            <tr>
                              <td class="formLabel">ID:</td>
                              <td align=left nowrap>
                              	<div>
	                              	<input name="locNameID" id="locNameID" class="inputMed" value="<%=server.HTMLEncode(locNameID & "")%>" maxlength="255" />
                                </div>
                                <div class="subLabel" style="padding-top:5px;">Unique identifier for this location.</div>                                
                              </td>
                            </tr>

                            <tr>
                              <td class="formLabel">Name:</td>
                              <td align=left nowrap>
                              	<input name="locName" id="locName" onkeyup="updateTitleText(this,'titleText','Location:&nbsp;')" class="inputLong" value="<%=server.HTMLEncode(locName & "")%>" maxlength="255" />
                              </td>
                            </tr>

                            <tr>                            
                              <td class="formLabel">Address:</td>
                              <td align=left nowrap>                                       
                                <div class="subLabel">Street Address:</div>
                                <div>
                                    <textarea name="locAddress" id="locAddress" style="width:300px; height:50px;"><%=server.HTMLEncode(locAddress & "")%></textarea>
                                </div>                               
                                <div style="float:left;">
	                                <div class="subLabel">City:</div>
                                    <input name="locCity" id="locCity" class="inputShort" value="<%=server.HTMLEncode(locCity & "")%>" maxlength="255" />
                                </div>                                
                                <div style="float:left;">
    	                            <div class="subLabel">State/Province:</div>
                                    <input name="locState" id="locState" class="inputShort" value="<%=locState%>" maxlength="255" />
                                </div>                                
                                <div>                                
        	                        <div class="subLabel">Postal Code:</div>
                                  <input name="locZip" id="locZip" class="inputShort" value="<%=server.HTMLEncode(locZip & "")%>" maxlength="255" />
                                </div>
                                <div>                                
        	                        <div class="subLabel">Country:</div>
                                    <input name="locCountry" id="locCountry" class="inputLong" value="<%=server.HTMLEncode(locCountry & "")%>" maxlength="255" />
                                </div>				                                                            
                                			                                                            
                              </td>                              
                            </tr>

                            <tr>
                              <td class="formLabel">Phone:</td>
                              <td align=left nowrap>
                              	<input name="locPhone" id="locPhone" class="inputMedium" value="<%=locPhone%>" maxlength="255" />
                              </td>
                            </tr>

                            <tr>                                                                                     
                              <td class="formLabel">
                              	<div id="imgLocation_Visible" style="height:22px;">
                                	<a href="#" onclick="locationVisible(''); return false;" title="Edit Hierarchy" class="formLabelLink"><img src="../_images/icons/16/arrow2_right.png" width="16" height="16" border="0" align="absmiddle" />Hierarchy:</a>
                                </div>
                              	<div id="imgLocation_Hide" style="display:none;">
                                	<a href="#" onclick="locationVisible('none'); return false;" title="Edit Hierarchy" class="formLabelLink"><img src="../_images/icons/16/arrow2_down.png" width="16" height="16" border="0" align="absmiddle" />Hierarchy:</a>
                                </div>
                              </td>                              
                              <td align=left nowrap style="padding-bottom:0px;">
                              	<div style="float:left; margin-top:3px;"><img src="../_images/icons/16/tree_level.gif" width="16" height="16" align="absmiddle"/>&nbsp;</div>
                                <div id="divHierarchy" style="margin-top:3px;"><% if len(locHierarchy)<=0 then  response.write("<em>none assigned</em>") else response.write( reformatHierarchy(locHierarchy,"string") ) %></div>                              
                              </td>                              
                            </tr>
                              
                            <tr id="tr_Location" style="display:none;">
                              <td style="border-top:none;">&nbsp;</td>
                              <td align=left nowrap style="border-top:none; padding-top:0px;">
                                <div class="subLabel">Level 1:</div>
                                <div>
                                    <input name="locLevel_1" id="locLevel_1" class="inputLong" type=text value="<%=server.HTMLEncode(locLevel_1 & "")%>" size=50 maxlength=50>
                                </div>                               

                                <div class="subLabel">Level 2:</div>
                                <div>
                                    <input name="locLevel_2" id="locLevel_2" class="inputLong" type=text value="<%=server.HTMLEncode(locLevel_2 & "")%>" size=50 maxlength=50>
                                </div>                               

                                <div class="subLabel">Level 3:</div>
                                <div>
                                    <input name="locLevel_3" id="locLevel_3" class="inputLong" type=text value="<%=server.HTMLEncode(locLevel_3 & "")%>" size=50 maxlength=50>
                                </div>                               

                                <div class="subLabel">Level 4:</div>
                                <div>
                                    <input name="locLevel_4" id="locLevel_4" class="inputLong" type=text value="<%=server.HTMLEncode(locLevel_4 & "")%>" size=50 maxlength=50>
                                </div>                               

                                <div class="subLabel">Level 5:</div>
                                <div>
                                    <input name="locLevel_5" id="locLevel_5" class="inputLong" type=text value="<%=server.HTMLEncode(locLevel_5 & "")%>" size=50 maxlength=50>
                                </div>                               

                                <div class="subLabel">Level 6:</div>
                                <div>
                                    <input name="locLevel_6" id="locLevel_6" class="inputLong" type=text value="<%=server.HTMLEncode(locLevel_6 & "")%>" size=50 maxlength=50>
                                </div>                               

                                <div class="subLabel">Level 7:</div>
                                <div>
                                    <input name="locLevel_7" id="locLevel_7" class="inputLong" type=text value="<%=server.HTMLEncode(locLevel_7 & "")%>" size=50 maxlength=50>
                                </div>                               

                                <div class="subLabel">Level 8:</div>
                                <div>
                                    <input name="locLevel_8" id="locLevel_8" class="inputLong" type=text value="<%=server.HTMLEncode(locLevel_8 & "")%>" size=50 maxlength=50>
                                </div>                               
                          		<div class="subLabel" style="padding-top:5px;">Used for creating a hierarchal structure for reporting.</div>
                              </td>                              
                            </tr>

                            <tr>
                                <td colspan="2" align="center">
                                	<table width="95%">
                                    	<tr>
                                        	<td align="left" class="clearFormat">
                                				<div style="color:#35487B; font-weight:bold; padding:3px;">Notes</div>
                                				<textarea name="locNotes" id="locNotes" style="width:100%; height:100px; margin-bottom:12px;" ><%=server.HTMLEncode(locNotes & "")%></textarea>
                            				</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

						</table>
   						<!-- END Account Info Form Fields -->                                                                                                                                       

                    </td>
                    <!-- STOP Middle Section -->			                    
                                        
                </tr>
            
            </table>
            <!-- END User Account table -->
	
    	</div>
		<!-- STOP Account div -->        

   		<script language="javascript">
			//hide/show caller related fields based on Anonynimity
			function locationVisible(objVisible) {						
				changeObjectDisplay('tr_Location', objVisible);
				if (objVisible == "none") {
					changeObjectDisplay('imgLocation_Visible', '');					
					changeObjectDisplay('imgLocation_Hide', 'none');										
				}
				else {
					changeObjectDisplay('imgLocation_Visible', 'none');					
					changeObjectDisplay('imgLocation_Hide', '');										
				}
			}
		</script>
        
		<script>
			//JSON QUERY BUILDER used to search database
			buildJQuery('country','country','locCountry','');
			buildJQuery('location','level_1','locLevel_1','<% =customerID %>');
			buildJQuery('location','level_2','locLevel_2','<% =customerID %>');
			buildJQuery('location','level_3','locLevel_3','<% =customerID %>');
			buildJQuery('location','level_4','locLevel_4','<% =customerID %>');
			buildJQuery('location','level_5','locLevel_5','<% =customerID %>');
			buildJQuery('location','level_6','locLevel_6','<% =customerID %>');
			buildJQuery('location','level_7','locLevel_7','<% =customerID %>');
			buildJQuery('location','level_8','locLevel_8','<% =customerID %>');

			function buildJQuery(table,field,form_field,cid) {
																
				$(function() {
					function split( val ) {
						return val.split( /,\s*/ );
					}
					function extractLast( term ) {
						return split( term ).pop();
					}                                                            
					$("#"+form_field)
						// don't navigate away from the field on tab when selecting an item
						.bind( "keydown", function( event ) {
                      		if ( event.keyCode === $.ui.keyCode.TAB &&
                            	$( this ).data( "autocomplete" ).menu.active ) {
                               		event.preventDefault();
                             }
                        })
                     	.autocomplete({
                      		source: function( request, response ) {
                          		$.getJSON( "../_jquery/suggestBox/json-data.asp?view="+table+"&field="+field+"&cid="+cid, {
                             		term: extractLast( request.term )
                           		}, response );
                          	},
                       		search: function() {
                        		// custom minLength
                             	var term = extractLast( this.value );
                           		if ( term.length < 1 ) {
                            		return false;
                         		}
                      		},
                      		focus: function() {
                    			// prevent value inserted on focus
                      			return false;
                         	}
                  		});
				});																																
			}
		</script>                                                            														

