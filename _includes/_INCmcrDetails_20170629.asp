        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

	  	<% 
      	dim headerStyle
      	if len(mcrLogoBG) > 1 then
        	headerStyle = "style='padding:0px; margin:0px; border:none; background: url(https://www.mycompliancereport.com/templates/" & CustomerID & "/" & mcrLogoBG & ") repeat top left;" & "'"
      	elseif len(mcrLogoBGColor) > 1 then
        	headerStyle = "style='padding:0px; margin:0px; border:none; background-color:" & mcrLogoBGColor & "; " & "'"
      	end if
      	%>        

		<!-- START Account div -->
        <div id="tab:details">
        
            <!-- START User Account table -->   
	        <table id="table_details" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>
                    
                    <!-- START Middle Section -->    
                    <td valign="top">


					  	<!-- START Account Info Fields -->	                    
						<table id="table:details:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">

                            <tr>
                              	<td class="formLabel">Website URL:</td>
                              	<td align=left nowrap>
 									<a href="http://www.mycompliancereport.com/report.asp?fid=11&cid=<% =CustomerID %>&rpt=1" target="_blank">http://www.mycompliancereport.com/report.asp?fid=11&cid=<% =CustomerID %>&rpt=1</a>
                              	</td>
                            </tr>


							<!-- START site HEADER section -->	 
							<tr>
                            	<td colspan="2" align="center">

                                    <table width="95%" style="border:#000000 1px solid;">
                                        <tr>
                                            <td align="left" class="clearFormat">

                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">                                        
                                                  <tr> 
                                                    <td id="mcrHeader" height="65" valign="top" <% response.write(headerStyle) %> >
                                                    	<% if len(mcrLogoName) > 0 then %>
	                                                        <img id="mcrHeaderLogo" src="<% response.write("https://www.mycompliancereport.com/templates/" & CustomerID & "/" & mcrLogoName) %>" align="top">
                                                            <div id="mcrHeaderLogoText" style="display:none;">&nbsp;</div>
                                                        <% 
														else 
															response.write( "<img id=""mcrHeaderLogo"" style=""display:none;"" src="""" align=""top"">" )
															response.write( "<div id=""mcrHeaderLogoText"" style=""padding:10px;""><b>" & getCustomerName(customerID) & "</b></div>" )
														end if
														%>
                                                    </td>
                                                  </tr>                                          
                                                </table>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                  <tr>
                                                    <td style="padding:0px; margin:0px; border:none;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
                                                  </tr>
                                                </table>
                                                <table width="100%" border="0" cellspacing="0">
                                                  <tr>
                                                    <td id="mcrMenu-Left" class="mcrHeaderMenu" <% response.write("style='FONT-SIZE: 8pt; FONT-WEIGHT: bold; padding:5px; margin:5px; border:none; background-color:" & mcrSubMenuBGColor & "';") %> > <span id="menuHome" <% response.write("style='color:" & mcrSubMenuFontColor & "';") %> >Home</span>&nbsp;|&nbsp;<span id="menuFollowUp" <% response.write("style='color:" & mcrSubMenuFontColor & "';") %> >Follow-Up</span>&nbsp;|&nbsp;<span id="menuFAQ" <% response.write("style='color:" & mcrSubMenuFontColor & "';") %> >Frequently Asked Questions</span></td>
                                                    <td id="mcrMenu-Right" class="mcrHeaderMenu" <% response.write("style='FONT-SIZE: 8pt; FONT-WEIGHT: bold; padding:5px; margin:5px; border:none; background-color:" & mcrSubMenuBGColor & "';") %> > <div align="right"><span id="menuLogoff" <% response.write("style='color:" & mcrSubMenuFontColor & "';") %> >Logoff</span></div></td>
                                                  </tr>
                                                </table>

                                            </td>
                                        </tr>                                                                                
                                    </table>


									<table width="95%" border="0" cellspacing="0" cellpadding="0">                                        
	                                	<tr> 
                                        	<td class="clearFormat" width="25%" style="padding-left:5px; padding-right:5px; background:#666; border-left: 1px solid #666; background:url(../_images/table-header-black.jpg); height:26px; color: #FFF; font-weight:bold; vertical-align:middle;" >Header</td>
                                            <td class="clearFormat" width="50%" style="padding-left:5px; padding-right:5px; background:#666; border-left: 1px solid #666; background:url(../_images/table-header-black.jpg); height:26px; color: #FFF; font-weight:bold; vertical-align:middle;">Background</td>
                                            <td class="clearFormat" width="25%" style="padding-left:5px; padding-right:5px; background:#666; border-left: 1px solid #666; background:url(../_images/table-header-black.jpg); height:26px; color: #FFF; font-weight:bold; border-right:1px solid #666; vertical-align:middle;">Menu</td>
                                        </tr>                                                                              
	                                	<tr> 
                                        	<td class="clearFormat" width="25%" style="padding:5px;">
                                            	<a href="#" onclick="SimpleModal.open('../_jquery/fancyupload/fancyupload-mcr.asp?cid=<% =CustomerID %>&image=logo', 400, 500, 'no'); resetSystemMsg('systemMessage'); return false;"><img src="../_images/icons/24/picture_add.png" align="absmiddle" border="none" style="vertical-align:middle; margin-right:5px;" /></a>
                                                <a href="#" onclick="SimpleModal.open('../_jquery/fancyupload/fancyupload-mcr.asp?cid=<% =CustomerID %>&image=logo', 400, 500, 'no'); resetSystemMsg('systemMessage'); return false;">Upload Image</a> (max 60x200)
                                            </td>
                                            <td class="clearFormat" width="50%" style="padding:5px;">
                                            	<a href="#" onclick="SimpleModal.open('../_jquery/fancyupload/fancyupload-mcr.asp?cid=<% =CustomerID %>&image=logobg', 400, 500, 'no'); resetSystemMsg('systemMessage'); return false;"><img src="../_images/icons/24/picture_add.png" align="absmiddle" border="none" style="vertical-align:middle; margin-right:5px;" /></a>
                                                <a href="#" onclick="SimpleModal.open('../_jquery/fancyupload/fancyupload-mcr.asp?cid=<% =CustomerID %>&image=logobg', 400, 500, 'no'); resetSystemMsg('systemMessage'); return false;">Upload Image</a>&nbsp;&nbsp;&nbsp;
                                            	<a href="#"><img src="../_images/icons/24/picture_add.png" align="absmiddle" border="none" style="vertical-align:middle; margin-right:5px;" /></a>
                                                <a href="#" onclick="removeMCRHeader(); return false;">Remove Image</a>&nbsp;&nbsp;&nbsp;                                                
                                                <a id="BGColorImage" href="#"><img src="../_images/icons/24/select_by_color.png" align="absmiddle" border="none" style="vertical-align:middle; margin-right:5px;" /></a>
                                                <a id="BGColorLink" href="#">Select Color</a>
                                            </td>
                                            <td class="clearFormat" width="25%" style="padding:5px;">
                                                <a id="MenuBGImage" href="#"><img src="../_images/icons/24/select_by_color.png" align="absmiddle" border="none" style="vertical-align:middle; margin-right:5px;" /></a>
                                                <a id="MenuBGLink" href="#">Background Color</a>&nbsp;&nbsp;&nbsp;
                                                <a id="MenuFontImage" href="#"><img src="../_images/icons/24/select_by_color.png" align="absmiddle" border="none" style="vertical-align:middle; margin-right:5px;" /></a>
                                                <a id="MenuFontLink" href="#">Font Color</a>
                                            </td> 
                                        </tr>                                          
                                    </table>
                                    
                                </td>
                            </tr>
                            
                            <input name="mcrLogoName" id="mcrLogoName" type="hidden" value="<% =mcrLogoName %>" />
                            <input name="mcrLogoBG" id="mcrLogoBG" type="hidden" value="<% =mcrLogoBG %>" />
                   			<input name="mcrLogoBGColor" id="mcrLogoBGColor" type="hidden" value="<% =mcrLogoBGColor %>" />
			        		<input name="mcrSubMenuFontColor" id="mcrSubMenuFontColor" type="hidden" value="<% =mcrSubMenuFontColor %>" />
			        		<input name="mcrSubMenuBGColor" id="mcrSubMenuBGColor" type="hidden" value="<% =mcrSubMenuBGColor %>" />
                            
                            <script>
								function removeMCRHeader() {								
									document.getElementById("mcrHeader").style.background = "url(../_images/x_cleardot.gif) top left";
								 	document.getElementById("mcrLogoBG").value = "";
									document.getElementById("mcrLogoBGColor").value = "";
								}
							
								// use to set element bg color if you want --> $(el).css('background-color',hex); 
								$('#BGColorLink,#BGColorImage').ColorPicker({
									livePreview: false,
									onSubmit: function(hsb, hex, rgb, el) {									
										document.getElementById("mcrHeader").style.backgroundColor = hex;
										document.getElementById("mcrLogoBGColor").value = hex;
										$(el).ColorPickerHide();
									},
									onBeforeShow: function () {
										$(this).ColorPickerSetColor(document.getElementById("mcrLogoBGColor").value);
									}								
								});
	
	
								$('#MenuBGLink,#MenuBGImage').ColorPicker({
									livePreview: false,
									onSubmit: function(hsb, hex, rgb, el) {									
										document.getElementById("mcrMenu-Left").style.backgroundColor = hex;
										document.getElementById("mcrMenu-Right").style.backgroundColor = hex;
										document.getElementById("mcrSubMenuBGColor").value = hex;
										$(el).ColorPickerHide();
									},
									onBeforeShow: function () {
										$(this).ColorPickerSetColor(document.getElementById("mcrSubMenuBGColor").value);
									}								
								});
	
								$('#MenuFontLink,#MenuFontImage').ColorPicker({
									livePreview: false,
									onSubmit: function(hsb, hex, rgb, el) {									
										document.getElementById("menuHome").style.color = hex;
										document.getElementById("menuFollowUp").style.color = hex;
										document.getElementById("menuFAQ").style.color = hex;
										document.getElementById("menuLogoff").style.color = hex;
										document.getElementById("mcrSubMenuFontColor").value = hex;
										$(el).ColorPickerHide();
									},
									onBeforeShow: function () {
										$(this).ColorPickerSetColor(document.getElementById("mcrSubMenuFontColor").value);
									}								
								});

                            </script>                                                                                                                                    
							<!-- STOP site HEADER section -->	 


							<!-- START text/disclaimer section -->						
                            <tr>
                                <td colspan="2" align="center">
                                    <table width="95%">
                                        <tr>
                                            <td align="left" class="clearFormat">
                                                <div style="color:#35487B; font-weight:bold; padding:3px;">Initial Disclaimer</div>
                                                <textarea name="mcrInitialDisclaimer" id="mcrInitialDisclaimer" style="width:100%; height:100px; margin-bottom:12px;" ><% =server.HTMLEncode(mcrInitialDisclaimer & "") %></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center">
                                    <table width="95%">
                                        <tr>
                                            <td align="left" class="clearFormat">
                                                <div style="color:#35487B; font-weight:bold; padding:3px;">Follow-Up Disclaimer</div>
                                                <textarea name="mcrFollowUpDisclaimer" id="mcrFollowUpDisclaimer" style="width:100%; height:100px; margin-bottom:12px;" ><% =server.HTMLEncode(mcrFollowUpDisclaimer & "") %></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center">
                                    <table width="95%">
                                        <tr>
                                            <td align="left" class="clearFormat">
                                                <div style="color:#35487B; font-weight:bold; padding:3px;">What's Next</div>
                                                <textarea name="mcrWhatsNext" id="mcrWhatsNext" style="width:100%; height:100px; margin-bottom:12px;" ><% =server.HTMLEncode(mcrWhatsNext & "") %></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center">
                                    <table width="95%">
                                        <tr>
                                            <td align="left" class="clearFormat">
                                                <div style="color:#35487B; font-weight:bold; padding:3px;">Frequently Asked Questions</div>
                                                <textarea name="mcrFaq" id="mcrFaq" style="width:100%; height:100px; margin-bottom:12px;" ><% =server.HTMLEncode(mcrFaq & "") %></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        
                        </table>
                                        
                    </td>
                    <!-- STOP Middle Section -->			                    
                                        
                </tr>
            
            </table>
            <!-- END User Account table -->
	
    	</div>
		<!-- STOP Account div -->        
        
		<script language="javascript">
			//set new RPT file...called from fancyupload-crystal.asp
			function addDocUploaded(file,date,size,ext,image){
				//document.getElementById('ReportFile').innerHTML = file;
				//document.getElementById('optFileName').value = file;				
				if (image=='logo') {
					document.getElementById("mcrLogoName").value = file;									
					document.getElementById("mcrHeaderLogo").style.display = "";
					document.getElementById("mcrHeaderLogo").src = "https://www.mycompliancereport.com/templates/<% =CustomerID%>/" + file;
					document.getElementById("mcrHeaderLogoText").style.display = "none";					
				}
				else if (image=='logobg') {
					document.getElementById("mcrLogoBG").value = file;				
					document.getElementById("mcrHeader").style.background = "url(https://www.mycompliancereport.com/templates/<% =CustomerID%>/" + file + ") top left";
				}
			}
		</script>
