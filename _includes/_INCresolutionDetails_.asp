        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Account div -->
        <div id="tab:configure">
                
            <!-- START User Account table -->   
	        <table id="table_configure" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>
                    
                    <!-- START Middle Section -->    
                    <td valign="top">
                                        
					  	<!-- START Account Info Fields -->	                    
						<table id="table:configure:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">

							<tr>
                            	<td align="left">                                
									<table width="95%">
                                        <tr>
                                          <td class="formLabel" style="border:none; padding-top:0px; padding-bottom:0px; margin-top:0px; margin-bottom:0px;"><span class="required">*</span>Name:</td>
                                          <td align="left" style="border:none; padding-top:0px; padding-bottom:0px; margin-top:0px; margin-bottom:0px;">
                                            <input name="temName" id="temName" onkeyup="updateTitleText(this,'titleText','Resolution:&nbsp;')" class="inputLong" value="<% =temName %>" maxlength="255" />
                                          </td>
                                        </tr>
									</table>
                                </td>
                            </tr>

                            <tr>
                                <td align="center">
                                	<table width="95%">
                                    	<tr>
                                        	<td align="left" class="clearFormat" style="padding-right:5px;">
                                				<div style="color:#35487B; font-weight:bold; padding:3px; float:left;">Resolution</div>
                                                <div class="subLabel" style="padding:3px;">&nbsp;[Enter plain text only. <em>HTML tags are not allowed.</em>]</div>
                                				<textarea name="temTemplate" id="temTemplate_<% =recId %>" style="width:100%; height:175px; margin-bottom:12px;" ><%=server.HTMLEncode(temTemplate & "")%></textarea>
                            				</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

						</table>
   						<!-- END Account Info Form Fields -->
                                                                        
                    </td>
                    <!-- STOP Middle Section -->			                    
                                        
                </tr>
            
            </table>
            <!-- END User Account table -->
	
    	</div>
		<!-- STOP Account div -->        
                                         														

