        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->       

		<!-- START Notes div, defaults to style="display:none;" -->
        <div id="tab:activity" style="display:none;">        
        
            <!-- START Issue Notes table -->   
	        <table id="table_activity" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>

                    <td valign="top" style="border-top:none; padding:0px; ">

						<!-- START Progress section -->        
   						<table id="table:activity:progress" width="100%" class="formTable" cellpadding="0" cellspacing="0">
                        	<tr>
                          		<td class="formLabel" style="background-color:#FFFFC8;">           
                                	<!-- DB Admins, CCI Admins and RAs -->                     
                                	<% if cLng(session(session("siteID") & "adminLoggedOn")) < 4 then %>                                
	                                    <a href="#" class="formLabelLinkImg" title="Configure Option" onclick="overrideProgress(); return false;" onmouseover="javascript:configImage('congif_Progress','over');" onmouseout="javascript:configImage('congif_Progress','out');"><img id="congif_Progress" src="../_images/x_cleardot_16.gif" align="absmiddle" border="0" style="vertical-align:middle;"/></a>
	                                    <a href="#" class="formLabelLink" title="Configure Option" onclick="overrideProgress(); return false;" onmouseover="javascript:configImage('congif_Progress','over');" onmouseout="javascript:configImage('congif_Progress','out');">Progress:</a>
                                	<% else %>
                                		Progress:
	                                <% end if %>
                                </td>
                                <td style="background-color:#FFFFC8;" align=left nowrap>                                
									<%
									'dim progressToShow : progressToShow = "1"
                                    dim rptRSStart									
									mySQL = "SELECT Logins.FirstName, Logins.LastName, Activity.CRSID, Activity.Activity, Activity.Date " _
										  & "	FROM Activity INNER JOIN Logins ON Activity.LOGID = Logins.LOGID " _
										  & "	WHERE Activity.Activity='RS Start [1]' AND Activity.CRSID='" & idIssue & "' " _
										  & "	ORDER BY Activity.Date DESC "
									set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                    if not rs.eof then
										'progressToShow = "2"
										rptRSStart = "1. RS Started: " & rs("date") & " by " & rs("FirstName") & " " & rs("LastName")
                                    end if
                                    closeRS(rs)
									
                                    dim rptRSMoved
									mySQL = "SELECT Logins.FirstName, Logins.LastName, Activity.CRSID, Activity.Activity, Activity.Date " _
										  & "	FROM Activity INNER JOIN Logins ON Activity.LOGID = Logins.LOGID " _
										  & "	WHERE Activity.Activity='Move to RA [2]' AND Activity.CRSID='" & idIssue & "' " _
										  & "	ORDER BY Activity.Date DESC "
									set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                    if not rs.eof then
										'progressToShow = "3"
										rptRSMoved = "2. Moved to RA: " & rs("date") & " by " & rs("FirstName") & " " & rs("LastName")
                                    end if
                                    closeRS(rs)
									
                                    dim rptRAComplete
									mySQL = "SELECT Logins.FirstName, Logins.LastName, Activity.CRSID, Activity.Activity, Activity.Date " _
										  & "	FROM Activity INNER JOIN Logins ON Activity.LOGID = Logins.LOGID " _
										  & "	WHERE Activity.Activity='RA Complete [3]' AND Activity.CRSID='" & idIssue & "' " _
										  & "	ORDER BY Activity.Date DESC "
									set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                    if not rs.eof then
										'progressToShow = "4"
										rptRAComplete = "3. Completed by RA: " & rs("date") & " by " & rs("FirstName") & " " & rs("LastName")
                                    end if
                                    closeRS(rs)

                                    dim rptXMLSent
									mySQL = "SELECT CRS.XMLSent " _
										  & "	FROM CRS " _
										  & "	WHERE CRS.CRSID='" & idIssue & "' "
									set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                    if not rs.eof then
										'progressToShow = "4"
										rptXMLSent = rs("XMLSent")
                                    end if
                                    closeRS(rs)
                                    %>    
                                	
                                    <% if len(rptProgress) > 0 then %>
                                        <!-- RS Working -->
                                        <div id="rsworking" style="padding-bottom:5px; <% if rptProgress < 1 or len(rptRSStart) <= 0 then response.write(" display:none;") %>"><% =rptRSStart %></div>
                                        <div id="progress:movetora" <% if rptProgress <> "1" then response.write("style=""display:none;""") %>><a class="myCMbutton" href="#"onClick="this.blur(); updateProgress('movetora','<% =cLng(session(session("siteID") & "adminLoggedOn")) %>'); return false;"><span class="next" style="padding-right:10px;">Move to RA</span></a></div>
    
                                        <!-- QA Working -->                                    
                                        <div id="movetora" style="padding-bottom:5px; <% if rptProgress < 2 or len(rptRSMoved) <= 0 then response.write(" display:none;") %>"><% =rptRSMoved %></div>
                                        <div id="progress:racomplete" <% if rptProgress <> "2" or cLng(session(session("siteID") & "adminLoggedOn")) >= 5 then response.write("style=""display:none;""") %>><a class="myCMbutton" href="#"onClick="this.blur(); updateProgress('racomplete','<% =cLng(session(session("siteID") & "adminLoggedOn")) %>'); return false;"><span class="next" style="padding-right:10px;">RA Complete</span></a></div>
    
                                        <!-- RA COMPLETE -->
                                        <div id="racomplete" style="padding-bottom:5px; <% if rptProgress < 3 or len(rptRAComplete) <= 0 then response.write(" display:none;") %>"><% =rptRAComplete %></div>

                                        <!-- XML File Sent -->
                                        <div id="xmlsent" style="padding-bottom:5px; <% if Not isDate(rptXMLSent) then response.write(" display:none;") %>">-- XML file sent: <% =rptXMLSent %> 
                                        <!-- DB Admins, CCI Admins and RAs re-send XML -->                     
                                        <% if cLng(session(session("siteID") & "adminLoggedOn")) < 4 and isDate(rptXMLSent) then %>                                
	                                        <a href="#" style="color:#2d73b2;" title="Configure Option" onclick="resendXML(); return false;">(Re-send XML)</a></div>
                                        <% end if %>
                                        
                                    <%
									else
                                    	response.write("<span style=""color:#F00""><em>no progress level set</em></span>")
                                    end if
									%>

                                    <input name="rptProgress" id="rptProgress" style="display:none;" value="<% =rptProgress %>" >
                                    <input name="rptProgressOriginal" id="rptProgressOriginal" style="display:none;" value="<% =rptProgress %>" >
                                    <input name="rptProgress01" id="rptProgress01" style="display:none;" value="<% =rptProgress01 %>" >
                                    <input name="rptRSID" id="rptRSID" style="display:none;" value="<% =rptRSID %>" >
                                    <input name="rptQAID" id="rptQAID" style="display:none;" value="<% =rptQAID %>" >
                                    <input name="rptXMLSent" id="rptXMLSent" style="display:none;" value="<% =rptXMLSent %>" >
                                    
                                </td>                                                                
							</tr>
                      	</table>
						<!-- STOP Progress section -->       


						<!-- START Coordinators section -->
   						<table id="table:activity:coordinators" width="100%" class="formTable" cellpadding="0" cellspacing="0">                            
                    		<tr>
                       			<td class="formLabel"><span class="emailTrigger">e</span>Coordinators:</td>                                              
                             	<!-- START User/Contact Table -->                         
                               	<td align="left"> 
                                	<img src="../_images/icons/16/help.png" width="16" height="16" align="absmiddle" style="margin-right:5px; margin-bottom:2px; vertical-align:middle;"/><span style="color:#2d73b2;">Double-click on a user to view details.</span>
                                    <br>                                
                             		<select name="listUser" id="listUser" ondblclick="userLookUp('listUser'); StopTheTimer();" size="5" style="width:85%; height:100px; margin-bottom:5px;" multiple="multiple">

                                    	<!--  Find Users ALREADY assigned -->
                                        <%
                                        dim tempFUPUser, tempFUPUserPermission
                                        'pull original case managers
                                        if action = "addfu" and appAssignFUPLogid = "Y" then
											mySQL="SELECT a.LogID, b.CustomerID AS [Logins.CustomerID], b.FirstName, b.LastName, a.ReadOnly, b.Email, b.Priority, b.Active " _
												& "FROM   CRS_Logins a LEFT JOIN Logins b " _
												& "ON     a.LogID = b.LogID " _
												& "WHERE  a.CRSID = '" & Mid(idIssue,1,Len(idIssue)-2) & "01" & "' " _
												& "ORDER BY b.LastName, b.FirstName"
                                                  
                                        'pull current case managers
                                        else
											mySQL="SELECT a.LogID, b.CustomerID AS [Logins.CustomerID], b.FirstName, b.LastName, a.ReadOnly, b.Email, b.Priority, b.Active " _
												& "FROM   CRS_Logins a LEFT JOIN Logins b " _
												& "ON     a.LogID = b.LogID " _
												& "WHERE  a.CRSID = '" & idIssue & "' " _
												& "ORDER BY b.LastName, b.FirstName"
                                        end if							
										'open recordset
										set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)                                                
                                      	do while not rs.eof
											tempUser = ""
                                      		if len(rs("LastName")) > 0 then tempUser = rs("LastName")
                                         	if len(rs("FirstName")) > 0 and len(tempUser) > 0 then tempUser = tempUser & ", " & rs("FirstName") else tempUser = rs("FirstName")
                                         	if len(rs("Priority")) > 0 then tempUser = rs("Priority") & ": " & tempUser
                                        	if rs("Active") = "C" then 
												tempUser = tempUser & " (contact only)" 
											elseif rs("Active") = "N" then
												tempUser = tempUser & " (disabled)"
											else
												tempUser = tempUser & " (active user)"
											end if
											
											'show linked from different profile
											if lCase(customerID) <> lCase(rs("Logins.CustomerID")) then tempUser = tempUser & " ([linked:" & rs("Logins.CustomerID") & "])"

                                         	'add user to list
                                           	Response.Write "<option value=""" & rs("LogID") & """>" & tempUser & "</option>"
											
											'for adding case managers to follow-ups
											tempFUPUser = tempFUPUser & "," & rs("LogID")
											if rs("ReadOnly") = "Y" then
												tempFUPUserPermission = tempFUPUserPermission & "," & rs("LogID") & ":Y"
											end if													

                                           	rs.movenext
                                      	loop
                                      	call closeRS(rs)									  
                                      	%>
                                	</select>
                                            
			                   		<!-- These elements MUST be set to style="display:none;" and NOT type="hidden" -->
               				      	<!-- when set as "hidden" FireFox does not recognize any change has been made  -->                                                                                            
                                 	<% if action="addfu" then %>
	                              		<input name="listUserAdded" id="listUserAdded" style="display:none;" value="<% =tempFUPUser %>" />
                                        <input name="listUserPermission" id="listUserPermission" style="display:none;" value="<% =tempFUPUserPermission %>" />
                                 	<% else %>
	                               		<input name="listUserAdded" id="listUserAdded" style="display:none;" value="" />          
                                        <input name="listUserPermission" id="listUserPermission" style="display:none;" value="" />                                  
                                	<% end if %>                                            
                                	<input name="listUserRemoved" id="listUserRemoved" style="display:none;" value="" />
                                 	
                                   	<br>                                            
                                            
                                 	<div style="float:left; width:175px;">
                                 		<a class="myCMbutton" href="#" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_user.asp?recid=<% =idIssue %>&cid=<% =customerID %>&multi=true', 380, 450, 'no'); StopTheTimer(); resetSystemMsg('systemMessage'); return false;"><span class="add" style="padding-right:10px;">Add</span></a>
                               			<a class="myCMbutton" href="#" onclick="this.blur(); javascript:removeUser('listUser'); resetSystemMsg('systemMessage'); return false;"><span class="delete" style="padding-right:7px;">Delete</span></a>
                                 	</div>

                                    <!-- TURNED OFF DUE TO TIME CONSTRANTS -->
                                    <% if 1=2 then %>
                                 	<div>
                                 		<a class="myCMbutton" href="#" onclick="this.blur(); javascript:attachNote('listUser',true); resetSystemMsg('systemMessage'); return false;"><span class="note" style="padding-right:7px;">Add Note</span></a>
                                	</div>
                                    <% end if %>
                                    
                              	</td>										                                
                          	</tr>     
                      	</table>
						<!-- STOP Coordinators section -->       


						<!-- START Activity section -->
   						<table id="table:activity:notes" width="100%" class="formTable" cellpadding="0" cellspacing="0">                            
							<tr>            	
								<td align="center">
                                                                        	
                                    <div style="color:#35487B; width:95%; font-weight:bold; text-align:left;">Activities</div>

									<!-- EDITING ISSUE look for existing uploaded documentation -->                                    
									<%
									dim arrActivityID
									dim activityCount : activityCount = 0
									
									'set alternating row colors
									dim rowColorActivity : rowColorActivity = col2
									
                                    if action = "edit" then									
											%>
											<!-- START Activity table  -->
											<table id="activityTable" width="95%" cellpadding="0" cellspacing="0" style="margin-top:5px;">
                                            	<tbody id="activityTableBody">
											  	<tr>
													<td nowrap="nowrap" align="left" class="clearFormat" style="padding-left:5px; padding-right:5px; background:#666; border-left: 1px solid #666; background:url(../_images/table-header-black.jpg); height:26px; color: #FFF; font-weight:bold; vertical-align:middle;">Note</td>
													<td nowrap="nowrap" align="left" class="clearFormat" style="padding-left:5px; padding-right:5px; background:#666; border-left: 1px solid #666; background:url(../_images/table-header-black.jpg); height:26px; color: #FFF; font-weight:bold; vertical-align:middle;">User</td>
													<td nowrap="nowrap" align="center" class="clearFormat" style="padding-left:5px; padding-right:5px; background:#666; border-left: 1px solid #666; background:url(../_images/table-header-black.jpg); height:26px; color: #FFF; font-weight:bold; border-right:1px solid #666; vertical-align:middle;">Date</td>
												</tr>
												<%
												'-----------------------------------
												'LOOK for existing notes
												mySQL = "SELECT Activity.ActivityID, Activity.CRSID, Activity.CustomerID, Activity.LOGID, Activity.Date, " _
													  & "	Activity.Activity, Logins.FirstName + ' ' + Logins.LastName AS [User] " _
													  & "		FROM Activity LEFT JOIN Logins ON Activity.LOGID = Logins.LOGID " _
													  & "		WHERE Activity.CRSID = '" & idIssue & "' " _
													  & "		ORDER BY Date "													  
													
												set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)      

												if not rs.eof then	
														
													do while not rs.eof			
													
														'used for saving form elements
														arrActivityID = arrActivityID & rs("ActivityID") & ","
																			
														activityCount = activityCount + 1
														%>                                                
														<!-- START Activity list -->
														<tr valign="middle">                                            
															<td class="activityLeft" style="background-color:<% =rowColorActivity%>;">                                                            
                                                            	<div style="float:left;">
                                                                	<% if lCase(rs("activity")) = "rs start [1]" or lCase(rs("activity")) = "move to ra [2]" or lCase(rs("activity")) = "ra complete [3]" then %>
		                                                                <a href="#" onclick="jAlert('Permission denied.', 'myCM Alert'); return false;"><img src="../_images/icons/16/note_delete_disabled.png" title="Delete Activity" width="16" height="16" align="absmiddle" style="vertical-align:middle; padding-right:5px;" border="0" /></a><a href="#" onclick="jAlert('Permission denied.', 'myCM Alert'); return false;"><img src="../_images/icons/16/note_edit_disabled.png" title="Edit Activity" width="16" height="16" align="absmiddle" style="vertical-align:middle; padding-right:6px;" border="0" /></a>
                                                                	<% else %>
		                                                                <a href="#" onclick="deleteNote(<% =rs("ActivityID") %>); return false;"><img src="../_images/icons/16/note_delete.png" title="Delete Activity" width="16" height="16" align="absmiddle" style="vertical-align:middle; padding-right:5px;" border="0" /></a><a href="#" onclick="editNote(<% =rs("ActivityID") %>); return false;"><img src="../_images/icons/16/note_edit.png" title="Edit Activity" width="16" height="16" align="absmiddle" style="vertical-align:middle; padding-right:6px;" border="0" /></a>
                                                                    <% end if %>
                                                                </div>
                                                                <div id="rptEditNote_<% =rs("ActivityID") %>">
																	<% =rs("activity") %>&nbsp;
                                                                </div>
                                                                <input name="rptActivityCheck_<% =rs("ActivityID") %>" id="rptActivityCheck_<% =rs("ActivityID") %>" type="checkbox" style="display:none;" value="" />
                                                                <input name="rptActivityNote_<% =rs("ActivityID") %>" id="rptActivityNote_<% =rs("ActivityID") %>"  style="display:none;" value="<% =rs("activity") %>">
															</td>
															<td class="activityCenter" style="background-color:<% =rowColorActivity%>;">
																<% =rs("user") %>&nbsp;
															</td>
															<td class="activityRight" style="background-color:<% =rowColorActivity%>;">
																<% =rs("date") %>&nbsp;
															</td>
														</tr>                                                        
														<%
														'Switch Row Color
														if rowColorActivity = col2 then rowColorActivity = col1 else rowColorActivity = col2																													
														'move to next activity					
														rs.movenext														
													loop
																								
												end if
												call closeRS(rs)
												%>                                                
                                                
                                       			<!-- FORM ELEMENT SAVING... -->
                                                <!-- These elements MUST be set to style="display:none;" and NOT type="hidden" -->
                                                <!-- when set as "hidden" FireFox does not recognize any change has been made  -->
												<input name="arrActivity" id="arrActivity" style="display:none;" value="<% =arrActivityID %>" />
                                                <input name="arrActivityAdded" id="arrActivityAdded" style="display:none;" value="" />
                                                <input name="arrActivityRemoved" id="arrActivityRemoved" style="display:none;" value="" />
                                                                                              
												</tbody>
										  	</table>

										  	<!-- SHADING table/row -->
                                          	<table width="95%" cellpadding="0" cellspacing="0">                                                                                     	  
										    	<tr>
													<td colspan="3" class="clearFormat" style="background:#D0D0D0" height="5px;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
										    	</tr>
										  	</table>                                             
										  	<!-- STOP Activity table  -->


										  	<!-- No Activities found  -->
											<% 
											if activityCount = 0 then 
												%>                                            
                                            	<table id="NoActivityTable" width="95%" cellpadding="0" cellspacing="0" style="margin-top:10px;">
                                                	<tr>
                                                        <td align="left" class="clearFormat" style="padding-right:2px;">                                                                                                
                                                	        <% call systemMessageBox("activityID","statusMessageINFO","No activities found. Click ""Add"" below.") %>
                                                        </td>
                                                    </tr>
                                                </table>												                                          
												<%			
											end if
																	
										%>
                                        <!-- STOP Activity table  -->
	
										<table width="95%" cellpadding="0" cellspacing="0">
											<tr>
												<td align="left" class="clearFormat">
                                                    <div style="float:left; padding-top:5px; width:125px;">
														<a class="myCMbutton" href="#" onclick="this.blur(); attachNote('',true); resetSystemMsg('systemMessage'); return false;"><span class="add" style="padding-right:7px;">Add</span></a>
                                                    </div>                
                                                    <div style="padding-top:5px;">
                                                        <a class="myCMbutton" href="#" onclick="this.blur(); attachNote('E-Mailed issue to client',false); resetSystemMsg('systemMessage'); return false;"><span class="note" style="padding-right:7px;">E-Mail Note</span></a>
                                                        <a class="myCMbutton" href="#" onclick="this.blur(); attachNote('Faxed issue to client',false); resetSystemMsg('systemMessage'); return false;"><span class="note" style="padding-right:7px;">Fax Note</span></a>
                                                    </div>
												</td>
											</tr>
										</table>
                                    
                                    <!-- ADDING new issue...activities not available yet -->
									<% else %>
                                    	<br>
										<table width="95%" cellpadding="0" cellspacing="0">
											<tr>
												<td align="left" class="clearFormat" style="padding-right:2px;">                                                                                                
												<% call systemMessageBox("activityID","statusMessageINFO","Activities will become available after saving issue.") %>
												</td>
											</tr>
										</table>
									<% end if %>
                                                                        
                                </td>    
							</tr>                            
                            
                      	</table>
						<!-- STOP Activity section -->     

                                        
                    </td>
                    <!-- START Page Details and Form Fields -->			                    
                                        
                </tr>
            
            </table>
            <!-- STOP Notes table -->
	
    	</div>
        <!-- STOP Notes div -->

		<%
		'set User Field default values...must be here so as to not interfear with the User Fields on Case Notes "_INCissueNotes.asp" tab
		'NOT setting if issue is being edited...don't want to mess up what customer has done.
        if action = "add" or action = "addfu" then							
			response.Write("<input type=""hidden"" name=""rptUserField1"" id=""rptUserField1"" value=""" & rptUserField1 & """ >")
			response.Write("<input type=""hidden"" name=""rptUserField2"" id=""rptUserField2"" value=""" & rptUserField2 & """ >")
			response.Write("<input type=""hidden"" name=""rptUserField3"" id=""rptUserField3"" value=""" & rptUserField3 & """ >")			
			response.Write("<input type=""hidden"" name=""rptUserField4"" id=""rptUserField4"" value=""" & rptUserField4 & """ >")						
			response.Write("<input type=""hidden"" name=""rptUserField5"" id=""rptUserField5"" value=""" & rptUserField5 & """ >")
			response.Write("<input type=""hidden"" name=""rptUserField6"" id=""rptUserField6"" value=""" & rptUserField6 & """ >")
			response.Write("<input type=""hidden"" name=""rptUserField7"" id=""rptUserField7"" value=""" & rptUserField7 & """ >")			
			response.Write("<input type=""hidden"" name=""rptUserField8"" id=""rptUserField8"" value=""" & rptUserField8 & """ >")		
			response.Write("<input type=""hidden"" name=""rptUserField9"" id=""rptUserField9"" value=""" & rptUserField9 & """ >")		
			response.Write("<input type=""hidden"" name=""rptUserField10"" id=""rptUserField10"" value=""" & rptUserField10 & """ >")													
			response.Write("<input type=""hidden"" name=""rptUserField11"" id=""rptUserField11"" value=""" & rptUserField11 & """ >")
			response.Write("<input type=""hidden"" name=""rptUserField12"" id=""rptUserField12"" value=""" & rptUserField12 & """ >")
			response.Write("<input type=""hidden"" name=""rptUserField13"" id=""rptUserField13"" value=""" & rptUserField13 & """ >")			
			response.Write("<input type=""hidden"" name=""rptUserField14"" id=""rptUserField14"" value=""" & rptUserField14 & """ >")						
			response.Write("<input type=""hidden"" name=""rptUserField15"" id=""rptUserField15"" value=""" & rptUserField15 & """ >")
			response.Write("<input type=""hidden"" name=""rptUserField16"" id=""rptUserField16"" value=""" & rptUserField16 & """ >")
			response.Write("<input type=""hidden"" name=""rptUserField17"" id=""rptUserField17"" value=""" & rptUserField17 & """ >")			
			response.Write("<input type=""hidden"" name=""rptUserField18"" id=""rptUserField18"" value=""" & rptUserField18 & """ >")		
			response.Write("<input type=""hidden"" name=""rptUserField19"" id=""rptUserField19"" value=""" & rptUserField19 & """ >")		
			response.Write("<input type=""hidden"" name=""rptUserField20"" id=""rptUserField20"" value=""" & rptUserField20 & """ >")													
		end if
		%>        

		<script>
			function updateProgress(progress,securitylevel) {
				var saveButtonTop = document.getElementById('buttonSaveTop');
				var saveButtonBottom = document.getElementById('buttonSaveBottom');				
				var progress = document.getElementById('rptProgress');				
				var progressOrig = document.getElementById('rptProgressOriginal');
				var progress01 = document.getElementById('rptProgress01');	
							
				var progLocation = document.getElementById('locName').value + document.getElementById('locAddr').value;
				var progLogID = '<% =response.write(session(session("siteID") & "logid")) %>';
				var validateMsg = '';
				
				if (progress.value == '1') {	
				
					// limit usage!!!
					if (progLogID == '4255' || progLogID == '309613' || progLogID == '309633' || progLogID == '4221' || progLogID == '4259' || progLogID == '300022') {
						
						// build validation message
						progLocation = trimStr(progLocation);
						if (progLocation.length <= 0) {
							validateMsg = '<strong>Form validation error:</strong><br><br>- Location <i>null</i><br><br>Update progress level to <strong>Move to RA</strong>?';
						} else {
							validateMsg = 'Update progress level to <strong>Move to RA</strong>?';
						}
						
					}
					
					jConfirm(validateMsg, 'Confirm Update', function(r) {
						//user agrees, remove categories
						if (r==true) {
							document.getElementById('movetora').style.display = '';
							document.getElementById('movetora').innerHTML = '2. Moved to RA: ' + CurrentDate() + ' ' + CurrentTime() + ' by <% =response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %>';					
							//hide button
							document.getElementById('progress:movetora').style.display = 'none';
							//update progress level
							progress.value = '2';										
							//add activity
							attachNote('Move to RA [2]',false);						
							//force stop autosave
							if (parseInt(securitylevel)>=5) {
								//disable anymore saves until user reloads page
								saveButtonTop.onclick = function () {return false;};
								saveButtonTop.className = "myCMbutton-off";
								saveButtonBottom.onclick = function () {return false;};							
								saveButtonBottom.className = "myCMbutton-off";
								//disable autosave just incase it's activated
								Aggressive_StopTheTimer();
								document.getElementById('AutosaveURL').innerHTML = "Autosave is currently";														
								document.getElementById('AutosaveONOFF').innerHTML = "<strong>OFF</strong>.&nbsp;";		
							}
							else {
								document.getElementById('progress:racomplete').style.display = '';
							}
							//reset any previous messages
							resetSystemMsg('systemMessage'); 
							//save changes
							autoPostForm('no');
						}
						//user disagrees
						else {
							return false;
						}
					});

				}				
				else if (progress.value == '2') {
					jConfirm('Update progress level to <strong>RA Complete</strong>?<br/><br/>Note: If the customer is a myCM user<br/>e-mails will be sent immediately.', 'Confirm Update', function(r) {
						//user agrees, remove categories
						if (r==true) {
							document.getElementById('racomplete').style.display = '';
							document.getElementById('racomplete').innerHTML = '3. Completed by RA: ' + CurrentDate() + ' ' + CurrentTime() + ' by <% =response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %>';
							//hide buttons
							document.getElementById('progress:movetora').style.display = 'none';
							document.getElementById('progress:racomplete').style.display = 'none';										
							//update progress level
							progress.value = '3';
							//disable anymore saves until user reloads page
							saveButtonTop.onclick = function () {return false;};
							saveButtonTop.className = "myCMbutton-off";
							saveButtonBottom.onclick = function () {return false;};							
							saveButtonBottom.className = "myCMbutton-off";							
							//disable autosave just incase it's activated
							Aggressive_StopTheTimer();
							document.getElementById('AutosaveURL').innerHTML = "Autosave is currently";														
							document.getElementById('AutosaveONOFF').innerHTML = "<strong>OFF</strong>.&nbsp;";									
							//add activity
							attachNote('RA Complete [3]',false);
							//reset any previous messages
							resetSystemMsg('systemMessage'); 
							//save changes
							autoPostForm('no');
						}
						//user disagrees
						else {
							return false;
						}
					});
				}
				
				else if (progress.value == '3') {
					//nothing right now...
				}								
			}
		</script>

		<script>
        //start follow-up
        function overrideProgress() {
			var progress = document.getElementById('rptProgress');
            jPrompt('Overriding an issue\'s progress level<br/>may effect it\'s delievery to client.<br/><br/>&nbsp;1 = RA Start<br/>&nbsp;2 = Move to RA<br/>&nbsp;3 = RA Complete<br/>&nbsp;4 = Restricted<br/><br/>Current Progress Level:', progress.value, 'Override Progress', function(r) {
                if( r ) { 					
					if (parseInt(r)<1 || parseInt(r)>4){
						jAlert('Invalid progress level. Only 1 to 4 allowed.', 'myCM Alert');
					}
					else {
						document.getElementById('rptProgress').value = r;
						//add note entry to activity list
						attachNote('Progress Level set to [' + r + ']',false);
					}					
                }
            });
        }
		</script>
        
        
		<script>
			//start new activity
			function attachNote(note,askfirst) {
				if (askfirst==true) {
					jPrompt('Enter note:', note, 'Add Activity', function(r) {
						if( r ) { 
							attachNoteRecord(r);
						}
					});
				}
				else {
					attachNoteRecord(note);
				}
			}
			
			function editNote(noteid) {
				var note = document.getElementById("rptActivityNote_" + noteid);				
				var onscreen = document.getElementById("rptEditNote_" + noteid);											
				jPrompt('Enter note:', note.value, 'Edit Activity', function(r) {
					if( r ) { 
						note.value = r;
						onscreen.innerHTML = r;
					}
				});
			}
			
			function deleteNote(noteid) {
				var chkActivity = document.getElementById("rptActivityCheck_" + noteid);
				var arrRemoved = document.getElementById("arrActivityRemoved");

				//make sure user agrees to change
				jConfirm('Remove activity note from issue?', 'Confirm Delete', function(r) {
					//user agrees, remove categories
					if (r==true) {
						//add ID to form element for removal on save
						arrRemoved.value += noteid + ",";
						//make invisible checkbox so we know which row to delete
						chkActivity.checked = true;			
						//delete table row
						var table = document.getElementById("activityTable"); 
						var rowCount = table.rows.length;						
						for(var i=0; i<rowCount; i++) { 
							var row = table.rows[i]; 
							var chkbox = row.cells[0].childNodes[2]; 
							if(null != chkbox && true == chkbox.checked) { 
								table.deleteRow(i); 
								rowCount--; 
								i--; 						
							}
						}
					}
					//user disagrees
					else {
						return false;
					}
				});
				
			}
		</script>

		<script>
        //start follow-up
        function resendXML() {
			//make sure user agrees to change
			jConfirm('Resend this issue\'s XML file to 3rd party vendor?', 'Confirm Re-send', function(r) {
				//user agrees, remove categories
				if (r==true) {
					document.getElementById('rptXMLSent').value = '';					
					document.getElementById('xmlsent').innerHTML = '-- <i>XML file will be resent shortly</i>';
				}
				//user disagrees
				else {
					return false;
				}
			});
        }
		</script>                                

		<script language="javascript">
			// set row color in global variable
			var rowColor = "<% =rowColorActivity %>";
			function attachNoteRecord(note){
				
				var tempHTML;
				var fileImg;
				var arrActivityAdded = document.getElementById("arrActivityAdded");

				// create randon numbers used to create unique element IDs for idQueryValue form fields
				var valueID_1 = Math.floor(Math.random()*1001);
				  
				// create table body
				var table = document.getElementById("activityTable");
				var tbody = table.getElementsByTagName("tbody")[0];
				
				// ----- ADD ROW -----
				// create row element and assign unique ID
				var rowCount = table.rows.length;
				var rowID = "rowActivity_" + (rowCount+2);
				var row = document.createElement("tr");
				row.id = rowID;
					
				// ----- ADD CELL #1 -----
				var td1 = document.createElement("td")					
				td1.className = "activityLeft";					
				td1.style.backgroundColor = rowColor;
				tempHTML = null;
				tempHTML = "<div style=\"float:left;\">";				
				tempHTML = tempHTML + "<a href=\"#\" onclick=\"jAlert('Reload issue to activate delete permission.', 'myCM Alert'); return false;\"><img src=\"../_images/icons/16/note_delete_disabled.png\" title=\"Delete Activity\" width=\"16\" height=\"16\" align=\"absmiddle\" style=\"vertical-align:middle; padding-right:5px;\" border=\"0\" /></a>";			
				if (note=='RS Start' || note=='Move to RA' || note=='RA Complete') {
					tempHTML = tempHTML + "<a href=\"#\" onclick=\"jAlert('Permission denied.', 'myCM Alert'); return false;\"><img src=\"../_images/icons/16/note_edit_disabled.png\" title=\"Edit Activity\" width=\"16\" height=\"16\" align=\"absmiddle\" style=\"vertical-align:middle; padding-right:6px;\" border=\"0\" /></a>";					
				}
				else {
					tempHTML = tempHTML + "<a href=\"#\" onclick=\"jAlert('Reload issue to activate edit permission.', 'myCM Alert'); return false;\"><img src=\"../_images/icons/16/note_edit_disabled.png\" title=\"Edit Activity\" width=\"16\" height=\"16\" align=\"absmiddle\" style=\"vertical-align:middle; padding-right:6px;\" border=\"0\" /></a>";
				}
				tempHTML = tempHTML + "</div>";
				tempHTML = tempHTML + "<div id=\"rptEditNote_" + valueID_1 + "\">" + note + "</div>"
				tempHTML = tempHTML + "<input name=\"rptActivityCheck_" + valueID_1 + "\" id=\"rptActivityCheck_" + valueID_1 + "\" type=\"checkbox\" style=\"display:none;\" value=\"\" />"
				tempHTML = tempHTML + "<input name=\"rptActivityNote_" + valueID_1 + "\" id=\"rptActivityNote_" + valueID_1 + "\" style=\"display:none;\" value=\"" + note + "\">";
				td1.innerHTML = tempHTML;

				// ----- ADD CELL #2 -----	
				var td2 = document.createElement("td")					
				td2.className = "activityCenter";
				td2.style.backgroundColor = rowColor;
				td2.innerHTML = '<% =response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %>';

				// ----- ADD CELL #3 -----	
				var td3 = document.createElement("td")
				td3.className = "activityRight";
				td3.style.backgroundColor = rowColor;
				td3.innerHTML = CurrentDate() + ' ' + CurrentTime() + '&nbsp;';

				// ----- FINILIZE AND APPEND NEW CELLS AND ROW -----
				row.appendChild(td1);
				row.appendChild(td2);
				row.appendChild(td3);
						
				// append row to table
				tbody.appendChild(row);

				// update row color for next row
				if (rowColor == "#F0F0F0") {rowColor = "#FFFFFF"} else {rowColor = "#F0F0F0"};
				
				// add new id to element for form saving
				arrActivityAdded.value += valueID_1 + ",";
				
				// hide "no documents" message if exists
				if (document.getElementById('NoActivityTable')) {
					document.getElementById('NoActivityTable').style.display = "none";				
				}

			}
		</script>
        
      