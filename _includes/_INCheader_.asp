<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>myCM 3.0 : <% =pageTitle %></TITLE>

	<link rel="shortcut icon" href="../_images/favicon.ico" >

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
	<link type="text/css" rel="stylesheet" href="../_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />
    <link type="text/css" rel="stylesheet" href="../_css/tabs.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/sidePanel.css" />
    	
    <!--[if IE]>
    <style>
    fieldset.infoBox
    {
        border:clear;
        background:none;
    }
    
    fieldset legend
    {
        background-color:#FFFFFF;
        padding:0px 4px;
    }
    
    fieldset div
    {
        margin:4px 8px 8px 8px;
    }
    </style>
    <![endif]-->
    

	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- Tab (top/side) Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/tabs.js"></script>

	<!-- Form Validation Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/forms.js"></script>
    
	<!-- Session variables used in Javascript functions and default Values -->
	<script language="Javascript1.2">
		<!--
		var sessionLogid=<%= session(session("siteID") & "logid")%>; //this value is used in DHTML menu,specifically the "Profile" mentu item
		//-->
	</script> 

	<!-- This is used by the TinyMCE component WYSIWYG control -->
	<script type="text/javascript" src="../scripts/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
        
	<!-- MENU TOP PAGE Sothink DHTML menu http://www.sothink.com/ -->		
	<script type="text/javascript" src="../scripts/menu/stmenu.js"></script>
	<script type="text/javascript" src="../scripts/tree/stlib.js"></script>

	<!-- HTML Table Filter Generator http://tablefilter.free.fr/ -->
    <link rel="stylesheet" type="text/css" href="../_tablefilter/filtergrid.css"/>
	<script type="text/javascript" src="../_tablefilter/tablefilter_all_min.js"></script>
	<script type="text/javascript" src="../_tablefilter/sortabletable.js"></script>
    <script type="text/javascript" src="../_tablefilter/tfAdapter.sortabletable.js"></script>        

	<!-- DHTML Calendar http://www.dhtmlx.com -->
	<link rel="stylesheet" type="text/css" href="../_calendar/dhtmlxCalendar/codebase/dhtmlxcalendar.css">
    <link rel="stylesheet" type="text/css" href="../_calendar/dhtmlxCalendar/codebase/skins/dhtmlxcalendar_simplegrey.css">
	<script type="text/javascript" src="../_calendar/dhtmlxCalendar/codebase/dhtmlxcommon.js"></script>
	<script type="text/javascript" src="../_calendar/dhtmlxCalendar/codebase/dhtmlxcalendar.js"></script>

    <link rel="stylesheet" href="../_accordion/style.css" type="text/css" />
    <script type="text/javascript" src="../_accordion/accordion.js"></script>

	<!-- HTML List/Table resorting functions (right now own) -->
	<!-- Example usage:
    		var table = document.getElementById('table-1');
			var tableDnD = new TableDnD();
			tableDnD.init(table);  -->
	<script type="text/javascript" src="../scripts/javascript/orderList.js"></script>

	<!-- COOKIE CRUMB Navigation http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/cookieCrumbs.js"></script>

	<!-- JQUERY Libraries -->
    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../_jquery/jquery-ui-1.8.11.custom.css" />
        
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/code-bank/simple-modal/ -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>
    
    <!-- ONLY FOR suggest boxes and JSON data pulls, includes full JQuery library -->
    <style>
        .ui-autocomplete-loading { background: white url('../_jquery/suggestbox/ui-anim_basic_16x16.gif') right center no-repeat; }
    </style>
    <style>
        .ui-autocomplete {
            max-height: 100px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
            /* add padding to account for vertical scrollbar */
            padding-right: 20px;
        }
        /* IE 6 doesn't support max-height
         * we use height instead, but this forces the menu to always be this tall
         */
        * html .ui-autocomplete {height: 100px;}
    </style>
    <!-- ----------------------------------------- -->

	<!-- JGROWL Notifications http://stanlemon.net/projects/jgrowl.html -->
    <script src="../_jquery/jgrowl/jquery.jgrowl.js"></script>
    <link rel="stylesheet" type="text/css" href="../_jquery/jgrowl/jquery.jgrowl.css"/>

	<!-- jQuery Prompt Replacements http://abeautifulsite.net/blog/2008/12/jquery-alert-dialogs/ -->
    <script src="../_jquery/prompt/jquery.alerts.js"></script>
    <link rel="stylesheet" type="text/css" href="../_jquery/prompt/jquery.alerts.css"/>

	<!-- jQuery Watermark http://digitalbush.com/projects/watermark-input-plugin/ -->
    <script src="../_jquery/watermark/jquery.watermarkinput.js"></script>

	<!-- JQuery SPINNER https://github.com/btburnett3/jquery.ui.spinner/ -->
    <link type="text/css" rel="stylesheet" href="../_jquery/spinner/ui.spinner.css" />
	<script type="text/javascript" src="../_jquery/spinner/ui.spinner.js"></script>

	<!-- to prevent user from using BACK button. -->
	<script type="text/javascript">
        window.history.forward();
    </script>
    
</head>

<body>

<!-- ANCHOR for links -->
<div id="top"></div>

<!-- START JGrowl System Notifications -->
<!-- note: _INCSecurity and LoadUser() -->
<!-- 	   must be loaded first        -->
<div id="systemAlerts" class="jGrowl bottom-right"></div>
<script type="text/javascript">
	var arrAlert = "<% =systemAlerts %>";				//pulled directly from DB from loaduser()
	var alertsViewed = getCookie("myCM3-AlertsViewed");	//current cookie holding alerts already viewed
	var temp;
	var i;		
	//build what's to show and what's to hide
	temp = arrAlert.split("*|*");
	arrAlert = "";
	for(i=0; i<temp.length; i++) {
		if (temp[i]>'') {
			if (alertsViewed) {			
				if (alertsViewed.indexOf("|" + temp[i].substring(0,temp[i].indexOf(":")) + "|") <= -1) {
					//for cookie, don't show this one again today
					alertsViewed += "|" + temp[i].substring(0,temp[i].indexOf(":")) + "|";
					//for jQuery, load this message now
					arrAlert += "*|*" + temp[i].substring(temp[i].indexOf(":")+1) + "*|*";
				}				
			}
			else {
				//for cookie, don't show this one again today
				alertsViewed += "|" + temp[i].substring(0,temp[i].indexOf(":")) + "|";
				//for jQuery, load this message now
				arrAlert += "*|*" + temp[i].substring(temp[i].indexOf(":")+1) + "*|*";						
			}
			
		}
	}
	//build messages
	temp = arrAlert.split("*|*");
	//add jQuery notification to page
	(function($){
		$(document).ready(function(){
			for(i=0; i<temp.length; i++) {
				if (temp[i]>'') {
					$('#systemAlerts').jGrowl( temp[i] , {header: 'System Alert', sticky: true } );
				}
			}
		});
	})(jQuery);		
	//set cookie with all alerts shown thus far
	//must use Path with "/" so cookie is found on all
	//files and sub-directories
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + 1);
	setCookie("myCM3-AlertsViewed", alertsViewed, exdate, "/");	
</script>
<!-- STOP JGrowl System Notifications -->


<!-- SIDE Panel help/directives -->
<% if helpID > 0 then %>
	<script type="text/javascript" src="../scripts/javascript/sidePanel.js"></script>
	<!--#include file="../_includes/_INCsidePanel_.asp"-->
<% end if %>


<!-- ADD New Issues/Reports/Follow-Ups script -->
<!--#include file="../scripts/javascript/addEvents.asp"-->


<table style="background:#3B5998" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
  
   	<!-- START Top-Left Box -->
    <td width="190" align="right" height="41">
    	<div style="float:left; width:44px; text-align:left;"><a href="#" onClick="initSlideLeftPanel(); return false;"><img src="../_images/help_header.png" title="Help F1" border="0"></a></div>
    	<div><a href="../default.asp"><img src="../_images/header.jpg" title="myCM Home" width="146" height="41" border="0"></a></div>
    </td>    
    
    <td valign="bottom">
    
    <!-- START Top-Middle Box -->
    <table id="simpleSearchBox" width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>

        <!-- START Search Box -->
        <td style="padding:3px; background:#627AAD; border-left: #1D4088 solid 1px; border-top: #1D4088 solid 1px;">                  
		  <% if showSearchBox = true then %>
          <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
            <tr>
		      	<form name="searchBox" method="post" action="../scripts/issues.asp">                
        	      <td nowrap="nowrap" style="width:100%;" valign="middle">					
                    <div style="border:1px solid #1D4088; background-color:#FFFBF0; display: inline-table; width: 326px;">
	                    <div style="float: left; margin-left: 1px; margin-top: 1px;">
                            <select id="showSEARCHfields" class="inputShort" name="showSEARCHfields">
                                <option value="crsid" <%=checkMatch(session(session("siteID") & "searchBoxfields"),"crsid")%>>Issue #</option>                            
                                <option value="all" <%=checkMatch(session(session("siteID") & "searchBoxfields"),"all")%>>All Fields</option>
                            </select>                    
                        </div>
	                    <div style="float: left;">                        
	                  		<input type="text" name="showSEARCH" id="showSEARCH" style="width:200px; height:16px; margin:0px; padding-left:2px; border:1px solid white;" value="<% =session(session("siteID") & "searchBox") %>"><img src="../_images/icons/16/search.jpg" height="16" width="16" border="0" align="absmiddle" title="Search Issues" style="padding-right:3px; cursor:pointer; padding-top: 1px;" onClick="document.forms['searchBox'].submit();" />
							<script>
                                jQuery(function($){ $("#showSEARCH").Watermark("Search Issues"); });
                            </script>
                        </div>
                    </div>
                  </td>
        		</form>
            </tr>
          </table>
          <%
		  else
		  	response.write("&nbsp;")
          end if
		  %>          
        </td>
       	<!-- END Search Box -->                  
        
        <!-- START DHTML TOP menu -->						
        <td align="right" style="background:#627AAD; border-top: #1D4088 solid 1px; border-right: #1D4088 solid 1px;">
		  	<% if showDHTMLMenu = true then %>	
				<!--#include file="../scripts/menu/menu.asp"-->
          	<% end if %>
        </td>
		<!-- END DHTML TOP menu -->					
            
      </tr>
    </table>
    <!-- END Top-Middle Box -->
    
    </td>
    <td width="2%">&nbsp;</td>
  </tr>
</table>





