        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Chart div -->
        <div id="tab:chart" style="display:none;">        
        
            <!-- START Chart table -->   
	        <table id="table_chart" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>
                    
                    <td valign="top">
                                        
					  	<!-- START Chart Form Fields -->	                    
						<table id="table:chart:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">

      						<tr>
                              <td class="formLabel"><span class="required">*</span>Style:</td>
                              <td align=left nowrap>
                              	<div>
                                  	<select name="optChart" id="optChart" size=1 style="width:200px;">
                                    <%
									Response.Write "<option value=""Area"" " & checkMatch(optChart,"Area") & ">Area</option>"
	                                Response.Write "<option value=""Bar"" " & checkMatch(optChart,"Bar") & ">Bar</option>"
									Response.Write "<option value=""Bar stacked"" " & checkMatch(optChart,"Bar stacked") & ">Bar stacked</option>"
									Response.Write "<option value=""Column"" " & checkMatch(optChart,"Column") & ">Column</option>"		
									Response.Write "<option value=""Line"" " & checkMatch(optChart,"Line") & ">Line</option>"	                                            
									%>
                                	</select>                                                                            
                                </div>
								<div class="subLabel" style="width:200px; margin-bottom:6px;">Type of chart used on your report</div>									
                              </td>
                            </tr>                            

      						<tr>
                              <td class="formLabel">Legend:</td>
                              <td align=left nowrap>
                              	<div>
                                  	<select name="optChartLegend" id="optChartLegend" size=1 style="width:200px;">
	                                    <option value="" selected="selected">-- No Legend --</option>
    	                                <%
										response.Write "<option value=""Top-Left"" " 		& checkMatch(optChartLegend,"Top-Left") 	& ">Top, Left</option>"
	        	                        response.Write "<option value=""Top-Center"" " 		& checkMatch(optChartLegend,"Top-Center") 	& ">Top, Center</option>"
										response.Write "<option value=""Top-Right"" " 		& checkMatch(optChartLegend,"Top-Right") 	& ">Top, Right</option>"
										response.Write "<option value=""Middle-Left"" " 	& checkMatch(optChartLegend,"Center-Left") 	& ">Middle, Left</option>"		
										response.Write "<option value=""Middle-Center"" " 	& checkMatch(optChartLegend,"Center-Center")& ">Middle, Center</option>"	                                            
										response.Write "<option value=""Middle-Right"" " 	& checkMatch(optChartLegend,"Center-Right") & ">Middle, Right</option>"
										response.Write "<option value=""Bottom-Left"" " 	& checkMatch(optChartLegend,"Bottom-Left") 	& ">Bottom, Left</option>"
										response.Write "<option value=""Bottom-Center"" " 	& checkMatch(optChartLegend,"Bottom-Center") & ">Bottom, Center</option>"
										response.Write "<option value=""Bottom-Right"" " 	& checkMatch(optChartLegend,"Bottom-Right") 	& ">Bottom, Right</option>"										
										%>
                                	</select>                                                                            
                                </div>
								<div class="subLabel" style="width:200px; margin-bottom:6px;">Placement of legend on chart (vertical, horizontal)</div>									
                              </td>
                            </tr>                            
                                          
						</table>
   						<!-- END Chart Form Fields -->

                    </td>
                                        
                </tr>
            
            </table>
            <!-- END Chart table -->
	
    	</div>
		<!-- STOP Chart div -->        


   		<script language="javascript">
			//hide/show caller related fields based on Anonynimity
			function ChartLegendVisible(objVisible) {						
				changeObjectDisplay('tr_urlField', objVisible);
			}
		</script>
