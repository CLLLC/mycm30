        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->       

		<!-- START Notes div, defaults to style="display:none;" -->
        <div id="tab:resolve" style="display:none;">        
        
            <!-- START Issue Notes table -->   
	        <table id="table_resolve" class="formTable" cellpadding="0" cellspacing="0" style="width:100%;">
                <tr>
                    <td align="center" valign="top" style="padding:0px; margin:0px;">
                    
						<!-- START Resolutions section -->                        
   						<table id="table:resolve:resolution" width="95%" class="formTable" cellpadding="0" cellspacing="0">                    
							<tr>            	
								<td align="center" style="border-top:none;">

                                    <div style="color:#35487B; width:100%; font-weight:bold; text-align:left; padding-bottom:5px;">Resolutions</div>

									<!-- EDITING ISSUE look for existing uploaded documentation -->                                    
									<%
									dim arrResolutionID
									dim resolutionCount : resolutionCount = 0
									
									'set alternating row colors
									dim rowColorResolution : rowColorResolution = col2
									
									if action = "edit" then									
										'-----------------------------------
										'LOOK for existing resolutions ALL even those not approved
										if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
											mySQL = "SELECT Resolution.ResolutionID, Resolution.CRSID, Resolution.ApprovedDate, Satisfaction, " _
												  & "Logins.FirstName + ' ' + Logins.LastName AS [ApprovedBy], " _
												  & "'Given' = CASE WHEN DateReadBy > 0 THEN 'Yes' ELSE 'No' END " _
												  & "	FROM Resolution LEFT JOIN Logins ON Resolution.ApprovedBy = Logins.LOGID " _											  
												  & "		WHERE CRSID Like '" & left(idIssue,len(idIssue)-3) & "%' " _
												  & "		ORDER BY ResolutionID "															  
										'only pull those that have been approved
										else
											mySQL = "SELECT Resolution.ResolutionID, Resolution.CRSID, Resolution.ApprovedDate, Satisfaction, " _
												  & "Logins.FirstName + ' ' + Logins.LastName AS [ApprovedBy], " _
												  & "'Given' = CASE WHEN DateReadBy > 0 THEN 'Yes' ELSE 'No' END " _
												  & "	FROM Resolution LEFT JOIN Logins ON Resolution.ApprovedBy = Logins.LOGID " _											  
												  & "		WHERE CRSID Like '" & left(idIssue,len(idIssue)-3) & "%' AND ApprovedBy>0 " _
												  & "		ORDER BY ResolutionID desc "
										end if
										'open recordset
										set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)      
										%>                                                
                                        
                                    	<!-- START Resolution table -->
                                    	<table id="resolutionTable" width="100%" style="background-color:#969696;" cellpadding=0 cellspacing=0>
                                                        
                                           <thead>
                                        		<tr>
                                                	<th align="left" nowrap="nowrap">&nbsp;</th>
                                             		<th align="left" nowrap="nowrap">Issue #</th>
                                                	<th align="left" nowrap="nowrap">Approved Date</th>
                                                 	<th align="left" nowrap="nowrap">Given to Reporter</th>
                                                  	<th align="left" nowrap="nowrap">Satisfaction</th>
                                            	</tr>
                                         	</thead>
                                      		<tbody>
                                
                                    		<%
                                      		do until rs.eof
                                                    
											'to hide messages if no records
											resolutionCount = resolutionCount + 1
													
												'START current row
												response.write("<tr id=""resolution:" & rs("resolutionid") & "_tr"">")
												
												'ICON actions
												response.Write("<td id=""resolution:" & rs("resolutionid") & "_delete"" align=""left"" style=""width:40px;"">")
													'only allow certain users to delete resolutions
													if cLng(session(session("siteID") & "adminLoggedOn")) > 3 then
														response.write("<a href=""#"" onclick=""this.blur(); jAlert('Permission denied.', 'myCM Alert'); return false;""><img src=""../_images/icons/16/document_quote_delete.png"" title=""Delete Resolution"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; padding-right:7px; vertical-align:middle;""></a>")
													'if provided No-one can delete
													elseif len(rs("ApprovedDate"))>0 and len(rs("Satisfaction"))>0 then 
														response.write("<a href=""#"" onclick=""this.blur(); jAlert('Permission denied.', 'myCM Alert'); return false;""><img src=""../_images/icons/16/document_quote_delete.png"" title=""Delete Resolution"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; padding-right:7px; vertical-align:middle;""></a>")
													else
														response.write("<a href=""#"" onClick=""delResolution(" & rs("resolutionid") & "); resetSystemMsg('systemMessage'); return false;""><img src=""../_images/icons/16/document_quote_delete.png"" title=""Delete Resolution"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; padding-right:7px; vertical-align:middle;""></a>")
													end if												
												response.write("<a href=""#"" onclick=""SimpleModal.open('../_dialogs/popup_config_resolution.asp?recid=" & rs("resolutionid") & "&cid=" & customerID & "&readcrsid=" & idIssue & "&action=edit', 475, 650, 'no'); StopTheTimer(); resetSystemMsg('systemMessage'); return false;""><img src=""../_images/icons/16/document_quote_edit.png"" title=""Open Resolution"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; vertical-align:middle;""></a>")												
												response.Write("</td>")
														
												'NAME link
												response.Write("<td id=""resolution:" & rs("resolutionid") & "_crsid"" align=""left"" style=""cursor:pointer;"" onclick=""SimpleModal.open('../_dialogs/popup_config_resolution.asp?recid=" & rs("resolutionid") & "&cid=" & customerID & "&readcrsid=" & idIssue & "&action=edit', 475, 650, 'no'); StopTheTimer(); resetSystemMsg('systemMessage'); return false;"" >")
												response.write("<a style=""color:#333333;"">" & rs("crsid") & "</a>")
												response.Write("</td>")
														
												response.Write("<td id=""resolution:" & rs("resolutionid") & "_date"" align=""left"">" & rs("ApprovedDate") & "</td>")												
												response.Write("<td id=""resolution:" & rs("resolutionid") & "_given"" align=""left"">" & rs("Given") & "</td>")
												response.Write("<td id=""resolution:" & rs("resolutionid") & "_satisfaction"" align=""left"">" & rs("Satisfaction") & "</td>")
														
												'CLOSE current row						
												response.write("</tr>")
                                                    
	                                          	rs.movenext
                                            loop
                                            %>					
                                
                                     		</tbody>
                                   		</table>
    
                                    	<%
                                      	dim tableResProperty
                                     	tableResProperty = "sort: true, sort_config:{sort_types:['String','String','String','String','String']}, filters_row_index: 1, " _
                                                      & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
                                                      & "status_bar: true, col_0: """", col_1: """", col_2: """", col_3: ""select"", col_4: ""select"", " _
                                                      & "col_width:[null,""40%"",null,null,null], paging: true, paging_length: 5, " _
                                                      & "enable_non_empty_option: true, or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, " _
                                                      & "highlight_keywords: true, " _
                                                      & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
                                                      & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"						  
                                        %>
                                        <!-- STOP Location table -->
                                    
                                        <!-- Fire table build -->
                                        <script language="javascript" type="text/javascript">
                                            //<![CDATA[
                                            var tableProp = {<% =tableResProperty %>};
                                            //initiate table setup
                                            var tfResolution = setFilterGrid("resolutionTable",tableProp);
                                            //]]>
                                        </script>
                                    
                                        <!-- Shadow table -->
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td height="5px" class="clearFormat" style="background:#D0D0D0; padding:0px; margin:0px;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
                                          </tr>
                                        </table>


										<!-- No Activities found  -->
                	                    <table id="NoResolutionTable" width="100%" cellpadding="0" cellspacing="0" style="margin-top:10px; <% if resolutionCount>0 then response.write("display:none;") %>">
                    	                  	<tr>
                        	                    <td align="left" class="clearFormat" style="padding-right:2px;">                                                                                                
                            	          	        <% call systemMessageBox("resolutionsID","statusMessageINFO","No resolutions found. Click ""Add"" below.") %>
                                	            </td>
                                    	    </tr>
                                        </table>												                                                                                      
            	                        <!-- STOP Resolutions table  -->

                                   		<!-- ADD button  -->
                                        <div style="float:left; padding-top:5px; width:125px;">                                        
                                        	<% if cLng(session(session("siteID") & "adminLoggedOn")) > 3 then %>
												<a class="myCMbutton" href="#" onclick="this.blur(); jAlert('Permission denied.', 'myCM Alert'); return false;"><span class="add" style="padding-right:7px;">Add</span></a>
                                            <% else %>
												<a class="myCMbutton" href="#" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_config_resolution.asp?recid=<% =idIssue %>&cid=<% =customerID %>&action=add', 475, 650, 'no'); StopTheTimer(); resetSystemMsg('systemMessage'); return false;"><span class="add" style="padding-right:7px;">Add</span></a>
                                     		<% end if%>                                        
                                        </div>                
                                        
                                        <% closeRS(rs) %>
	                                    
                                    <!-- ADDING new issue...resolutions not available yet -->
									<% else %>
                                    	<br>
										<table id="NoResolutionTable" width="100%" cellpadding="0" cellspacing="0">
											<tr>
												<td align="left" class="clearFormat" style="padding-right:2px;">                                                                                                
												<% call systemMessageBox("resolutionsID","statusMessageINFO","Resolutions will become available after saving and reloading issue.") %>
												</td>
											</tr>
										</table>
									<% end if %>

                          			<!-- FORM ELEMENT SAVING... -->
                             		<!-- These elements MUST be set to style="display:none;" and NOT type="hidden" -->
                                	<!-- when set as "hidden" FireFox does not recognize any change has been made  -->
                              		<input name="arrResolutionRemoved" id="arrResolutionRemoved" style="display:none;" value="" />
                                                                        
                                </td>    
							</tr>                            
                      	</table>
						<!-- STOP Resolutions section -->       
                                                         
                    </td>
                    <!-- START Page Details and Form Fields -->

                </tr>

            </table>
            <!-- STOP Notes table -->
	
    	</div>
        <!-- STOP Notes div -->
        
        <script>
			function addResolution(resolutionid,crsid,approved,given,satisfaction){
				
				var tempHTML;

				// create randon numbers used to create unique element IDs for idQueryValue form fields
				var valueID_1 = Math.floor(Math.random()*1001);
				  
				// create table body
				var table = document.getElementById("resolutionTable");
				var tbody = table.getElementsByTagName("tbody")[0];
				
				// ----- ADD ROW -----
				// create row element and assign unique ID
				var rowCount = table.rows.length;
				var rowID = "resolution:" + resolutionid + "_tr";				
				var row = document.createElement("tr");
				row.id = rowID;

				// ----- ADD CELL #1 -----	
				var td1 = document.createElement("td")					
				td1.id = "resolution:" + resolutionid + "_delete";
				td1.style.textAlign = "left";			
				td1.style.width = "40px";
				tempHTML = "<a href=\"#\" onClick=\"delResolution(" + resolutionid + "); resetSystemMsg(\'systemMessage\'); return false;\"><img src=\"../_images/icons/16/document_quote_delete.png\" title=\"Delete Resolution\" border=\"0\" width=\"16\" height=\"16\" align=\"absmiddle\" style=\"padding-top:1px; padding-bottom:1px; padding-right:7px; vertical-align:middle;\"></a>";
				tempHTML += "<a href=\"#\" onclick=\"SimpleModal.open(\'../_dialogs/popup_config_resolution.asp?recid=" + resolutionid + "&cid=<% =customerID %>&readcrsid=" + crsid + "&action=edit\', 475, 650, \'no\'); StopTheTimer(); resetSystemMsg(\'systemMessage\'); return false;\"><img src=\"../_images/icons/16/document_quote_edit.png\" title=\"Open Resolution\" border=\"0\" width=\"16\" height=\"16\" align=\"absmiddle\" style=\"padding-top:1px; padding-bottom:1px; vertical-align:middle;\"></a>";
				td1.innerHTML = tempHTML;				

				// ----- ADD CELL #2 -----	
				var td2 = document.createElement("td")					
				td2.id = "resolution:" + resolutionid + "_crsid";
				td2.style.textAlign = "left";			
				td2.style.cursor = "pointer";
				tempHTML = "<div style=\"width:100%;\" onclick=\"SimpleModal.open(\'../_dialogs/popup_config_resolution.asp?recid=" + resolutionid + "&cid=<% =customerID %>&readcrsid=" + crsid + "&action=edit\', 475, 650, \'no\'); StopTheTimer(); resetSystemMsg(\'systemMessage\'); return false;\">";
				tempHTML += "<a style=\"color:#333333;\">" + crsid + "</a>";
				tempHTML += "</div>";
				td2.innerHTML = tempHTML;				

				// ----- ADD CELL #3 -----
				var td3 = document.createElement("td")			
				td3.id = "resolution:" + resolutionid + "_date";				
				td3.style.textAlign = "left";
				td3.innerHTML = approved;
				
				// ----- ADD CELL #4 -----	
				var td4 = document.createElement("td")			
				td4.id = "resolution:" + resolutionid + "_given";				
				td4.style.textAlign = "left";				
				td4.innerHTML = given;			

				// ----- ADD CELL #5 -----	
				var td5 = document.createElement("td")			
				td5.id = "resolution:" + resolutionid + "_satisfaction";				
				td5.style.textAlign = "left";				
				tempHTML = satisfaction;
				td5.innerHTML = satisfaction;							

				// ----- FINILIZE AND APPEND NEW CELLS AND ROW -----
				row.appendChild(td1);				
				row.appendChild(td2);
				row.appendChild(td3);
				row.appendChild(td4);
				row.appendChild(td5);
						
				// append row to table
				tbody.appendChild(row);
							
				// hide "no documents" message if exists
				if (document.getElementById('NoResolutionTable')) {
					document.getElementById('NoResolutionTable').style.display = "none";				
				}
				//make sure table is visible
				if (document.getElementById('resolutionTable').style.display == "none") {
					document.getElementById('resolutionTable').style.display = "";
				}
				//show save message
				//if (document.getElementById('systemMessage')) {
				//	document.getElementById('systemMessage').style.display = "";
				//}
				
				//refresh grid
				tfResolution.RefreshGrid();
			}		

			function saveResolution(resolutionid,approved,given,satisfaction){
				var tempHTML;				
				var td3 = document.getElementById("resolution:" + resolutionid + "_date");
				var td4 = document.getElementById("resolution:" + resolutionid + "_given");
				var td5 = document.getElementById("resolution:" + resolutionid + "_satisfaction");								
				//update existing columns
				td3.innerHTML = approved;
				td4.innerHTML = given;			
				td5.innerHTML = satisfaction;											
			}

			function delResolution(resolutionid) {
				var theRow = document.getElementById("resolution:" + resolutionid + "_tr");
				var theTable = document.getElementById("resolutionTable");		
				var deleteElement = document.getElementById("arrResolutionRemoved");
				
				jConfirm('This action cannot be undone.<br/><br/>Continue with deleting this resolution?', 'Confirm Delete', function(r) {
					//user agrees, remove categories
					if (r==true) {
						var rowIndex = theRow.parentNode.parentNode.rowIndex;						
						//move through all rows and find the one to delete
						for (var i = 0; i < theTable.rows.length; i++) {
							if (theRow == theTable.rows[i]) {
								//this is the only way that works deleting rows
								//from a tableFilter table
								theTable.deleteRow(i);
								tfResolution.nbRows--;			//necessary for tableFitler to work
								tfResolution.RefreshGrid();	//necessary for tableFitler to work
								deleteElement.value += resolutionid + ","	//used for deleting subject on Ajax form save
							}
						}								
						//show NO SUBJECTS table
						if (theTable.rows.length<=2) {
							document.getElementById('NoResolutionTable').style.display = "";
						}											
					}
					//user disagrees
					else {
						return false;
					}
				});						
			}

		</script>
        
        
        
        
        
        
        
        