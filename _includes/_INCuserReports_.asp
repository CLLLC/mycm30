        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->       

		<!-- START Settings div -->
        <div id="tab:reports">
                
            <!-- START User Reports table -->   
	        <table id="table_reports" class="formTable" cellpadding="0" cellspacing="0" style="width:100%;">
                <tr>
                                
	                <!-- START Middle Section --> 
                    <td align="center" valign="top" style="padding:0px; margin:0px;">
                    
						<!-- START Reports section -->                        
   						<table id="table:reports:userreports" width="95%" class="formTable" cellpadding="0" cellspacing="0">                    
							<tr>            	
								<td align="center" style="border-top:none;">
                                                                        	
                                    <div style="color:#35487B; width:100%; font-weight:bold; text-align:left; padding-bottom:5px;">Reports</div>

									<%
									dim reportsCount
									
                                    mySQL = "SELECT Reports.Name, Reports.Category, Dataset.Name AS Dataset, Dataset.Type, Reports.IssueList, Convert(varchar, LastRunDate, 101) AS [Last Ran], Reports.FileName, ReportID " _
                                          & "   FROM Reports INNER JOIN Dataset ON (Reports.DatasetID = Dataset.DatasetID) " _
                                          & "   WHERE (LOGID=" & userLogID & ") AND (Reports.CustomerID = '" & customerID & "') " _
                                          & "   ORDER BY Reports.Name ASC "			
							        set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
									
									reportsCount = rs.recordcount
									%>
									
                                    <!-- START Reports table -->
                                    <table id="reportTable" width="100%" style="background-color:#969696;" cellpadding=0 cellspacing=0>
                                                
                                        <thead>
                                            <tr>
                                                <th align="left" nowrap="nowrap">&nbsp;</th>
                                                <th align="left" nowrap="nowrap">Name</th>
                                                <th align="left" nowrap="nowrap">Category</th>                                
                                                <th align="left" nowrap="nowrap">Dataset</th>
                                                <th align="left" nowrap="nowrap">Issue List</th>                                
                                                <th align="left" nowrap="nowrap">Last Ran</th>                                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                        
                                        <%
                                        do until rs.eof
                                            
                                            response.write("<tr>")
                        
                                            'ICONS delete/print
                                            response.Write("<td width=""58"" nowrap=""nowrap"" align=""left"">")								
                                            if lCase(rs("type")) = "crystal" then

												response.Write("<a href=""#"" onClick=""jAlert('Function not allowed.<br/><br/>You must first login (ghost) as user to run this report.', 'myCM Alert'); return false;"" ><img src=""../_images/icons/16/file_extension_pdf.png"" title=""View PDF"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; vertical-align:middle; margin-right:5px;"" border=""0"" /></a>")
                                                response.Write("<a href=""#"" onClick=""jAlert('Function not allowed.<br/><br/>You must first login (ghost) as user to run this report.', 'myCM Alert'); return false;"" ><img src=""../_images/icons/16/file_extension_xls.png"" title=""Download CSV"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle; margin-right:5px;"" border=""0"" /></a>")
                                                
												'ORIGINAL...put back once you figure out how to pass the customerid being ran against
												'response.Write("<a href=""#"" onClick=""runCrystal('PDF','" & Server.HtmlEncode(rs("filename")) & "'); return false;"" ><img src=""../_images/icons/16/file_extension_pdf.png"" title=""View PDF"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; vertical-align:middle; margin-right:5px;"" border=""0"" /></a>")
                                                'response.Write("<a href=""#"" onClick=""runCrystal('ExcelData','" & Server.HtmlEncode(rs("filename")) & "'); return false;"" ><img src=""../_images/icons/16/file_extension_xls.png"" title=""Download CSV"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle; margin-right:5px;"" border=""0"" /></a>")			
                                                
                                                'only allow the deleting of a report if ADMIN
												if cLng(session(session("siteID") & "adminLoggedOn")) <= 10 then
                                                    response.Write("<a href=""#"" onClick=""confirmPrompt('Confirm Delete','This action can not be undone.<br><br>Are you sure you want to delete this report?','redirect', '../reports/reports_exec.asp?action=del&recID=" & rs("reportid") & "&top=reports&return=user'); return false;"" ><img src=""../_images/icons/16/cancel.png"" title=""Delete Report"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle;"" border=""0"" /></a>")
                                                else
                                                    response.Write("<img src=""../_images/icons/16/cancel_disabled.png"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle;"" border=""0"" />")									
                                                end if									
                                            
                                            elseif lCase(rs("type")) = "sql" then
                                                response.Write("<a href=""#"" onClick=""javascript:popup('../reports/reports_print.asp?pageview=" & rs("reportid") & "',600,600); return false;"" ><img src=""../_images/icons/16/database_lightning.png"" title=""Execute"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; vertical-align:middle; margin-right:5px;"" border=""0"" /></a>")						
                                                response.Write("<a href=""../reports/reports_export.asp?pageview=" & rs("reportid") & "&action=csv"" ><img src=""../_images/icons/16/file_extension_xls.png"" title=""Download CSV"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle; margin-right:5px;"" border=""0"" /></a>")																
                                                response.Write("<a href=""#"" onClick=""confirmPrompt('Confirm Delete','This action can not be undone.<br><br>Are you sure you want to delete this report?','redirect', '../reports/reports_exec.asp?action=del&recID=" & rs("reportid") & "&top=reports&return=user'); return false;"" ><img src=""../_images/icons/16/cancel.png"" title=""Delete Report"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle;"" border=""0"" /></a>")					
                                            else
                                                response.Write("<a href=""#"" onClick=""javascript:popup('../reports/reports_print.asp?pageview=" & rs("reportid") & "',600,600); return false;"" ><img src=""../_images/icons/16/document_inspector.png"" title=""Print Preview"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; vertical-align:middle; margin-right:5px;"" border=""0"" /></a>")						
                                                response.Write("<a href=""../reports/reports_export.asp?pageview=" & rs("reportid") & "&action=csv"" ><img src=""../_images/icons/16/file_extension_xls.png"" title=""Download CSV"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle; margin-right:5px;"" border=""0"" /></a>")																
                                                response.Write("<a href=""#"" onClick=""confirmPrompt('Confirm Delete','This action can not be undone.<br><br>Are you sure you want to delete this report?','redirect', '../reports/reports_exec.asp?action=del&recID=" & rs("reportid") & "&top=reports&return=user'); return false;"" ><img src=""../_images/icons/16/cancel.png"" title=""Delete Report"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle;"" border=""0"" /></a>")					
                                            end if
                                            response.Write("</td>")
                        
                                            'NAME and edit icon link
                                            if lCase(rs("type")) = "crystal" then
                                                response.write("<td align=""left"" nowrap=""nowrap"" style=""cursor:pointer;"" onClick=""window.location='../reports/crystal/execute.asp?type=edit&recID=" & rs("reportid") & "&cid=" & customerID & "&name=" & Server.URLEncode(rs("filename")) & "&return=user'"">")
                                                response.write("<img style=""margin-right:5px; vertical-align:middle;"" src=""../_images/icons/16/page_blue_edit.png"" title=""Open Item"" border=""0"" align=""absmiddle"">")
                                                response.write("<a style=""color:#333333;"">" & rs("name") & "</a>")
                                                response.write("</td>")
                                            else
                                                response.Write("<td align=""left"" style=""cursor:pointer;"" onClick=""window.location='../reports/reports_edit.asp?action=edit&recID=" & rs("reportid") & "&return=user'"">")
                                                response.Write("<img style=""margin-right:5px; vertical-align:middle;"" src=""../_images/icons/16/page_blue_edit.png"" title=""Open Item"" border=""0"" align=""absmiddle"">")
                                                response.write("<a style=""color:#333333;"">" & rs("name") & "</a>")
                                                response.Write("</td>")
                                            end if
                        
                                            response.Write("<td align=""left"">" & rs("category") & "</td>")
                                            response.Write("<td align=""left"">" & rs("dataset") & "</td>")
                                            response.Write("<td align=""left"">" & rs("issuelist") & "</td>")
                                            response.Write("<td align=""left"">" & rs("last ran") & "</td>")						
                        
                                            'CLOSE current row						
                                            response.write("</tr>")
                                            
                                            rs.movenext
                                        loop
                                        %>					
                        
                                        </tbody>
                                    </table>
                                    <!-- STOP Reports table -->
                        
                                    <%
                                    tableProperty = "sort: true, sort_config:{sort_types:['String','String','String','String','String','date']}, filters_row_index: 1, " _
                                                  & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
                                                  & "status_bar: true, col_0: """", col_1: ""input"", col_2: ""select"", col_3: ""select"", col_4: ""select"", col_5: ""none"", " _
                                                  & "col_width:[null,""40%"",null,null,null,null], paging: true, paging_length: 10, " _
                                                  & "enable_non_empty_option: true, or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, " _
                                                  & "highlight_keywords: true, " _
                                                  & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
                                                  & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"			
                                    %>
                                    <!-- STOP Reports table -->
                        
                                    <!-- Fire table build -->
                                    <script language="javascript" type="text/javascript">
                                        //<![CDATA[
                                        var tableProp = {<% =tableProperty %>};
                                        //initiate table setup
                                        var tf1 = setFilterGrid("reportTable",tableProp);
                                        //]]>
                                    </script>
                        
									
                                        <!-- Shadow table -->
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td height="5px" class="clearFormat" style="background:#D0D0D0; padding:0px; margin:0px;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
                                          </tr>
                                        </table>



										<!-- No Activities found  -->
                	                    <table id="NoReportsTable" width="100%" cellpadding="0" cellspacing="0" style="margin-top:10px; <% if reportsCount>0 then response.write("display:none;") %>">
                    	                  	<tr>
                        	                    <td align="left" class="clearFormat" style="padding-right:2px;">                                                                                                
                            	          	        <% call systemMessageBox("resolutionsID","statusMessageINFO","No reports found. Click ""Add"" below.") %>
                                	            </td>
                                    	    </tr>
                                        </table>												                                                                                      
            	                        <!-- STOP Resolutions table  -->

                                   		<!-- ADD button  -->
                                        <div style="float:left; padding-top:5px;">                                        
											<a class="myCMbutton" href="#" onclick="this.blur(); SimpleModal.open('../datasets/popup_default.asp?pageview=datasets&security=<% =securityLevel %>&cid=<% =customerID %>&action=add', 525, 700, 'no'); resetSystemMsg('systemMessage'); return false;"><span class="add" style="padding-right:10px;">Add</span></a>
											<a class="myCMbutton" href="#" onClick="this.blur(); resetSystemMsg('systemMessage'); submitForm('loadreports'); return false;"><span class="setup" style="padding-right:5px;">Load Set</span></a>
                                        </div>                
                                        
                                                                        
                                </td>    
							</tr>                            
                      	</table>
						<!-- STOP Reports section -->       
                                                         
                    </td>
                </tr>

            </table>
            <!-- STOP Reports table -->
	
    	</div>
        <!-- STOP Notes div -->
        
		<script>
			function addReport(dataset) {				
				window.location="../datasets/default.asp?s=2&d=" + dataset + "&cid=<% =customerID %>&u=<% =userLogID %>&return=user"  ;
			}		
		</script>
        
        
        
        