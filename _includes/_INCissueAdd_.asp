        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Details div -->
        <div id="tab:details">
        
        
            <!-- START Issue Details table -->   
	        <table id="table_details" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>
                    
                    <!-- START Middle Section -->    
                    <td valign="top">

						<!-- START BEGIN section -->                
                        <input type="hidden" id="title:begin" value="Opening Instructions">                       
   						<table id="table:begin" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">
                            <tr style="background-color:#FFFFC8;">
                              <td align=left style="padding:15px;">        
                              	<% =dirBegin %>                      
                              </td>
                            </tr>
                      	</table>
						<!-- STOP BEGIN section -->    
                        

						<!-- START Interpreter/origination section -->     
                        <input type="hidden" id="title:origination" value="Is an interpreter required?">                           
   						<table id="table:origination" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="border:none; padding:0px; margin:0px;">                        			
                                    <table width="100%" cellpadding="0" cellspacing="0">            
                                        <tr>
                                          <td class="formLabel">Interpreter Used:</td>
                                          <td align=left nowrap>                              
                                            <label>
                                              <input type="radio" name="interpreterusedOpt" id="interpreterusedYes" onclick="changeObjectDisplay('tr_language',''); changeObjectDisplay('tr_langoperator',''); changeObjectDisplay('tr_langhelp',''); document.getElementById('interpreterUsed').value='-1';" value="-1" style="vertical-align:middle;" <%=checkRadio(interpreterused,"True")%>>Yes&nbsp;&nbsp;&nbsp;&nbsp;
                                            </label>							    
                                            <label>
                                              <input type="radio" name="interpreterusedOpt" id="interpreterusedNo" onclick="changeObjectDisplay('tr_language','none'); changeObjectDisplay('tr_langoperator','none'); changeObjectDisplay('tr_langhelp','none'); document.getElementById('interpreterUsed').value='0';" value="0" style="vertical-align:middle;" <%=checkRadio(interpreterused,"True")%>>No
                                            </label>
                                            <div class="subLabel" style="padding-top:5px;">Signifies if an interpreter used during the intake of this issue.</div>                                                                                                                                                                                        
                                          </td>
                                        </tr>                        
                                    </table>                        
                                </td>
                            </tr>

       						<%
                         	mySQL = "SELECT Notes " _
                        		& "	FROM Directive " _
                           		& "	WHERE CustomerID='" & customerID & "' AND Event = 'Interpreter Required' " _
                             	& "	ORDER BY SortOrder "
                          	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                          	%>
                            <tr>
                                <td style="border:none; padding:0px; margin:0px;">               		
                                    <table width="100%" cellpadding="0" cellspacing="0">            
                                        <tr id="tr_langhelp" style="display:none; background-color:#FFFFC8;">
                                            <td align="left" style="padding:15px;">                                	 
												<% if not rs.eof then response.write(rs("notes")) %>
                                            </td>
                                        </tr>
                                    </table>                        
                                </td>
                            </tr>

                            <tr>
                                <td style="border:none; padding:0px; margin:0px;">               
                                    <table width="100%" cellpadding="0" cellspacing="0">            
                                        <tr id="tr_language" style="display:none;">
                                            <td class="formLabel">Language:</td>
                                            <td align=left nowrap>
                                                <%
                                                'Build array for dropdown selections
                                                optArr = getDropDownConfig("pLanguages")
                                                call buildSelect(optArr,language,"language","inputMediumLong","Y")
                                                %>
                                                <div class="subLabel" style="padding-top:5px;">Language used by reporter.</div>                                      
                                          </td>
                                        </tr>            
                                        <tr id="tr_langoperator" style="display:none;">
                                            <td class="formLabel">Interpreter #:</td>
                                            <td align=left nowrap>
                                                <input name="interpreterNumber" class="inputMedium" type=text value="" maxlength=50>
                                                <div class="subLabel" style="padding-top:5px;">Interpreter's operator number.</div> 
                                            </td>
                                        </tr>
                                    </table>                        
                                </td>
                            </tr>                            
                      	</table>
						<!-- STOP Origination section -->       


						<!-- START Awareness section XXX -->
                        <input type="hidden" id="title:awareness" value="How did you become aware of this helpline?">         
   						<table id="table:awareness" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">                    												

							<% if sLogid=4255 or sLogid=309613 or sLogid=309633 or sLogid=300022 or sLogid=4259 or sLogid=4221 then %>
                                <tr style="background-color: #ffffc8;">                            
                                  <td class="formLabel"><span class="required">*</span>Confirm ID:</td>
                                  <td align=left nowrap>                              
                                    <input name="customerIDconfirm" id="customerIDconfirm" class="inputShort" value="" maxlength="255" style="text-transform:uppercase;" />
                                    <div class="subLabel" style="padding-top:5px;">Confirm Customer ID as displayed within Vertical.</div>
                                  </td>
                                </tr>    
                            <% else %>
                                <input type="hidden" name="customerIDconfirm" id="customerIDconfirm" value="<%=customerID%>">
                            <% end if %>

						    <tr>
                                <td class="formLabel">Awareness:</td>
                                <td align=left>
                                    <%
                                    'Build array for dropdown selections
                                    optArr = getDropDownOpt("pCommTool",customerID)
									call buildSelect(optArr,commTool,"commTool","inputMediumLong","Y")
                                    %>                                    
	                                <div class="subLabel" style="padding-top:5px;">How reporter discovered toll-free number or reporting process.</div>
                                </td>
                            </tr>
                            <tr>
                              <td class="formLabel">First Time:</td>
                              <td align=left nowrap>                              
                   				<label>
							      <input type="radio" name="firstTimeOpt" id="firstTimeYes" onclick="document.getElementById('firstTime').value='-1';" value="-1" style="vertical-align:middle;" <%=checkRadio(firstTime,"True")%>>Yes&nbsp;&nbsp;&nbsp;&nbsp;
                                </label>							    
							    <label>
							      <input type="radio" name="firstTimeOpt" id="firstTimeNo" onclick="document.getElementById('firstTime').value='0';" value="0" style="vertical-align:middle;" <%=checkRadio(firstTime,"False")%>>No
                                </label>
                                <div class="subLabel" style="padding-top:5px;">Signifies if this is the first issue ever filed by reporter.</div>                                    
                                                            
                              </td>
                            </tr>
                      	</table>
						<!-- STOP Awareness section -->       


						<!-- START Issue Type section -->     
                        <input type="hidden" id="title:issuetype" value="Compliance or Request for Information?">                   
   						<table id="table:issuetype" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="border:none; padding:0px; margin:0px;">                        			
                                    <table width="100%" cellpadding="0" cellspacing="0">            
                                        <tr>
                                          <td class="formLabel"><span class="required">*</span>Issue Type:</td>
                                          <td align=left nowrap>                              
                                            <label>
                                              <input type="radio" name="issueTypeID" id="issueTypeIDCRS" onclick="document.getElementById('issueType').value = '1';" value="1" style="vertical-align:middle;" <%=checkRadio(issueType,"1")%>>Compliance Report&nbsp;&nbsp;&nbsp;&nbsp;
                                            </label>							    
                                            <label>
                                              <input type="radio" name="issueTypeID" id="issueTypeIDRFI" onclick="document.getElementById('issueType').value = '2';" value="2" style="vertical-align:middle;" <%=checkRadio(issueType,"2")%>>Request for Information
                                            </label>           							                      
                                         </td>
                                        </tr>
									</table>
                                </td>
                            </tr>
                            <tr>
                                <td style="border:none; padding:0px; margin:0px;">                        			
                                    <table width="100%" cellpadding="0" cellspacing="0">                                        
                                        <tr style="background-color:#FFFFC8;">
                                            <td colspan="2" align="left" style="padding:15px; border-bottom:1px dotted #cccccc;">
                                                <p style="text-align: center;">
                                                	<span style="color: #ff0000; font-size: medium;"><strong>STOP</strong></span>
                                                    <span style="color: #ff0000; font-size: small;">and decide which type of report is being filed.</span>
                                                    <span style="color: #ff0000; font-size: small;"><br/>This will effect categories, custom questions and issue number assigned.</span>                                                    
                                                </p>                                                    
                                             </td>
                                        </tr>
									</table>
                                </td>
                            </tr>
                      	</table>
						<!-- STOP Issue Type section -->       


						<!-- START Anonymous section -->     
                        <input type="hidden" id="title:anonymous" value="Does caller want to remain anonymous?">                   
   						<table id="table:anonymous" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="border:none; padding:0px; margin:0px;">                        			
                                    <table width="100%" cellpadding="0" cellspacing="0">            
                                        <tr>
                                          <td class="formLabel"><span class="required">*</span>Identity:</td>
                                          <td align=left nowrap>                              
                                            <label>
                                              <input type="radio" name="anonCallerOpt" id="anonCallerOptYes" onclick="callerVisible('none'); changeObjectDisplay('tr_callerDetail','none'); changeObjectDisplay('tr_identAnonymous',''); changeObjectDisplay('tr_identIdentified','none'); document.getElementById('anonCaller').value='-1';" value="-1" style="vertical-align:middle;" <%=checkRadio(anonCaller,"True")%>>Anonymous&nbsp;&nbsp;&nbsp;&nbsp;
                                            </label>							    
                                            <label>
                                              <input type="radio" name="anonCallerOpt" id="anonCallerOptNo" onclick="callerVisible(''); changeObjectDisplay('tr_callerDetail',''); changeObjectDisplay('tr_identAnonymous','none'); changeObjectDisplay('tr_identIdentified',''); document.getElementById('anonCaller').value='0';" value="0" style="vertical-align:middle;" <%=checkRadio(anonCaller,"False")%>>Identified
                                            </label>           							                      
                                         </td>
                                        </tr>
									</table>
                                </td>
                            </tr>
									
            				<%
							mySQL = "SELECT Notes " _
								  & "	FROM Directive " _
								  & "	WHERE CustomerID='" & customerID & "' AND Event = 'Caller: Anonymous' " _
							      & "	ORDER BY SortOrder "
							set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
							%>
                            <tr>
                                <td style="border:none; padding:0px; margin:0px;">                        			
                                    <table width="100%" cellpadding="0" cellspacing="0">                                        
                                        <tr id="tr_identAnonymous" style="display:none; background-color:#FFFFC8;">
                                            <td align="left" style="padding:15px;">                                
                                                <% if not rs.eof then response.write(rs("notes")) %>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

            				<%
							mySQL = "SELECT Notes " _
								  & "	FROM Directive " _
								  & "	WHERE CustomerID='" & customerID & "' AND Event = 'Caller: Identified' " _
							      & "	ORDER BY SortOrder "
							set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
							%>
                            <tr>
                                <td style="border:none; padding:0px; margin:0px;">                        			
                                    <table width="100%" cellpadding="0" cellspacing="0">                                                                                
                                        <tr id="tr_identIdentified" style="display:none; background-color:#FFFFC8;">
                                            <td align="left" style="padding:15px;">
                                                <% if not rs.eof then response.write(rs("notes")) %>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                                        
                            <tr id="tr_callerDetail" <% if trim(anonCaller)="" or len(anonCaller)<=0 then response.write("style=""display:none;""") %>>
                                <td style="border:none; padding:0px; margin:0px;">                        			
                                    <table width="100%" cellpadding="0" cellspacing="0">                                                                                                    												                                                                   
                                        <tr id="tr_callerName" <% if anonCaller="True" then response.write("style=""display:none;""") %>>
                                          <td class="formLabel">Name:</td>
                                          <td align=left nowrap>                              
                                            <div style="float:left;">
                                                <div class="subLabel">First Name:</div>
                                                <div><input name="callerFirst" id="callerFirst" class="inputMedium" value="<%=callerFirst%>" maxlength="20" /></div>
                                            </div>                                
                                            <div style="float:left;">
                                                <div class="subLabel">Last Name:</div>
                                                <div>
                                                    <div style="float:left;">
                                                        <input name="callerLast" id="callerLast" class="inputMedium" value="<%=callerLast%>" maxlength="20" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="padding-top:15px;">
	                                            <a class="myCMbuttonSmall" href="#" title="Caller LookUp" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_caller.asp?cid=<% =customerID %>', 525, 700, 'no');"><span class="user" style="padding-right:7px;">&nbsp;</span></a>
                                            </div>
                                          </td>
                                        </tr>
                                        
                                        <tr>
                                            <td class="formLabel">Relationship:</td>
                                            <td align=left nowrap>
                                                <%
                                                'Build array for dropdown selections
                                                optArr = getDropDownOpt("pCallerType",customerID)
                                                call buildSelect(optArr,callerType,"callerType","inputMediumLong","Y")
                                                %>
                                                <div class="subLabel" style="padding-top:5px;">Reporter's relationship to the organization.</div>                                      
                                          </td>
                                        </tr>
                                        
                                        <tr id="tr_callerTitle" <% if anonCaller = "True" then response.write("style=""display:none;""") %>>
                                            <td class="formLabel">Job Title:</td>
                                            <td align=left nowrap><input name="callerTitle" id="callerTitle" class="inputLong" type=text value="<%=callerTitle%>" maxlength=50></td>
                                        </tr>
                                        
                                        <tr id="tr_callerAddress" <% if anonCaller = "True" then response.write("style=""display:none;""") %>>
                                          <td class="formLabel">Address:</td>
                                          <td align=left nowrap>
                                          
                                            <div class="subLabel">Street Address:</div>
                                            <div>
                                                <textarea name="callerAddr" id="callerAddr" style="width:300px; height:50px;"><%=callerAddr%></textarea>
                                            </div>

                                            <div style="float:left;">
                                                <div class="subLabel">City:</div>
                                                <input name="callerCity" id="callerCity" class="inputShort" value="<%=callerCity%>" maxlength="255" />
                                            </div>
                                            
                                            <div style="float:left;">
                                                <div class="subLabel">State/Province:</div>
                                                <input name="callerState" id="callerState" class="inputShort" value="<%=callerState%>" maxlength="255" />
                                            </div>
                                            
                                            <div>                                
                                                <div class="subLabel">Postal Code:</div>
                                                <input name="callerZip" id="callerZip" class="inputShort" value="<%=callerZip%>" maxlength="255" />
                                            </div>
            
                                          </td>
                                        </tr>
                                                                    
                                        <tr id="tr_callerPhone" <% if anonCaller = "True" then response.write("style=""display:none;""") %>>
                                          <td class="formLabel">Phone Number(s):</td>
                                          <td align=left nowrap>
                                                                          
                                            <div style="float:left;">
	                                            <div class="subLabel">Home Phone:</div>
                                                <input name="callerHome" id="callerHome" class="inputShort" value="<%=callerHome%>" maxlength="255" />
                                            </div>
                                            
                                            <div style="float:left;">
	                                            <div class="subLabel">Work Phone:</div>
                                                <input name="callerWork" id="callerWork" class="inputShort" value="<%=callerWork%>" maxlength="255" />
                                            </div>                                                                
                                            
                                            <% if appDeliverySystem <> "CLive" then %>
                                                <div class="subLabel">
                                                    <div>Mobile Phone:</div>
                                                    <input name="callerCell" class="inputShort" value="<%=callerCell%>" maxlength="255" />
                                                </div>
                                                
                                                <div style="float:left;">
                                                    <div class="subLabel">Fax:</div>
                                                    <input name="callerFax" class="inputShort" value="<%=callerFax%>" maxlength="255" />
                                                </div>
                                                
                                                <div style="float:left;">
                                                    <div class="subLabel">Pager:</div>
                                                    <input name="callerPager" class="inputShort" value="<%=callerPager%>" maxlength="255" />
                                                </div>
                                                                                                                
                                                <div>
                                                    <div class="subLabel">Other Phone:</div>
                                                    <input name="callerOther" class="inputShort" value="<%=callerOther%>" maxlength="255" />
                                                </div>                                          
                                            <% end if %>
                                          
                                          </td>
                                        </tr>
                                        
                                        <tr id="tr_callerEmail" <% if anonCaller = "True" then response.write("style=""display:none;""") %>>
                                            <td class="formLabel">E-Mail:</td>
                                            <td align=left nowrap><input name=callerEmail class="inputLong" type=text value="<%=callerEmail%>" size=50 maxlength=50></td>
                                        </tr>                                                 
                                    </table>
                                </td>
                            </tr>                                                        
                      	</table>
						<!-- STOP Anonymous section -->       


						<!-- START Location section -->    
                        <input type="hidden" id="title:location" value="Where did the issue or incident occur?">                    
   						<table id="table:location" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">                    												
                            <tr>
                                <td style="border:none; padding:0px; margin:0px;">                        			
                                    <table width="100%" cellpadding="0" cellspacing="0">                                        
                                        <tr>
                                          <td class="formLabel" nowrap="nowrap">                              	
                                            Issue Location:
                                          </td>
                                          <td align=left nowrap style="padding-bottom:0px;">                              
                                            <div>
                                                <div style="float:left;">
                                                	<input name="locNameID" id="locNameID" value="<%=locNameID%>" type="hidden" />
                                                	<input name="locName" id="locName" class="inputLong" value="<%=locName%>" maxlength="255" />
                                                </div>
                                                <div><a class="myCMbutton" href="#" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_location.asp?cid=<% =customerID %>', 525, 700, 'no'); resetSystemMsg('systemMessage'); return false;"><span class="search" style="padding-right:10px;">Look up</span></a></div>
                                            </div>							  
                                          </td>
                                        </tr>
									</table>
                                </td>
                            </tr>

                            <tr>
                                <td style="border:none; padding:0px; margin:0px;">                        			
                                    <table width="100%" cellpadding="0" cellspacing="0">                                        
                                        <tr id="tr_locationHelp" style="display:none; background-color:#FFFFC8;">
                                            <td id="locDirective" colspan="2" align="left" style="padding:15px; border-bottom:1px dotted #cccccc;">
                                                No special instructions or directive available.
                                             </td>
                                        </tr>
									</table>
                                </td>
                            </tr>
                        
                            <tr>
                                <td style="border:none; padding:0px; margin:0px;">                        			
                                    <table width="100%" cellpadding="0" cellspacing="0">                                                                    
                                        <tr id="tr_Location">                            
                                          <td class="formLabel" style="border-top:none;" nowrap="nowrap">                     
                                          <td align=left nowrap style="border-top:none; padding-top:0px;">                               
                                            <div class="subLabel">Street Address:</div>
                                            <div>
                                                <textarea name="locAddr" id="locAddr" style="width:300px; height:50px;"><%=locAddr%></textarea>
                                            </div>                               
                                            <div style="float:left;">
                                                <div class="subLabel">City:</div>
                                                <input name="locCity" id="locCity" class="inputShort" value="<%=locCity%>" maxlength="255" />
                                            </div>                                
                                            <div style="float:left;">
                                                <div class="subLabel">State/Province:</div>
                                                <input name="locState" id="locState" class="inputShort" value="<%=locState%>" maxlength="255" />
                                            </div>                                
                                            <div>                                
                                                <div class="subLabel">Postal Code:</div>
                                              <input name="locZip" id="locZip" class="inputShort" value="<%=locZip%>" maxlength="255" />
                                            </div>
                                            <div>                                
                                                <div class="subLabel">Country:</div>
                                                <input name="locCountry" id="locCountry" class="inputLong" value="<%=locCountry%>" maxlength="255" />
                                            </div>				                                                            
                                            <div>                                
                                                <div class="subLabel">Hierarchy:</div>
                                                <div style="float:left; margin-top:3px;"><img src="../_images/icons/16/tree_level.gif" width="16" height="16" align="absmiddle" style="vertical-align:middle;"/>&nbsp;</div>
                                                <div id="divHierarchy" style="margin-top:3px;"><% if len(locHierarchy)<=0 then  response.write("<em>none assigned</em>") else response.write( reformatHierarchy(locHierarchy,"html") ) %></div>
                                            </div>	                                                                                                                      
                                            <input type="hidden" name="locHierarchy" id="locHierarchy" value="<% =locHierarchy %>">                                                                                                                    
                                          </td>                              
                                        </tr>
									</table>
                                </td>
                            </tr>                                                        
                      	</table>
						<!-- STOP Location section -->       


						<!-- START Synopsis section -->                        
                        <input type="hidden" id="title:synopsis" value="Please provide a brief synopsis">          
   						<table id="table:synopsis" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">                    												
                            <tr>
                                <td align="center" style="clearFormat">
                                	<table width="95%">
                                    	<tr>
                                        	<td align="left" class="clearFormat">
                                				<div style="color:#35487B; font-weight:bold; padding:3px;">Summary</div>
                                				<textarea name="rptSummary" id="rptSummary" style="width:100%; height:100px; margin-bottom:12px;" ><%=server.HTMLEncode(rptSummary & "")%></textarea>
                            				</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>                            
                      	</table>
						<!-- STOP Synopsis section -->       


						<!-- START Severity section -->     
                        <input type="hidden" id="title:severity" value="Severity of issue">                      
   						<table id="table:severity" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="border:none; padding:0px; margin:0px;">                        			
                                    <table width="100%" cellpadding="0" cellspacing="0">                                                                                    												
                                        <tr>
                                            <td class="formLabel"><span class="required">*</span>Severity:</td>
                                            <td align=left>
                                                <select name="severityLevel" id="severityLevel" onchange="severityHelp();" size=1 class="inputMediumLong">
	                                                <option value="">-- Select --</option>
                                                    <option value="3" <%=checkMatch(severityLevel,"3")%>>3-Low</option>
                                                    <option value="2" <%=checkMatch(severityLevel,"2")%>>2-Medium</option>
                                                    <option value="1" <%=checkMatch(severityLevel,"1")%>>1-High</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>                            

            				<%
							mySQL = "SELECT Notes " _
								  & "	FROM Directive " _
								  & "	WHERE CustomerID='" & customerID & "' AND Event = 'Severity: 1-High' " _
							      & "	ORDER BY SortOrder "
							set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
							%>
                            <tr>
                                <td style="border:none; padding:0px; margin:0px;">                        			
                                    <table width="100%" cellpadding="0" cellspacing="0">                                        
                                        <tr id="tr_severity1Help" style="background-color:#FFFFC8; display:none;">
                                            <td align="left" style="padding:15px;">                                
                                                <% if not rs.eof then response.write(rs("notes")) %>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                 
            				<%
							mySQL = "SELECT Notes " _
								  & "	FROM Directive " _
								  & "	WHERE CustomerID='" & customerID & "' AND Event = 'Severity: 2-Medium' " _
							      & "	ORDER BY SortOrder "
							set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
							%>
                            <tr>
                                <td style="border:none; padding:0px; margin:0px;">                        			
                                    <table width="100%" cellpadding="0" cellspacing="0">                                        
                                        <tr id="tr_severity2Help" style="background-color:#FFFFC8; display:none;">
                                            <td align="left" style="padding:15px;">                                
                                                <% if not rs.eof then response.write(rs("notes")) %>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

            				<%
							mySQL = "SELECT Notes " _
								  & "	FROM Directive " _
								  & "	WHERE CustomerID='" & customerID & "' AND Event = 'Severity: 3-Low' " _
							      & "	ORDER BY SortOrder "
							set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
							%>
                            <tr>
                                <td style="border:none; padding:0px; margin:0px;">                        			
                                    <table width="100%" cellpadding="0" cellspacing="0">                                        
                                        <tr id="tr_severity3Help" style="background-color:#FFFFC8; display:none;">
                                            <td align="left" style="padding:15px;">                                
                                                <% if not rs.eof then response.write(rs("notes")) %>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>                            
                      	</table>
						<!-- STOP Severity section -->       


						<!-- START Category section -->          
                        <input type="hidden" id="title:category" value="Category assigned to issue">                      
   						<table id="table:category" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">
                    												
                            <tr>
                              <td class="formLabel">Categories:</td>    
                              
                              <!-- START Category Table -->                         
                              <td align=left> 
                               	<img src="../_images/icons/16/help.png" width="16" height="16" align="absmiddle" style="margin-right:5px; margin-bottom:2px; vertical-align:middle;"/><span style="color:#2d73b2;">Double-click on a category to view details.</span>
                                <br>                                                              
								<select name="listCategory" id="listCategory" ondblclick="categoryLookUp('listCategory');" size="5" style="width:85%; height:100px; margin-bottom:5px;" multiple="multiple">
									<!--  Find Categories ALREADY assigned -->
                                  	<%
									mySQL = "SELECT a.CRSID, a.[Primary], Category.CategoryID, Category.Name, Category.Sub, Category.Active " _
										  & "	FROM CRS_Category AS a INNER JOIN Category ON a.CategoryID = Category.CategoryID " _
										  & "	WHERE a.CRSID = '" & idIssue & "' " _
										  & "	ORDER BY Category.Name, Category.Sub "
									set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
									dim tempCat, priCategory
                    	            do while not rs.eof
										if len(rs("Sub")) > 0 then
											tempCat = rs("Name") & " [" & rs("Sub") & "]"
										else
											tempCat = rs("Name")
										end if
										if rs("primary")="Y" then
											priCategory = rs("CategoryID")
											tempCat = tempCat & " (primary)"
										end if
										if rs("active")="N" then
											tempCat = "Missing:" & tempCat
										end if
										Response.Write "<option value=""" & rs("CategoryID") & """>" & tempCat & "</option>"
                                        rs.movenext
                                    loop
                                    call closeRS(rs)									  
                        	        %>
								</select>
                                <input name="listCategoryAdded" id="listCategoryAdded" type="hidden" value="" />
                                <input name="listCategoryRemoved" id="listCategoryRemoved" type="hidden" value="" />
                                <input name="listCategoryInvAdd" id="listCategoryInvAdd" type="hidden" value="" />
                                <input name="primaryOriginal" id="primaryOriginal" type="hidden" value="<% =priCategory %>" />
                                <input name="primaryCategory" id="primaryCategory" type="hidden" value="<% =priCategory %>" />
                                <br>     

                            	<div style="float:left; width:175px;">
									<a class="myCMbutton" href="#" onclick="this.blur(); categoryAdd(); resetSystemMsg('systemMessage'); return false;"><span class="add" style="padding-right:10px;">Add</span></a>
									<a class="myCMbutton" href="#" onclick="this.blur(); javascript:removeCategory('listCategory'); resetSystemMsg('systemMessage'); return false;"><span class="delete" style="padding-right:7px;">Delete</span></a> 
                                </div>
                               	<div>
									<a class="myCMbutton" href="#" onclick="this.blur(); javascript:setPrimary(); resetSystemMsg('systemMessage'); return false;"><span class="flag" style="padding-right:5px;">Primary</span></a>                            
                                </div>

                              </td>
                              <!-- STOP Category Table -->
                              
                            </tr>                            
                                                        
                      	</table>
						<!-- STOP Category section -->       


						<!-- START Questions section -->        
                        <input type="hidden" id="title:questions" value="Additional questions">                
   						<table id="table:questions" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="border:none; padding:0px; margin:0px;">                        			
                                	
										<%
                                        dim arrQesID, qesNumber, arrType, typeI ', arrQuestionSet
                                        '----------------------------
                                        'add questions for new issue
                                        '----------------------------
                                        if rptQuestionCount > 0 then
                							'cycle through all issue typs and find related questions
											arrType = split(arrIssueType,",")
												
	                                        'redim arrQuestionSet(UBound(arrType),1)
											
											for typeI = 0 to UBound(arrType)
											
												mySQL = "SELECT QuestionID, Name, Value, Required " _
													  & "	FROM Question " _
													  & "	WHERE CustomerID='" & customerID & "' AND IssueTypeID = " & trim(arrType(typeI)) & " AND Active='Y' AND LanguageCD='en-US' " _
													  & "	ORDER BY SortOrder "
												set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
					
												'reset counts
												arrQesID = ""
												qesNumber = 0
												
												response.write("<table id=""questionTableSet:" & trim(arrType(typeI)) & """ style=""display:none;"" width=""100%"" cellpadding=""0"" cellspacing=""0"">")												
					
												'make sure there ARE questions to show...
												if not rs.eof then
													
													do until rs.eof		
														
														response.write("<input name=""rptQuesName:" & trim(arrType(typeI)) & ":" & qesNumber & """ type=""hidden"" value=""" & rs("Name") & """ />")
														'build field with possible dropdown
														call dispQFields(rs("Name"), rs("QuestionID"), "rptQuesAnswer:" & trim(arrType(typeI)) & ":" & qesNumber, "", "text")
														
														'used for saving form elements
														qesNumber = qesNumber + 1
														
														'arrQuestionSet(typeI+1,1) = arrQuestionSet(typeI+1,1) & rs("QuestionID") & ","
														
														arrQesID = arrQesID & rs("QuestionID") & ","
														
														rs.movenext
													loop
													
													'<!-- FORM ELEMENT SAVING... -->
													response.write("<input name=""arrQuestion:" & trim(arrType(typeI)) & """ id=""arrQuestion"" type=""hidden"" value=""" & arrQesID & """ />")

												else												
													response.write("<tr><td>")
													response.write("<table width=""95%"" cellpadding=""0"" cellspacing=""0""<td><td class=""clearFormat"" style=""padding:5px;"" >")
													call systemMessageBox("questionsID","statusMessageINFO","No questions are assigned to this issue type.")
													response.write("</td></tr></table>")										
													response.write("</td></tr>")										
												end if

												response.write("</table>")												

											next		
											
											call closeRS(rs)
											
                                        'no questions with issue ARE there any for customer
                                        else														
                                            'not fields have been setup
											response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0"">")
                                            response.write("<tr><td>")
                                            response.write("<table width=""95%"" cellpadding=""0"" cellspacing=""0""<td><td class=""clearFormat"" style=""padding:5px;"" >")
                                            call systemMessageBox("questionsID","statusMessageINFO","No questions are assigned to this issue type.")
                                            response.write("</td></tr></table>")										
                                            response.write("</td></tr>")																					
                                            response.write("</table>")
                                        end if					
                                        %>
                                        
                                </td>
                            </tr>                            
                                                    
                      	</table>
						<!-- STOP Questions section -->       


						<!-- START CRSID section -->                        
                        <input type="hidden" id="title:add" value="Closing Instructions"> 
   						<table id="table:add" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">

            				<%
							mySQL = "SELECT Notes " _
								  & "	FROM Directive " _
								  & "	WHERE CustomerID='" & customerID & "' AND Event = 'Interview: End' " _
							      & "	ORDER BY SortOrder "
							set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
							%>
                            <tr>
                                <td style="border:none; padding:0px; margin:0px;">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr id="tr_crsidhelp" style="background-color:#FFFFC8;">
                                            <td align="left" style="padding:15px;">
                                                <% 
												if not rs.eof then 
													response.write(rs("notes")) 
												else
													response.write("No closing instructions or directive available. Click 'Add Issue' to generate Issue # and Call Back date.") 												
												end if												
												%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                      	</table>
						<!-- STOP CRSID section -->       

                    </td>
                    <!-- START Page Details and Form Fields -->			                    
                                        
                </tr>
            
            </table>
            <!-- END Issues Details table -->
	
    	</div>
		<!-- STOP Details div -->        

		<%
		'set User Field default values...must be here so as to not interfear with the User Fields on Case Notes "_INCissueNotes.asp" tab
		'NOT setting if issue is being edited...don't want to mess up what customer has done.
        if action = "add" or action = "addfu" then							
			response.Write("<input type=""hidden"" name=""rptUserField1"" id=""rptUserField1"" value=""" & rptUserField1 & """ >")
			response.Write("<input type=""hidden"" name=""rptUserField2"" id=""rptUserField2"" value=""" & rptUserField2 & """ >")
			response.Write("<input type=""hidden"" name=""rptUserField3"" id=""rptUserField3"" value=""" & rptUserField3 & """ >")			
			response.Write("<input type=""hidden"" name=""rptUserField4"" id=""rptUserField4"" value=""" & rptUserField4 & """ >")						
			response.Write("<input type=""hidden"" name=""rptUserField5"" id=""rptUserField5"" value=""" & rptUserField5 & """ >")
			response.Write("<input type=""hidden"" name=""rptUserField6"" id=""rptUserField6"" value=""" & rptUserField6 & """ >")
			response.Write("<input type=""hidden"" name=""rptUserField7"" id=""rptUserField7"" value=""" & rptUserField7 & """ >")			
			response.Write("<input type=""hidden"" name=""rptUserField8"" id=""rptUserField8"" value=""" & rptUserField8 & """ >")		
			response.Write("<input type=""hidden"" name=""rptUserField9"" id=""rptUserField9"" value=""" & rptUserField9 & """ >")		
			response.Write("<input type=""hidden"" name=""rptUserField10"" id=""rptUserField10"" value=""" & rptUserField10 & """ >")													
			response.Write("<input type=""hidden"" name=""rptUserField11"" id=""rptUserField11"" value=""" & rptUserField11 & """ >")
			response.Write("<input type=""hidden"" name=""rptUserField12"" id=""rptUserField12"" value=""" & rptUserField12 & """ >")
			response.Write("<input type=""hidden"" name=""rptUserField13"" id=""rptUserField13"" value=""" & rptUserField13 & """ >")			
			response.Write("<input type=""hidden"" name=""rptUserField14"" id=""rptUserField14"" value=""" & rptUserField14 & """ >")						
			response.Write("<input type=""hidden"" name=""rptUserField15"" id=""rptUserField15"" value=""" & rptUserField15 & """ >")
			response.Write("<input type=""hidden"" name=""rptUserField16"" id=""rptUserField16"" value=""" & rptUserField16 & """ >")
			response.Write("<input type=""hidden"" name=""rptUserField17"" id=""rptUserField17"" value=""" & rptUserField17 & """ >")			
			response.Write("<input type=""hidden"" name=""rptUserField18"" id=""rptUserField18"" value=""" & rptUserField18 & """ >")		
			response.Write("<input type=""hidden"" name=""rptUserField19"" id=""rptUserField19"" value=""" & rptUserField19 & """ >")		
			response.Write("<input type=""hidden"" name=""rptUserField20"" id=""rptUserField20"" value=""" & rptUserField20 & """ >")													
		end if
		%>

   		<script language="javascript">
			//hide/show caller related fields based on Anonynimity
			function locationVisible(objVisible) {						
				changeObjectDisplay('tr_Location', objVisible);
				if (objVisible == "none") {
					changeObjectDisplay('imgLocation_Visible', '');					
					changeObjectDisplay('imgLocation_Hide', 'none');										
				}
				else {
					changeObjectDisplay('imgLocation_Visible', 'none');					
					changeObjectDisplay('imgLocation_Hide', '');										
				}
			}
		
			//hide/show caller related fields based on Anonynimity
			function callerVisible(objVisible) {						
				changeObjectDisplay('tr_callerName', objVisible);
				changeObjectDisplay('tr_callerTitle', objVisible);
				changeObjectDisplay('tr_callerAddress', objVisible);
				changeObjectDisplay('tr_callerPhone', objVisible);
				changeObjectDisplay('tr_callerEmail', objVisible);			
			}						

			//hide/show caller related fields based on Anonynimity
			function severityHelp() {
				var sev = document.getElementById('severityLevel');
				if (sev.value == "1") {
					changeObjectDisplay('tr_severity1Help', '');
					changeObjectDisplay('tr_severity2Help', 'none');
					changeObjectDisplay('tr_severity3Help', 'none');
				}				
				else if (sev.value == "2") {
					changeObjectDisplay('tr_severity1Help', 'none');
					changeObjectDisplay('tr_severity2Help', '');
					changeObjectDisplay('tr_severity3Help', 'none');
				}				
				else if (sev.value == "3") {
					changeObjectDisplay('tr_severity1Help', 'none');
					changeObjectDisplay('tr_severity2Help', 'none');
					changeObjectDisplay('tr_severity3Help', '');
				}
				else {
					changeObjectDisplay('tr_severity1Help', 'none');
					changeObjectDisplay('tr_severity2Help', 'none');
					changeObjectDisplay('tr_severity3Help', 'none');					
				}								
			}
		</script>

		<script>
			function categoryAdd() {
				SimpleModal.open("../_dialogs/popup_category.asp?recid=<% =idIssue %>&cid=<% =customerID %>&tid=" + document.getElementById('issueType').value + "&pageview=<% =pageView %>", 410, 450, "no");
			}
    	</script>
        
        
        