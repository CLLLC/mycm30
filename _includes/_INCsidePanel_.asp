<!-- Side Menus visible on every page
	(C) www.dhtmlgoodies.com, October 2005
	
	Version 1.2: Updated, November 12th. 2005
	
	This is a script from www.dhtmlgoodies.com. You will find this and a lot of other scripts at our website.	
	
	Terms of use:
	You are free to use this script as long as the copyright message is kept intact. However, you may not
	redistribute, sell or repost it without our permission.
	
	Thank you!
	
	www.dhtmlgoodies.com
	Alf Magne Kalleland
 -->

<!-- Code for the left panel -->
<div id="dhtmlgoodies_leftPanel">


	<div style="width:100%;">
        <div style="float:left; padding-left:5px; padding-top:5px;">
        	<img src="../_images/sidePanel_bg.png" />
        </div>        
        <div style="float:right; padding:5px;">
            <a class="myCMbutton" href="#" onClick="this.blur(); initSlideLeftPanel(); return false"><span class="cancel">Close</span></a>
        </div>
	</div>
        
	<div id="leftPanelContent" style="float:left">
    
        <!-- START help content -->
        <div>        
        	<%
			dim helpSQL, helpRS
            helpSQL="SELECT Title, HelpText " _
                & "   FROM Help " _
                & "   WHERE HID = " & helpID
            set helpRS = openRSexecute(helpSQL)
			if not helpRS.eof then			
				response.write("<div class=""Title"">" & helpRS("Title") & "</div>")
				'CCI Staff using to add/edit issue
				if helpID = 7 then				
				
					if pageView = "issue" or pageView = "issue:00" then
						extraClick = "resetSystemMsg('systemMessage'); StopTheTimer();"
					else
						extraClick = ""
					end if
				
					response.write("<p><strong>" & customerName & "</strong><br/>" & customerProfile & "</p>")
					response.write("<table style=""width:100%;"" border=""0"" cellspacing=""0"" cellpadding=""2"">")
					response.write("	<tr> ")
					response.write("		<td style=""padding-bottom:10px;""><img src=""../_images/icons/24/briefcase.png"" title=""Profile Snapshot"" width=""24"" height=""24"" align=""absmiddle"" style=""vertical-align:middle; margin-left:4px; margin-right:4px;"" border=""0"" /><a href=""#"" class=""SubTitle"" onClick=""SimpleModal.open('../_dialogs/popup_profile_snapshot.asp?cid=" & customerID & "&pageview=" & pageView & "', 475, 500, 'no');" & extraClick & " return false;"">Profile Snapshot</a><span class=""Fkeys"" style=""padding-left:5px;"">[F3]</span></td>")
					response.write("	</tr>")
					response.write("	<tr> ")
					response.write("		<td style=""padding-bottom:10px;""><img src=""../_images/icons/24/direction.png"" title=""Directives"" width=""24"" height=""24"" align=""absmiddle"" style=""vertical-align:middle; margin-left:4px; margin-right:4px;"" border=""0"" /><a href=""#"" class=""SubTitle"" onClick=""SimpleModal.open('../_dialogs/popup_directive_table.asp?cid=" & customerID & "&pageview=" & pageView & "', 500, 625, 'no'); " & extraClick & " return false;"">Directives</a><span class=""Fkeys"" style=""padding-left:5px;"">[F4]</span></td>")
					response.write("	</tr>")
					response.write("	<tr> ")
					response.write("		<td style=""padding-bottom:10px;""><img src=""../_images/icons/24/group.png"" title=""Contacts"" width=""24"" height=""24"" align=""absmiddle"" style=""vertical-align:middle; margin-left:4px; margin-right:4px;"" border=""0"" /><a href=""#"" class=""SubTitle"" onClick=""SimpleModal.open('../_dialogs/popup_user_table.asp?cid=" & customerID & "&pageview=" & pageView & "', 500, 700, 'no'); " & extraClick & " return false;"">Contacts &amp; Users</a><span class=""Fkeys"" style=""padding-left:5px;"">[F6]</span></td>")
					response.write("	</tr>")
					response.write("	<tr> ")
					response.write("		<td style=""padding-bottom:10px;""><img src=""../_images/icons/24/globe.png"" title=""Locations"" width=""24"" height=""24"" align=""absmiddle"" style=""vertical-align:middle; margin-left:4px; margin-right:4px;"" border=""0"" /><a href=""#"" class=""SubTitle"" onClick=""SimpleModal.open('../_dialogs/popup_location_table.asp?cid=" & customerID & "&pageview=" & pageView & "', 525, 700, 'no'); " & extraClick & " return false;"">Locations</a><span class=""Fkeys"" style=""padding-left:5px;"">[F7]</span></td>")
					response.write("	</tr>")
					response.write("	<tr> ")
					response.write("		<td style=""padding-bottom:10px;""><img src=""../_images/icons/24/phone.png"" title=""Telcom (800#)"" width=""24"" height=""24"" align=""absmiddle"" style=""vertical-align:middle; margin-left:4px; margin-right:4px;"" border=""0"" /><a href=""#"" class=""SubTitle"" onClick=""SimpleModal.open('../_dialogs/popup_telcom_table.asp?cid=" & customerID & "&pageview=" & pageView & "', 500, 625, 'no'); " & extraClick & " return false;"">Telcom (800#)</a></td>")
					response.write("	</tr>")
					response.write("</table>") 		
					
							
				else
					response.write( helpRS("HelpText") )
				end if
			end if
			call closeRS(helpRS)									  
			%>                    
		</div>
        <!-- STOP help content -->
        
	</div>


</div>
<!-- End code for the left panel -->

<!-- For RS help with customer profile, go ahead and open side panel-->
<% if helpID = 7 and pageView = "issue:add" then %>
	<script>
        initSlideLeftPanel();
    </script>
<% end if %>


