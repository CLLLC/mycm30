        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<%
       	dim labelX
		dim arrDisabled
		dim labelTemp
		dim valueTemp
		%>

		<!-- START Account div -->
        <div id="tab:details">
        
        
            <!-- START Group Detail table -->   
	        <table id="table_details" cellpadding="0" cellspacing="0" style="width:100%;">

                <tr>
                    
                    <td valign="top">
                                        
					  	<!-- START Details Form Fields -->	                    
						<table id="table:details:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">                           
				
                       		<tr>                            
								<td class="formLabel"><span class="required">*</span>Name:</td>
                                <td align=left>
                                	<input name="optName" class="inputLong" style="width:285px;" onkeyup="updateTitleText(this,'titleText','Rule:&nbsp;')" value="<% =optName %>" maxlength="150" />
                                </td>
                            </tr>

							<!-- START Rule Table -->                                
                       		<tr>                            
								<td class="formLabel"><span class="required">*</span>Rule:</td>
                                <td align=left>
                                    
                                    <div class="subLabel" style="margin-bottom:6px;">Only issues matching the criteria below will assign the users listed under 'Assignments'.</div>
                                                     
									<!-- START field selection for filter -->
                                    <table width="85%" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
										<td style="background-color:#C7CDE9; border:none; padding:7px;">
                                            <img src="../_images/icons/18/add.png" width="18" height="18" title="Add" align="absmiddle" style="margin-right:7px; cursor:pointer; vertical-align:middle;" onClick="javascript:addQueryRow();" />
                                            <select name="optQueryField" id="optQueryField" size=1 style="width:200px;">
                                            	<%
                                                'build dropdown of available fields for filter selection
                                                '...do not include if already a part of XML filter
                                                for labelX = 0 to UBound(arrColumn)
                                                    response.write "<option value='" & arrColumn(labelX) & "'>" & replace(arrFriendly(labelX), "_", " ") & "</option>"
                                                next
                                                %>
                                            </select>                                        
										</td>
                                      </tr>
                                    </table>                                        
									<!-- STOP field selection for filter -->
                                    
                                    
									<!-- START WHERE clause filter -->                                                                        
                                    <table width="85%" class="formFilter" id="tblFilter" border="0" cellspacing="0" cellpadding="0">
                                        <tbody id="tblFilterBody">
                                        
										<%
										'malformed XML needs ampersand "&" converted to "&amp;" to display correctly
										optFilter = replace(optFilter, " & ", " &amp; ")
										
										'prepare XML filter
										dim objPopCalendar
										dim objLst, subLst, subLstNext, i
										dim xmlDoc : set xmlDoc = Server.CreateObject("Microsoft.XMLDOM")												
										xmlDoc.async = False
										xmlDoc.loadXml(optFilter)
										set objLst = xmlDoc.getElementsByTagName("filter")
										for i = 0 to objLst.Length - 1
											set subLst = objLst.item(i)
											
											'date field go ahead and get next values
											if inStr(lCase(subLst.childNodes(0).text),"date") > 0 then
												set subLstNext = objLst.item(i+1)
											end if
											
                                        	%>					
                                             <tr id="rowFilter_<% =i %>">
                                                <td width="50">                                                       
													<div style="white-space:nowrap;">
                                                    
                                                        <%
														'no date, create singe hidden field
														if inStr(lCase(subLst.childNodes(0).text),"date") = 0 then
                                                        	response.write("<input type=hidden name=""idQueryField"" id=""idQueryField"" value=""" & subLst.childNodes(0).text & """>")
														
														'date field filtered on, create two (2) hidden fields
														else															
                                                        	response.write("<input type=hidden name=""idQueryField"" id=""idQueryField"" value=""" & subLst.childNodes(0).text & """>")
                                                        	response.write("<input type=hidden name=""idQueryField"" id=""idQueryField"" value=""" & subLstNext.childNodes(0).text & """>")																													
														end if                                                            

														'temporary clean up for display only    
														labelTemp = subLst.childNodes(0).text
														labelTemp = replace(labelTemp, "[","")
														labelTemp = replace(labelTemp, "]","")		
														labelTemp = replace(labelTemp, "_"," ")														
														%>
                                                       	<img src="../_images/icons/18/cancel.png" width="18" height="18" title="Delete" align="absmiddle" style="margin-right:7px; cursor:pointer; vertical-align:middle;" onClick="javascript:delQueryRow('rowFilter_<% =i %>','<% =labelTemp %>','<% =subLst.childNodes(0).text %>');"/>
														<!-- field name here -->
														<% =labelTemp %>                                                        

                                                    </div>
                                                </td>
                                                <td width="125">
                                                    <%
													if inStr(lCase(subLst.childNodes(0).text),"date") = 0 then
                                                	%>
                                                        <select name="idQueryExp" id="idQueryExp_<% =i %>" style="width:125px;" onchange="javascript:changeElementStyle(this.id,this.value);">								
                                                            <option value="=" 			<%=checkMatch(subLst.childNodes(1).text,"=") %>>equals to</option>
                                                            <option value="<>" 			<%=checkMatch(subLst.childNodes(1).text,"<>") %>>does not equal to</option>
                                                            <option value="<=" 			<%=checkMatch(subLst.childNodes(1).text,"<=") %>>less than</option>
                                                            <option value=">=" 			<%=checkMatch(subLst.childNodes(1).text,">=") %>>greater than</option>
                                                            <option value="like" 		<%=checkMatch(subLst.childNodes(1).text,"like") %>>contains</option>
                                                            <option value="not like" 	<%=checkMatch(subLst.childNodes(1).text,"not like") %>>does not contain</option>
                                                            <option value="is null" 	<%=checkMatch(subLst.childNodes(1).text,"is null") %>>is null</option>
                                                            <option value="is not null"	<%=checkMatch(subLst.childNodes(1).text,"is not null") %>>is not null</option>
                                                            <option value="in" 			<%=checkMatch(subLst.childNodes(1).text,"in") %>>is one of</option>
                                                            <option value="not in"		<%=checkMatch(subLst.childNodes(1).text,"not in") %>>is not one of</option>
                                                        </select>                                                    
													<%
													else
													%>
                                                        <select name="idQueryExp" style="width:125px; display:none;">								
                                                            <option value=">=" <%=checkMatch(subLst.childNodes(1).text,">=") %>>greater than</option>
                                                        </select>
                                                       	<select name="idQueryExp" style="width:125px; display:none;">								
                                                            <option value="<=" <%=checkMatch(subLst.childNodes(1).text,"<=") %>>less than</option>
                                                        </select>          
                                                       	<select name="idNOT-USED" style="width:125px;">
                                                            <option value="">between</option>
                                                        </select>
													<%
													end if
													%>
                                                </td>
                                                <td>
                                                    <%
													'input boxes already selected by user
													if inStr(lCase(subLst.childNodes(0).text),"date") = 0 then
													
														'set height if IN or NOT IN operator is used
														if (subLst.childNodes(1).text = "in") or (subLst.childNodes(1).text = "not in") then
															response.write("<textarea name=""idQueryValue"" id=""idQueryValue_" & i & """ style=""overflow-y: auto; height: 42px; width:150px;"">" & subLst.childNodes(2).text & "</textarea>&nbsp;")
                                                        else
															'hide element...using IS NULL or IS NOT NULL
															if subLst.childNodes(1).text = "is null" or subLst.childNodes(1).text = "is not null" then													
																response.write("<textarea name=""idQueryValue"" id=""idQueryValue_" & i & """ style=""overflow-y:hidden; height:13px; width:150px; display:none;"">" & subLst.childNodes(2).text & "</textarea>&nbsp;")
															'normal element
															else
																response.write("<textarea name=""idQueryValue"" id=""idQueryValue_" & i & """ style=""overflow-y:hidden; height:13px; width:150px;"">" & subLst.childNodes(2).text & "</textarea>&nbsp;")
															end if
														end if
														
														'------------------------------------------
														'START JSON filter builter
														'------------------------------------------
														'dont allow JSON filtering on these fields
														if instr(labelTemp,"Caller")<=0 and instr("Total,CRSID,Issue #,Time,Location Address,Location Postal Code,ustomerID,Summary,Details,Addendum,Resolution,CaseNotes,Notes,Outcome",labelTemp) <= 0 then
														%>
															<script>
                                                            	$(function() {
                                                                    function split( val ) {
                                                                        return val.split( /,\s*/ );
                                                                    }
                                                                    function extractLast( term ) {
                                                                        return split( term ).pop();
                                                                    }                                                            
                                                                    $("#idQueryValue_<% =i %>")
                                                                        // don't navigate away from the field on tab when selecting an item
                                                                        .bind( "keydown", function( event ) {
                                                                            if ( event.keyCode === $.ui.keyCode.TAB &&
                                                                                    $( this ).data( "autocomplete" ).menu.active ) {
                                                                                event.preventDefault();
                                                                            }
                                                                        })
                                                                        .autocomplete({
                                                                            source: function( request, response ) {
                                                                                $.getJSON( "../_jquery/suggestBox/json-data.asp?cid=<% =server.URLEncode(customerID) %>&view=<% =server.URLEncode(optTable) %>&field=<% =server.URLEncode(labelTemp) %>", {
                                                                                    term: extractLast( request.term )
                                                                                }, response );
                                                                            },
                                                                            search: function() {
                                                                                // custom minLength
                                                                                var term = extractLast( this.value );
                                                                                if ( term.length < 1 ) {
                                                                                    return false;
                                                                                }
                                                                            },
                                                                            focus: function() {
                                                                                // prevent value inserted on focus
                                                                                return false;
                                                                            },
                                                                            select: function( event, ui ) {
																				//sets up for multiple responses...
																				//only do this IF the operator equals IN or NOT IN
																				if (document.getElementById("idQueryExp_<% =i %>").value == 'in' || document.getElementById("idQueryExp_<% =i %>").value == 'not in') {
																					var terms = split( this.value );
																					// remove the current input
																					terms.pop();
																					// add the selected item
																					terms.push( ui.item.value );
																					// add placeholder to get the comma-and-space at the end
																					terms.push( "" );
																					this.value = terms.join( ", " );
																					return false;
																				}																				
                                                                            }
                                                                    	});
                                                                });
															</script>                                                            														
                                                        	<%
															'END JSON filter builter
															'------------------------------------------
															
														end if		
																								
													else															
                                                       	response.write("<input type=text name=""idQueryValue"" id=""idQueryValue_" & i & """ size=20 maxlength=75 style=""width:70px;"" value=""" & subLst.childNodes(2).text & """>")
														response.write("<input type=text name=""idQueryValue"" id=""idQueryValue_" & i+1 & """ size=20 maxlength=75 style=""width:70px; margin-left:5px;"" value=""" & subLstNext.childNodes(2).text & """>")														
														'used on dhtml calendar instances
														if right(objPopCalendar,1) = "'" then
															objPopCalendar = objPopCalendar & ",'" & "idQueryValue_" & i & "','" & "idQueryValue_" & i+1 & "'"
														else
															objPopCalendar = objPopCalendar & "'" & "idQueryValue_" & i & "','" & "idQueryValue_" & i+1 & "'"
														end if
														
													end if                                                            
													%>
                                                    
                                                </td>
                                            </tr>						
                                        	<%
										
											'date field so increase row count by 1 so as to not repeat the second date value
											if inStr(lCase(subLst.childNodes(0).text),"date") > 0 then
												i = i+1
											end if
											
                                        next										
                                    	%>

									    <!-- START calendar instances -->
										<% if len(objPopCalendar) > 0 then %>                                        
                                            <script type="text/javascript">
                                                var dateCalonLoad;
                                                //issue date calendar
                                                dateCalonLoad = new dhtmlxCalendarObject([<% =objPopCalendar %>], true, {isWinHeader:true, headerButtons:"XT", isWinDrag:false, isYearEditable:true, isMonthEditable:true });
                                                dateCalonLoad.setSkin("simplegrey");
                                                dateCalonLoad.setDateFormat("%m/%d/%Y");
                                                dateCalonLoad.setYearsRange(2000, 2020);
                                                dateCalonLoad.setHeaderText("Filter Date");
                                            </script>
                                        <% end if %>
                                        <!-- STOP calendar instances -->                                        

										<!-- THIS IS WHERE FILTER OPTIONS ARE APPENDED WITH JAVASCRIPT -->
                                  		<tr style="display:none;">                                   			
                                    		<td></td><td></td><td></td>
                                   		</tr>			
                                        
                                        </tbody>
                         			</table>
									<!-- STOP WHERE clause filter -->
                                    
                                    
                                </td>
                            </tr>
                            
                            
                            
                            
                            
                            
                            
                            <!-- START Members Table -->    
                            <tr>
								<td class="formLabel">Assignments:</td>
								<td align="left"> 
	                               	<img src="../_images/icons/16/help.png" width="16" height="16" align="absmiddle" style="margin-right:5px; margin-bottom:2px;"/><span style="color:#2d73b2;">Double-click on a user to view details.</span>
    	                            <br>                                                                                              
                            		<select name="listUser" id="listUser" ondblclick="userLookUp('listUser');" size="5" style="width:85%; height:100px; margin-bottom:5px;" multiple="multiple">
                                 		<%
										if action = "edit" then
											dim tempUser
											
											mySQL = "SELECT a.LOGID, a.CustomerID, c.CustomerID AS [Rule.CustomerID], b.RuleID, a.FirstName, a.LastName, a.Active " _
												  & "	FROM Logins AS a INNER JOIN Logins_Rule AS b ON a.LOGID = b.LOGID INNER JOIN [Rule] AS c ON b.RuleID = c.RuleID "
												  
											'find all existing members INCLUDING CCI Staff
											if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
												mySQL = mySQL & " WHERE b.RuleID=" & recId & " AND a.Active<>'N' "
											'find all existing members CUSTOMER USERS ONLY
											else
												mySQL = mySQL & " WHERE b.RuleID=" & recId & " AND a.Active<>'N' AND a.SecurityLevel>10 "
											end if
											
											mySQL = mySQL & " ORDER BY a.LastName, a.FirstName "																																	
											
											'open recordset
											set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)                                                
											do while not rs.eof
												'prepare name format
												tempUser = ""
												if len(rs("lastname")) <= 0 then
													tempUser = rs("firstname")
												elseif len(rs("firstname")) <= 0 then
													tempUser = rs("lastname")
												elseif len(rs("lastname")) <= 0 and len(rs("firstname")) <= 0 then
													tempUser = "<no name>"
												else
													tempUser = rs("lastname") & ", " & rs("firstname")
												end if																		
												'show status
												if rs("Active") = "C" then 
													tempUser = tempUser & " (contact only)" 
												elseif rs("Active") = "N" then 
													tempUser = tempUser & " (disabled)" 
												else 
													tempUser = tempUser & " (active user)"
												end if
												'show linked from other profile
												if lCase(rs("CustomerID")) <> lCase(rs("Rule.CustomerID")) then 
													tempUser = tempUser & " ([linked:" & rs("CustomerID") & "])" 
												end if																								
												'add user to list
												Response.Write "<option value=""" & rs("LogID") & """>" & tempUser & "</option>"
												rs.movenext
											loop
											call closeRS(rs)									  
										end if
                                     	%>
                                    </select>
                                                                
                                	<!-- These elements MUST be set to style="display:none;" and NOT type="hidden" -->
                                 	<!-- when set as "hidden" FireFox does not recognize any change has been made  -->                                                                                            
                                  	<input name="listUserAdded" id="listUserAdded" style="display:none;" value="" />                                            
                                 	<input name="listUserRemoved" id="listUserRemoved" style="display:none;" value="" />
                                   	<br>
                                	<div style="float:left; width:175px;">
                                    	<% if action = "edit" then %>
	                                 		<a class="myCMbutton" href="#" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_members.asp?recid=<% =recId %>&cid=<% =customerID %>&multi=true', 380, 450, 'no'); return false;"><span class="add" style="padding-right:10px;">Add</span></a>
    	                            		<a class="myCMbutton" href="#" onclick="this.blur(); javascript:removeUser('listUser'); resetSystemMsg('systemMessage'); return false;"><span class="delete" style="padding-right:7px;">Delete</span></a>
                                        <% else %>
	                                 		<a class="myCMbutton" href="#" onclick="this.blur(); jAlert('<strong>Memberships disabled.</strong><br/><br/>You can add new members after adding rule.', 'myCM Alert'); return false;"><span class="add" style="padding-right:10px;">Add</span></a>
    	                            		<a class="myCMbutton" href="#" onclick="this.blur(); jAlert('<strong>Memberships disabled.</strong><br/><br/>You can remove members after adding rule.', 'myCM Alert'); return false;"><span class="delete" style="padding-right:7px;">Delete</span></a>
                                        <% end if %>
                                   	</div>
                                    <div>
	                                    <% 
										'for CCI Admin use ONLY!
										if cLng(session(session("siteID") & "adminLoggedOn")) < 3 and action = "edit" then %>
											<a class="myCMbutton" href="#" onclick="this.blur(); linkUser(); return false;"><span class="userlink" style="padding-right:7px;">Link Users</span></a>                                    
                                        <% end if %>                                    
                                    </div>
                                </td>
                            </tr>
                            <!-- STOP Members Table -->      
                                                                                                
                            <tr>
                                <td colspan="2" align="center">
                                    <table width="95%">
                                        <tr>
                                            <td align="left" class="clearFormat" style="padding-right:5px;">
                                                <div style="color:#35487B; font-weight:bold; padding:3px;">Notes</div>
                                                <textarea name="optNotes" id="optNotes" style="width:100%; height:100px; margin-bottom:12px;" ><%=server.HTMLEncode(optNotes & "")%></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            
                        </table>
   						<!-- END Details Form Fields -->
                        
                    </td>
                    
                </tr>
            
            </table>
            <!-- END User Account table -->
	
    	</div>
		<!-- STOP Account div -->        

		<script>
			function linkUser() {
				//make sure user agrees to change
				jConfirm('Be <strong>EXTREMELY CAREFUL</strong> when linking<br/>users from another customer profile.<br/><br/>Continue with linking?', 'Confirm Link', function(r) {
					//user agrees, remove categories
					if (r==true) {
						//open link window
						SimpleModal.open('../_dialogs/popup_members.asp?recid=<% =recId %>&cid=<% =customerID %>&multi=true&userlink=true', 380, 450, 'no');							
					}
					//user disagrees
					else {
						return false;
					}
				});							    
			}        
        </script>
                
        <script type="text/javascript">
        
          // Add new row to table listing all added filters. All filters are added to form.
          // When form is submittted idQueryField, idQueryExp, and idQueryValue are split
          // within counter_edit_exec.asp and added to datebase table ReportQuery_Filter
          function addQueryRow()
          {  	
          
            var tempHTML;
            
            // create randon numbers used to create unique element IDs for idQueryValue form fields
            var valueID_1 = Math.floor(Math.random()*1001);
            var valueID_2 = Math.floor(Math.random()*1001);
          
            // create table body
            var table = document.getElementById("tblFilter");
            var tbody = table.getElementsByTagName("tbody")[0];
        
            // ----- ADD ROW -----
            // create row element and assign unique ID
            var rowCount = table.rows.length;
            var rowID = "rowFilter_" + (rowCount+2);
            var row = document.createElement("tr");
            row.id = rowID;
            
            // ----- ADD CELL #1 -----
            var selValue = document.getElementById('optQueryField').value;					// get VALUE from field dropdown
            var selIndex = document.getElementById('optQueryField').selectedIndex;			// get selected INDEX from dropdown
            var selText = document.getElementById('optQueryField').options[selIndex].text;	// get selected TEXT not value from dropdown
            var td1 = document.createElement("td")
            td1.width = "50";
            
            // check if field selected has "Date" text in name
            if (selValue.indexOf("Date")==-1){
                tempHTML = "<div style=\"white-space:nowrap;\">";
                tempHTML = tempHTML + "<img src=\"../_images/icons/18/cancel.png\" width=\"18\" height=\"18\" alt=\"Delete\" align=\"absmiddle\" style=\"margin-right:7px; cursor:pointer; vertical-align:middle;\" onClick=\"javascript:delQueryRow('" + rowID + "','" + selText + "','" + document.getElementById('optQueryField').value + "');\"/>";
                tempHTML = tempHTML + "<input type=hidden name=\"idQueryField\" id=\"idQueryField\" value=\"" + document.getElementById('optQueryField').value + "\">";
                tempHTML = tempHTML + selText + "</div>";
            }
            // date filed...add 2 hidden fields to hold field name
            else {
                tempHTML = "<div style=\"white-space:nowrap;\">";
                tempHTML = tempHTML + "<img src=\"../_images/icons/18/cancel.png\" width=\"18\" height=\"18\" alt=\"Delete\" align=\"absmiddle\" style=\"margin-right:7px; cursor:pointer; vertical-align:middle;\" onClick=\"javascript:delQueryRow('" + rowID + "','" + selText + "','" + document.getElementById('optQueryField').value + "');\"/>";
                tempHTML = tempHTML + "<input type=hidden name=\"idQueryField\" id=\"idQueryField\" value=\"" + document.getElementById('optQueryField').value + "\">";
                tempHTML = tempHTML + "<input type=hidden name=\"idQueryField\" id=\"idQueryField\" value=\"" + document.getElementById('optQueryField').value + "\">";
                tempHTML = tempHTML + selText + "</div>";
            }	
            td1.innerHTML = tempHTML;
            
            //remove selected text from dropdown
        ///	document.getElementById('optQueryField').remove(selIndex);		
            
            
            // ----- ADD CELL #2 -----	
            var td2 = document.createElement("td")
            // check if field selected has "Date" in name
            if (selValue.indexOf("Date")==-1){
                tempHTML = "<select name=\"idQueryExp\" id=\"idQueryExp_" + valueID_1 + "\" style=\"width:125px;\" onchange=\"javascript:changeElementStyle(this.id,this.value);\" >";
                tempHTML = tempHTML + "<option value=\"=\" >equals to</option>";
                tempHTML = tempHTML + "<option value=\"<>\" >does not equal to</option>";	
                tempHTML = tempHTML + "<option value=\"<=\" >less than</option>";
                tempHTML = tempHTML + "<option value=\">=\" >greater than</option>";
                tempHTML = tempHTML + "<option value=\"like\" >contains</option>";
                tempHTML = tempHTML + "<option value=\"not like\" >does not contain</option>";
                tempHTML = tempHTML + "<option value=\"is null\" >is null</option>";
                tempHTML = tempHTML + "<option value=\"is not null\" >is not null</option>";
                tempHTML = tempHTML + "<option value=\"in\" >is one of</option>";	
                tempHTML = tempHTML + "<option value=\"not in\" >is not one of</option>";
                tempHTML = tempHTML + "</select>";
            }
            // date filed...add 2 options
            else {
                tempHTML = "<select name=\"idQueryExp\" id=\"idQueryExp_" + valueID_1 + "\" style=\"width:125px; display:none;\">";
                tempHTML = tempHTML + "<option value=\">=\" >greater than</option>";
                tempHTML = tempHTML + "</select>";
                tempHTML = tempHTML + "<select name=idQueryExp style=\"width:125px; display:none;\">";
                tempHTML = tempHTML + "<option value=\"<=\" >less than</option>";
                tempHTML = tempHTML + "</select>";
                tempHTML = tempHTML + "<select name=\"idNOT-USED\" id=\"idNOT-USED\" style=\"width:125px;\">";
                tempHTML = tempHTML + "<option value=\"\" >between</option>";
                tempHTML = tempHTML + "</select>";
            }
            td2.innerHTML = tempHTML;
            td2.width = "125";
            
            
            // ----- ADD CELL #3 -----
            var td3 = document.createElement("td")
            // not date...add textarea input
            if (selValue.indexOf("Date")==-1){	
                tempHTML = "<textarea name=\"idQueryValue\" id=\"idQueryValue_" + valueID_1 + "\" style=\"overflow-y:hidden; height:13px; width:150px;\"></textarea>&nbsp;";
            }	
            // date filed...add 2 inputs
            else {	
                tempHTML = "<input type=text name=\"idQueryValue\" id=\"idQueryValue_" + valueID_1 + "\" size=10 maxlength=10 value=\"\" style=\"width:70px;\">";
                tempHTML = tempHTML + "<input type=text name=\"idQueryValue\" id=\"idQueryValue_" + valueID_2 + "\" size=10 maxlength=10 value=\"\" style=\"width:70px; margin-left:5px;\">";
            }
            td3.innerHTML = tempHTML;
        
        
            // ----- FINILIZE AND APPEND NEW CELLS AND ROW -----
            row.appendChild(td1);
            row.appendChild(td2);
            row.appendChild(td3);
                
            // append row to table
            tbody.appendChild(row);
        
        
            // ----- ADD CALENDAR INSTANCE TO DATE ELEMENTS -----
            if (selValue.indexOf("Date")!=-1){	
                var dateCal;
                dateCal = new dhtmlxCalendarObject(["idQueryValue_"+ valueID_1,"idQueryValue_"+ valueID_2], true, {isWinHeader:true, headerButtons:"XT", isWinDrag:false, isYearEditable:true, isMonthEditable:true });
                dateCal.setSkin("simplegrey");
                dateCal.setDateFormat("%m/%d/%Y");
                dateCal.setYearsRange(2000, 2020);
                dateCal.setHeaderText("Filter Date");
            }
            
            // ----- ADD AUTO JSON QUERY IF NOT DATE FIELD -----
            if (selValue.indexOf("Date")<=-1){
                var ignoreFields = "[Total],[CRSID],[Issue #],[Time],[Location Address],[Location Postal Code],[CustomerID],[Summary],[Details],[Addendum],[Resolution],[CaseNotes],[Notes],[Outcome]";
                
                //make sure field selected is not one to be ignored		
                if (selValue.indexOf("Caller")<=-1 && ignoreFields.indexOf(selValue)<=-1) {
                    var queryField = selValue;
                    queryField = queryField.replace("[","");
                    queryField = queryField.replace("]","");
                    
                    //JSON filter builder
                    $(function() {
                        function split( val ) {
                            return val.split( /,\s*/ );
                        }
                        function extractLast( term ) {
                            return split( term ).pop();
                        }		
                        $("#idQueryValue_"+ valueID_1)
                            // don't navigate away from the field on tab when selecting an item
                            .bind( "keydown", function( event ) {
                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                        $( this ).data( "autocomplete" ).menu.active ) {
                                    event.preventDefault();
                                }
                            })
                            .autocomplete({
                                source: function( request, response ) {
                                    $.getJSON( "../_jquery/suggestBox/json-data.asp?cid="+ document.getElementById('customerID').value +"&view="+ document.getElementById('optTable').value +"&field="+ escape(queryField) , {
                                        term: extractLast( request.term )
                                    }, response );
                                },
                                search: function() {
                                    // custom minLength
                                    var term = extractLast( this.value );
                                    if ( term.length < 1 ) {
                                        return false;
                                    }
                                },
                                focus: function() {
                                    // prevent value inserted on focus
                                    return false;
                                },
                                select: function( event, ui ) {
                                    //sets up for multiple responses...
                                    //only do this IF the operator equals IN or NOT IN
                                    if (document.getElementById("idQueryExp_" + valueID_1).value == 'in' || document.getElementById("idQueryExp_" + valueID_1).value == 'not in') {
                                        var terms = split( this.value );
                                        // remove the current input
                                        terms.pop();
                                        // add the selected item
                                        terms.push( ui.item.value );
                                        // add placeholder to get the comma-and-space at the end
                                        terms.push( "" );
                                        this.value = terms.join( ", " );
                                        return false;
                                    }
                                }
                            });
                    });
                                
                }
                
            // END JSON QUERY BUILDER	
            }
        
          }
        </script>
        
        <script type="text/javascript"> 
        
          // used to remove selected row from filter table
          //   obj = Row ID being removed
          function delQueryRow(obj,label,value)
          {
            var theRow = document.getElementById(obj);
            var theTable = document.getElementById("tblFilter");
            
            for (var i = 0; i < theTable.rows.length; i++)
            {
                if (theRow == theTable.rows[i]) {
                       theTable.deleteRow(i);
                  }	
            }
            
            //add selected text back in dropdown
        ///	var selectBox = document.getElementById("optQueryField");
        ///	selectBox.options[selectBox.options.length] = new Option(label, value);
          }
        
          // used to set height and scroll bars
          // if user selected IN or NOT IN as the opearator 
          function changeElementStyle(expID,expValue) {
            var theElement = document.getElementById( "idQueryValue_" + expID.substr(11,3) );
            // user selected IN or NOT IN
            if (expValue == "in" || expValue == "not in") {
                theElement.style.overflowY = "auto";
                theElement.style.height = "42px";
                
                theElement.style.display = "";		
            }
            // regular operator...make like normal input box
            else {
                theElement.style.overflowY = "hidden";
                theElement.style.height = "13px";
                
                
                if (expValue == "is null" || expValue == "is not null") {
                    theElement.style.display = "none";
                }
                else {
                    theElement.style.display = "";
                }
                
                
            }	
          }
        </script>
        
        
        <script type="text/javascript"> 
            function checkXandY() {
                //used to check password strenght
                var objX = document.getElementById('optChartXAxis').value;
                var objY = document.getElementById('optChartYAxis').value;
        
                //check password history
                if(objX == objY) {
                    jAlert('X-Axis cannot equal the Y-Axis', 'myCM Alert');
                }
                
            }		
        </script>        
