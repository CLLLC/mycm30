<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
%>

<%
' Based off SQL Stored Procedure: 
' http://searchsqlserver.techtarget.com/tip/Stored-procedure-A-simple-way-to-perform-crosstab-operations
sub buildChart(recID,fromPage,chartOutputAs,chartSQL,whereClause)

'Declare variables
dim mySQL, cn, rs
dim field, colCount, xCol, xRow, tmpValue
dim mySQLChart

dim mySQLName
dim mySQLStatement
dim mySQLFields
dim mySQLGroup
dim mySQLGroupCalc

'Chart Settings
dim chartType
dim chartXAxis
dim chartXRotate
dim chartYAxis
dim chartYMaxValue
dim chartSize
dim	chartWidth
dim	chartHeight

dim chartLegend, chartLegendEnabled, chartLegendAlign, chartLegendVertical
dim chartLegendX, chartLegendY, chartLegendFloat, chartLegendLayout, chartTop

dim labelTop15
dim seriesAdjust
dim tableWidth

dim rowCount

	'Look for Query ID
	if recID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid chart reference.")
	end if
						
	'Get Chart Values from ReportQuery table
	if len(recID) > 0 then	
		
		'get report record
		mySQL="SELECT * " _
			& "       FROM Reports a " _
			& "		  WHERE a.ReportID = " & recID		
		'NEED USER LEVEL SECURITY????
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		if rs.eof then
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid chart reference.")
		else
			mySQLName 		= rs("Name")
			mySQLFields 	= rs("QueryFields")
			mySQLGroup 		= rs("QuerySumGroup")
			mySQLGroupCalc 	= rs("QuerySumCalculation")
			mySQLStatement	= rs("SQL")
			chartType		= rs("ChartType")
			chartXAxis		= rs("ChartXAxis")
			chartYAxis		= rs("ChartYAxis")
			chartSize		= rs("ChartSize")
			chartLegend		= rs("ChartLegend") 
			chartTop		= rs("ChartTop") 
			
			'set chart legend to OFF
			if lCase(chartLegend) = "" or chartYAxis = "" then
				chartLegendEnabled = "false"				
				chartLegendAlign = "left"
				chartLegendVertical = "top"
				chartLegendLayout = "horizontal"
				chartLegendFloat = "false"
				chartLegendX = "0"
				chartLegendY = "0"
			
			'set chart legend to ON
			else			
				chartLegendEnabled = "true"				
				
				'top-left, center-right etc.
				if len(chartLegend) > 0 then
					chartLegend = split(chartLegend,"-")
				else
					chartLegend = split("Top-Left","-")
				end if
				
				chartLegendVertical = lCase(chartLegend(0))
				chartLegendAlign = lCase(chartLegend(1))			
				
				if chartLegendVertical = "top" and chartLegendAlign = "right" then
					chartLegendY = "50"
				else
					chartLegendY = "0"
				end if

				if chartLegendAlign = "right" then
					chartLegendX = "-25"
				elseif chartLegendAlign = "left" then
					chartLegendX = "75"
				else
					chartLegendX = ""
				end if
				
				if chartLegendVertical = "bottom" then
					chartLegendFloat = "false"
					chartLegendLayout = "horizontal"
				else
					chartLegendFloat = "true"
					chartLegendLayout = "vertical"
				end if
				
			end if
			
			
			if isNull(chartTop) or chartTop = "" then
				chartTop = 15
			elseif cInt(chartTop) < 0 then
				chartTop = 15
			elseif cInt(chartTop) = 0 then
				chartTop = 1000
			end if
			
		end if
		call closeRS(rs)	
			
	end if

	'SQL statement already built and passed here...
	mySQLStatement = chartSQL

	'if necessary, refomat WHERE clause
	if len(whereClause) > 0 then
		mySQLStatement = insertWhereClause(mySQLStatement,whereClause)						
	end if

	'''DNN - moved to counting records in Crosstab	
	'Open RecordSet to get row count
	'''set rs = openRSopen(replace(chartSQL,"^","'"),3,adOpenStatic,adLockReadOnly,adCmdText,0)
	
'''	if rs.State <> adStateClosed then
		'get row count
'''		rowCount = rs.recordcount
		'open recordset 
'''		if rowCount < 1001 then
			if len(chartYAxis) > 0 then		
				'crosstab stored procedure
				'''response.redirect "../error/default.asp?errMsg=" & server.URLEncode(" EXEC sp_Crosstab '" & chartSQL & "', Null, Null, '" & chartXAxis & "', 'CRSID', 'COUNT', Null, 1 ")	
				
				on error resume next
				set rs = openRSexecute(" EXEC sp_Crosstab '" & chartSQL & "', Null, Null, '" & chartXAxis & "', 'CRSID', 'COUNT', Null, 1 ")
				if err.number <> 0 then
					set rs = openRSexecute(" SELECT CRSID FROM CRS WHERE 1=2 ")
					closeRS(rs)
				end if
				on error goto 0	
				
			else
				'group by COLUMN only in recordset
				set rs = openRSopen(chartSQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)			
			end if
			if rs.State = adStateClosed then
				'error or closed...
			else
				rowCount = rs.recordcount			
			end if
'''		end if
'''	end if

	'make sure < 1,000 and state not closed
	if rowCount > 1000 or rs.State = adStateClosed then
		'do nothing...
				
	'recordset open...keep going
	else

		dim strChartCategories, strChartSeries, strChartSeriesValues, theURL, list_field, list_field_type
		dim strChartCaptions, bolGetCaptions, arrSeriesCount, seriesCount, tempSeries, tempValue, strValue, tempLabel
		dim seriesX, arrSeriesName, arrSeriesValue
		
		dim arrChartSeriesName, arrChartSericesValue
		
		'get number of columns returned from stored procudure
		'PUT -2 IF USING TOTAL COLUMN???????
		for each field in rs.fields
			'colCount = colCount + 1
			seriesCount = seriesCount + 1
		next						
			
		redim arrSeriesName(seriesCount)
		redim arrSeriesValue(seriesCount)

		'cycle through all records to build series and values
		'only chart the TOP 15!!!!
		do while not rs.eof and xRow < chartTop
			
			tempSeries = ""
			tempValue = ""
			tempLabel = ""
			xCol = 0		'keep up with column count
			xRow = xRow + 1 'keep up with row count
				
			for each field in rs.fields		
				
				'first column/label
				if xCol = 0 then					
					'single quotes in category name thows chart error, escape with HTML code
					if len(rs(field.name)) > 0 then
						strChartCategories = strChartCategories & replace(rs(field.name),"'","&rsquo;") & "','"						
					else
						strChartCategories = strChartCategories & rs(field.name) & "','"
					end if
				else
					'pull from 1st row only
					if xRow = 1 then
						arrSeriesName(xCol) = field.name
					end if
					arrSeriesValue(xCol) = arrSeriesValue(xCol) & rs(field.name) & ","						
				end if
			
				xCol = xCol + 1
					
			next
			
			rs.Movenext
				
		loop
		
		'only continue if records found/returned
		if xRow > 0 then
		
			'show sub-title "Top 15 Charted"
			if xRow >= (chartTop-1) then
				labelTop15 = "Top " & chartTop & " Charted"
			end if
	
	
			'clean up and remove extra "," and "*|*"			
			strChartCategories = "'" & left(strChartCategories,len(strChartCategories)-3) & "'"
	
	
			if len(chartYAxis) > 0 then	
				seriesAdjust = 2
			else
				seriesAdjust = 1
			end if
	
	
			'the "-2" is because "ALL_COUNT" total column is turned on in sp_Crosstab call
			'change back to "-1" if total column is desired		
			for seriesX = 1 to (seriesCount - seriesAdjust)
				tempSeries = tempSeries & "{name: '" & arrSeriesName(seriesX) & "', data: [" & left(arrSeriesValue(seriesX),len(arrSeriesValue(seriesX))-1) & "]}"
				if seriesX + 1 < (seriesCount) then
					tempSeries = tempSeries & ", "
				end if
			next		
			strChartSeries = tempSeries			
	
			'close recordset, no longer needed
			call closeRS(rs)
	
		end if
	
	end if
	%>


	<%
    if xRow > 0 then
    	%>
        
        <!-- START CHART table -->
        <table id="chartTable" width="100%" cellpadding="0" cellspacing="0" style="border:1px solid #B3B3B3; margin-bottom:5px;">
          <tr>
            <td>
    
            <!-- jQuery Hightcharts JS http://http://www.highcharts.com -->
            <script type="text/javascript" src="../_jquery/chart/js/highcharts.js"></script>		
            
            <% if fromPage = "print" then %>
                <!-- Optional: the exporting module -->
                <script type="text/javascript" src="../_jquery/chart/js/modules/exporting.js"></script>		
            <% end if %>
            
            
            <%
            '''chartType = "barstacked"
            if lCase(chartType) = "column" then
            %>
                <!--#include file="../_jquery/chart/js/_myCM_Column.asp"-->
            <%
            elseif lCase(chartType) = "area" then
            %>
                <!--#include file="../_jquery/chart/js/_myCM_Area.asp"-->
            <%
            elseif lCase(chartType) = "line" then
            %>
                <!--#include file="../_jquery/chart/js/_myCM_Line.asp"-->
            <%
            elseif lCase(chartType) = "datezoom" then
            %>
                <!--#include file="../_jquery/chart/js/_myCM_DateZoom_WORKS_BUT_FINISH.asp"-->
            <%
            elseif lCase(chartType) = "bar" then
            %>
                <!--#include file="../_jquery/chart/js/_myCM_Bar.asp"-->
            <%
            elseif lCase(chartType) = "barstacked" then
            %>
                <!--#include file="../_jquery/chart/js/_myCM_BarStacked.asp"-->
            <%
            end if		
            %>
            
            <!-- DIV to hold new chart -->
            <div id="chartDiv" style="width: 100%; height: 300px; margin: 0 auto"></div>        
                
            </td>
          </tr>
        </table>    
        <!-- END CHART table -->
        
		<%
	end if
	
end sub
%>



