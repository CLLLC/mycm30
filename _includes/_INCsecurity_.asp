<%
'********************************************************************
' Product  : C-LIVE ComplianceLine Web Reports
' Version  : 2.0
' Modified : November 2004
' Copyright: Copyright (C) 2004 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'			 website, please contact webmaster@complianceline.com
'********************************************************************

'*************************************************************************
'Declare local configuration variables
'*************************************************************************
dim programAdmin
dim Email									'logged in user's email address
dim userIssuesView 							'used to set default SQL View used when viewing Issues
dim issuesColumns
dim userWhereClause
dim userSortField, userSortOrder
dim editReport, addReport, deleteIssues 

'--- the "viewDetails..." is not being used right now. Only the "editDetails..." is being used.
'--- ADD later... dim viewDetails, viewDetailsGeneral, viewDetailsReporter, viewDetailsSubjects, viewDetailsSummary
'--- ADD later... dim editDetails, editDetailsGeneral, editDetailsReporter, editDetailsSubjects
dim editDetailsSummary, addNewIssue, addNewFollowUp

dim viewCaseNotes, viewCaseUserFields, editCaseMgr, editOwnerNotes, editCaseResolution, editCaseResolutionApp 
dim editCaseUserFields, addInvestigations, viewInvestigations, viewAssignedInvOnly, viewDocuments, viewToolBox
dim editCaseStatus, editOwner, editDateClosed, editSource
dim profileCount							'used on tree.asp and menu.asp to show/hide customers and customer-popup
dim profileIDs								'used in all adhoc reports to supply what customer ids the user can view/access
dim accountLabelPlural, accountLabelSingle	'user on customer-popup, tree.asp and menu.asp
dim systemAlerts							'used to display system alerts established by customer
dim userGroup
dim autoSave
dim appSendFromEmail						'global FROM EMAIL address in customer profile
dim emailFrom								'global FROM EMAIL address used to send emails

'*************************************************************************
'Retrieve configuration settings from DB and load into local variables
'*************************************************************************
function loadUser(cid)

	'Work variables
	dim mySQL, rsTemp
	
	on error resume next
	
	'user logged in
	if cLng(session(session("siteID") & "adminLoggedOn")) > 0 then

		'get user security levels... 
		'	USE GROUPING TO ALLOW USER IN MULTIPLE GROUPS AND 
		'	ONLY PULL LOWEST SETTING (I.E. Y/N WOULD PULL N)		
		'	--> MOVE DELETEREPORT TO GROUP_ISSUETYPE TABLE
		mySQL = "SELECT a.Email, a.programAdmin, " _
			  & 	"a.editReport, a.addReport, a.deleteIssues, " _
			  &		"a.sortField, a.sortOrder, " _
			  &		"a.whereClause, a.issuesColumns, a.issuesView, a.autosave, a.viewToolBox, " _
			  &		"Groups.GroupID, " _
			  &		"Groups.editDetailsSummary, Groups.addNewIssue, Groups.addNewFollowUp, " _
			  &		"Groups.viewCaseNotes, Groups.viewCaseUserFields, Groups.editCaseMgr, Groups.editOwnerNotes, " _
			  &		"Groups.editCaseResolution, Groups.editCaseResolutionApp, Groups.editCaseUserFields, Groups.addInvestigations, Groups.viewInvestigations, Groups.viewAssignedInvOnly, Groups.viewDocuments, " _
			  &		"Groups.editCaseStatus, Groups.editOwner, Groups.editDateClosed, Groups.editSource " _			  
			  & "FROM (Logins AS a LEFT JOIN Logins_Groups ON a.LOGID = Logins_Groups.LOGID) LEFT JOIN Groups ON Logins_Groups.GroupID = Groups.GroupID " _
			  & "WHERE  a.logid = " & session(session("siteID") & "logid")
		set rsTemp = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		if not rsTemp.eof then	
		
			userGroup 			= rsTemp("GroupID")
			userGroup 			= "*|*" & userGroup & "*|*" 'NEED TO LOOP AND ADD ALL GROUPS TO THIS
		
			Email					= rsTemp("Email")	
			programAdmin 			= rsTemp("programAdmin")		
			editReport				= rsTemp("editReport")
			addReport				= rsTemp("addReport")
			deleteIssues			= rsTemp("deleteIssues")			
			userSortField			= rsTemp("sortField")
			userSortOrder			= rsTemp("sortOrder")		
			userWhereClause			= rsTemp("whereClause")					
			issuesColumns			= rsTemp("issuesColumns")			
			userIssuesView			= rsTemp("issuesView")		
			autoSave				= rsTemp("AutoSave")		
			editDetailsSummary		= rsTemp("editDetailsSummary")		
			addNewIssue				= rsTemp("addNewIssue")		
			addNewFollowUp			= rsTemp("addNewFollowUp")					
			viewCaseNotes 			= rsTemp("viewCaseNotes")				
			editCaseStatus			= rsTemp("editCaseStatus")	
			editOwner				= rsTemp("editOwner")	
			editDateClosed			= rsTemp("editDateClosed")	
			editSource				= rsTemp("editSource")				
			viewCaseUserFields		= rsTemp("viewCaseUserFields")
			editCaseMgr 			= rsTemp("editCaseMgr")	
			editOwnerNotes			= rsTemp("editOwnerNotes")	
			editCaseResolution 		= rsTemp("editCaseResolution")	
			editCaseResolutionApp 	= rsTemp("editCaseResolutionApp")	
			editCaseUserFields 		= rsTemp("editCaseUserFields")			
			addInvestigations		= rsTemp("addInvestigations")			
			viewInvestigations		= rsTemp("viewInvestigations")		
			viewAssignedInvOnly		= rsTemp("viewAssignedInvOnly")
			viewDocuments			= rsTemp("viewDocuments")
			viewToolBox				= rsTemp("viewToolBox")
		else
			loadUser = false	
		end if
		call closeRS(rsTemp)

		'----------------------------------------------
		'GROUP ACCESS
		'	-pull all user group assignemnts
		'----------------------------------------------	
		' **** THIS WORKS, BUT NEED TO CYCLE THROUGH
		' **** ARRAY TO VERIFY GROUP ASSIGNMENTS		
		'userGroup = ""
		'mySQL = "SELECT Groups.GroupID " _			  
		'	  & "FROM (Logins AS a LEFT JOIN Logins_Groups ON a.LOGID = Logins_Groups.LOGID) LEFT JOIN Groups ON Logins_Groups.GroupID = Groups.GroupID " _
		'	  & "WHERE  a.logid = " & session(session("siteID") & "logid")
		'set rsTemp = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,0)
		'if not rsTemp.eof then
		'	do until rsTemp.eof
		'		userGroup = userGroup & "*|*" & rsTemp("GroupID")
		'		rsTemp.movenext
		'	loop
		'	userGroup = userGroup & "*|*"
		'end if
		'call closeRS(rsTemp)

		'----------------------------------------------
		'PROFILE ACCESS for requested customer
		'	-also pull some global customer settings
		'	-only called if CUSTOMERID is provided, not required
		'----------------------------------------------	
		if (loadUser <> false) or loadUser="" or isNull(loadUser) then		
			if cid > "" and len(cid) > 0 then
				'DBAdmin and CCI Admins
				if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
					mySQL = "SELECT Customer.Name, appSendFromEmail " _
						  & "	FROM Customer " _
						  & "	WHERE Customer.CustomerID='" & cid & "' " _
						  & "	ORDER BY Name "
				
				'CCI staff RA/RS
				elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
					mySQL = "SELECT Customer.Name, appSendFromEmail " _
						  & "	FROM Customer " _
						  & "	WHERE Customer.CustomerID='" & cid & "' AND Canceled=0 AND " _ 
						  & "	 	EXISTS ( " _
						  & "			SELECT Customer_IssueType.CustomerID " _
						  & "				FROM Customer_IssueType " _
						  & "				WHERE (Customer_IssueType.CustomerID=Customer.CustomerID) AND (Customer_IssueType.IssueTypeID=1 OR Customer_IssueType.IssueTypeID=2 OR Customer_IssueType.IssueTypeID=4) ) " _
						  & "	ORDER BY Name "
			
				'everyone else...based on group assignments
				else
					mySQL = "SELECT Customer.Name, appSendFromEmail " _
						  & "	FROM Customer " _
						  & "	WHERE Customer.CustomerID='" & cid & "' AND Canceled=0 AND " _ 
						  & "			EXISTS ( " _
						  & "			SELECT vwLogins_IssueType.CustomerID, vwLogins_IssueType.LogID " _
						  & "				FROM vwLogins_IssueType " _
						  & "				WHERE (vwLogins_IssueType.CustomerID=Customer.CustomerID) AND (vwLogins_IssueType.LogID=" & sLogid & ") ) "
			
				end if	
				set rsTemp = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
				'NO access bail out!
				if rsTemp.eof then
					loadUser = false
				'GOOD to access account
				else
					loadUser = true
					appSendFromEmail = rsTemp("appSendFromEmail")
				end if
				call closeRS(rsTemp)							
			end if
			
		end if

		'----------------------------------------------
		'EMAIL FROM address to use on ALL outbound emails
		'	-this is necessary as some email servers
		'	 see the FROM address as user@client.com
		'	 and the sending domain as "ccius.com"
		'	 and consider it SPOOFING and block the email.
		'----------------------------------------------	
		if len(appSendFromEmail) > 0 then 
			emailFrom = appSendFromEmail 	'use global setting to send email from
		else 
			emailFrom = email				'user logged in user's email address for sending emails
		end if					

		'----------------------------------------------
		'PROFILE LABELS (i.e. Customers, Accoutns)
		'----------------------------------------------	
		if (loadUser <> false) or loadUser="" or isNull(loadUser) then
			'All CCI Staff...
			if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then		
				mySQL = "SELECT CustomerID " _
					  & "	FROM Customer " _
					  & "	WHERE Canceled<>-1 "
					  
			'everyone else...
			else
				mySQL = "SELECT CustomerID " _
					  & "	FROM vwLogins_IssueType " _
					  & "	WHERE LOGID = " & session(session("siteID") & "logid") & " " _
					  & "	GROUP BY CustomerID "			
			end if
			set rsTemp = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,0)
			'used to decide to pop popup_profile.asp versus straight to issue_edit.asp
			profileCount = rsTemp.recordcount
			'make a string of all ALLOWED CustomerIDs for non-CCI users
			if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then					
				profileIDs = ""
				do while not rsTemp.eof
					profileIDs = profileIDs & "'" & rsTemp("CustomerID") & "',"
					rsTemp.movenext
				loop
				profileIDs = left(profileIDs,len(profileIDs)-1)
			end if			
			call closeRS(rsTemp)			
			'set label for account pages and pop-ups
			if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
				accountLabelPlural = "Customers"
				accountLabelSingle = "Customer"
			else
				accountLabelPlural = "Accounts"
				accountLabelSingle = "Account"
			end if	
		end if
		
		'----------------------------------------------
		'SYSTEM NOTIFICATIONS for all profiles for user
		'----------------------------------------------	
		if (loadUser <> false) or loadUser="" or isNull(loadUser) then
			mySQL = "SELECT vwLogins_IssueType.CustomerID,  Alerts.AlertID, Alerts.Alert " _
				  & "	FROM vwLogins_IssueType INNER JOIN Alerts ON vwLogins_IssueType.CustomerID = Alerts.CustomerID " _
				  & "	WHERE Alerts.Begins <= '" & Date() & "' AND Alerts.Ends > '" & Date() & "' AND vwLogins_IssueType.LOGID = " & session(session("siteID") & "logid") & " " _
				  & "	GROUP BY vwLogins_IssueType.CustomerID, Alerts.AlertID, Alerts.Alert "
			set rsTemp = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,0)
			if not rsTemp.eof then
				do until rsTemp.eof
					systemAlerts = systemAlerts & "*|*" & rsTemp("alertid") & ":" & rsTemp("alert")
					rsTemp.movenext
				loop
				systemAlerts = systemAlerts & "*|*"
			end if
			call closeRS(rsTemp)
		end if
								
		'check for errors access user information
		if (loadUser <> false) or loadUser="" or isNull(loadUser) then		
			if err.number = 0 then
				loadUser = true
			else
				loadUser = false
			end if
		end if
		
	else		
		loadUser = false
	end if

	on error goto 0
	
end function
%>

