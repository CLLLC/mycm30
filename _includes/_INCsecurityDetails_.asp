        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Account div -->
        <div id="tab:details">
        
        
            <!-- START User Account table -->   
	        <table id="table_details" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>
                    
                    <!-- START Middle Section -->    
                    <td valign="top">
                                        
					  	<!-- START Security Fields -->	                    
						<table id="table:details:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">

                            <tr>
                              	<td class="formLabel" style="background-color:#FFFFC8;">Delivery System:</td>
                              	<td style="background-color:#FFFFC8;" align=left>        
                                    <select name="cusDeliverySystem" id="cusDeliverySystem" size=1 class="inputMedium" onchange="showDelivery();">
						                <option value="">-- Select --</option>                                                                        
                                        <option value="CLive" <%=checkMatch(cusDeliverySystem,"CLive")%>>CLive</option>                                        
                                        <option value="Manual" <%=checkMatch(cusDeliverySystem,"Manual")%>>Fax/Email</option>
                                        <option value="myCM" <%=checkMatch(cusDeliverySystem,"myCM")%>>myCM</option>                                
                                        <option value="XML" <%=checkMatch(cusDeliverySystem,"XML")%>>XML Automation</option>                                                                        
                                    </select>                                               
                                	<div class="subLabel" style="padding-top:5px;">Mechanism used to deliver newly created issues.</div>                                    
                              	</td>
                            </tr>                            

                            <tr id="userlimitTR">
                            	<td class="formLabel">User Limit:</td>
                                <td align=left>
                                	<!-- DB Admins ONLY security level = 1 -->
                                    <% if cLng(session(session("siteID") & "adminLoggedOn")) >= 2 then %>
                              			<div class="subLabel">Current active users: <% =appUserCount %></div>
                                   		<div class="subLabel" style="padding-top:5px; padding-bottom:5px;">User count allowed: <% =cusNumUsers %></div>                                        
                                        <input type=hidden name="cusNumUsers" id="cusNumUsers" value="<% =cusNumUsers %>">
                                    <% else %>
	                                    <div class="subLabel" style="padding-bottom:5px;">Current active users: <% =appUserCount %></div>
		                                <input name="cusNumUsers" id="cusNumUsers" class="inputShort" value="<% =cusNumUsers %>" maxlength="255" />
                                    <% end if %>
									<%
                                   	dim countMsg
                                   	countMsg = "The numbers below are based on profile settings and<br/>your myCM contract. To increase your user limit please<br/>contact CCI at <strong>1-800-617-0415</strong> or<br/>e-mail <strong>support@ccius.com</strong>.<br/><br/>"
                                   	%>                                                                                                                                
                                    <div class="subLabel" style="padding-top:5px;">Numeric entries only, no special characters. To increase your User Limit <a href="#" onclick="jAlert('<% =countMsg %>', 'Program Administrator'); return false;">contact CCI</a>.</div>
                                </td>
                            </tr>
							<script type="text/javascript">
                                jQuery().ready(function($) {
                                    $('#cusNumUsers').spinner({ min: 0, step: 1, increment: 'fast' });			
                                });
                            </script>
                            

                            <tr id="passwordTR">
                              	<td class="formLabel">Password Strength:</td>
                              	<td colspan="3" align=left nowrap>        
                                    <select name="cusPasswordStrength" id="cusPasswordStrength" size=1 class="inputMediumLong" onchange="showStrength();">
                                        <option value="Weak" <%=checkMatch(cusPasswordStrength,"Weak")%>>Weak</option>                                    
                                        <option value="Medium" <%=checkMatch(cusPasswordStrength,"Medium")%>>Medium</option>                                        
                                        <option value="Strong" <%=checkMatch(cusPasswordStrength,"Strong")%>>Strong</option>
                                    </select>               
                                    <%
									if cusPasswordStrength = "Weak" then
                                		response.write("<div id=""passStrength"" class=""subLabel"" style=""padding-top:5px;"">")
										response.write("A setting of <strong>Weak</strong> enforces:<br />")
										response.write("&nbsp;&nbsp;- password reset every 365 days<br />")
										response.write("&nbsp;&nbsp;- password cannot match any of the past 6 passwords used")
										response.write("</div>")
									elseif cusPasswordStrength = "Medium" then
                                		response.write("<div id=""passStrength"" class=""subLabel"" style=""padding-top:5px;"">")
										response.write("A setting of <strong>Medium</strong> enforces:<br />")
										response.write("&nbsp;&nbsp;- at least 6 characters<br />")
										response.write("&nbsp;&nbsp;- at least 1 lower-case letter<br />")
										response.write("&nbsp;&nbsp;- at least 1 upper-case letter<br />")
										response.write("&nbsp;&nbsp;- password reset every 180 days<br />")
										response.write("&nbsp;&nbsp;- password reset if 'forgot password' email requested<br />")																				
										response.write("&nbsp;&nbsp;- password cannot match any of the past 6 passwords used")
										response.write("</div>")
									elseif cusPasswordStrength = "Strong" then
                                		response.write("<div id=""passStrength"" class=""subLabel"" style=""padding-top:5px;"">")
										response.write("A setting of <strong>Strong</strong> enforces:<br />")
										response.write("&nbsp;&nbsp;- at least 8 characters<br />")
										response.write("&nbsp;&nbsp;- at least 1 lower-case letter<br />")
										response.write("&nbsp;&nbsp;- at least 1 upper-case letter<br />")
										response.write("&nbsp;&nbsp;- at least 1 special character<br />")
										response.write("&nbsp;&nbsp;- password reset every 60 days<br />")
										response.write("&nbsp;&nbsp;- password reset if 'forgot password' email requested<br />")																				
										response.write("&nbsp;&nbsp;- password cannot match any of the past 6 passwords used")
										response.write("</div>")
									end if										                                    
									%>                                    
                              	</td>
                            </tr>                            

                            <tr id="emailTR">
                            	<td class="formLabel">E-mail Override:</td>
                                <td align=left>
	                                <input name="cusSendFromEmail" id="cusSendFromEmail" class="inputMediumLong" value="<% =cusSendFromEmail %>" maxlength="255" />
                                    <div class="subLabel" style="padding-top:5px;">
                                    	E-mail address to use as <strong>FROM</strong> address in all e-mails send from myCM.<br />
										&nbsp;&nbsp;- Customers who have strict e-mail policies may need this option.<br />
                                        &nbsp;&nbsp;- If used, all emails sent from myCM will be delivered from the address provided.
                                    </div>
                                </td>
                            </tr>

                            <tr id="ownershipTR">
                            	<td class="formLabel">Owner Override:</td>
                                <td align=left>
	                                <input name="cusOwnerOverride" id="cusOwnerOverride" class="inputMediumLong" value="<% =cusOwnerOverride %>" maxlength="255" />
                                    <div class="subLabel" style="padding-top:5px;">
                                    	Sets/Removes default ownership for all newly added issues.<br />
                                        &nbsp;&nbsp;- Enter <strong>Zero (0)</strong> to disable ownership assignemnt.<br />
                                        &nbsp;&nbsp;- Enter <strong>User's LOGID</strong> to set default ownnership for all issues.
                                    </div>
                                </td>
                            </tr>

						</table>
   						<!-- END Security Form Fields -->                                                                                                                                       

                    </td>
                    <!-- STOP Middle Section -->			                    
                                        
                </tr>
            
            </table>
            <!-- END User Account table -->
	
    	</div>
		<!-- STOP Account div -->        
        
		<script language="javascript">
			function showStrength() {
				var el = document.getElementById('passStrength');
				var elSel = document.getElementById('cusPasswordStrength');
				var weak = "<div id=\"passStrength\" class=\"subLabel\" style=\"padding-top:5px;\">A setting of <strong>Weak</strong> enforces:<br />&nbsp;&nbsp;- password reset every 365 days<br />&nbsp;&nbsp;- password cannot match any of the past 6 passwords used</div>";
				var medium = "<div id=\"passStrength\" class=\"subLabel\" style=\"padding-top:5px;\">A setting of <strong>Medium</strong> enforces:<br />&nbsp;&nbsp;- at least 6 characters<br />&nbsp;&nbsp;- at least 1 lower-case letter<br />&nbsp;&nbsp;- at least 1 upper-case letter<br />&nbsp;&nbsp;- password reset every 180 days<br />&nbsp;&nbsp;- password reset if 'forgot password' email requested<br />&nbsp;&nbsp;- password cannot match any of the past 6 passwords used</div>";
				var strong = "<div id=\"passStrength\" class=\"subLabel\" style=\"padding-top:5px;\">A setting of <strong>Strong</strong> enforces:<br />&nbsp;&nbsp;- at least 8 characters<br />&nbsp;&nbsp;- at least 1 lower-case letter<br />&nbsp;&nbsp;- at least 1 upper-case letter<br />&nbsp;&nbsp;- at least 1 special character<br />&nbsp;&nbsp;- password reset every 60 days<br />&nbsp;&nbsp;- password reset if 'forgot password' email requested<br />&nbsp;&nbsp;- password cannot match any of the past 6 passwords used</div>";

				if (elSel.options[elSel.selectedIndex].value == 'Weak') el.innerHTML = weak;
				else if (elSel.options[elSel.selectedIndex].value == 'Medium') el.innerHTML = medium;					
				else if (elSel.options[elSel.selectedIndex].value == 'Strong') el.innerHTML = strong;
			}

			function showDelivery() {
				var el = document.getElementById('cusDeliverySystem').value;
				if (el == "myCM") {
					document.getElementById('userlimitTR').style.display = '';
					document.getElementById('passwordTR').style.display = '';
					document.getElementById('emailTR').style.display = '';
					document.getElementById('ownershipTR').style.display = '';					
				} else {
					document.getElementById('userlimitTR').style.display = 'none';
					document.getElementById('passwordTR').style.display = 'none';
					document.getElementById('emailTR').style.display = 'none';					
					document.getElementById('ownershipTR').style.display = 'none';										
				}				
			}
			//load initial visibility
			showDelivery();
		</script>                                                            														
