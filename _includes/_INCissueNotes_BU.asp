        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
         
        __ AUTOSAVE Note: Any hidden form element that is changed/modified must be set to style="display:none;"
        __ and NOT be a hidden element otherwise FireFox will not recognize the element has changed and 
        __ WILL NOT be saved during the autosave process.
        -->       

		<!-- START Notes div, defaults to style="display:none;" -->
        <div id="tab:notes" style="display:none;">
        
        
            <!-- START Issue Notes table -->   
	        <table id="table_notes" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>

	                <!-- START Side Page Tabs [genearl] "id=table:notes:general" --> 
                    <!-- [this = object makeing the call ] -->
                    <!-- [notes = current top tab tab ] -->
                    <!-- [information = selected side tab ] -->
                    <td rowspan="4" valign="top" style="width:100px; border-top:none; padding:0px; ">
                        <div class="tabSide" style="width:100px;">
                            <div id="information" class="on" onClick="clickSideTab('notes','information');">Information</div>			
                            
							<% 
							if uCase(viewInvestigations) = "N" then 
								'do nothing...
							else
							%>                         
	                            <div id="investigation" class="off" onClick="clickSideTab('notes','investigation');">Investigation</div>
                            <% end if %>
                            
							<% 
							if uCase(viewDocuments) = "N" then 
								'do nothing...
							else
							%>                         
	                            <div id="documentation" class="off" onClick="clickSideTab('notes','documentation');">Documents</div>
                            <% end if %>
                            
							<% 
							if uCase(viewCaseUserFields) = "N" then 
								'do nothing...
							else
							%>                         
	                            <div id="customfields" class="off" onClick="clickSideTab('notes','customfields');">User Fields</div>
                            <% end if %>
                            
							<% 
							if useResolutions = "" or useResolutions = "N" or editCaseResolution = "N" then
								'do nothing...
							else
							%>                         
	                            <div id="resolution" class="highlight" onClick="clickSideTab('notes','resolution');">Resolution</div>        
                            <% end if %>                            
                                                   
                            <div class="empty">&nbsp;</div>		
                        </div>                        
                    </td>
                    <!-- STOP Side Page Tabs -->
                    
                    <!-- START Middle Section -->    
                    <td valign="top">
                                        
					  	<!-- START Information Form Fields -->	                    
					  	<table id="table:notes:information" width="100%" cellpadding="0" cellspacing="0">                                                                       
							<tr>
                            	<td>                                                          
                            
                                    <table width="100%" class="formTable" cellpadding="0" cellspacing="0">       
                                    
										<%                                         
										if editCaseStatus<>"N" then 
										%>                                                                 
                                        <tr>                            
                                            <td class="formLabel">
	                                            <!-- DBAdmins and Program Admins -->
												<% if programAdmin = "Y" or cLng(session(session("siteID") & "adminLoggedOn")) < 2 then %>
                                                	<a href="#" class="formLabelLinkImg" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=customers&field=pCaseStatus&form=rptStatus', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_Status','over');" onmouseout="javascript:configImage('congif_Status','out');"><img id="congif_Status" src="../_images/x_cleardot_16.gif" align="absmiddle" border="0" style="vertical-align:middle;"/></a>
                                                    <a href="#" class="formLabelLink" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=customers&field=pCaseStatus&form=rptStatus', 320, 375, 'no'); StopTheTimer(); return false;" onmouseover="javascript:configImage('congif_Status','over');" onmouseout="javascript:configImage('congif_Status','out');"><span class="emailTrigger">e</span>Status:</a>
                                                <% else %>
                                                    <span class="emailTrigger">e</span>Status:
                                                <% end if %>
                                            </td>
                                            <td align="left">                                
                                            	<%
												dim statusLock : statusLock = " "
                                              	'Build array for dropdown selections
                                           		optArr = getDropDownOpt("pCaseStatus",customerID)			
												
												'user not owner, check settings									
												if rptLogID<>sLogid then
													'group is status=locked
													if editCaseStatus="L" then
														statusLock = " disabled"
													else	
														'not admin and not owner so lock out!		 								
														if (cLng(session(session("siteID") & "adminLoggedOn"))>=10) and (editCaseStatus<>"Y") and (programAdmin <> "Y") and (rptLogID<>sLogid) then 
															'call buildSelect(optArr,rptStatus,"rptStatus","inputMediumLong","N")
															statusLock = " disabled"
														end if
													end if
												
												end if
												
												
                                            	%>                                                
												<select name="rptStatus" id="rptStatus" onchange="status_update()" size=1 class="inputMediumLong" <% =statusLock %>>
													<%									
													labelX = 0
													response.write("<option value="""">-- Select --</option>")
													for labelX = 0 to UBound(optArr)
														response.write "<option value='" & optArr(labelX) & "' " & checkMatch(rptStatus, optArr(labelX)) & ">" & optArr(labelX) & "</option>"
													next
													%>
												</select>                                                                            
                                                <div class="subLabel" style="padding-top:5px;">
                                                    All assigned users will be notified on status changes.
                                                </div>
                                            </td>
                                        </tr>
                                        <% else %>
                                            <!-- These elements MUST be set to style="display:none;" and NOT type="hidden" -->
                                            <!-- when set as "hidden" FireFox does not recognize any change has been made  -->                                                
                                            <input name="rptStatus" id="rptStatus" style="display:none;" value="<% =rptStatus %>" >                                        
                                        <% end if %>


										<%                                         
										if editOwner<>"N" then %>                                                                 
                                            <tr>
                                              <td class="formLabel"><span class="emailTrigger">e</span>Owner:</td>
                                              <td align=left nowrap style="padding-bottom:0px;">                                                            	 
                                                <div style="float:left;">
                                                    <input name="rptOwner" id="rptOwner" class="inputMediumLong" value="<% =rptOwner %>" maxlength="255" readonly="readonly" />
                                                    <!-- These elements MUST be set to style="display:none;" and NOT type="hidden" -->
                                                    <!-- when set as "hidden" FireFox does not recognize any change has been made  -->                                                
                                                    <input name="rptLogid" id="rptLogid" style="display:none;" value="<% =rptLogid %>" >
                                                </div>
                                                <div <% if (cLng(session(session("siteID") & "adminLoggedOn"))>=10) and (editOwner<>"Y") and (programAdmin <> "Y") and (rptLogID <> sLogid) then response.write("style=""display:none;""") %>>
                                                    <a class="myCMbuttonSmall" href="#" title="Change Owner" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_user.asp?recid=<% =idIssue %>&cid=<% =customerID %>&multi=false', 380, 450, 'no'); StopTheTimer(); resetSystemMsg('systemMessage');"><span class="user" style="padding-right:7px;">&nbsp;</span></a>
                                                </div>
                                              </td>
                                            </tr>                                        
                                            <tr>
                                              <td style="border-top:none; padding-bottom:5px;">&nbsp;</td>
                                              <td align=left nowrap style="border-top:none; padding:0px;">                               
                                                <div class="subLabel" >Owner has full control over issue and its settings.</div>
                                              </td>
                                            </tr>
                                        <% else %>
                                            <!-- These elements MUST be set to style="display:none;" and NOT type="hidden" -->
                                            <!-- when set as "hidden" FireFox does not recognize any change has been made  -->                                                
                                            <input name="rptLogid" id="rptLogid" style="display:none;" value="<% =rptLogid %>" >                                        
                                        <% end if %>
                                    

										<%                                         
										if editDateClosed<>"N" then 
											%>                                                                                                     

                                            <tr>
                                                <td class="formLabel"><span class="emailTrigger">e</span>Deadline:</td>
                                                <td align="left">                                
                                                   <input name="rptDateDeadline" id="date:rptDateDeadline" type=text class="inputShort" value="<% =rptDateDeadline %>" size=10 maxlength=10 <% if (cLng(session(session("siteID") & "adminLoggedOn"))>=10) and (editDateClosed<>"Y") and (programAdmin <> "Y") and (rptLogID <> sLogid) then response.write(" disabled")%>>
                                                   <div class="subLabel" style="padding-top:5px;">Date issue is due to be completed: 
														<%
                                                        if len(useDeadlineDays)<=0 or useDeadlineDays="" or isNull(useDeadlineDays) then
                                                            response.write("<i>&lt;default not set&gt;</i>")
                                                        else
                                                            response.write("<strong>" & useDeadlineDays & " days</strong>")
                                                        end if														
                                                       %>
                                                    </div>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td class="formLabel">Date Closed:</td>
                                                <td align="left">                                
                                                    <div id="IssueDateClosed">                                        
                                                        <!-- RPTDATECLOSED ON OLD WAS...DUE DATE...NOW IM USING IT AS CLOSEDATE -->
                                                        <input name="rptDateClosed" id="date:rptDateClosed" type=text class="inputShort" value="<% =rptDateClosed %>" size=10 maxlength=10 <% if (cLng(session(session("siteID") & "adminLoggedOn"))>=10) and (editDateClosed<>"Y") and (programAdmin <> "Y") and (rptLogID <> sLogid) then response.write(" disabled")%>>
                                                        <%
                                                        if lCase(rptStatus) <> "closed" then 				
                                                            response.write(" [Days Open: <span style=""color:#D40000;"">" & (Date()-rptDate) & "</span>]")
                                                            response.write(" <span class=""subLabel""><em>&lt;This report is not yet closed&gt;</em></span>")
                                                        else			
                                                            if isDate(rptDateClosed) then
                                                                'USED TO USE... rptDaysOpen -- might again in the future
                                                                response.write(" [Days Open: <span style=""color:#D40000;"">" & (rptDateClosed-rptDate) & "</span>]")
                                                            else
                                                                response.write("&nbsp;")
                                                            end if
                                                        end if
                                                        %>
                                                        <div class="subLabel" style="padding-top:5px;">Date status was set to: <strong><% =pCaseStatusCloseOn %></strong></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            
                                        <% else %>
                                            <!-- These elements MUST be set to style="display:none;" and NOT type="hidden" -->
                                            <!-- when set as "hidden" FireFox does not recognize any change has been made  -->                                                
                                            <input name="rptDateClosed" id="date:rptDateClosed" style="display:none;" value="<% =rptDateClosed %>" >                                        
                                        <% end if %>
                                                                                                        

										<% 
										if editCaseMgr<>"N" then %>                                                                    
										<tr>
                                          <td class="formLabel"><span class="emailTrigger">e</span>Case Managers:</td>    
                                          
                                          <!-- START User/Contact Table -->                         
                                          <td align="left"> 
                                            <img src="../_images/icons/16/help.png" width="16" height="16" align="absmiddle" style="margin-right:5px; margin-bottom:2px; vertical-align:middle;"/><span style="color:#2d73b2;">Double-click on a user to view details.</span>
                                            <br>                                                                          
                                            <select name="listUser" id="listUser" ondblclick="userLookUp('listUser'); StopTheTimer();" size="5" style="width:85%; height:100px; margin-bottom:5px;" multiple="multiple">
                                                <!--  Find Users ALREADY assigned -->
                                                <%
                                                dim tempFUPUser, tempFUPUserPermission
                                                'pull original case managers
		                                        if action = "addfu" and appAssignFUPLogid = "Y" then
                                                    mySQL="SELECT a.LogID, b.CustomerID AS [Logins.CustomerID], b.FirstName, b.LastName, a.ReadOnly, b.Email, b.Priority, b.Active " _
                                                        & "FROM   CRS_Logins a LEFT JOIN Logins b " _
                                                        & "ON     a.LogID = b.LogID " _
                                                        & "WHERE  a.CRSID = '" & Mid(idIssue,1,Len(idIssue)-2) & "01" & "' " _
                                                        & "ORDER BY b.LastName, b.FirstName"
                                                          
                                                'pull current case managers
                                                else
                                                    mySQL="SELECT a.LogID, b.CustomerID AS [Logins.CustomerID], b.FirstName, b.LastName, a.ReadOnly, b.Email, b.Priority, b.Active " _
                                                        & "FROM   CRS_Logins a LEFT JOIN Logins b " _
                                                        & "ON     a.LogID = b.LogID " _
                                                        & "WHERE  a.CRSID = '" & idIssue & "' " _
                                                        & "ORDER BY b.LastName, b.FirstName"
                                                end if							
                                                'open recordset
												set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                                do while not rs.eof
													tempUser = ""
                                                    if len(rs("LastName")) > 0 then tempUser = rs("LastName")
                                                    if len(rs("FirstName")) > 0 and len(tempUser) > 0 then tempUser = tempUser & ", " & rs("FirstName") else tempUser = rs("FirstName")
                                                    if len(rs("Email")) > 0 then tempUser = tempUser & " [" & rs("Email") & "]"
													
													'show users status and setting on this issue
													if rs("Active") = "C" then
														tempUser = tempUser & " (contact only)"
													elseif rs("Active") = "N" then	
														tempUser = tempUser & " (disabled)"
													else
                                                    	if rs("ReadOnly") = "Y" then 
															tempUser = tempUser & " (read only)" 
														else
															tempUser = tempUser & " (full access)"
														end if
													end if
													
													'show linked from different profile
													if lCase(customerID) <> lCase(rs("Logins.CustomerID")) then tempUser = tempUser & " ([linked:" & rs("Logins.CustomerID") & "])"
													
                                                    'add user to list
                                                    Response.Write "<option value=""" & rs("LogID") & """>" & tempUser & "</option>"

													'for adding case managers to follow-ups
													tempFUPUser = tempFUPUser & "," & rs("LogID")
													if rs("ReadOnly") = "Y" then
														tempFUPUserPermission = tempFUPUserPermission & "," & rs("LogID") & ":Y"
													end if													
													
                                                    rs.movenext
                                                loop
                                                call closeRS(rs)									  
                                                %>
                                            </select>
                                            
			                                <!-- These elements MUST be set to style="display:none;" and NOT type="hidden" -->
               				                <!-- when set as "hidden" FireFox does not recognize any change has been made  -->                                                                                            
                                            <% if action = "add" then %>
	                                            <input name="listUserAdded" id="listUserAdded" style="display:none;" value="<% =sLogid %>" />                                            
                                                <input name="listUserPermission" id="listUserPermission" style="display:none;" value="" />
                                            <% elseif action="addfu" then %>
                                                <input name="listUserAdded" id="listUserAdded" style="display:none;" value="<% =tempFUPUser %>" />
                                                <input name="listUserPermission" id="listUserPermission" style="display:none;" value="<% =tempFUPUserPermission %>" />
                                            <% else %>
	                                            <input name="listUserAdded" id="listUserAdded" style="display:none;" value="" />          
                                                <input name="listUserPermission" id="listUserPermission" style="display:none;" value="" />                                  
                                            <% end if %>                                            
                                            
                                            <input name="listUserRemoved" id="listUserRemoved" style="display:none;" value="" />
                                            
                                            <br>                                            
                                            
                                            <% 
											'case manager ADD/DEL BUTTONS
											'not admin and not owner so lock out!											
											if (cLng(session(session("siteID") & "adminLoggedOn"))>=10) and (editOwner<>"Y") and (programAdmin <> "Y") and (rptLogID <> sLogid) then 
												'do nothing --  USED to have 'permission denied' here, but i like this better
                                            else 
											%>
                                                <div style="float:left; width:175px;">
                                                    <a class="myCMbutton" href="#" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_user.asp?recid=<% =idIssue %>&cid=<% =customerID %>&multi=true', 380, 450, 'no'); StopTheTimer(); resetSystemMsg('systemMessage'); return false;"><span class="add" style="padding-right:10px;">Add</span></a>
                                                    <a class="myCMbutton" href="#" onclick="this.blur(); javascript:removeUser('listUser'); resetSystemMsg('systemMessage'); return false;"><span class="delete" style="padding-right:7px;">Delete</span></a>
                                                </div>
                                                <div>
                                                    <a class="myCMbutton" href="#" onclick="this.blur(); javascript:setUserFull('listUser'); resetSystemMsg('systemMessage'); return false;"><span class="unlock" style="padding-right:10px;">Full Access</span></a>
                                                    <a class="myCMbutton" href="#" onclick="this.blur(); javascript:setUserReadOnly('listUser'); resetSystemMsg('systemMessage'); return false;"><span class="lock" style="padding-right:10px;">Read Only</span></a>
                                                </div>
                                            <% end if %>                                            
                                          </td>										  
                              
                                        </tr>                                                                                                                                                        
                                        <% else %>
                                            <!-- These elements MUST be set to style="display:none;" and NOT type="hidden" -->
                                            <!-- when set as "hidden" FireFox does not recognize any change has been made  -->                                                
                                            <input name="listUserAdded" id="listUserAdded" style="display:none;" value="" />          
                                            <input name="listUserPermission" id="listUserPermission" style="display:none;" value="" />                                                                                                         
                                        <% end if %>
                                        
                                    </table>						
                                  
								</td>                                                            
                            </tr>  


							<% 
							if editOwnerNotes<>"N" then %>                                                                    
							<tr>
                            	<td>                              

                                  	<!-- START Owner Notes -->                              
	                              	<% if (uCase(editOwnerNotes)="Y") or (programAdmin="Y") or (rptLogID=sLogid) or (cLng(session(session("siteID") & "adminLoggedOn")) < 3) then %>
                                    <table width="100%" class="formTable" cellpadding="0" cellspacing="0">                                                                                                  	
                                        <tr> 
                                            <td align="left" style="padding-left:20px; padding-right:30px;">
                                                <div style="padding:3px;">
                                                    <span style="color:#35487B; font-weight:bold; margin-right:5px;">Owner Notes</span>
                                                    <span class="subLabel">(Additional notes, log entries)
                                                    <!-- DBAdmins, Program Admins or Issue Owner -->
                                                    <% if (cLng(session(session("siteID") & "adminLoggedOn")) < 3) or (programAdmin="Y") or (rptLogID=sLogid) then %>
	                                                  	[<img src="../_images/icons/12/book_open.png" title="Audit Logs" width="10" height="10" align="absmiddle" style="vertical-align:middle; margin-left:4px; margin-right:4px;" border="0" /><a href="#" style="margin-right:4px;" onclick="SimpleModal.open('../_dialogs/popup_hist_table.asp?recid=<% =crsid %>', 500, 700, 'no'); StopTheTimer(); return false;">Audit Logs</a>]
                                                    <% end if %>
                                                    </span>
                                                </div> 
                                                <textarea name="rptCaseNotes" id="rptCaseNotes_<% =idIssue %>" style="width:100%; height:100px;" ><%=server.HTMLEncode(rptCaseNotes & "")%></textarea>
                                            </td>                                
                                        </tr>                                                                                                                
                                    </table>					
                                   	<% 
									else
                                       	response.write("<input type=""hidden"" name=""rptCaseNotes"" value=""" & rptCaseNotes & """ >")
                                    end if 
									%>
                                    <!-- STOP Case Notes -->
                                    
								</td>                                                            
                            </tr>  
							<% else %>
								<input name="rptCaseNotes" type="hidden" value="<% =rptCaseNotes %>" />          
                            <% end if %>
                                                                                                                       
                                                                                                                
						</table>
   						<!-- STOP Information Form Fields -->

                        
   						<!-- START Investigation Form Fields -->                        
                  		<table id="table:notes:investigation" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">                
							<tr>
                                <td align="center">

	                            	<table width="95%" cellpadding="0" cellspacing="0">
                                		<tr>
                                  			<td align="left" class="clearFormat">                                            
                                				<div style="color:#35487B; font-weight:bold; padding-bottom:5px; margin-right:5px; text-align:left; float:left">Investigations</div>
                                                <div><span class="subLabel">(Click within the 'Name' column to view details)</span></div>                                                
                                                <% if action = "edit" then %>
	                                                <!-- START Investigations -->
    	                                            <!--#include file="../_includes/_INCissueInvestigations_.asp"-->            
        	                                        <!-- STOP Investigations -->                                                        
                                                    <div style="padding-top:5px; <% if (cLng(session(session("siteID") & "adminLoggedOn"))>=10) and (programAdmin <> "Y") and (rptLogID <> sLogid) and (addInvestigations <> "Y") then response.write("display:none;") %>">
                                                    	<a class="myCMbutton" href="#" onclick="this.blur(); navigateAway('../scripts/issues_exec.asp?recID=<% =crsid %>&action=addinv&top=notes&side=investigation'); return false;"><span class="add" style="padding-right:10px;">Add</span></a>
                                                    </div>
												<% else %>                                     
                                                    <!-- show INFO that NO investigations were available -->
                                                    <br>
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left" class="clearFormat" style="padding-right:2px;">
                                                                <% call systemMessageBox("investigationID","statusMessageINFO","Investigations will become available after saving issue.") %>
                                                            </td>
                                                        </tr>
                                                    </table>
												<% end if %>                                                
											</td>
										</tr>
                                    </table>
                                    
                                </td>
                        	</tr>
                            
                        </table>
   						<!-- STOP Investigation Form Fields -->


						<!-- START Documentation section -->
						<!-- 'NEED TO CHECK THIS SECURITY...  -->
                        <!-- if viewFileUpload = "Y" then  -->
   						<table id="table:notes:documentation" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">                            
							<tr>            	
								<td align="center">
                                                                        	
                                    <div style="color:#35487B; width:95%; font-weight:bold; text-align:left;">Documents</div>

									<!-- EDITING ISSUE look for existing uploaded documentation -->                                    
									<%
									'set alternating row colors
									rowColor = col2
									
                                    if action = "edit" then
									
										'NOTE: You can delete the reference to non-Year folder structuer after the new Year folder
										'structure has been tested....
																			
										dim viewObjects, Upload, Item, tmpItemCountRoot, tmpItemCountYear
										dim Directory, DirRoot, DirYear, extPos, extImg

										tmpItemCountRoot = 0
										tmpItemCountYear = 0

										on error resume next

										'Open new ASPUpload object		
										set Upload = Server.CreateObject("Persits.Upload")			
										
										'------------------------------
										'standard root directory files.
										Directory = dataDir & "\profiles\" & CustomerID & "\incoming\" & left(crsid,len(crsid)-3) & "-01" & "\"
										Set DirRoot = Upload.Directory( Directory & "*.*", , True)
										'Error 33 equal 'Directory not found'
										if Err.Number <> 33 then	
											'count number of files in directory											
											for each Item in DirRoot
											  	if (Not Item.IsSubdirectory) and (Item.FileName <> "Thumbs.db") then
													tmpItemCountRoot = tmpItemCountRoot + 1
												end if
											next
										end if

										'reset error handling
										on error goto 0		  
										on error resume next
										
										'------------------------------
										'yyyy/mm directory files.
										Directory = dataDir & "\profiles\" & CustomerID & "\incoming\" & "20" & left(crsid,2) & "\" & mid(crsid,3,2) & "\" & left(crsid,len(crsid)-3) & "-01" & "\"
										Set DirYear = Upload.Directory( Directory & "*.*", , True)										
										'Error 33 equal 'Directory not found'
										if Err.Number <> 33 then	
											'count number of files in directory											
											for each Item in DirYear
											  	if (Not Item.IsSubdirectory) and (Item.FileName <> "Thumbs.db") then
													tmpItemCountYear = tmpItemCountYear + 1
												end if
											next
										end if
										
										'------------------------------
										'files found, show to user
										'''if (tmpItemCountRoot + tmpItemCountYear) > 0 then
										if 1=1 then
										%>

                                        <table width="95%" class="formTable" cellpadding="0" cellspacing="0">                    
                                            <tr>            	
                                                <td align="center" class="clearFormat" style="border-top:none;">
                                                
													<!-- START Documents table  -->
                                                  	<table id="documentationTable" width="100%" style="background-color:#969696;" cellpadding=0 cellspacing=0>
                                          
														<thead>
                                                            <tr>
                                                                <th align="left" nowrap="nowrap">File Name</th>
                                                                <th align="left" nowrap="nowrap">Uploaded</th>
                                                                <th align="center" nowrap="nowrap">Size</th>
                                                            </tr>
                                                        </thead>                                              
                                              
                                                        <tbody id="documentationTableBody">                                                                    
                                                        
                                                        <%
                                                        '-----------------------------------
                                                        'START standard root directory files.																								
                                                        if tmpItemCountRoot > 0 then
                                                            '''tmpItemCountRoot = 0 'reset count
                                                            For Each Item in DirRoot
                                                              if (Not Item.IsSubdirectory) and (Item.FileName <> "Thumbs.db") Then
                                                                '''tmpItemCountRoot = tmpItemCountRoot + 1													 
                                                                'discover file extension
                                                                extPos = 1
                                                                for extPos = 1 to 6
                                                                    extImg = right(Item.FileName,extPos)
                                                                    if instr(1,extImg,".") then
                                                                        extPos = 6
                                                                    end if
                                                                next
                                                                'get rid of extra dot
                                                                extImg = replace(extImg,".","")
                                                                'set to default if unknown extenstion found
                                                                if instr(1,"doc,docx,html,xls,xlsx,jpg,jpeg,mpg,gif,zip,msg,pdf,png,txt,bmp,mdb,flv,fla,exe,log,wav,wps,tif",extImg) <= 0 then 
                                                                    extImg = "default"
                                                                end if
                                                                %>                                                
                                                                <!-- START Document list -->
                                                                <tr>                                            
                                                                    <td align="left">
                                                                        <a href="../_upload/download.asp?Name=<% =Server.URLEncode( Item.FileName ) %>&dir=root&recID=<% =idIssue %>&cid=<% =CustomerID %>"><img src="../_upload/images/file_extension_<% =extImg %>.png" title="Download Document" width="16" height="16" align="absmiddle" style="vertical-align:middle; padding-right:0px;" border="0" /></a>
                                                                        <a href="#" onClick="confirmDocDelete('../_upload/delete.asp?upDel=Yes&dir=root&recID=<% =crsid %>&cid=<% =CustomerID %>&del=<% =Server.URLEncode( Item.FileName ) %>&top=notes&side=documentation'); return false;"><img src="../_images/icons/16/cancel.png" title="Delete Document" width="16" height="16" align="absmiddle" style="vertical-align:middle;" border="0" /></a>
                                                                        <a href="../_upload/download.asp?Name=<% =Server.URLEncode( Item.FileName ) %>&dir=root&recID=<% =idIssue %>&cid=<% =CustomerID %>"><% = Server.HtmlEncode(Item.FileName) %></a>
                                                                    </td>
                                                                    <td align="left" nowrap="nowrap">
                                                                        <% = Item.LastWriteTime %>
                                                                    </td>
                                                                    <td align="right" nowrap="nowrap">
                                                                        <% = filesize(Item.Size) %>
                                                                    </td>
                                                                </tr>
                                                                <%
            
                                                                'Switch Row Color
                                                                if rowColor = col2 then rowColor = col1 else rowColor = col2
                                                                
                                                              end if		
                                                                            
                                                            'move to next document					
                                                            next
                                                            
                                                        end if
            
            
                                                        '-----------------------------------
                                                        'START YYYY/MM directory files.																								
                                                        if tmpItemCountYear > 0 then
                                                            '''tmpItemCountYear = 0 'reset count
                                                            For Each Item in DirYear
                                                              if (Not Item.IsSubdirectory) and (Item.FileName <> "Thumbs.db") Then
                                                                '''tmpItemCountYear = tmpItemCountYear + 1													 
                                                                'discover file extension
                                                                extPos = 1
                                                                for extPos = 1 to 6
                                                                    extImg = right(Item.FileName,extPos)
                                                                    if instr(1,extImg,".") then
                                                                        extPos = 6
                                                                    end if
                                                                next
                                                                'get rid of extra dot
                                                                extImg = replace(extImg,".","")
                                                                'set to default if unknown extenstion found
                                                                if instr(1,"doc,docx,html,xls,xlsx,jpg,jpeg,mpg,gif,zip,msg,pdf,png,txt,bmp,mdb,flv,fla,exe,log,wav,wps,tif",extImg) <= 0 then 
                                                                    extImg = "default"
                                                                end if
                                                                %>                                                
                                                                <!-- START Document list -->
                                                                <tr>                                            														
                                                                    <td align="left">
                                                                        <a href="../_upload/download.asp?Name=<% =Server.URLEncode( Item.FileName ) %>&dir=year&recID=<% =idIssue %>&cid=<% =CustomerID %>"><img src="../_upload/images/file_extension_<% =extImg %>.png" title="Download Document" width="16" height="16" align="absmiddle" style="vertical-align:middle; padding-right:0px;" border="0" /></a>
                                                                        <a href="#" onClick="confirmDocDelete('../_upload/delete.asp?upDel=Yes&dir=year&recID=<% =crsid %>&cid=<% =CustomerID %>&del=<% =Server.URLEncode( Item.FileName ) %>&top=notes&side=documentation'); return false;"><img src="../_images/icons/16/cancel.png" title="Delete Document" width="16" height="16" align="absmiddle" style="vertical-align:middle;" border="0" /></a>
                                                                        <a href="../_upload/download.asp?Name=<% =Server.URLEncode( Item.FileName ) %>&dir=year&recID=<% =idIssue %>&cid=<% =CustomerID %>"><% = Server.HtmlEncode(Item.FileName) %></a>
                                                                    </td>														
                                                                    <td align="left" nowrap="nowrap">
                                                                        <% = Item.LastWriteTime %>
                                                                    </td>
                                                                    <td align="right" nowrap="nowrap">
                                                                        <% = filesize(Item.Size) %>
                                                                    </td>
                                                                </tr>
                                                                <%
            
                                                                'Switch Row Color
                                                                if rowColor = col2 then rowColor = col1 else rowColor = col2
                                                                
                                                              end if		
                                                                            
                                                            'move to next document					
                                                            next
                                                            
                                                        end if
                                                        %>                                                
                                                                                                                                                          
                                                        </tbody>
													</table>

                                                    <%
                                                    dim tableDocProperty
                                                    tableDocProperty = "sort: true, sort_config:{sort_types:['String','String','String']}, filters_row_index: 1, " _
                                                                  	 & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
                                                                  	 & "status_bar: true, col_0: ""input"", col_1: """", col_2: """", " _
                                                                  	 & "col_width:[""75%"",null,null], paging: true, paging_length: 10, " _
                                                                  	 & "enable_non_empty_option: true, or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, " _
                                                                  	 & "highlight_keywords: true, " _
                                                                  	 & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
                                                                  	 & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"						  
                                                    %>
                                                    <!-- STOP Location table -->
                                                
                                                    <!-- Fire table build 999 -->
                                                    <script language="javascript" type="text/javascript">
                                                        //<![CDATA[
                                                        var tableDocProp = {<% =tableDocProperty %>};
                                                        //initiate table setup
                                                        var tfDocuments = setFilterGrid("documentationTable",tableDocProp);
                                                        //]]>
                                                    </script>

                                                    <!-- Shadow table -->
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                      <tr>
                                                        <td height="5px" class="clearFormat" style="background:#D0D0D0; padding:0px; margin:0px;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
                                                      </tr>
                                                    </table>

                                                </td>
                                          	</tr>
                                   		</table>
										<!-- STOP Documents table  -->
                                          
										<%									
										end if
	
										'reset error handling
										on error goto 0	
												  
										'no files found
										if (tmpItemCountRoot + tmpItemCountYear) <= 0 then
										%>						
											<table id="NoDocTable" width="95%" style="margin-top:10px;" cellpadding="0" cellspacing="0" >
												<tr>
													<td align="left" class="clearFormat" style="padding-right:2px;">                                                                                                
													<% call systemMessageBox("documentsID","statusMessageINFO","No documents found. Click ""Add"" below.") %>
													</td>
												</tr>
											</table>
										<%
										end if
										%>
	
										<table width="95%" cellpadding="0" cellspacing="0">
											<tr>
												<td align="left" class="clearFormat">
                                                    <div style="float:left; padding-top:5px;">
														<a class="myCMbutton" href="#" onclick="SimpleModal.open('../_jquery/fancyupload/fancyupload.asp?recID=<% =idIssue %>&cid=<% =CustomerID %>', 400, 500, 'yes'); StopTheTimer(); resetSystemMsg('systemMessage'); return false;"><span class="add" style="padding-right:10px;">Add</span></a>
                                                    </div>                                                    
												</td>
											</tr>
										</table>
                                    
                                    <!-- ADDING new issue...documents not available yet -->
									<% else %>
                                    	<br>
										<table width="95%" cellpadding="0" cellspacing="0">
											<tr>
												<td align="left" class="clearFormat" style="padding-right:2px;">                                                                                                
												<% call systemMessageBox("documentsID","statusMessageINFO","Documents will become available after saving issue.") %>
												</td>
											</tr>
										</table>
									<% end if %>
                                                                        
                                </td>    
							</tr>                            
                            
                      	</table>
						<!-- STOP Documentation section -->       


						<!-- START Custom Fields section -->                        
   						<table id="table:notes:customfields" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">
                    
                            <%
                            'Only viewable for Specified Users...NEED TO HIDE SIDE TAB IF NOT ALLOWED!!!
                            if uCase(viewCaseUserFields) = "N" then
							
                                response.Write("<input type=""hidden"" name=""rptUserField1"" id=""rptUserField1"" value=""" & rptUserField1 & """ >")
                                response.Write("<input type=""hidden"" name=""rptUserField2"" id=""rptUserField2"" value=""" & rptUserField2 & """ >")
                                response.Write("<input type=""hidden"" name=""rptUserField3"" id=""rptUserField3"" value=""" & rptUserField3 & """ >")			
                                response.Write("<input type=""hidden"" name=""rptUserField4"" id=""rptUserField4"" value=""" & rptUserField4 & """ >")						
                                response.Write("<input type=""hidden"" name=""rptUserField5"" id=""rptUserField5"" value=""" & rptUserField5 & """ >")
                                response.Write("<input type=""hidden"" name=""rptUserField6"" id=""rptUserField6"" value=""" & rptUserField6 & """ >")
                                response.Write("<input type=""hidden"" name=""rptUserField7"" id=""rptUserField7"" value=""" & rptUserField7 & """ >")			
                                response.Write("<input type=""hidden"" name=""rptUserField8"" id=""rptUserField8"" value=""" & rptUserField8 & """ >")		
                                response.Write("<input type=""hidden"" name=""rptUserField9"" id=""rptUserField9"" value=""" & rptUserField9 & """ >")		
                                response.Write("<input type=""hidden"" name=""rptUserField10"" id=""rptUserField10"" value=""" & rptUserField10 & """ >")													
                                response.Write("<input type=""hidden"" name=""rptUserField11"" id=""rptUserField11"" value=""" & rptUserField11 & """ >")
                                response.Write("<input type=""hidden"" name=""rptUserField12"" id=""rptUserField12"" value=""" & rptUserField12 & """ >")
                                response.Write("<input type=""hidden"" name=""rptUserField13"" id=""rptUserField13"" value=""" & rptUserField13 & """ >")			
                                response.Write("<input type=""hidden"" name=""rptUserField14"" id=""rptUserField14"" value=""" & rptUserField14 & """ >")						
                                response.Write("<input type=""hidden"" name=""rptUserField15"" id=""rptUserField15"" value=""" & rptUserField15 & """ >")
                                response.Write("<input type=""hidden"" name=""rptUserField16"" id=""rptUserField16"" value=""" & rptUserField16 & """ >")
                                response.Write("<input type=""hidden"" name=""rptUserField17"" id=""rptUserField17"" value=""" & rptUserField17 & """ >")			
                                response.Write("<input type=""hidden"" name=""rptUserField18"" id=""rptUserField18"" value=""" & rptUserField18 & """ >")		
                                response.Write("<input type=""hidden"" name=""rptUserField19"" id=""rptUserField19"" value=""" & rptUserField19 & """ >")		
                                response.Write("<input type=""hidden"" name=""rptUserField20"" id=""rptUserField20"" value=""" & rptUserField20 & """ >")													
							
							
							else
							
                            
                                dim pUserField1, pUserField2, pUserField3, pUserField4, pUserField5
								dim pUserField6, pUserField7, pUserField8, pUserField9, pUserField10
                                dim pUserField11, pUserField12, pUserField13, pUserField14, pUserField15
								dim pUserField16, pUserField17, pUserField18, pUserField19, pUserField20
								
                                dim pUserField1Type, pUserField2Type, pUserField3Type, pUserField4Type, pUserField5Type
								dim pUserField6Type, pUserField7Type, pUserField8Type, pUserField9Type, pUserField10Type
                                dim pUserField11Type, pUserField12Type, pUserField13Type, pUserField14Type, pUserField15Type
								dim pUserField16Type, pUserField17Type, pUserField18Type, pUserField19Type, pUserField20Type
								
                                dim tmpValue, valueArr, i

								'Get current configuration settings from database
								mySQL = "SELECT pUserField1, pUserField1Value, pUserField1Type, " _
									  & "       pUserField2, pUserField2Value, pUserField2Type, " _
									  & "       pUserField3, pUserField3Value, pUserField3Type, " _
									  & "       pUserField4, pUserField4Value, pUserField4Type, " _
									  & "       pUserField5, pUserField5Value, pUserField5Type, " _
									  & "       pUserField6, pUserField6Value, pUserField6Type, " _
									  & "       pUserField7, pUserField7Value, pUserField7Type, " _
									  & "       pUserField8, pUserField8Value, pUserField8Type, " _
									  & "       pUserField9, pUserField9Value, pUserField9Type, " _
									  & "       pUserField10, pUserField10Value, pUserField10Type, " _
									  & "       pUserField11, pUserField11Value, pUserField11Type, " _
									  & "       pUserField12, pUserField12Value, pUserField12Type, " _
									  & "       pUserField13, pUserField13Value, pUserField13Type, " _
									  & "       pUserField14, pUserField14Value, pUserField14Type, " _
									  & "       pUserField15, pUserField15Value, pUserField15Type, " _
									  & "       pUserField16, pUserField16Value, pUserField16Type, " _
									  & "       pUserField17, pUserField17Value, pUserField17Type, " _
									  & "       pUserField18, pUserField18Value, pUserField18Type, " _
									  & "       pUserField19, pUserField19Value, pUserField19Type, " _
									  & "       pUserField20, pUserField20Value, pUserField20Type " _
									  & "FROM   Customer " _
									  & "WHERE  CustomerID='" & customerID & "' "
								'get UDF names
								set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
								'assign field names
								
                                pUserField1 = rs("pUserField1")
								pUserField2 = rs("pUserField2")
								pUserField3 = rs("pUserField3")
								pUserField4 = rs("pUserField4")
								pUserField5 = rs("pUserField5")
								pUserField6 = rs("pUserField6")
								pUserField7 = rs("pUserField7")
								pUserField8 = rs("pUserField8")
								pUserField9 = rs("pUserField9")
								pUserField10 = rs("pUserField10")
                                pUserField11 = rs("pUserField11")
								pUserField12 = rs("pUserField12")
								pUserField13 = rs("pUserField13")
								pUserField14 = rs("pUserField14")
								pUserField15 = rs("pUserField15")
								pUserField16 = rs("pUserField16")
								pUserField17 = rs("pUserField17")
								pUserField18 = rs("pUserField18")
								pUserField19 = rs("pUserField19")
								pUserField20 = rs("pUserField20")

                                pUserField1Type = rs("pUserField1Type")
								pUserField2Type = rs("pUserField2Type")
								pUserField3Type = rs("pUserField3Type")
								pUserField4Type = rs("pUserField4Type")
								pUserField5Type = rs("pUserField5Type")
								pUserField6Type = rs("pUserField6Type")
								pUserField7Type = rs("pUserField7Type")
								pUserField8Type = rs("pUserField8Type")
								pUserField9Type = rs("pUserField9Type")
								pUserField10Type = rs("pUserField10Type")
                                pUserField11Type = rs("pUserField11Type")
								pUserField12Type = rs("pUserField12Type")
								pUserField13Type = rs("pUserField13Type")
								pUserField14Type = rs("pUserField14Type")
								pUserField15Type = rs("pUserField15Type")
								pUserField16Type = rs("pUserField16Type")
								pUserField17Type = rs("pUserField17Type")
								pUserField18Type = rs("pUserField18Type")
								pUserField19Type = rs("pUserField19Type")
								pUserField20Type = rs("pUserField20Type")
																
                                call closeRS(rs)

                                'make sure there are fields to show			
                                if len(pUserField1 & pUserField2 & pUserField3 & pUserField4 & pUserField5 & pUserField6 & pUserField7 & pUserField8 & pUserField9 & pUserField10) > 0 then
                                        call dispUFields(pUserField1,"pUserField1Value","rptUserField1",rptUserField1,pUserField1Type)
                                        call dispUFields(pUserField2,"pUserField2Value","rptUserField2",rptUserField2,pUserField2Type)
                                        call dispUFields(pUserField3,"pUserField3Value","rptUserField3",rptUserField3,pUserField3Type)
                                        call dispUFields(pUserField4,"pUserField4Value","rptUserField4",rptUserField4,pUserField4Type)					
                                        call dispUFields(pUserField5,"pUserField5Value","rptUserField5",rptUserField5,pUserField5Type)
                                        call dispUFields(pUserField6,"pUserField6Value","rptUserField6",rptUserField6,pUserField6Type)
                                        call dispUFields(pUserField7,"pUserField7Value","rptUserField7",rptUserField7,pUserField7Type)
                                        call dispUFields(pUserField8,"pUserField8Value","rptUserField8",rptUserField8,pUserField8Type)
                                        call dispUFields(pUserField9,"pUserField9Value","rptUserField9",rptUserField9,pUserField9Type)
                                        call dispUFields(pUserField10,"pUserField10Value","rptUserField10",rptUserField10,pUserField10Type)											
                                        call dispUFields(pUserField11,"pUserField11Value","rptUserField11",rptUserField11,pUserField11Type)
                                        call dispUFields(pUserField12,"pUserField12Value","rptUserField12",rptUserField12,pUserField12Type)
                                        call dispUFields(pUserField13,"pUserField13Value","rptUserField13",rptUserField13,pUserField13Type)
                                        call dispUFields(pUserField14,"pUserField14Value","rptUserField14",rptUserField14,pUserField14Type)					
                                        call dispUFields(pUserField15,"pUserField15Value","rptUserField15",rptUserField15,pUserField15Type)
                                        call dispUFields(pUserField16,"pUserField16Value","rptUserField16",rptUserField16,pUserField16Type)
                                        call dispUFields(pUserField17,"pUserField17Value","rptUserField17",rptUserField17,pUserField17Type)
                                        call dispUFields(pUserField18,"pUserField18Value","rptUserField18",rptUserField18,pUserField18Type)
                                        call dispUFields(pUserField19,"pUserField19Value","rptUserField19",rptUserField19,pUserField19Type)
                                        call dispUFields(pUserField20,"pUserField20Value","rptUserField20",rptUserField20,pUserField20Type)	
																													
                                else
								
									'no fields have been setup
                            		response.write("<tr><td>")
									response.write("<table width=""96%"" cellpadding=""0"" cellspacing=""0"" align=""center""><tr><td class=""clearFormat"" style=""padding:5px;"" >")
									call systemMessageBox("customerFieldsID","statusMessageINFO","No custom fields have been defined in the database.")
                            		response.write("</td></tr></table>")										
									response.write("</td></tr>")
									
									response.Write("<input type=""hidden"" name=""rptUserField1"" id=""rptUserField1"" value=""" & rptUserField1 & """ >")
									response.Write("<input type=""hidden"" name=""rptUserField2"" id=""rptUserField2"" value=""" & rptUserField2 & """ >")
									response.Write("<input type=""hidden"" name=""rptUserField3"" id=""rptUserField3"" value=""" & rptUserField3 & """ >")			
									response.Write("<input type=""hidden"" name=""rptUserField4"" id=""rptUserField4"" value=""" & rptUserField4 & """ >")						
									response.Write("<input type=""hidden"" name=""rptUserField5"" id=""rptUserField5"" value=""" & rptUserField5 & """ >")
									response.Write("<input type=""hidden"" name=""rptUserField6"" id=""rptUserField6"" value=""" & rptUserField6 & """ >")
									response.Write("<input type=""hidden"" name=""rptUserField7"" id=""rptUserField7"" value=""" & rptUserField7 & """ >")			
									response.Write("<input type=""hidden"" name=""rptUserField8"" id=""rptUserField8"" value=""" & rptUserField8 & """ >")		
									response.Write("<input type=""hidden"" name=""rptUserField9"" id=""rptUserField9"" value=""" & rptUserField9 & """ >")		
									response.Write("<input type=""hidden"" name=""rptUserField10"" id=""rptUserField10"" value=""" & rptUserField10 & """ >")													
									response.Write("<input type=""hidden"" name=""rptUserField11"" id=""rptUserField11"" value=""" & rptUserField11 & """ >")
									response.Write("<input type=""hidden"" name=""rptUserField12"" id=""rptUserField12"" value=""" & rptUserField12 & """ >")
									response.Write("<input type=""hidden"" name=""rptUserField13"" id=""rptUserField13"" value=""" & rptUserField13 & """ >")			
									response.Write("<input type=""hidden"" name=""rptUserField14"" id=""rptUserField14"" value=""" & rptUserField14 & """ >")						
									response.Write("<input type=""hidden"" name=""rptUserField15"" id=""rptUserField15"" value=""" & rptUserField15 & """ >")
									response.Write("<input type=""hidden"" name=""rptUserField16"" id=""rptUserField16"" value=""" & rptUserField16 & """ >")
									response.Write("<input type=""hidden"" name=""rptUserField17"" id=""rptUserField17"" value=""" & rptUserField17 & """ >")			
									response.Write("<input type=""hidden"" name=""rptUserField18"" id=""rptUserField18"" value=""" & rptUserField18 & """ >")		
									response.Write("<input type=""hidden"" name=""rptUserField19"" id=""rptUserField19"" value=""" & rptUserField19 & """ >")		
									response.Write("<input type=""hidden"" name=""rptUserField20"" id=""rptUserField20"" value=""" & rptUserField20 & """ >")													

                                end if
                                
                            end if
                            %>	                    
                                
                      	</table>
						<!-- STOP Custom Fields section -->       


						<!-- START Resolution section -->        
   						<table id="table:notes:resolution" style="display:none;" width="100%" class="formTable" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" valign="top" style="padding:0px; margin:0px;">
                                
                                    <table id="table:resolve:resolution2" width="95%" class="formTable" cellpadding="0" cellspacing="0">                    
                                        <tr>            	
                                            <td align="center" style="border-top:none;">
                                                                                        
                                                <div style="color:#35487B; width:100%; font-weight:bold; text-align:left; padding-bottom:5px;">Resolutions</div>
            
                                                <!-- EDITING ISSUE look for existing uploaded documentation -->                                    
                                                <%
                                                dim arrResolutionID
                                                dim resolutionCount : resolutionCount = 0
                                                
                                                'set alternating row colors
                                                dim rowColorResolution : rowColorResolution = col2
                                                
                                                if action = "edit" then
                                                
                                                    '-----------------------------------
                                                    'LOOK for existing notes
                                                    mySQL = "SELECT Resolution.ResolutionID, Resolution.CRSID, Resolution.ApprovedDate, Satisfaction, " _
                                                          & "Logins.FirstName + ' ' + Logins.LastName AS [ApprovedBy], " _
                                                          & "'Given' = CASE WHEN DateReadBy > 0 THEN 'Yes' ELSE 'No' END " _
                                                          & "	FROM Resolution LEFT JOIN Logins ON Resolution.ApprovedBy = Logins.LOGID " _											  
                                                          & "		WHERE CRSID Like '" & left(idIssue,len(idIssue)-3) & "%' " _
                                                          & "		ORDER BY ResolutionID "													
                                                    set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)      
                                                    %>                                                
                                                    
                                                    <!-- START Resolution table -->
                                                    <table id="resolutionTable" width="100%" style="background-color:#969696;" cellpadding=0 cellspacing=0>
                                                                    
                                                       <thead>
                                                            <tr>
                                                                <th align="left" nowrap="nowrap">&nbsp;</th>
                                                                <th align="left" nowrap="nowrap">Issue #</th>
                                                                <th align="left" nowrap="nowrap">Approved Date</th>
                                                                <th align="left" nowrap="nowrap">Given to Reporter</th>
                                                                <th align="left" nowrap="nowrap">Satisfaction</th>                                                                
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                            			
                                                        <%
                                                        do until rs.eof
                                                        
                                                        'to hide messages if no records
                                                        resolutionCount = resolutionCount + 1
                                                            
                                                            'START current row
															response.write("<tr id=""resolution:" & rs("resolutionid") & "_tr"">")					
                                                            
															'ICON actions
															response.Write("<td id=""resolution:" & rs("resolutionid") & "_delete"" align=""left"" style=""width:40px;"">")												
																'only allow certain users to delete resolutions
																if uCase(editCaseResolutionApp)="N" and ProgramAdmin<>"Y" and cLng(session(session("siteID") & "adminLoggedOn")) >=10 then 
																	response.write("<a href=""#"" onclick=""this.blur(); jAlert('Permission denied.', 'myCM Alert'); return false;""><img src=""../_images/icons/16/document_quote_delete.png"" title=""Delete Resolution"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; padding-right:7px; vertical-align:middle;""></a>")
																'if already provided NO-one can delete
																elseif len(rs("ApprovedDate"))>0 and len(rs("Satisfaction"))>0 then 
																	response.write("<a href=""#"" onclick=""this.blur(); jAlert('Permission denied.', 'myCM Alert'); return false;""><img src=""../_images/icons/16/document_quote_delete.png"" title=""Delete Resolution"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; padding-right:7px; vertical-align:middle;""></a>")
																else
																	response.write("<a href=""#"" onClick=""delResolution(" & rs("resolutionid") & "); resetSystemMsg('systemMessage'); return false;""><img src=""../_images/icons/16/document_quote_delete.png"" title=""Delete Resolution"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; padding-right:7px; vertical-align:middle;""></a>")
																end if												
															response.write("<a href=""#"" onclick=""SimpleModal.open('../_dialogs/popup_config_resolution.asp?recid=" & rs("resolutionid") & "&cid=" & customerID & "&readcrsid=" & idIssue & "&action=edit', 475, 650, 'no'); StopTheTimer(); resetSystemMsg('systemMessage'); return false;""><img src=""../_images/icons/16/document_quote_edit.png"" title=""Open Resolution"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; vertical-align:middle;""></a>")												
															response.Write("</td>")
															
															'NAME link
															response.Write("<td id=""resolution:" & rs("resolutionid") & "_crsid"" align=""left"" style=""cursor:pointer;"" onclick=""SimpleModal.open('../_dialogs/popup_config_resolution.asp?recid=" & rs("resolutionid") & "&cid=" & customerID & "&readcrsid=" & idIssue & "&action=edit', 475, 650, 'no'); StopTheTimer(); resetSystemMsg('systemMessage'); return false;"" >")
															response.write("<a style=""color:#333333;"">" & rs("crsid") & "</a>")
															response.Write("</td>")
															
                                                            response.Write("<td id=""resolution:" & rs("resolutionid") & "_date"" align=""left"">" & rs("ApprovedDate") & "</td>")
                                                            response.Write("<td id=""resolution:" & rs("resolutionid") & "_given"" align=""left"">" & rs("Given") & "</td>")
                                                            response.Write("<td id=""resolution:" & rs("resolutionid") & "_satisfaction"" align=""left"">" & rs("Satisfaction") & "</td>")
                                                            
                                                            'CLOSE current row						
                                                            response.write("</tr>")
                                                            
                                                            rs.movenext
                                                        loop
                                                        %>					
                                            
                                                        </tbody>
                                                    </table>
                
                                                    <%
                                                    dim tableResProperty
                                                    tableResProperty = "sort: true, sort_config:{sort_types:['String','String','String','String','String']}, filters_row_index: 1, " _
                                                                  	 & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
                                                                  	 & "status_bar: true, col_0: """", col_1: """", col_2: """", col_3: ""select"", col_4: ""select"", " _
                                                                  	 & "col_width:[null,""40%"",null,null,null], paging: true, paging_length: 5, " _
                                                                  	 & "enable_non_empty_option: true, or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, " _
                                                                  	 & "highlight_keywords: true, " _
                                                                  	 & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
                                                                  	 & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"						  
                                                    %>
                                                    <!-- STOP Location table -->
                                                
                                                    <!-- Fire table build -->
                                                    <script language="javascript" type="text/javascript">
                                                        //<![CDATA[
                                                        var tableProp = {<% =tableResProperty %>};
                                                        //initiate table setup
                                                        var tfResolution = setFilterGrid("resolutionTable",tableProp);
                                                        //]]>
                                                    </script>
                                                
                                                    <!-- Shadow table -->
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                      <tr>
                                                        <td height="5px" class="clearFormat" style="background:#D0D0D0; padding:0px; margin:0px;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
                                                      </tr>
                                                    </table>
            
            
                                                    <!-- No Activities found  -->
                                                    <table id="NoResolutionTable" width="100%" cellpadding="0" cellspacing="0" style="margin-top:10px; <% if resolutionCount>0 then response.write("display:none;") %>">
                                                        <tr>
                                                            <td align="left" class="clearFormat" style="padding-right:2px;">                                                                                                
                                                                <% call systemMessageBox("resolutionsID","statusMessageINFO","No resolutions found. Click ""Add"" below.") %>
                                                            </td>
                                                        </tr>
                                                    </table>												                                                                                      
                                                    <!-- STOP Resolutions table  -->
            
                                                    <!-- ADD button  -->
                                                    <div style="float:left; padding-top:5px; width:125px;">
                                                        <a class="myCMbutton" href="#" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_config_resolution.asp?recid=<% =idIssue %>&cid=<% =customerID %>&action=add', 475, 650, 'no'); StopTheTimer(); resetSystemMsg('systemMessage'); return false;"><span class="add" style="padding-right:10px;">Add</span></a>
                                                    </div>                
                                                    
                                                    <% closeRS(rs) %>
                                                    
                                                <!-- ADDING new issue...resolutions not available yet -->
                                                <% else %>
                                                    <br>
                                                    <table id="NoResolutionTable" width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left" class="clearFormat" style="padding-right:2px;">                                                                                                
                                                            <% call systemMessageBox("resolutionsID","statusMessageINFO","Resolutions will become available after saving and reloading issue.") %>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                <% end if %>
                                                                        
                                                <!-- FORM ELEMENT SAVING... -->
                                                <!-- These elements MUST be set to style="display:none;" and NOT type="hidden" -->
                                                <!-- when set as "hidden" FireFox does not recognize any change has been made  -->
                                                <input name="arrResolutionRemoved" id="arrResolutionRemoved" style="display:none;" value="" />
                                                                                    
                                            </td>    
                                        </tr>                            
                                    </table>

								</td>
                            </tr>
                      	</table>
						<!-- STOP Resolution section -->       
                        
                                        
                    </td>
                    <!-- START Page Details and Form Fields -->			                    
                                        
                </tr>
            
            </table>
            <!-- STOP Notes table -->
	
    	</div>
        <!-- STOP Notes div -->
        
		<script language="javascript">
			// set row color in global variable
			var rowColor = "<% =rowColor %>";
			function addDocUploaded(file,date,size,ext){
				
				var tempHTML;
				var fileExt = "doc,docx,html,xls,xlsx,jpg,jpeg,mpg,gif,zip,msg,pdf,png,txt,bmp,mdb,flv,fla,exe,log,wav,wps,tif";
				var fileImg;

				if (fileExt.indexOf(ext)<=0) ext = "default";					

				// create randon numbers used to create unique element IDs for idQueryValue form fields
				var valueID_1 = Math.floor(Math.random()*1001);
				var valueID_2 = Math.floor(Math.random()*1001);
				  
				// create table body
				var table = document.getElementById("documentationTable");
				var tbody = table.getElementsByTagName("tbody")[0];
				
				// ----- ADD ROW -----
				// create row element and assign unique ID
				var rowCount = table.rows.length;
				var rowID = "rowDocUploaded_" + (rowCount+2);
				var row = document.createElement("tr");
				row.id = rowID;
					
				// ----- ADD CELL #1 -----
				var td1 = document.createElement("td")					
				//td1.className = "docLeft";					
				//td1.style.backgroundColor = rowColor;
				td1.style.textAlign = "left";
				tempHTML = null;
				tempHTML = "<a href=\"../_upload/download.asp?Name=" + escape(file) + "&dir=year&recID=<% =idIssue %>&cid=<% =CustomerID %>\"><img src=\"../_upload/images/file_extension_" + ext + ".png\" title=\"Download Document\" width=\"16\" height=\"16\" align=\"absmiddle\" style=\"vertical-align:middle; padding-right:4px;\" border=\"0\" /></a>";
				tempHTML = tempHTML + "<a href=\"#\" onClick=\"confirmDocDelete('../_upload/delete.asp?upDel=Yes&dir=year&recID=<% =crsid %>&cid=<% =CustomerID %>&del=" + escape(file) + "&top=notes&side=documentation'); return false;\"><img src=\"../_images/icons/16/cancel.png\" title=\"Delete Document\" width=\"16\" height=\"16\" align=\"absmiddle\" style=\"vertical-align:middle; padding-right:4px;\" border=\"0\" /></a>";				
				tempHTML = tempHTML + "<a href=\"../_upload/download.asp?Name=" + escape(file) + "&dir=year&recID=<% =idIssue %>&cid=<% =CustomerID %>\">" + file + "</a>";
				td1.innerHTML = tempHTML;

				// ----- ADD CELL #2 -----	
				var td2 = document.createElement("td")					
				//td2.className = "docCenter";
				//td2.style.backgroundColor = rowColor;
				td2.style.textAlign = "left";
				td2.innerHTML = date;

				// ----- ADD CELL #3 -----	
				var td3 = document.createElement("td")
				//td3.className = "docRight";
				//td3.style.backgroundColor = rowColor;
				td3.style.textAlign = "right";
				td3.innerHTML = bytesToSize(size,2);

				// ----- FINILIZE AND APPEND NEW CELLS AND ROW -----
				row.appendChild(td1);
				row.appendChild(td2);
				row.appendChild(td3);
						
				// append row to table
				tbody.appendChild(row);

				// update row color for next row
				//if (rowColor == "#F0F0F0") {rowColor = "#FFFFFF"} else {rowColor = "#F0F0F0"};
				
				// hide "no documents" message if exists
				if (document.getElementById('NoDocTable')) {
					document.getElementById('NoDocTable').style.display = "none";				
				}

				//refresh grid
				tfDocuments.RefreshGrid();


			}

			function confirmDocDelete(file) {
				var title = "Cancel Changes";
				var msg = "Changes have been made to this issue and <strong>must<br>be saved</strong> before continuing.<br><br>Continue and cancel changes?";
				//check for form changes before continuing
				if(isFormChanged(document.frm)==true) {
					jConfirm(msg, title, function(r) {
						if (r==true) {
							confirmPrompt('Confirm Delete','This action can not be undone.<br><br>Are you sure you want to continue?','redirect',file);
						}
						else {
							return false;
						}
					});					
				}
				//form hasn't changed...go for it!
				else {
					confirmPrompt('Confirm Delete','This action can not be undone.<br><br>Are you sure you want to continue?','redirect',file);
				}
			}


			//NOT USED ANYMORE!!!!!!! CAN DELETE AFTER BEEN IN PLACE FOR AWHILE
			//launches document dialog window...
			function confirmUpload(objID) {
				var winTitle = "Upload";
				var winWidth = 415;
				var winHeight = 390;
				var topTab = document.getElementById('topTab');
				var sideTab = document.getElementById('sideTab');				
				var url = "../_upload/default.asp?fUpload=No&recID="+objID;
				var h=Math.min(winHeight,screen.height-50);		

				var title = "Cancel Changes";
				var msg = "Changes have been made to this issue and <strong>must<br>be saved</strong> before continuing.<br><br>Continue and cancel changes?";
				
				StopTheTimer(); //stop autosave timer
				//form changed, make sure user is ok with losing changes
				if(isFormChanged(document.frm)==true) {
					jConfirm(msg, title, function(r) {
						if (r==true) {
							SimpleModal.open(url, 390, 390, 'no');
						}
						else {
							return false;
						}
					});					
				}
				//form hasn't changed...go for it!
				else {
					SimpleModal.open(url, 390, 390, 'no');
				}
				StartTheTimer(); //restart autosave timer
				
			}        
			
			//NOT USED ANYMORE!!!!!!! CAN DELETE AFTER BEEN IN PLACE FOR AWHILE
	        function documentsReload() {  	
            	window.location = '../scripts/<% session(session("siteID") & "issuesEdit") %>?recID=<% =crsid %>&action=edit&top=notes&side=documentation';
          	}

			//fill-in resolution box
			function fillResolution() {
				var res = document.getElementById('rptResolution');
				var opt = document.getElementById('resTemplate')
				if (opt.selectedIndex != 0) {
					if (!res.value) {
						res.value = opt.value;
					}
					else {
						res.value += '\r\r' + opt.value;						
					}
				}
			}		
			
		</script>


        <script>
			function addResolution(resolutionid,crsid,approved,given,satisfaction){
				
				var tempHTML;

				// create randon numbers used to create unique element IDs for idQueryValue form fields
				var valueID_1 = Math.floor(Math.random()*1001);
				  
				// create table body
				var table = document.getElementById("resolutionTable");
				var tbody = table.getElementsByTagName("tbody")[0];
				
				// ----- ADD ROW -----
				// create row element and assign unique ID
				var rowCount = table.rows.length;
				var rowID = "resolution:" + resolutionid + "_tr";				
				var row = document.createElement("tr");
				row.id = rowID;

				// ----- ADD CELL #1 -----	
				var td1 = document.createElement("td")					
				td1.id = "resolution:" + resolutionid + "_delete";
				td1.style.textAlign = "left";			
				td1.style.width = "40px";
				tempHTML = "<a href=\"#\" onClick=\"delResolution(" + resolutionid + "); resetSystemMsg(\'systemMessage\'); return false;\"><img src=\"../_images/icons/16/document_quote_delete.png\" title=\"Delete Resolution\" border=\"0\" width=\"16\" height=\"16\" align=\"absmiddle\" style=\"padding-top:1px; padding-bottom:1px; padding-right:7px; vertical-align:middle;\"></a>";
				tempHTML += "<a href=\"#\" onclick=\"SimpleModal.open(\'../_dialogs/popup_config_resolution.asp?recid=" + resolutionid + "&cid=<% =customerID %>&readcrsid=" + crsid + "&action=edit\', 475, 650, \'no\'); StopTheTimer(); resetSystemMsg(\'systemMessage\'); return false;\"><img src=\"../_images/icons/16/document_quote_edit.png\" title=\"Open Resolution\" border=\"0\" width=\"16\" height=\"16\" align=\"absmiddle\" style=\"padding-top:1px; padding-bottom:1px; vertical-align:middle;\"></a>";
				td1.innerHTML = tempHTML;				

				// ----- ADD CELL #2 -----	
				var td2 = document.createElement("td")					
				td2.id = "resolution:" + resolutionid + "_crsid";
				td2.style.textAlign = "left";			
				td2.style.cursor = "pointer";
				tempHTML = "<div style=\"width:100%;\" onclick=\"SimpleModal.open(\'../_dialogs/popup_config_resolution.asp?recid=" + resolutionid + "&cid=<% =customerID %>&readcrsid=" + crsid + "&action=edit\', 475, 650, \'no\'); StopTheTimer(); resetSystemMsg(\'systemMessage\'); return false;\">";
				tempHTML += "<a style=\"color:#333333;\">" + crsid + "</a>";
				tempHTML += "</div>";
				td2.innerHTML = tempHTML;				

				// ----- ADD CELL #3 -----
				var td3 = document.createElement("td")			
				td3.id = "resolution:" + resolutionid + "_date";				
				td3.style.textAlign = "left";
				td3.innerHTML = approved;
				
				// ----- ADD CELL #4 -----	
				var td4 = document.createElement("td")			
				td4.id = "resolution:" + resolutionid + "_given";				
				td4.style.textAlign = "left";				
				td4.innerHTML = given;			

				// ----- ADD CELL #5 -----	
				var td5 = document.createElement("td")			
				td5.id = "resolution:" + resolutionid + "_satisfaction";				
				td5.style.textAlign = "left";				
				tempHTML = satisfaction;
				td5.innerHTML = satisfaction;							

				// ----- FINILIZE AND APPEND NEW CELLS AND ROW -----
				row.appendChild(td1);
				row.appendChild(td2);
				row.appendChild(td3);
				row.appendChild(td4);
				row.appendChild(td5);
						
				// append row to table
				tbody.appendChild(row);
							
				// hide "no documents" message if exists
				if (document.getElementById('NoResolutionTable')) {
					document.getElementById('NoResolutionTable').style.display = "none";				
				}
				//make sure table is visible
				if (document.getElementById('resolutionTable').style.display == "none") {
					document.getElementById('resolutionTable').style.display = "";
				}				
				//refresh grid
				tfResolution.RefreshGrid();
			}		

			function saveResolution(resolutionid,approved,given,satisfaction){
				var tempHTML;
				var td3 = document.getElementById("resolution:" + resolutionid + "_date");
				var td4 = document.getElementById("resolution:" + resolutionid + "_given");
				var td5 = document.getElementById("resolution:" + resolutionid + "_satisfaction");								
				//update existing columns
				td3.innerHTML = approved;
				td4.innerHTML = given;			
				td5.innerHTML = satisfaction;											
			}
			
			function delResolution(resolutionid) {
				var theRow = document.getElementById("resolution:" + resolutionid + "_tr");
				var theTable = document.getElementById("resolutionTable");		
				var deleteElement = document.getElementById("arrResolutionRemoved");
				
				jConfirm('This action cannot be undone.<br/><br/>Continue with deleting this resolution?', 'Confirm Delete', function(r) {
					//user agrees, remove categories
					if (r==true) {
						var rowIndex = theRow.parentNode.parentNode.rowIndex;						
						//move through all rows and find the one to delete
						for (var i = 0; i < theTable.rows.length; i++) {
							if (theRow == theTable.rows[i]) {
								//this is the only way that works deleting rows
								//from a tableFilter table
								theTable.deleteRow(i);
								tfResolution.nbRows--;			//necessary for tableFitler to work
								tfResolution.RefreshGrid();	//necessary for tableFitler to work
								deleteElement.value += resolutionid + ","	//used for deleting subject on Ajax form save
							}
						}								
						//show NO SUBJECTS table
						if (theTable.rows.length<=2) {
							document.getElementById('NoResolutionTable').style.display = "";
						}											
					}
					//user disagrees
					else {
						return false;
					}
				});						
			}

			function status_update() {				
				//case notes options on ALL options
				if (document.getElementById("rptStatus").value == '<% =pCaseStatusCloseOn %>') {
			        $('#IssueDateClosed :input').removeAttr('disabled');	
					if (document.getElementById("date:rptDateClosed").value == null || document.getElementById("date:rptDateClosed").value == '') {
						document.getElementById("date:rptDateClosed").value = CurrentDate();
					}
				}
				else {					
					$('#IssueDateClosed :input').attr('disabled', true);	
				}
			}
			status_update();
			
		</script>
