<%
'********************************************************************
' Product  : C-LIVE ComplianceLine Web Reports
' Function : Send Emails
' Version  : 2.0
' Modified : November 2004
' Copyright: Copyright (C) 2004 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'			 website, please contact webmaster@complianceline.com
'********************************************************************

'*************************************************************************
'Forward email send request to appropriate component
'*************************************************************************
Function sendMail(fromName, fromEmail, toEmail, subject, body, contType)

dim toEmailArr

	on error resume next
	
	'Check for multilple SendTo addresses and create Array if found.
	if inStr(1,toEmail,";") then
		toEmailArr = split(toEmail,";")
	else
		toEmailArr = toEmail
	end if	
	
	select case mailComp
	
		case "1" 'JMail
			call JMail(fromName, fromEmail, toEmailArr, subject, body, contType)
			
		case "2" 'CDONTS
			call CDONTS(fromName, fromEmail, toEmailArr, subject, body, contType)
			
		case "3" 'Persits ASPEmail
			call PASPEmail(fromName, fromEmail, toEmailArr, subject, body, contType)
			
		case "4" 'ServerObjects ASPMail
			call SOASPMail(fromName, fromEmail, toEmailArr, subject, body, contType)
			
		case "5" 'Bamboo SMTP
			call bamboo(fromName, fromEmail, toEmailArr, subject, body, contType)
			
		case "6" 'CDOSYS
			call CDOSYS(fromName, fromEmail, toEmailArr, subject, body, contType)
			
		case "7" 'CDOSYS with SMPT Server
			call CDOSYSwithServer(fromName, fromEmail, toEmailArr, subject, body, contType)					

		case "8" 'ActiveXperts ActiveEmail
			call ActiveEmail(fromName, fromEmail, toEmailArr, subject, body, contType)					
			
	end select
	
	if mailcomp > "0" then
		'Log e-mail event in database
		call logMail(fromName, fromEmail, toEmail, subject, body, mailComp, contType, err.description)
	end if

	on error goto 0
	
end Function

'JMail
Function JMail(fromName, fromEmail, toEmail, subject, body, contType)
	dim mail,I
	
	'Version 4.3
	'------------------------------------------------------------
	set mail			= server.CreateObject("JMail.Message")
	mail.From			= fromEmail
	mail.FromName		= fromName
	'------------------------------------------------------------
	
	'Version 3.7
	'------------------------------------------------------------
	'set mail			= server.CreateObject("JMail.SMTPMail")
	'mail.ServerAddress	= pSMTPServer
	'mail.Sender		= fromEmail
	'mail.SenderName	= fromName
	'------------------------------------------------------------

	mail.silent   = true
	mail.Subject  = subject
	mail.Body     = body
	if contType = 1 then			'Send HTML Email
		mail.ContentType = "text/html"
	end if
	if isArray(toEmail) then		'Send Multiple Emails
		for I = 0 to Ubound(toEmail)
			if len(toEmail(I)) > 0 then
				mail.ClearRecipients()
				mail.AddRecipient toEmail(I)
				mail.Send(pSmtpServer)	'V4.3
				'mail.Execute			'V3.7
			end if
		next
	else							'Send Single Email
		mail.AddRecipient toEmail
		mail.Send(pSmtpServer)			'V4.3
		'mail.Execute					'V3.7
	end if
	set mail = nothing
end Function

'CDONTS
'Note : After the "Send" method, the "NewMail" object becomes invalid.
'       We therefore have to create the "NewMail" object for each email.
Function CDONTS(fromName, fromEmail, toEmail, subject, body, contType)
	dim mail,I
	if isArray(toEmail) then		 'Send Multiple Emails
		for I = 0 to Ubound(toEmail)
			if len(toEmail(I)) > 0 then
				Set mail = Server.CreateObject ("CDONTS.NewMail")
				if contType = 1 then 'Send HTML Email
					mail.BodyFormat = 0
					mail.MailFormat = 0
				end if
				mail.Send fromEmail & " (" & fromName & ")", toEmail(I), subject, body
				set mail = nothing
			end if
		next
	else							 'Send Single Email
		Set mail = Server.CreateObject ("CDONTS.NewMail")
		if contType = 1 then		 'Send HTML Email
			mail.BodyFormat = 0
			mail.MailFormat = 0
		end if
		mail.Send fromEmail & " (" & fromName & ")", toEmail, subject, body
		set mail = nothing
	end if
end Function

'Persits ASP Email
Function PASPEmail(fromName, fromEmail, toEmail, subject, body, contType)
	dim mail,I
	set mail 	  = server.CreateObject("Persits.MailSender")
	mail.Host 	  = pSmtpServer 
	mail.From 	  = fromEmail
	mail.FromName = fromName
	mail.Subject  = subject
	mail.Body 	  = body
	if contType = 1 then			'Send HTML Email
		mail.IsHTML = True 
	end if
	if isArray(toEmail) then		'Send Multiple Emails
		for I = 0 to Ubound(toEmail)
			if len(toEmail(I)) > 0 then
				mail.Reset
				mail.AddAddress toEmail(I)
				mail.Send
			end if
		next
	else							'Send Single Email
		mail.AddAddress toEmail
		mail.Send
	end if
	set mail = nothing
end Function

'ServerObjects ASPMail
Function SOASPMail(fromName, fromEmail, toEmail, subject, body, contType)
	dim mail,I
	set mail		 = server.CreateObject("SMTPsvg.Mailer")
	mail.RemoteHost	 = pSmtpServer
	mail.FromAddress = fromEmail
	mail.FromName	 = fromName
	mail.Subject	 = subject
	mail.BodyText	 = body
	if contType = 1 then			'Send HTML Email
		mail.ContentType = "text/html"
	end if
	if isArray(toEmail) then		'Send Multiple Emails
		for I = 0 to Ubound(toEmail)
			if len(toEmail(I)) > 0 then
				mail.ClearRecipients
				mail.AddRecipient "", toEmail(I)
				mail.SendMail
			end if
		next
	else							'Send Single Email
		mail.AddRecipient "", toEmail
		mail.SendMail
	end if
	set mail = nothing
end Function

'Bamboo SMTP
Function bamboo(fromName, fromEmail, toEmail, subject, body, contType)
	dim mail,I
	set mail      = Server.CreateObject("Bamboo.SMTP") 
	mail.Server   = pSmtpServer
	mail.From     = fromEmail
	mail.FromName = fromName
	mail.Subject  = subject
	mail.Message  = body
	if contType = 1 then			'Send HTML Email
		mail.ContentType = "text/html"
	end if
	if isArray(toEmail) then		'Send Multiple Emails
		for I = 0 to Ubound(toEmail)
			if len(toEmail(I)) > 0 then
				mail.Rcpt = toEmail(I)
				mail.Send 
			end if
		next
	else							'Send Single Email
		mail.Rcpt = toEmail
		mail.Send 
	end if
	set mail      = nothing
end Function

'CDOSYS without Server Settings
Function CDOSYS(fromName, fromEmail, toEmail, subject, body, contType)

dim mail

	Set mail=CreateObject("CDO.Message")
	mail.Subject	=	subject
	mail.From		=	fromEmail
	if contType = 1 then			'Send HTML Email
		mail.HTMLBody = body
	else
		mail.TextBody = body
	end if	
	if isArray(toEmail) then		'Send Multiple Emails
		for I = 0 to Ubound(toEmail)
			if len(toEmail(I)) > 0 then
				mail.To = toEmail(I)
				mail.Send
			end if
		next
	else							'Send Single Email
		mail.To = toEmail
		mail.Send
	end if
	
end Function

'CDOSYS with SMTP Server Specified
Function CDOSYSwithServer(fromName, fromEmail, toEmail, subject, body, contType)

dim mail,conf,flds,I
	
	Set mail = Server.CreateObject("CDO.Message")
	Set conf = Server.CreateObject("CDO.Configuration")
	Set flds = conf.Fields

	'Configure the server
	flds("http://schemas.microsoft.com/cdo/configuration/smtpserver") = pSmtpServer
	flds("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
	flds("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
	flds("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60
	flds.Update
	mail.Configuration = conf
	
	'Message
	mail.From = fromEmail           'fromEmail & " <" & fromEmail & ">"
	mail.Subject = subject
	if contType = 1 then			'Send HTML Email
		mail.HTMLBody = body
	else
		mail.TextBody = body
	end if
	if isArray(toEmail) then		'Send Multiple Emails
		for I = 0 to Ubound(toEmail)
			if len(toEmail(I)) > 0 then
				mail.To = toEmail(I)
				mail.Send
			end if
		next
	else							'Send Single Email
		mail.To = toEmail
		mail.Send
	end if
	
	'Clean up
	set mail = nothing
	set flds = nothing
	set conf = nothing

end Function

'ActiveXperts ActiveEmail - http://www.activexperts.com/activemail/
Function ActiveEmail(fromName, fromEmail, toEmail, subject, body, contType)

dim mail, I

	if isArray(toEmail) then		'Send Multiple Emails
		for I = 0 to Ubound(toEmail)
			if len(toEmail(I)) > 0 then
				Set mail = CreateObject("ActiveXperts.SmtpMail")
				mail.HostName = pSmtpServer				
				mail.FromAddress = fromEmail
				
				mail.AccountName = "clsupport"
				mail.AccountPassword = "l3tm31n"
				
				'mail.FromAddress = "case.manager@complianceline.com"
				mail.FromName = fromEmail		
				mail.ReplyAddress = fromEmail
				mail.Organization = pCompany		
				mail.Subject = subject				
				mail.Body = body
				mail.BodyType = contType			
				mail.AddTo toEmail(I), toEmail(I)
				mail.Queue
			end if
		next
	else							'Send Single Email
		Set mail = CreateObject("ActiveXperts.SmtpMail")
		mail.HostName = pSmtpServer				

		mail.FromAddress = fromEmail
		'''mail.FromAddress = "case.manager@complianceline.com"
		'''mail.FromName = session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")
		mail.FromName = fromEmail				

		mail.AccountName = "clsupport"
		mail.AccountPassword = "l3tm31n"

		mail.ReplyAddress = fromEmail
		mail.Organization = pCompany		
		'''>>mail.ReadReceiptAddress = fromEmail
		'''>>mail.Priority = 1
		mail.Subject = subject
		mail.Body = body
		mail.BodyType = contType			
		mail.AddTo toEmail, toEmail
		mail.Queue
	end if
	
end Function

'Log Mail Event
Sub logMail(fromName, fromEmail, toEmail, subject, body, mailComp, contType, mailError)

dim mailDate

	mailDate = CStr(Now())
		
	'Add Record to MailLog
	mySQL = "INSERT INTO MailLog (" _
		  & "mailDate,mailComp,contType,mailError,alias,fromEmail,toEmail,subject,body" _
		  & ") VALUES (" _
		  & "'" & mailDate						& "'," _
		  & "'" & mailComp						& "'," _
		  & "'" & contType						& "'," _
		  & "'" & replace(mailError,"'","''")	& "'," _
		  & "'" & replace(fromName,"'","''")	& "'," _
		  & "'" & fromEmail						& "'," _
		  & "'" & toEmail						& "'," _
		  & "'" & replace(subject,"'","''")		& "'," _
		  & "'" & replace(body,"'","''")		& "' " _
		  & ")"
	set rs = openRSexecute(mySQL)

end Sub
%>