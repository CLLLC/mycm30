<%@ Language=VBScript %>

 <%
 dim style, chart
 Dim myArray(4)
 
	' Create a new Swiff Chart object.
	Set chart= Server.CreateObject("SwiffChartObject.ChartObj")

	' Fill the chart with data stored in the query string
	chart.SetSeparators ";", true	
	chart.SetDataFromQuery

	' Get style
	style = Request.QueryString("loadstyle")
	chart.LoadStyle style

	chart.SetOutputFormat "PNG"
	
	' Finally generate the Flash chart and provide the browser with it	
	chart.ExportAsResponse
	
	' Release the chart object
	Set chart= Nothing
	Response.End 

 %>
