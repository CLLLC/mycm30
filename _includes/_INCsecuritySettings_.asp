        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Settings div -->
        <div id="tab:settings" style="display:none;">        
        
            <!-- START Settings table -->   
	        <table id="table_settings" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>
                    
                    <td valign="top">
                                        
					  	<!-- START Settings Form Fields -->	                    
						<table id="table:settings:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">                           

                            <tr>
                              <td class="formLabel">BLANK</td>
                              <td colspan="3" align=left nowrap>this page left blank on purpose.</td>
                            </tr>
                            
						</table>
   						<!-- END Settings Form Fields -->

                    </td>
                                        
                </tr>
            
            </table>
            <!-- END Settings table -->
	
    	</div>
		<!-- STOP Settings div -->        


