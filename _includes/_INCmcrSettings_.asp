        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Settings div -->
        <div id="tab:settings" style="display:none;">        
        
            <!-- START Settings table -->   
	        <table id="table_settings" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>
                    
                    <td valign="top">
                                        
					  	<!-- START Settings Form Fields -->	                    
						<table id="table:settings:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">                           

                            <tr>
                              	<td class="formLabel">Config Active:</td>
                              	<td align="left" nowrap >        
                                	<label>
                                 		<input type="radio" name="mcrActive" id="mcrActive" onclick="setupOptions();" value="Y" style="vertical-align:middle;" <%=checkRadio(mcrActive,"Y")%> >Yes&nbsp;&nbsp;&nbsp;&nbsp;
                                     </label>			      
                                  	<label>
                                   		<input type="radio" name="mcrActive" id="mcrActive" onclick="setupOptions();" value="N" style="vertical-align:middle;" <%=checkRadio(mcrActive,"N")%>>No
                                  	</label>
                                    <div class="subLabel" style="padding-top:5px;">Activates or disables current configuration.</div>
                              	</td>
                            </tr>                            

                            <tr>
                              	<td class="formLabel">Language:</td>
                                <td align=left nowrap>                                
									<%
                                    'Find Language available
                                    mySQL="SELECT LanguageCD, LanguageName " _
                                        & "FROM   Language " _
                                        & "ORDER BY LanguageName "                                                                    
									set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
									'build combo box
									response.write("<select name=""mcrLanguageCD"" id=""mcrLanguageCD"" size=1 class=""inputLong"">")
										response.write("<option value=""en-US"">--select--</option>")									
										do until rs.eof			
											response.write("<option value=""" & rs("languagecd") & """ " & checkMatch(mcrLanguageCD,rs("languagecd")) & ">" & rs("languagename") & "</option>")
											rs.movenext
										loop
									response.write("</select>")
									call closeRS(rs)
									%>
                                    <div class="subLabel" style="padding-top:5px;">Sets language for this configuration (default is English).</div>                                    
                                </td>
                            </tr>                            
                                                              
						</table>
   						<!-- END Settings Form Fields -->

                    </td>
                                        
                </tr>
            
            </table>
            <!-- END Settings table -->
	
    	</div>
		<!-- STOP Settings div -->        


