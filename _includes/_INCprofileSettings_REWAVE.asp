        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

		<!-- START Settings div -->
        <div id="tab:settings" style="display:none;">        
        
            <!-- START Settings table -->   
	        <table id="table_settings" cellpadding="0" cellspacing="0" style="width:100%;">
				
                <tr>
                    
                    <td valign="top">
                                        
					  	<!-- START Settings Form Fields -->	                    
						<table id="table:settings:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">                           

                            <!-- START Issue Type Table -->
                            <tr>
                                <td class="formLabel">Issue Types:</td>    
                                <td align=left> 
                    
                                    <div id="field_list_container">
                                        <div id="field_list" style="width:300px; border: 1px solid #ccc; padding: 2px; height:57px; overflow:auto; overflow-x: hidden; overflow-y: scroll;">
                                            <!-- table used for javascript slider -->
                                            <table id="table:field:list" width="95%" border="0" cellspacing="0" cellpadding="0">
                                                <%
                                                dim arrIssues, checkIssue
                                                                            
                                                'find all issue types already assigned to category
                                                if lCase(action) = "edit" then
                                                    mySQL = "SELECT IssueTypeID " _
                                                          & "	FROM Customer_IssueType " _
                                                          & "	WHERE CustomerID='" & customerID & "' AND Active ='Y'"
                                                    set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                                    do while not rs.eof
                                                        arrIssues = arrIssues & "|" & rs("IssueTypeID")
                                                        rs.movenext
                                                    loop
                                                    arrIssues = arrIssues & "|"
                                                    call closeRS(rs)
                                                end if
                                                
                                                'find all issue types for this customer
                                                mySQL = "SELECT IssueTypeID, Name " _
                                                      & "	FROM IssueType " _
                                                      & "	ORDER BY IssueTypeID "													
                                                set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
                                                do while not rs.eof
                                                    if instr(arrIssues, "|" & rs("IssueTypeID") & "|") > 0 then
                                                        checkIssue = " checked "
                                                    else
                                                        checkIssue = " "
                                                    end if								
                                                    response.write("<tr><td class=""clearFormat"" style=""border:none; padding:0px; margin:0px;"" align=""left"">")                                                    
                                                    response.write("	<div id=""field_list_item_Name"" class=""checkItem"">")											
                                                    response.write("		<input name=""list_checkBox"" id=""field_list_checkBox"" type=""checkbox"" style=""cursor:default;"" value=""" & rs("IssueTypeID") & """ " & checkIssue & " />")
                                                    response.write("		<label for=""field_list_checkBox"" style=""cursor:move;"" onclick=""javascript:return false;"">" & rs("name") & "</label>")
                                                    response.write("	</div>")    
                                                    response.write("</td></tr>")          
                    
                                                    rs.movenext
                                                loop
                                                call closeRS(rs)
                                                %>
                                            </table>
                                            <!-- Initiate listOrder.js for moving fields in order -->
                                            <script type="text/javascript">
                                                var table = document.getElementById('table:field:list');
                                                var tableDnD = new TableDnD();
                                                tableDnD.init(table);
                                            </script>
                                        </div>
                                    </div>
                    
                                    <div style="margin-top:5px;">Select: <a href="#" onClick="javascript:fieldCheckList('frm','field_list_checkBox','checked'); return false;">All</a> | <a href="#" onClick="javascript:fieldCheckList('frm','list_checkBox',''); return false;">None</a></div>
                    
                                </td>
                    
                            </tr>                       
                            <!-- STOP Issue Type Table -->                           

                            <tr>
                              	<td class="formLabel">Sub Categories:</td>
                              	<td colspan="3" align=left nowrap>        
                                    <label>
                                        <input type="radio" name="cusAppPopUpSubCategoryRS" id="cusAppPopUpSubCategoryRSYes" value="Y" style="vertical-align:middle;" <%=checkRadio(cusAppPopUpSubCategoryRS,"Y")%>>Yes&nbsp;&nbsp;
                                    </label>			
    
                                    <label>
                                        <input type="radio" name="cusAppPopUpSubCategoryRS" id="cusAppPopUpSubCategoryRSNo" value="N" style="vertical-align:middle;" <%=checkRadio(cusAppPopUpSubCategoryRS,"N")%>>No
                                    </label>
                                	<div class="subLabel" style="padding-top:5px;">Makes sub-categories available to <strong>Risk Specialist</strong> when creating new issues.</div>                                    
                              	</td>
                            </tr>                            

                            <tr>
                              	<td class="formLabel">Follow-Up Categories:</td>
                              	<td colspan="3" align=left nowrap>        
                                    <label>
                                        <input type="radio" name="cusAppAssignFUPCategory" id="cusAppAssignFUPCategoryYes" value="Y" style="vertical-align:middle;" <%=checkRadio(cusAppAssignFUPCategory,"Y")%>>Assign&nbsp;&nbsp;
                                    </label>			
    
                                    <label>
                                        <input type="radio" name="cusAppAssignFUPCategory" id="cusAppAssignFUPCategoryNo" value="N" style="vertical-align:middle;" <%=checkRadio(cusAppAssignFUPCategory,"N")%>>Unassigned
                                    </label>
                                	<div class="subLabel" style="padding-top:5px;">Automatically assign categories from original issue to new follow-ups.</div>                                    
                              	</td>
                            </tr>                            
                        
							<tr>
                                <td class="formLabel">Custom Questions:</td>
                                <td align=left>                                
                                    <label>
                                        <input type="radio" name="cusQuestion" id="cusQuestionYes" value="Y" style="vertical-align:middle;" <%=checkRadio(cusQuestion,"Y")%>>Visible&nbsp;&nbsp;
                                    </label>			
    
                                    <label>
                                        <input type="radio" name="cusQuestion" id="cusQuestionNo" value="N" style="vertical-align:middle;" <%=checkRadio(cusQuestion,"N")%>>Invisible
                                    </label>
                                	<div class="subLabel" style="padding-top:5px;">Questions found under 'Details' tab of issue page.</div>
                                </td>                                
							</tr>

                            <tr>
                              	<td class="formLabel">Follow-Up Managers:</td>
                              	<td colspan="3" align=left nowrap>        
                                    <label>
                                        <input type="radio" name="cusAppAssignFUPLogid" id="cusAppAssignFUPLogidYes" value="Y" style="vertical-align:middle;" <%=checkRadio(cusAppAssignFUPLogid,"Y")%>>Assign&nbsp;&nbsp;
                                    </label>			
    
                                    <label>
                                        <input type="radio" name="cusAppAssignFUPLogid" id="cusAppAssignFUPLogidNo" value="N" style="vertical-align:middle;" <%=checkRadio(cusAppAssignFUPLogid,"N")%>>Unassigned
                                    </label>
                                	<div class="subLabel" style="padding-top:5px;">Automatically assign case managers from original issue to new follow-ups.</div>                                    
                              	</td>
                            </tr>                            

                            <tr>
                              	<td class="formLabel">User Fields:</td>
                              	<td colspan="3" align=left nowrap>        
                                    <label>
                                        <input type="radio" name="cusAppShowUserFields" id="cusAppShowUserFieldsYes" value="Y" style="vertical-align:middle;" <%=checkRadio(cusAppShowUserFields,"Y")%>>Show Unassigned&nbsp;&nbsp;
                                    </label>			
    
                                    <label>
                                        <input type="radio" name="cusAppShowUserFields" id="cusAppShowUserFieldsNo" value="N" style="vertical-align:middle;" <%=checkRadio(cusAppShowUserFields,"N")%>>Hide Unassigned
                                    </label>
                                	<div class="subLabel" style="padding-top:5px;">User Fields not assigned to a User Group will show disabled.</div>                                    
                              	</td>
                            </tr>                                                                                                                                            
                                          
						</table>
   						<!-- END Settings Form Fields -->

                    </td>
                                        
                </tr>
            
            </table>
            <!-- END Settings table -->
	
    	</div>
		<!-- STOP Settings div -->        


