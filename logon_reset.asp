<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement, Pro, Lite, Metrics
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="_includes/_INCconfig_.asp"-->
<!--#include file="_includes/_INCappFunctions_.asp"-->
<!--#include file="_includes/_INCappDBConn_.asp"-->

<%
'Database
dim mySQL, cn, rs

dim userName
dim userPassword
dim passwordStrength
dim passwordHistory

dim msg


'*********************************************************
'Open Database Connection
call openDB()

'Site Configuration
if loadConfig() = false then	
	response.redirect "error/default.asp?errMsg=" & server.URLEncode("Could not load Configuration settings.")
end if
'*********************************************************


'Get possible redirect
dim urlLink
urlLink = request.querystring("redirect")

'Get action
dim action
action = trim(Request.QueryString("action"))
if action = "" then	
	response.redirect "logon.asp"
end if

'Get UserID
dim userLogID
if action = "reset" then
	userLogID = trim(Request.QueryString("recId"))
	if userLogID = "" then			
		response.redirect "logon.asp"
	end if
	'check for invalid user id or someone spoofing the URL
	if (cLng(userLogID) <> cLng(session(session("siteID") & "logid"))) then
		response.redirect "logon.asp"
	end if	
end if

'Get user values
if action = "reset" then
	mySQL="SELECT Logins.*, Customer.appPasswordStrength " _
		& "		FROM Customer INNER JOIN Logins ON Customer.CustomerID = Logins.CustomerID " _
		& "		WHERE LOGID='" & userLogID & "' "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	userName			= rs("username")
	userPassword		= rs("password")
	passwordStrength	= rs("appPasswordStrength")
	passwordHistory		= rs("passwordHistory")
	call closeRS(rs)	
else	
	response.redirect "logon.asp"
end if

%>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>myCM 3.0</TITLE>

	<link rel="shortcut icon" href="_images/favicon.ico" >
    	
	<link type="text/css" rel="stylesheet" href="_css/default.css" />
	<link type="text/css" rel="stylesheet" href="_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="_css/text.css" />
    <link type="text/css" rel="stylesheet" href="_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="_css/statusMessage.css" />

	<!--JQuery required components -->
    <script language="JavaScript" src="_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="_jquery/jquery-ui-1.8.11.custom.min.js"></script>
    
	<link rel="stylesheet" type="text/css" href="_jquery/password/simplePassMeter.css"/>    
	<script language="JavaScript" src="_jquery/password/jquery.simplePassMeter-0.4.js"></script>    
    
</head>

<body topmargin="0">

<form name="frm" id="frm" method="post" action="profiles/users_exec.asp" style="padding:0px; margin:0px;">

  <div style="margin-bottom:25px;">&nbsp;</div>

  <div id="content">
    
    <table width="375" cellpadding="0" cellspacing="0" border="0" align="center">
    
          <tr>
            <td style="padding:35px;">&nbsp;</td>
          </tr>        
    
      <tr>
        <td style="background-color: #3B5998;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="padding-left:10px;"><b><font size=3 color="#FFFFFF">Login</font></b></td>
                    <td align="right"><img src="_images/login2.jpg"></td>
                </tr>
            </table>
        </td>
      </tr>
      
      <tr>  
        <td style="border: 1px solid #3B5998; padding:10px; background:url(_images/login_bg.jpg) repeat;">


                <table width="375" border="0" align="center" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="padding-top:10px; padding-right:30px;">
                            <b><font size=3>Password Reset</font></b><br><br>
                            Your password has expired or needs to be reset.
                        </td>
                    </tr>
                    
                    
							<tr>
                                <td align="left" valign="top">
                                
                                	<table>
                                    	<tr>
                                        	<td style="border:none; padding:0px; margin:0px;">
                                                <div class="subLabel" style="padding-top:5px; padding-bottom:5px;">Enter new password:</div>
                                                <div><input name="userPassword1" id="userPassword1" class="inputMedium" type="password" value="" size=20 maxlength=20></div>
                                                <div class="subLabel" style="padding-top:5px; padding-bottom:5px;">Confirm password:</div>
                                                <div><input name="userPassword2" id="userPassword2" class="inputMedium" type="password" value="" size=20 maxlength=20></div>
											</td>
                                            <td style="border:none; padding:0px; margin:0px;">
				                                <div id='passMeter'></div>
                                            </td>
                                        </tr>
                                    </table>
                        
                                    <!-- STRONG password -->
                                    <% if lCase(passwordStrength) = "strong" then %>
										<script>
                                            $('#userPassword1').simplePassMeter({
                                                'requirements': {
                                                    'matchField': {'value': '#userPassword2'},
                                                    'minLength': {'value': 8},  // at least 8 characters
                                                    'lower': {'value': true},   // at least 1 lower-case letter
                                                    'upper': {'value': true},   // at least 1 upper-case letter
                                                    'special': {'value': true} 	// at least 1 special character
                                                },
                                                'offset': 10,
												'container': '#passMeter'
                                            });
											$('#userPassword1').bind('score.simplePassMeter', function(jQEvent, score) {$('#passwordScore').text(score);});
                                        </script>

                                    <!-- MEDIUM password -->                                        
                                    <% elseif lCase(passwordStrength) = "medium" then %>                                    
										<script>
                                            $('#userPassword1').simplePassMeter({
                                                'requirements': {
                                                    'matchField': {'value': '#userPassword2'},
                                                    'minLength': {'value': 6},  // at least 6 characters
                                                    'lower': {'value': true},   // at least 1 lower-case letter
                                                    'upper': {'value': true},   // at least 1 upper-case letter
                                                    'special': {'value': false} // NOT least 1 special character
                                                },
                                                'offset': 10,
												'container': '#passMeter'
                                            });
											$('#userPassword1').bind('score.simplePassMeter', function(jQEvent, score) {$('#passwordScore').text(score);});
                                        </script>                                    

                                    <!-- WEAK password -->                                        
                                    <% else %>
										<script>
                                            $('#userPassword1').simplePassMeter({
                                                'requirements': {
                                                    'matchField': {'value': '#userPassword2'}
                                                },
                                                'offset': 10,
												'container': '#passMeter'												
                                            });
											$('#userPassword1').bind('score.simplePassMeter', function(jQEvent, score) {$('#passwordScore').text(score);});
                                        </script>                                    
                                    <% end if %>                                    

									<span id='passwordScore' style="display:none;"></span>
                                    
                                  	<input type="hidden" name="userPassword" value="<% =userPassword %>"/>
	    	                        <input type="hidden" name="userLogID" value="<% =userLogID %>">
                                    <input type="hidden" name="userName" value="<% =userName %>">                                    
                                  	<input type="hidden" name="passwordStrength" id="passwordStrength" value="<% =passwordStrength %>"/>
                                  	<input type="hidden" name="passwordHistory" id="passwordHistory" value="<% =passwordHistory %>"/>
                                    <input type="hidden" name="urlLink" id="urlLink" value="<% =urlLink %>">
    	    	                    <input type="hidden" name="action" id="action" value="passreset">

                                </td>
							</tr>
                            
                            
                            
                    <tr>
                        <td style="padding-right:30px;"><input class="submitButton" type="button" name="Submit" onClick="javascript:submitForm()" value="Save">&nbsp;&nbsp;[&nbsp;<a href="logon.asp">Return to Login</a>&nbsp;]</td>
                    </tr>
                    <tr>
                        <td style="padding-top:10px; padding-right:30px;">&nbsp;</td>
                     </tr>
                </table>            
                            
        </td>
      </tr>
	  <tr>
	    <td style="padding-top:10px;">
        	<div style="float:left;">Copyright &copy; 2011</div>
            <div style="float:right;"><a href="http://www.complianceconcepts.com" target="_blank">Compliance Concepts, Inc.</a></div>        
        </td>
      </tr>
    </table>

  </div>

</form>

</body>
</html>

<%
'close database connection
call closeDB()
%>

<script language="javascript">
	function submitForm() {
		//used to check password strenght
		var valid = document.getElementById('passwordScore');
		var pass1 = document.getElementById('userPassword1').value;
		var pass2 = document.getElementById('userPassword2').value;
		var passStrength = document.getElementById('passwordStrength').value;
		var passHistory = document.getElementById('passwordHistory').value;
		//password entered
		if (pass1) {
			//good password so far...
			if (valid.innerHTML >= 25) {
				if(pass1 == pass2) {
					//check password history
					////if (passStrength == "Strong") {
						var temp = passHistory.split("*|*");
						var i;
						for(i=0; i<temp.length; i++) {
							if (pass1 == temp[i]) {
								alert("Password cannot match any of the past 6 passwords used. Please try again.");
								return false;
							}
						}
						//good, submit
						document.frm.submit();
					////}
					//no need to check
					////else {
					////	document.frm.submit();
					////}
					
				}
				//passwords do NOT match
				else {
					alert("Passwords do not match. Please try again.");
					return false;
				}
			}
			//bad password, score below 25
			else {
				alert("Password invalid. Please try again.");
				return false;
			}			
		}
		//NO password change
		else {
			document.frm.submit();
		}
		
	}		
</script>
