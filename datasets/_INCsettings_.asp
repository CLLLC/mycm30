        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

        <!-- START Settings table -->   
		<table id="table_settings" cellpadding="0" cellspacing="0" style="width:100%;">				
            <tr>                    
                <td valign="top">
                                        
				  	<!-- START Settings Form Fields -->	                    
					<table id="table:settings:general" width="100%" class="formTable" cellpadding="0" cellspacing="0">

						<tr>
                        	<td class="formLabel"><span class="required">*</span>Name:</td>
                            <td align=left nowrap>
                            	<input name="optName" id="optName" class="inputLong" value="<% =optName %>" maxlength="255" />
                            </td>
                        </tr>
                            
						<tr>
                        	<td class="formLabel"><span class="required">*</span>Owner:</td>
                            <td align=left nowrap style="padding-bottom:0px;">                                                            	 
                              	<div style="float:left;">
                                	<input name="rptOwner" id="rptOwner" class="inputMediumLong" value="<% =optOwner %>" maxlength="255" readonly="readonly" />
                                    <input name="rptLogid" id="rptLogid" type="hidden" value="<% =optUser %>" >
                                </div>
                                <div><a class="myCMbuttonSmall" href="#" title="Change Owner" onclick="this.blur(); SimpleModal.open('../_dialogs/popup_user.asp?recid=<% =idIssue %>&cid=<% =customerID %>&multi=false&pageView=reports', 380, 450, 'no'); resetSystemMsg(); return false;"><span class="user" style="padding-right:7px;">&nbsp;</span></a></div>
                            </td>
                   		</tr>
      					<tr>
                        	<td style="border-top:none; padding-bottom:5px;">&nbsp;</td>
                            <td align=left nowrap style="border-top:none; padding:0px;">                               
								<div class="subLabel" >Owner has full control over report and its settings.</div>
                            </td>
						</tr>












      					<tr <% if lCase(optType)<>"crystal" then response.write("style=""display:none;""") %>>
                           	<td class="formLabel"><span class="required">*</span>Report File:</td>
                          	<td align=left nowrap style="padding-bottom:0px;">                                                            	 
                         		<div style="float:left; margin-right:5px;">
                
                                    <%
                                    dim viewObjects, Upload, tmpItemCount
                                    dim Directory, Dir, extPos, extImg, Item
                                                        
                                    'ignore any errors
                                    On Error Resume Next
                                    'Set directory and find all files
                                    Directory = dataDir & "\profiles\" & optDatasetCustID & "\crystal\"
                                                       
                                    'Open new ASPUpload object		
                                    Set Upload = Server.CreateObject("Persits.Upload")			
                                    Set Dir = Upload.Directory( Directory & "*.rpt", , True)

									%>									
                                    <select name="optFileName" id="optFileName" size=1 >
                               			<option value="">-- Select --</option>
										 <%                                                     
                                        'Error 33 equal 'Directory not found'
                                        if Err.Number <> 33 then		                  									
                                            For Each Item in Dir
                                                if Not Item.IsSubdirectory Then
                                          			Response.Write "<option value='" & Server.HtmlEncode(Item.FileName) & "' " & checkMatch(crystalFile,Server.HtmlEncode(Item.FileName)) & ">" & Server.HtmlEncode(Item.FileName) & "</option>"
												end if		
                                            'move to next document					
                                            next
                                        end if         

										'reset error handling
										On error goto 0		  
										       
	                                    %>                                
                                 	</select>                                                                                                                              
                                </div>
                                                                
                     		</td>
                     	</tr>
                        
                        
      					<tr <% if lCase(optType)<>"crystal" then response.write("style=""display:none;""") %>>
                      		<td style="border-top:none; padding-bottom:5px;">&nbsp;</td>
                        	<td align=left nowrap style="border-top:none; padding:0px;">                               
								<div class="subLabel">Crystal Report (.rpt) file for this report.
                                    <% if cLng(session(session("siteID") & "adminLoggedOn")) <= 10 then %>                     							                                    
										[&nbsp;<a href="#" onclick="SimpleModal.open('../_jquery/fancyupload/fancyupload-crystal.asp?cid=<% =optDatasetCustID %>', 400, 500, 'no'); resetSystemMsg('systemMessage'); return false;">Upload File</a>&nbsp;]									
									<% end if %>
                                </div>
                         	</td>
                      	</tr>
                        
                        

      					<tr>
                        	<td class="formLabel">
                              	<% if cLng(sLogid) = cLng(optUser) then %>
		                      		<!-- ALL users can edit their own report categories -->
    	                           	<a href="#" class="formLabelLinkImg" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=logins&field=ReportsCategory&form=optCategory&pageView=reports', 320, 375, 'no'); return false;" onmouseover="javascript:configImage('congif_Category','over');" onmouseout="javascript:configImage('congif_Category','out');"><img id="congif_Category" src="../_images/x_cleardot_16.gif" align="absmiddle" border="0" /></a>
        	                      	<a href="#" class="formLabelLink" title="Configure Option" onclick="SimpleModal.open('../_dialogs/popup_config_options.asp?cid=<% =customerID %>&type=logins&field=ReportsCategory&form=optCategory&pageView=reports', 320, 375, 'no'); return false;" onmouseover="javascript:configImage('congif_Category','over');" onmouseout="javascript:configImage('congif_Category','out');">Category:</a>
                                <%
								else
									response.write("Category:")
                                end if 
								%>
                            </td>
                            <td align=left nowrap style="padding-bottom:0px;">                                                            	 
                              	<div style="float:left;">
                                	<%
									dim arrCategory, myMatch
                                    mySQL="SELECT FirstName, ReportsCategory " _
                                        & "FROM   Logins " _
                                        & "WHERE  Logid=" & optUser & " AND CustomerID='" & customerID & "' "
									set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
									if not rs.eof then
										arrCategory = rs("ReportsCategory")
									end if
									call closeRS(rs)
									'build combobox
									call buildSelect(arrCategory,optCategory,"optCategory","inputMediumLong","Y")
                                    %>
                                </div>
                            </td>
                   		</tr>
                        
      					<tr>
                      		<td style="border-top:none; padding-bottom:5px;">&nbsp;</td>
                        	<td align=left nowrap style="border-top:none; padding:0px;">                               
                                <div class="subLabel">Allows for the organizing of reports.</div>
                       		</td>
                      	</tr>






					</table>
   					<!-- END Settings Form Fields -->

				</td>                                        
			</tr>            
		</table>
		<!-- END Settings table -->

		<script language="javascript">
			//set new RPT file...called from fancyupload-crystal.asp
			function addDocUploaded(file,date,size,ext){						
				var i;
				var el = document.getElementById('optFileName');				
				//check for existance of report		
				var bolMatch = false;
				for (i=0; i<el.options.length; i++) {
					if (el.options[i].text == file) {
						bolMatch = true; //origianl already exists in list
						break;
					}
				}						
				//report not found in list
				if (bolMatch == false){
					//add new report to dropdown
					addOptionsItems('optFileName',file,file)			
				}				
				//select report in dropdoown
				for (i=0; i<el.options.length; i++) {
					if (el.options[i].text == file) {
						el.selectedIndex = i;
					}
				}											
			}
		</script>

