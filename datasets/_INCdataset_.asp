        <!--
        Form validation functions:    
        __ Any ID with "required:" entered will be validated based on type of element (i.e. text, radio)
        __ Any ID with "date:" entered will be validated for a proper date format (mm/dd/yyyy)	   
           [ex: id="required:date:callBack" = this is a required field with date format checking]
        -->

        <table width="100%" class="formTable" border="0" cellspacing="0" cellpadding="0">
        	<tr><td style="padding:0px; margin:0px;">&nbsp;</td></tr>
        </table>
        
       	<%
        mySQL = "SELECT DatasetID, Name, 'Built-In' AS [Type], Type as [Style], Description " _
              & "FROM   Dataset " _
              & "WHERE  CustomerID='MYCM' and SecurityLevel=" & session(session("siteID") & "adminLoggedOn") & " "
		if cLng(session(session("siteID") & "adminLoggedOn")) > 10 then
			mySQL = mySQL & " AND Type <> 'Crystal' "
		end if
        mySQL = mySQL & " UNION ALL " _
			  & "SELECT DatasetID, Name, 'User-Defined' AS [Type], Type as [Style], Description " _
              & "FROM   Dataset " _
              & "WHERE  CustomerID='" & sCustomerID & "' and SecurityLevel=" & session(session("siteID") & "adminLoggedOn") & " "
		if cLng(session(session("siteID") & "adminLoggedOn")) > 10 then
			mySQL = mySQL & " AND Type <> 'Crystal' "
		end if		  
	  	mySQL = mySQL & " ORDER BY Name "
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		%>

        <!-- START Configuration table -->           
        <table id="table_datasets" width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#969696;">
            <thead>
                <tr>
                    <th align="left" nowrap="nowrap">Name</th>
                    <th align="left" nowrap="nowrap">Style</th>       
                    <th align="left" nowrap="nowrap">Type</th>                           
              </tr>
            </thead>
            <tbody>

			<%        
			do until rs.eof			
				response.write("<tr>")
				response.write("	<td align=""left"">")
				response.write("		<div style=""height:22px;""><label><input type=""radio"" name=""optDataset"" value=""" & rs("datasetid") & """><strong>" & rs("name") & "</strong></label></div>")
				response.write("		<div style=""padding-left:21px;"">" & rs("description") & "</div>")
				response.write("	</td>")				
				response.write("	<td align=""left"">")				
				if lCase(rs("style")) = "register" then
					response.write("<img src=""../_images/icons/16/table.png"" title="""" width=""16"" height=""16"" align=""absmiddle"" style=""margin-right:5px;"">")					
				elseif lCase(rs("style")) = "calculated" then
					response.write("<img src=""../_images/icons/16/table_sum.png"" title="""" width=""16"" height=""16"" align=""absmiddle"" style=""margin-right:5px;"">")			
				elseif lCase(rs("style")) = "sql" then
					response.write("<img src=""../_images/icons/16/table_link.png"" title="""" width=""16"" height=""16"" align=""absmiddle"" style=""margin-right:5px;"">")							
				elseif lCase(rs("style")) = "crystal" then
					response.write("<img src=""../_images/icons/16/file_extension_rpt.png"" title="""" width=""16"" height=""16"" align=""absmiddle"" style=""margin-right:5px;"">")							
				end if												
				response.write(		rs("style") & "&nbsp;")
				response.write("	</td>")
				
				response.write("	<td align=""left"">" & rs("type") & "&nbsp;</td>")				
				
				response.write("</tr>")				
				rs.movenext
			loop					
			%>            
            
		</table>
        <!-- END Issues Details table -->

			<%
			dim tableProperty
			tableProperty = "sort: true, sort_config:{sort_types:['String','String','String']}, filters_row_index: 1, " _
						  & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
						  & "status_bar: true, col_0: ""input"", col_1: ""select"", col_2: ""select"", " _
						  & "col_width:[""75%"",null,null], paging: true, paging_length: 10, " _
						  & "enable_non_empty_option: true, or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, " _
						  & "highlight_keywords: true, " _
						  & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
						  & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"			
			%>
            <!-- STOP Reports table -->

            <!-- Fire table build -->
			<script language="javascript" type="text/javascript">   															
				//<![CDATA[	
				var tableProp = {<% =tableProperty %>};
				//initiate table setup
				var tf1 = setFilterGrid("table_datasets",tableProp);
				//]]>
			</script>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">         
	            <tr>
		            <td class="clearFormat" style="background:#D0D0D0" height="5px;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
        	    </tr>
            </table>
	
