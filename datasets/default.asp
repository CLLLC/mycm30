<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = false
const showDHTMLMenu = true
const helpID = 3
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "datasets"
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'validate security access
if loadUser(null) = false then	
	response.redirect ("../default.asp")
end if
'*********************************************************

dim showCRSID '...need for NEW search box

'Issue Fields on DETAILS tab
dim optID
dim crystalFile
dim intStep
dim strTemp

'Work Fields
dim action, idProduct, pgNotice
dim idIssue

'Work Fields
dim optArr, optArrIndex

'Used with Dynamic List Contact selections
dim intGroups, arrGroups, groupsI, intUsers
dim tmpUserID, tmpUserName, lstUserName
dim arrUsers, usersI


'Check User Security
if addReport = "N" then	
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized perform this function.")	
end if


'ACTUAL FIELDS USED
dim idReport

'used to determine what next
'step/page to show to user
dim formStep
formStep = trim(Request.Form("formStep")) 
if len(formStep) = 0 then
	formStep = trim(Request.QueryString("s"))
end if


dim optDataset, optType, optDatasetName, optDatasetCustID
optDataset = trim(Request.Form("optDataset")) 
if len(optDataset) = 0 then
	optDataset = trim(Request.QueryString("d"))
end if


customerID = trim(Request.Form("customerID"))
if len(customerID) = 0 then
	customerID = trim(Request.QueryString("cid"))
end if


if optDataset = "" and formStep > "1" then	
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Report Dataset.")
		
'user has selected dataset, check it...
elseif formStep > "1" then

    mySQL = "SELECT CustomerID, Type, Name " _
          & "FROM   Dataset " _
          & "WHERE  CustomerID='MYCM' and DatasetID=" & optDataset & " "		  
	if cLng(session(session("siteID") & "adminLoggedOn")) > 10 then
		mySQL = mySQL & " AND Type <> 'Crystal' "
	end if		  
    mySQL = mySQL &  " UNION ALL " _
		  & "SELECT CustomerID, Type, Name " _
          & "FROM   Dataset " _
          & "WHERE  CustomerID='" & customerID & "' and DatasetID=" & optDataset & " "
	if cLng(session(session("siteID") & "adminLoggedOn")) > 10 then
		mySQL = mySQL & " AND Type <> 'Crystal' "
	end if
		  
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if rs.eof then		
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Report Dataset.")
	else
		optDatasetCustID = rs("customerid")
		optType = rs("type")
		optDatasetName = rs("name")
	end if		
	call closeRS(rs)	
end if



dim optName
optName = trim(Request.Form("optName"))
if optName = "" and formStep > "2" then	
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Report Name.")
end if


'Page to return user too.
dim pageReturn
pageReturn = trim(Request.QueryString("return"))
if len(pageReturn) = 0 then
	pageReturn = trim(Request.Form("pageReturn"))
end if


dim optOwner, optUser
optUser = trim(Request.Form("rptLogid"))
if len(optUser) = 0 then
	optUser = trim(Request.QueryString("u"))
end if


if optUser = "" and formStep > "2" then	
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Report Owner.")
	
elseif formStep = "2" then

	'default to current user
	'optUser = sLogid
	
	mySQL="SELECT FirstName, LastName, Email " _
        & "FROM   Logins " _
        & "WHERE  CustomerID='" & customerID & "' AND LogID=" & optUser		
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		
	if rs.eof then		
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Report Owner.")
	else	
		'set optOwner...used for display only
		if len(rs("LastName")) > 0 then optOwner = rs("LastName")
		if len(rs("FirstName")) > 0 and len(optOwner) > 0 then optOwner = optOwner & ", " & rs("FirstName") else optOwner = rs("FirstName")
		if len(rs("Email")) > 0 then optOwner = optOwner & " [" & rs("Email") & "]"
	end if		

	call closeRS(rs)

end if



dim optFileName
optFileName = trim(Request.Form("optFileName"))
if optFileName = "" and formStep > "2" and lCase(optType)="crystal" then	
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Report File Name.")
end if

dim optCategory, optCategory_new_value
optCategory = trim(Request.Form("optCategory"))
optCategory_new_value = trim(Request.Form("optCategory_new_value"))

dim optChart

'save new report and send to reports_edit.asp
if formStep = "3" then	
	
	'is the user requesting a new category be added...the "true" is being sent by DHTML ComboBox
	if lCase(optCategory_new_value) = "true" then
		mySQL = "SELECT Logins.ReportsCategory FROM Logins " _
			  & "WHERE CustomerID='" & customerID & "' AND LogID=" & optUser
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		
		'add new category without carriage return
		if len(rs("ReportsCategory")) <= 0 then
			call closeRS(rs)
			'add new category
			mySQL = "UPDATE Logins SET " _ 
				  & "	Logins.ReportsCategory = '" & replace(optCategory,"'","''") & "'," _
				  & "	ModifiedBy="				& sLogid						& ", " _
				  & "	ModifiedDate='"    		 	& Now()						 	& "' "	_				
				  & "WHERE CustomerID='" & customerID & "' AND LogID=" & optUser		
		'append new category with carriage return
		else
			call closeRS(rs)
			'add new category
			mySQL = "UPDATE Logins SET " _
				  & "	Logins.ReportsCategory = Logins.ReportsCategory + CHAR(13) + CHAR(10) + '" & replace(optCategory,"'","''") & "'," _
				  & "	ModifiedBy="				& sLogid						& ", " _
				  & "	ModifiedDate='"    		 	& Now()						 	& "' "	_				
				  & "WHERE CustomerID='" & customerID & "' AND LogID=" & optUser				
		end if		
		'add new report category
		set rs = openRSexecute(mySQL)	
		
	end if

	mySQL = "SELECT Type FROM Dataset " _
		  & "WHERE DatasetID=" & optDataset		
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if rs("Type") = "Calculated" then
		optChart = "Column"
	end if
	
	'add report
	mySQL = "INSERT INTO Reports (" _
		  & "CustomerID,DatasetID,LogID,Name,Category,FileName,ChartType,ModifiedBy,ModifiedDate" _
	      & ") VALUES (" _
		  & "'"   	& customerID					& "'," _
		  & " "		& optDataset	  				& ", " _
		  & " "		& optUser		  				& ", " _
		  & "'"   	& replace(optName,"'","''")		& "'," _
		  & "'"   	& replace(optCategory,"'","''")	& "'," _		  
		  & "'"   	& optFileName					& "'," _	
		  & "'"   	& optChart						& "'," _			  		  
		  & " " 	& sLogid 						& ", " _
		  & "'" 	& Now()			 				& "' " _		  		  	  		  		  
	      & ")"
	set rs = openRSexecute(mySQL)

	'Get RPQID of INSERTed Record
	mySQL = "SELECT MAX(ReportID) AS idReport " _
		  & "FROM   Reports " _
		  & "WHERE  CustomerID='" & customerID & "' "
	set rs = openRSexecute(mySQL)
	idReport = rs("idReport")	
	
	session(session("siteID") & "okMsg") = "Report was added successfully."
	if lCase(optType)="crystal" then	
		response.redirect "../reports/crystal/execute.asp?type=edit&recID=" & idReport & "&cid=" & customerID & "&name=" & Server.URLEncode(optFileName) & "&return=" & pageReturn
	else
		response.redirect "../reports/reports_edit.asp?action=edit&recID=" & idReport & "&return=" & pageReturn
	end if
	
end if

%>

<% dim pageTitle : pageTitle = "Datasets" %>
<!--#include file="../_includes/_INCheaderLite_.asp"-->


<form name="frm" id="frm" method="post" action="default.asp" style="padding:0px; margin:0px;">            

<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
  
	    <!-- START Left sided area -->  
		<td width="190" align="right" valign="top">    

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="../profiles/users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>

              <!-- START Left sided menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">
    
                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->        
                    <% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>      
	                    <script>
   		                	stExpandSubTree('myCMtree',3,0);
   		                	// --> SET TO stExpandSubTree('myCMtree',2,0); WHEN SUB-MENUS ARE BACK OFF							
       		            </script>
            		<% else %>
	                    <script>
   		                	stExpandSubTree('myCMtree',2,0);
       		            </script>
                    <% end if %>
                    <!-- STOP side menu include -->

                </td>
              </tr>
              <!-- STOP Left sided menu -->

            </table>

		</td>
    	<!-- STOP Left sided area -->


        <!-- START Main Middle Section -->
        <td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">    

            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs());
       	        </script>
            </div>
    
            <div style="text-align:left; margin-bottom:10px;">
                <img src="../_images/icons/32/chart_bar_add.png" title="Issues" width="32" height="32" align="absmiddle"> <span class="pageTitle">Report: <em>none assigned</em></span>
            </div>                   
 
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
            	            <legend>Information</legend>
	                        <div align="left">
    	                        <img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px;">Use the interface below to select filters to apply to this report.
								<% response.write( getInlineHelp("getPageURL", 0) ) %>
        	                </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             
                                    
                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">&nbsp;</td>
                    <!-- STOP Information buttons -->
                        
                </tr>
            </table>        
            <!-- STOP information box -->


    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			'clear system messages
			call clearSessionMessages()
            %>            
    		<!-- STOP message notification box -->


            <!-- START Top Page section with tabs & buttons -->
            <table id="table_tabs" width="100%" cellpadding="0" cellspacing="0">            
                <tr>                
                
                   	<% if formStep < "1" or formStep = "" or isNull(formStep) then %>                
	                    <td height="40" width="70%" align="left"><span class="sectionTitle">Select Dataset</span></td>                
                   	<% elseif formStep = "2" then %>                                        
	                    <td height="40" width="70%" align="left"><span class="sectionTitle">Dataset: <% =optDatasetName %></span></td>                                    
					<% end if %>
                                            
                    <!-- START Submit Button at top of page--> 
                    <td nowrap style="padding-bottom:10px;" align="left">
                    	<% if formStep < "1" or formStep = "" or isNull(formStep) then %>
							<!-- NEXT button -->                            
							<a class="myCMbutton" href="#" accesskey="N" onClick="this.blur(); document.frm.submit(); return false;"><span class="next" style="padding-right:10px;"><u>N</u>ext</span></a>
                    	<% else %>
							<!-- ADD button -->
							<a class="myCMbutton" href="#" accesskey="A" onClick="this.blur(); addReport('<% =lCase(optType) %>'); return false;"><span class="add" style="padding-right:15px;"><u>A</u>dd</span></a>        
						<% end if %>
                        
                        <!-- CLOSE button -->
						<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); location.href='../reports/reports.asp'; return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>
                    </td>
                    <!-- STOP Submit Button -->
                             
                </tr>
            </table>
            <!-- STOP Top Page Tabs -->


            
            <!-- START Side Page Tabs [details] "id=tab:details" defines the section for the Top Tabs -->
            <% if formStep = "1" or formStep = "" or isNull(formStep) then %>
                <!-- hidden fields --> 
                <input name="formStep" id="formStep" type="hidden" value="2" />                                
				<input name="customerID" id="customerID" type="hidden" value="<% =sCustomerID %>" />
                <input name="rptLogid" id="rptLogid" type="hidden" value="<% =sLogid %>" >                
                <input type=hidden name="pageReturn" value="<% =pageReturn %>">                                       
                <!--#include file="_INCdataset_.asp"-->            
            <% end if %>
            <!-- STOP Side Page Tabs [details] -->    
        
        
            <!-- START Side Page Tabs [notes] "id=tab:notes" defines the section for the Top Tabs -->
            <% if formStep = "2" then %>            
               	<!-- hidden fields --> 
                <input type="hidden" name="formStep" value="3">
				<input name="customerID" type="hidden" value="<% =customerID %>" />                
                <input type="hidden" name="optType" value="<% =optType %>">                
                <input type="hidden" name="optDataset" value="<% =optDataset %>">
                <input type=hidden name="pageReturn" value="<% =pageReturn %>">                                       
                <!--#include file="_INCsettings_.asp"-->            
            <% end if %>                   
            <!-- STOP Side Page Tabs [notes] -->


			<div style="padding:10px;">&nbsp;</div>    
            
    
            <!-- START Final table -->        
            <table class="formTable" width="100%" cellpadding="0" cellspacing="0">      
                                                              
                <tr>
                    <td width="70%" valign="top" align="left" nowrap>&nbsp;</td>
                    
                    <!-- START Submit Button at bottom of page -->                 
                    <td valign="top" align="left" nowrap>                
                    	<% if formStep < "1" or formStep = "" or isNull(formStep) then %>
							<!-- NEXT button -->                            
							<a class="myCMbutton" href="#" accesskey="N" onClick="this.blur(); document.frm.submit(); return false;"><span class="next" style="padding-right:10px;"><u>N</u>ext</span></a>
                    	<% else %>        
							<!-- ADD button -->                                                    
							<a class="myCMbutton" href="#" accesskey="A" onClick="this.blur(); addReport('<% =lCase(optType) %>'); return false;"><span class="add" style="padding-right:15px;"><u>A</u>dd</span></a>        
						<% end if %>
                        
                        <!-- CLOSE button -->
						<a class="myCMbutton" href="#" onClick="this.blur(); location.href='../reports/reports.asp'; return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>
                    </td>
                    <!-- STOP Submit Button --> 
    
                </tr>      
                                      
            </table>
            <!-- STOP Final table -->                
            
    
	    </td>         
	    <!-- STOP Main Middle Section -->

 
    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
	</tr>
</table>
<!-- STOP Page -->

</form>

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<script language="javascript">
	function setOwner(text,id) {
		//used by child window to add selected Report Owners
		var addE = document.getElementById('rptOwner');
		addE.value = text

		//check and see if value being added is in Removed list
		var delE = document.getElementById('rptLogid');
		delE.value = id 
	}
	
	function addReport(pageView) {
		var frmSubmit = true;
		if (!document.getElementById('optName').value) {
			jAlert('<strong>Report Error</strong>! <br><br> You must enter a report name.', 'myCM Alert');
			frmSubmit = false;			
		}
		if (!document.getElementById('rptLogid').value) {
			jAlert('<strong>Report Error</strong>! <br><br> You must provide a report owner.', 'myCM Alert');
			frmSubmit = false;
		}	
		if (pageView=='crystal') {
			if (!document.getElementById('optFileName').value) {
				jAlert('<strong>Report Error</strong>! <br><br> You must provide a Crystal Report file.', 'myCM Alert');
				frmSubmit = false;
			}			
		}
		//submit for if good
		if (frmSubmit == true) {
	 		document.frm.submit(); 
		}
	}
</script>


<script>
	function configDropDownOption(obj,list) {
		//used to rebuild options list with new values
		var el = document.getElementById(obj);
		var selIndex = el.selectedIndex;
		var selectedValue;
		//get current option value selected
		if (selIndex != -1) {
			for(i=el.length-1; i>=0; i--)
			{
			  if(el.options[i].selected)
			  {
				if(el.options[i].value){
					selectedValue = el.options[i].value; //current selection
				}
			  }
			}
		}
			
		//removed current selects in dropdown
		el.length = 0;
				
		//add new values to dropdown
		var i;		
		var temp = list.split("|"); //split list provided by popup_config_options.asp
		//add new options provided by user
		addOptionsItems(obj,"","-- Select --");
		for(i=0; i<temp.length; i++) {
			if (temp[i].length>1) {
				addOptionsItems(obj,temp[i],temp[i]);
			}
		}
		
		//check for existance of originally selected value		
		if(selectedValue){
			var bolMatch = false;
			i = 0;		
			for (i=0; i<el.options.length; i++) {
				if (el.options[i].text == selectedValue) {
					el.selectedIndex = i;
					bolMatch = true; //origianl still exists in list
				}
			}		
			//user removed original value, put back with "Missing:" at end of list
			if (bolMatch == false){
				addOptionsItems(obj,selectedValue,"Missing:"+selectedValue);
				el.selectedIndex = el.options.length-1;
			}
		}

	}		
</script>

<script language="javascript">	
	function navigateAway(url) {
		window.location = url;
	}		
</script>	


