<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache"
Response.AddHeader "pragma", "no-cache"
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'Database
dim mySQL, cn, rs, count

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	%>
	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>    
	<script language="javascript">
		//close window
		SimpleModal.close();				
		//open login dialog
		SimpleModal.open('../logon_dialog.asp', 243, 395, 'no');
    </script>    
	<%
	response.end	
end if
'*********************************************************

dim pageView : pageView = "datasets" 'Request.QueryString("pageView")

dim securityLevel
securityLevel = trim(lCase(Request.QueryString("security")))
if len(securityLevel) <= 0 then
	response.redirect "../error/popup_default.asp?errMsg=" & server.URLEncode("Invalid Security Level.")
end if

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Choose a Dataset</title>

	<link type="text/css" rel="stylesheet" href="../_css/default.css" />
    <link type="text/css" rel="stylesheet" href="../_css/text.css" />
    <link type="text/css" rel="stylesheet" href="../_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="../_css/buttons.css" />    
    <link type="text/css" rel="stylesheet" href="../_css/statusMessage.css" />    
    
	<!-- CCI Common Javascript functions -->
	<script type="text/javascript" src="../scripts/javascript/common.js"></script>

	<!-- HTML Table Filter Generator http://tablefilter.free.fr/ -->
    <link rel="stylesheet" type="text/css" href="../_tablefilter/filtergrid.css"/>
	<script type="text/javascript" src="../_tablefilter/tablefilter_all.js"></script>
	<script type="text/javascript" src="../_tablefilter/sortabletable.js"></script>
    <script type="text/javascript" src="../_tablefilter/tfAdapter.sortabletable.js"></script>        

    <script language="JavaScript" src="../_jquery/jquery-1.5.1.min.js"></script>
    <script language="JavaScript" src="../_jquery/jquery-ui-1.8.11.custom.min.js"></script>

	<!-- SIMPLE MODAL windows http://tomcoote.co.uk/?s=crumb -->
	<script type="text/javascript" src="../scripts/javascript/simpleModal.js"></script>

</head>

<body style="padding:20px;">

	<div style="text-align:left; margin-bottom:10px;">
   		<img src="../_images/icons/24/globe.png" title="Locations" width="24" height="24" align="absmiddle"> 
       	<span class="popupTitle">Datasets</span>
   	</div>                   

	<!-- START Information box -->
    <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
		<tr>
       		<td style="width:100%;">
       			<fieldset class="infoBox" style="padding:0px;">
          			<legend>Information</legend>
                		<div align="left">
	            			<img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; padding:5px;">Select a dataset by clicking within the 'Name' column.
                        </div>
        		</fieldset>                                    
           	</td>
 		</tr>
	</table>        
	<!-- STOP Information box -->

	<!-- START Locations table -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
    
        <%
		if lCase(customerID) = "mycm" then
			mySQL = "SELECT DatasetID, Name, 'Built-In' AS [Type], Type as [Style], Description " _
				  & "FROM   Dataset " _
				  & "WHERE  CustomerID='MYCM' and SecurityLevel=" & securityLevel & " " _
				  & "ORDER BY Name "
				  
		else
			mySQL = "SELECT DatasetID, Name, 'Built-In' AS [Type], Type as [Style], Description " _
				  & "FROM   Dataset " _
				  & "WHERE  CustomerID='MYCM' and SecurityLevel=" & securityLevel & " "
			if cLng(session(session("siteID") & "adminLoggedOn")) > 10 then
				mySQL = mySQL & " AND Type <> 'Crystal' "
			end if				  
			mySQL = mySQL & " UNION ALL " _
				  & "SELECT DatasetID, Name, 'User-Defined' AS [Type], Type as [Style], Description " _
				  & "FROM   Dataset " _
				  & "WHERE  CustomerID='" & customerID & "' and SecurityLevel=" & securityLevel & " "
			if cLng(session(session("siteID") & "adminLoggedOn")) > 10 then
				mySQL = mySQL & " AND Type <> 'Crystal' "
			end if	  
			mySQL = mySQL & "ORDER BY Name "		
			
		end if
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)			  
		%>
        
        <tr>
        	<td>
            
            
            
            
            
            
        <!-- START Configuration table -->           
        <table id="table_datasets" width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#969696;">
            <thead>
                <tr>
                    <th align="left" nowrap="nowrap">Name</th>
                    <th align="left" nowrap="nowrap">Style</th>       
                    <th align="left" nowrap="nowrap">Type</th>                           
              </tr>
            </thead>
            <tbody>

			<%        
			do until rs.eof			
				response.write("<tr>")
				
				response.write("	<td align=""left"">")												
				response.write("		<div style=""height:18px;""><a href=""#"" onClick=""addReport('" &  rs("datasetid") & "');""><strong>" & rs("name") & "</strong></a></div>")
				response.write("		<div>" & rs("description") & "</div>")
				response.write("	</td>")				
				
				response.write("	<td align=""left"" nowrap=""nowrap"">")				
				if lCase(rs("style")) = "register" then
					response.write("<img src=""../_images/icons/16/table.png"" title="""" width=""16"" height=""16"" align=""absmiddle"" style=""margin-right:5px;"">")					
				elseif lCase(rs("style")) = "calculated" then
					response.write("<img src=""../_images/icons/16/table_sum.png"" title="""" width=""16"" height=""16"" align=""absmiddle"" style=""margin-right:5px;"">")			
				elseif lCase(rs("style")) = "sql" then
					response.write("<img src=""../_images/icons/16/table_link.png"" title="""" width=""16"" height=""16"" align=""absmiddle"" style=""margin-right:5px;"">")							
				elseif lCase(rs("style")) = "crystal" then
					response.write("<img src=""../_images/icons/16/file_extension_rpt.png"" title="""" width=""16"" height=""16"" align=""absmiddle"" style=""margin-right:5px;"">")							
				end if												
				response.write(		rs("style") & "&nbsp;")
				response.write("	</td>")
				
				response.write("	<td align=""left"" nowrap=""nowrap"">" & rs("type") & "&nbsp;</td>")				
				
				response.write("</tr>")				
				rs.movenext
			loop					
			%>            
            
		</table>
        <!-- END Issues Details table -->
        
			<%
			dim tableProperty
			tableProperty = "sort: true, sort_config:{sort_types:['String','String','String']}, filters_row_index: 1, " _
						  & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
						  & "status_bar: true, col_0: ""input"", col_1: ""select"", col_2: ""select"", " _
						  & "col_width:[""75%"",null,null], paging: true, paging_length: 5, " _
						  & "enable_non_empty_option: true, or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, " _
						  & "highlight_keywords: true, " _
						  & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
						  & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"			
			%>
            <!-- STOP Reports table -->
        
            <!-- Fire table build -->
			<script language="javascript" type="text/javascript">   															
				//<![CDATA[	
				var tableProp = {<% =tableProperty %>};
				//initiate table setup
				var tf1 = setFilterGrid("table_datasets",tableProp);
				//]]>
			</script>
                    
            

        	</td>
        </tr>
    </table>
	<!-- STOP Locations table -->

	<!-- START Cancel Button -->
	<div style="padding-top:5px; margin-bottom:0px;">
    	<% if pageView = "issue" or pageView = "issue:00" then %>
			<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); topWin.parent.StartTheTimer(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>
		<% else %>
			<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); SimpleModal.close(); return false;" style="float:right;"><span class="cancel"><u>C</u>lose</span></a>
		<% end if %>        
    </div>
	<!-- STOP Cancel Button -->     
         
</body>
</html>

<%
'close database connection
call closeDB()
%>

<script language="javascript">	
    // dependent on SimpleModal.js
	// note: topWin.parent is defined in SimpleModal.js and replaces "window.opener"
    function addReport(dataset) {
        //push selected location to parent Issue window
        topWin.parent.addReport(dataset)       
        //close window
        SimpleModal.close();
    }
</script>
