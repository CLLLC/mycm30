<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement, Pro, Lite, Metrics
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="_includes/_INCconfig_.asp"-->
<!--#include file="_includes/_INCappFunctions_.asp"-->

<%
dim msg
%>

<html>

<head>
	<title>myCIM 3.0</TITLE>

	<link rel="shortcut icon" href="_images/favicon.ico" >
    	
	<link type="text/css" rel="stylesheet" href="_css/default.css" />
	<link type="text/css" rel="stylesheet" href="_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="_css/text.css" />
    <link type="text/css" rel="stylesheet" href="_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="_css/statusMessage.css" />
</head>

<body topmargin="0">

  <div style="margin-bottom:25px;">&nbsp;</div>

  <div id="content">
    
    <table width="375" cellpadding="0" cellspacing="0" border="0" align="center">
    
          <tr>
            <td style="padding:40px;">&nbsp;</td>
          </tr>        
    
      <tr>
        <td style="background-color: #3B5998;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="padding-left:10px;"><b><font size=3 color="#FFFFFF">Login</font></b></td>
                    <td align="right"><img src="_images/login2.jpg"></td>
                </tr>
            </table>
        </td>
      </tr>
      
      <tr>  
        <td style="border: 1px solid #3B5998; padding:10px; background:url(_images/login_bg.jpg) repeat;">


			<% if lCase(request.querystring("t")) = "locked" then %>
                <table width="375" border="0" align="center" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="padding-top:10px; padding-left:30px; padding-right:30px;">
                            <b><font size=3>Account Locked!</font></b><br><br>
                            Due to invalid login attempts, your account is temporarily locked.<br><br>Please contact 1-800-617-2111.
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top:10px; padding-left:30px; padding-right:30px;"><a href="logon.asp">Return to Login</a></td>
                    </tr>
                    <tr>
                        <td style="padding-top:10px; padding-left:30px; padding-right:30px;">&nbsp;</td>
                     </tr>
                </table>            
                
			<% elseif lCase(request.querystring("t")) = "inactive" then %>
                <table width="375" border="0" align="center" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="padding-top:10px; padding-left:30px; padding-right:30px;">
                            <b><font size=3>Account Expired!</font></b><br><br>
                            Your account is inactive or has expired.<br><br>Please contact 1-800-617-2111.
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top:10px; padding-left:30px; padding-right:30px;"><a href="logon.asp">Return to Login</a></td>
                    </tr>
                    <tr>
                        <td style="padding-top:10px; padding-left:30px; padding-right:30px;">&nbsp;</td>
                     </tr>
                </table>            
                            
            <% end if %>
            
        </td>
      </tr>
	  <tr>
	    <td style="padding-top:10px;">
        	<div style="float:left;">Copyright &copy; 2011</div>
            <div style="float:right;"><a href="http://www.complianceconcepts.com" target="_blank">Compliance Concepts, Inc.</a></div>        
        </td>
      </tr>
    </table>

  </div>

</body>
</html>

