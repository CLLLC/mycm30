<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>myCM 3.0 : OFFLINE</TITLE>

	<link rel="shortcut icon" href="../_images/favicon.ico" >

	<link type="text/css" rel="stylesheet" href="_css/default.css" />
	<link type="text/css" rel="stylesheet" href="_css/pagingControls.css" />
    <link type="text/css" rel="stylesheet" href="_css/text.css" />
    <link type="text/css" rel="stylesheet" href="_css/forms.css" />
    <link type="text/css" rel="stylesheet" href="_css/buttons.css" />    
    <link type="text/css" rel="stylesheet" href="_css/statusMessage.css" />
    <link type="text/css" rel="stylesheet" href="_css/tabs.css" />    
	<link type="text/css" rel="stylesheet" href="_css/icons.css" />    
    	
<style type="text/css">
<!--
body {
	margin-left: 50px;
	margin-top: 50px;
	margin-right: 50px;
	margin-bottom: 50px;
}
-->
</style></head>

<!--#include file="_includes/_INCappFunctions_.asp"-->

<body>

	<%
	response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
	response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
	call systemMessageBox("systemMessage","statusMessageERROR","MyComplianceManagement.com is down for system maintenance. Please check back soon.")
	response.write("   </td>")
	response.write("</tr></table>")				
	%>

</body>
</html>
