<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->

<%
'Database
dim mySQL, cn, rs, rs2

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("customerID")
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'Get action
dim action, recId
action = trim(lCase(Request.Form("action")))
if len(action) = 0 then
	action = trim(lCase(Request.QueryString("action")))
end if
if  action <> "edit" _
and action <> "add" _
and action <> "del" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if

'Get ID of location to work with
if action = "edit"  _
or action = "del" then
	recId = trim(Request.Form("GroupID"))
	if recId = "" then
		recId = trim(lCase(Request.QueryString("recid")))
	end if	
	if recId = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Group ID.")
	end if
end if

'make sure user can update this record
if programAdmin = "N" and cLng(session(session("siteID") & "adminLoggedOn")) > 3 then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if


'used to redirect user back to tabs they last were on
dim topTab, sideTab
topTab = trim(Request.Form("topTab"))
if len(topTab) = 0 then
	topTab = trim(Request.QueryString("top"))
end if
sideTab = trim(Request.Form("sideTab"))
if len(sideTab) = 0 then
	sideTab = trim(Request.QueryString("side"))
end if


'Group fields in DETAILS tab
dim optGroupID
dim optName
dim optEditDetailsSummary
dim optViewCaseNotes
dim optViewCaseUserFields
dim optEditCaseStatus
dim optEditOwner
dim optEditDateClosed
dim optEditSource
dim optEditCaseMgr
dim optEditOwnerNotes
dim optEditCaseResolution
dim optEditCaseResolutionApp
dim optEditCaseUserFields
dim optViewInvestigations
dim optViewAssignedInvOnly
dim optAddInvestigations
dim optViewInvOutcome
dim optViewDocuments
dim optNotes
dim optAddNewIssue
dim optAddNewFollowUp
dim optReadOnly

dim optType, arrType, expI
	
'flag right now as when this issue was modified
dim modifiedDate : modifiedDate = Now()


'Get Group Details
if action = "edit" or action = "add" then

	'optGroupID = trim(Request.Form("locName"))
	optName = trim(Request.Form("optName"))
	if optName = "" or isNull(optName) then
		'for AJax display
		if action = "edit" then
			response.write("Error: Invalid Group Name.")
			response.end 
		'all others, redirect
		else
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Group Name.")		
		end if
	end if

	optAddNewIssue = trim(Request.Form("optAddNewIssue"))
	if optAddNewIssue = "" or isNull(optAddNewIssue) then optAddNewIssue = "Y"

	optAddNewFollowUp = trim(Request.Form("optAddNewFollowUp"))
	if optAddNewFollowUp = "" or isNull(optAddNewFollowUp) then optAddNewFollowUp = "Y"
		
	optEditDetailsSummary = trim(Request.Form("optEditDetailsSummary"))
	if optEditDetailsSummary = "" or isNull(optEditDetailsSummary) then optEditDetailsSummary = "Y"

	optViewCaseNotes = trim(Request.Form("optViewCaseNotes"))
	if optViewCaseNotes = "" or isNull(optViewCaseNotes) then optViewCaseNotes = "N"

	optEditCaseStatus = trim(Request.Form("optEditCaseStatus"))
	if optEditCaseStatus = "" or isNull(optEditCaseStatus) then optEditCaseStatus = "N"

	optEditOwner = trim(Request.Form("optEditOwner"))
	if optEditOwner = "" or isNull(optEditOwner) then optEditOwner = "N"

	optEditDateClosed = trim(Request.Form("optEditDateClosed"))
	if optEditDateClosed = "" or isNull(optEditDateClosed) then optEditDateClosed = "N"
	
	optEditSource = trim(Request.Form("optEditSource"))
	if optEditSource = "" or isNull(optEditSource) then optEditSource = "N"
				
	optEditCaseMgr = trim(Request.Form("optEditCaseMgr"))
	if optEditCaseMgr = "" or isNull(optEditCaseMgr) then optEditCaseMgr = "N"

	optEditOwnerNotes = trim(Request.Form("optEditOwnerNotes"))
	if optEditOwnerNotes = "" or isNull(optEditOwnerNotes) then optEditOwnerNotes = "N"
		
	optEditCaseResolution = trim(Request.Form("optEditCaseResolution"))
	if optEditCaseResolution = "" or isNull(optEditCaseResolution) then optEditCaseResolution = "N"
		
	optEditCaseResolutionApp = trim(Request.Form("optEditCaseResolutionApp"))
	if optEditCaseResolutionApp = "" or isNull(optEditCaseResolutionApp) or optEditCaseResolution = "N" then optEditCaseResolutionApp = "N"
		
	optViewCaseUserFields = trim(Request.Form("optViewCaseUserFields"))
	if optViewCaseUserFields = "" or isNull(optViewCaseUserFields) then optViewCaseUserFields = "N"	

	optEditCaseUserFields = trim(Request.Form("optEditCaseUserFields"))
	if optEditCaseUserFields = "" or isNull(optEditCaseUserFields) or optViewCaseUserFields = "N" then optEditCaseUserFields = "N"

	optViewInvestigations = trim(Request.Form("optViewInvestigations"))
	if optViewInvestigations = "" or isNull(optViewInvestigations) then optViewInvestigations = "N"

	optViewAssignedInvOnly = trim(Request.Form("optViewAssignedInvOnly"))
	if optViewAssignedInvOnly = "" or isNull(optViewAssignedInvOnly) or optViewInvestigations = "N" then optViewAssignedInvOnly = "Y"

	optViewInvOutcome = trim(Request.Form("optViewInvOutcome"))
	if optViewInvOutcome = "" or isNull(optViewInvOutcome) then optViewInvOutcome = "Y"

	optAddInvestigations = trim(Request.Form("optAddInvestigations"))
	if optAddInvestigations = "" or isNull(optAddInvestigations) then optAddInvestigations = "N"

	optViewDocuments = trim(Request.Form("optViewDocuments"))
	if optViewDocuments = "" or isNull(optViewDocuments) then optViewDocuments = "N"

	optReadOnly = trim(Request.Form("optReadOnly"))
	if optReadOnly = "" or optReadOnly=" " then optReadOnly = null
			
	optNotes = trim(Request.Form("optNotes"))

	optType = trim(Request.Form("list_checkBox"))
	if (len(optType) = 0 or optType = "" or isNull(optType)) then
		'for AJax display
		if action = "edit" then
			response.write("Error: Invalid Issue Type Selection. At least one issue type must be selected.")
			response.end 
		'all others, redirect
		else
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue Type Selection. At least one issue type must be selected.")		
		end if		
	end if
	arrType = split(optType,",")

end if


'Delete/Add Members
if action = "edit" then

	'***************************************
	'Add any Users/Logins selected to be added
	dim listUserAdded
	dim arrAddedUser
	dim addedU

	'discover added users
	listUserAdded = trim(Request.Form("listUserAdded"))
	if len(listUserAdded) > 0 then

		'-----------------------------------		
		'add newly assigned users to issue
		'-----------------------------------		
		arrAddedUser = split(listUserAdded,",")
		'move through all users selected
		for addedU = LBound(arrAddedUser) to UBound(arrAddedUser)
			if len(arrAddedUser(addedU)) > 0 then
				'make sure does NOT already exist so it's not added twice
				mySQL = "SELECT Count(GroupID) AS GroupCount FROM Logins_Groups " _
					  & "WHERE GroupID = " & recId & " and LogID = " & arrAddedUser(addedU) & " "
				'''set rs = openRSexecute(mySQL)
				'for AJax display execute update
				set rs = openRSexecuteAjax(mySQL)				
				'does NOT exist, go ahead and add
				if rs("GroupCount") <= 0 then									
					'add record to table
					mySQL = "INSERT INTO Logins_Groups (" _
						  & "LogID,GroupID,ModifiedBy,ModifiedDate" _
						  & ") VALUES (" _
						  & " " & arrAddedUser(addedU) & "," & recId & "," & sLogid & ",'" & modifiedDate & "' " _
						  & ")"
					'''set rs = openRSexecute(mySQL)					
					'for AJax display execute update
					set rs = openRSexecuteAjax(mySQL)					
				end if			
			end if
		next								
		
	end if


	'***************************************
	'Remove any Users selected	
	dim listUserRemoved
	dim arrayRemovedUser
	dim removedU

	'Add new users to Case History
	listUserRemoved = trim(Request.Form("listUserRemoved"))
	if len(listUserRemoved) > 0 then
		arrayRemovedUser = split(listUserRemoved,",")		
		'move through all users.
		for removedU = LBound(arrayRemovedUser) to UBound(arrayRemovedUser)
			if len(arrayRemovedUser(removedU)) > 0 then
				'delete record from table
				mySQL = "DELETE FROM Logins_Groups " _
					  & "WHERE GroupID = " & recId & " AND LogID = " & arrayRemovedUser(removedU)
				'''set rs = openRSexecute(mySQL)
				'for AJax display execute update
				set rs = openRSexecuteAjax(mySQL)				
			end if
		next	
	end if


	'***************************************
	'Update selected issue types
	mySQL = "DELETE FROM Groups_IssueType " _
		  & "WHERE GroupID=" & recId & " "
	set rs = openRSexecute(mySQL)

	'add all issue types selected by user
	for expI = 0 to UBound(arrType)
		mySQL = "INSERT INTO Groups_IssueType (" _
			  & "GroupID,CustIssueTypeID,ModifiedBy,ModifiedDate" _
			  & ") VALUES (" _
			  & "'" & recId & "'," & arrType(expI) & "," & sLogid & ",'" & modifiedDate & "' " _
			  & ")"
		'''set rs = openRSexecute(mySQL)			
		'for AJax display execute update
		set rs = openRSexecuteAjax(mySQL)		
	next	

end if


'ADD
if action = "add" then

	dim fieldsSQL, valueSQL

	'Add Issue with Callback
	fieldsSQL = "INSERT INTO Groups (" _
		  & "CustomerID,Name,AddNewIssue,AddNewFollowUp,editDetailsSummary,viewCaseNotes,editCaseStatus,editOwner,editDateClosed,editSource,viewCaseUserFields,editCaseMgr,EditOwnerNotes,editCaseResolution,editCaseResolutionApp," _
		  & "editCaseUserFields,AddInvestigations,ViewInvestigations,ViewAssignedInvOnly,ViewInvOutcome,ViewDocuments,ReadOnly," _
		  & "Notes,ModifiedBy,ModifiedDate"
	fieldsSQL = fieldsSQL & ") "
		  
	valueSQL = " VALUES (" _
		  & "'"    	& uCase(customerID)				& "'," _
		  & "'"    	& replace(optName,"'","''")		& "'," _
		  & "'"    	& optAddNewIssue				& "'," _
  		  & "'"    	& optAddNewFollowUP				& "'," _				  
		  & "'"    	& optEditDetailsSummary			& "'," _
		  & "'"    	& optViewCaseNotes				& "'," _
		  & "'"    	& optEditCaseStatus				& "'," _
		  & "'"    	& optEditOwner					& "'," _
		  & "'"    	& optEditDateClosed				& "'," _
		  & "'"    	& optEditSource					& "'," _	  
		  & "'"    	& optViewCaseUserFields			& "'," _
		  & "'"    	& optEditCaseMgr				& "'," _
		  & "'"    	& optEditOwnerNotes				& "'," _		  		  
		  & "'"    	& optEditCaseResolution			& "'," _
		  & "'"    	& optEditCaseResolutionApp		& "'," _
		  & "'"    	& optEditCaseUserFields			& "'," _
		  & "'"    	& optAddInvestigations			& "'," _		  
		  & "'"    	& optViewInvestigations			& "'," _
		  & "'"    	& optViewAssignedInvOnly		& "'," _
		  & "'"    	& optViewInvOutcome				& "'," _
		  & "'"    	& optViewDocuments				& "'," _
		  & "'"    	& optReadOnly					& "'," _		  
		  & "'"    	& replace(optNotes,"'","''")	& "'," _
		  & " "    	& sLogid						& ", " _
		  & "'"    	& modifiedDate					& "' "
	valueSQL = valueSQL & ") "

	'set identity to pull back newly added record new record id is returned to newID
	mySQL = "SET NOCOUNT ON; " & fieldsSQL & valueSQL & " SELECT SCOPE_IDENTITY() AS newID;"
	set rs = openRSexecute(mySQL)		
	recId = rs("newID")		  				

	'add all issue types selected by user
	for expI = 0 to UBound(arrType)
		mySQL = "INSERT INTO Groups_IssueType (" _
			  & "GroupID,CustIssueTypeID,ModifiedBy,ModifiedDate" _
			  & ") VALUES (" _
			  & "'" & recId & "'," & arrType(expI) & "," & sLogid & ",'" & modifiedDate & "' " _
			  & ")"
		set rs = openRSexecute(mySQL)			
	next	
	
	session(session("siteID") & "okMsg") = "Group was Added. Check the <strong>[Settings]</strong> tab to confirm security settings."
	response.redirect "group_edit.asp?action=edit&recID=" & recId & "&cid=" & customerID & "&top=" & topTab & "&side=" & sideTab
	
end if


'EDIT
if action = "edit" then
		
	'Update Record
	mySQL = "UPDATE Groups SET " _	
		  & "Name='"   					& replace(optName,"'","''")		& "'," _		  
		  & "AddNewIssue='"   			& optAddNewIssue				& "'," _
		  & "AddNewFollowUp='"   		& optAddNewFollowUp				& "'," _		  
		  & "editDetailsSummary='"   	& optEditDetailsSummary			& "'," _
		  & "viewCaseNotes='"    		& optViewCaseNotes				& "'," _
		  & "editCaseStatus='"    		& optEditCaseStatus				& "'," _
  		  & "editOwner='"    			& optEditOwner					& "'," _
		  & "editDateClosed='"    		& optEditDateClosed				& "'," _
		  & "editSource='"    			& optEditSource					& "'," _
		  & "viewCaseUserFields='"    	& optViewCaseUserFields			& "'," _
		  & "editCaseMgr='" 			& optEditCaseMgr				& "'," _
		  & "editOwnerNotes='" 			& optEditOwnerNotes				& "'," _		  
  		  & "editCaseResolution='" 		& optEditCaseResolution			& "'," _
		  & "editCaseResolutionApp='"   & optEditCaseResolutionApp		& "'," _		  
		  & "editCaseUserFields='"     	& optEditCaseUserFields			& "'," _
		  & "addInvestigations='"     	& optAddInvestigations			& "'," _		  		  		  
		  & "viewInvestigations='"     	& optViewInvestigations			& "'," _		  
		  & "viewAssignedInvOnly='"     & optViewAssignedInvOnly		& "'," _		  
		  & "viewInvOutcome='"	     	& optViewInvOutcome				& "'," _		  
		  & "viewDocuments='"	     	& optViewDocuments				& "'," _		  		  		  
		  & "ReadOnly='"	     		& optReadOnly					& "'," _		  		  		  		  
		  & "Notes='"     				& replace(optNotes,"'","''")	& "'," _
		  & "ModifiedBy="  				& sLogid						& ", " _
		  & "ModifiedDate='"			& modifiedDate					& "' "
		  
	'finalize update query
	mySQL = mySQL & "WHERE GroupID = '" & recId & "' "

	'for AJax display execute update
	set rs = openRSexecuteAjax(mySQL)
	response.write("Group has been saved successfully.")

	'close database connection
	call closeDB()

end if


'DELETE
if action = "del" then

	'Delete records from optionsGroupsXref
	mySQL = "DELETE FROM Groups " _
	      & "WHERE GroupID = " &  recId & " "
	set rs = openRSexecute(mySQL)

	'delete existing records from Category_IssueType
	mySQL = "DELETE FROM Groups_IssueType " _
		  & "WHERE GroupID=" & recId & " "
	set rs = openRSexecute(mySQL)

	'delete existing records from Category_IssueType
	mySQL = "DELETE FROM Logins_Groups " _
		  & "WHERE GroupID=" & recId & " "
	set rs = openRSexecute(mySQL)	
	
	session(session("siteID") & "okMsg") = "Group was deleted and any members removed."
	response.redirect "groups.asp?cid=" & customerID
	
end if


'just in case we ever get this far...and NOT...called by action=edit
if action <> "edit" then
	call closeDB()
	Response.Redirect "groups.asp"
end if

%>
