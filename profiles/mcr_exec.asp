<%@ Language=VBScript CodePage = 65001%>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->

<%
'Database
dim mySQL, cn, rs, rs2

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'fields in DETAILS tab
dim cusName
dim cusDisplayAs
dim cusAddress
dim cusCity
dim cusState
dim cusZip
dim cusCountry
dim cusIndustry
dim cusEmployeeCount
dim cusContractDate
dim cusStartDate
dim cusCanceled
dim cusPhone_Work
dim cusPhone_Fax
dim cusPhone_Customer
dim cusPhone_Other
dim cusRetention
dim optTypes
dim cusQuestion
dim cusAppShowUserFields
dim cusAppAssignFUPLogid
dim cusAppAssignFUPCategory
dim cusNotes

dim mcrSource
dim mcrLogoName
dim mcrLogoBG
dim mcrLogoBGColor
dim mcrSubMenuBGColor
dim mcrSubMenuFontColor

dim mcrInitialDisclaimer
dim mcrFollowUpDisclaimer
dim mcrWhatsNext
dim mcrFaq
dim mcrCommTool
dim mcrCallerType
dim mcrActive
dim mcrLanguageCD

'flag right now as when this issue was modified
dim modifiedDate : modifiedDate = Now()

dim action, recId

'Get action --  this must come before "get customerID" below
action = trim(lCase(Request.Form("action")))
if len(action) = 0 then
	action = trim(lCase(Request.QueryString("action")))
end if
if  action <> "edit" _
and action <> "add" _
and action <> "del" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if

'Get Customer ID
if action = "edit" or action = "add" or action = "del" then
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'Get ID of location to work with
if action = "edit"  _
or action = "del" then
	recId = trim(Request.Form("mcrID"))
	if recId = "" then
		recId = trim(lCase(Request.QueryString("recid")))
	end if	
	if recId = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid MCR Config ID.")
	end if
end if


'make sure user can update this record
'if programAdmin = "N" and cLng(session(session("siteID") & "adminLoggedOn")) > 2 then
if cLng(session(session("siteID") & "adminLoggedOn")) > 2 then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if


'used to redirect user back to tabs they last were on
dim topTab, sideTab
topTab = trim(Request.Form("topTab"))
if len(topTab) = 0 then
	topTab = trim(Request.QueryString("top"))
end if


'Get Location Details
if action = "edit" or action = "add" then

	mcrSource = trim(Request.Form("mcrSource"))

	mcrLogoName = trim(Request.Form("mcrLogoName"))
	mcrLogoBG = trim(Request.Form("mcrLogoBG"))
	
	mcrLogoBGColor = trim(Request.Form("mcrLogoBGColor"))
	if left(mcrLogoBGColor,1) <> "#" then mcrLogoBGColor = "#" & mcrLogoBGColor
	
	mcrSubMenuBGColor = trim(Request.Form("mcrSubMenuBGColor"))
	if left(mcrSubMenuBGColor,1) <> "#" then mcrSubMenuBGColor = "#" & mcrSubMenuBGColor
	
	mcrSubMenuFontColor = trim(Request.Form("mcrSubMenuFontColor"))
	if left(mcrSubMenuFontColor,1) <> "#" then mcrSubMenuFontColor = "#" & mcrSubMenuFontColor
	
	mcrInitialDisclaimer = trim(Request.Form("mcrInitialDisclaimer"))
	mcrFollowUpDisclaimer = trim(Request.Form("mcrFollowUpDisclaimer"))
	mcrWhatsNext = trim(Request.Form("mcrWhatsNext"))
	mcrFaq = trim(Request.Form("mcrFaq"))
	mcrCommTool = trim(Request.Form("mcrCommTool"))
	mcrCallerType = trim(Request.Form("mcrCallerType"))
	mcrActive = trim(Request.Form("mcrActive"))
	mcrLanguageCD = trim(Request.Form("mcrLanguageCD"))						
	
end if

'ADD
if action = "add" then

	dim fieldsSQL, valueSQL

	'Add Issue with Callback
	fieldsSQL = "INSERT INTO MCR_Config (" _
		  & "CustomerID,MCRLogoName,MCRLogo,MCRLogoBG,MCRLogoBGImg,MCRLogoBGColor,MCRSubMenuBGColor,MCRSubMenuFontColor,MCRInitDisclaimer,MCRFollowUpDisclaimer, " _
		  & "MCRWhatsNext,MCRFaq,MCR_CommTool,MCR_CallerType,ActiveYN,LanguageCD,ModifiedBy,ModifiedDate"

	'check for empty dates	      
	'if len(locDirectiveID) > 0 then fieldsSQL = fieldsSQL & ",DirectiveID"		  
	fieldsSQL = fieldsSQL & ") "
		  
	valueSQL = " VALUES (" _
		  & "'"		& uCase(customerID)							& "'," _	
		  & "'"		& mcrLogoName								& "'," _	
		  &			"null"										& ", " _
		  & "'"    	& mcrLogoBG									& "'," _
		  &			"null"										& ", " _
		  & "'"    	& mcrLogoBGColor							& "'," _
		  & "'"    	& mcrSubMenuBGColor							& "'," _
		  & "'"    	& mcrSubMenuFontColor						& "'," _
		  & "N'"   	& replace(mcrInitialDisclaimer,"'","''")	& "'," _		  		  
		  & "N'"  	& replace(mcrFollowUpDisclaimer,"'","''")	& "'," _		  		  
		  & "N'"     & replace(mcrWhatsNext,"'","''")			& "'," _		  		  
		  & "N'"     & replace(mcrFaq,"'","''")					& "'," _		  		  
		  & "N'"     & replace(mcrCommTool,"'","''")				& "'," _		  		  
		  & "N'"     & replace(mcrCallerType,"'","''")			& "'," _		  		  		  		  
		  & "'"    	& mcrActive									& "'," _
		  & "'" 	& mcrLanguageCD								& "'," _		  		  
		  & ""  	& sLogid									& ", " _
		  & "'"		& modifiedDate								& "' "

	'check for empty dates
	'if len(locDirectiveID) > 0 then valueSQL = valueSQL & "," & locDirectiveID & " "		  
	valueSQL = valueSQL & ") "

	'response.redirect "../error/default.asp?errMsg=" & server.URLEncode(fieldsSQL & valueSQL)
	
	'set identity to pull back newly added record new record id is returned to newID
	mySQL = "SET NOCOUNT ON; " & fieldsSQL & valueSQL & " SELECT SCOPE_IDENTITY() AS newID;"
	set rs = openRSexecute(mySQL)		
	recId = rs("newID")		  				

	'import logo image into DB
	if len(mcrLogoName) > 0 then
		mySQL = "UPDATE MCR_Config " _
			  & "	SET MCRLogo = (SELECT * FROM Openrowset (Bulk '" & mcrDir & "\" & customerID & "\" & mcrLogoName & "', Single_Blob) as img) " _
			  & "	WHERE CustomerID = '" & customerID & "' AND MCRID=" & recId & " "
		set rs = openRSexecute(mySQL)
	end if

	'import background image into DB
	if len(mcrLogoBG) > 0 then
		mySQL = "UPDATE MCR_Config " _
			  & "	SET MCRLogoBGImg = (SELECT * FROM Openrowset (Bulk '" & mcrDir & "\" & customerID & "\" & mcrLogoBG & "', Single_Blob) as img) " _
			  & "	WHERE CustomerID = '" & customerID & "' AND MCRID=" & recId & " "
		set rs = openRSexecute(mySQL)
	end if
	
	session(session("siteID") & "okMsg") = "MCR config was Added."
	response.redirect "mcr_edit.asp?action=edit&recID=" & recId & "&cid=" & customerID & "&top=" & topTab & "&side=" & sideTab
	
end if

'EDIT
if action = "edit" then

	'Update Record
	mySQL = "UPDATE MCR_Config SET " _			  
		  & "MCRLogoName='"				& mcrLogoName								& "'," _	
		  & "MCRLogo="					& "null"									& ", " _
		  & "MCRLogoBG='"    			& mcrLogoBG									& "'," _
		  & "MCRLogoBGImg="				& "null"									& ", " _
		  & "MCRLogoBGColor='"    		& mcrLogoBGColor							& "'," _
		  & "MCRSubMenuBGColor='"    	& mcrSubMenuBGColor							& "'," _
		  & "MCRSubMenuFontColor='"    	& mcrSubMenuFontColor						& "'," _
		  & "MCRInitDisclaimer=N'"   	& replace(mcrInitialDisclaimer,"'","''")	& "'," _		  		  
		  & "MCRFollowUpDisclaimer=N'"  	& replace(mcrFollowUpDisclaimer,"'","''")	& "'," _		  		  
		  & "MCRWhatsNext=N'"     		& replace(mcrWhatsNext,"'","''")			& "'," _		  		  
		  & "MCRFaq=N'"     				& replace(mcrFaq,"'","''")					& "'," _		  		  
		  & "MCR_CommTool=N'"				& replace(mcrCommTool,"'","''")				& "'," _		  		  
		  & "MCR_CallerType=N'"    		& replace(mcrCallerType,"'","''")			& "'," _		  		  
		  & "ActiveYN='"    			& mcrActive									& "'," _
		  & "LanguageCD='" 	   			& mcrLanguageCD								& "'," _		  		  
		  & "ModifiedBy="  				& sLogid									& ", " _
		  & "ModifiedDate='"			& modifiedDate								& "' "
		  
	'finalize update query
	mySQL = mySQL & "WHERE CustomerID = '" & customerID & "' AND MCRID=" & recId & " "

	'update issue
	set rs = openRSexecute(mySQL)

	'import logo image into DB
	if len(mcrLogoName) > 0 then
		mySQL = "UPDATE MCR_Config " _
			  & "	SET MCRLogo = (SELECT * FROM Openrowset (Bulk '" & mcrDir & "\" & customerID & "\" & mcrLogoName & "', Single_Blob) as img) " _
			  & "	WHERE CustomerID = '" & customerID & "' AND MCRID=" & recId & " "
		set rs = openRSexecute(mySQL)
	end if

	'import background image into DB
	if len(mcrLogoBG) > 0 then
		mySQL = "UPDATE MCR_Config " _
			  & "	SET MCRLogoBGImg = (SELECT * FROM Openrowset (Bulk '" & mcrDir & "\" & customerID & "\" & mcrLogoBG & "', Single_Blob) as img) " _
			  & "	WHERE CustomerID = '" & customerID & "' AND MCRID=" & recId & " "
		set rs = openRSexecute(mySQL)
	end if
		
	'response.redirect "../error/default.asp?errMsg=" & server.URLEncode(mySQL)

	session(session("siteID") & "okMsg") = "MCR settings have been saved successfully."
	response.redirect "mcr_edit.asp?action=edit&recID=" & recId & "&cid=" & customerID & "&top=" & topTab & "&side=" & sideTab

end if

'DELETE
if action = "del" then

	'Delete records from optionsGroupsXref
	mySQL = "DELETE FROM MCR_Config " _
	      & "WHERE MCRID = " &  recId & " "
	set rs = openRSexecute(mySQL)		
	
	session(session("siteID") & "okMsg") = "MCR config was Deleted."
	response.redirect "mcr.asp?cid=" & customerID
	
end if

'Just in case we ever get this far...
call closeDB()
Response.Redirect "profiles.asp"

%>
