<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = false
const showDHTMLMenu = true
const helpID = 6
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "locations"
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and programAdmin = "N" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if

dim I
dim item
dim pageSize
dim totalPages
dim showArr
dim sortField
dim sortOrder
dim curPage

dim action
dim tableProperty

dim showCRSID

dim tempHierarchy


'check size of location list...might need to redirect
dim mySQLCount
dim rowCount
mySQL = "SELECT Location.LocationID FROM Location " _
	  & "	WHERE CustomerID = '" & customerID & "' "
mySQLCount = "SELECT Count([LocationID]) AS ReturnCount " & mid(mySQL,inStr(1,mySQL,"FROM"),len(mySQL))
set rs = openRSopen(mySQLCount,0,adOpenStatic,adLockReadOnly,adCmdText,0)		
rowCount = rs("ReturnCount")
if rowCount > 2000 then
	response.redirect "location_large.asp?cid=" & customerID
end if

%>

<% dim pageTitle : pageTitle = "Locations" %>
<!--#include file="../_includes/_INCheaderLite_.asp"-->

<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
        
        <!-- START Left sided area -->
		<td width="190" align="right" valign="top">

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided tree menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">                

                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->        
                    <% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>      
	                    <script>
							stExpandSubTree('myCMtree',1,0)
       		            </script>
            		<% else %>
	                    <script>
							stCollapseSubTree('myCMtree',1,0);
							stCollapseSubTree('myCMtree',2,0);							
       		            </script>
                    <% end if %>                          
                    <!-- STOP side menu include -->
                    
                </td>
              </tr>
              <!-- END Left sided tree menu -->
        
            </table>
        
        </td>
        <!-- STOP Left sided area -->
            
	    <!-- START Main Middle Section -->
		<td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">       

            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs());
       	        </script>
            </div>
            
            <div style="text-align:left; margin-bottom:10px;">				
                <div style="float:left; padding-right:2px; margin-right:2px;"><img src="../_images/icons/32/globe.png" title="" width="32" height="32" align="absmiddle" style="vertical-align:middle;"></div>
                <div>
                	<span class="pageTitle">Locations</span>
                	<br/>:: <a href="../profiles/profiles_menu.asp?cid=<% =customerID %>"><% response.write( getCustomerName(customerID) ) %></a>
                </div>
            </div>                   
                                          
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
                        <legend>Information</legend>
                        <div align="left">
	                        <img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; vertical-align:middle;">Below is a list of locations. Click 'Name' to view/modify record.
							<% response.write( getInlineHelp("getPageURL", 0) ) %>
                        </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             

                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">
                        <div class="infoButtons">
                            <a href="location_edit.asp?action=add&cid=<% =customerid %>" ><img src="../_images/icons/24/globe_add.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                            <a href="location_edit.asp?action=add&cid=<% =customerid %>" >Add New</a>
                        </div>
                        
                        <% 
						'CAN TURN ON FOR CUSTOMER...after used by CCI for awhilte
						if cLng(session(session("siteID") & "adminLoggedOn")) <= 2 or sCustomerID = "SSC" or sCustomerID = "GHC" or sCustomerID = "GDL" or sCustomerID = "GER" then %>
                            <div class="infoButtons">
                                <a href="#" onClick="javascript:parent.location='location_import.asp?cid=<% =customerid %>'; return false;"><img src="../_images/icons/24/table_import.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                <a href="#" onClick="javascript:parent.location='location_import.asp?cid=<% =customerid %>'; return false;">Import</a>
                            </div>                                                                   
                        <% end if %>                        
                        
                        <div class="infoButtons">
                            <a href="#" onClick="javascript:parent.location='../reports/reports_export.asp?pageview=profile&view=vwProfileLocation&cid=<% =customerid %>&action=csv'; return false;"><img src="../_images/icons/24/file_extension_xls.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                          	<a href="#" onClick="javascript:parent.location='../reports/reports_export.asp?pageview=profile&view=vwProfileLocation&cid=<% =customerid %>&action=csv'; return false;">Export (.csv)</a>
                        </div>
                    </td>
                    <!-- STOP Information buttons -->
                                                                                
                </tr>
            </table>        
            <!-- STOP information box -->
    

    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			'clear system messages
			call clearSessionMessages()
            %>            
    		<!-- STOP message notification box -->


            <!-- START Reports table -->
            <%
			'open location table...
			mySQL = "SELECT Location.LocationID, Location.Name, Location.Address, Location.Country, " _
				  & "		Location.City, Location.State, Location.PostalCode, " _
				  & "		Location.Level_1, Location.Level_2, Location.Level_3, Location.Level_4, " _
				  & "		Location.Level_5, Location.Level_6, Location.Level_7, Location.Level_8 " _
				  & "	FROM Location " _
				  & "	WHERE CustomerID = '" & customerID & "' " _
				  & "	ORDER BY Location.Name "			 				  
	        set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
			
			if not rs.eof then
			%>
			
                <!-- START Location table -->
                <table id="customerTable" width="100%" style="background-color:#969696;" cellpadding=0 cellspacing=0>
                            
                    <thead>
                        <tr>
                            <th align="left" nowrap="nowrap">&nbsp;</th>
                            <th align="left" nowrap="nowrap">Name</th>
                            <th align="left" nowrap="nowrap">Address</th>
                            <th align="left" nowrap="nowrap">City/State</th>
                            <th align="left" nowrap="nowrap">Country</th>                            
                            <th align="left" nowrap="nowrap">Hierarchy</th>
                        </tr>
                    </thead>
                    <tbody>
    
                    <%
                    do until rs.eof

						tempHierarchy = rs("level_1") & "*|*" & rs("level_2") & "*|*" & rs("level_3") & "*|*" & rs("level_4") & "*|*" & rs("level_5") & "*|*" & rs("level_6") & "*|*" & rs("level_7") & "*|*" & rs("level_8")
						tempHierarchy = reformatHierarchy(tempHierarchy,"string")
						tempHierarchy = replace(tempHierarchy,"'","\'")	'replaces all single quotes with proper Javascript \'
                        
						'START current row
                        response.write("<tr>")									
                        
						'ICONS
                        response.Write("<td width=""39"" nowrap=""nowrap"" align=""center"">")
                        response.Write("<a href=""#"" onClick=""confirmPrompt('Confirm Delete','This action can not be undone.<br><br>Are you sure you want to delete this location?','redirect','location_exec.asp?recID=" & rs("locationid") & "&action=del&cid=" & customerid & "'); return false;"" ><img src=""../_images/icons/16/cancel.png"" title=""Delete Location"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:7px; vertical-align:middle;"" /></a>")
						response.Write("<a href=""location_edit.asp?action=edit&recid=" & rs("locationid") & "&cid=" & customerid & """ ><img src=""../_images/icons/16/page_blue_edit.png"" title=""Open Location"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; vertical-align:middle;""></a>")
                        response.Write("</td>")											
                            
                        'NAME link
                        response.Write("<td align=""left"" style=""cursor:pointer;"" onclick=""window.location='location_edit.asp?action=edit&recid=" & rs("locationid") & "&cid=" & customerid & "' ; return false;"" >")
                        response.write("<a style=""color:#333333;"">" & rs("name") & "</a>")
                        response.Write("</td>")
                        
						response.Write("<td align=""left"">" & rs("address") & "</td>")
						
						response.Write("<td align=""left"">")						
							if len(rs("city")) > 0 then response.Write(rs("city") & ", ")
							response.Write( rs("state") & " " & rs("postalcode") )
						response.Write("</td>")

						response.Write("<td align=""left"">" & rs("country") & "</td>")
												
						response.Write("<td align=""left"">" & tempHierarchy & "</td>")
						
                        'CLOSE current row						
                        response.write("</tr>")
                        
                        rs.movenext
                    loop
                    %>					
    
                    </tbody>
                </table>
    
                <%
                tableProperty = "sort: true, sort_config:{sort_types:['String','String','String','String','String','String']}, filters_row_index: 1, " _
                              & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
                              & "status_bar: true, col_0: """", col_1: ""input"", col_2: ""input"", col_3: ""input"", col_4: ""input"", col_5: ""input"", " _
                              & "col_width:[null,""35%"",null,null,null,null], paging: true, paging_length: 25, " _
                              & "enable_non_empty_option: true, or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, " _
                              & "highlight_keywords: true, " _
							  & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
                              & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"						  
                %>
                <!-- STOP Location table -->
    
                <!-- Fire table build -->
                <script language="javascript" type="text/javascript">
                    //<![CDATA[
                    var tableProp = {<% =tableProperty %>};
                    //initiate table setup
                    var tf1 = setFilterGrid("customerTable",tableProp);
                    //]]>
                </script>
    
                <!-- Shadow table -->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="5px" style="background:#D0D0D0;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
                  </tr>
                </table>
        
        	<% else %>
	            <!-- show INFO that NO locations exist -->
                <table width="100%" cellpadding="0" cellspacing="0">
           			<tr>
         				<td align="left" class="clearFormat" style="padding-right:2px;">
                			<% call systemMessageBox("locationID","statusMessageINFO","No locations found. Click ""Add Location"" above.") %>
                    	</td>
             		</tr>
        		</table>            
            <% end if %>
            
        </td>
    	<!-- STOP Main Middle Section -->

    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
  	</tr>
</table>
<!-- STOP Page -->

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<script language="javascript">	
	function pageReload(cid) {  	
    	window.location = '../profiles/location.asp?cid='+cid;
    }
	//used to navigate elsewhere
	function navigateAway(url) {
		window.location = url;
	}		
</script>	

