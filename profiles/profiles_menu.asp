<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = false
const showDHTMLMenu = true
const helpID = 6
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "customers"
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
'customerID = trim(lCase(Request.QueryString("cid")))
'if customerID = "" then
'	customerID = request.form("customerID")
'	if customerID = "" then
'		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
'	end if
'end if

'---------------------------------------------------------
'*******************  DNIS LOOKUP  ***********************
'---------------------------------------------------------
dim dNis 'phone number passed by Vertical
'for CCI Staff to start new issue from Vertical
if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
	'get/set DNIS digit from vertical
	dNis = trim(lCase(Request.QueryString("dnis")))
	if len(dNis) = 0 then
		dNis = trim(Request.Form("dnis"))
	end if	
end if

'get/set customerID -- either from string or DNIS
customerID = trim(lCase(Request.QueryString("cid")))
if len(customerID) = 0 then
	customerID = trim(Request.Form("customerID"))
end if	
'no customerID, but there is a DNIS digit
if customerID = "" and len(dNis) > 0 and cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
	mySQL = "SELECT Customer.CustomerID " _
		  & "	FROM Customer INNER JOIN Telcom ON Customer.CustomerID = Telcom.CustomerID " _
		  & "	WHERE Customer.Canceled<>-1 AND Telcom.DNIS='" & dNis & "' "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	else
		customerID = rs("customerid")
	end if
elseif customerID = "" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
end if
'---------------------------------------------------------
'---------------------------------------------------------
'---------------------------------------------------------

'validate security access
if loadUser(customerID) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************


'get action --> action=addfu&dnis=1234
dim action
action = trim(Request.QueryString("action"))
if len(action) = 0 then
	action = trim(Request.Form("action"))
end if
action = lCase(action)


dim tableProperty
dim I
dim item
dim pageSize
dim totalPages
dim showArr
dim sortField
dim sortOrder
dim curPage

dim showCRSID

'validate user's access to customer
if len(customerID) > 0 then
	  
	'CCI Admin/DBAdmin (bryan or steve)
	if cLng(session(session("siteID") & "adminLoggedOn")) < 2 then			
		mySQL = "SELECT Customer.Name " _
			  & "	FROM Customer " _
			  & "	WHERE Customer.CustomerID='" & customerID & "' "

	elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then			
		mySQL = "SELECT Customer.Name " _
			  & "	FROM Customer " _
			  & "	WHERE Customer.CustomerID='" & customerID & "' AND Canceled<>-1 "

	'everyone else
	else			  
		mySQL = "SELECT Customer.Name " _
			  & "	FROM Customer " _
			  & "	WHERE Customer.CustomerID='" & customerID & "' AND " _ 
			  & "			EXISTS ( " _
			  & "			SELECT vwLogins_IssueType.CustomerID, vwLogins_IssueType.LogID " _
			  & "				FROM vwLogins_IssueType " _
			  & "				WHERE (vwLogins_IssueType.CustomerID=Customer.CustomerID) AND (vwLogins_IssueType.LogID=" & sLogid & ") ) "
			  
	end if		  
	
	'open customer record
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
	else
		customerName = rs("Name")
	end if
	
end if

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and programAdmin = "N" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if

%>

<% dim pageTitle : pageTitle = customerName & " (edit)" %>
<!--#include file="../_includes/_INCheaderLite_.asp"-->

<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
        
        <!-- START Left sided area -->
		<td width="190" align="right" valign="top">

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided tree menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">                

                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->        
                    <% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>      
	                    <script>
							stExpandSubTree('myCMtree',1,0)
       		            </script>
            		<% else %>
	                    <script>
							stCollapseSubTree('myCMtree',1,0);
							stCollapseSubTree('myCMtree',2,0);							
       		            </script>
                    <% end if %>                          
                    <!-- STOP side menu include -->
                    
                </td>
              </tr>
              <!-- END Left sided tree menu -->
        
            </table>
        
        </td>
        <!-- STOP Left sided area -->
            
	    <!-- START Main Middle Section -->
		<td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">       

            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs());
       	        </script>
            </div>
            
            <div style="text-align:left; margin-bottom:10px;">				
                <div style="float:left; padding-right:2px; margin-right:2px;"><img src="../_images/icons/32/building_edit.png" title="" width="32" height="32" align="absmiddle" style="vertical-align:middle;"></div>
                <div>
                	<span class="pageTitle"><% =customerName %></span>
                	<br/><% response.write( getInlineHelp("getPageURL", 0) ) %>
                </div>                
            </div>                   

    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			'clear system messages
			call clearSessionMessages()
            %>            
    		<!-- STOP message notification box -->


            <!-- START Profile menu table -->
			<table id="customerTable" width="100%" cellpadding=0 cellspacing=0>
            	<tr>
                	<td align="left" style="width:auto;">
                    


                    <div class="iconMenuContainer">
						<div class="label">Profile</div>
						<div class="icons" id="menu_system" >
                        
                        	<!-- MENU for CCI Risk Specialist -->
							<% if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>
                                <div class="navIcon">
                                    <a href="#" onClick="SimpleModal.open('../_dialogs/popup_profile_snapshot.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 475, 500, 'no'); return false;"><img src="../_images/icons/32/briefcase.png" /></a><br /><a href="#" onClick="SimpleModal.open('../_dialogs/popup_profile_snapshot.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 475, 500, 'no'); return false;">Account<br />Profile</a>
                                </div>                                                    
                                <div class="navIcon">
                                    <a href="#" onClick="SimpleModal.open('../_dialogs/popup_location_table.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 500, 675, 'no'); return false;"><img src="../_images/icons/32/globe.png" /></a><br /><a href="#" onClick="SimpleModal.open('../_dialogs/popup_location_table.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 500, 675, 'no'); return false;">Locations</a>
                                </div>                                                        
                                <div class="navIcon">
                                    <a href="#" onClick="SimpleModal.open('../_dialogs/popup_category_table.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 500, 675, 'no'); return false;"><img src="../_images/icons/32/move_to_folder.png" /></a><br /><a href="#" onClick="SimpleModal.open('../_dialogs/popup_category_table.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 500, 675, 'no'); return false;">Categories</a>
                                </div>                                                        
                                <div class="navIcon">
                                    <img src="../_images/icons/32/question_disabled.png" /><br />Custom<br />Questions
                                </div>                                                        
                                <div class="navIcon">                            
                                    <a href="#" onClick="SimpleModal.open('../_dialogs/popup_directive_table.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 500, 625, 'no'); return false;"><img src="../_images/icons/32/direction.png" /></a><br /><a href="#" onClick="SimpleModal.open('../_dialogs/popup_directive_table.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 500, 625, 'no'); return false;">Directives</a>
                                </div>                            
                                <div class="navIcon">
                                    <a href="#" onClick="SimpleModal.open('../_dialogs/popup_telcom_table.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 500, 625, 'no'); return false;"><img src="../_images/icons/32/phone.png" /></a><br /><a href="#" onClick="SimpleModal.open('../_dialogs/popup_telcom_table.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 500, 625, 'no'); return false;">Telecom</a>
                                </div>      
                        	<!-- MENU for ALL Others -->    
                            <% else %>
                                <div class="navIcon">
                                    <a href="profile_edit.asp?cid=<% =customerid %>&action=edit"><img src="../_images/icons/32/briefcase.png" /></a><br /><a href="profile_edit.asp?cid=<% =customerid %>&action=edit">Account<br />Profile</a>
                                </div>        
                                <div class="navIcon">
                                    <a href="issuetypes.asp?cid=<% =customerid %>"><img src="../_images/icons/32/server_components.png" /></a><br /><a href="issuetypes.asp?cid=<% =customerid %>">Issue<br />Types</a>
                                </div>                                                                                                                                                                                      
                                <div class="navIcon">
                                    <a href="userfields.asp?cid=<% =customerid %>"><img src="../_images/icons/32/option_list.png" /></a><br /><a href="userfields.asp?cid=<% =customerid %>">Options &<br />User Fields</a>
                                </div>                                                                          
                                <div class="navIcon">
                                    <a href="location.asp?cid=<% =customerid %>"><img src="../_images/icons/32/globe.png" /></a><br /><a href="location.asp?cid=<% =customerid %>">Locations</a>
                                </div>                                                        
                                <div class="navIcon">
                                    <a href="category.asp?cid=<% =customerid %>"><img src="../_images/icons/32/move_to_folder.png" /></a><br /><a href="category.asp?cid=<% =customerid %>">Categories</a>
                                </div>                                                        
                                <div class="navIcon">
                                    <a href="questions.asp?cid=<% =customerid %>"><img src="../_images/icons/32/question.png" /></a><br /><a href="questions.asp?cid=<% =customerid %>">Custom<br />Questions</a>
                                </div>                                                        
                                <div class="navIcon">                            
                                    <% if cLng(session(session("siteID") & "adminLoggedOn")) > 2 then %>
                                        <% '<img src="../_images/icons/32/direction_disabled.png" /><br />Directives %>
	                                    <a href="#" onClick="SimpleModal.open('../_dialogs/popup_directive_table.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 500, 625, 'no'); return false;"><img src="../_images/icons/32/direction.png" /></a><br /><a href="#" onClick="SimpleModal.open('../_dialogs/popup_directive_table.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 500, 625, 'no'); return false;">Directives</a>                                        
                                    <% else %>
                                        <a href="directive.asp?cid=<% =customerid %>"><img src="../_images/icons/32/direction.png" /></a><br /><a href="directive.asp?cid=<% =customerid %>">Directives</a>
                                    <% end if %>                            
                                </div>                            
                                <div class="navIcon">
                                    <% if cLng(session(session("siteID") & "adminLoggedOn")) > 2 then %>
                                        <% '<img src="../_images/icons/32/phone_disabled.png" /><br />Telcom<br />Settings %>                                    
	                                    <a href="#" onClick="SimpleModal.open('../_dialogs/popup_telcom_table.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 500, 625, 'no'); return false;"><img src="../_images/icons/32/phone.png" /></a><br /><a href="#" onClick="SimpleModal.open('../_dialogs/popup_telcom_table.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 500, 625, 'no'); return false;">Telecom</a>                                                                                
                                    <% else %>
                                        <a href="telcom.asp?cid=<% =customerid %>"><img src="../_images/icons/32/phone.png" /></a><br /><a href="telcom.asp?cid=<% =customerid %>">Telecom</a>
                                    <% end if %>                            
                                </div>                                  

								<% if cLng(session(session("siteID") & "adminLoggedOn")) < 3 or uCase(customerid)="CEV" then %>
	                                <div class="navIcon">
										<a href="mcr.asp?cid=<% =customerid %>"><img src="../_images/icons/32/monitor_window_3d.png" /></a><br /><a href="mcr.asp?cid=<% =customerid %>">MCR<br />Configs</a>
        	                        </div>      
                                <% end if %>

								<% if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then 
									dim salesforceName
									if instr(customerName, " ") > 1 then
										salesforceName = trim(mid(customerName,1,instr(customerName, " ")))
									else
										salesforceName = trim(customerName)									
									end if
									%>
	                                <div class="navIcon">
										<a href="https://complianceline.my.salesforce.com/_ui/search/ui/UnifiedSearchResults?searchType=1&str=#!/fen=001&initialViewMode=detail&collapse=1&str=<% =salesforceName %>*" target="_blank"><img src="../_images/icons/32/salesforce.png" /></a><br /><a href="https://complianceline.my.salesforce.com/_ui/search/ui/UnifiedSearchResults?searchType=1&str=#!/fen=001&initialViewMode=detail&collapse=1&str=<% =salesforceName %>*" target="_blank">Salesforce</a>
        	                        </div>      
                                <% end if %>
                                
                            <% end if %>
                        
						</div>
                    </div>



                    <div class="iconMenuContainer">
						<div class="label">Users & Groups</div>
						<div class="icons" id="menu_system" >
                        	<!-- MENU for CCI RS -->
							<% if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>                        
                                <div class="navIcon">
                                    <a href="#" onClick="SimpleModal.open('../_dialogs/popup_user_table.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 500, 700, 'no'); return false;"><img src="../_images/icons/32/user.png" /></a><br /><a href="#" onClick="SimpleModal.open('../_dialogs/popup_user_table.asp?cid=<% =customerID %>&pageview=<% =pageView %>', 500, 700, 'no'); return false;">Users &amp;<br/>Contacts</a>
                                </div>                        
                                <div class="navIcon">
                                    <img src="../_images/icons/32/group_disabled.png" /><br />Groups
                                </div>
                            <!-- MENU for ALL Others -->
							<% else %>
                                <div class="navIcon">
									<% if cLng(session(session("siteID") & "adminLoggedOn")) > 3 and programAdmin = "N" then %>
										<img src="../_images/icons/32/user_disabled.png" /><br />Users
                                    <% else %>
                                    	<a href="users.asp?cid=<% =customerid %>"><img src="../_images/icons/32/user.png" /></a><br /><a href="users.asp?cid=<% =customerid %>">Users</a>
									<% end if %>
                                </div>                        
                                <div class="navIcon">
									<% if cLng(session(session("siteID") & "adminLoggedOn")) > 3 and programAdmin = "N" then %>
										<img src="../_images/icons/32/group_disabled.png" /><br />Groups
                                    <% else %>
                                    	<a href="groups.asp?cid=<% =customerid %>"><img src="../_images/icons/32/group.png" /></a><br /><a href="groups.asp?cid=<% =customerid %>">Groups</a>
									<% end if %>
                                </div>
                                <div class="navIcon">
									<% if cLng(session(session("siteID") & "adminLoggedOn")) > 3 and programAdmin = "N" then %>
										<img src="../_images/icons/32/group_gear_disabled.png" /><br />Assignment<br/>Rules
                                    <% else %>
                                    	<a href="rules.asp?cid=<% =customerid %>"><img src="../_images/icons/32/group_gear.png" /></a><br /><a href="rules.asp?cid=<% =customerid %>">Assignment<br/>Rules</a>
									<% end if %>
                                </div>
                                
                                <div class="navIcon">
									<% if cLng(session(session("siteID") & "adminLoggedOn")) > 3 and programAdmin = "N" then %>
										<img src="../_images/icons/32/group_go_disabled.png" /><br />Reassign<br/>Issues
                                    <% else %>
                                    	<a href="#" onClick="SimpleModal.open('../_dialogs/popup_user_reassign.asp?cid=<% =customerID %>&action=assignall', 300, 500, 'no'); return false;"><img src="../_images/icons/32/group_go.png" /></a><br /><a href="#" onClick="SimpleModal.open('../_dialogs/popup_user_reassign.asp?cid=<% =customerID %>&action=assignall', 300, 500, 'no'); return false;">Reassign<br/>Issues</a>
									<% end if %>
                                </div>
                                
							<% end if %>
						</div>
                    </div>
                    

                    <div class="iconMenuContainer">
						<div class="label">System</div>
						<div class="icons" id="menu_system" >
                        	<!-- MENU for CCI RS -->
							<% if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>                                                    
                                <div class="navIcon">                            
                                    <img src="../_images/icons/32/lock_disabled.png" /><br />Security<br />Settings                                
                                </div>
                                <div class="navIcon">
                                    <img src="../_images/icons/32/page_white_database.png" /><br />Audit<br />Logs
                                </div>
                                <div class="navIcon">
                                    <img src="../_images/icons/32/events_disabled.png" /><br />System<br />Alerts
                                </div>                                                        
                            <% else %>
                            
	                            <% if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then %>
	                                <div class="navIcon">                            
    	                                <a href="security_edit.asp?cid=<% =customerid %>&action=edit"><img src="../_images/icons/32/lock.png" /></a><br /><a href="security_edit.asp?cid=<% =customerid %>&action=edit">Security<br />Settings</a>
	                                </div>
                                <% end if %>                            
                                
                                <div class="navIcon">
                                    <img src="../_images/icons/32/page_white_database.png" /><span style="color:#666;"><br />Audit<br />Logs</span>
                                </div>                                                        
    
                                <div class="navIcon">
                                    <a href="alerts.asp?cid=<% =customerid %>"><img src="../_images/icons/32/events.png" /></a><br /><a href="alerts.asp?cid=<% =customerid %>">System<br />Alerts</a>
                                </div>                              
                                
                                <div class="navIcon">
                                    <a href="template.asp?cid=<% =customerid %>"><img src="../_images/icons/32/document_template.png" /></a><br /><a href="template.asp?cid=<% =customerid %>">Text<br />Templates</a>
                                </div>                              

                                <div class="navIcon">
                                    <a href="resolution.asp?cid=<% =customerid %>"><img src="../_images/icons/32/document_quote.png" /></a><br /><a href="resolution.asp?cid=<% =customerid %>">Resolution<br />Templates</a>
                                </div>                              
                                
                                <div class="navIcon">
                                    <a href="email.asp?cid=<% =customerid %>"><img src="../_images/icons/32/elements.png" /></a><br /><a href="email.asp?cid=<% =customerid %>">E-mail<br />Templates</a>
                                </div>                              
                                
                                <% if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then %>   
                                    <div class="navIcon">                            
                                        <a href="datasets.asp?cid=<% =customerid %>"><img src="../_images/icons/32/database_table.png" /></a><br /><a href="datasets.asp?cid=<% =customerid %>">Datasets</a>
                                    </div>
                                <% end if %>                            

                                <% if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then %>   
                                    <div class="navIcon">                            
	                                    <a href="purge.asp?cid=<% =customerid %>"><img src="../_images/icons/32/bin_closed.png" /></a><br /><a href="purge.asp?cid=<% =customerid %>">Record<br/>Retention</a>
                                    </div>
                                <% end if %>                            
                                                          
                            <% end if %>                            
						</div>
                    </div>
                    
                    
                    </td>
                </tr>
			</table>
       	    <!-- STOP Profile menu table -->
        
        
        </td>
    	<!-- STOP Main Middle Section -->

    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
  	</tr>
</table>
<!-- STOP Page -->

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()

'PROMPT follow-up pop-up for RS/RA
if action = "addfu" and cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
%>
	<script>	
        addFollowUp('-<% =customerID %>-');
    </script>
<%
end if
%>

<script language="javascript">	
	function pageReload() {  	
	 	window.location.reload();
    }
	//used by logon_dialog.asp to navigate back
	//to home page if neccssary
	function navigateAway(url) {
		window.location = url;
	}		
</script>	


