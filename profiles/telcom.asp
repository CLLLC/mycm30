<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = false
const showDHTMLMenu = true
const helpID = 6
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "telcom"
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

dim action
dim tableProperty

'Work Fields
dim I
dim item
dim pageSize
dim totalPages
dim showArr
dim sortField
dim sortOrder
dim curPage

dim showCRSID

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and programAdmin = "N" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if

%>

<% dim pageTitle : pageTitle = "Telcom" %>
<!--#include file="../_includes/_INCheaderLite_.asp"-->

<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
        
        <!-- START Left sided area -->
		<td width="190" align="right" valign="top">

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided tree menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">                

                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->        
                    <% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>      
	                    <script>
							stExpandSubTree('myCMtree',1,0)
       		            </script>
            		<% else %>
	                    <script>
							stCollapseSubTree('myCMtree',1,0);
							stCollapseSubTree('myCMtree',2,0);							
       		            </script>
                    <% end if %>                          
                    <!-- STOP side menu include -->
                    
                </td>
              </tr>
              <!-- END Left sided tree menu -->
        
            </table>
        
        </td>
        <!-- STOP Left sided area -->
            
	    <!-- START Main Middle Section -->
		<td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">       

            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs());
       	        </script>
            </div>
            
            <div style="text-align:left; margin-bottom:10px;">				
                <div style="float:left; padding-right:2px; margin-right:2px;"><img src="../_images/icons/32/phone.png" title="" width="32" height="32" align="absmiddle" style="vertical-align:middle;"></div>
                <div>
                	<span class="pageTitle">Telecom</span>
                	<br/>:: <a href="../profiles/profiles_menu.asp?cid=<% =customerID %>"><% response.write( getCustomerName(customerID) ) %></a>
                </div>
            </div>                   
                                          
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
                        <legend>Information</legend>
                        <div align="left">
	                        <img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; vertical-align:middle;">Below is a list of telcom entries. Click 'Number' to view/modify record.
							<% response.write( getInlineHelp("getPageURL", 0) ) %>
                        </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             

                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">
                        <div class="infoButtons">
                            <a href="#" onClick="SimpleModal.open('../_dialogs/popup_config_telcom.asp?action=add&cid=<% =customerid %>', 425, 475, 'no'); return false;" ><img src="../_images/icons/24/phone_add.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                            <a href="#" onClick="SimpleModal.open('../_dialogs/popup_config_telcom.asp?action=add&cid=<% =customerid %>', 425, 475, 'no'); return false;" >Add New</a>
                        </div>                   
                    </td>
                    <!-- STOP Information buttons -->
                                                                                
                </tr>
            </table>        
            <!-- STOP information box -->
    

    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			'clear system messages
			call clearSessionMessages()
            %>            
    		<!-- STOP message notification box -->


            <!-- START Reports table -->
            <%
			'CCI Admin/DBAdmin (bryan or steve)
			if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then			
				mySQL = "SELECT Telcom.*  " _
					  & "	FROM Telcom " _
					  & "	WHERE CustomerID='" & customerID & "' " _
					  & "	ORDER BY [Number] ASC "			  							  
			'everyone else
			else
				mySQL = "SELECT Telcom.*  " _
					  & "	FROM Telcom " _
					  & "	WHERE EXISTS ( " _
					  & "			SELECT vwLogins_IssueType.CustomerID, vwLogins_IssueType.LogID " _
					  & "				FROM vwLogins_IssueType " _
					  & "				WHERE (vwLogins_IssueType.CustomerID=Telcom.CustomerID) AND (vwLogins_IssueType.LogID=" & sLogid & ") ) " _
					  & "		  AND CustomerID='" & customerID & "' " _
					  & "	ORDER BY [Number] ASC "			  		
			
			end if				  
			'open telcom table...				  
	        set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
						
			if not rs.eof then
			%>
			
                <!-- START Telcom table -->
                <table id="customerTable" width="100%" style="background-color:#969696;" cellpadding=0 cellspacing=0>
                            
                    <thead>
                        <tr>
                            <th align="left" nowrap="nowrap">&nbsp;</th>
                            <th align="left" nowrap="nowrap">Number</th>
                            <th align="left" nowrap="nowrap">DNIS</th>                                
                            <th align="left" nowrap="nowrap">Greeting</th>
                        </tr>
                    </thead>
                    <tbody>
    
                    <%
                    do until rs.eof
					
                        'START current row
                        response.write("<tr>")									
                        
						'ICONS
                        response.Write("<td width=""60"" nowrap=""nowrap"" align=""center"">")
                        response.Write("<a href=""#"" onClick=""confirmPrompt('Confirm Delete','This action can not be undone.<br><br>Are you sure you want to delete this telcom entry?','redirect','../_dialogs/popup_config_telcom.asp?recID=" & rs("telcomid") & "&action=del&cid=" & customerid & "'); return false;"" ><img src=""../_images/icons/16/cancel.png"" title=""Delete Notification"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:5px; vertical-align:middle;"" /></a>")
						if cLng(session(session("siteID") & "adminLoggedOn")) >= 3 then 	
							response.Write("<a href=""#"" onclick=""jAlert('Permission denied.', 'myCM Alert'); return false;"" ><img src=""../_images/icons/16/clock_history_frame.png"" title=""Telecom Logs"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:7px; vertical-align:middle;""></a>")											
						else
							response.Write("<a href=""#"" onclick=""SimpleModal.open('../_dialogs/popup_hist_table.asp?recid=" & rs("dnis") & "&hist=Telecom', 500, 700, 'no'); return false;"" ><img src=""../_images/icons/16/clock_history_frame.png"" title=""Telecom Logs"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:7px; vertical-align:middle;""></a>")											
						end if
						response.Write("<a href=""#"" onclick=""SimpleModal.open('../_dialogs/popup_config_telcom.asp?action=edit&recid=" & rs("telcomid") & "&cid=" & customerid & "', 425, 475, 'no'); return false;"" ><img src=""../_images/icons/16/page_blue_edit.png"" title=""Open Telcom"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; vertical-align:middle;""></a>")											
                        response.Write("</td>")											
                            
                        'NAME link
                        response.Write("<td align=""left"" style=""cursor:pointer;"" onclick=""SimpleModal.open('../_dialogs/popup_config_telcom.asp?action=edit&recid=" & rs("telcomid") & "&cid=" & customerid & "', 425, 475, 'no'); return false;"" >")
                        response.write("<a style=""color:#333333;"">" & left(rs("number"), 150) & "</a>")
                        response.Write("</td>")
        
                        response.Write("<td align=""left"">" & rs("dnis") & "</td>")
                        response.Write("<td align=""left"">" & rs("greeting") & "</td>")
                                            
                        'CLOSE current row						
                        response.write("</tr>")
                        
                        rs.movenext
                    loop
                    %>					
    
                    </tbody>
                </table>
    
                <%
                tableProperty = "sort: true, sort_config:{sort_types:['String','String','String','String']}, filters_row_index: 1, " _
                              & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
                              & "status_bar: true, col_0: """", col_1: ""input"", col_2: ""input"", col_3: ""input"", " _
                              & "col_width:[null,""50%"",null,null], paging: true, paging_length: 25, " _
                              & "enable_non_empty_option: true, or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, " _
                              & "highlight_keywords: true, " _
							  & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
                              & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"						  
                %>
                <!-- STOP Telcom table -->
    
                <!-- Fire table build -->
                <script language="javascript" type="text/javascript">
                    //<![CDATA[
                    var tableProp = {<% =tableProperty %>};
                    //initiate table setup
                    var tf1 = setFilterGrid("customerTable",tableProp);
                    //]]>
                </script>
    
                <!-- Shadow table -->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="5px" style="background:#D0D0D0;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
                  </tr>
                </table>
        
        	<% else %>
	            <!-- show INFO that NO telcom exist -->
                <table width="100%" cellpadding="0" cellspacing="0">
           			<tr>
         				<td align="left" class="clearFormat" style="padding-right:2px;">
                			<% call systemMessageBox("telcomID","statusMessageINFO","No telcom entries found. Click ""Add Telcom"" above.") %>
                    	</td>
             		</tr>
        		</table>            
            <% end if %>
            
        </td>
    	<!-- STOP Main Middle Section -->

    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
  	</tr>
</table>
<!-- STOP Page -->

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<script language="javascript">	
	function pageReload(cid) {  	
    	window.location = '../profiles/telcom.asp?cid='+cid;
    }
	function navigateAway(url) {
		window.location = url;
	}		
</script>	
