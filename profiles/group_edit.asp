<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = false
const showDHTMLMenu = true
const helpID = 6
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "group"
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 3 and programAdmin = "N" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if

'Get action
dim action : action = trim(Request.QueryString("action"))
if len(action) = 0 then
	action = trim(Request.Form("action"))
end if
action = lCase(action)
if  action <> "edit" _
and action <> "add"  _
and action <> "del" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if

'Get Record ID
if action = "edit" _
or action = "del" then
	recId = trim(Request.QueryString("recId"))
	if recId = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Record ID.")
	end if
end if

'used to set side top and side tabs after user saves record
dim topTab, sideTab
topTab = trim(Request.QueryString("top"))
if len(topTab) <= 0 then
	topTab = "details"
end if
sideTab = trim(Request.QueryString("side"))
'the General reset is because there are NO side tabs on the 
'first TopTab. Once you add them you can take off 
'	--> or lCase(sideTab) = "general"
'if len(sideTab) <= 0 or lCase(sideTab) = "general" then
'	sideTab = ""
'end if


dim showCRSID '...need for NEW search box

dim recId
dim optGroupID
dim optName
dim optEditDetailsSummary
dim optViewCaseNotes
dim optEditCaseStatus
dim optEditOwner
dim optEditDateClosed
dim optEditSource
dim optViewCaseUserFields
dim optEditCaseMgr
dim optEditOwnerNotes
dim optEditCaseResolution
dim optEditCaseResolutionApp
dim optEditCaseUserFields
dim optViewInvestigations
dim optAddInvestigations
dim optViewAssignedInvOnly
dim optViewInvOutcome
dim optViewDocuments
dim optNotes
dim optAddNewIssue
dim optAddNewFollowUp
dim optReadOnly

'Get Record Details
if action = "edit" then

	mySQL="SELECT Groups.* " _
		& "		FROM Groups " _
		& "		WHERE CustomerID = '" & customerID & "' AND GroupID = " & recId & " "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Group ID.")
	else			
		'only assign when editing a user
		CustomerID = rs("customerid")
		optGroupID = rs("GroupID")			
		optName = rs("name")
		optAddNewIssue = rs("AddNewIssue")		
		optAddNewFollowUp = rs("AddNewFollowUp")		
		optEditDetailsSummary = rs("EditDetailsSummary")		
		optViewCaseNotes = rs("ViewCaseNotes")
		optEditCaseStatus = rs("EditCaseStatus")		
		optEditOwner = rs("EditOwner")		
		optEditDateClosed = rs("EditDateClosed")		
		optEditSource = rs("EditSource")								
		optViewCaseUserFields = rs("ViewCaseUserFields")
		optEditCaseMgr = rs("EditCaseMgr")
		optEditOwnerNotes = rs("EditOwnerNotes")
		optEditCaseResolution = rs("EditCaseResolution")
		optEditCaseResolutionApp = rs("EditCaseResolutionApp")
		optEditCaseUserFields = rs("EditCaseUserFields")
		optViewInvestigations = rs("ViewInvestigations")
		optAddInvestigations = rs("AddInvestigations")
		optViewAssignedInvOnly = rs("ViewAssignedInvOnly")	'SET BACKWARDS -- Y=NO AND N=YES for presentation purposes
		optViewInvOutcome = rs("ViewInvOutcome")
		optViewDocuments = rs("ViewDocuments")		
		optNotes = rs("Notes")
		optReadOnly = rs("ReadOnly")
	end if
	call closeRS(rs)

	'set readonly value if not provided
	if trim(optReadOnly) = "" or isNull(optReadOnly) then
		optReadOnly = "N"
	end if

elseif action = "add" then

	'only assign when editing a user
	optAddNewIssue = "Y"	
	optAddNewFollowUp = "Y"		
	optEditDetailsSummary = "Y"
	optViewCaseNotes = "Y"
	optEditCaseStatus = "Y"
	optEditOwner = "Y"
	optEditDateClosed = "Y"
	optEditSource = "Y"	
	optViewCaseUserFields = "Y"
	optEditCaseMgr = "Y"
	optEditOwnerNotes = "Y"
	optEditCaseResolution = "Y"
	optEditCaseResolutionApp = "Y"
	optEditCaseUserFields = "Y"
	optViewInvestigations = "Y"
	optAddInvestigations = "Y"	
	optViewAssignedInvOnly = "N"	'SET BACKWARDS -- Y=NO AND N=YES for presentation purposes
	optViewInvOutcome = "Y"
	optViewDocuments = "Y"
	optReadOnly = "N"

end if

dim activeElements : activeElements = "optNotes"
%>

<% 
dim pageTitle
if action = "edit" then
	pageTitle = "Group: " & optName 
else
	pageTitle = "Group: (new group)"
end if
%>
<!--#include file="../_includes/_INCheader_.asp"-->

<form name="frm" id="frm" method="post" action="group_exec.asp" style="padding:0px; margin:0px;">

<!-- ----------------------------------------------------------------- -->
<!-- AUTO SAVE fuctions for posting "frm" data to issues_exec.asp      -->
<!-- use autoPostForm('no'); to manually call form submit              -->
<!-- source: http://jquery.malsup.com/form/#getting-started            -->
<!-- ----------------------------------------------------------------- -->
<!--#include file="../_jquery/jform/autosave.asp"-->
<script type="text/javascript" src="../_jquery/jform/jquery.form.js"></script>  

<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
  
	    <!-- START Left sided area -->  
		<td width="190" align="right" valign="top" >    

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">
    
                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->              
                    <script>
						stCollapseSubTree('myCMtree',1,0);
						stCollapseSubTree('myCMtree',2,0);
						stCollapseSubTree('myCMtree',3,0);
       	            </script>
                    <!-- STOP side menu include -->
                                    
                </td>
              </tr>
              <!-- STOP Left sided menu -->
        
            </table>

		</td>
    	<!-- STOP Left sided area -->


        <!-- START Main Middle Section -->
        <td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">    

			<%
			'used to STOP updating the cookie crumb if page is being recalled from a SAVE
			dim updateCrumb
			if inStr(1, session(session("siteID") & "okMsg"), "was Added") > 0 then
				'do nothing...will update cookie
            elseif len(session(session("siteID") & "errMsg")) > 0 or len(session(session("siteID") & "okMsg")) > 0 then
				updateCrumb = "false"
            end if
			%>
            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs('<% =updateCrumb %>'));
       	        </script>
            </div>
    
            <div style="text-align:left; margin-bottom:10px;">				
                <div style="float:left; padding-right:2px; margin-right:2px;"><img src="../_images/icons/32/group.png" title="" width="32" height="32" align="absmiddle" style="vertical-align:middle;"></div>
                <div>
                	<span id="titleText" class="pageTitle">Group: <% if action = "edit" then response.write(optName) else response.Write("<i>&lt;none assigned&gt;</i>") %></span>
                	<br/>:: <a href="../profiles/profiles_menu.asp?cid=<% =customerID %>"><% response.write( getCustomerName(customerID) ) %></a>
                </div>
            </div>                   
                               
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
            	            <legend>Information</legend>
	                        <div align="left">
    	                        <img src="../_images/icons/16/information.png" alt="Information" width="16" height="16" align="absmiddle" style="margin-right:3px;">Add or modify this group's properties using the form below.
								<% response.write( getInlineHelp("getPageURL", 0) ) %>
        	                </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             
                                    
                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">               	
                    	<!-- TEMPORARILY TUNRED OFF TILL I CAN FIGURE WHAT TO PUT HERE... -->
                        &nbsp;
                    </td>
                    <!-- STOP Information buttons -->
                        
                </tr>
            </table>        
            <!-- STOP information box -->

    
    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) <= 0 and len(session(session("siteID") & "errMsg")) <= 0 then	
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")						
				response.write("		<div id=""systemMessage"" class=""statusMessageOK"" style=""display:none;"">")
				response.write("			<div class=""innerBorder"">")
				response.write("				<div class=""pad"">")
				response.write("					<div class=""icon"" style=""float:left;""></div>")
				response.write("					<div style=""margin-left:24px; margin-bottom:0px; text-align:left; height:100%;"">")
				response.write("						<span class=""title"">Notice</span><br><span id=""systemMessageTxt"" class=""description"">message...</span>")
				response.write("					</div>")
				response.write("				</div>")
				response.write("			</div>")
				response.write("		</div>  ")
				response.write("   </td>")
				response.write("</tr></table>")				
			end if
			
			'clear system messages
			call clearSessionMessages()
            %>
    		<!-- STOP message notification box -->

            <!-- START Top Page section with tabs & buttons -->
            <table id="table_tabs" width="100%" cellpadding="0" cellspacing="0">            
                <tr>
                
                    <!-- START Top Page Tabs-->
                    <td height="40" width="70%" valign="top">
                        <ul id="ul_tabs" class="tabs">
                            <li id="details" style="width:125px;" class="on" onClick="clickTopTab('details',''); resetSystemMsg('systemMessage');"><div>Group</div></li>
                            <!-- TURNED OFF for now until have use for it -->
                            <li id="settings" style="width:125px;" class="off" onClick="clickTopTab('settings','permissions'); resetSystemMsg('systemMessage');"><div>Settings</div></li>
                            <!-- the last id must be set to "end" -->
                            <li id="end" class="empty" style="width:150px;"><div>&nbsp;</div></li>                            
                        </ul> 
                        
                        <input type=hidden name="topTab" id="topTab" value="<% =topTab %>">      
                        <input type=hidden name="sideTab" id="sideTab" value="<% =sideTab %>">                                                                                  
                        
                    </td>                
                    <!-- STOP Top Page Tabs-->              
                    
                    <!-- START Submit Button at top of page--> 
                    <td nowrap style="padding-bottom:10px;" align="left">
                        <%
                        if action = "edit" then
                        %>
                            <!-- SAVE button -->
							<a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); saveThisForm(); return false;"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
                        <%
                        elseif action = "add" and (cLng(session(session("siteID") & "adminLoggedOn")) < 3 or programAdmin = "Y") then
                        %>
                            <!-- ADD button -->                            
							<a class="myCMbutton" href="#" accesskey="A" onClick="this.blur(); document.frm.submit(); return false;"><span class="add" style="padding-right:15px;"><u>A</u>dd</span></a>
                        <%
                        end if
                        %>
                       	<input type=hidden name="customerID" id="customerID" value="<% =customerID %>"> 
                        <input type=hidden name="groupID" id="groupID" value="<% =recId %>"> 
                        <input type=hidden name="action" id="action" value="<%=action%>">                        
                        <!-- CLOSE button -->
						<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); navigateAway('groups.asp?cid=<% =customerID %>'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>
                    </td>
                    <!-- STOP Submit Button -->
                             
                </tr>
            </table>
            <!-- STOP Top Page Tabs -->


            <!-- START Side Page Tabs [details] "id=tab:details" defines the section for the Top Tabs -->
            <!--#include file="../_includes/_INCgroupDetails_.asp"-->            
            <!-- STOP Side Page Tabs [details] -->    
        

            <!-- START Side Page Tabs [settings] "id=tab:settings" defines the section for the Top Tabs -->
            <!--#include file="../_includes/_INCgroupSettings_.asp"-->            
            <!-- STOP Side Page Tabs [notes] -->

    
            <!-- START Final table -->        
            <table class="formTable" width="100%" cellpadding="0" cellspacing="0">      
                                                              
                <tr>
                    <td width="70%" valign="top" align="left" nowrap>&nbsp;</td>
                    
                    <!-- START Submit Button at bottom of page -->                 
                    <td valign="top" align="left" nowrap>                
                        <%
                        if action = "edit" then
                        %>
							<a class="myCMbutton" href="#" onClick="this.blur(); saveThisForm(); return false;"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
                        <%
						elseif action = "add" and (cLng(session(session("siteID") & "adminLoggedOn")) < 3 or programAdmin = "Y") then
                        %>
                            <!-- ADD button -->                            
							<a class="myCMbutton" href="#" onClick="this.blur(); document.frm.submit(); return false;"><span class="add" style="padding-right:15px;"><u>A</u>dd</span></a>
                      	<%
                        end if
                        %>
                        <!-- CLOSE button -->
						<a class="myCMbutton" href="#" onClick="this.blur(); navigateAway('groups.asp?cid=<% =customerID %>'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>
                    </td>
                    <!-- STOP Submit Button --> 
    
                </tr>      
                                      
            </table>
            <!-- STOP Final table -->               
    
	    </td>         
	    <!-- STOP Main Middle Section -->

 
    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
	</tr>
</table>
<!-- STOP Page -->

</form>

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<script type="text/javascript"> 
	//load form where user last was...pulled from query URL or form submit
	window.onload = function () {
		var sideTab = '<% =sideTab %>';		
		clickTopTab('<% =topTab %>','<% =sideTab %>');		
		clickSideTab('<% =topTab %>','<% =sideTab %>');
	}		
</script>

<script> 
	function saveThisForm() {
		//document.getElementById('action').value = "edit";
		autoPostForm('no');
	}
</script>

<script language="javascript">
	//for adding a single investigation using 
	//the ADD button
	function navigateAway(url) {
		var title = "Cancel Changes";
		var msg = "Changes have been made to this profile and <strong>must<br>be saved</strong> before continuing.<br><br>Continue and cancel changes?";		
		tinyMCE.triggerSave();	//trigger save/convert of all TinyMCE text areas for proper save
		if(isFormChanged(document.frm)==true) {		
			confirmPrompt(title,msg,'redirect',url);
		}
		else {
			window.location = url;
		}
	}		
</script>

<script type="text/javascript"> 
    // Initialize TinyMCE with the tab_focus option 
    tinyMCE.init({ 
        mode : "exact",
        elements : "<% =activeElements %>",
        auto_resize : true,
        theme : "advanced",
        content_css : "../_css/tinyMCE.css",
        plugins : "tabfocus,searchreplace,fullscreen,insertdatetime,paste,inlinepopups",
        theme_advanced_buttons1 : "bold,italic,underline,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,fontsizeselect,forecolor,backcolor,separator,search,separator,pasteword,separator,insertdate,inserttime,separator,fullscreen",
        theme_advanced_buttons2 : "", 
        theme_advanced_buttons3 : "", 
        theme_advanced_toolbar_location : "top", 
        theme_advanced_toolbar_align : "center", 
        onchange_callback : "setTinyMCEdirty",
        plugin_insertdate_dateFormat : "%A, %B %d, %Y",
        plugin_insertdate_timeFormat : "%I:%M %p",
        tab_focus : ':prev,:next'
    }); 
</script>     

<script language="javascript">
	function setUserList(text,id) {
		var i, x;
		
		//used by child window to add selected users
		var el = document.getElementById('listUser');		
		el.options[el.length] = new Option(text, id, false);

		//add to users to hidden element for adding to CRS_Logins table
		var addE = document.getElementById('listUserAdded');
		addE.value = id + "," + addE.value;

		//check and see if value being added is in Removed list
		var delE = document.getElementById('listUserRemoved');
		delE.value = delE.value.replace(new RegExp(id + ",", "gi"),"");
						
	}

	function removeUser(obj) {
		//used to remove all selected items in an Option/Select list
		var addE = document.getElementById('listUserAdded');
		var delE = document.getElementById('listUserRemoved');
		var el = document.getElementById(obj);		
		var selIndex = el.selectedIndex;
		
		//make sure user was selected
		if (selIndex != -1) {						
			//make sure user agrees to change
			jConfirm('Remove selected user(s) from group?', 'Confirm Delete', function(r) {
				//user agrees, remove categories
				if (r==true) {
					//cycle through all selected users
					for(i=el.length-1; i>=0; i--)
					{
					  if(el.options[i].selected)
					  {
						//add to Removed User hidden field to delete from CRS_Logins table				
						delE.value = el.options[i].value + "," + delE.value;
						
						//check and see if value being removed is in Added list
						addE.value = addE.value.replace(new RegExp(el.options[i].value + ",", "gi"),"");
											
						//remove fron list listUser
						el.options[i] = null;				
					  }
					}
				}
				//user disagrees
				else {
					return false;
				}
			});							
		}
		//no users selected
		else {
			jAlert('No users were selected.', 'myCM Alert');			
		}
	}	

	function userLookUp(obj) {
		//used by child window to add selected Categories
		var i, x;
		var el = document.getElementById(obj);		
		var selIndex = el.selectedIndex;
		if (selIndex != -1) {

			x=0;
			//cycle through all categories to get selected count
			for(i=el.length-1; i>=0; i--) {
			  if(el.options[i].selected) {					  
				x=x+1;
			  }
			}
			//more that one 1 was choosen so STOP
			if(x>1) {
			  jAlert('More than one user was selected.', 'myCM Alert');
			  return false;
			}
			
			//all is good...process selection of primary			
			for(i=el.length-1; i>=0; i--) {
			  //found selected category
			  if(el.options[i].selected) {
				  
				  //alert(el.options[i].value);
				  SimpleModal.open('../_dialogs/popup_user_record.asp?recid=' + el.options[i].value + '&pageview=<% =pageView %>', 500, 700, 'yes');
			  }	
			}				
			
		//no category selected
		}
		else {
			jAlert('No user was selected.', 'myCM Alert');			
		}
	}
</script>
