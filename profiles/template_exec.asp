<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->

<%
'Database
dim mySQL, cn, rs, rs2

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'Directive fields in DETAILS tab
dim temName
dim	temTemplate

'flag right now as when this issue was modified
dim modifiedDate : modifiedDate = Now()

'Work Fields
dim action, recId

'Get action
action = trim(lCase(Request.Form("action")))
if len(action) = 0 then
	action = trim(lCase(Request.QueryString("action")))
end if
if  action <> "edit" _
and action <> "add" _
and action <> "del" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if

'Get ID of location to work with
if action = "edit"  _
or action = "del" then
	recId = trim(Request.Form("TemplateID"))
	if recId = "" then
		recId = trim(lCase(Request.QueryString("recid")))
	end if	
	if recId = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Template ID.")
	end if
end if

'make sure user can update this record -- PUT BACK programAdmin = "N" when ready for customers to view
if cLng(session(session("siteID") & "adminLoggedOn")) > 2 then
	if programAdmin = "N" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
	end if
end if


'used to redirect user back to tabs they last were on
dim topTab, sideTab
topTab = trim(Request.Form("topTab"))
if len(topTab) = 0 then
	topTab = trim(Request.QueryString("top"))
end if


'Get Location Details
if action = "edit" or action = "add" then

	temName = trim(Request.Form("temName"))
	temTemplate = trim(Request.Form("temTemplate"))
					
end if


'ADD
if action = "add" then

	dim fieldsSQL, valueSQL

	'Add Issue with Callback
	fieldsSQL = "INSERT INTO Template (" _
		  & "CustomerID,Name,Template,ModifiedBy,ModifiedDate"
	fieldsSQL = fieldsSQL & ") "
		  
	valueSQL = " VALUES (" _
		  & "'"    	& uCase(customerID)				& "'," _
		  & "'"    	& temName						& "'," _
		  & "'"    	& replace(temTemplate,"'","''")	& "'," _
		  & " "    	& sLogid						& ", " _
		  & "'"    	& modifiedDate					& "' "		  
	valueSQL = valueSQL & ") "

	'set identity to pull back newly added record new record id is returned to newID
	mySQL = "SET NOCOUNT ON; " & fieldsSQL & valueSQL & " SELECT SCOPE_IDENTITY() AS newID;"
	set rs = openRSexecute(mySQL)		
	recId = rs("newID")
		  				
	session(session("siteID") & "okMsg") = "Template was Added."
	response.redirect "template_edit.asp?action=edit&recID=" & recId & "&cid=" & customerID & "&top=" & topTab
	
end if


'EDIT
if action = "edit" then
		
	'Update Record
	mySQL = "UPDATE Template SET " _	
		  & "Name='"   			& temName						& "'," _
		  & "Template='"   		& replace(temTemplate,"'","''")	& "'," _
		  & "ModifiedBy="  		& sLogid						& ", " _
		  & "ModifiedDate='"	& modifiedDate					& "' "

	'finalize update query
	mySQL = mySQL & "WHERE TemplateID = '" & recId & "' "

	'update issue
	set rs = openRSexecute(mySQL)	

	session(session("siteID") & "okMsg") = "Template has been saved successfully."
	response.redirect "template_edit.asp?action=edit&recID=" & recId & "&cid=" & customerID & "&top=" & topTab

end if


'DELETE
if action = "del" then

	'Delete records from optionsGroupsXref
	mySQL = "DELETE FROM Template " _
	      & "WHERE TemplateID = " &  recId & " "
	set rs = openRSexecute(mySQL)	
	
	session(session("siteID") & "okMsg") = "Template was Deleted."
	response.redirect "template.asp?cid=" & customerID
	
end if


'Just in case we ever get this far...
call closeDB()
Response.Redirect "template.asp"

%>
