<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->

<%
'Database
dim mySQL, cn, rs, rs2

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'Directive fields in DETAILS tab
dim dirName
dim	dirNotes
dim	dirBegins
dim	dirEnds
dim	dirSortOrder
dim dirEvent

'flag right now as when this issue was modified
dim modifiedDate : modifiedDate = Now()

'Work Fields
dim action, recId

'Get action
action = trim(lCase(Request.Form("action")))
if len(action) = 0 then
	action = trim(lCase(Request.QueryString("action")))
end if
if  action <> "edit" _
and action <> "add" _
and action <> "del" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if

'Get ID of location to work with
if action = "edit"  _
or action = "del" then
	recId = trim(Request.Form("DirectiveID"))
	if recId = "" then
		recId = trim(lCase(Request.QueryString("recid")))
	end if	
	if recId = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Directive ID.")
	end if
end if

'make sure user can update this record -- PUT BACK programAdmin = "N" when ready for customers to view
if cLng(session(session("siteID") & "adminLoggedOn")) > 2 then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if


'used to redirect user back to tabs they last were on
dim topTab, sideTab
topTab = trim(Request.Form("topTab"))
if len(topTab) = 0 then
	topTab = trim(Request.QueryString("top"))
end if


'Get Location Details
if action = "edit" or action = "add" then

	dirName = trim(Request.Form("dirName"))
	dirNotes = trim(Request.Form("dirNotes"))
	dirBegins = trim(Request.Form("dirBegins"))
	dirEnds = trim(Request.Form("dirEnds"))
	dirSortOrder = trim(Request.Form("dirSortOrder"))
	dirEvent = trim(Request.Form("dirEvent"))
					
end if


'ADD
if action = "add" then

	dim fieldsSQL, valueSQL

	'Add Issue with Callback
	fieldsSQL = "INSERT INTO Directive (" _
		  & "CustomerID,Name,Notes,Event,ModifiedBy,ModifiedDate"

	'check for empty dates	      
	if len(dirBegins) > 0 then fieldsSQL = fieldsSQL & ",Begins"
	if len(dirEnds) > 0 then fieldsSQL = fieldsSQL & ",Ends"
	if len(dirSortOrder) > 0 then fieldsSQL = fieldsSQL & ",SortOrder"		
	fieldsSQL = fieldsSQL & ") "
		  
	valueSQL = " VALUES (" _
		  & "'"    	& uCase(customerID)				& "'," _
		  & "'"    	& dirName						& "'," _
		  & "'"    	& replace(dirNotes,"'","''")	& "'," _
		  & "'"    	& dirEvent						& "'," _
		  & " "    	& sLogid						& ", " _
		  & "'"    	& modifiedDate					& "' "

	'check for empty dates
	if len(dirBegins) > 0 then valueSQL = valueSQL & ",'" & dirBegins & "'"
	if len(dirEnds) > 0 then valueSQL = valueSQL & ",'" & dirEnds & "'"				  
	if len(dirSortOrder) > 0 then valueSQL = valueSQL & "," & dirSortOrder & " "		  
	valueSQL = valueSQL & ") "

	'set identity to pull back newly added record new record id is returned to newID
	mySQL = "SET NOCOUNT ON; " & fieldsSQL & valueSQL & " SELECT SCOPE_IDENTITY() AS newID;"
	set rs = openRSexecute(mySQL)		
	recId = rs("newID")
		  				
	session(session("siteID") & "okMsg") = "Directive was Added."
	response.redirect "directive_edit.asp?action=edit&recID=" & recId & "&cid=" & customerID & "&top=" & topTab
	
end if


'EDIT
if action = "edit" then
		
	'Update Record
	mySQL = "UPDATE Directive SET " _	
		  & "Name='"   			& dirName						& "'," _
		  & "Notes='"    		& replace(dirNotes,"'","''")	& "'," _
  		  & "Event='" 			& dirEvent						& "'," _
		  & "ModifiedBy="  		& sLogid						& ", " _
		  & "ModifiedDate='"	& modifiedDate					& "' "

	'check for special existance of dates  
	if len(dirBegins) > 0 then mySQL = mySQL & ",Begins='" & dirBegins & "' " else mySQL = mySQL & ",Begins=NULL "
	if len(dirEnds) > 0 then mySQL = mySQL & ",Ends='" & dirEnds & "' " else mySQL = mySQL & ",Ends=NULL "
	if len(dirSortOrder) > 0 then mySQL = mySQL & ",SortOrder='" & dirSortOrder & "' " else mySQL = mySQL & ",SortOrder=NULL "
		  
	'finalize update query
	mySQL = mySQL & "WHERE DirectiveID = '" & recId & "' "

	'update issue
	set rs = openRSexecute(mySQL)	

	session(session("siteID") & "okMsg") = "Directive has been saved successfully."
	response.redirect "directive_edit.asp?action=edit&recID=" & recId & "&cid=" & customerID & "&top=" & topTab

end if


'DELETE
if action = "del" then

	'Delete records from optionsGroupsXref
	mySQL = "DELETE FROM Directive " _
	      & "WHERE DirectiveID = " &  recId & " "
	set rs = openRSexecute(mySQL)	
	
	session(session("siteID") & "okMsg") = "Directive was Deleted."
	response.redirect "directive.asp?cid=" & customerID
	
end if


'Just in case we ever get this far...
call closeDB()
Response.Redirect "directive.asp"

%>
