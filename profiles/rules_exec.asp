<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->

<%
'Database
dim mySQL, cn, rs, rs2

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("customerID")
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'Get action
dim action, recId
action = trim(lCase(Request.Form("action")))
if len(action) = 0 then
	action = trim(lCase(Request.QueryString("action")))
end if
if  action <> "edit" _
and action <> "add" _
and action <> "del" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if

'Get ID of location to work with
if action = "edit"  _
or action = "del" then
	recId = trim(Request.Form("RuleID"))
	if recId = "" then
		recId = trim(lCase(Request.QueryString("recid")))
	end if	
	if recId = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Rule ID.")
	end if
end if

'make sure user can update this record
if programAdmin = "N" and cLng(session(session("siteID") & "adminLoggedOn")) > 3 then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if


'used to redirect user back to tabs they last were on
dim topTab, sideTab
topTab = trim(Request.Form("topTab"))
if len(topTab) = 0 then
	topTab = trim(Request.QueryString("top"))
end if
sideTab = trim(Request.Form("sideTab"))
if len(sideTab) = 0 then
	sideTab = trim(Request.QueryString("side"))
end if


'Rule fields in DETAILS tab
dim optRuleID
dim optName
dim optViewCaseNotes
dim optViewCaseUserFields
dim optEditCaseMgr
dim optEditOwnerNotes
dim optEditCaseResolution
dim optEditCaseResolutionApp
dim optEditCaseUserFields
dim optViewAssignedInvOnly

dim optType, arrType, expI

dim optFilter
dim optNotes

'ADD FILTER
dim arrField
dim arrExp
dim arrValue
dim tmpValue
dim i
	
'flag right now as when this issue was modified
dim modifiedDate : modifiedDate = Now()


'Get Rule Details
if action = "edit" or action = "add" then

	optName = trim(Request.Form("optName"))
	if optName = "" or isNull(optName) then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Rule Name.")		
	end if
			
	optNotes = trim(Request.Form("optNotes"))

end if


'******************************
'build filter XML with filter
if action = "edit" or action = "add" then	
	
	optFilter = "<?xml version='1.0' encoding='ISO-8859-1'?>"
	optFilter = optFilter & "<filters>"

	'------------------------------------------------
	'find all form elements in filter box
	'------------------------------------------------	
	'discover filtered fields
	arrField = split(Request.Form("idQueryField"),",")
	'discover operators on each filtered field
	arrExp = split(Request.Form("idQueryExp"),",")
	'discovery query values entered by user
	for i = 1 To request.form("idQueryValue").Count 
		arrValue = arrValue & request.form("idQueryValue")(i) & "|"
  	next 
	if right(arrValue,1) = "|" then arrValue = left(arrValue,len(arrValue)-1)
	arrValue = split(arrValue,"|")
	
	'------------------------------------------------
	'Loop through filter list and add one by one to XML
	'------------------------------------------------	
	for expI = LBound(arrField) to UBound(arrField)	
		'Clean up value and replace where needed
		if len(trim(Request.Form("idQueryValue"))) > 0 then
			tmpValue = trim(arrValue(expI))
			tmpValue = replace(tmpValue,"*","%")
			tmpValue = replace(tmpValue,"""","'")
		else
			tmpValue = ""
		end if
		'put string together
		optFilter = optFilter & "<filter>"
		optFilter = optFilter & "<field>" & trim(arrField(expI)) & "</field>"
		optFilter = optFilter & "<operator>" & Server.HTMLEncode(trim(arrExp(expI))) & "</operator>"		
		optFilter = optFilter & "<value>" & tmpValue & "</value>"
		optFilter = optFilter & "</filter>"		
	next	
	'close XML
	optFilter = optFilter & "</filters>"
	'------------------------------------------------
	
end if


'Delete/Add Members
if action = "edit" then

	'***************************************
	'Add any Users/Logins selected to be added
	dim listUserAdded
	dim arrAddedUser
	dim addedU

	'discover added users
	listUserAdded = trim(Request.Form("listUserAdded"))
	if len(listUserAdded) > 0 then

		'-----------------------------------		
		'add newly assigned users to issue
		'-----------------------------------		
		arrAddedUser = split(listUserAdded,",")
		'move through all users selected
		for addedU = LBound(arrAddedUser) to UBound(arrAddedUser)
			if len(arrAddedUser(addedU)) > 0 then
				'make sure does NOT already exist so it's not added twice
				mySQL = "SELECT Count(RuleID) AS RuleCount FROM Logins_Rule " _
					  & "WHERE RuleID = " & recId & " and LogID = " & arrAddedUser(addedU) & " "
				set rs = openRSexecute(mySQL)
				'does NOT exist, go ahead and add
				if rs("RuleCount") <= 0 then									
					'add record to table
					mySQL = "INSERT INTO Logins_Rule (" _
						  & "LogID,RuleID,ModifiedBy,ModifiedDate" _
						  & ") VALUES (" _
						  & " " & arrAddedUser(addedU) & "," & recId & "," & sLogid & ",'" & modifiedDate & "' " _
						  & ")"
					set rs = openRSexecute(mySQL)					
				end if			
			end if
		next								
		
	end if


	'***************************************
	'Remove any Users selected	
	dim listUserRemoved
	dim arrayRemovedUser
	dim removedU

	'Add new users to Case History
	listUserRemoved = trim(Request.Form("listUserRemoved"))
	if len(listUserRemoved) > 0 then
		arrayRemovedUser = split(listUserRemoved,",")		
		'move through all users.
		for removedU = LBound(arrayRemovedUser) to UBound(arrayRemovedUser)
			if len(arrayRemovedUser(removedU)) > 0 then
				'delete record from table
				mySQL = "DELETE FROM Logins_Rule " _
					  & "WHERE RuleID = " & recId & " AND LogID = " & arrayRemovedUser(removedU)
				set rs = openRSexecute(mySQL)
			end if
		next	
	end if

end if


'ADD
if action = "add" then

	dim fieldsSQL, valueSQL

	'Add Issue with Callback
	fieldsSQL = "INSERT INTO [Rule] (" _
		  & "CustomerID,Name,RuleXML,Notes,ModifiedBy,ModifiedDate"
	fieldsSQL = fieldsSQL & ") "
		  
	valueSQL = " VALUES (" _
		  & "'"    	& uCase(customerID)				& "'," _
		  & "'"    	& replace(optName,"'","''")		& "'," _
		  & "'"    	& replace(optFilter,"'","''") 	& "'," _
		  & "'"    	& replace(optNotes,"'","''")	& "'," _
		  & " "    	& sLogid						& ", " _
		  & "'"    	& modifiedDate					& "' "
	valueSQL = valueSQL & ") "

	'set identity to pull back newly added record new record id is returned to newID
	mySQL = "SET NOCOUNT ON; " & fieldsSQL & valueSQL & " SELECT SCOPE_IDENTITY() AS newID;"
	set rs = openRSexecute(mySQL)		
	recId = rs("newID")		  				

	session(session("siteID") & "okMsg") = "Rule was Added."
	response.redirect "rules_edit.asp?action=edit&recID=" & recId & "&cid=" & customerID & "&top=" & topTab & "&side=" & sideTab
	
end if


'EDIT
if action = "edit" then
		
	'Update Record
	mySQL = "UPDATE [Rule] SET " _	
		  & "Name='"   			& replace(optName,"'","''")		& "'," _
		  & "RuleXML='"    		& replace(optFilter,"'","''")	& "'," _
		  & "Notes='"     		& replace(optNotes,"'","''")	& "'," _
		  & "ModifiedBy="  		& sLogid						& ", " _
		  & "ModifiedDate='"	& modifiedDate					& "' "
		  
	'finalize update query
	mySQL = mySQL & "WHERE RuleID = '" & recId & "' "

	'update issue
	set rs = openRSexecute(mySQL)	

	session(session("siteID") & "okMsg") = "Rule has been saved successfully."
	response.redirect "rules_edit.asp?action=edit&recID=" & recId & "&cid=" & customerID & "&top=" & topTab & "&side=" & sideTab

end if


'DELETE
if action = "del" then

	'Delete records from optionsGroupsXref
	mySQL = "DELETE FROM [Rule] " _
	      & "WHERE RuleID = " &  recId & " "
	set rs = openRSexecute(mySQL)

	'delete existing records from Category_IssueType
	mySQL = "DELETE FROM Logins_Rule " _
		  & "WHERE RuleID=" & recId & " "
	set rs = openRSexecute(mySQL)
	
	session(session("siteID") & "okMsg") = "Rule was deleted and any members removed."
	response.redirect "rules.asp?cid=" & customerID
	
end if


'Just in case we ever get this far...
call closeDB()
Response.Redirect "rules.asp"

%>
