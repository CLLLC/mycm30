<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = false
const showDHTMLMenu = true
const helpID = 6
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "rules"
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 3 and programAdmin = "N" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if

'Get action
dim action : action = trim(Request.QueryString("action"))
if len(action) = 0 then
	action = trim(Request.Form("action"))
end if
action = lCase(action)
if  action <> "edit" _
and action <> "add"  _
and action <> "del" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if

'Get Record ID
if action = "edit" _
or action = "del" then
	recId = trim(Request.QueryString("recId"))
	if recId = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Record ID.")
	end if
end if

'used to set side top and side tabs after user saves record
dim topTab
topTab = trim(Request.QueryString("top"))
if len(topTab) <= 0 then
	topTab = "details"
end if


dim showCRSID '...need for NEW search box

dim startDate
dim endDate

'Issue Fields on DETAILS tab
dim optID
dim optName
dim optSort
dim optQueryValue
dim optSQLValue
dim optUserRole
dim optUser
dim rptOwner
dim rptLogid
dim optUsers
dim optOwner
dim optDataset
dim optDatasetDesc
dim optTable
dim optType
dim optDesc
dim optFields
dim optFilter
dim optIssueList
dim optURLField
dim optGroup
dim optGroupCalc
dim optSortOrder
dim optSortField
dim optCategory
dim optNotes

dim listColumn, listFriendly

'Chart options
dim optChart
dim optChartXAxis
'dim optChartXRotate
dim optChartYAxis
dim optChartSize
dim optChartLegend
'dim optChartBackground
'dim optChartCanvas
'dim optChartElement

dim intStep
dim strTemp

'Work Fields
dim idReport

'Work Fields
dim optArr, optArrIndex

'Used with Dynamic List Contact selections
dim intGroups, arrGroups, groupsI, intUsers
dim tmpUserID, tmpUserName, lstUserName
dim arrUsers, usersI

dim field, arrFields, arrColumn, arrFriendly


dim recId
dim optViewCaseNotes
dim optViewCaseUserFields
dim optEditCaseStatus
dim optEditCaseMgr
dim optEditOwnerNotes
dim optEditCaseResolution
dim optEditCaseResolutionApp
dim optEditCaseUserFields
dim optViewAssignedInvOnly

dim optRuleID


'Get Record Details
if action = "edit" then

	mySQL="SELECT RuleID, CustomerID, Name, RuleXML, Notes " _
		& "		FROM [Rule] " _
		& "		WHERE CustomerID = '" & customerID & "' AND RuleID = " & recId & " "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Rule ID.")
	else			
		'only assign when editing a user
		CustomerID = rs("customerid")
		optRuleID = rs("RuleID")			
		optName = rs("name")
		optFilter = rs("RuleXML")
		optNotes = rs("Notes")
		
	end if
	call closeRS(rs)

elseif action = "add" then
	'nothing to do...

end if





'Get fields for list box...these are the fields the use selects
if action = "edit" or action = "add" then

	optTable = "vwIssues_Rule_Assignment"
	
	'get customer record
	mySQL="SELECT * " _
		& "   FROM vwIssues_Rule_Assignment " _
	    & "	  WHERE 1=2"								
	'set rs = openRSexecute(mySQL)
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	'loop through all columns
	for each field in rs.fields
		if lCase(field.name) = "logid" or lCase(field.name) = "viewedby" or lCase(field.name) = "customerid" or lCase(field.name) = "issuetypeid" or lCase(field.name) = "crsid" or lCase(field.name) = "logidview" or inStr(lCase(field.name),"hierarchy_search") then
			'DO NOT ADD THIS FIELD...
		else
			listColumn = listColumn & "[" & field.name & "],"
			listFriendly = listFriendly & field.name & ","
		end if
	next
	
	'create array of available fields
	if right(listColumn,1) = "," then listColumn = left(listColumn,len(listColumn)-1)	
	arrColumn = split(listColumn,",")

	'create array of available fields WITHOUT "[ ]" brackets for display only	
	if right(listFriendly,1) = "," then listColumn = left(listFriendly,len(listFriendly)-1)	
	arrFriendly = split(listFriendly,",")

	'create array of fields already selected by user previously
	arrFields = split(optFields,",")

end if





dim activeElements : activeElements = "optNotes"
%>

<% 
dim pageTitle
if action = "edit" then
	pageTitle = "Rule: " & optName 
else
	pageTitle = "Rule: (new rule)"
end if
%>
<!--#include file="../_includes/_INCheader_.asp"-->

<form name="frm" id="frm" method="post" action="rules_exec.asp" style="padding:0px; margin:0px;">

<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
  
	    <!-- START Left sided area -->  
		<td width="190" align="right" valign="top" >    

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">
    
                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->              
                    <script>
						stCollapseSubTree('myCMtree',1,0);
						stCollapseSubTree('myCMtree',2,0);
						stCollapseSubTree('myCMtree',3,0);
       	            </script>
                    <!-- STOP side menu include -->
                                    
                </td>
              </tr>
              <!-- STOP Left sided menu -->
        
            </table>

		</td>
    	<!-- STOP Left sided area -->


        <!-- START Main Middle Section -->
        <td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">    

			<%
			'used to STOP updating the cookie crumb if page is being recalled from a SAVE
			dim updateCrumb
			if inStr(1, session(session("siteID") & "okMsg"), "was Added") > 0 then
				'do nothing...will update cookie
            elseif len(session(session("siteID") & "errMsg")) > 0 or len(session(session("siteID") & "okMsg")) > 0 then
				updateCrumb = "false"
            end if
			%>
            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs('<% =updateCrumb %>'));
       	        </script>
            </div>
    
            <div style="text-align:left; margin-bottom:10px;">				
                <div style="float:left; padding-right:2px; margin-right:2px;"><img src="../_images/icons/32/group_gear.png" title="" width="32" height="32" align="absmiddle" style="vertical-align:middle;"></div>
                <div>
                	<span id="titleText" class="pageTitle">Rule: <% if action = "edit" then response.write(optName) else response.Write("<i>&lt;none assigned&gt;</i>") %></span>
                	<br/>:: <a href="../profiles/profiles_menu.asp?cid=<% =customerID %>"><% response.write( getCustomerName(customerID) ) %></a>
                </div>
            </div>                   
                               
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
            	            <legend>Information</legend>
	                        <div align="left">
    	                        <img src="../_images/icons/16/information.png" alt="Information" width="16" height="16" align="absmiddle" style="margin-right:3px;">Add or modify this rules's properties using the form below.
								<% response.write( getInlineHelp("getPageURL", 0) ) %>
                                <div align="left" style="padding-left:0px; padding-top:3px; margin:0px;">
                                    <img src="../_images/icons/16/exclamation.png" alt="Information" width="16" height="16" align="absmiddle" style="margin-right:3px;"><span class="required">WARNING</span>: Changing this rule may affect real time call processing.
                                </div>
        	                </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             
                                    
                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">               	
                    	<!-- TEMPORARILY TUNRED OFF TILL I CAN FIGURE WHAT TO PUT HERE... -->
                        &nbsp;
                    </td>
                    <!-- STOP Information buttons -->
                        
                </tr>
            </table>        
            <!-- STOP information box -->

    
    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			'clear system messages
			call clearSessionMessages()
            %>
    		<!-- STOP message notification box -->


            <!-- START Top Page section with tabs & buttons -->
            <table id="table_tabs" width="100%" cellpadding="0" cellspacing="0">            
                <tr>
                
                    <!-- START Top Page Tabs-->
                    <td height="40" width="70%" valign="top">
                        <ul id="ul_tabs" class="tabs">
                            <li id="details" style="width:125px;" class="on" onClick="clickTopTab('details',''); resetSystemMsg('systemMessage');"><div>Rule</div></li>
                            <!-- TURNED OFF for now until have use for it -->
                            <li id="settings" style="width:125px; display:none;" class="off" onClick="clickTopTab('settings',''); resetSystemMsg('systemMessage');"><div>Settings</div></li>
                            <!-- the last id must be set to "end" -->
                            <li id="end" class="empty" style="width:150px;"><div>&nbsp;</div></li>                            
                        </ul> 
                        
                        <input type=hidden name="topTab" id="topTab" value="<% =topTab %>">                                                                
                        
                    </td>                
                    <!-- STOP Top Page Tabs-->              
                    
                    <!-- START Submit Button at top of page--> 
                    <td nowrap style="padding-bottom:10px;" align="left">
                        <%
                        if action = "edit" then
                        %>
                            <!-- SAVE button -->
							<a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); document.frm.submit(); return false;"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
                        <%
                        elseif action = "add" and (cLng(session(session("siteID") & "adminLoggedOn")) < 3 or programAdmin = "Y") then
                        %>
                            <!-- ADD button -->                            
							<a class="myCMbutton" href="#" accesskey="A" onClick="this.blur(); document.frm.submit(); return false;"><span class="add" style="padding-right:15px;"><u>A</u>dd</span></a>
                        <%
                        end if
                        %>
                       	<input type=hidden name="customerID" id="customerID" value="<% =customerID %>"> 
                        <input type=hidden name="optTable" id="optTable" value="<% =optTable %>">                        
                        <input type=hidden name="ruleID" id="ruleID" value="<% =recId %>"> 
                        <input type=hidden name=action value="<%=action%>">                        
                        <!-- CLOSE button -->
						<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); navigateAway('rules.asp?cid=<% =customerID %>'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>
                    </td>
                    <!-- STOP Submit Button -->
                             
                </tr>
            </table>
            <!-- STOP Top Page Tabs -->


            <!-- START Side Page Tabs [details] "id=tab:details" defines the section for the Top Tabs -->
            <!--#include file="../_includes/_INCrulesDetails_.asp"-->            
            <!-- STOP Side Page Tabs [details] -->    
        

            <!-- START Side Page Tabs [settings] "id=tab:settings" defines the section for the Top Tabs -->
            <!--#include file="../_includes/_INCrulesSettings_.asp"-->            
            <!-- STOP Side Page Tabs [notes] -->

    
            <!-- START Final table -->        
            <table class="formTable" width="100%" cellpadding="0" cellspacing="0">      
                                                              
                <tr>
                    <td width="70%" valign="top" align="left" nowrap>&nbsp;</td>
                    
                    <!-- START Submit Button at bottom of page -->                 
                    <td valign="top" align="left" nowrap>                
                        <%
                        if action = "edit" then
                        %>
							<a class="myCMbutton" href="#" onClick="this.blur(); document.frm.submit(); return false;"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
                        <%
						elseif action = "add" and (cLng(session(session("siteID") & "adminLoggedOn")) < 3 or programAdmin = "Y") then
                        %>
                            <!-- ADD button -->                            
							<a class="myCMbutton" href="#" onClick="this.blur(); document.frm.submit(); return false;"><span class="add" style="padding-right:15px;"><u>A</u>dd</span></a>
                      	<%
                        end if
                        %>
                        <!-- CLOSE button -->
						<a class="myCMbutton" href="#" onClick="this.blur(); navigateAway('rules.asp?cid=<% =customerID %>'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>
                    </td>
                    <!-- STOP Submit Button --> 
    
                </tr>      
                                      
            </table>
            <!-- STOP Final table -->               
    
	    </td>         
	    <!-- STOP Main Middle Section -->

 
    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
	</tr>
</table>
<!-- STOP Page -->

</form>

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<script type="text/javascript"> 
	//load form where user last was...pulled from query URL or form submit
	window.onload = function () {
		clickTopTab('<% =topTab %>','');
	}		
</script>

<script language="javascript">
	//for adding a single investigation using 
	//the ADD button
	function navigateAway(url) {
		var title = "Cancel Changes";
		var msg = "Changes have been made to this profile and <strong>must<br>be saved</strong> before continuing.<br><br>Continue and cancel changes?";		
		tinyMCE.triggerSave();	//trigger save/convert of all TinyMCE text areas for proper save
		if(isFormChanged(document.frm)==true) {		
			confirmPrompt(title,msg,'redirect',url);
		}
		else {
			window.location = url;
		}
	}		
</script>

<script type="text/javascript"> 
    // Initialize TinyMCE with the tab_focus option 
    tinyMCE.init({ 
        mode : "exact",
        elements : "<% =activeElements %>",
        auto_resize : true,
        theme : "advanced",
        content_css : "../_css/tinyMCE.css",
        plugins : "tabfocus,searchreplace,fullscreen,insertdatetime,paste,inlinepopups",
        theme_advanced_buttons1 : "bold,italic,underline,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,fontsizeselect,forecolor,backcolor,separator,search,separator,pasteword,separator,insertdate,inserttime,separator,fullscreen",
        theme_advanced_buttons2 : "", 
        theme_advanced_buttons3 : "", 
        theme_advanced_toolbar_location : "top", 
        theme_advanced_toolbar_align : "center", 
        onchange_callback : "setTinyMCEdirty",
        plugin_insertdate_dateFormat : "%A, %B %d, %Y",
        plugin_insertdate_timeFormat : "%I:%M %p",
        tab_focus : ':prev,:next'
    }); 
</script>     

<script language="javascript">
	function setUserList(text,id) {
		var i, x;
		
		//used by child window to add selected users
		var el = document.getElementById('listUser');		
		el.options[el.length] = new Option(text, id, false);

		//add to users to hidden element for adding to CRS_Logins table
		var addE = document.getElementById('listUserAdded');
		addE.value = id + "," + addE.value;

		//check and see if value being added is in Removed list
		var delE = document.getElementById('listUserRemoved');
		delE.value = delE.value.replace(new RegExp(id + ",", "gi"),"");
						
	}

	function removeUser(obj) {
		//used to remove all selected items in an Option/Select list
		var addE = document.getElementById('listUserAdded');
		var delE = document.getElementById('listUserRemoved');
		var el = document.getElementById(obj);		
		var selIndex = el.selectedIndex;
		
		//make sure user was selected
		if (selIndex != -1) {						
			//make sure user agrees to change
			jConfirm('Remove selected user(s) from rule?', 'Confirm Delete', function(r) {
				//user agrees, remove categories
				if (r==true) {
					//cycle through all selected users
					for(i=el.length-1; i>=0; i--)
					{
					  if(el.options[i].selected)
					  {
						//add to Removed User hidden field to delete from CRS_Logins table				
						delE.value = el.options[i].value + "," + delE.value;
						
						//check and see if value being removed is in Added list
						addE.value = addE.value.replace(new RegExp(el.options[i].value + ",", "gi"),"");
											
						//remove fron list listUser
						el.options[i] = null;				
					  }
					}
				}
				//user disagrees
				else {
					return false;
				}
			});							
		}
		//no users selected
		else {
			jAlert('No users were selected.', 'myCM Alert');			
		}
	}	

	function userLookUp(obj) {
		//used by child window to add selected Categories
		var i, x;
		var el = document.getElementById(obj);		
		var selIndex = el.selectedIndex;
		if (selIndex != -1) {

			x=0;
			//cycle through all categories to get selected count
			for(i=el.length-1; i>=0; i--) {
			  if(el.options[i].selected) {					  
				x=x+1;
			  }
			}
			//more that one 1 was choosen so STOP
			if(x>1) {
			  jAlert('More than one user was selected.', 'myCM Alert');
			  return false;
			}
			
			//all is good...process selection of primary			
			for(i=el.length-1; i>=0; i--) {
			  //found selected category
			  if(el.options[i].selected) {
				  
				  //alert(el.options[i].value);
				  SimpleModal.open('../_dialogs/popup_user_record.asp?recid=' + el.options[i].value + '&pageview=<% =pageView %>', 500, 700, 'yes');
			  }	
			}				
			
		//no category selected
		}
		else {
			jAlert('No user was selected.', 'myCM Alert');			
		}
	}
</script>

<script>
	function configDropDownOption(obj,list) {
		//used to rebuild options list with new values
		var el = document.getElementById(obj);
		var selIndex = el.selectedIndex;
		var selectedValue;
		//get current option value selected
		if (selIndex != -1) {
			for(i=el.length-1; i>=0; i--)
			{
			  if(el.options[i].selected)
			  {
				if(el.options[i].value){
					selectedValue = el.options[i].value; //current selection
				}
			  }
			}
		}
			
		//removed current selects in dropdown
		el.length = 0;
				
		//add new values to dropdown
		var i;		
		var temp = list.split("|"); //split list provided by popup_config_options.asp
		//add new options provided by user
		addOptionsItems(obj,"","-- Select --");
		for(i=0; i<temp.length; i++) {
			if (temp[i].length>1) {
				addOptionsItems(obj,temp[i],temp[i]);
			}
		}
		
		//check for existance of originally selected value		
		if(selectedValue){
			var bolMatch = false;
			i = 0;		
			for (i=0; i<el.options.length; i++) {
				if (el.options[i].text == selectedValue) {
					el.selectedIndex = i;
					bolMatch = true; //origianl still exists in list
				}
			}		
			//user removed original value, put back with "Missing:" at end of list
			if (bolMatch == false){
				addOptionsItems(obj,selectedValue,"Missing:"+selectedValue);
				el.selectedIndex = el.options.length-1;
			}
		}
		
	}
</script>

<script language="javascript">
	function filterCheck() {	
		var elSel 				//select element
		var errFound = false;	//retunred value
		var el = document.frm.elements.length;	//all elements in form
		//cycle though all form elements
    	for (i=0; i<el; i++) {
			//form element is a query field
			if (document.frm.elements[i].id.indexOf("idQueryValue_") > -1) {							
				//pull out ### on ID
				var elNbr = document.frm.elements[i].id.substring(document.frm.elements[i].id.indexOf("_")+1, document.frm.elements[i].id.length);
				//using number in elNbr find coresponding dropdown with query expression
				var elSel = document.getElementById('idQueryExp_' + elNbr);
				//check existance of select element
				if (document.getElementById('idQueryExp_' + elNbr)) {
					//assign select element
					elSel = document.getElementById('idQueryExp_' + elNbr);
					//keep going if expression is NOT null or is null
					if (elSel.options[elSel.selectedIndex].value != 'is null' && elSel.options[elSel.selectedIndex].value != 'is not null') {
						//check for NULL or BLANK value
						if (!document.frm.elements[i].value) {
							//set to yellow
							document.frm.elements[i].style.background = '#FFFF9F';
							//inform user of error
							jAlert('<strong>Filter Error!</strong><br/><br/>Please ensure all query filters have a value.', 'myCM Alert');
							errFound = true;
						}
						else {
							if (document.frm.elements[i].style.background == '#FFFF9F') {
								//set to white
								document.frm.elements[i].style.background = '#FFF';
							}
						}					
					}				
				}				
			}
		}	
		return errFound;
	}
</script>
