<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->

<%
'Database
dim mySQL, cn, rs, rs2

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			
			
'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("customerID")
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************
			
'Get action
dim action, recId
action = trim(lCase(Request.Form("action")))
if len(action) = 0 then
	action = trim(lCase(Request.QueryString("action")))
end if
if  action <> "edit" _
and action <> "add" _
and action <> "del" _
and action <> "emailtest" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if

'Get ID of location to work with
if action = "edit" _
or action = "del" _
or action = "emailtest" then
	recId = trim(Request.Form("optEmailID"))
	if recId = "" then
		recId = trim(lCase(Request.QueryString("recid")))
	end if	
	if recId = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Template ID.")
	end if
end if

'make sure user can update this record
if programAdmin = "N" and cLng(session(session("siteID") & "adminLoggedOn")) > 3 then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if



'used to redirect user back to tabs they last were on
dim topTab, sideTab
topTab = trim(Request.Form("topTab"))
if len(topTab) = 0 then
	topTab = trim(Request.QueryString("top"))
end if
sideTab = trim(Request.Form("sideTab"))
if len(sideTab) = 0 then
	sideTab = trim(Request.QueryString("side"))
end if

'Rule fields in DETAILS tab
dim optEmailID
dim optName
dim optViewCaseNotes
dim optViewCaseUserFields
dim optEditCaseMgr
dim optEditOwnerNotes
dim optEditCaseResolution
dim optEditCaseResolutionApp
dim optEditCaseUserFields
dim optViewAssignedInvOnly

dim optType, arrType, expI

dim optFilter
dim optTemplate
dim optEvent

'ADD FILTER
dim arrField
dim arrExp
dim arrValue
dim tmpValue
dim i
	
'flag right now as when this issue was modified
dim modifiedDate : modifiedDate = Now()

'Get Rule Details
if action = "edit" or action = "add" or action = "emailtest" then

	optName = trim(Request.Form("optName"))
	if optName = "" or isNull(optName) then
		'for AJax display
		if action = "edit" then
			response.write("Error: Invalid Template Name.")
			response.end 
		'all others, redirect
		else
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Template Name.")		
		end if
	end if

	optTemplate = trim(Request.Form("optTemplate"))			
	optEvent = trim(Request.Form("optEvent"))
	
end if


'EDIT
if action = "emailtest" then

	'***************************************
	'Get global FROM EMAIL address for emails
	'	-this is necessary as some email servers see the FROM address as user@client.com
	'	 and the sending domain as "ccius.com" and consider it SPOOFING and block the email.	
	'***************************************	
	mySQL = "SELECT appSendFromEmail FROM Customer " _
		  & "	WHERE Customer.CustomerID='" & customerID & "' "
	set rs = openRSopenAjax(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	'get from address
	if len(rs("appSendFromEmail")) > 0 then emailFrom = rs("appSendFromEmail") else emailFrom = email

	'not doing this just yet, may not need to ever
	'optTemplate = replace(optTemplate,"|CRS.Name|","1201-DEMO-10001-01")
	'|CRS.Name|
	'|Logins.Email| Assigned user's e-mail address 
	'|URL.Link| Valid URL link to referenced issue 
	'|CRS.Summary| Issue summary 
	'|CRS.Status| Issue status found under Case Notes 
	'|Investigation.Name| Name given to investigation 
	'|Investigation.Manager| Investigator assigned to investigation 
	'|Investigation.Status| Ivenstigation status 
	'|Investigation.Category| Category linked to investigation 
  	
	'create email
	mySQL = "INSERT INTO MailLog (" _
		  & "CustomerID, mailDate, fromEmail, toEmail, subject, body, conttype" _
		  & ") VALUES (" _
		  & "'" & customerID & "'," _
		  & "'" & Now() & "'," _
		  & "'" & emailFrom & "'," _
		  & "'" & email & "'," _
		  & "'" & "TEST E-MAIL: " & optName & "'," _
		  & "'" & replace(optTemplate,"'","''") & "'," _
		  & "'" & "HTML" & "'" _
		  & ")"
	'for AJax display execute update
	set rs = openRSexecuteAjax(mySQL)
	
	response.write("Test email has been queued.")

	'close database connection
	call closeDB()
				
end if


'ADD
if action = "add" then

	dim fieldsSQL, valueSQL

	'Add Issue with Callback
	fieldsSQL = "INSERT INTO [Email_Template] (" _
		  & "CustomerID,Name,Template,Event,ModifiedBy,ModifiedDate"
	fieldsSQL = fieldsSQL & ") "
		  
	valueSQL = " VALUES (" _
		  & "'"    	& uCase(customerID)				& "'," _
		  & "'"    	& replace(optName,"'","''")		& "'," _
		  & "'"    	& replace(optTemplate,"'","''") 	& "'," _
		  & "'"    	& replace(optEvent,"'","''")	& "'," _
		  & " "    	& sLogid						& ", " _
		  & "'"    	& modifiedDate					& "' "
	valueSQL = valueSQL & ") "

	'set identity to pull back newly added record new record id is returned to newID
	mySQL = "SET NOCOUNT ON; " & fieldsSQL & valueSQL & " SELECT SCOPE_IDENTITY() AS newID;"
	set rs = openRSexecute(mySQL)		
	recId = rs("newID")		  				

	session(session("siteID") & "okMsg") = "Template was Added."
	response.redirect "email_edit.asp?action=edit&recID=" & recId & "&cid=" & customerID & "&top=" & topTab & "&side=" & sideTab
	
end if


'EDIT
if action = "edit" then
		
	'Update Record
	mySQL = "UPDATE [Email_Template] SET " _	
		  & "Name='"   			& replace(optName,"'","''")		& "'," _
		  & "Template='"    	& replace(optTemplate,"'","''")	& "'," _
		  & "Event='"     		& replace(optEvent,"'","''")	& "'," _
		  & "ModifiedBy="  		& sLogid						& ", " _
		  & "ModifiedDate='"	& modifiedDate					& "' "
		  
	'finalize update query
	mySQL = mySQL & "WHERE EmailTempID = '" & recId & "' "

	'for AJax display execute update
	set rs = openRSexecuteAjax(mySQL)
	response.write("Template has been saved successfully.")

	'close database connection
	call closeDB()

end if


'DELETE
if action = "del" then

	'Delete records from optionsGroupsXref
	mySQL = "DELETE FROM [Email_Template] " _
	      & "WHERE EmailTempID = " &  recId & " "
	set rs = openRSexecute(mySQL)
	
	session(session("siteID") & "okMsg") = "Template was deleted."
	response.redirect "email.asp?cid=" & customerID
	
end if


'just in case we ever get this far...and NOT...called by action=edit
if action <> "edit" and action <> "emailtest" then
	call closeDB()
	Response.Redirect "email.asp"
end if

%>
