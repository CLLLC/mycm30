<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = false
const showDHTMLMenu = true
const helpID = 6
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "locations"
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and programAdmin = "N" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if

dim I
dim item
dim pageSize
dim totalPages
dim showArr
dim sortField
dim sortOrder
dim curPage

dim action
dim tableProperty

dim showCRSID

dim tempHierarchy

dim rowCount
dim bolRecordsFound
dim xCol
dim count
dim field
dim colCount
dim columnSQL


'Query fields
dim showSEARCH
dim showField
dim showValue
dim showType


'Get Page to show
curPage = Request.Form("curPage")
if len(curPage) = 0 then
	curPage = Request.QueryString("curPage")
end if

'Get showField from Dynamic DropDowns
showType = Request.Form("showType")
if len(showType) = 0 then
	showType = Request.QueryString("showType")
end if

showSEARCH = Request.Form("showSEARCH")
if len(showSEARCH) = 0 then
	showSEARCH = Request.QueryString("showSEARCH")
else
	'form search, reset to page 1
	curPage = 1
end if

'put search request in session
if len(showSEARCH) = 0 then
	session(session("siteID") & "searchBox") = ""
else
	session(session("siteID") & "searchBox") = showSEARCH
end if

'Get showField from Dynamic DropDowns
showField = Request.Form("showField")
if len(showField) = 0 then
	showField = Request.QueryString("showField")
else
	'form search, reset to page 1
	curPage = 1
end if

'Get showValue from Dynamic DropDowns
showValue = Request.Form("showValue")
if len(showValue) = 0 then
	showValue = Request.QueryString("showValue")
else
	'form search, reset to page 1
	curPage = 1
end if

'Check what we will be sorting the results on
sortField = Request.Form("sortField")
if len(sortField) = 0 then
	sortField = Request.QueryString("sortField")
end if
if len(sortField) = 0 or IsNull(sortField) or sortField = "" then
	sortField = "name"
end if

'Get sortOrder
sortOrder = Request.Form("sortOrder")
if len(sortOrder) = 0 then
	sortOrder = Request.QueryString("sortOrder")
end if
if len(sortOrder) = 0 or IsNull(sortOrder) or SortOrder = "" then
	sortOrder = "asc"
end if


'Set Row Colors
dim foreColor, fontWeight
dim rowColor, col1, col2,rowSet
col1 = "#F0F0F0"
col2 = "#FFFFFF"

'Set Number of Items per Page
pageSize = 25

%>

<% dim pageTitle : pageTitle = "Locations" %>
<!--#include file="../_includes/_INCheaderLite_.asp"-->

<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
        
        <!-- START Left sided area -->
		<td width="190" align="right" valign="top">

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided tree menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">                

                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->        
                    <% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>      
	                    <script>
							stExpandSubTree('myCMtree',1,0)
       		            </script>
            		<% else %>
	                    <script>
							stCollapseSubTree('myCMtree',1,0);
							stCollapseSubTree('myCMtree',2,0);							
       		            </script>
                    <% end if %>                          
                    <!-- STOP side menu include -->
                    
                </td>
              </tr>
              <!-- END Left sided tree menu -->
        
            </table>
        
        </td>
        <!-- STOP Left sided area -->
            
	    <!-- START Main Middle Section -->
		<td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">       

            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs());
       	        </script>
            </div>
            
            <div style="text-align:left; margin-bottom:10px;">				
                <div style="float:left; padding-right:2px; margin-right:2px;"><img src="../_images/icons/32/globe.png" title="" width="32" height="32" align="absmiddle" style="vertical-align:middle;"></div>
                <div>
                	<span class="pageTitle">Locations</span>
                	<br/>:: <a href="../profiles/profiles_menu.asp?cid=<% =customerID %>"><% response.write( getCustomerName(customerID) ) %></a>
                </div>
            </div>                   
                                          
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
                        <legend>Information</legend>
                        <div align="left">
	                        <img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; vertical-align:middle;">Below is a list of locations. Click 'Name' to view/modify record.
							<% response.write( getInlineHelp("getPageURL", 0) ) %>
                        </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             

                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">
                        <div class="infoButtons">
                            <a href="location_edit.asp?action=add&cid=<% =customerid %>" ><img src="../_images/icons/24/globe_add.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                            <a href="location_edit.asp?action=add&cid=<% =customerid %>" >Add New</a>
                        </div>
                        
                        <% 
						'CAN TURN ON FOR CUSTOMER...after used by CCI for awhilte
						if cLng(session(session("siteID") & "adminLoggedOn")) <= 2 then %>
                            <div class="infoButtons">
                                <a href="#" onClick="javascript:parent.location='location_import.asp?cid=<% =customerid %>'; return false;"><img src="../_images/icons/24/table_import.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                <a href="#" onClick="javascript:parent.location='location_import.asp?cid=<% =customerid %>'; return false;">Import</a>
                            </div>                                                                   
                        <% end if %>                        
                        
                        <div class="infoButtons">
                            <a href="#" onClick="javascript:parent.location='../reports/reports_export.asp?pageview=profile&view=vwProfileLocation&cid=<% =customerid %>&action=csv'; return false;"><img src="../_images/icons/24/file_extension_xls.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                          	<a href="#" onClick="javascript:parent.location='../reports/reports_export.asp?pageview=profile&view=vwProfileLocation&cid=<% =customerid %>&action=csv'; return false;">Export (.csv)</a>
                        </div>
                    </td>
                    <!-- STOP Information buttons -->
                                                                                
                </tr>
            </table>        
            <!-- STOP information box -->
    

    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			'clear system messages
			call clearSessionMessages()
            %>            
    		<!-- STOP message notification box -->



            <!-- START Reports table -->
            <%
			'open location table...put Location.LocationID LAST in the MIDDLE of the list
			mySQL = "SELECT Location.Name, Location.LocationID, " _
				  & "		Location.Address, Location.City, Location.State, Location.Country, " _
				  & "		Location.Level_1, Location.Level_2 " _
				  & "	FROM Location " _
				  & "	WHERE CustomerID = '" & customerID & "' "
			
			'----------------------------------------------------
			'BEGIN retrieving record results and setup counts
			'----------------------------------------------------
				
			dim mySQLCount, mySQLStart, mySQLStop
		
			'get total record count for page and record counts ONLY pull out FROM statement from mySQL
			mySQLCount = "SELECT Count([LocationID]) AS ReturnCount " & mid(mySQL,inStr(1,mySQL,"FROM"),len(mySQL))
			set rs = openRSopen(mySQLCount,0,adOpenStatic,adLockReadOnly,adCmdText,0)		
			rowCount = rs("ReturnCount")
				
			'check for records returned
			if rowCount <= 0 then
				bolRecordsFound = False
				curPage = 0
				totalPages = 0
				mySQLStart = 1
				mySQLStop = 25							
			else
			
				'records were found
				bolRecordsFound = True
				
				'set total page count
				curPage = formatNumber(curPage,0)
				
				'calculate total pages
				totalPages = (rowCount/pageSize) - int(rowCount/pageSize)		
				if totalPages > 0 and int(rowCount/pageSize) > 0 then 
					totalPages = formatNumber(int(rowCount/pageSize)+1,0) 			
				else 		
					totalPages = formatNumber(rowCount/pageSize,0)		
				end if
				if cLng(totalPages) <= 0 then totalPages = 1
				
				'set current page
				if cLng(curPage) <= 0 then
					curPage = 1
				elseif cLng(curPage) > cLng(totalPages) then 
					curPage = totalPages
				end if
				
				'set SQL start and stop
				if cLng(curPage) = 1 then 			
					mySQLStart = 1
					mySQLStop = 25								
				elseif curPage = totalPages then 
					mySQLStart = ((curPage*pageSize)-pageSize)+1
					mySQLStop = rowCount								
				else
					mySQLStart = ((curPage*pageSize)-pageSize)+1
					mySQLStop = curPage*pageSize
				end if
				
			end if    					
			
			'----------------------------------------------------
			'PULL actual records
			'----------------------------------------------------
			dim mySQLOrder
			mySQLOrder = " ORDER BY " & sortField & " " & sortOrder	
		
			mySQL = "SELECT MyDerivedTable.* " _
				  & "FROM (" & replace(mySQL,"FROM",", ROW_NUMBER() OVER (" & mySQLOrder & ") AS RowNum FROM") & ") AS MyDerivedTable " _
				  & "WHERE MyDerivedTable.RowNum BETWEEN " & mySQLStart & " AND " & mySQLStop & " "		  

'response.redirect "../error/default.asp?errMsg=" & server.URLEncode(mySQL)
			
			'execute recordset
			set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,0)		


					
			%>
			<!-- STOP Location Discovery in SQL -->                


            <!-- START Grid Header and Pagination -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <form method="post" action="location_large.asp<% =navQueryStr(curPage) %>" name="formIssuesPaging">        
                    <td align="left" valign="top" style="background:#f4f4f4 url(../_images/bg_infDiv.jpg) left bottom repeat-x; height:23px; border:1px solid #ccc;">
        
                    <div style="float:left; width:180px; position:inherit; text-align:left; padding-left:7px; padding-top:1px;">	        	
                        <%
                        'CLEAN THIS UP LATER...DON'T THINK I NEED ALL THIS CODE FOR PAGE DISPLAY				
                        if cLng(curPage) = 1 then
                            if rowCount > pageSize then
                                response.write("Rows: 1-" & formatNumber(curPage*pageSize,0) & " / " & formatNumber(rowCount,0))
                            else
                                'number of rows <= record count (i.e. 1-10 / 10)
                                response.write("Rows: 1-" & formatNumber(rowCount,0) & " / " & formatNumber(rowCount,0))
                            end if
                        elseif curPage = TotalPages then
                            if bolRecordsFound = True then				
                                response.write("Rows: " & formatNumber((((curPage*pageSize)-pageSize)+1),0) & "-" & formatNumber(rowCount,0) & " / " & formatNumber(rowCount,0))					
                            else
                                'no records found curPage=0, TotalPages=0 
                                response.write("Rows: 0-0 / 0")					
                            end if						
                        else
                            response.write("Rows: " & formatNumber((((curPage*pageSize)-pageSize)+1),0) & "-" & formatNumber(curPage*pageSize,0) & " / " & formatNumber(rowCount,0))
                        end if				
                        %>
                    </div>
                    
                    <div style="float:left; width:158px; position:inherit; text-align:left; padding:1px 0; border-left:1px solid #ccc; height:22px;">&nbsp;</div>
        
                    <!-- START Pagination -->
                    <div style="float:left; position:inherit; text-align:center; padding-left:38px; padding-top:3px; border-left:1px solid #ccc; height:22px;">
                        <%        
                        'move backwards
                        response.write("<a href=""location_large.asp" & navQueryStr(curPage-1) & """ class=""pagerLink""><img src=""../_images/btn_previous_page.png"" width=""19"" height=""19"" border=""0"" align=""absmiddle"" style=""vertical-align:middle;"" /></a>")
                        'move to first page
                        response.write("<a href=""location_large.asp" & navQueryStr(1) & """ class=""pagerLink""><img src=""../_images/btn_first_page.png"" style=""margin-left:2px; margin-right:2px; vertical-align:middle;"" width=""19"" height=""19"" border=""0"" align=""absmiddle"" /></a>")
                        %>                            
                        Page <input name="curPage" style="width:25px; height:12px; font-size:8pt; vertical-align:middle; text-align:center; margin-left:6px;" value="<% =curPage %>"> of&nbsp;&nbsp;<% =TotalPages %>                
                        <%        
                        'move to last page
                        response.write("<a href=""location_large.asp" & navQueryStr(TotalPages) & """ class=""pagerLink""><img src=""../_images/btn_last_page.png"" style=""margin-left:6px; margin-right:2px; vertical-align:middle;"" width=""19"" height=""19"" border=""0"" align=""absmiddle"" /></a>")
                        'move forwards
                        response.write("<a href=""location_large.asp" & navQueryStr(curPage+1) & """ class=""pagerLink""><img src=""../_images/btn_next_page.png"" width=""19"" height=""19"" border=""0"" align=""absmiddle"" style=""vertical-align:middle;"" /></a>")
                        %>           
                    </div>
                    <!-- STOP Pagination -->
                    
                    <div style="float:right; position:inherit; text-align:right; padding-top:2px; height:22px; padding-right:3px;">
                        <a href="location_large.asp?resetCookie=1&curPage=1" class="pagerLink" style="padding-right:2px;"><img src="../_images/icons/12/btn_filter.png" width="50" height="16" border="0" /></a>
                    </div>
        
                    </td>
                  </form>
                </tr>
            </table>
            <!-- STOP Grid Header and Pagination -->        
        
        
            <!-- START Location Table -->
            <%                      

			'retrieve column count		
			colCount = rs.Fields.Count
			
            response.write("<table width=""100%"" id=""tableIssues"" cellpadding=""0"" cellspacing=""0"">")
        
            'no records found!
            if bolRecordsFound = False then
                response.write("<tr><td align=center valign=middle>&nbsp;</td></tr>")
                response.write("<tr><td align=center valign=middle style=""padding-right:2px;"">")
				call systemMessageBox("locationID","statusMessageINFO","No locations found. Click ""Add Location"" above.")
                response.write("</td></tr>")
                  
            'records found...build table
            else
        
				'set alternating row colors
				rowColor = col2
	
				'Reset count
				xCol = 0
				'set starting count
				count = 0 'was 1
	
				'*******************************************
				'loop through all records found
				'*******************************************
				do while not rs.eof
	
					'Switch Row Color
					if rowColor = col2 then rowColor = col1 else rowColor = col2
	
					foreColor = "color:#333333;"
					
					fontWeight = "font-weight:normal;"				
												
					'start new row
					response.write("<tr valign=""middle"">")
					
					'loop through all columns
					for each field in rs.fields
		
						'*******************************************
						'build/print column HEADER(s) (count = row#)
						'*******************************************
						if count = 0 then
	
							'first column
							if xCol = 0 then 
								'ICONs...
								response.write("<td class=""listRowTop"" nowrap=""nowrap"" style=""border-left:none;"" width=""39"">&nbsp;</td>")

								'Location Name...
								response.write("<td class=""listRowTop"" nowrap=""nowrap"" width=""10%"" align=""left"" style=""cursor:pointer;"" ") 
									response.write("onClick=""window.location='location_large.asp?cid=" & customerid &"&sortField=name&sortOrder=")
									if sortField="name" and sortOrder="asc" then 
										response.write("desc") 
									else 
										response.write("asc")								
									end if
									response.write("&showSEARCH=" & server.URLEncode(showSEARCH) & "'"">")
									response.write("Name")
									checkSortImg("name")							
								response.write("</td>")
														
							end if
	
							'don't do anything for these headers
							if lCase(field.name) <> "customerid" and _
							   lCase(field.name) <> "name" and _
							   lCase(field.name) <> "locationid" and _
							   lCase(field.name) <> "rownum" then
		
								'LAST column header, different class setting
								if xCol = (colCount - 1) then
									response.write("<td class=""listRowTopEnd"" nowrap=""nowrap"" align=""left"" style=""cursor:pointer;"" ") 
								'print header
								else							   
									response.write("<td class=""listRowTop"" nowrap=""nowrap"" align=""left"" style=""cursor:pointer;"" ") 								
								end if
	
								response.write("onClick=""window.location='location_large.asp?cid=" & customerid &"&sortField=[" & field.name & "]&sortOrder=")
								if sortOrder="asc" then response.write("desc") else response.write("asc")
								response.write("&showSEARCH=" & server.URLEncode(showSEARCH) & "'"">")															
								response.write(replace(field.name,"_", " "))
								checkSortImg("[" & field.name & "]")
								response.write("</td>")
	
							end if
	
						'*******************************************
						'build/print field DATA value(s)
						'*******************************************
						else
						
							'first column of data row
							if xCol = 0 then
	
								'ICONs...
								response.write("<td style=""background-color:" & rowColor & "; " & foreColor & " " & fontWeight & " padding-left:5px; padding-right:5px; border-left:#969696 solid 1px; border-bottom:#969696 solid 1px;"" height=""20"" width=""39"" align=""center"" nowrap>")
									response.Write("<a href=""#"" onClick=""confirmPrompt('Confirm Delete','This action can not be undone.<br><br>Are you sure you want to delete this location?','redirect','location_exec.asp?recID=" & rs("locationid") & "&action=del&cid=" & customerid & "'); return false;"" ><img src=""../_images/icons/16/cancel.png"" title=""Delete Location"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:7px; vertical-align:middle;"" /></a>")
									response.Write("<a href=""location_edit.asp?action=edit&recid=" & rs("locationid") & "&cid=" & customerid & """ ><img src=""../_images/icons/16/page_blue_edit.png"" title=""Open Location"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; vertical-align:middle;""></a>")
								response.write("</td>")              
		
								'CRSID...clickable TD to issues_edit.asp
								response.write("<td width=""10%"" height=""30"" align=""left"" nowrap style=""background-color:" & rowColor & "; " & foreColor & " " & fontWeight & " padding:0px; border-bottom:#969696 solid 1px; cursor:pointer;"" onClick=""window.location='location_edit.asp?cid=" & customerID & "&action=edit&recid=" & rs("locationid") & "'"">")
								response.write("<a style=""" & foreColor & " " & fontWeight & """>" & highlight(rs("name") & "&nbsp;", showSEARCH) & "</a>")
								response.write("</td>")
	
							end if
																				
							'don't do anything for these headers included in SQL
							if lCase(field.name) <> "customerid" and _
							   lCase(field.name) <> "name" and _
							   lCase(field.name) <> "locationid" and _
							   lCase(field.name) <> "rownum" then
			
								'start TD for row....for LAST column add different class setting
								if xCol = (colCount - 2) then
									response.write("<td align=""left"" style=""background-color:" & rowColor & "; " & foreColor & " " & fontWeight & " padding:0px; border-bottom:#969696 solid 1px; border-right:#969696 solid 1px;"" height=""20"">")
								else						
									response.write("<td align=""left"" style=""background-color:" & rowColor & "; " & foreColor & " " & fontWeight & " padding:3px; border-bottom:#969696 solid 1px;"" height=""20"">")								
								end if							
	
								'show/print nulls with empty space
								if isNull(rs(field.name)) or rs(field.name) = "" then
									response.write "&nbsp;"								
								'show/print values
								else
										
									'check if field is MEMO/MAX field and truncate if necessary
									if (field.type = adLongVarWChar) then
										if (len(rs(field.name)) > 150) then
											response.write( highlight( left( stripHTML(rs(field.name)),150) & "...", showSEARCH) )
										else
											response.write( highlight(stripHTML(rs(field.name)), showSEARCH) ) 
										end if
											
									'all good...print as is
									else
										response.write( highlight(rs(field.name), showSEARCH) ) 
									end if									
																	
								end if
								
								'close TD for row
								response.write("</td>")
	
							end if							
											
						end if
						
						xCol = xCol + 1
						'Reset for next row
						if xCol > (colCount - 1) then
							xCol = 0
						end if
						
					next
					
					Response.Write "</tr>"
					
					'Move to next record
					if count > 0 then
						rs.movenext
					end if
					count = count + 1
				
				
				'*******************************************
				'loop through all records returned
				'*******************************************
				loop		
	
				'shadow row
				response.write("<tr><td colspan=""" & colCount-1 & """ style=""background:#D0D0D0"" height=""5px;""><img src=""../_images/x_cleardot.gif"" width=""1"" height=""1""></td></tr>")
			
            'main IF regarding were any records found?
            end if
            
            'close grid table		
            response.write "</table>"
            %>
        
            <!-- END Issues Table -->    
            
        </td>
    	<!-- STOP Main Middle Section -->

    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
  	</tr>
</table>
<!-- STOP Page -->

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<%
'*********************************************************************
'Make QueryString for Paging
'*********************************************************************
function navQueryStr(pageNum)

dim tmpField, tmpValue
    
	navQueryStr = "?curPage="		& server.URLEncode(pageNum) _
				& "&cid="			& customerid _
                & "&showSEARCH="	& server.URLEncode(showSEARCH) _
                & "&showField="		& server.URLEncode(showField) _
                & "&showValue="		& server.URLEncode(showValue) _
                & "&sortOrder="		& server.URLEncode(sortOrder) _
                & "&sortField="		& server.URLEncode(sortField)
				
end function
%>        

<script language="javascript">	
	function pageReload(cid) {  	
    	window.location = '../profiles/location_large.asp?cid='+cid;
    }
	//used to navigate elsewhere
	function navigateAway(url) {
		window.location = url;
	}		
</script>	

