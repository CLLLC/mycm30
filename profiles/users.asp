<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = false
const showDHTMLMenu = true
const helpID = 6
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "users"
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

dim action
dim tableProperty

'Work Fields
dim I
dim item
dim pageSize
dim totalPages
dim showArr
dim sortField
dim sortOrder
dim curPage

dim showCRSID
dim tempFullName

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and programAdmin = "N" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if


dim tempHierarchy
%>

<% dim pageTitle : pageTitle = "Users" %>
<!--#include file="../_includes/_INCheaderLite_.asp"-->
  
<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
        
        <!-- START Left sided area -->
		<td width="190" align="right" valign="top">

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="users_edit.asp?action=edit&recID=<% =sLogid %>&pageview=users" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided tree menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">                

                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->        
                    <% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>      
	                    <script>
							stExpandSubTree('myCMtree',1,0)
       		            </script>
            		<% else %>
	                    <script>
							stCollapseSubTree('myCMtree',1,0);
							stCollapseSubTree('myCMtree',2,0);							
       		            </script>
                    <% end if %>                          
                    <!-- STOP side menu include -->
                    
                </td>
              </tr>
              <!-- END Left sided tree menu -->
        
            </table>
        
        </td>
        <!-- STOP Left sided area -->
            
	    <!-- START Main Middle Section -->
		<td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">       

            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs());
       	        </script>
            </div>
            
            <div style="text-align:left; margin-bottom:10px;">				
                <div style="float:left; padding-right:2px; margin-right:2px;"><img src="../_images/icons/32/user.png" title="" width="32" height="32" align="absmiddle" style="vertical-align:middle;"></div>
                <div>
                	<span class="pageTitle">Users</span>
                	<br/>:: <a href="../profiles/profiles_menu.asp?cid=<% =customerID %>"><% response.write( getCustomerName(customerID) ) %></a>
                </div>
            </div>                   
                                          
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
                        <legend>Information</legend>
                        <div align="left">
	                        <img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; vertical-align:middle;">Below is a list of users. Click 'Name' to view/modify record.
							<% response.write( getInlineHelp("getPageURL", 0) ) %>
                        </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             

                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">
                        <div class="infoButtons">
                            <a href="users_edit.asp?action=add&cid=<% =customerid %>&pageview=users" ><img src="../_images/icons/24/user_add.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                            <a href="users_edit.asp?action=add&cid=<% =customerid %>&pageview=users" >Add New</a>
                        </div>
                        <div class="infoButtons">
							<a href="#" onClick="SimpleModal.open('../_dialogs/popup_user_copy.asp?cid=<% =customerID %>&action=copy', 500, 600, 'yes'); return false;"><img src="../_images/icons/24/drive_user.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                            <a href="#" onClick="SimpleModal.open('../_dialogs/popup_user_copy.asp?cid=<% =customerID %>&action=copy', 500, 600, 'yes'); return false;">Copy User</a>
                        </div>
                        <div class="infoButtons">
                            <a href="#" onClick="javascript:parent.location='../reports/reports_export.asp?pageview=profile&view=vwProfileLogins&cid=<% =customerid %>&action=csv'; return false;"><img src="../_images/icons/24/file_extension_xls.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                          	<a href="#" onClick="javascript:parent.location='../reports/reports_export.asp?pageview=profile&view=vwProfileLogins&cid=<% =customerid %>&action=csv'; return false;">Excel (.csv)</a>
                        </div>                                                                   
                    </td>
                    <!-- STOP Information buttons -->
                                                                                
                </tr>
            </table>        
            <!-- STOP information box -->
    

    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			'clear system messages
			call clearSessionMessages()
            %>            
    		<!-- STOP message notification box -->

            <!-- START Reports table -->
            <%
			'================================================
			'get users from LOGINS table
			'================================================
			mySQL = "SELECT Logins.LogID, Logins.FirstName, Logins.LastName, Logins.Email, Logins.Priority, Logins.Delivery, " _
				  & "		CASE CAST(SecurityLevel AS varchar(2)) WHEN NULL THEN '-- [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '10' THEN 'DB Admin' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '20' THEN '--' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '30' THEN 'Administrator' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '40' THEN 'Standard' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '50' THEN 'Restricted' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '60' THEN 'Disabled' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' ELSE '-- [' + CAST(SecurityLevel AS varchar(2)) + ']' END AS [ProgramAdmin], " _
				  & " 		CASE Active WHEN 'Y' THEN 'Enable' WHEN 'C' THEN 'Contact' ELSE 'Disabled' END AS [Active], Logins.LoginDate AS [Last Login], " _
				  & "		Logins.CustomerID " _
				  & "	FROM Logins " _
				  & "	WHERE CustomerID = '" & customerID & "' "

			'DBAdmin ONLY
			if cLng(session(session("siteID") & "adminLoggedOn")) < 2 then
				'do nothing...
			'CCI Admins
			elseif cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
				mySQL = mySQL & " AND CASE Active WHEN 'Y' THEN 'Enable' WHEN 'C' THEN 'Contact' ELSE 'Disabled' END <> 'Disabled' "				
			'CCI RA/RS
			elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
				mySQL = mySQL & " AND CASE Active WHEN 'Y' THEN 'Enable' WHEN 'C' THEN 'Contact' ELSE 'Disabled' END <> 'Disabled' AND SecurityLevel>10 AND Priority>' ' "
			'all others
			else
				mySQL = mySQL & " AND CASE Active WHEN 'Y' THEN 'Enable' WHEN 'C' THEN 'Contact' ELSE 'Disabled' END <> 'Disabled' AND SecurityLevel>10 "
			end if

			'================================================
			'================================================
			'UNION QUERY - users linked from other profiles
			mySQL = mySQL & " UNION ALL "
			'================================================		
			'================================================
		
			'get users from linked GROUPS
			mySQL = mySQL & "SELECT     Logins.LOGID, Logins.FirstName, Logins.LastName, Logins.Email, Logins.Priority, Logins.Delivery, " _
				  		  & "			CASE CAST(SecurityLevel AS varchar(2)) WHEN NULL THEN '-- [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '10' THEN 'DB Admin' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '20' THEN '--' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '30' THEN 'Administrator' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '40' THEN 'Standard' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '50' THEN 'Restricted' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '60' THEN 'Disabled' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' ELSE '-- [' + CAST(SecurityLevel AS varchar(2)) + ']' END AS [ProgramAdmin], " _
						  & "			CASE Active WHEN 'Y' THEN 'Enable' WHEN 'C' THEN 'Contact' ELSE 'Disabled' END AS Active, Logins.LoginDate AS [Last Login], " _
						  & "		  	Logins.CustomerID " _
						  & "		FROM 	Logins INNER JOIN Logins_Groups ON Logins.LOGID = Logins_Groups.LOGID INNER JOIN " _
						  & "	  			Groups ON Logins_Groups.GroupID = Groups.GroupID " _
						  & "		WHERE   (Logins.CustomerID <> '" & customerID & "') AND (Groups.CustomerID = '" & customerID & "') "
						  
			'DBAdmin ONLY
			if cLng(session(session("siteID") & "adminLoggedOn")) < 2 then
				'do nothing...
			'CCI Admins
			elseif cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
				mySQL = mySQL & " AND CASE Active WHEN 'Y' THEN 'Enable' WHEN 'C' THEN 'Contact' ELSE 'Disabled' END <> 'Disabled' "				
			'CCI RA/RS
			elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
				mySQL = mySQL & " AND CASE Active WHEN 'Y' THEN 'Enable' WHEN 'C' THEN 'Contact' ELSE 'Disabled' END <> 'Disabled' AND SecurityLevel>10 AND Priority>' ' "
			'all others
			else
				mySQL = mySQL & " AND CASE Active WHEN 'Y' THEN 'Enable' WHEN 'C' THEN 'Contact' ELSE 'Disabled' END <> 'Disabled' AND SecurityLevel>10 "
			end if						  

			'groupBY to prevent DUPLICATES who may be assigned to multiple groups
			mySQL = mySQL & "		GROUP BY 	Logins.LOGID, Logins.FirstName, Logins.LastName, Logins.Email, Logins.Priority, Logins.Delivery, Logins.UserName, Logins.Password, " _
				  		  & "					CASE CAST(SecurityLevel AS varchar(2)) WHEN NULL THEN '-- [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '10' THEN 'DB Admin' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '20' THEN '--' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '30' THEN 'Administrator' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '40' THEN 'Standard' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '50' THEN 'Restricted' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' WHEN '60' THEN 'Disabled' + ' [' + CAST(SecurityLevel AS varchar(2)) + ']' ELSE '-- [' + CAST(SecurityLevel AS varchar(2)) + ']' END, " _
						  & "					CASE Active WHEN 'Y' THEN 'Enable' WHEN 'C' THEN 'Contact' ELSE 'Disabled' END, Logins.LoginDate, Logins.CustomerID "
	
			'================================================
			'ORDER BY  - union query
			'================================================		
			mySQL = mySQL & " ORDER BY Logins.LastName, Logins.FirstName "			 				  
			
	        set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
			
			if not rs.eof then
			%>
			
                <!-- START Location table -->
                <table id="customerTable" width="100%" style="background-color:#969696;" cellpadding=0 cellspacing=0>
                            
                    <thead>
                        <tr>
                            <th align="left" nowrap="nowrap">&nbsp;</th>
                            <th align="left" nowrap="nowrap">Name</th>
                            <th align="left" nowrap="nowrap">E-mail</th>
                            <% if cLng(session(session("siteID") & "adminLoggedOn")) >= 1 and cLng(session(session("siteID") & "adminLoggedOn")) < 5 then %>
                                <th align="left" nowrap="nowrap">Priority</th>                                
                                <th align="left" nowrap="nowrap">Active</th>
								<th align="left" nowrap="nowrap">Security Level</th>                                
                            <% elseif cLng(session(session("siteID") & "adminLoggedOn")) >= 5 and cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>
	                            <th align="left" nowrap="nowrap">Priority</th>
                                <th align="left" nowrap="nowrap">Delivery</th>
                                <th align="left" nowrap="nowrap">Active</th>
                            <% else %>
                                <th align="left" nowrap="nowrap">Active</th>
	                            <th align="left" nowrap="nowrap">Security Level</th>
                                <th align="left" nowrap="nowrap">Last Login</th>                        
                            <% end if %>
                        </tr>
                    </thead>
                    <tbody>
    
                    <%
                    do until rs.eof
                        
						'START current row
                        response.write("<tr>")									
                        
						'ICONS
                        response.Write("<td width=""62"" nowrap=""nowrap"" align=""center"">")
						
						'user linked from other profile
						if lCase(customerID) <> lCase(rs("customerid")) then						
							response.Write("<img src=""../_images/icons/16/lock_disabled.png"" title=""Disable User"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:7px; vertical-align:middle;"" />")						
							response.Write("<img src=""../_images/icons/16/key_go_disabled.png"" title=""User Login"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:7px; vertical-align:middle;"" />")																													
							response.Write("<a href=""#"" onClick=""confirmPrompt('Confirm Edit','<strong>This user is linked from another " & lCase(accountLabelSingle) & ".</strong><br/><br/>If you edit this user you be taken to the<br/>" & lCase(accountLabelSingle) & " associted with this account.<br/><br/>Continue with edit?','redirect','users_edit.asp?action=edit&recid=" & rs("logid") & "&pageview=users&cid=" & customerid & "'); return false;"" ><img src=""../_images/icons/16/group_link.png"" title=""Open User"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; vertical-align:middle;""></a>")
						'all GOOD...user belongs to this profile
						else
							if rs("active") = "Enable" then
								response.Write("<a href=""#"" onClick=""confirmPrompt('Confirm Disable','Are you sure you want to disable this user?','redirect','users_exec.asp?recID=" & rs("logid") & "&action=del&cid=" & customerid & "'); return false;"" ><img src=""../_images/icons/16/lock.png"" title=""Disable User"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:7px; vertical-align:middle;"" /></a>")						
								response.Write("<a href=""#"" onClick=""confirmPrompt('Confirm Login','Login to myCM as this user?','redirect','../logon_ghost.asp?recID=" & rs("logid") & "&action=ghost'); return false;"" ><img src=""../_images/icons/16/key_go.png"" title=""User Login"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:7px; vertical-align:middle;"" /></a>")																	
							elseif rs("active") = "Contact" then
								response.Write("<a href=""#"" onClick=""confirmPrompt('Confirm Disable','Are you sure you want to disable this user?','redirect','users_exec.asp?recID=" & rs("logid") & "&action=del&cid=" & customerid & "'); return false;"" ><img src=""../_images/icons/16/lock.png"" title=""Disable User"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:7px; vertical-align:middle;"" /></a>")					
								response.Write("<img src=""../_images/icons/16/key_go_disabled.png"" title=""User Login"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:7px; vertical-align:middle;"" />")																							
							else
								response.Write("<img src=""../_images/icons/16/lock_disabled.png"" title=""Disable User"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:7px; vertical-align:middle;"" />")						
								response.Write("<img src=""../_images/icons/16/key_go_disabled.png"" title=""User Login"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:7px; vertical-align:middle;"" />")																													
							end if					
							response.Write("<a href=""users_edit.asp?action=edit&recid=" & rs("logid") & "&pageview=users&cid=" & customerid & """ ><img src=""../_images/icons/16/page_blue_edit.png"" title=""Open User"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; vertical-align:middle;""></a>")
						end if						
						
						response.Write("</td>")											
                            
                        'NAME link
						tempFullName = ""
						if len(rs("lastname")) <= 0 or rs("lastname") = "" or isNull(rs("lastname")) then
							tempFullName = rs("firstname")
						elseif len(rs("firstname")) <= 0 or rs("firstname") = "" or isNull(rs("firstname")) then
							tempFullName = rs("lastname")
						elseif (len(rs("lastname")) <= 0 and len(rs("firstname")) <= 0) or (rs("lastname") = "" and rs("firstname") = "") or (isNull(rs("lastname")) and isNull(rs("firstname"))) then
							tempFullName = "<em><empty></em>"
						else
							tempFullName = rs("lastname") & ", " & rs("firstname")
						end if						
						'user linked from another profile
                        if lCase(customerID) <> lCase(rs("customerid")) then						
							response.Write("<td align=""left"" style=""cursor:pointer;"" onClick=""confirmPrompt('Confirm Edit','<strong>This user is linked from another " & lCase(accountLabelSingle) & ".</strong><br/><br/>If you edit this user you be taken to the<br/>" & lCase(accountLabelSingle) & " associted with this account.<br/><br/>Continue with edit?','redirect','users_edit.asp?action=edit&recid=" & rs("logid") & "&pageview=users&cid=" & customerid & "'); return false;"" >")
						else
							response.Write("<td align=""left"" style=""cursor:pointer;"" onclick=""window.location='users_edit.asp?action=edit&recid=" & rs("logid") & "&pageview=users&cid=" & customerid & "'; return false;"" >")
						end if
						response.write("	<a style=""color:#333333;"">" & tempFullName & "</a>")
                        response.Write("</td>")
                        
						'user EMAIL
						response.Write("<td align=""left"">" & rs("email") & "</td>")
												
						'for CCI Admins
						if cLng(session(session("siteID") & "adminLoggedOn")) >= 1 and cLng(session(session("siteID") & "adminLoggedOn")) < 5 then
							response.Write("<td align=""left"">" & rs("priority") & "</td>")							
							response.Write("<td align=""left"">" & rs("active") & "</td>")
							response.Write("<td align=""left"">" & rs("programadmin") & "</td>")							
						'for RA/RS show priority of user NOT program admin status
						elseif cLng(session(session("siteID") & "adminLoggedOn")) >= 5 and cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
							response.Write("<td align=""left"">" & rs("priority") & "</td>")
							response.Write("<td align=""left"">" & rs("delivery") & "</td>")
							response.Write("<td align=""left"">" & rs("active") & "</td>")
						'for Customers
						else
							response.Write("<td align=""left"">" & rs("active") & "</td>")
							response.Write("<td align=""left"">" & rs("programadmin") & "</td>")
							response.Write("<td align=""left"">" & rs("last login") & "</td>")
						end if
						
                        'CLOSE current row						
                        response.write("</tr>")
                        
                        rs.movenext
                    loop
                    %>					
    
                    </tbody>
                </table>
    
                <%
				if cLng(session(session("siteID") & "adminLoggedOn")) >= 1 and cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
					tableProperty = "sort: true, sort_config:{sort_types:['String','String','String','String','String','String']}, filters_row_index: 1, " _
								  & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
								  & "status_bar: true, col_0: """", col_1: ""input"", col_2: ""input"", col_3: ""select"", col_4: ""select"", col_5: ""select"", " _
								  & "col_width:[null,""35%"",null,null,null,null], paging: true, paging_length: 25, " _
								  & "enable_non_empty_option: true, or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, " _
								  & "highlight_keywords: true, " _
								  & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
								  & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"						  
				else
					tableProperty = "sort: true, sort_config:{sort_types:['String','String','String','String','String','String']}, filters_row_index: 1, " _
								  & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
								  & "status_bar: true, col_0: """", col_1: ""input"", col_2: ""input"", col_3: ""select"", col_4: ""select"", col_5: ""input"", " _
								  & "col_width:[null,""35%"",null,null,null,null], paging: true, paging_length: 25, " _
								  & "enable_non_empty_option: true, or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, " _
								  & "highlight_keywords: true, " _
								  & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
								  & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"						  
				end if                
				
				%>
                <!-- STOP Location table -->
    
                <!-- Fire table build -->
                <script language="javascript" type="text/javascript">
                    //<![CDATA[
                    var tableProp = {<% =tableProperty %>};
                    //initiate table setup
                    var tf1 = setFilterGrid("customerTable",tableProp);
                    //]]>
                </script>
    
                <!-- Shadow table -->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="5px" style="background:#D0D0D0;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
                  </tr>
                </table>
        
        	<% else %>
	            <!-- show INFO that NO locations exist -->
                <table width="100%" cellpadding="0" cellspacing="0">
           			<tr>
         				<td align="left" class="clearFormat" style="padding-right:2px;">
                			<% call systemMessageBox("locationID","statusMessageINFO","No users found. Click ""Add User"" above.") %>
                    	</td>
             		</tr>
        		</table>            
            <% end if %>
            
        </td>
    	<!-- STOP Main Middle Section -->

    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
  	</tr>
</table>
<!-- STOP Page -->

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<script language="javascript">	
	function pageReload(cid) {  	
    	window.location = '../profiles/users.asp?cid='+cid;
    }
	function navigateAway(url) {
		window.location = url;
	}		
</script>	


