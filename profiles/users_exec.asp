<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->

<%
'Database
dim mySQL, cn, rs, rs2, cmd

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
if trim(lCase(Request.Form("action"))) <> "passreset" then
	dim sLogid, sCustomerID
	sLogid = session(session("siteID") & "logid")
	sCustomerID = session(session("siteID") & "customerid")			
	
	'validate security access
	if loadUser(null) = false then
		response.redirect ("../default.asp")
	end if
end if
'*********************************************************

dim customerID
dim recId

'User Fields in DETAILS tab
dim userLogID
dim userFirstName
dim userLastName
dim userName

dim userPassword
dim userPassword1
dim userPassword2

dim userProgramAdmin
dim userApproveResolution
dim userNewUserEmail
dim userViewLegal
dim userViewPrivileged
dim userViewNotes
dim userViewStats
dim userViewProfile
dim userViewAppMenu
dim userEditReport
dim userEditNotes
dim userAddReport
dim userPhone
dim userEmail
dim userNotes
dim showSummary
dim sortField
dim sortOrder
dim whereClause
dim userIssuesColumns
dim userLoginDate
dim userViewNotesCase
dim userViewNotesInv
dim userViewNotesMgr
dim userViewNotesRes
dim userDeleteIssues
dim userEmailIssueTypes
dim userViewNotesUFields
dim userViewUsers
dim userviewFileUpload
dim userDataFile
dim userReportsView
dim userExpires
dim userActive
dim userAutosave
dim issuesView

dim userTitle
dim userAddress
dim userCity
dim userState
dim userPostalCode
dim userPhone_Home
dim userPhone_Work
dim userPhone_Cell
dim userPhone_Pager
dim userPhone_Fax
dim userPhone_Other
dim userPriority
dim userDelivery

dim emailAssignment
dim emailStatusChange
dim emailInvAssignment
dim emailInvStatusChange
dim emailDeadlineNotice

dim passwordStrength
dim passwordHistory
dim passwordReset
dim loginAttempts
dim homePage
dim homePageKey
dim securityLevel

dim userEmployee_ID
dim userJob_Class
dim userDivision
dim userRegion
dim userDepartment

'dim userModifiedDate

'Work Fields
dim action

'Get action
action = trim(lCase(Request.Form("action")))
if len(action) = 0 then
	action = trim(lCase(Request.QueryString("action")))
end if
if  action <> "edit" _
and action <> "add" _
and action <> "copy" _
and action <> "passreset" _
and action <> "loadreports" _
and action <> "del" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if


'Get userLogID
if action = "edit"  _
or action = "copy" _
or action = "passreset" _
or action = "loadreports" _
or action = "del" then

	if action = "del" then
		userLogID = trim(lCase(Request.QueryString("recID")))	
	else	
		'ID of issue to work with
		userLogID = trim(Request.Form("userLogID"))
		if userLogID = "" then
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid User ID.")
		end if
	end if
	
end if

'make sure user can update this record
if (programAdmin = "N") and (cLng(userLogID) <> cLng(sLogid)) then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if


'used to redirect user back to tabs they last were on
dim topTab, sideTab
topTab = trim(Request.Form("topTab"))
if len(topTab) = 0 then
	topTab = trim(Request.QueryString("top"))
end if
sideTab = trim(Request.Form("sideTab"))
if len(sideTab) = 0 then
	sideTab = trim(Request.QueryString("side"))
end if


'Get Issue Details
if action = "edit" or action = "add" or action = "copy" then

	'flag right now as when this issue was modified
	'userModifiedDate = Now()
	
	'!!! THIS MUST BE FIRST...other fields are check
	'depending on what value this is
	'user action Y/N
	userActive = trim(Request.Form("userActive"))
	if userActive <> "Y" and userActive <> "C" then
		userActive = "N"
	end if

	'user CustomerID
	customerID = trim(Request.Form("customerID"))
	if len(customerID) <= 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if

	'user first name
	userFirstName = trim(Request.Form("userFirstName"))
	if len(userFirstName) <= 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid First Name.")
	end if
	
	'user last name
	userLastName = trim(Request.Form("userLastName"))
	if len(userLastName) <= 0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Last Name.")
	end if
	
	'user login name
	userName = trim(Request.Form("userName"))
	
	userTitle = trim(Request.Form("userTitle"))
	userAddress = trim(Request.Form("userAddress"))
	userCity = trim(Request.Form("userCity"))
	userState = trim(Request.Form("userState"))
	userPostalCode = trim(Request.Form("userPostalCode"))
	userPhone_Home = trim(Request.Form("userPhone_Home"))
	userPhone_Work = trim(Request.Form("userPhone_Work"))
	userPhone_Cell = trim(Request.Form("userPhone_Cell"))
	userPhone_Pager = trim(Request.Form("userPhone_Pager"))
	userPhone_Fax = trim(Request.Form("userPhone_Fax"))
	userPhone_Other = trim(Request.Form("userPhone_Other"))
	userPriority = trim(Request.Form("userPriority"))
	userDelivery = trim(Request.Form("userDelivery"))	
	userNotes = trim(Request.Form("userNotes"))
	userEmployee_ID = trim(Request.Form("userEmployee_ID"))
	userJob_Class = trim(Request.Form("userJob_Class"))
	userDivision = trim(Request.Form("userDivision"))
	userRegion = trim(Request.Form("userRegion"))
	userDepartment = trim(Request.Form("userDepartment"))

	'fields required when user id ENABLED
	if userActive = "Y" then

		if len(userName) <= 0 then
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Username.")
		else
			if cLng(session(session("siteID") & "adminLoggedOn")) > 1 then
				if inStr(1,lCase(userName),"admin") > 0 then
					response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Username cannot contain 'admin' or 'administrator'.")
				end if
			end if
		end if

		loginAttempts = trim(Request.Form("loginAttempts"))
		if loginAttempts = "" or isNull(loginAttempts) or len(loginAttempts) <= 0 then
			loginAttempts = "0"
		end if
	
		homePage = trim(Request.Form("homePage"))
		if homePage = "" or isNull(homePage) or len(homePage) <= 0 then
			homePage = "issues.asp"
		end if

		homePageKey = trim(Request.Form("homePageKey"))
		if homePageKey <> "Y" then
			homePageKey = "N"
		end if
	
	
		securityLevel = trim(Request.Form("securityLevel"))
		if len(trim(securityLevel))<=0 then
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Security Level.")
		end if
		'set Program Admin accordingly
		if cLng(securityLevel) = 1 or cLng(securityLevel) = 2 or cLng(securityLevel) = 10 or cLng(securityLevel) = 30 then
			userProgramAdmin = "Y"
		else
			userProgramAdmin = "N"		
		end if		
		'set Anaylsis Report access
		if cLng(securityLevel) = 1 or cLng(securityLevel) = 2 or cLng(securityLevel) = 3 or cLng(securityLevel) = 10 or cLng(securityLevel) = 30 or cLng(securityLevel) = 40 then
			userAddReport = "Y"
			userEditReport = "Y"
		else
			userAddReport = "N"
			userEditReport = "N"		
		end if
		'set disable/enable based on security
		if cLng(securityLevel) = 60 then
			userActive = "N"
		end if


		'NOW DONE VIA SECURITY LEVEL!!!!
		'user add report Y/N
		'userProgramAdmin = trim(Request.Form("userProgramAdmin"))
		'if userProgramAdmin <> "Y" then
		'	userProgramAdmin = "N"
		'end if

		'NOW DONE VIA SECURITY LEVEL!!!!
		'user add report Y/N
		'userAddReport = trim(Request.Form("userAddReport"))
		'if userAddReport <> "Y" then
		'	userAddReport = "N"
		'end if

		'NOW DONE VIA SECURITY LEVEL!!!!	
		'user edit report Y/N
		'userEditReport = trim(Request.Form("userEditReport"))
		'if userEditReport <> "Y" then
		'	userEditReport = "N"
		'end if


		'issues user can delete "1,2,3,4"
		userDeleteIssues = trim(Request.Form("list_checkBox"))
		userDeleteIssues = replace(userDeleteIssues," ","")
		
		'user email
		userEmail = trim(Request.Form("userEmail"))
		if len(userEmail) <= "0" then
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid E-mail Address.")
		end if
	
		'issues to email users on "1,2,3,4"
		userEmailIssueTypes = trim(Request.Form("email_checkBox"))
		userEmailIssueTypes = replace(userEmailIssueTypes," ","")
	
		'user emailAssignment Y/N
		emailAssignment = trim(Request.Form("emailAssignment"))
		if emailAssignment <> "Y" then
			emailAssignment = "N"
		end if

		'user emailStatusChange Y/N
		emailStatusChange = trim(Request.Form("emailStatusChange"))
		if emailStatusChange <> "Y" then
			emailStatusChange = "N"
		end if
	
		'user emailInvAssignment Y/N
		emailInvAssignment = trim(Request.Form("emailInvAssignment"))
		if emailInvAssignment <> "Y" then
			emailInvAssignment = "N"
		end if
	
		'user emailInvStatusChange Y/N
		emailInvStatusChange = trim(Request.Form("emailInvStatusChange"))
		if emailInvStatusChange <> "Y" then
			emailInvStatusChange = "N"
		end if

		'user emailInvStatusChange Y/N
		emailDeadlineNotice = trim(Request.Form("emailDeadlineNotice"))
		if emailDeadlineNotice <> "Y" then
			emailDeadlineNotice = "N"
		end if
			
		'user expiration date
		userExpires = trim(Request.Form("userExpires"))
		if len(userExpires) > 0 then
			if not isDate(userExpires) = true then
				response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Expires Date.")
			end if
		end if
	
		'user autosave
		userAutosave = trim(Request.Form("userAutosave"))
		if userAutosave = "" or isNull(userAutosave) or len(userAutosave) <= 0 then
			userAutosave = 0
		end if
	
		'user issuesView
		issuesView = trim(Request.Form("issuesView"))
		if issuesView = "" or isNull(issuesView) or len(issuesView) <= 0 then
			issuesView = "vwIssues"
		end if

		sortField = trim(Request.Form("sortField"))
		if len(sortField) <= 0 or sortField = "" or isNull(sortField) then 
			sortField = "[Date]"
		end if
		
		sortOrder = trim(Request.Form("sortOrder"))
		if len(sortOrder) <= 0 or sortOrder = "" or isNull(sortOrder) then 
			sortOrder = "desc"
		end if
		
		whereClause = trim(Request.Form("WhereClause"))
		if len(whereClause) <= 0 or whereClause = "" or isNull(whereClause) then 
			whereClause = "<?xml version='1.0' encoding='ISO-8859-1'?><filters></filters>"
		end if

		userIssuesColumns = trim(Request.Form("userIssuesColumns"))
		if len(userIssuesColumns) <= 0 or userIssuesColumns = "" or isNull(userIssuesColumns) then 
			userIssuesColumns = "[Issue #],[Date],[Severity],[Status],[Location Name],[Owner],[Caller First Name],[Caller Last Name]"
		end if
		
	'not active, set low security
	else
		'user email
		userEmail = trim(Request.Form("userEmail"))
		'security level
		securityLevel = "60"
		
	end if
							
end if


'Get Issue Details
if ((action = "edit" or action = "add" or action = "copy") and userActive = "Y") or action = "passreset" then

	userName = trim(Request.Form("userName"))

	'user password strenght
	passwordStrength = trim(Request.Form("passwordStrength"))
	'user password history
	passwordHistory = trim(Request.Form("passwordHistory"))	
	'user password reset
	passwordReset = trim(Request.Form("passwordReset"))

	'user password 1st
	userPassword1 = trim(Request.Form("userPassword1"))
	'user password 2nd ... confirm
	userPassword2 = trim(Request.Form("userPassword2"))	

	'user password..MUST BE AFTER...getting password 1 and 2
	userPassword = trim(Request.Form("userPassword"))
	if len(userPassword) <= 0 and (len(userPassword1) <= 0 and len(userPassword2) <= 0) then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Password.")
	end if	
	
	'check for password change
	if len(userPassword1) > 0 or len(userPassword2) > 0 then
		'don't match, throw away
		if userPassword1 <> userPassword2 then
			response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Passwords do not match. Please re-enter.")
		'all good...change/update password
		else
			'invalid password values
			'if (inStr(1,lCase(userPassword1),"password")>0) or (inStr(1,lCase(userPassword1),"12345")>0) or (inStr(1,lCase(userPassword1),"654321")>0) then 
			'	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Password contains invalid characters.")
			'else
				userPassword = userPassword1
								
				'user is resetting password, move reset date
				if action = "passreset" then
					'assign new reset date since password was changed/updated
					if lCase(passwordStrength) = "strong" then
						passwordReset = dateadd("d", 60, Date())
					elseif lCase(passwordStrength) = "medium" then
						passwordReset = dateadd("d", 180, Date())
					else
						passwordReset = dateadd("d", 365, Date())
					end if
				
				'admin/manager is changing password through users_edit.asp
				else
					if cDate(passwordReset) < date() then
						'dont do anything. password was set to expire prior to today by admin
					else				
						'assign new reset date since password was changed/updated
						if lCase(passwordStrength) = "strong" then
							passwordReset = dateadd("d", 60, Date())
						elseif lCase(passwordStrength) = "medium" then
							passwordReset = dateadd("d", 180, Date())
						else
							passwordReset = dateadd("d", 365, Date())
						end if
					end if
				
				end if
								
				'move through all past passwords
				'if lCase(passwordStrength) = "strong" then
					if len(passwordHistory) > 0 then
						dim arrPassword, tempPass, passI
						arrPassword = split(passwordHistory,"*|*")
						'reached limit, pull out most recent 6 passwords used						
						if cLng(uBound(arrPassword)) >= 7 then
							passwordHistory = "*|*" & arrPassword(uBound(arrPassword)-5) & "*|*"
							passwordHistory = passwordHistory & arrPassword(uBound(arrPassword)-4) & "*|*"
							passwordHistory = passwordHistory & arrPassword(uBound(arrPassword)-3) & "*|*"
							passwordHistory = passwordHistory & arrPassword(uBound(arrPassword)-2) & "*|*"
							passwordHistory = passwordHistory & arrPassword(uBound(arrPassword)-1) & "*|*"
							passwordHistory = passwordHistory & userPassword1 & "*|*"						
						else
							passwordHistory = passwordHistory & userPassword1 & "*|*"
						end if							
					else
						passwordHistory = "*|*" & userPassword1 & "*|*"
					end if
				'end if
								
			'end if
		end if		
	end if

end if


'RESET password
if action = "passreset" then

	dim urlLink
	urlLink = request.form("urlLink")

	'save encrypted password
	call openStoredProc("proc_SetPassword")					
	cmd.Parameters.Append(cmd.CreateParameter("@UserName", adVarWChar, adParamInput, 50, userName))  
	cmd.Parameters.Append(cmd.CreateParameter("@Passwrd", adVarWChar, adParamInput, 50, userPassword))
	cmd.Execute

	'Update Record
	mySQL = "UPDATE Logins SET " _	
		  & "PasswordHistory='" & passwordHistory	& "'," _
		  & "LoginAttempts="    & 0					& ", " _		  
  		  & "PasswordReset='" 	& passwordReset		& "' " 

	'finalize update query
	mySQL = mySQL & "WHERE LogID = '" & userLogID & "' "

	'update issue
	set rs = openRSexecute(mySQL)

	session(session("siteID") & "okMsg") = "Password has been saved successfully."

	'***********************************************
	'reopen user account to assign session variables -- MUST OR USER WILL BE LOGGED OUT
	mySQL = "SELECT Logins.* from Logins WHERE LogID = '" & userLogID & "' "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	'set user session variables
	session(session("siteID") & "adminLoggedOn") = rs("SecurityLevel")
	if cLng(session(session("siteID") & "adminLoggedOn")) <= 0 then											
		msg = "Invalid permission settings. Please contact CCI at 1-800-617-0415."				
		session.abandon	
		response.redirect ("../logon.asp")			
	end if
				
	session(session("siteID") & "firstname")	 = rs("FirstName")
	session(session("siteID") & "lastname")		 = rs("LastName")
	session(session("siteID") & "customerid")	 = rs("customerid")
	session(session("siteID") & "homepage")	 	 = rs("homepage")				
	session(session("siteID") & "homepagekey") 	 = rs("homepagekey")	

	'Used to diirect user to proper issues_edit.asp page
	'	issues_edit.asp =  users level >= 10
	'	issues_edit_00.asp = users level < 10
	if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
		session(session("siteID") & "issuesEdit") = "issues_edit_00.asp"
	else
		session(session("siteID") & "issuesEdit") = "issues_edit.asp"
	end if						
	'***********************************************

	'Send to issues page
	if len(urlLink) > 0 then
		response.redirect "../scripts/" & session(session("siteID") & "issuesEdit") & "?action=edit&recid=" & urlLink
	else
		if session(session("siteID") & "homepage") = "profiles.asp" then						
			response.redirect siteURL & "profiles/profiles.asp"
		elseif session(session("siteID") & "homepage") = "issues.asp" then						
			response.redirect siteURL & "scripts/issues.asp"
		elseif len(session(session("siteID") & "homepage")) > 0 then						
			response.redirect siteURL & session(session("siteID") & "homepage")
		end if		
	end if		

end if


'ADD/UPDATE group assignments
dim arrGroups, expI
if action = "edit" then

	arrGroups = split(trim(Request.Form("list_checkBox-groups")),",")

	'delete existing records from Category_IssueType
	mySQL = "DELETE FROM Logins_Groups " _
		  & "WHERE LogID=" & userLogID & " "
	set rs = openRSexecute(mySQL)

	'add all issue types selected by user
	for expI = 0 to UBound(arrGroups)
		mySQL = "INSERT INTO Logins_Groups (" _
			  & "LogID,GroupID,ModifiedBy,ModifiedDate" _
			  & ") VALUES (" _
			  & " " & userLogID & "," & arrGroups(expI) & "," & sLogid & ",'" & Now() & "' " _
			  & ")"
		set rs = openRSexecute(mySQL)			
	next

'elseif action = "copy" then

'	arrGroups = split(trim(Request.Form("list_checkBox-groups")),",")
'
'	'add all issue types selected by user
'	for expI = 0 to UBound(arrGroups)
'		mySQL = "INSERT INTO Logins_Groups (" _
'			  & "LogID,GroupID,ModifiedBy,ModifiedDate" _
'			  & ") VALUES (" _
'			  & " " & userLogID & "," & arrGroups(expI) & "," & sLogid & ",'" & userModifiedDate & "' " _
'			  & ")"
'		set rs = openRSexecute(mySQL)			
'	next
	
end if


'ADD
if action = "add" or action = "copy" then

	if action = "add" then
		if uCase(customerID) = "CCI" then
			sortField = "[Date]"
			sortOrder = "desc"
			whereClause = "<?xml version='1.0' encoding='ISO-8859-1'?><filters><filter><field>[Year]</field><operator>&gt;=</operator><value>2012</value></filter></filters>"
			userIssuesColumns = "[Issue #],[Date],[Progress],[Severity],[Location Name],[Risk Specialist]"
		else	
			sortField = "[Date]"
			sortOrder = "desc"
			whereClause = "<?xml version='1.0' encoding='ISO-8859-1'?><filters></filters>"
			userIssuesColumns = "[Issue #],[Date],[Severity],[Status],[Location Name],[Owner],[Caller First Name],[Caller Last Name]"
		end if	
		
	else
		sortField = "[Date]"
		sortOrder = "desc"	
		whereClause = "<?xml version='1.0' encoding='ISO-8859-1'?><filters></filters>"
		userIssuesColumns = "[Issue #],[Date],[Severity],[Status],[Location Name],[Owner],[Caller First Name],[Caller Last Name]"	
	end if
	
	dim fieldsSQL, valueSQL

	fieldsSQL = "INSERT INTO Logins (" _
		  & "SiteID,CustomerID,FirstName,LastName,UserName,PasswordHistory,Email,Active,addReport,ProgramAdmin," _
		  & "editReport,Title,Address,City,State,PostalCode,Phone_Home,Phone_Work,Phone_Cell,Phone_Pager," _
		  & "Phone_Fax,Phone_Other,Notes,Priority,Delivery,DeleteIssues,EmailIssueTypes,HomePage,HomePageKey,IssuesView,sortField,sortOrder,whereClause,IssuesColumns," _
		  & "Employee_ID,Job_Class,Division,Region,Department," _
		  & "EmailAssignment,EmailStatusChange,EmailInvAssignment,EmailInvStatusChange,emailDeadlineNotice,ModifiedBy,ModifiedDate"

	'check for empty dates	      
	if len(trim(userAutosave)) > 0 then fieldsSQL = fieldsSQL & ",Autosave" 
	if len(trim(loginAttempts)) > 0 then fieldsSQL = fieldsSQL & ",LoginAttempts" 
	if len(trim(securityLevel)) > 0 then fieldsSQL = fieldsSQL & ",SecurityLevel" 
	if len(trim(userExpires)) > 0 then fieldsSQL = fieldsSQL & ",Expires"
	if len(trim(passwordReset)) > 0 then fieldsSQL = fieldsSQL & ",PasswordReset"
	fieldsSQL = fieldsSQL & ") "

	valueSQL = " VALUES (" _
		  & "'"    	& uCase(customerID)					& "'," _		
		  & "'"    	& uCase(customerID)					& "'," _	
		  & "'"    	& replace(userFirstName,"'","''")	& "'," _
		  & "'"    	& replace(userLastName,"'","''")	& "'," _
		  & "'"    	& userName							& "'," _		  
		  & "'"    	& passwordHistory					& "'," _
		  & "'"    	& userEmail							& "'," _
		  & "'"    	& userActive						& "'," _
		  & "'"    	& userAddReport						& "'," _
		  & "'"    	& userProgramAdmin					& "'," _
		  & "'"    	& userEditReport					& "'," _
		  & "'"    	& userTitle							& "'," _
		  & "'"    	& replace(userAddress,"'","''")		& "'," _
		  & "'"    	& userCity							& "'," _
		  & "'"    	& userState							& "'," _
		  & "'"    	& userPostalCode					& "'," _
		  & "'"    	& userPhone_Home					& "'," _
		  & "'"    	& userPhone_Work					& "'," _
		  & "'"    	& userPhone_Cell					& "'," _
		  & "'"    	& userPhone_Pager					& "'," _
		  & "'"    	& userPhone_Fax						& "'," _
		  & "'"    	& userPhone_Other					& "'," _
		  & "'"    	& replace(userNotes,"'","''")		& "'," _
		  & "'"    	& userPriority						& "'," _
		  & "'"    	& userDelivery						& "'," _
		  & "'"    	& userDeleteIssues					& "'," _
		  & "'"    	& userEmailIssueTypes				& "'," _		  
		  & "'"    	& homePage							& "'," _
		  & "'"    	& homePageKey						& "'," _
		  & "'"    	& issuesView						& "'," _
		  & "'"    	& sortField							& "'," _
		  & "'"    	& sortOrder							& "'," _
		  & "'"    	& replace(whereClause,"'","''")		& "'," _
		  & "'"    	& userIssuesColumns					& "'," _	
		  & "'"    	&  replace(userEmployee_ID,"'","''")& "'," _
		  & "'"    	&  replace(userJob_Class,"'","''") 	& "'," _
		  & "'"    	&  replace(userDivision,"'","''") 	& "'," _
		  & "'"    	&  replace(userRegion,"'","''") 	& "'," _
		  & "'"    	&  replace(userDepartment,"'","''") & "'," _
		  & "'"    	& emailAssignment					& "'," _
		  & "'"    	& emailStatusChange					& "'," _
		  & "'"    	& emailInvAssignment				& "'," _
		  & "'"    	& emailInvStatusChange				& "'," _
		  & "'"    	& emailDeadlineNotice				& "'," _		  
		  &	" "		& sLogid							& ", " _		
		  & "'"    	& Now()								& "' "

	'check for empty values/numbers/dates
	if len(trim(userAutosave)) > 0 then valueSQL = valueSQL & "," & userAutosave
	if len(trim(loginAttempts)) > 0 then valueSQL = valueSQL & "," & loginAttempts
	if len(trim(securityLevel)) > 0 then valueSQL = valueSQL & "," & securityLevel
	if len(trim(userExpires)) > 0 then valueSQL = valueSQL & ",'" & userExpires & "'"
	if len(trim(passwordReset)) > 0 then valueSQL = valueSQL & ",'" & passwordReset & "'"
	valueSQL = valueSQL & ") "

	'set identity to pull back newly added record new record id is returned to newID
	mySQL = "SET NOCOUNT ON; " & fieldsSQL & valueSQL & " SELECT SCOPE_IDENTITY() AS newID;"
	set rs = openRSexecute(mySQL)		
	recId = rs("newID")		  				

	'save encrypted password
	if userPassword>"" and not isNull(userPassword) and len(userPassword) > 0 then
		call openStoredProc("proc_SetPassword")					
		cmd.Parameters.Append(cmd.CreateParameter("@UserName", adVarWChar, adParamInput, 50, userName))  
		cmd.Parameters.Append(cmd.CreateParameter("@Passwrd", adVarWChar, adParamInput, 50, userPassword))
		cmd.Execute
	end if
		
	session(session("siteID") & "okMsg") = "User was Added."
	response.redirect "users_edit.asp?action=edit&recID=" & recId & "&top=" & topTab & "&side=" & sideTab & "&pageView=users"
		  	
end if


'EDIT
if action = "edit" then

	if userName = "userdemo" _
	or userName = "admindemo" _
	or userName = "admin" then
		'DO NOTHING... DON'T LET PEOPLE CHANGE THESE.
	else
	
		'Update Record
		mySQL = "UPDATE Logins SET " _	
			  & "FirstName='"   			& replace(userFirstName,"'","''") & "'," _
			  & "LastName='"    			& replace(userLastName,"'","''") & "'," _
			  & "UserName='"    			& userName						& "'," _
			  & "PasswordHistory='" 		& passwordHistory				& "'," _
			  & "Email='"     				& userEmail						& "'," _		  
			  & "Active='"     				& userActive					& "'," _
			  & "addReport='"     			& userAddReport					& "'," _
			  & "ProgramAdmin='"   			& userProgramAdmin				& "'," _		  		  
			  & "editReport='"     			& userEditReport				& "'," _		  
			  & "Title='"     				& userTitle						& "'," _
			  & "Address='"     			& replace(userAddress,"'","''")	& "'," _		  		  
			  & "City='"     				& userCity						& "'," _
			  & "State='"     				& userState						& "'," _
			  & "PostalCode='"     			& userPostalCode				& "'," _
			  & "Phone_Home='"     			& userPhone_Home				& "'," _
			  & "Phone_Work='"     			& userPhone_Work				& "'," _
			  & "Phone_Cell='"     			& userPhone_Cell				& "'," _
			  & "Phone_Pager='"     		& userPhone_Pager				& "'," _		  
			  & "Phone_Fax='"     			& userPhone_Fax					& "'," _
			  & "Phone_Other='"     		& userPhone_Other				& "'," _		  
			  & "Notes='"     				& replace(userNotes,"'","''")	& "'," _		  
			  & "Priority='"     			& userPriority					& "'," _
			  & "Delivery='"     			& userDelivery					& "'," _		
			  & "DeleteIssues='"   			& userDeleteIssues				& "'," _				  		    		  		  
			  & "EmailIssueTypes='"			& userEmailIssueTypes			& "'," _				  		    		  		  		  		  
			  & "HomePage='"     			& homePage						& "'," _
			  & "HomePageKey='"     		& homePageKey					& "'," _
			  & "IssuesView='"     			& issuesView					& "'," _	
			  & "sortField='"     			& sortField						& "'," _	
			  & "sortOrder='"     			& sortOrder						& "'," _	
			  & "whereClause='"     		& replace(whereClause,"'","''")	& "'," _	
			  & "IssuesColumns='"   		& userIssuesColumns				& "'," _			  		  		  		  		  		  		  
			  & "Employee_ID='"   			& replace(userEmployee_ID,"'","''") & "'," _			  		  		  		  		  
			  & "Job_Class='"   			& replace(userJob_Class,"'","''")	& "'," _			  		  		  		  		  		  
			  & "Division='"   				& replace(userDivision,"'","''")	& "'," _			  		  		  		  		  		  		  
			  & "Region='"   				& replace(userRegion,"'","''")		& "'," _			  		  		  		  		  		  		  
			  & "Department='"   			& replace(userDepartment,"'","''")	& "'," _			  		  		  		  		  		  		  		  
			  & "EmailAssignment='"     	& emailAssignment				& "'," _	
			  & "EmailStatusChange='"     	& emailStatusChange				& "'," _	
			  & "EmailInvAssignment='"     	& emailInvAssignment			& "'," _	
			  & "EmailInvStatusChange='"    & emailInvStatusChange			& "'," _		
			  & "EmailDeadlineNotice='"     & emailDeadlineNotice			& "'," _			  		  		  		  		  	  		  		  		  
			  & "ModifiedBy="				& sLogid						& ", " _
			  & "ModifiedDate='"			& Now()							& "' "
	
		'check for special existance of dates
		if len(trim(userAutosave)) > 0 then mySQL = mySQL & ",Autosave=" & userAutosave & " "
		if len(trim(loginAttempts)) > 0 then mySQL = mySQL & ",LoginAttempts=" & loginAttempts & " "
		if len(trim(securityLevel)) > 0 then mySQL = mySQL & ",SecurityLevel=" & securityLevel & " "
		if len(trim(userExpires)) > 0 then mySQL = mySQL & ",Expires='" & userExpires & "' "
		if len(trim(passwordReset)) > 0 then mySQL = mySQL & ",PasswordReset='" & passwordReset & "' "	
		'finalize update query
		mySQL = mySQL & "WHERE LogID = '" & userLogID & "' "
	
		'update issue
		set rs = openRSexecute(mySQL)

		'save encrypted password
		if userPassword>"" and not isNull(userPassword) and len(userPassword) > 0 then
			call openStoredProc("proc_SetPassword")					
			cmd.Parameters.Append(cmd.CreateParameter("@UserName", adVarWChar, adParamInput, 50, userName))  
			cmd.Parameters.Append(cmd.CreateParameter("@Passwrd", adVarWChar, adParamInput, 50, userPassword))
			cmd.Execute				
		end if

	end if

	'reset home page key
	session(session("siteID") & "homePageKey") = homePageKey

	'reset home page of user if same person saving record
	if cLng(sLogid) = cLng(userLogID) then
		session(session("siteID") & "homepage") = homePage
	end if
	
	session(session("siteID") & "okMsg") = "User has been saved successfully."
	response.redirect "users_edit.asp?action=edit&recID=" & userLogID & "&top=" & topTab & "&side=" & sideTab & "&pageView=users"

end if


'load reports
if action = "loadreports" then

	'user CustomerID
	customerID = trim(Request.Form("customerID"))	
	if len(trim(customerID))<=0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if

	'user Security Level
	securityLevel = trim(Request.Form("securityLevel"))	
	if len(trim(securityLevel))<=0 then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Security Level.")
	end if

	mySQL = "INSERT INTO Reports ( " _
		  & "		CustomerID, LogID, DatasetID, Name, Category, IssueList, ListURLField, FileName, QueryFields, QuerySumGroup, " _
		  & "		QuerySumCalculation, QuerySortOrder, QuerySortField, QueryFilter, [SQL], ChartType, ChartXAxis, ChartYAxis, ChartLegend, " _
		  & "		ChartSize, SortOrder, ModifiedBy, ModifiedDate ) " _
		  & "	SELECT '" & customerID & "' AS [CustID], " & userLogID & " AS [userLogID], a.DatasetID, a.Name, a.Category, a.IssueList, " _
		  & "			a.ListURLField, a.FileName, a.QueryFields, a.QuerySumGroup, a.QuerySumCalculation, " _
		  & "			a.QuerySortOrder, a.QuerySortField, a.QueryFilter, a.SQL, a.ChartType, " _
		  & "			a.ChartXAxis, a.ChartYAxis, a.ChartLegend, a.ChartSize, a.SortOrder, " & sLogid & " AS [ModifiedBy], '" & Now() & "' AS [ModifiedDate] " _
		  & "		FROM Reports a INNER JOIN Logins ON a.LogID = Logins.LOGID " _
		  & "		WHERE Logins.CustomerID='MYCM' AND Logins.SecurityLevel=" & securityLevel & " AND " _
		  & "	NOT (Exists (SELECT b.DatasetID, b.Name, b.LogID " _
		  & "			  FROM Reports b " _
		  & "			 WHERE a.DatasetID = b.DatasetID AND a.Name = b.Name AND b.LogID=" & userLogID & " )) "
	set rs = openRSexecute(mySQL)

	session(session("siteID") & "okMsg") = "All available reports have been loaded."
	response.redirect "users_edit.asp?action=edit&recID=" & userLogID & "&top=" & topTab & "&side=" & sideTab & "&pageView=users"
	
end if


'disable user
if action = "del" then

	customerID = trim(lCase(Request.QueryString("cid")))
	
	if userName = "userdemo" _
	or userName = "admindemo" _
	or userName = "admin" then
		'DO NOTHING... DON'T LET PEOPLE CHANGE THESE.		
	else
		mySQL = "UPDATE Logins SET Active='N', SecurityLevel=60, ProgramAdmin='N' " _
			  & "	WHERE LogID = '" & userLogID & "' "
		set rs = openRSexecute(mySQL)
	end if

	session(session("siteID") & "okMsg") = "User has been disabled."
	response.redirect "users.asp?cid=" & customerID
	
end if


'Just in case we ever get this far...
call closeDB()
Response.Redirect "users.asp"

%>
