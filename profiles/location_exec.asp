<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->

<%
'Database
dim mySQL, cn, rs, rs2

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("customerID")
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'Location fields in DETAILS tab
dim locNameID
dim locName
dim	locAddress
dim	locCity
dim	locState
dim	locZip
dim	locCountry
dim	locPhone
dim	locLevel_1
dim	locLevel_2
dim	locLevel_3
dim	locLevel_4
dim	locLevel_5
dim	locLevel_6
dim	locLevel_7
dim	locLevel_8
dim	locNotes
dim locDirectiveID

'flag right now as when this issue was modified
dim modifiedDate : modifiedDate = Now()

'Work Fields
dim action, recId

'Get action
action = trim(lCase(Request.Form("action")))
if len(action) = 0 then
	action = trim(lCase(Request.QueryString("action")))
end if
if  action <> "edit" _
and action <> "add" _
and action <> "del" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if


'Get ID of location to work with
if action = "edit"  _
or action = "del" then
	recId = trim(Request.Form("LocationID"))
	if recId = "" then
		recId = trim(lCase(Request.QueryString("recid")))
	end if	
	if recId = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Location ID.")
	end if
end if

'make sure user can update this record
if programAdmin = "N" and cLng(session(session("siteID") & "adminLoggedOn")) > 2 then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if


'used to redirect user back to tabs they last were on
dim topTab, sideTab
topTab = trim(Request.Form("topTab"))
if len(topTab) = 0 then
	topTab = trim(Request.QueryString("top"))
end if
sideTab = trim(Request.Form("sideTab"))
if len(sideTab) = 0 then
	sideTab = trim(Request.QueryString("side"))
end if

'Get Location Details
if action = "edit" or action = "add" then

	locNameID = trim(Request.Form("locNameID"))
	locName = trim(Request.Form("locName"))
	locAddress = trim(Request.Form("locAddress"))
	locCity = trim(Request.Form("locCity"))
	locState = trim(Request.Form("locState"))
	locZip = trim(Request.Form("locZip"))
	locCountry = trim(Request.Form("locCountry"))
	locPhone = trim(Request.Form("locPhone"))
	locLevel_1 = trim(Request.Form("locLevel_1"))
	locLevel_2 = trim(Request.Form("locLevel_2"))
	locLevel_3 = trim(Request.Form("locLevel_3"))
	locLevel_4 = trim(Request.Form("locLevel_4"))
	locLevel_5 = trim(Request.Form("locLevel_5"))
	locLevel_6 = trim(Request.Form("locLevel_6"))
	locLevel_7 = trim(Request.Form("locLevel_7"))
	locLevel_8 = trim(Request.Form("locLevel_8"))
	locNotes = trim(Request.Form("locNotes"))
	locDirectiveID = trim(Request.Form("locDirectiveID"))
					
end if


'ADD
if action = "add" then

	dim fieldsSQL, valueSQL

	'Add Issue with Callback
	fieldsSQL = "INSERT INTO Location (" _
		  & "CustomerID,NameID,Name,Address,City,State,PostalCode,Country,Phone,Level_1,Level_2,Level_3,Level_4, " _
		  & "Level_5,Level_6,Level_7,Level_8,Notes,ModifiedBy,ModifiedDate"

	'check for empty dates	      
	if len(locDirectiveID) > 0 then fieldsSQL = fieldsSQL & ",DirectiveID"		  
	fieldsSQL = fieldsSQL & ") "
		  
	valueSQL = " VALUES (" _
		  & "'"    	& uCase(customerID)				& "'," _
		  & "'"    	& replace(locNameID,"'","''")	& "'," _		  
		  & "'"    	& replace(locName,"'","''")		& "'," _
		  & "'"    	& replace(locAddress,"'","''")	& "'," _
		  & "'"    	& replace(locCity,"'","''")		& "'," _
		  & "'"    	& locState						& "'," _
		  & "'"    	& replace(locZip,"'","''")		& "'," _
		  & "'"    	& replace(locCountry,"'","''")	& "'," _
		  & "'"    	& locPhone						& "'," _
		  & "'"    	& replace(locLevel_1,"'","''")	& "'," _
		  & "'"    	& replace(locLevel_2,"'","''")	& "'," _
		  & "'"    	& replace(locLevel_3,"'","''")	& "'," _
		  & "'"    	& replace(locLevel_4,"'","''")	& "'," _
		  & "'"    	& replace(locLevel_5,"'","''")	& "'," _
		  & "'"    	& replace(locLevel_6,"'","''")	& "'," _
		  & "'"    	& replace(locLevel_7,"'","''")	& "'," _
		  & "'"    	& replace(locLevel_8,"'","''")	& "'," _
		  & "'"    	& replace(locNotes,"'","''")	& "'," _
		  & " "    	& sLogid						& ", " _
		  & "'"    	& modifiedDate					& "' "

	'check for empty dates
	if len(locDirectiveID) > 0 then valueSQL = valueSQL & "," & locDirectiveID & " "		  
	valueSQL = valueSQL & ") "

	'set identity to pull back newly added record new record id is returned to newID
	mySQL = "SET NOCOUNT ON; " & fieldsSQL & valueSQL & " SELECT SCOPE_IDENTITY() AS newID;"
	set rs = openRSexecute(mySQL)		
	recId = rs("newID")		  				
	
	session(session("siteID") & "okMsg") = "Location was Added."
	response.redirect "location_edit.asp?action=edit&recID=" & recId & "&cid=" & customerID & "&top=" & topTab & "&side=" & sideTab
	
end if


'EDIT
if action = "edit" then
		
	'Update Record
	mySQL = "UPDATE Location SET " _	
		  & "NameID='"  		& replace(locNameID,"'","''")	& "'," _	
		  & "Name='"   			& replace(locName,"'","''")		& "'," _
		  & "Address='"    		& replace(locAddress,"'","''")	& "'," _
		  & "City='"    		& replace(locCity,"'","''")		& "'," _
		  & "State='"    		& locState						& "'," _
		  & "PostalCode='" 		& replace(locZip,"'","''")		& "'," _
  		  & "Country='" 		& replace(locCountry,"'","''")	& "'," _
		  & "Phone='"     		& locPhone						& "'," _		  
		  & "Level_1='"     	& replace(locLevel_1,"'","''")	& "'," _
		  & "Level_2='"     	& replace(locLevel_2,"'","''")	& "'," _
		  & "Level_3='"     	& replace(locLevel_3,"'","''")	& "'," _		  		  
		  & "Level_4='"     	& replace(locLevel_4,"'","''")	& "'," _
		  & "Level_5='"     	& replace(locLevel_5,"'","''")	& "'," _
		  & "Level_6='"     	& replace(locLevel_6,"'","''")	& "'," _
		  & "Level_7='"     	& replace(locLevel_7,"'","''")	& "'," _
		  & "Level_8='"     	& replace(locLevel_8,"'","''")	& "'," _
		  & "Notes='"     		& replace(locNotes,"'","''")	& "'," _
		  & "ModifiedBy="  		& sLogid						& ", " _
		  & "ModifiedDate='"	& modifiedDate					& "' "

	'check for special existance of dates  
	if len(locDirectiveID) > 0 then mySQL = mySQL & ",DirectiveID=" & locDirectiveID & " " else mySQL = mySQL & ",DirectiveID=NULL "
		  
	'finalize update query
	mySQL = mySQL & "WHERE LocationID = '" & recId & "' "

	'update issue
	set rs = openRSexecute(mySQL)	

	session(session("siteID") & "okMsg") = "Location has been saved successfully."
	response.redirect "location_edit.asp?action=edit&recID=" & recId & "&cid=" & customerID & "&top=" & topTab & "&side=" & sideTab

end if


'DELETE
if action = "del" then

	'Delete records from optionsGroupsXref
	mySQL = "DELETE FROM Location " _
	      & "WHERE LocationID = " &  recId & " "
	set rs = openRSexecute(mySQL)		
	
	session(session("siteID") & "okMsg") = "Location was Deleted."
	response.redirect "location.asp?cid=" & customerID
	
end if


'Just in case we ever get this far...
call closeDB()
response.redirect "location.asp?cid=" & customerID

%>
