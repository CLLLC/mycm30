<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = false
const showDHTMLMenu = true
const helpID = 6
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "issue_import"
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) > 2 and programAdmin = "N" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if

dim I
dim item
dim pageSize
dim totalPages
dim showArr
dim sortField
dim sortOrder
dim curPage

dim action
dim tableProperty

dim showCRSID

dim tempHierarchy
%>

<% dim pageTitle : pageTitle = "Issues Import" %>
<!--#include file="../_includes/_INCheaderLite_.asp"-->

<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
        
        <!-- START Left sided area -->
		<td width="190" align="right" valign="top">

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided tree menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">                

                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->        
                    <% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>      
	                    <script>
							stExpandSubTree('myCMtree',1,0)
       		            </script>
            		<% else %>
	                    <script>
							stCollapseSubTree('myCMtree',1,0);
							stCollapseSubTree('myCMtree',2,0);							
       		            </script>
                    <% end if %>                          
                    <!-- STOP side menu include -->
                    
                </td>
              </tr>
              <!-- END Left sided tree menu -->
        
            </table>
        
        </td>
        <!-- STOP Left sided area -->
            
	    <!-- START Main Middle Section -->
		<td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">       

            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs());
       	        </script>
            </div>
            
            <div style="text-align:left; margin-bottom:10px;">				
                <div style="float:left; padding-right:2px; margin-right:2px;"><img src="../_images/icons/32/globe.png" title="" width="32" height="32" align="absmiddle" style="vertical-align:middle;"></div>
                <div>
                	<span class="pageTitle">Issues Import</span>
                	<br/>:: <a href="../profiles/profiles_menu.asp?cid=<% =customerID %>"><% response.write( getCustomerName(customerID) ) %></a>
                </div>
            </div>                   
                                          
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
                        <legend>Information</legend>
                        <div align="left">
	                        <img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px;">Below is a list of issue fields that can be imported.
							<% response.write( getInlineHelp("getPageURL", 0) ) %>
                        </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             

                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">&nbsp;
                    </td>
                    <!-- STOP Information buttons -->
                                                                                
                </tr>
            </table>        
            <!-- STOP information box -->
    

    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			'clear system messages
			call clearSessionMessages()
            %>            
    		<!-- STOP message notification box -->

            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
				<tr> 
					<td valign="top">

            <!-- START Import Spec table -->            
            <table id="importTable" style="width:100%;" border="0" cellspacing="0" cellpadding="0">
				<tr> 
                    <th align="left" style="width:75px;">Column</th>
                    <th align="left">Data</th>
                    <th>Required</th>
                    <th>Unique</th>
                    <th>Type</th>
                    <th>Length</th>                    
                </tr>
                <tr> 
                    <td align="left">Column A</td>
                    <td align="left" style="font-weight:bold">Name</td>
                    <td>YES</td>
                    <td>YES</td>
                    <td>Text</td>
                	<td>50</td>
                </tr>
                <tr> 
                    <td align="left">Column B</td>
                    <td align="left" style="font-weight:bold">Date</td>
                    <td>YES</td>
                    <td>-</td>
                    <td>Date</td>
                    <td>-</td>
                </tr>
                <tr> 
                    <td align="left">Column C</td>
                    <td align="left" style="font-weight:bold">Time</td>
                    <td>YES</td>
                    <td>-</td>
                    <td>Date</td>
                    <td>-</td>
                </tr>
                <tr> 
                    <td align="left">Column D</td>
                    <td align="left" style="font-weight:bold">Severity</td>
                    <td>YES</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>5</td>
                </tr>
                <tr> 
                    <td align="left">Column E</td>
                    <td align="left" style="font-weight:bold">Callback</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Date</td>
                    <td>-</td>
                </tr>
                <tr> 
                    <td align="left">Column F</td>
                    <td align="left" style="font-weight:bold">Location Name</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>150</td>
                </tr>
                <tr> 
                    <td align="left">Column G</td>
                    <td align="left" style="font-weight:bold">Location Address</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>255</td>
                </tr>
                <tr> 
                    <td align="left">Column H</td>
                    <td align="left" style="font-weight:bold">Location City</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>50</td>
                </tr>
                <tr> 
                    <td align="left">Column I</td>
                    <td align="left" style="font-weight:bold">Location State</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>50</td>
                </tr>
                <tr> 
                    <td align="left">Column J</td>
                    <td align="left" style="font-weight:bold">Location Postalcode</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>50</td>
                </tr>
                <tr> 
                    <td align="left">Column K</td>
                    <td align="left" style="font-weight:bold">Location Country</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>150</td>
                </tr>
                <tr> 
                    <td align="left">Column L</td>
                    <td align="left" style="font-weight:bold">Location Level 1</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>150</td>
                </tr>
                <tr> 
                    <td align="left">Column M</td>
                    <td align="left" style="font-weight:bold">Location Level 2</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>150</td>
                </tr>
                <tr> 
                    <td align="left">Column N</td>
                    <td align="left" style="font-weight:bold">Location Level 3</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>150</td>
                </tr>
                <tr> 
                    <td align="left">Column O</td>
                    <td align="left" style="font-weight:bold">Location Level 4</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>150</td>
                </tr>
                <tr> 
                    <td align="left">Column P</td>
                    <td align="left" style="font-weight:bold">Location Level 5</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>150</td>
                </tr>
                <tr> 
                    <td align="left">Column Q</td>
                    <td align="left" style="font-weight:bold">Category (main)</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>150</td>
                </tr>
                <tr> 
                    <td align="left">Column R</td>
                    <td align="left" style="font-weight:bold">Category (sub)</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>150</td>
                </tr>
                <tr> 
                    <td align="left">Column S</td>
                    <td align="left" style="font-weight:bold">Anonymous (yes/no)</td>
                    <td>YES</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>5</td>
                </tr>
                <tr> 
                    <td align="left">Column T</td>
                    <td align="left" style="font-weight:bold">Reporter First Name</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>50</td>
                </tr>
                <tr> 
                    <td align="left">Column U</td>
                    <td align="left" style="font-weight:bold">Reporter Last Name</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>50</td>
                </tr>
                <tr> 
                    <td align="left">Column V</td>
                    <td align="left" style="font-weight:bold">Reporter Title</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>150</td>
                </tr>

                <tr> 
                    <td align="left">Column W</td>
                    <td align="left" style="font-weight:bold">Reporter Address</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>255</td>
                </tr>
                <tr> 
                    <td align="left">Column X</td>
                    <td align="left" style="font-weight:bold">Reporter City</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>50</td>
                </tr>
                <tr> 
                    <td align="left">Column Y</td>
                    <td align="left" style="font-weight:bold">Reporter State</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>50</td>
                </tr>
                <tr> 
                    <td align="left">Column Z</td>
                    <td align="left" style="font-weight:bold">Reporter Postalcode</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>50</td>
                </tr>
                <tr> 
                    <td align="left">Column AA</td>
                    <td align="left" style="font-weight:bold">Reporter Home Phone</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>50</td>
                </tr>
                <tr> 
                    <td align="left">Column AB</td>
                    <td align="left" style="font-weight:bold">Reporter Work Phone</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>50</td>
                </tr>
                <tr> 
                    <td align="left">Column AC</td>
                    <td align="left" style="font-weight:bold">Reporter Email</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>50</td>
                </tr>
                <tr> 
                    <td align="left">Column AD</td>
                    <td align="left" style="font-weight:bold">Summary</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AE</td>
                    <td align="left" style="font-weight:bold">Details</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AF</td>
                    <td align="left" style="font-weight:bold">Addendum</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AG</td>
                    <td align="left" style="font-weight:bold">Owner Email</td>
                    <td>YES</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>50</td>
                </tr>
                <tr> 
                    <td align="left">Column AH</td>
                    <td align="left" style="font-weight:bold">Status</td>
                    <td>YES</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>150</td>
                </tr>
                <tr> 
                    <td align="left">Column AI</td>
                    <td align="left" style="font-weight:bold">Source</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>50</td>
                </tr>
                <tr> 
                    <td align="left">Column AJ</td>
                    <td align="left" style="font-weight:bold">Case Notes</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AK</td>
                    <td align="left" style="font-weight:bold">User Field #1</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AL</td>
                    <td align="left" style="font-weight:bold">User Field #2</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AM</td>
                    <td align="left" style="font-weight:bold">User Field #3</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AN</td>
                    <td align="left" style="font-weight:bold">User Field #4</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AO</td>
                    <td align="left" style="font-weight:bold">User Field #5</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AP</td>
                    <td align="left" style="font-weight:bold">User Field #6</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AQ</td>
                    <td align="left" style="font-weight:bold">User Field #7</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AR</td>
                    <td align="left" style="font-weight:bold">User Field #8</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AS</td>
                    <td align="left" style="font-weight:bold">User Field #9</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AT</td>
                    <td align="left" style="font-weight:bold">User Field #10</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AU</td>
                    <td align="left" style="font-weight:bold">User Field #11</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AV</td>
                    <td align="left" style="font-weight:bold">User Field #12</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AW</td>
                    <td align="left" style="font-weight:bold">User Field #13</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AX</td>
                    <td align="left" style="font-weight:bold">User Field #14</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AY</td>
                    <td align="left" style="font-weight:bold">User Field #15</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column AZ</td>
                    <td align="left" style="font-weight:bold">User Field #16</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column BA</td>
                    <td align="left" style="font-weight:bold">User Field #17</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column BB</td>
                    <td align="left" style="font-weight:bold">User Field #18</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column BC</td>
                    <td align="left" style="font-weight:bold">User Field #19</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column BD</td>
                    <td align="left" style="font-weight:bold">User Field #20</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Text</td>
                    <td>Unlimited</td>
                </tr>
                <tr> 
                    <td align="left">Column BE</td>
                    <td align="left" colspan="5">ROW END - Enter '<strong>END</strong>' in this column to indicate the end of the row.</td>
                </tr>                
            </table>	
            <!-- STOP Import Spec table -->

			</td>
            
            <td valign="top" style="width:300px; padding:5px;">
				<table style="width:100%; border: 1px solid #969696; background-color:#F0F0F0; padding:10px;" border="0" cellspacing="0" cellpadding="0" class="formTable">
                    <tr>
                    	<td style="text-align:left; border-top:none;">
							<div class="formLabel" style="text-transform:uppercase; margin-bottom:5px; text-align:left;"><strong>Actions:</strong></div>                             
                            
                            <div style="padding-bottom:15px;">                            
                                <div style="float:left; padding-right:5px;">
                                    <a href="#" onClick="SimpleModal.open('../_jquery/fancyupload/fancyupload-import.asp?cid=<% =customerID %>', 400, 500, 'no'); return false;"><img src="../_images/icons/24/upload.png" width="24" height="24" align="absmiddle" border="0" style="padding-right:5px; vertical-align:middle;"/></a><a href="#" onClick="SimpleModal.open('../_jquery/fancyupload/fancyupload-import.asp?cid=<% =customerID %>', 400, 500, 'no'); return false;" style="font-weight:bold;">Upload File</a>
                                </div>
                                <div id="ImportFile" style="padding-top:5px;">[<em>no file selected</em>]</div>
							</div>
                            
                            <div id="StartImport" style="float:left; font-weight:bold;">
	                        	<img src="../_images/icons/24/stopwatch_start_disabled.png" width="24" height="24" align="absmiddle" border="0" style="padding-right:5px;" />Start Import
                        	</div>

							<input type="hidden" name="FileName" id="FileName" value="">
                            
                        </td>
                    </tr>
                    
                    <tr>
                    	<td style="text-align:left;">
							<div class="formLabel" style="text-transform:uppercase; margin-bottom:5px; text-align:left;"><strong>Important:</strong></div>
                       		<img src="../_images/icons/16/exclamation.png" width="16" height="16" align="absmiddle" style="margin-right:7px;" />The [Name ID] column is used to uniquely identify individual locations.<br><br>
							<img src="../_images/icons/16/exclamation.png" width="16" height="16" align="absmiddle" style="margin-right:7px;" />This process is a "raw-data" process. Your data will not be checked against all validation rules. You should be sure that your raw data complies with your validation settings prior to uploading.<br><br>
                       		<img src="../_images/icons/16/exclamation.png" width="16" height="16" align="absmiddle" style="margin-right:7px;" />Note the following before creating your file:<br><br>
                            <ul style="padding-left:15px; margin-left:15px; padding-bottom:0px; margin-bottom:0px;">
                              <li style="margin-bottom:5px;">Uploaded file must be a tab-delimited text file (with extension .TXT).</li>
                              <li style="margin-bottom:5px;">Include all data columns (even those marked as NOT USED) in your file.</li>
                              <li style="margin-bottom:5px;">All required fields must have data.</li>
                              <li style="margin-bottom:5px;">Be sure that no extra empty lines exist at the end of your file.</li>
							  <li style="margin-bottom:5px;">Do not include column headers.</li>
							  <li style="margin-bottom:5px;">Do not enclose cells values in quotations ("").</li>
							  <li style="margin-bottom:5px;">Do not include tabs in cell values.</li>
                              <li>Be sure every row ends with 'END' in Column Q</li>
                            </ul>                       
                       	</td>                        
                    </tr>

                    <tr>
                   	  	<td style="text-align:left;">
                            <div class="formLabel" style="text-transform:uppercase; margin-bottom:5px; text-align:left;"><strong>Column Details:</strong></div>                             
                            <ul style="padding-left:15px; margin-left:15px; padding-bottom:0px; margin-bottom:0px;">
                              <li style="margin-bottom:5px;">Required - 'YES' indicates that all rows must contain data in this column otherwise your file will not be processed. This requirement will be validated.</li>
                              <li style="margin-bottom:5px;">Unique - 'YES' indicates that data in this field must be unique for each location. Used for fields such as [name id] or identification numbers that cannot be shared by more than one location. This requirement will be validated.</li>
                              <li>Length - The maximum number of characters that the database will allow for the column. Note that your system may be configured for more restrictive lengths. This is for information only and will not be validated - be sure to review your configuration.</li>
                            </ul>                       
                    	</td>                        
                    </tr>
                    
              </table>
            </td>
            
            </tr>
            </table>
            
		</td>
    	<!-- STOP Main Middle Section -->

    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;</td>        
    	<!-- STOP Right side area -->
        
  	</tr>
</table>
<!-- STOP Page -->

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<script language="javascript">	
	function pageReload(cid) {  	
    	window.location = '../profiles/issuetypes.asp?cid='+cid;
    }
	//used to navigate elsewhere
	function navigateAway(url) {
		window.location = url;
	}		
</script>	

<script language="javascript" type="text/javascript">   															
	//<![CDATA[	
	var tableProp = { sort: true, grid: false, alternate_rows: true, themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]} };
	//initiate table setup
	var tf1 = setFilterGrid("importTable",tableProp);
	//]]>
</script>

<script language="javascript">
	//set new RPT file...called from fancyupload-crystal.asp
	function addDocUploaded(file,date,size,ext){		
		document.getElementById('FileName').value = file;		
		document.getElementById('ImportFile').innerHTML = '[' + file.substring(0,50) + ']';
		document.getElementById('StartImport').innerHTML = '<a href=\"#\" onclick=\"FireImport(); return false;\" style=\"padding-right:5px;\"><img src=\"../_images/icons/24/stopwatch_start.png\" width=\"24\" height=\"24\" align=\"absmiddle\" border=\"0\"></a><a href=\"#\" onclick=\"FireImport(); return false;\">Start Import</a>'
	}

	function FireImport() {
		SimpleModal.open('../_dialogs/popup_issue_import.asp?cid=<% =customerID %>&file=' + document.getElementById('FileName').value, 275, 350, 'no');
	}
</script>


