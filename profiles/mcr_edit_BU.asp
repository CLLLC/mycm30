<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = false
const showDHTMLMenu = true
const helpID = 6
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "mcr"
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
'	if customerID = "" then
'		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
'	end if
end if

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

dim showCRSID '...need for NEW search box

dim startDate
dim endDate
dim optArr
dim cusName
dim cusDisplayAs
dim cusAddress
dim cusCity
dim cusState
dim cusZip
dim cusCountry
dim cusIndustry
dim cusEmployeeCount
dim cusContractDate
dim cusStartDate
dim cusCanceled
dim cusPhone_Work
dim cusPhone_Fax
dim cusPhone_Customer
dim cusPhone_Other
dim cusRetention
dim cusQuestion
dim cusAppShowUserFields
dim cusAppAssignFUPLogid
dim cusAppAssignFUPCategory
dim cusNotes

dim mcrSource
dim mcrLogo
dim mcrLogoBG
dim mcrLogoBGColor
dim mcrSubMenuBGColor
dim mcrSubMenuFontColor

dim mcrInitialDisclaimer
dim mcrFollowUpDisclaimer
dim mcrWhatsNext


'get/set customerID
if customerID = "" and action = "edit" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
end if


'Get action --  this must come before "get/set customerID" below
dim action : action = trim(Request.QueryString("action"))
if len(action) = 0 then
	action = trim(Request.Form("action"))
end if
action = lCase(action)
if  action <> "edit" _
and action <> "add"  _
and action <> "del" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if


'make sure user can update this page
'if action = "edit" and (cLng(session(session("siteID") & "adminLoggedOn")) > 2 and programAdmin = "N") then
if action = "edit" and cLng(session(session("siteID") & "adminLoggedOn")) > 2 then
	if uCase(customerid)="CEV" then
		'nothing...
	else
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
	end if
end if


'TinyMCE
dim activeElements : activeElements = "mcrInitialDisclaimer,mcrFollowUpDisclaimer,mcrWhatsNext"

'used to set side top and side tabs after user saves record
dim topTab
topTab = trim(Request.QueryString("top"))
if len(topTab) <= 0 then
	topTab = "details"
end if


'Get Record Details
if action = "edit" then

	mySQL = "SELECT pMCRSource, pMCRLogo, pMCRLogoBG, pMCRLogoBGColor, pMCRSubMenuBGColor, pMCRSubMenuFontColor, " _
		  & " 	pMCRInitialDisclaimer, pMCRFollowUpDisclaimer, pMCRWhatsNext " _
		  & "	FROM Customer INNER JOIN Customer_IssueType ON Customer.CustomerID = Customer_IssueType.CustomerID " _
		  & "	WHERE Customer.CustomerID='" & CustomerID & "' AND Customer_IssueType.IssueTypeID=4 AND Customer_IssueType.Active='Y' "		
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("MyComplianceReport.com has not been activated for this account.")
	else			
		mcrSource = rs("pMCRSource")
		mcrLogo = rs("pMCRLogo")
		mcrLogoBG = rs("pMCRLogoBG")
		mcrLogoBGColor = rs("pMCRLogoBGColor")
		mcrSubMenuBGColor = rs("pMCRSubMenuBGColor")
		mcrSubMenuFontColor = rs("pMCRSubMenuFontColor")
		mcrInitialDisclaimer = rs("pMCRInitialDisclaimer")
		mcrFollowUpDisclaimer = rs("pMCRFollowUpDisclaimer")
		mcrWhatsNext = rs("pMCRWhatsNext")		
	end if
	call closeRS(rs)
	
end if

%>

<% 
dim pageTitle
if action = "edit" then
	pageTitle = "MyComplianceReport.com" 
end if
%>
<!--#include file="../_includes/_INCheader_.asp"-->

<!--JQuery color picker -->
<link rel="stylesheet" media="screen" type="text/css" href="../_jquery/colorpicker/css/colorpicker.css" />
<script type="text/javascript" src="../_jquery/colorpicker/js/colorpicker.js"></script>

<form name="frm" id="frm" method="post" action="mcr_exec.asp" style="padding:0px; margin:0px;">

<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
  
	    <!-- START Left sided area -->  
		<td width="190" align="right" valign="top" >    

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">
    
                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->              
                    <script>
						stCollapseSubTree('myCMtree',1,0);
						stCollapseSubTree('myCMtree',2,0);
						stCollapseSubTree('myCMtree',3,0);
       	            </script>
                    <!-- STOP side menu include -->
                                    
                </td>
              </tr>
              <!-- STOP Left sided menu -->
        
            </table>

		</td>
    	<!-- STOP Left sided area -->


        <!-- START Main Middle Section -->
        <td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">    

			<%
			'used to STOP updating the cookie crumb if page is being recalled from a SAVE
			dim updateCrumb
			if inStr(1, session(session("siteID") & "okMsg"), "was Added") > 0 then
				'do nothing...will update cookie
            elseif len(session(session("siteID") & "errMsg")) > 0 or len(session(session("siteID") & "okMsg")) > 0 then
				updateCrumb = "false"
            end if
			%>
            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs('<% =updateCrumb %>'));
       	        </script>
            </div>
    
            <div style="text-align:left; margin-bottom:10px;">				
                <div style="float:left; padding-right:2px; margin-right:2px;"><img src="../_images/icons/32/monitor_window_3d.png" title="" width="32" height="32" align="absmiddle" style="vertical-align:middle;"></div>
                <div>
                	<span id="titleText" class="pageTitle">MyComplianceReport.com</span>
                	<br/>:: <a href="../profiles/profiles_menu.asp?cid=<% =customerID %>"><% response.write( getCustomerName(customerID) ) %></a>                    
                </div>
            </div>                   
                               
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
            	            <legend>Information</legend>
	                        <div align="left">
    	                        <img src="../_images/icons/16/information.png" alt="Information" width="16" height="16" align="absmiddle" style="margin-right:3px;">Modify the online reporting settings using the form below.
        	                </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             
                                    
                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">               	
                        <div class="infoButtons">
                        	&nbsp;
                        </div>                        
                    </td>
                    <!-- STOP Information buttons -->
                        
                </tr>
            </table>        
            <!-- STOP information box -->

    
    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			'clear system messages
			call clearSessionMessages()
            %>
    		<!-- STOP message notification box -->


            <!-- START Top Page section with tabs & buttons -->
            <table id="table_tabs" width="100%" cellpadding="0" cellspacing="0">            
                <tr>
                
                    <!-- START Top Page Tabs-->
                    <td height="40" width="70%" valign="top">
                        <ul id="ul_tabs" class="tabs">
                            <li id="details" style="width:125px;" class="on" onClick="clickTopTab('details',''); resetSystemMsg('systemMessage');"><div>Display</div></li>
                            <li id="settings" style="width:125px;" class="off" onClick="clickTopTab('settings',''); resetSystemMsg('systemMessage');"><div>Settings</div></li>
                            <!-- the last id must be set to "end" -->
                            <li id="end" class="empty" style="width:150px;"><div>&nbsp;</div></li>                            
                        </ul> 
                        
                        <input type=hidden name="topTab" id="topTab" value="<% =topTab %>">                                                                
                        <input type=hidden name="cid" id="cid" value="<% =customerID %>">
                        
                    </td>                
                    <!-- STOP Top Page Tabs-->              
                    
                    <!-- START Submit Button at top of page--> 
                    <td nowrap style="padding-bottom:10px;" align="left">
                        <input type=hidden name="action" id="action" value="edit">                                
                        <!-- SAVE button -->
						<a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); document.frm.submit(); return false;"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
	                    <!-- CLOSE button -->
						<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); navigateAway('profiles_menu.asp?cid=<% =customerID %>'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>                            
                    </td>
                    <!-- STOP Submit Button -->
                             
                </tr>
            </table>
            <!-- STOP Top Page Tabs -->


            <!-- START Side Page Tabs [details] "id=tab:details" defines the section for the Top Tabs -->
            <!--#include file="../_includes/_INCmcrDetails_.asp"-->            
            <!-- STOP Side Page Tabs [details] -->    
        

            <!-- START Side Page Tabs [settings] "id=tab:settings" defines the section for the Top Tabs -->
            <!--#include file="../_includes/_INCmcrSettings_.asp"-->
            <!-- STOP Side Page Tabs [notes] -->

    
            <!-- START Final table -->        
            <table class="formTable" width="100%" cellpadding="0" cellspacing="0">      
                                                              
                <tr>
                    <td width="70%" valign="top" align="left" nowrap>&nbsp;</td>
                    
                    <!-- START Submit Button at bottom of page -->                 
                    <td valign="top" align="left" nowrap>                
                        <!-- SAVE button -->
						<a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); document.frm.submit(); return false;"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
                        <!-- CLOSE button -->
						<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); navigateAway('profiles_menu.asp?cid=<% =customerID %>'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>                            
                    </td>
                    <!-- STOP Submit Button --> 
    
                </tr>      
                                      
            </table>
            <!-- STOP Final table -->               
    
	    </td>         
	    <!-- STOP Main Middle Section -->

 
    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
	</tr>
</table>
<!-- STOP Page -->

</form>

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<script type="text/javascript"> 
	//load form where user last was...pulled from query URL or form submit
	window.onload = function () {
		clickTopTab('<% =topTab %>','');
	}		
</script>

<script language="javascript">
	//for adding a single investigation using 
	//the ADD button
	function navigateAway(url) {
		var title = "Cancel Changes";
		var msg = "Changes have been made to this profile and <strong>must<br>be saved</strong> before continuing.<br><br>Continue and cancel changes?";		
		//tinyMCE.triggerSave();	//trigger save/convert of all TinyMCE text areas for proper save
		if(isFormChanged(document.frm)==true) {		
			confirmPrompt(title,msg,'redirect',url);
		}
		else {
			window.location = url;
		}
	}		
</script>

<script type="text/javascript"> 
    // Initialize TinyMCE with the tab_focus option 
    tinyMCE.init({ 
        mode : "exact",
        elements : "<% =activeElements %>",
        auto_resize : true,
        theme : "advanced",
        content_css : "../_css/tinyMCE.css",
        plugins : "searchreplace,fullscreen,insertdatetime,paste,print,tinyautosave",		
		theme_advanced_buttons1 : "newdocument,paragraphinsert,print,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect,|,tinyautosave,|,fullscreen",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,undo,redo,|,bullist,numlist,hr,|,outdent,indent,blockquote,|,insertdate,inserttime,preview,|,forecolor,backcolor,|,link,|,print",
		theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top", 
        theme_advanced_toolbar_align : "left", 
        onchange_callback : "setTinyMCEdirty",
        plugin_insertdate_dateFormat : "%A, %B %d, %Y",
        plugin_insertdate_timeFormat : "%I:%M %p",
		setup : function(ed) { 
				// add CUSTOM PARAGRAPH button 
				// used by CCI staff only on issues_edit_00.asp
				ed.addButton('paragraphinsert', { 
					title : 'Insert Paragraph', 
					image : '../_images/icons/20/document_insert.png', 
					onclick : function() { 
						//set focus to active editor
						ed.focus(); 					
						//open custom paragraphs for insert
						SimpleModal.open('../_dialogs/popup_paragraphs.asp', 500, 625, 'no');
					} 
				}); 
		} 
		
    }); 

	//append select text
	function insertHTML(text) {
		//appends text to END of current content	
		tinyMCE.activeEditor.setContent(tinyMCE.activeEditor.getContent() + decodeURI(text));	
	}

</script>   

<script>
	function configDropDownOption(obj,list) {
		//used to rebuild options list with new values
		var el = document.getElementById(obj);
		var selIndex = el.selectedIndex;
		var selectedValue;
		//get current option value selected
		if (selIndex != -1) {
			for(i=el.length-1; i>=0; i--)
			{
			  if(el.options[i].selected)
			  {
				if(el.options[i].value){
					selectedValue = el.options[i].value; //current selection
				}
			  }
			}
		}
			
		//removed current selects in dropdown
		el.length = 0;
				
		//add new values to dropdown
		var i;		
		var temp = list.split("|"); //split list provided by popup_config_options.asp
		//add new options provided by user
		addOptionsItems(obj,"","-- Select --");
		for(i=0; i<temp.length; i++) {
			if (temp[i].length>1) {
				addOptionsItems(obj,temp[i],temp[i]);
			}
		}
		
		//check for existance of originally selected value		
		if(selectedValue){
			var bolMatch = false;
			i = 0;		
			for (i=0; i<el.options.length; i++) {
				if (el.options[i].text == selectedValue) {
					el.selectedIndex = i;
					bolMatch = true; //origianl still exists in list
				}
			}		
			//user removed original value, put back with "Missing:" at end of list
			if (bolMatch == false){
				addOptionsItems(obj,selectedValue,"Missing:"+selectedValue);
				el.selectedIndex = el.options.length-1;
			}
		}

	}		
</script>
