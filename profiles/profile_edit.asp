<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = false
const showDHTMLMenu = true
const helpID = 6
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "profile"
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
'	if customerID = "" then
'		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
'	end if
end if

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

dim showCRSID '...need for NEW search box

dim startDate
dim endDate
dim optArr
dim cusName
dim cusDisplayAs
dim cusAddress
dim cusCity
dim cusState
dim cusZip
dim cusCountry
dim cusIndustry
dim cusEmployeeCount
dim cusContractDate
dim cusStartDate
dim cusCanceled
dim cusPhone_Work
dim cusPhone_Fax
dim cusPhone_Customer
dim cusPhone_Other
dim cusRetention
dim cusQuestion
dim cusAppShowUserFields
dim cusAppAssignFUPLogid
dim cusAppAssignFUPCategory
dim cusAppPopUpSubCategoryRS
dim cusNotes

'get/set customerID
if customerID = "" and action = "edit" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
end if


'Get action --  this must come before "get/set customerID" below
dim action : action = trim(Request.QueryString("action"))
if len(action) = 0 then
	action = trim(Request.Form("action"))
end if
action = lCase(action)
if  action <> "edit" _
and action <> "add"  _
and action <> "del" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if


'make sure user can update this page
if action = "edit" and (cLng(session(session("siteID") & "adminLoggedOn")) > 2 and programAdmin = "N") then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if


'make sure user can update this record -- PUT BACK programAdmin = "N" when ready for customers to view
if action = "add" and cLng(session(session("siteID") & "adminLoggedOn")) > 2 then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if

'TinyMCE
dim activeElements : activeElements = "cusNotes"

'used to set side top and side tabs after user saves record
dim topTab
topTab = trim(Request.QueryString("top"))
if len(topTab) <= 0 then
	topTab = "details"
end if


'Get Record Details
if action = "edit" then

	mySQL="SELECT Customer.* " _
		& "		FROM Customer " _
		& "		WHERE CustomerID = '" & customerID & "' "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	else			
		'only assign when editing a customer
		cusName					= rs("Name")
		cusDisplayAs			= rs("DisplayAs")
		cusAddress				= rs("Address")
		cusCity					= rs("City")
		cusState				= rs("State")
		cusZip					= rs("PostalCode")
		cusCountry				= rs("Country")
		cusIndustry				= rs("Industry")
		cusEmployeeCount		= rs("EmployeeCount")
		cusContractDate			= rs("ContractDate")
		cusStartDate			= rs("StartDate")
		cusCanceled				= rs("Canceled")
		cusPhone_Work			= rs("Phone_Work")
		cusPhone_Fax			= rs("Phone_Fax")
		cusPhone_Customer		= rs("Phone_Customer")
		cusPhone_Other			= rs("Phone_Other")
		cusRetention			= rs("Retention")				
		cusQuestion				= rs("appShowIssueQuestions")			
		cusAppShowUserFields	= rs("appShowUnassignedUserFields")			
		cusAppAssignFUPLogid	= rs("appAssignFUPLogid")			
		cusAppAssignFUPCategory	= rs("appAssignFUPCategory")			
		cusAppPopUpSubCategoryRS= rs("appPopUpSubCategoryRS")			
		cusNotes				= rs("Notes")			
	end if
	call closeRS(rs)
	
end if

if len(cusCanceled) <= 0 then
	cusCanceled = "0"
end if

dim activeCalendars : activeCalendars = "'cusContractDate','cusStartDate'"

%>

<% 
dim pageTitle
if action = "edit" then
	pageTitle = "Customer: " & cusName 
else
	pageTitle = "Customer: (new customer)"
end if
%>
<!--#include file="../_includes/_INCheader_.asp"-->

<form name="frm" id="frm" method="post" action="profile_exec.asp" style="padding:0px; margin:0px;">

<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
  
	    <!-- START Left sided area -->  
		<td width="190" align="right" valign="top" >    

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">
    
                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->              
                    <script>
						stCollapseSubTree('myCMtree',1,0);
						stCollapseSubTree('myCMtree',2,0);
						stCollapseSubTree('myCMtree',3,0);
       	            </script>
                    <!-- STOP side menu include -->
                                    
                </td>
              </tr>
              <!-- STOP Left sided menu -->
        
            </table>

		</td>
    	<!-- STOP Left sided area -->


        <!-- START Main Middle Section -->
        <td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">    

			<%
			'used to STOP updating the cookie crumb if page is being recalled from a SAVE
			dim updateCrumb
			if inStr(1, session(session("siteID") & "okMsg"), "was Added") > 0 then
				'do nothing...will update cookie
            elseif len(session(session("siteID") & "errMsg")) > 0 or len(session(session("siteID") & "okMsg")) > 0 then
				updateCrumb = "false"
            end if
			%>
            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs('<% =updateCrumb %>'));
       	        </script>
            </div>
    
            <div style="text-align:left; margin-bottom:10px;">				
                <div style="float:left; padding-right:2px; margin-right:2px;"><img src="../_images/icons/32/briefcase.png" title="" width="32" height="32" align="absmiddle" style="vertical-align:middle;"></div>
                <div>
                	<span class="pageTitle"><% =accountLabelSingle %>: <% if action = "edit" then response.write(cusName) else response.Write("<i>&lt;none assigned&gt;</i>") %></span>
                </div>
            </div>                   
                               
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
            	            <legend>Information</legend>
	                        <div align="left">
    	                        <img src="../_images/icons/16/information.png" alt="Information" width="16" height="16" align="absmiddle" style="margin-right:3px;">Modify the profile properties using the form below.
								<% response.write( getInlineHelp("getPageURL", 0) ) %>
        	                </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             
                                    
                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">               	
                        <div class="infoButtons">
                            <a href="#" onClick="SimpleModal.open('../_dialogs/popup_profile_copy.asp?cid=<% =customerID %>&action=copy', 300, 425, 'no'); return false;"><img src="../_images/icons/24/copy_profile.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                            <a href="#" onClick="SimpleModal.open('../_dialogs/popup_profile_copy.asp?cid=<% =customerID %>&action=copy', 300, 425, 'no'); return false;">Copy Profile</a>
                        </div>
                        
                    </td>
                    <!-- STOP Information buttons -->
                        
                </tr>
            </table>        
            <!-- STOP information box -->

    
    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			'clear system messages
			call clearSessionMessages()
            %>
    		<!-- STOP message notification box -->


            <!-- START Top Page section with tabs & buttons -->
            <table id="table_tabs" width="100%" cellpadding="0" cellspacing="0">            
                <tr>
                
                    <!-- START Top Page Tabs-->
                    <td height="40" width="70%" valign="top">
                        <ul id="ul_tabs" class="tabs">
                            <li id="details" style="width:125px;" class="on" onClick="clickTopTab('details',''); resetSystemMsg('systemMessage');"><div>Profile</div></li>
                            <li id="settings" style="width:125px;<% if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 then response.write("display:none;")%>" class="off" onClick="clickTopTab('settings',''); resetSystemMsg('systemMessage');"><div>Settings</div></li>
                            <!-- the last id must be set to "end" -->
                            <li id="end" class="empty" style="width:150px;"><div>&nbsp;</div></li>                            
                        </ul> 
                        
                        <input type=hidden name="topTab" id="topTab" value="<% =topTab %>">                                                                
                        
                    </td>                
                    <!-- STOP Top Page Tabs-->              
                    
                    <!-- START Submit Button at top of page--> 
                    <td nowrap style="padding-bottom:10px;" align="left">
                        <%
                        if action = "edit" then
                        %>
                            <input type=hidden name="action" id="action" value="edit">                                
                            <!-- SAVE button -->
							<a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); document.frm.submit(); return false;"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
	                        <!-- CLOSE button -->
							<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); navigateAway('profiles_menu.asp?cid=<% =customerID %>'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>                            
                        <%
                        elseif action = "add" then
                        %>
                            <input type=hidden name=action value="<%=action%>">
                            <!-- ADD button -->                            
							<a class="myCMbutton" href="#" accesskey="A" onClick="this.blur(); document.frm.submit(); return false;"><span class="add" style="padding-right:15px;"><u>A</u>dd</span></a>
	                        <!-- CLOSE button -->
							<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); navigateAway('profiles.asp'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>                            
                        <%
                        end if
                        %>
                    </td>
                    <!-- STOP Submit Button -->
                             
                </tr>
            </table>
            <!-- STOP Top Page Tabs -->


            <!-- START Side Page Tabs [details] "id=tab:details" defines the section for the Top Tabs -->
            <!--#include file="../_includes/_INCprofileDetails_.asp"-->            
            <!-- STOP Side Page Tabs [details] -->    
        

            <!-- START Side Page Tabs [settings] "id=tab:settings" defines the section for the Top Tabs -->
            <!--#include file="../_includes/_INCprofileSettings_.asp"-->
            <!-- STOP Side Page Tabs [notes] -->

    
            <!-- START Final table -->        
            <table class="formTable" width="100%" cellpadding="0" cellspacing="0">      
                                                              
                <tr>
                    <td width="70%" valign="top" align="left" nowrap>&nbsp;</td>
                    
                    <!-- START Submit Button at bottom of page -->                 
                    <td valign="top" align="left" nowrap>                
                        <%
                        if action = "edit" then
                        %>
                            <!-- SAVE button -->
							<a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); document.frm.submit(); return false;"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
	                        <!-- CLOSE button -->
							<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); navigateAway('profiles_menu.asp?cid=<% =customerID %>'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>                            
                        <%
                        elseif action = "add" then
                        %>
                            <!-- ADD button -->                            
							<a class="myCMbutton" href="#" accesskey="A" onClick="this.blur(); document.frm.submit(); return false;"><span class="add" style="padding-right:15px;"><u>A</u>dd</span></a>
	                        <!-- CLOSE button -->
							<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); navigateAway('profiles.asp'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>                            
                        <%
                        end if
                        %>
                    </td>
                    <!-- STOP Submit Button --> 
    
                </tr>      
                                      
            </table>
            <!-- STOP Final table -->               
    
	    </td>         
	    <!-- STOP Main Middle Section -->

 
    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
	</tr>
</table>
<!-- STOP Page -->

</form>

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<script type="text/javascript"> 
	//load form where user last was...pulled from query URL or form submit
	window.onload = function () {
		clickTopTab('<% =topTab %>','');
	}		
</script>

<script language="javascript">
	//for adding a single investigation using 
	//the ADD button
	function navigateAway(url) {
		var title = "Cancel Changes";
		var msg = "Changes have been made to this profile and <strong>must<br>be saved</strong> before continuing.<br><br>Continue and cancel changes?";		
		//tinyMCE.triggerSave();	//trigger save/convert of all TinyMCE text areas for proper save
		if(isFormChanged(document.frm)==true) {		
			confirmPrompt(title,msg,'redirect',url);
		}
		else {
			window.location = url;
		}
	}		
</script>

<script type="text/javascript"> 
    // Initialize TinyMCE with the tab_focus option 
    tinyMCE.init({ 
        mode : "exact",
        elements : "<% =activeElements %>",
        auto_resize : true,
        theme : "advanced",
        content_css : "../_css/tinyMCE.css",
        plugins : "tabfocus,searchreplace,fullscreen,insertdatetime,paste,inlinepopups",
        theme_advanced_buttons1 : "bold,italic,underline,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,fontsizeselect,forecolor,backcolor,separator,search,separator,pasteword,separator,insertdate,inserttime,separator,fullscreen",
        theme_advanced_buttons2 : "", 
        theme_advanced_buttons3 : "", 
        theme_advanced_toolbar_location : "top", 
        theme_advanced_toolbar_align : "center", 
        onchange_callback : "setTinyMCEdirty",
        plugin_insertdate_dateFormat : "%A, %B %d, %Y",
        plugin_insertdate_timeFormat : "%I:%M %p",
        tab_focus : ':prev,:next'
    }); 
</script>     

<script type="text/javascript"> 
	// Initialize TinyMCE with the tab_focus option 
	var dateCal;
	dateCal = new dhtmlxCalendarObject([<% =activeCalendars %>], true, {isWinHeader:true, headerButtons:"XT", isWinDrag:false, isYearEditable:true, isMonthEditable:true });
	dateCal.setSkin("simplegrey");
	dateCal.setDateFormat("%m/%d/%Y");
	dateCal.setYearsRange(2000, 2020);
	dateCal.setHeaderText("Issue Date");
</script>     

<script>
	function configDropDownOption(obj,list) {
		//used to rebuild options list with new values
		var el = document.getElementById(obj);
		var selIndex = el.selectedIndex;
		var selectedValue;
		//get current option value selected
		if (selIndex != -1) {
			for(i=el.length-1; i>=0; i--)
			{
			  if(el.options[i].selected)
			  {
				if(el.options[i].value){
					selectedValue = el.options[i].value; //current selection
				}
			  }
			}
		}
			
		//removed current selects in dropdown
		el.length = 0;
				
		//add new values to dropdown
		var i;		
		var temp = list.split("|"); //split list provided by popup_config_options.asp
		//add new options provided by user
		addOptionsItems(obj,"","-- Select --");
		for(i=0; i<temp.length; i++) {
			if (temp[i].length>1) {
				addOptionsItems(obj,temp[i],temp[i]);
			}
		}
		
		//check for existance of originally selected value		
		if(selectedValue){
			var bolMatch = false;
			i = 0;		
			for (i=0; i<el.options.length; i++) {
				if (el.options[i].text == selectedValue) {
					el.selectedIndex = i;
					bolMatch = true; //origianl still exists in list
				}
			}		
			//user removed original value, put back with "Missing:" at end of list
			if (bolMatch == false){
				addOptionsItems(obj,selectedValue,"Missing:"+selectedValue);
				el.selectedIndex = el.options.length-1;
			}
		}

	}		
</script>
