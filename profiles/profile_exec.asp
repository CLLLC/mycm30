<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->

<%
'Database
dim mySQL, cn, rs, rs2

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'fields in DETAILS tab
dim cusName
dim cusDisplayAs
dim cusAddress
dim cusCity
dim cusState
dim cusZip
dim cusCountry
dim cusIndustry
dim cusEmployeeCount
dim cusContractDate
dim cusStartDate
dim cusCanceled
dim cusPhone_Work
dim cusPhone_Fax
dim cusPhone_Customer
dim cusPhone_Other
dim cusRetention
dim optTypes
dim cusQuestion
dim cusAppShowUserFields
dim cusAppAssignFUPLogid
dim cusAppAssignFUPCategory
dim cusAppPopUpSubCategoryRS
dim cusNotes

'flag right now as when this issue was modified
dim modifiedDate : modifiedDate = Now()

'Get action --  this must come before "get customerID" below
dim action
action = trim(lCase(Request.Form("action")))
if len(action) = 0 then
	action = trim(lCase(Request.QueryString("action")))
end if
if  action <> "edit" _
and action <> "add" _
and action <> "del" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if

'Get Customer ID
if action = "edit" or action = "add" or action = "del" then
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'make sure user can update this record
if programAdmin = "N" and cLng(session(session("siteID") & "adminLoggedOn")) > 2 then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if


'used to redirect user back to tabs they last were on
dim topTab, sideTab
topTab = trim(Request.Form("topTab"))
if len(topTab) = 0 then
	topTab = trim(Request.QueryString("top"))
end if


'Get Location Details
if action = "edit" or action = "add" then

	cusName = trim(Request.Form("cusName"))
	cusDisplayAs = trim(Request.Form("cusDisplayAs"))
	cusAddress = trim(Request.Form("cusAddress"))
	cusCity = trim(Request.Form("cusCity"))
	cusState = trim(Request.Form("cusState"))
	cusZip = trim(Request.Form("cusZip"))
	cusCountry = trim(Request.Form("cusCountry"))
	cusIndustry = trim(Request.Form("cusIndustry"))
	cusEmployeeCount = trim(Request.Form("cusEmployeeCount"))
	cusContractDate = trim(Request.Form("cusContractDate"))
	cusStartDate = trim(Request.Form("cusStartDate"))
	cusCanceled = trim(Request.Form("cusCanceled"))
	cusPhone_Work = trim(Request.Form("cusPhone_Work"))
	cusPhone_Fax = trim(Request.Form("cusPhone_Fax"))
	cusPhone_Customer = trim(Request.Form("cusPhone_Customer"))
	cusPhone_Other = trim(Request.Form("cusPhone_Other"))
	cusRetention = trim(Request.Form("cusRetention"))
	optTypes = trim(Request.Form("list_checkBox"))
	cusQuestion	= trim(Request.Form("cusQuestion"))
	cusAppShowUserFields = trim(Request.Form("cusAppShowUserFields"))
	cusAppAssignFUPLogid = trim(Request.Form("cusAppAssignFUPLogid"))
	cusAppPopUpSubCategoryRS = trim(Request.Form("cusAppPopUpSubCategoryRS"))
	cusAppAssignFUPCategory	= trim(Request.Form("cusAppAssignFUPCategory"))
	cusNotes = trim(Request.Form("cusNotes"))
						
end if


'*********************************
'get array of issue types selected
'*********************************
if action = "edit" or action = "add" then
	dim expI, arrType, typeExist, custIssueTypeID
	
	arrType = split(optTypes,",")
	optTypes = ""
	'loop through all selected issue types one by one
	for expI = 0 to UBound(arrType)
		optTypes = optTypes & "," & trim(arrType(expI))
	next	
		
	'get master list of issue types
	if len(trim(optTypes)) > 0 then

		expI = ""
		arrType = null
		mySQL = "SELECT IssueTypeID, Name FROM IssueType ORDER BY IssueTypeID"
		set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
		do until rs.eof
			arrType = arrType & "," & rs("IssueTypeID")
			rs.movenext
		loop
					
		'build array of types
		arrType = split(arrType,",")
		
		'loop through all available types
		for expI = 0 to UBound(arrType)
		
			if len(trim(arrType(expI))) > 0 then
			
				'check for previous existance
				mySQL = "SELECT CustIssueTypeID FROM Customer_IssueType " _
					  & "WHERE IssueTypeID = " & trim(arrType(expI)) & " and CustomerID = '" & uCase(customerID) & "' "
				set rs = openRSexecute(mySQL)
				if rs.eof then
					custIssueTypeID = ""
				else
					custIssueTypeID = rs("custIssueTypeID")
				end if
			
				'not already assiged, go ahead and add
				if inStr(optTypes,trim(arrType(expI))) > 0 and len(custIssueTypeID) <= 0 then
					mySQL = "INSERT INTO Customer_IssueType (" _
						  & "CustomerID,IssueTypeID,Active,ModifiedBy,ModifiedDate" _
						  & ") VALUES (" _
						  & "'" & uCase(customerID) & "'," & trim(arrType(expI)) & ",'Y'," & sLogid & ",'" & modifiedDate & "' " _
						  & ")"
					set rs = openRSexecute(mySQL)								
				
				'found, but needs to be deactivated
				elseif inStr(optTypes,trim(arrType(expI))) <= 0 and len(custIssueTypeID) > 0 then							
					mySQL = "UPDATE Customer_IssueType SET " _
						  & "Active = '"   		& "N" 					& "'," _
						  & "ModifiedBy="		& sLogid				& ", " _
						  & "ModifiedDate='"    & modifiedDate			& "' " _					  
						  & "WHERE CustomerID = '" 	& uCase(customerID) & "' " _ 
						  & "  AND CustIssueTypeID = " & custIssueTypeID		  		  		  		    
					set rs = openRSexecute(mySQL)

				'found, make sure it's active
				elseif inStr(optTypes,trim(arrType(expI))) > 0 and len(custIssueTypeID) > 0 then							
					mySQL = "UPDATE Customer_IssueType SET " _
						  & "Active = '"   		& "Y" 					& "'," _
						  & "ModifiedBy="		& sLogid				& ", " _
						  & "ModifiedDate='"    & modifiedDate			& "' " _					  
						  & "WHERE CustomerID = '" 	& uCase(customerID) & "' " _ 
						  & "  AND CustIssueTypeID = " & custIssueTypeID		  		  		  		    
					set rs = openRSexecute(mySQL)

				end if

			end if
		
		next

	else
		'inform user of bad selection
		'response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Issue Types. At least one (1) Issue Type must be selected.")

		mySQL = "UPDATE Customer_IssueType SET " _
			  & "Active = '"   		& "N" 					& "'," _
			  & "ModifiedBy="		& sLogid				& ", " _
			  & "ModifiedDate='"    & modifiedDate			& "' " _					  
			  & "WHERE CustomerID = '" 	& uCase(customerID) & "' "
		set rs = openRSexecute(mySQL)

	end if
	
end if


'ADD
if action = "add" then

	dim fieldsSQL, valueSQL

	'Add Issue with Callback
	fieldsSQL = "INSERT INTO Customer (" _
			  & "CustomerID,Name,DisplayAs,Address,City,State,PostalCode,Country,Industry,Canceled, " _
			  & "Phone_Work,Phone_Fax,Phone_Customer,Phone_Other, " _
			  & "appShowIssueQuestions,appShowUnassignedUserFields,appAssignFUPLogid,appPopUpSubCategoryRS,appAssignFUPCategory,Notes, " _
			  & "ModifiedBy,ModifiedDate"

	'check for empty dates	      	
	if len(cusEmployeeCount) > 0 then fieldsSQL = fieldsSQL & ",EmployeeCount"		  
	if len(cusContractDate) > 0 then fieldsSQL = fieldsSQL & ",ContractDate"		  
	if len(cusStartDate) > 0 then fieldsSQL = fieldsSQL & ",StartDate"		  
	if len(cusRetention) > 0 then fieldsSQL = fieldsSQL & ",Retention"		  
	fieldsSQL = fieldsSQL & ") "
		  
	valueSQL = " VALUES (" _
		  & "'"    	& uCase(customerID)					& "'," _
		  & "'"    	& replace(cusName,"'","''")			& "'," _
		  & "'"    	& replace(cusDisplayAs,"'","''")	& "'," _
		  & "'"    	& replace(cusAddress,"'","''")		& "'," _
		  & "'"    	& cusCity							& "'," _
		  & "'"    	& cusState							& "'," _
		  & "'"    	& cusZip							& "'," _
		  & "'"    	& cusCountry						& "'," _
		  & "'"    	& cusIndustry						& "'," _
		  & "'"    	& cusCanceled						& "'," _
		  & "'"    	& cusPhone_Work						& "'," _
		  & "'"    	& cusPhone_Fax						& "'," _
		  & "'"    	& cusPhone_Customer					& "'," _
		  & "'"    	& cusPhone_Other					& "'," _		  
		  & "'" 	& cusQuestion						& "'," _		  
		  & "'" 	& cusAppShowUserFields				& "'," _		  		  				  
		  & "'" 	& cusAppAssignFUPLogid				& "'," _		  		  		
		  & "'" 	& cusAppPopUpSubCategoryRS			& "'," _		  		  				  
		  & "'" 	& cusAppAssignFUPCategory			& "'," _		  		  				  		  		  		  
		  & "'"    	& replace(cusNotes,"'","''")		& "'," _		  
		  & " "    	& sLogid							& ", " _
		  & "'"    	& modifiedDate						& "' "

	'check for empty dates
	if len(cusEmployeeCount) > 0 then valueSQL = valueSQL & "," & cusEmployeeCount & " "		  
	if len(cusContractDate) > 0 then valueSQL = valueSQL & ",'" & cusContractDate & "' "		  
	if len(cusStartDate) > 0 then valueSQL = valueSQL & ",'" & cusStartDate & "' "		  
	if len(cusRetention) > 0 then valueSQL = valueSQL & "," & cusRetention & " "		  
	valueSQL = valueSQL & ") "

	'update issue
	set rs = openRSexecute(fieldsSQL & valueSQL)
	
	session(session("siteID") & "okMsg") = accountLabelSingle & " was Added."
	response.redirect "profile_edit.asp?action=edit&cid=" & customerID & "&top=" & topTab
	
end if


'EDIT
if action = "edit" then
		
	'Update Record
	mySQL = "UPDATE Customer SET " _	
		  & "Name='"   						& replace(cusName,"'","''")			& "'," _
		  & "DisplayAs='"    				& replace(cusDisplayAs,"'","''")	& "'," _
		  & "Address='"    					& replace(cusAddress,"'","''")		& "'," _
		  & "City='"    					& cusCity							& "'," _
		  & "State='"    					& cusState							& "'," _
		  & "PostalCode='" 					& cusZip							& "'," _
  		  & "Country='" 					& cusCountry						& "'," _
		  & "Industry='"     				& cusIndustry						& "'," _		  
		  & "Canceled='"     				& cusCanceled						& "'," _
		  & "Phone_Work='"     				& cusPhone_Work						& "'," _		  		  
		  & "Phone_Fax='"     				& cusPhone_Fax						& "'," _
		  & "Phone_Customer='" 				& cusPhone_Customer					& "'," _
		  & "Phone_Other='" 				& cusPhone_Other					& "'," _		  		  
		  & "appShowIssueQuestions='" 		& cusQuestion						& "'," _		  
		  & "appShowUnassignedUserFields='" & cusAppShowUserFields				& "'," _		  		  				  
		  & "appAssignFUPLogid='" 			& cusAppAssignFUPLogid				& "'," _		  		  		
		  & "appPopUpSubCategoryRS='" 		& cusAppPopUpSubCategoryRS			& "'," _		  		  				  		  
		  & "appAssignFUPCategory='" 		& cusAppAssignFUPCategory			& "'," _		  		  				  		  		  
		  & "Notes='"     					& replace(cusNotes,"'","''")		& "'," _		  
		  & "ModifiedBy="  					& sLogid							& ", " _
		  & "ModifiedDate='"				& modifiedDate						& "' "

	'check for special existance of dates  
	if len(cusEmployeeCount) > 0 then mySQL = mySQL & ",EmployeeCount=" & cusEmployeeCount & " " else mySQL = mySQL & ",EmployeeCount=NULL "
	if len(cusContractDate) > 0 then mySQL = mySQL & ",ContractDate='" & cusContractDate & "' " else mySQL = mySQL & ",ContractDate=NULL "
	if len(cusStartDate) > 0 then mySQL = mySQL & ",StartDate='" & cusStartDate & "' " else mySQL = mySQL & ",StartDate=NULL "
	if len(cusRetention) > 0 then mySQL = mySQL & ",Retention=" & cusRetention & " " else mySQL = mySQL & ",Retention=NULL "
		  
	'finalize update query
	mySQL = mySQL & "WHERE CustomerID = '" & customerID & "' "

	'update issue
	set rs = openRSexecute(mySQL)

	session(session("siteID") & "okMsg") = accountLabelSingle & " has been saved successfully."
	response.redirect "profile_edit.asp?action=edit&cid=" & customerID & "&top=" & topTab

end if


'DELETE
if action = "del" then

	'Mark for deletion from Customers
	mySQL = "UPDATE Customer Set " _
			& "Cancel=-1 " _	
			& "WHERE CustomerID='" & customerID & "' "   
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	session(session("siteID") & "okMsg") = accountLabelSingle & " was Deleted."
	response.redirect "profiles.asp"
	
end if


'Just in case we ever get this far...
call closeDB()
Response.Redirect "profiles.asp"

%>
