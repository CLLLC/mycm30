<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->

<%
'Database
dim mySQL, cn, rs, rs2

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'customerID of requested profile
dim customerID, customerName
customerID = trim(lCase(Request.QueryString("cid")))
if customerID = "" then
	customerID = request.form("cid")
	if customerID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	end if
end if

'validate security access
if loadUser(customerID) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'fields in DETAILS tab
dim cusNumUsers
dim cusPasswordStrength
dim cusSendFromEmail
dim cusDeliverySystem
dim cusOwnerOverride

'flag right now as when this issue was modified
dim modifiedDate : modifiedDate = Now()

'Work Fields
dim action

'Get action --  this must come before "get customerID" below
action = trim(lCase(Request.Form("action")))
if len(action) = 0 then
	action = trim(lCase(Request.QueryString("action")))
end if
if  action <> "edit" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if


'make sure user can update this record
if cLng(session(session("siteID") & "adminLoggedOn")) > 2 then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if


'used to redirect user back to tabs they last were on
dim topTab, sideTab
topTab = trim(Request.Form("topTab"))
if len(topTab) = 0 then
	topTab = trim(Request.QueryString("top"))
end if


'Get Security Details
if action = "edit" then
	cusNumUsers 		 = trim(Request.Form("cusNumUsers"))
	cusPasswordStrength  = trim(Request.Form("cusPasswordStrength"))				
	cusSendFromEmail 	 = trim(Request.Form("cusSendFromEmail"))		
	cusDeliverySystem	 = trim(Request.Form("cusDeliverySystem"))		
	cusOwnerOverride 	 = trim(Request.Form("cusOwnerOverride"))			
end if

'EDIT
if action = "edit" then
		
	'Update Record
	mySQL = "UPDATE Customer SET " _	
		  & "appPasswordStrength='" 	& cusPasswordStrength	& "'," _		  
		  & "appSendFromEmail='" 		& cusSendFromEmail		& "'," _		  		  		  
		  & "appDeliverySystem='" 		& cusDeliverySystem		& "'," _
		  & "ModifiedBy="  				& sLogid				& ", " _
		  & "ModifiedDate='"			& modifiedDate			& "' "
		  
	if len(cusNumUsers) > 0 then mySQL = mySQL & ",appNumUsers=" & cusNumUsers & " " else mySQL = mySQL & ",appNumUsers=NULL "
	if len(cusOwnerOverride) > 0 then mySQL = mySQL & ",appOwnerOverride=" & cusOwnerOverride & " " else mySQL = mySQL & ",appOwnerOverride=NULL "
			  
	'finalize update query
	mySQL = mySQL & "WHERE CustomerID = '" & customerID & "' "

	'update issue
	set rs = openRSexecute(mySQL)	

	session(session("siteID") & "okMsg") = "Security Settings have been saved successfully."
	response.redirect "security_edit.asp?action=edit&cid=" & customerID & "&top=" & topTab

end if


'Just in case we ever get this far...
call closeDB()
Response.Redirect "profiles.asp"

%>
