<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
Response.Expires = 0
Response.CacheControl = "no-cache" 
Response.AddHeader "pragma", "no-cache" 
Server.ScriptTimeout =25000

const showSearchBox = true
const showDHTMLMenu = true
const helpID = 6
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = "customers"
'===================================================================

'Database
dim mySQL, cn, rs

'Open Database Connection
call openDB()

'*********************************************************	
'validate user security, redirect to login if "false"
'*********************************************************	
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")

'validate user security
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'Products
dim action
dim tableProperty
dim I
dim item
dim pageSize
dim totalPages
dim showArr
dim sortField
dim sortOrder
dim curPage

dim showCRSID

'make sure user can update this page
if cLng(session(session("siteID") & "adminLoggedOn")) >= 10 and programAdmin = "N" then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
end if

%>

<% dim pageTitle : pageTitle = accountLabelPlural %>
<!--#include file="../_includes/_INCheaderLite_.asp"-->


<!-- START Page Loader and inform user content being loaded -->
<div id="loaderContainer" style="margin-top:15px; margin-left:15px; border:1px solid #B3B3B3; background-color:#FFFFFF; padding:2px; width:225px;">
	<img src="../_images/myloader.gif" alt="loading..." align="absmiddle" style="margin-left:5px; margin-right:10px; vertical-align:middle;" />Loading content, please wait...
</div>
<%
'push to window right now!
response.flush()
%>
<!-- STOP Page Loader -->
    
    
<!-- START Page -->
<table id="masterTable" style="background:#FFF; display:none;" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
        
        <!-- START Left sided area -->
		<td width="190" align="right" valign="top">

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided tree menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">                

                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->        
                    <% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then %>      
	                    <script>
							stExpandSubTree('myCMtree',1,0)
       		            </script>
            		<% else %>
	                    <script>
							stCollapseSubTree('myCMtree',1,0);
							stCollapseSubTree('myCMtree',2,0);							
       		            </script>
                    <% end if %>                          
                    <!-- STOP side menu include -->
                    
                </td>
              </tr>
              <!-- END Left sided tree menu -->
        
            </table>
        
        </td>
        <!-- STOP Left sided area -->
            
	    <!-- START Main Middle Section -->
		<td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">       

            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs());
       	        </script>
            </div>
            
            <div style="text-align:left; margin-bottom:10px;">				
                <div style="float:left; padding-right:2px; margin-right:2px;"><img src="../_images/icons/32/building.png" title="" width="32" height="32" align="absmiddle" style="vertical-align:middle;"></div>
                <div>
                	<span class="pageTitle"><% =accountLabelPlural %></span>
                </div>
            </div>                   

            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
                        <legend>Information</legend>
                        <div align="left">
	                        <img src="../_images/icons/16/information.png" title="Information" width="16" height="16" align="absmiddle" style="margin-right:3px; vertical-align:middle;">Below is a list of <% =lCase(accountLabelPlural) %>. Click on 'Name' to view profile.
							<% response.write( getInlineHelp("getPageURL", 0) ) %>
                        </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             

                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">
                    	<% if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then %>
                            <div class="infoButtons">
                                <a href="profile_edit.asp?action=add"><img src="../_images/icons/24/building_add.png" width="24" height="24" align="absmiddle" style="margin-right:3px; vertical-align:middle;"/></a>
                                <a href="profile_edit.asp?action=add">Add <% =accountLabelSingle %></a>
                            </div>                   
						<% end if %>                                                
                    </td>
                    <!-- STOP Information buttons -->
                                                                                
                </tr>
            </table>        
            <!-- STOP information box -->
    

    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			'clear system messages
			call clearSessionMessages()
            %>            
    		<!-- STOP message notification box -->


            <!-- START Reports table -->
            <%
			'CCI Admin/DBAdmin (bryan or steve)
			if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then			
				mySQL = "SELECT CustomerID, Name, City, State, Industry, EmployeeCount, appDeliverySystem AS [Users]  " _
					  & "	FROM Customer " _
					  & "	WHERE Canceled=0 " _
					  & "	ORDER BY Name "			  				  

			'CCI Staff RA/RS
			elseif cLng(session(session("siteID") & "adminLoggedOn")) < 10 then					
				mySQL = "SELECT CustomerID, Name, City, State, Industry, EmployeeCount, appDeliverySystem AS [Users] " _
					  & "	FROM Customer " _
					  & "	WHERE Canceled=0 AND " _
					  & "	 	EXISTS ( " _
					  & "			SELECT Customer_IssueType.CustomerID " _
					  & "				FROM Customer_IssueType " _
					  & "				WHERE (Customer_IssueType.CustomerID=Customer.CustomerID) AND (Customer_IssueType.IssueTypeID=1 OR Customer_IssueType.IssueTypeID=2) ) " _
					  & "	ORDER BY Name "
					  
			'everyone else
			else
				mySQL = "SELECT CustomerID, Name, City, State, Industry, EmployeeCount, appNumUsers AS [Users] " _
					  & "	FROM Customer " _
					  & "	WHERE Canceled=0 AND " _
					  & "		EXISTS ( " _
					  & "			SELECT vwLogins_IssueType.CustomerID, vwLogins_IssueType.LogID " _
					  & "				FROM vwLogins_IssueType " _
					  & "				WHERE (vwLogins_IssueType.CustomerID=Customer.CustomerID) AND (vwLogins_IssueType.LogID=" & sLogid & ") AND (vwLogins_IssueType.Active='Y') ) " _
					  & "	ORDER BY Name "			  				  
			
			end if				  
			'open reports table...				  
	        set rs = openRSopen(mySQL,0,adOpenStatic,adLockReadOnly,adCmdText,pageSize)
			%>

            <!-- START Reports table -->
			<table id="customerTable" width="100%" style="background-color:#969696;" cellpadding=0 cellspacing=0>
                        
                <thead>
                    <tr>
                        <th align="left" nowrap="nowrap">&nbsp;</th>
                        <th align="left" nowrap="nowrap">Name</th>
                        <th align="left" nowrap="nowrap">ID</th>                                
                        <th align="left" nowrap="nowrap">City/ST</th>
                        <th align="left" nowrap="nowrap">Industry</th>
                        <th align="left" nowrap="nowrap"><% if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then response.write("Delivery") else response.write("Users") %></th>                        
                    </tr>
                </thead>
                <tbody>

				<%
                do until rs.eof
					
					response.write("<tr>")
										
					'CCI Admins (bryan, steve)
					if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
						response.Write("<td width=""62"" nowrap=""nowrap"" align=""center"">")							
						response.Write("<a href=""#"" onClick=""SimpleModal.open('../_dialogs/popup_profile_stats.asp?cid=" & rs("customerid") & "', 500, 700, 'no'); return false;""><img src=""../_images/icons/16/system_monitor.png"" title=""Stats"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:7px; vertical-align:middle;""></a>")														
						response.Write("<a href=""../scripts/issues_add.asp?cid=" & rs("customerid") & "&action=add""><img src=""../_images/icons/16/page_add.png"" title=""Add Issue"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; padding-right:7px; vertical-align:middle;"" border=""0"" /></a>")
						response.Write("<a href=""profiles_menu.asp?cid=" & rs("customerid") & """><img src=""../_images/icons/16/page_blue_edit.png"" title=""Open Profile"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""vertical-align:middle;""></a>")
						response.Write("</td>")
					
					'ALL other users
					else
						response.Write("<td width=""39"" nowrap=""nowrap"" align=""center"">")
						if cLng(session(session("siteID") & "adminLoggedOn")) < 10 then
							response.Write("<a href=""../scripts/issues_add.asp?cid=" & rs("customerid") & "&action=add""><img src=""../_images/icons/16/page_add.png"" title=""Add Issue"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:7px; vertical-align:middle;"" /></a>")
						else
							response.Write("<a href=""../scripts/" & session(session("siteID") & "issuesEdit") & "?cid=" & rs("customerid") & "&action=add""><img src=""../_images/icons/16/page_add.png"" title=""Add Issue"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-right:7px; vertical-align:middle;"" /></a>")
						end if											
						response.Write("<a href=""profiles_menu.asp?cid=" & rs("customerid") & """><img src=""../_images/icons/16/page_blue_edit.png"" title=""Open Profile"" border=""0"" width=""16"" height=""16"" align=""absmiddle"" style=""padding-top:1px; padding-bottom:1px; vertical-align:middle;""></a>")											
						response.Write("</td>")			
								
					end if
						
					'NAME link
					response.Write("<td align=""left"" style=""cursor:pointer;"" onClick=""window.location='profiles_menu.asp?cid=" & rs("customerid") & "'"">")
					response.write("<a style=""color:#333333;"">" & rs("name") & "</a>")
					response.Write("</td>")
	
					response.Write("<td align=""left"">" & rs("customerid") & "</td>")
					response.Write("<td align=""left"">" & rs("city") & ", " & rs("state") & "</td>")
					response.Write("<td align=""left"">" & rs("industry") & "</td>")
					
					
					response.Write("<td align=""left"">" & rs("users") & "</td>")
										
					'CLOSE current row						
					response.write("</tr>")
					
					rs.movenext
				loop
				%>					

				</tbody>
			</table>
       	    <!-- STOP Reports table -->

			<%
			tableProperty = "sort: true, sort_config:{sort_types:['String','String','String','String','String','String']}, filters_row_index: 1, " _
						  & "alternate_rows: true, rows_counter: true, rows_counter_text: ""Rows: "", btn_reset: true, btn_reset_text: ""Clear"", " _
						  & "status_bar: true, col_0: """", col_1: ""input"", col_2: ""input"", col_3: ""input"", col_4: ""input"", col_5: ""input"", " _
						  & "col_width:[null,""40%"",null,null,null,null], paging: true, paging_length: 25, " _
						  & "enable_non_empty_option: true, or_operator: ';', on_keyup: true, on_keyup: true, on_keyup_delay: 1000, " _
						  & "highlight_keywords: true, " _
						  & "page_selector_type: ""input"", display_all_text: ""[ Clear ]"", loader_text: """", help_instructions: false, base_path: ""../_TableFilter/"", " _
						  & "themes: {name:['MyTheme'], src:['../_tablefilter/TF_Themes/myCM/myCMFilter.css'], description:['My stylesheet'], initialize:[null]}"						  
			%>
            <!-- STOP Reports table -->

            <!-- Fire table build -->
			<script language="javascript" type="text/javascript">
				//<![CDATA[
				var tableProp = {<% =tableProperty %>};
				//initiate table setup
				var tf1 = setFilterGrid("customerTable",tableProp);
				//]]>
			</script>

            <!-- Shadow table -->
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td height="5px" style="background:#D0D0D0;"><img src="../_images/x_cleardot.gif" width="1" height="1"></td>
              </tr>
            </table>
        
        </td>
    	<!-- STOP Main Middle Section -->

    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
  	</tr>
</table>
<!-- STOP Page -->

<script>
	//REFERENCE: <div> "loaderContainer" at top of page
	document.getElementById('loaderContainer').style.display = 'none';	//hide page loader
	document.getElementById('masterTable').style.display = '';			//display table holding page content
</script>

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<script language="javascript">	
	function pageReload() {  	
	 	window.location.reload();
    }
	//used by logon_dialog.asp to navigate back
	//to home page if neccssary
	function navigateAway(url) {
		window.location = url;
	}		
</script>	


