<%@ Language=VBScript %>
<%
'********************************************************************
' Product  : MyComplianceManagement.com (myCM)
' Version  : 3.0
' Modified : September 2010
' Copyright: Copyright (C) 2010 Compliance Concepts, Inc. 
'            You are NOT authorized to copy or distribute any part  
'            of this code! If you have any questions regarding this
'	     website, please contact support@ccius.com
'********************************************************************
Option explicit
Response.Buffer = true
const showSearchBox = false
const showDHTMLMenu = true
const helpID = 6
%>

<!--#include file="../_includes/_INCconfig_.asp"-->
<!--#include file="../_includes/_INCappDBConn_.asp"-->
<!--#include file="../_includes/_INCappFunctions_.asp"-->
<!--#include file="../_includes/_INCsecurity_.asp"-->
<!--#include file="../_tablefilter/default.asp"-->

<%
'===================================================================
'DO NOT DELETE, used by fuctions keys to fire evens in addEvents.asp
dim CRSID, extraClick, customerID, customerName, customerProfile
dim pageView : pageView = Request.QueryString("pageView")
'===================================================================

'Database
dim mySQL, cn, rs, cmd
dim pageSize

'Open Database Connection
call openDB()

'*********************************************************
'user & customerID for current user
dim sLogid, sCustomerID
sLogid = session(session("siteID") & "logid")
sCustomerID = session(session("siteID") & "customerid")			

'validate security access
if loadUser(null) = false then
	response.redirect ("../default.asp")
end if
'*********************************************************

'Get type of view being requested
dim tableProperty

dim showCRSID '...need for NEW search box
dim startDate
dim endDate

'User Fields in DETAILS tab
dim userLogID
dim userCustomerID
dim userFirstName
dim userLastName
dim userName
dim userPassword
dim userProgramAdmin
dim userApproveResolution
dim userNewUserEmail
dim userViewLegal
dim userViewPrivileged
dim userViewNotes
dim userViewStats
dim userViewProfile
dim userViewAppMenu
dim userEditReport
dim userEditNotes
dim userAddReport
dim userPhone
dim userEmail
dim showSummary
dim sortField
dim sortOrder
dim whereClause
dim userIssuesColumns
dim userLoginDate
dim userViewNotesCase
dim userViewNotesInv
dim userViewNotesMgr
dim userViewNotesRes
dim userEmailIssueTypes
dim userDeleteIssues
dim userViewNotesUFields
dim userViewUsers
dim userviewFileUpload
dim userDataFile
dim userReportsView
dim userExpires
dim userActive
dim userAutosave
dim userDelivery
dim userPriority
dim userNotes
dim issuesView
dim userTitle
dim userAddress
dim userCity
dim userState
dim userPostalCode
dim userPhone_Home
dim userPhone_Work
dim userPhone_Cell
dim userPhone_Pager
dim userPhone_Fax
dim userPhone_Other
dim emailAssignment
dim emailStatusChange
dim emailInvAssignment
dim emailInvStatusChange
dim emailDeadlineNotice
dim passwordStrength
dim passwordHistory
dim passwordReset
dim loginAttempts
dim homePage
dim homePageKey
dim securityLevel

dim userEmployee_ID
dim userJob_Class
dim userDivision
dim userRegion
dim userDepartment

'Work Fields
dim action, pgNotice
dim optArr, optArrIndex

'Used with Dynamic List Contact selections
dim intGroups, arrGroups, groupsI, intUsers
dim tmpUserID, tmpUserName, lstUserName
dim arrUsers, usersI
dim checkIssue

dim issueTypeCount

dim rowColor, col1, col2,rowSet
col1 = "#F0F0F0"
col2 = "#FFFFFF"

'Get action
action = trim(Request.QueryString("action"))
if len(action) = 0 then
	action = trim(Request.Form("action"))
end if
action = lCase(action)
if  action <> "edit" _
and action <> "add"  _
and action <> "del"  _
and action <> "copy"  then
	response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Action Indicator.")
end if

'Get UserID
if action = "edit" _
or action = "copy" _
or action = "del" then

	'DB Admins and CCI Admins
	if cLng(session(session("siteID") & "adminLoggedOn")) < 3 then
		userLogID = trim(Request.QueryString("recId"))
		
	'CUSTOMER use
	else
		if programAdmin = "N" then
			userLogID = sLogid
		else
			userLogID = trim(Request.QueryString("recId"))
		end if	
		
	end if	
	
	if userLogID = "" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid User ID.")
	end if
	
end if

'Can User add new user?
if action = "add" then
	if cLng(session(session("siteID") & "adminLoggedOn"))>=3 and ProgramAdmin="N" then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("You are not authorized to perform this function.")
	end if
end if


'Determin DHTML pop-up calendars to Initialize...will append Investigation TextAreas also
dim activeCalendars :activeCalendars = "'userExpires','passwordReset'"

'TinyMCE
dim activeElements : activeElements = "userNotes"


'used to set side top and side tabs after user saves record
dim topTab, sideTab
topTab = trim(Request.QueryString("top"))
if len(topTab) <= 0 then
	topTab = "details"
end if
sideTab = trim(Request.QueryString("side"))


'Get User Record
if action = "edit" or action = "copy" then

	'CCI staff looking up
	mySQL="SELECT Logins.*, Customer.appPasswordStrength " _
		& "		FROM Customer INNER JOIN Logins ON Customer.CustomerID = Logins.CustomerID " _
		& "		WHERE LOGID='" & userLogID & "' "	
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)

	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid User ID.")
	else			
		'only assign when editing a user
		if action = "edit" then	
			customerID 				= rs("CustomerID")
			userFirstName			= rs("FirstName")
			userLastName			= rs("LastName")
			userName				= rs("UserName")
			userPassword			= rs("Password")				
			userTitle				= rs("Title")
			userAddress				= rs("Address")
			userCity				= rs("City")
			userState				= rs("State")
			userPostalCode			= rs("PostalCode")
			userPhone_Home			= rs("Phone_Home")
			userPhone_Work			= rs("Phone_Work")
			userPhone_Cell			= rs("Phone_Cell")
			userPhone_Pager			= rs("Phone_Pager")
			userPhone_Fax			= rs("Phone_Fax")
			userPhone_Other			= rs("Phone_Other")			
			userEmail				= rs("Email")
			userAddReport			= rs("addReport")
			userEditReport			= rs("editReport")
			userActive				= rs("Active")
			userExpires				= rs("Expires")
			userAutosave			= rs("Autosave")
			userPriority			= rs("Priority") 
			userDelivery			= rs("Delivery")
			userNotes				= rs("Notes")			
			userDeleteIssues		= rs("DeleteIssues")			
			userEmailIssueTypes		= rs("EmailIssueTypes")			
			issuesView				= rs("IssuesView")			
			sortField				= rs("sortField")
			sortOrder				= rs("sortOrder")
			whereClause				= rs("whereClause")
			userIssuesColumns		= rs("IssuesColumns")			
			passwordStrength		= rs("appPasswordStrength")
			passwordHistory			= rs("passwordHistory")
			passwordReset			= rs("passwordReset")			
			loginAttempts			= rs("loginAttempts")
			homePage				= rs("homePage")
			homePageKey				= rs("homePageKey")
			securityLevel			= rs("securityLevel")	
			emailAssignment			= rs("emailAssignment")				
			emailStatusChange		= rs("emailStatusChange")	
			emailInvAssignment		= rs("emailInvAssignment")	
			emailInvStatusChange	= rs("emailInvStatusChange")	
			emailDeadlineNotice		= rs("emailDeadlineNotice")					
			userEmployee_ID			= rs("employee_id")	
			userJob_Class 			= rs("job_class")	
			userDivision			= rs("division")	
			userRegion				= rs("region")	
			userDepartment			= rs("department")			
		end if		
		userProgramAdmin		= rs("ProgramAdmin")
		userLoginDate			= rs("LoginDate")
	end if
	
	dim encryptedPass
	call openStoredProc("proc_GetPassword")
	cmd.Parameters.Append(cmd.CreateParameter("@UserName", adVarWChar, adParamInput, 50, userName))
	cmd.Parameters.Append(cmd.CreateParameter("@Passwrd", adVarWChar, adParamOutput, 50))
	cmd.Execute
	encryptedPass = cmd.Parameters("@Passwrd").Value
	userPassword = encryptedPass

	call closeRS(rs)
			
end if

'customerID so we can add new
if action = "add" then

	'get customer id
	customerID = trim(Request.QueryString("cid"))	
	'find password strength
	mySQL="SELECT Customer.appPasswordStrength " _
		& "		FROM Customer " _
		& "		WHERE CustomerID='" & customerID & "' "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	
	if rs.eof then
		response.redirect "../error/default.asp?errMsg=" & server.URLEncode("Invalid Customer ID.")
	else
		passwordStrength = rs("appPasswordStrength")
	end if
	call closeRS(rs)

	'set initial Active to --> Contact Only
	userActive = "C"
	
end if


'clean up any required variables
if action = "edit" or action = "copy" or action = "add" then

	if len(userLogID)<=0 or trim(userLogID)="" or isNull(userLogID) then userLogID = "0"
	if len(userAddReport)<=0 or trim(userAddReport)="" or isNull(userAddReport) then userAddReport = "Y"
	if len(userEditReport)<=0 or trim(userEditReport)="" or isNull(userEditReport) then userEditReport = "Y"
	if len(userActive)<=0 or trim(userActive)="" or isNull(userActive) then userActive = "N"
	if len(userExpires)<=0 or trim(userExpires)="" or isNull(userExpires) then userExpires = dateAdd("d",365,Date())
	if len(userAutosave)<=0 or trim(userAutosave)="" or isNull(userAutosave) then userAutosave = "0"
	if len(userDeleteIssues)<=0 or trim(userDeleteIssues)="" or isNull(userDeleteIssues) then userDeleteIssues = ""
	if len(issuesView)<=0 or trim(issuesView)="" or isNull(issuesView) then issuesView = "vwIssues"
	if len(passwordHistory)<=0 or trim(passwordHistory)="" or isNull(passwordHistory) then passwordHistory = ""
	if len(userAutosave)<=0 or trim(userAutosave)="" or isNull(userAutosave) then userAutosave = "60"
	if len(loginAttempts)<=0 or trim(loginAttempts)="" or isNull(loginAttempts) then loginAttempts = "0"
	
	if len(securityLevel)<=0 or trim(securityLevel)="" or isNull(securityLevel) then securityLevel = "30"
	if securityLevel=99 then securityLevel = 60
	
	if len(emailAssignment)<=0 or trim(emailAssignment)="" or isNull(emailAssignment) then emailAssignment = "N"
	if len(emailStatusChange)<=0 or trim(emailStatusChange)="" or isNull(emailStatusChange) then emailStatusChange = "N"
	if len(emailInvAssignment)<=0 or trim(emailInvAssignment)="" or isNull(emailInvAssignment) then emailInvAssignment = "N"
	if len(emailInvStatusChange)<=0 or trim(emailInvStatusChange)="" or isNull(emailInvStatusChange) then emailInvStatusChange = "N"
	if len(userProgramAdmin)<=0 or trim(userProgramAdmin)="" or isNull(userProgramAdmin) then userProgramAdmin = "N"

	if len(passwordReset)<=0 or trim(passwordReset)="" or isNull(passwordReset) then
		if lCase(passwordStrength) = "weak" then
			passwordReset = dateAdd("d",365,Date())
		elseif lCase(passwordStrength) = "medium" then
			passwordReset = dateAdd("d",180,Date())
		elseif lCase(passwordStrength) = "strong" then
			passwordReset = dateAdd("d",60,Date())
		end if	
	end if

end if


'get user counts
if action = "edit" or action = "copy" or action = "add" then

	'get allowed user count from customer record
	dim appNumUsers
	mySQL="SELECT Customer.appNumUsers " _
		& "		FROM Customer " _
		& "		WHERE CustomerID='" & customerID & "' "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if not rs.eof then
		appNumUsers = rs("appNumUsers")
	end if
	if len(appNumUsers) <= 0 or isNull(appNumUsers) or appNumUsers = "" then
		appNumUsers = "0"
	end if
	
	'get current user count incase they want to add more
	dim appUserCount
	mySQL="SELECT Count(LogID) AS [UserCount]" _
		& "		FROM Logins " _
		& "		WHERE CustomerID='" & customerID & "' AND Active='Y' AND SecurityLevel>10 "
	set rs = openRSopen(mySQL,3,adOpenStatic,adLockReadOnly,adCmdText,0)
	if not rs.eof then
		appUserCount = rs("UserCount")
	end if
	if len(appUserCount) <= 0 or isNull(appUserCount) or appUserCount = "" then
		appUserCount = "0"
	end if

end if

%>

<% 
dim pageTitle
if action = "edit" then
	pageTitle = "User: " & userFirstName & " " & userlastName 
else
	pageTitle = "User: (new user)"
end if
%>
<!--#include file="../_includes/_INCheader_.asp"-->

<link rel="stylesheet" type="text/css" href="../_jquery/password/simplePassMeter.css"/>    
<script language="JavaScript" src="../_jquery/password/jquery.simplePassMeter-0.4.js"></script>    

<form name="frm" id="frm" method="post" action="users_exec.asp" style="padding:0px; margin:0px;">

<!-- START Page -->
<table style="background:#FFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
  
	    <!-- START Left sided area -->  
		<td width="190" align="right" valign="top" >    

            <table width="180" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" style="padding-left:7px; padding-top:10px;">
                    <a href="users_edit.asp?action=edit&recID=<% =sLogid %>" class="sideMenuHeader"><% response.Write(session(session("siteID") & "firstname") & " " & session(session("siteID") & "lastname")) %></a>
					<% 
					if session(session("siteID") & "userGhost") = "Yes" then
						response.write("<em>ghosted</em>")
					end if
					%>
                </td>
              </tr>
        
              <!-- START Left sided menu -->
              <tr>
                <td align="right" style="padding-left:7px; padding-top:10px;">
    
                    <!-- START side menu include -->                
                    <!--#include file="../scripts/tree/tree.asp"-->              
                    <% if session(session("siteID") & "adminLoggedOn") < "10" then %>      
	                    <script>
   		                	stExpandSubTree('myCMtree',1,0);
       		            </script>
            		<% else %>
	                    <script>
							stCollapseSubTree('myCMtree',1,0);
							stCollapseSubTree('myCMtree',2,0);							
       		            </script>
                    <% end if %>
                    <!-- STOP side menu include -->
                                    
                </td>
              </tr>
              <!-- STOP Left sided menu -->
        
            </table>

		</td>
    	<!-- STOP Left sided area -->


        <!-- START Main Middle Section -->
        <td align="center" valign="top" style="padding:10px; border-right:#B3B3B3 solid 1px; border-left:#B3B3B3 solid 1px;">    

			<%
			'used to STOP updating the cookie crumb if page is being recalled from a SAVE
			dim updateCrumb
            if len(session(session("siteID") & "errMsg")) > 0 or len(session(session("siteID") & "okMsg")) > 0 then
				updateCrumb = "false"
            end if
			%>
            <div id="breadCrumb">
            	<a href="../default.asp">Home</a>&nbsp;&raquo;
				<script>
					document.write(CookieCrumbs(5).GetCrumbs('<% =updateCrumb %>'));
       	        </script>
            </div>
    
            <div style="text-align:left; margin-bottom:10px;">				
                <div style="float:left; padding-right:2px; margin-right:2px;"><img src="../_images/icons/32/user.png" title="" width="32" height="32" align="absmiddle" style="vertical-align:middle;"></div>
                <div>
                	<span class="pageTitle">User: <% if action = "edit" then response.write(userFirstName & " " & userlastName) else response.Write("<i>&lt;none assigned&gt;</i>") %></span>
                    <%
					if (cLng(session(session("siteID") & "adminLoggedOn")) < 10) or (programAdmin = "Y") then
						response.write("<br/>:: <a href=""../profiles/profiles_menu.asp?cid=" & customerID & """>" & getCustomerName(customerID) & "</a>")
					else
						response.write("<br/>:: " & getCustomerName(customerID) )
					end if					
					%>                    
                </div>
            </div>                   
                               
            <!-- START information box -->
            <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    
                    <!-- START Information message -->             
                    <td style="width:60%;">
                        <fieldset class="infoBox">
            	            <legend>Information</legend>
	                        <div align="left">
    	                        <img src="../_images/icons/16/information.png" alt="Information" width="16" height="16" align="absmiddle" style="margin-right:3px;">Add or modify this user's properties using the form below.
								<% response.write( getInlineHelp("getPageURL", 0) ) %>
                            </div>
                        </fieldset>                                    
                    </td>
                    <!-- STOP Information message -->             
                                    
                    <!-- START Information buttons -->                
                    <td width="40%" valign="top" style="padding-top:7px;">               	
                    	<!-- DBAdmin, CCI Admin adn Program Admin -->
                    	<% if (cLng(session(session("siteID") & "adminLoggedOn"))<3 or ProgramAdmin="Y") and userLogID > 0 then %>
                            <div class="infoButtons">
                                <a href="#" onClick="confirmPrompt('Confirm Login','Login to myCM as this user?','redirect','../logon_ghost.asp?recID=<% =userLogID %>&action=ghost'); return false;"><img src="../_images/icons/24/key_go.png" width="24" height="24" align="absmiddle" style="margin-right:3px;"/></a>
                                <a href="#" onClick="confirmPrompt('Confirm Login','Login to myCM as this user?','redirect','../logon_ghost.asp?recID=<% =userLogID %>&action=ghost'); return false;">Login as User</a>
                            </div>                   
                    	<% end if %>                    
                    </td>
                    <!-- STOP Information buttons -->
                        
                </tr>
            </table>        
            <!-- STOP information box -->

    
    		<!-- START message notification box -->
            <%
            if len(session(session("siteID") & "errMsg")) > 0 then
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageERROR",session(session("siteID") & "errMsg"))
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			if len(session(session("siteID") & "okMsg")) > 0 then			
   				response.write("<table width=""100%"" cellpadding=""0"" cellspacing=""0""><tr>")
				response.write("   <td align=""left"" class=""clearFormat"" style=""padding-right:2px;"">")			
                call systemMessageBox("systemMessage","statusMessageOK",session(session("siteID") & "okMsg"))		
				response.write("   </td>")
				response.write("</tr></table>")				
            end if
			'clear system messages
			call clearSessionMessages()
            %>
    		<!-- STOP message notification box -->


            <!-- START Top Page section with tabs & buttons -->
            <table id="table_tabs" width="100%" cellpadding="0" cellspacing="0">            
                <tr>
                
                    <!-- START Top Page Tabs-->
                    <td height="40" width="70%" valign="top">
                        <ul id="ul_tabs" class="tabs">
                            <li id="details" style="width:125px;" class="on" onClick="clickTopTab('details','general'); resetSystemMsg('systemMessage');"><div>Account</div></li>
                            <li id="settings" style="width:125px;" class="off" onClick="clickTopTab('settings','preferences'); resetSystemMsg('systemMessage');"><div>Settings</div></li>
                            <li id="reports" style="width:125px;" class="off" onClick="clickTopTab('reports',''); resetSystemMsg('systemMessage');"><div>Reports</div></li>                            
                            <!-- the last id must be set to "end" -->
                            <li id="end" class="empty" style="width:250px;"><div>&nbsp;</div></li>
                        </ul> 
                        
                        <input type="hidden" name="topTab" id="topTab" value="<% =topTab %>">                                                                
                        <input type="hidden" name="sideTab" id="sideTab" value="<% =sideTab %>">
                        
                    </td>                
                    <!-- STOP Top Page Tabs-->              
                    
                    <!-- START Submit Button at top of page--> 
                    <td nowrap style="padding-bottom:10px;" align="left">
                        <%
                        if action = "edit" then
                        %>
                            <!-- SAVE button -->
							<a class="myCMbutton" href="#" accesskey="S" onClick="this.blur(); resetSystemMsg('systemMessage'); submitForm('<% =action %>'); return false;"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
                        <%
                        elseif (action = "add" or action = "copy") and uCase(programAdmin) = "Y" then
                        %>
                            <!-- ADD button -->                            
							<a class="myCMbutton" href="#" accesskey="A" onClick="this.blur(); resetSystemMsg('systemMessage'); submitForm('<% =action %>'); return false;"><span class="add" style="padding-right:15px;"><u>A</u>dd</span></a>
                        <%
                        end if
                        %>
                        
                        <input type=hidden name="customerID" id="customerID" value="<% =customerID %>"> 
                        <input type=hidden name="userLogID" id="userLogID" value="<% =userLogID %>"> 
                        
                        <input type=hidden name="sortField" id="sortField" value="<% =sortField %>"> 
                        <input type=hidden name="sortOrder" id="sortOrder" value="<% =sortOrder %>"> 
                        <input type=hidden name="WhereClause" id="WhereClause" value="<% =WhereClause %>"> 
                        <input type=hidden name="userIssuesColumns" id="userIssuesColumns" value="<% =userIssuesColumns %>">                         
                        
                        <input type=hidden name="action" id="action" value="<%=action%>">
                        
                        <!-- CLOSE button -->
                        <%                        
                        if pageView = "users" then
						%>
							<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); navigateAway('users.asp?cid=<% =customerID %>'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>
                        <%                        
						else
						%>
							<a class="myCMbutton" href="#" accesskey="C" onClick="this.blur(); navigateAway('../default.asp'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>
                        <%                        
                        end if
						%>                           

                    </td>
                    <!-- STOP Submit Button -->
                             
                </tr>
            </table>
            <!-- STOP Top Page Tabs -->


            <!-- START Side Page Tabs [details] "id=tab:details" defines the section for the Top Tabs -->
            <!--#include file="../_includes/_INCuserDetails_.asp"-->            
            <!-- STOP Side Page Tabs [details] -->    
        

            <!-- START Side Page Tabs [settings] "id=tab:settings" defines the section for the Top Tabs -->
            <!--#include file="../_includes/_INCuserSettings_.asp"-->            
            <!-- STOP Side Page Tabs [settings] -->


            <!-- START Side Page Tabs [settings] "id=tab:settings" defines the section for the Top Tabs -->
            <!--#include file="../_includes/_INCuserReports_.asp"-->            
            <!-- STOP Side Page Tabs [settings] -->

    
            <!-- START Final table -->        
            <table class="formTable" width="100%" cellpadding="0" cellspacing="0">      
                                                              
                <tr>
                    <td width="70%" valign="top" align="left" nowrap>&nbsp;</td>
                    
                    <!-- START Submit Button at bottom of page -->                 
                    <td valign="top" align="left" nowrap>                
                        <%
                        if action = "edit" then
                        %>
							<a class="myCMbutton" href="#" onClick="this.blur(); resetSystemMsg('systemMessage'); submitForm('<% =action %>'); return false;"><span class="save" style="padding-right:10px;"><u>S</u>ave</span></a>
                        <%
                        elseif (action = "add" or action = "copy") and uCase(programAdmin) = "Y" then
                        %>
                            <!-- ADD button -->                            
							<a class="myCMbutton" href="#" onClick="this.blur(); resetSystemMsg('systemMessage'); submitForm('<% =action %>'); return false;"><span class="add" style="padding-right:15px;"><u>A</u>dd</span></a>
                      	<%
                        end if
                        %>
                        <!-- CLOSE button -->
                        <%                        
                        if pageView = "users" then
						%>
							<a class="myCMbutton" href="#" onClick="this.blur(); navigateAway('users.asp?cid=<% =customerID %>'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>
                        <%                        
						else
						%>
							<a class="myCMbutton" href="#" onClick="this.blur(); navigateAway('../default.asp'); return false;"><span class="cancel" style="padding-right:10px;"><u>C</u>lose</span></a>
                        <%                        
                        end if
						%>                           
                    </td>
                    <!-- STOP Submit Button --> 
    
                </tr>      
                                      
            </table>
            <!-- STOP Final table -->               
    
	    </td>         
	    <!-- STOP Main Middle Section -->

 
    	<!-- START Right side area -->
		<td width="2%" align="left" valign="top">&nbsp;
        	
        </td>        
    	<!-- STOP Right side area -->
        
	</tr>
</table>
<!-- STOP Page -->

</form>

<!--#include file="../_includes/_INCfooter_.asp"-->

<%
'close database connection
call closeDB()
%>

<script type="text/javascript"> 
	//load form where user last was...pulled from query URL or form submit
	window.onload = function () {
		//used so JQuery Spinner would initialize. If the div/table is 
		//defaulted to display:none the height of the spinner is 0 and doesn't
		//present right. leave it visible and turn off here after it's been loaded
		document.getElementById('table:details:employee').style.display = 'none';	
		document.getElementById('table:details:login').style.display = 'none';		
		document.getElementById('tab:settings').style.display = 'none';
		document.getElementById('tableAccount').style.display = '<% if userActive="N" or userActive="C" then response.write("none") %>';	
		document.getElementById('settings').style.display = '<% if userActive="N" or userActive="C" then response.write("none") %>';
		document.getElementById('reports').style.display = '<% if userActive="N" or userActive="C" then response.write("none") %>';		
		//show user selected tabs
		clickTopTab('<% =topTab %>','<% =sideTab %>');
		clickSideTab('<% =topTab %>','<% =sideTab %>');	
	}		
</script>


<script type="text/javascript"> 
	// Initialize TinyMCE with the tab_focus option 
	var dateCal;
	dateCal = new dhtmlxCalendarObject([<% =activeCalendars %>], true, {isWinHeader:true, headerButtons:"XT", isWinDrag:false, isYearEditable:true, isMonthEditable:true });
	dateCal.setSkin("simplegrey");
	dateCal.setDateFormat("%m/%d/%Y");
	dateCal.setYearsRange(2000, 2020);
	dateCal.setHeaderText("Issue Date");
</script>     


<script language="javascript">
	function submitForm(frmAction) {

		//edit or add user
		if (frmAction=="edit" || frmAction=="add") {
			
			//used to check password strenght
			var valid = document.getElementById('passwordScore');
			var pass1 = document.getElementById('userPassword1').value;
			var pass2 = document.getElementById('userPassword2').value;
			var passStrength = document.getElementById('passwordStrength').value;
			var passHistory = document.getElementById('passwordHistory').value;
			
			//validate required fields
			if ($("#userFirstName").val()=='') {jAlert('Invalid First Name. Please correct and re-submit', 'myCM Alert'); return false;}
			if ($("#userLastName").val()=='') {jAlert('Invalid Last Name. Please correct and re-submit', 'myCM Alert'); return false;}		
			if ($("#securityLevel").val()=='') {jAlert('Invalid Security Level. Please correct and re-submit', 'myCM Alert'); return false;}
	
			//only check these if user is set to ACTIVE -- userActiveYes is visible to ProgramAdmins
			if (document.getElementById('userActiveYes')) {
				if (document.getElementById('userActiveYes').checked==true) {
					if ( !isValidEmailAddress( $("#userEmail").val() ) ) { jAlert('Invalid user e-mail address.', 'Send Login'); return false; }
					if ($("#userName").val()=='') {jAlert('Invalid User Name. Please correct and re-submit', 'myCM Alert'); return false;}
					if ($("#userPassword").val()=='' && ($("#userPassword1").val()=='' && $("#userPassword2").val()=='')) {jAlert('Invalid Password.. Please correct and re-submit', 'myCM Alert'); return false;}
				}
			}
			//userActive is visible to all non-ProgramAdmins
			else if (document.getElementById('userActive')) {
				if (document.getElementById('userActive').value=="Y") {
					if ( !isValidEmailAddress( $("#userEmail").val() ) ) { jAlert('Invalid user e-mail address.', 'Send Login'); return false; }
					if ($("#userName").val()=='') {jAlert('Invalid User Name. Please correct and re-submit', 'myCM Alert'); return false;}
					if ($("#userPassword").val()=='' && ($("#userPassword1").val()=='' && $("#userPassword2").val()=='')) {jAlert('Invalid Password... Please correct and re-submit', 'myCM Alert'); return false;}
				}			
			}
			
			//password entered
			if (pass1) {
				//good password so far...
				if (valid.innerHTML >= 25) {
					if(pass1 == pass2) {
						//check password history
						////if (passStrength == "Strong") {
							var temp = passHistory.split("*|*");
							var i;
							for(i=0; i<temp.length; i++) {
								if (pass1 == temp[i]) {
									jAlert('The security settings prevent new passwords<br/>from mathcing any of the past 6 passwords used.<br/><br/>Please try again.', 'myCM Alert');								
									return false;
								}
							}
							//good, submit
							document.frm.submit();
						////}
						//no need to check
						////else {
						////	document.frm.submit();
						////}
						
					}
					//passwords do NOT match
					else {
						jAlert('Passwords do not match. Please try again.', 'myCM Alert');
						return false;
					}
				}
				//bad password, score below 25
				else {
					jAlert('Password invalid. Please try again.', 'myCM Alert');				
					return false;
				}			
			}
			//NO password change
			else {
				document.frm.submit();
			}

		// load standard/default reports for user
		} else if (frmAction=="loadreports") {
			var secLevel = document.getElementById('securityLevel').value;
			//make sure user agrees to change				
			jConfirm('Load standard report set for a <br/>Security Level [' + secLevel + '] user?', 'Confirm Reqest', function(r) {
				//user agrees, remove categories
				if (r==true) {
					document.getElementById('action').value = frmAction;
					document.frm.submit();			
				}
				//user disagrees
				else {
					return false;
				}
			});		
		}
		
	}		
</script>

<script language="javascript">
	//for adding a single investigation using 
	//the ADD button
	function navigateAway(url) {
		var title = "Cancel Changes";
		var msg = "Changes have been made to this user and <strong>must<br>be saved</strong> before continuing.<br><br>Continue and cancel changes?";
		tinyMCE.triggerSave();	//trigger save/convert of all TinyMCE text areas for proper save
		if(isFormChanged(document.frm)==true) {		
			confirmPrompt(title,msg,'redirect',url);
		}
		else {
			window.location = url;
		}
	}		
</script>

<script type="text/javascript"> 
    // Initialize TinyMCE with the tab_focus option 
    tinyMCE.init({ 
        mode : "exact",
        elements : "<% =activeElements %>",
        auto_resize : true,
        theme : "advanced",
        content_css : "../_css/tinyMCE.css",
        plugins : "tabfocus,searchreplace,fullscreen,insertdatetime,paste,inlinepopups",
        theme_advanced_buttons1 : "bold,italic,underline,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,fontsizeselect,forecolor,backcolor,separator,search,separator,pasteword,separator,insertdate,inserttime,separator,fullscreen",
        theme_advanced_buttons2 : "", 
        theme_advanced_buttons3 : "", 
        theme_advanced_toolbar_location : "top", 
        theme_advanced_toolbar_align : "center", 
        onchange_callback : "setTinyMCEdirty",
        plugin_insertdate_dateFormat : "%A, %B %d, %Y",
        plugin_insertdate_timeFormat : "%I:%M %p",
        tab_focus : ':prev,:next'
    }); 
</script>     

<script>
	//JSON QUERY BUILDER used to search database
	buildJQuery('logins','priority','userPriority','<% =customerID %>');
	buildJQuery('logins','delivery','userDelivery','');
	
	function buildJQuery(table,field,form_field,custid) {
																
		$(function() {
			function split( val ) {
				return val.split( /,\s*/ );
			}
			function extractLast( term ) {
				return split( term ).pop();
			}                                                            
			$("#"+form_field)
				// don't navigate away from the field on tab when selecting an item
				.bind( "keydown", function( event ) {
               		if ( event.keyCode === $.ui.keyCode.TAB &&
                       	$( this ).data( "autocomplete" ).menu.active ) {
                       		event.preventDefault();
                    }
                })
               	.autocomplete({
              		source: function( request, response ) {
                  		$.getJSON( "../_jquery/suggestBox/json-data.asp?view="+table+"&field="+field+"&cid="+custid, {
                      		term: extractLast( request.term )
                   		}, response );
                  	},
               		search: function() {
                  		// custom minLength
                      	var term = extractLast( this.value );
                   		if ( term.length < 1 ) {
                      		return false;
                   		}
               		},
               		focus: function() {
               			// prevent value inserted on focus
               			return false;
                  	}
           		});
		});																																
	}
</script>         

<script> 
	function sendLogin() {
		var useremail = $("#userEmail").val();

		//make sure an email was pasted
		if( !isValidEmailAddress( useremail ) ) {
			jAlert('Invalid user e-mail address.', 'Send Login');
		}
		else {
			//make sure user agrees to change				
			jConfirm('Send user their login credentials?', 'Confirm Request', function(r) {
				//user agrees, remove categories
				if (r==true) {
					SimpleModal.open('../logon_lost.asp?recid='+useremail+'&action=adminsend', 275, 475, 'no');		
				}
				//user disagrees
				else {
					return false;
				}
			});		
		}
	}

	function isValidEmailAddress(emailAddress) {
		//var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
		//return pattern.test(emailAddress);		
		var pos = emailAddress.lastIndexOf("@");
		if (pos > 0 && (emailAddress.lastIndexOf(".") > pos) && (emailAddress.length - pos > 4)) { return true; } else { return false; }
	};
</script> 

